#include "Model.h"
#include "../CommonStuff/Defines.h" //DELIMITER_TO_STRING_ITEMS
#include "../Log/errors.h"
#include "../DataCode/DataModel/DataMetaInfo.h" /* dataInfo */
/** Task ������� ������ �� �����! #include "../Task/Task.h" */

#include "../Params/Params.h"
#include "../Distributions/Distributions.h"
#include "../Distributions/erfinv.h"
#include <math.h> /* NAN */

#include <cfloat>
#include <sstream>
#include <algorithm>
/** static in IModel */
constexpr const char* IModel::modelNames[FAKE_MODEL];
/** static in IModel */
enum IModel::ModelsTypes IModel::getModelTypeByModelName(char const * const modelName)
{
    if(NULL == modelName) return FAKE_MODEL;
    size_t i=0;
    for(i=0; i < IModel::ModelsTypes::FAKE_MODEL; ++i)
    {
        size_t pos = std::strcmp(modelName, IModel::modelNames[i]);
        if (0 == pos)
        {
            return (enum IModel::ModelsTypes)i;
        }
    }
    return IModel::ModelsTypes::FAKE_MODEL;
}
///TODO!!! ���� ������ ��� �������? ��������������� �������, ������� ����� ��� ���������� DataInfo'�� � ������� IParams ��� IParamsUnion ���������� ������!
/// extra stuff for the following int IModel::parseDataInfoAndCreateModelParams(const enum IModel::ModelsTypes mt, size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams, Logger* const logger)
int findModelNameInDataInfoArray(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, const char * modelName)
{
    int index = -1;
    for(size_t i=0; i< totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dataModelName == modelName)
        {
            return i;
        }
    }
    return index;
}
/***********************************************
* Base abstract IModel class implementation
************************************************/
/** anonymous namespace to hide private implementations into this cpp-file */
namespace
{
/** class container */
/** implementation of abstract Base class IModel */
class IModel_Impl : public virtual IModel
{
public:
    /** Public CTOR ! BUT it is visible into this cpp-file only! */
    IModel_Impl(const size_t& dim);
    virtual ~IModel_Impl();
    /** Task ������� ������ �� �����! virtual void setTask(Task* const & task);
    */
    /** ��� ��� ������ ����������� � ���� ������� ������! */
    virtual std::string toString() const;

    virtual void setLog(Logger* const & log);
    virtual void getModelType(enum IModel::ModelsTypes& modelType) const;
    /** to parse DataInfo
    and transform double** to IParams1D
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const;
    /** ���������� ������ ����� ���� IParamsUnion */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log);
    /** ���� ������ �������� size, �.�. ���������� ���������� ������ ���� �����,
    ����� ������ ������� ���������� ��������� ������
    �� �������� �������� � �������������� �������,
    ��� ��� �������� ��������� � ������� ����� ������� ������ ������ ������!
    � ����������� ������ ���������� �������������� ������
    ����� ��������� ������ ������� ���� ������!
    */
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
protected:
    virtual int createEmptyParamsOfModel(IParams*& ptrToIParams, size_t dim) const;
    ///TOOD!!! ����� ����, ��� ����� ������ � ������ �����, ��� ������� ���� ����� ���� ������ ������������ � �������?
    void parsingFailed(std::string msg) const;
    /** dimension of Any Parametric Model's Params space */
    size_t m_dimension;
    /** Task ������� ������ �� �����! Task* _task;
    */
    Logger* m_logger;
    /** m_modelType must be defined ONLY ONCE!
    In CTOR()! m_modelType REDEFINITION is forbidden!
    */
    enum IModel::ModelsTypes m_modelType;
private:

    /** assign and copy CTORs are disabled */
    IModel_Impl(const IModel_Impl&);
    void operator=(const IModel_Impl&);
};

/** implementation of abstract Base class IModelPlain
    It may be treated as non-parametric model.
*/
class IModelPlain_Impl : public virtual IModel_Impl,
    public virtual IModelPlain
{
public:
    /** Public CTOR ! BUT it is visible into this cpp-file only! */
    IModelPlain_Impl(const size_t& dim);
    virtual ~IModelPlain_Impl();

    /** ��� ��� ������ ����������� � ���� ������� ������! */
    using IModel_Impl::toString;
    /** Task ������� ������ �� �����! using IModel_Impl::setTask;
    */
    using IModel_Impl::setLog;
    using IModel_Impl::getModelType;
    using IModel_Impl::parseAndCreateModelParams;
    /** ���� ������ �������� size, �.�. ���������� ���������� ������ ���� �����,
    ����� ������ ������� ���������� ��������� ������
    �� �������� �������� � �������������� �������,
    ��� ��� �������� ��������� � ������� ����� ������� ������ ������ ������!
    � ����������� ������ ���������� �������������� ������
    ����� ��������� ������ ������� ���� ������!
    */
    using IModel_Impl::canCastModelParams;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParams1D
    */
    using IModel_Impl::parseDataInfoAndCreateModelParams;

    void setDimension(const size_t& dim);
    int getDimension(size_t& dim) const;

protected:
    using IModel_Impl::createEmptyParamsOfModel;
    ///TOOD!!! ����� ����, ��� ����� ������ � ������ �����, ��� ������� ���� ����� ���� ������ ������������ � �������?
    using IModel_Impl::parsingFailed;
    /** m_modelType must be defined ONLY ONCE!
    In CTOR()! m_modelType REDEFINITION is forbidden!
    */
private:
    /** assign and copy CTORs are disabled */
    IModelPlain_Impl(const IModelPlain_Impl&);
    void operator=(const IModelPlain_Impl&);
};

/**************************************************
* Base abstract IModelPolynomial class implementation
***************************************************/
class IModelPolynomial_Impl : public virtual IModel_Impl,
    public virtual IModelPolynomial
{
public:
    static IModelPolynomial_Impl* createEmptyModel(const size_t& dim);
    virtual void setDimension(const size_t& dim);
    virtual int getDimension(size_t& dim) const;
    /** ���� ������ �������� size,
     �.�. ���������� ���������� �������������� ������
     ������ ���� �����, ����� ������ ������� ���������� ��������� ������
     �.�. ����������� �������������� ������ ������ ���� � size!
    */
    virtual int createEmptyParamsOfModel(IParams*& ptrToIParams) const;
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParams1D
    */
    using IModel_Impl::parseDataInfoAndCreateModelParams;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;

private:
    /** No public ctor! Factory is in use! */
    IModelPolynomial_Impl(const size_t& dim);
    /** assign and copy CTORs are disabled */
    IModelPolynomial_Impl(const IModelPolynomial_Impl&);
    void operator=(const IModelPolynomial_Impl&);
};

/**************************************************
* Base abstract IModelGaussian class implementation
***************************************************/
class IModelGaussian_Impl : public virtual IModel_Impl,
    public virtual IModelGaussian,
    public virtual I_Probabilistic_Model,
    public virtual I_PF_Model
{
public:
    static IModelGaussian_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToIParams) const;

    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsGaussian
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_Probabilistic_Model interface implementation */
    virtual int CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t = 0) const;
    virtual int PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t = 0) const;
    virtual int getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const;
    virtual int generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t = 0) const;
    virtual int generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const;
    /** I_PF_Model interface implementation */
    virtual double estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
    virtual double estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;
///TODO!!!
    virtual int initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TODO!!!
    virtual int generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;

private:
    /** No public ctor! Factory is in use! */
    IModelGaussian_Impl();
    /** assign and copy CTORs are disabled */
    IModelGaussian_Impl(const IModelGaussian_Impl&);
    void operator=(const IModelGaussian_Impl&);
    int ptrAdditiveModelParamsToPtrParamsGaussian(int indexOfParams, const IParamsUnion* h, IParamsGaussian*& pg) const;
};///end IModelGaussian_Impl
/**************************************************
* Base abstract IModelWiener class implementation
***************************************************/
class IModelWiener_Impl : public virtual IModel_Impl,
    public virtual IModelWiener,
    public virtual I_Probabilistic_Model,
    public virtual I_PF_Model
{
public:
    static IModelWiener_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToIParams) const;

    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo and transform double** to IParamsWiener */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_Probabilistic_Model interface implementation */
    virtual int CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t = 0) const;
    virtual int PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t = 0) const;
    virtual int getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const;
    virtual int generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t = 0) const;
    virtual int generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const;
    /** I_PF_Model interface implementation */
    double estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
    double estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;
///TODO!!!
    virtual int initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TODO!!!
    virtual int generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;

private:
    /** No public ctor! Factory is in use! */
    IModelWiener_Impl();
    /** assign and copy CTORs are disabled */
    IModelWiener_Impl(const IModelWiener_Impl&);
    void operator=(const IModelWiener_Impl&);
    int ptrAdditiveModelParamsToPtrParamsWiener(int indexOfParams, const IParamsUnion* h, IParamsWiener*& pw) const;
};///end IModelWiener_Impl
/**************************************************
* Base abstract IModelPareto class implementation
***************************************************/
class IModelPareto_Impl : public virtual IModel_Impl,
    public virtual IModelPareto,
    public virtual I_Probabilistic_Model,
    public virtual I_PF_Model
{
public:
    static IModelPareto_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToIParams) const;

    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsPareto
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_Probabilistic_Model interface implementation */
    virtual int CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t = 0) const;
    virtual int PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t = 0) const;
    virtual int getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const;
    virtual int generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t = 0) const;
    virtual int generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const;
    /** I_PF_Model interface implementation */
    int initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
    int generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TOOD!!!
    virtual double estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
///TODO!!!
    virtual double estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;

///TODO!!! move to IntervalEstimator bool estimateAndFillParamBoundaries(const vector<double>& points, double deviation, double regression, IParams* start, IParams* end);
private:
    /** No public ctor! Factory is in use ! */
    IModelPareto_Impl();
    /** assign and copy CTORs are disabled */
    IModelPareto_Impl(const IModelPareto_Impl&);
    void operator=(const IModelPareto_Impl&);
    int ptrAdditiveModelParamsToPtrParamsPareto(int indexOfParams, const IParamsUnion* h, IParamsPareto*& pp) const;
};///end IModelPareto_Impl

/**************************************************
* Base abstract IModelPoisson class implementation
***************************************************/
class IModelPoisson_Impl : public virtual IModel_Impl,
    public virtual IModelPoisson,
    public virtual I_Probabilistic_Model
///TODO!!!     public virtual I_PF_Model
{
public:
    static IModelPoisson_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToIParams) const;

    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsPoisson
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;

    /** I_Probabilistic_Model interface implementation */
    virtual int CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t = 0) const;
    virtual int PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t = 0) const;
    virtual int getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const;
    virtual int generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t = 0) const;
    virtual int generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const;

private:
    /** No public ctor! Factory is in use ! */
    IModelPoisson_Impl();
    /** assign and copy CTORs are disabled */
    IModelPoisson_Impl(const IModelPoisson_Impl&);
    void operator=(const IModelPoisson_Impl&);
    int ptrAdditiveModelParamsToPtrParamsPoisson(int indexOfParams, const IParamsUnion* h, IParamsPoisson*& ps) const;
};///end IModelPoisson_Impl

/*******************************************************
* Base abstract IModelPoissonDrvnParetoJumpsExpMeanReverting class implementation
********************************************************/
class IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl : public virtual IModel_Impl,
    public virtual IModelPoissonDrvnParetoJumpsExpMeanReverting,
    public virtual I_Probabilistic_Model,
    public virtual I_PF_Model
{
public:
    static IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl* createEmptyModel();

    virtual int createEmptyParamsOfModel(IParams*& ptrToIParams) const;
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsPoissonDrvnParetoJumpsExpMeanReverting
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_Probabilistic_Model interface implementation */
    virtual int CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t = 0) const;
    virtual int PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t = 0) const;
    virtual int getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const;
    virtual int generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t = 0) const;
    virtual int generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const;
    /** I_PF_Model interface implementation */
    int initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
    int generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TOOD!!!
    virtual double estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
///TODO!!!
    virtual double estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;

private:
    /** No public ctor! Factory is in use ! */
    IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    /** assign and copy CTORs are disabled */
    IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl(const IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
    void operator=(const IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
    int ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(int indexOfParams, const IParamsUnion* h, IParamsPoissonDrvnParetoJumpsExpMeanReverting*& ppp) const;
    /** I_Probabilistic_Model interface PRIVATE implementation */
    virtual int generateFirstRV(const IParamsPoissonDrvnParetoJumpsExpMeanReverting* const hyp, double& randomValue) const;
    /** PARAM [IN|OUT] double& randomValue = previous ordinata as IN, next ordinata as OUT
    */
    virtual int generateNextRV(const IParamsPoissonDrvnParetoJumpsExpMeanReverting* const hyp, double& randomValue, unsigned dt = 1) const;

};///end IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl

} ///end namespace

/******************** IMPLEMENTATIONs ****************************/
/****************************************************************
*   Base class - implementation
*****************************************************************/
///static IModel_Impl!
/** ���������� ������, ��� �������� ����������� Params */
/** ���������� ������ ����� ���� IParams */
int IModel_Impl::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPoissonDrvnParetoJumpsExpMeanReverting* params = IParamsPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::Params1D, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse Params1D");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** always return ONLY ONE IParams */
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}

IModel_Impl::IModel_Impl(const size_t& dim)
{
    m_dimension = dim;
    m_logger    = NULL;
    m_modelType = FAKE_MODEL;
}

/***************************
*        DTOR()
***************************/
IModel_Impl::~IModel_Impl()
{
    /* NO WAY!
        if ( _task != 0) {
            delete _task;
        }
        if (m_logger != 0) {
            delete m_logger;
        }
    */
}

std::string IModel_Impl::toString() const
{
    return (std::string) IModel::modelNames[m_modelType];
}
/*OLD REDUNDANT
void IModel_Impl::setTask(Task* const & task)
{
    m_task = task;
}
*/
/** Invariant for every model
*/
void IModel_Impl::setLog(Logger* const & log)
{
    m_logger = log;
}

void IModel_Impl::getModelType(enum IModel::ModelsTypes& modelType) const
{
    modelType = m_modelType;
}
/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModel_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::FAKE_PARAMS_TYPES;
}

/** to parse DataInfo and transform double** to IParams1D */
int IModel_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const
{

    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = ptrToPtrDI[0]->m_dataSetSizes[0];
    IParams * ptrIParams1D = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, size);
    if(ptrIParams1D == NULL)
    {
        delete ptrIParams1D;
        return _ERROR_NO_ROOM_;
    }

    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParams1D;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� ����������� ���� Params ���������!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParams1D;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
            if(!isNAN_double(tmpDbl))
                ptrIParams1D->setParam(j,tmpDbl);
        }
    }
    ptrToIParams = ptrIParams1D;
    return _RC_SUCCESS_;
}
///TOOD!!! ����� ����, ��� ����� ������ � ������ �����, ��� ������� ���� ����� ���� ������ ������������ � �������?
void IModel_Impl::parsingFailed(std::string msg) const
{
//"Failed to parse parameters"
    m_logger->error(ALGORITHM_MODELDATA_ERROR, msg);
}

int IModel_Impl::createEmptyParamsOfModel(IParams*& ptrToIParams, size_t dim) const
{
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, dim);
    if( ptrIParams == NULL )
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToIParams = ptrIParams;
    return _RC_SUCCESS_;
}
/** IParams always to be casted to Params1D - the simplest Params reincarnation. */
bool IModel_Impl::canCastModelParams(IParams * params) const
{
    if (!params)
        return false;
    else
        return true;
}
/****************************************************************
* Plain IModel class implementation
****************************************************************/
/***************************
*        CTOR()
***************************/
IModelPlain_Impl::IModelPlain_Impl(const size_t& dim) : IModel_Impl(dim)
{
    m_modelType = PlainModel;
}
/***************************
*        DTOR()
***************************/
IModelPlain_Impl::~IModelPlain_Impl()
{}

void IModelPlain_Impl::setDimension(const size_t& dim)
{
    m_dimension = dim;
}
int IModelPlain_Impl::getDimension(size_t& dim) const
{
    dim = m_dimension;
    return _RC_SUCCESS_;
}

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPlain_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::Params1D;
}

/****************************************************************
* Polynomial IModel class implementation
*****************************************************************/
///static in IModelPolynomial!
/** ���������� ������ ����� ���� IParams */
int IModelPolynomial::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger)
{
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::Params1D, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse Polynomial Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** always return ONLY ONE IParams */
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}
///static in IModelPolynomial!
IModelPolynomial* IModelPolynomial::createEmptyModel(const size_t& dim)
{
    return IModelPolynomial_Impl::createEmptyModel(dim);
}

IModelPolynomial_Impl* IModelPolynomial_Impl::createEmptyModel(const size_t& dim)
{
    return new IModelPolynomial_Impl(dim);
}
void IModelPolynomial_Impl::setDimension(const size_t& dim)
{
    m_dimension = dim;
}
int IModelPolynomial_Impl::getDimension(size_t& dim) const
{
    dim = m_dimension;
    return _RC_SUCCESS_;
}

int IModelPolynomial_Impl::createEmptyParamsOfModel(IParams*& ptrToIParams) const
{
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, m_dimension);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToIParams = ptrIParams;
    return _RC_SUCCESS_;
}

bool IModelPolynomial_Impl::canCastModelParams(IParams * params) const
{
    return (params != NULL);
}

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPolynomial_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::Params1D;
}

IModelPolynomial_Impl::IModelPolynomial_Impl(const size_t& dim) : IModel_Impl(dim)
{
    m_modelType = PolynomialModel;
}

///static in IModelPolynomial
///��� ��� ��� ����� �� ���������� IUnivariateFunction
/*
int IModelPolynomial::evaluatePolynom(const double arg, const IParams*& coeffs, double& value)
{
    if(coeffs == NULL)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t dim = coeffs->getSize();
    if(dim == 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    value = 0;
    for (int i = dim-1; i >= 0; --i)
    {
        value *= arg;
        double coef=0;
        coeffs->getParam(i, coef);
        value += coef;
    }
    return _RC_SUCCESS_;
}
*/

/****************************************************************
*           Gaussian IModel class implementation
*****************************************************************/
///static in IModelGaussian!
/** ���������� ������ ����� ���� IParams */
int IModelGaussian::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger)
{
    IParams* params = IParams::parseAndCreateParams(IParams::ParamsTypes::GaussianParams, mapParams, postfix, logger);
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse GaussianParams");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** always return ONLY ONE IParams */
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}

int IModelGaussian_Impl::createEmptyParamsOfModel(IParams*& ptrToIParams) const
{
    /** for GaussianParams param 'size' DOES NOT matter!
    size = IParamsGaussian::GaussianCDFParamsDimension;
    */
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::GaussianParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToIParams = ptrIParams;
    return _RC_SUCCESS_;
}

bool IModelGaussian_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsGaussian *>(params) != NULL);
}

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelGaussian_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::GaussianParams;
}

/** to parse DataInfo and transform double** to IParamsGaussian */
int IModelGaussian_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const
{

    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsGaussian * ptrIParamsGaussian = dynamic_cast<IParamsGaussian *>(IParams::createEmptyParams(m_logger, IParams::ParamsTypes::GaussianParams));
    if(ptrIParamsGaussian == NULL)
    {
        delete ptrIParamsGaussian;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsGaussian->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsGaussian;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� GaussianParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsGaussian;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsGaussian �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsGaussian->setParam(j,tmpDbl);
        }
    }
    ptrToIParams = ptrIParamsGaussian;
    return _RC_SUCCESS_;
}

IModelGaussian_Impl::IModelGaussian_Impl() : IModel_Impl(IParamsGaussian::GaussianCDFParamsDimension)
{
    m_modelType = GaussianModel;
}

IModelGaussian_Impl* IModelGaussian_Impl::createEmptyModel()
{
    return new IModelGaussian_Impl();
}

int IModelGaussian_Impl::ptrAdditiveModelParamsToPtrParamsGaussian(int indexOfParams, const IParamsUnion* h, IParamsGaussian*& pg) const
{
    if (h == NULL)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in ptrAdditiveModelParamsToPtrParamsGaussian()");
        pg = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = h->getTotalOfItemsInParamsUnion();
    if ((int)size <= indexOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Gaussian model parameters not found in ptrAdditiveModelParamsToPtrParamsGaussian()");
        pg = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsGaussian!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsGaussian* hyp = dynamic_cast<IParamsGaussian*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Gaussian model parameters in ptrAdditiveModelParamsToPtrParamsGaussian()");
        pg = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
    pg = hyp;
    return _RC_SUCCESS_;
}
/*****************************************************************************************************
*                  Gaussian I_ProbabilisticModel Implementation
*****************************************************************************************************/
int IModelGaussian_Impl::CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t) const
{
    IParamsGaussian* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in CDF()");
        valueOfCDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfCDF = Distributions::Phi_CDF(x, hyp->mu(), hyp->sigma());
    return _RC_SUCCESS_;
}

int IModelGaussian_Impl::PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t) const
{
    IParamsGaussian* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in PDF()");
        valueOfPDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfPDF = Distributions::Phi_PDF(x, hyp->mu(), hyp->sigma());
    return _RC_SUCCESS_;
}

int IModelGaussian_Impl::getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const
{
    IParamsGaussian* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in getMaxMode()");
        valueOfMaxMode = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfMaxMode = hyp->mu();
    return _RC_SUCCESS_;
}

int IModelGaussian_Impl::generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t) const
{
    IParamsGaussian* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in generateRV()");
        randomValue = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    randomValue = Distributions::Normal(hyp->mu(), hyp->sigma());
    return _RC_SUCCESS_;
}

int IModelGaussian_Impl::generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const
{
    if((NULL == sampleValues) || (sampleSize == 0))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing params to put sample of Gaussian model in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsGaussian* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    double mu = hyp->mu();
    double sigma = hyp->sigma();
    for(size_t i=0; i < sampleSize; ++i)
    {
        sampleValues[i] = Distributions::Normal(mu, sigma);
    }
    if((NULL == pointEstimatesOfParams) && (NULL == intervalEstimatesOfParams))
    {
        return _RC_SUCCESS_;
    }
    double mean = 0.0f;
    for(size_t i=0; i < sampleSize; ++i)
    {
        mean += sampleValues[i];
    }
    mean /=sampleSize;
    double variance = 0.0f;
    if(sampleSize > 1)
    {
        for(size_t i=0; i < sampleSize; ++i)
        {
            variance += (sampleValues[i] - mean)*(sampleValues[i] - mean);
        }
        variance /= (sampleSize-1);
    }
    else
    {
        variance = NAN;
    }
    if(NULL != pointEstimatesOfParams)
    {
        hyp = NULL;
        rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, pointEstimatesOfParams, hyp);
        if((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing IParams of Gaussian model parameters to put sample's point estimates in generateSample()");
            return _ERROR_WRONG_ARGUMENT_;
        }
        hyp->setMean(mean);
        hyp->setSDeviation(sqrtf(variance));
    }
    return _RC_SUCCESS_;
/** TODO
    if(NULL != intervalEstimatesOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Interval estimates for Gaussian model sample NOT IMPLEMENTED yet in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
*/
}
///����� ���������� estimateHypothesis �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsGaussian!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelGaussian_Impl::estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
    /*OLD
        IParamsGaussian* hyp = NULL;
        int rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, h, hyp);
        if((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in estimateHypothesis()");
            return NAN;
        }
    */
    unsigned particlesNum = particles.size();

    std::vector<double> particleProbability(particlesNum, 0);
    double expectationForPointOrdinata = 0;
    double pointProbability = 0;

    unsigned i;

    for (i = 0; i < particlesNum; ++i)
    {
        /** ����� ����� ������������ ���������� ������ PDF */
///OLD        particleProbability[i] = Distributions::Phi_PDF(point - particles[i], hyp->mu(), hyp->sigma());
        int rc = PDF(point - particles[i], indexOfParams, h, particleProbability[i]);
        if(rc != _RC_SUCCESS_)
        {
            return NAN;
        }
        pointProbability += particleProbability[i];
    }

    if (pointProbability < DBL_MIN)
    {
        return -LDBL_MAX;
    }

    for (i = 0; i < particlesNum; ++i)
    {
        double weightOfParticle = particleProbability[i] / pointProbability;
        expectationForPointOrdinata += particles[i] * weightOfParticle;
    }
    /** ����� ����� ������������ ���������� ������ PDF */
//OLD    double estimate = log(Distributions::Phi_PDF(point - expectationalPoint, hyp->mu(), hyp->sigma()));
    double estimate = NAN;
    int rc = PDF(point - expectationForPointOrdinata, indexOfParams, h, estimate);
    if(rc != _RC_SUCCESS_)
    {
        return NAN;
    }
    return log(estimate);
}

///����� ���������� estimatePointMove �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsGaussian!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelGaussian_Impl::estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
    /* OLD
        IParamsGaussian* hyp = NULL;
        int rc = ptrAdditiveModelParamsToPtrParamsGaussian(indexOfParams, h, hyp);
        if((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in estimatePointMove()");
            return NAN;
        }
    */
    /** ����� ����� ������������ ���������� ������ PDF */
//OLD    return Distributions::Phi_PDF(move, hyp->mu(), hyp->sigma());
    double particleProbability = NAN;
    PDF(move, indexOfParams, h, particleProbability);
    return particleProbability;
}
///TODO!!!
int IModelGaussian_Impl::initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� initParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented initParticles() in Gaussian model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented initParticles() in Gaussian model");
    return _ERROR_WRONG_WORKFLOW_;
}
///TODO!!!
int IModelGaussian_Impl::generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� generateParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?}
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented generateParticles() in Gaussian model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented generateParticles() in Gaussian model");
    return _ERROR_WRONG_WORKFLOW_;
}
/**
* static method to estimate Gaussian's 0.999-quantile
* PARAMS [IN] sampleOfOrderedAbsItems -- abs values of gaussian r.v., NB ordered by ASC
* PARAMS [OUT]  quantile -- So far 0.999-quantile ONLY!
* Returns _RC_SUCCESS_ if success
         _ERROR_WRONG_INPUT_DATA_ otherwise
*/
int IModelGaussian::Quantil(const std::vector<double>& sampleOfOrderedAbsItems, double pValue, double& quantile)
{
    for(size_t i=0; i < sampleOfOrderedAbsItems.size()-1; ++i)
    {
        if(sampleOfOrderedAbsItems[i] > sampleOfOrderedAbsItems[i+1])
        {
            quantile = NAN;
            return _ERROR_WRONG_INPUT_DATA_;
        }
    }
    int n = sampleOfOrderedAbsItems.size();
    double mediana; // Quantile 0.674 of N(0,1) is 0.75, which is mediana of N+(0, 1)

    if (n % 2 == 0)
    {
        mediana = sampleOfOrderedAbsItems[n / 2];
    }
    else
    {
        mediana = (sampleOfOrderedAbsItems[n / 2] + sampleOfOrderedAbsItems[n / 2 +1]) / 2.;
    }
    // Evaluate 0.999-quantile
    // 0.999-quantile for N(0,1) is equal 3.090, which is 0.998-quantile for N+(0, 1)
    // 3.090 / 0.674 = 4.5845 ~ 5.;
    double denominator = 0.67449;
    double nominator  = M_SQRT2*erfinv(pValue);

    quantile = mediana * nominator/denominator;
    return _RC_SUCCESS_;
}
/****************************************************************
* Wiener IModel class implementation
*****************************************************************/
///static in IModelWiener!
/** ���������� ������ ����� ���� IParams */
int IModelWiener::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsWiener* params = IParamsWiener::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::WienerParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse ParamsWiener");
        return _ERROR_WRONG_ARGUMENT_;
    }
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}

int IModelWiener_Impl::createEmptyParamsOfModel(IParams*& ptrToIParams) const
{
    /** for WienerParams param 'size' DOES NOT matter!
     size = IParamsWiener::WienerCDFParamsDimension;
     */
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::WienerParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToIParams = ptrIParams;
    return _RC_SUCCESS_;
}

bool IModelWiener_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsWiener *>(params) != NULL);
}

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelWiener_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::WienerParams;
}
/** to parse DataInfo and transform double** to IParamsGaussian */
int IModelWiener_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const
{
    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsWiener * ptrIParamsWiener = dynamic_cast<IParamsWiener *>(IParams::createEmptyParams(m_logger, IParams::ParamsTypes::WienerParams));
    if(ptrIParamsWiener == NULL)
    {
        delete ptrIParamsWiener;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsWiener->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsWiener;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� WienerParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsWiener;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsWiener �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsWiener->setParam(j,tmpDbl);
        }
    }
    ptrToIParams = ptrIParamsWiener;
    return _RC_SUCCESS_;
}


IModelWiener_Impl::IModelWiener_Impl() : IModel_Impl(IParamsWiener::WienerCDFParamsDimension)
{
    m_modelType = WienerModel;
}

IModelWiener_Impl* IModelWiener_Impl::createEmptyModel()
{
    return new IModelWiener_Impl();
}

int IModelWiener_Impl::ptrAdditiveModelParamsToPtrParamsWiener(int indexOfParams, const IParamsUnion* h, IParamsWiener*& pw) const
{
    if (h == NULL)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in ptrAdditiveModelParamsToPtrParamsWiener()");
        pw = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = h->getTotalOfItemsInParamsUnion();
    if ((int)size <= indexOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Wiener model parameters not found in ptrAdditiveModelParamsToPtrParamsWiener()");
        pw = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsWiener!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsWiener* hyp = dynamic_cast<IParamsWiener*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Wiener model parameters in ptrAdditiveModelParamsToPtrParamsWiener()");
        pw = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
    pw = hyp;
    return _RC_SUCCESS_;
}
/*****************************************************************************************************
*                  Wiener I_ProbabilisticModel Implementation
*****************************************************************************************************/
int IModelWiener_Impl::CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t) const
{
    IParamsWiener* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in CDF()");
        valueOfCDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfCDF = Distributions::Phi_CDF(x - hyp->initialCondition(), hyp->mu(), hyp->sigma() * sqrt(((double)t < FLT_MIN) ? FLT_MIN : (double)t) );
    return _RC_SUCCESS_;
}

int IModelWiener_Impl::PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t) const
{
    IParamsWiener* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in PDF()");
        valueOfPDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfPDF = Distributions::Phi_PDF(x - hyp->initialCondition(), hyp->mu(), hyp->sigma() * sqrt(((double)t < FLT_MIN) ? FLT_MIN : (double)t) );
    return _RC_SUCCESS_;
}

int IModelWiener_Impl::getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const
{
    IParamsWiener* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in getMaxMode()");
        valueOfMaxMode = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfMaxMode = hyp->initialCondition() + hyp->mu();
    return _RC_SUCCESS_;
}

int IModelWiener_Impl::generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t) const
{
    IParamsWiener* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in getMaxMode()");
        randomValue = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    randomValue = hyp->initialCondition() + Distributions::Normal(hyp->mu(), hyp->sigma() * sqrt(((double)t < FLT_MIN) ? FLT_MIN : (double)t) );
    return _RC_SUCCESS_;
}

int IModelWiener_Impl::generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const
{
    if((NULL == sampleValues) || (sampleSize == 0))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing params to put sample of Wiener model in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsWiener* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }


    double ic = hyp->initialCondition();
    if(isNAN_double(ic))
    {
        ic=0.0f;
    }
    double mu = hyp->mu();
    double sigma = hyp->sigma();
    sampleValues[0] = ic;
    for(size_t i=1; i < sampleSize; ++i)
    {
        sampleValues[i] = sampleValues[i-1] + Distributions::Normal(mu, sigma); //BAD Distributions::Normal(mu, sigma * sqrt((double)i) );
    }
    if((NULL == pointEstimatesOfParams) && (NULL == intervalEstimatesOfParams))
    {
        return _RC_SUCCESS_;
    }
    double mean = 0.0f;
    for(size_t i=1; i < sampleSize; ++i)
    {
        mean += sampleValues[i]-sampleValues[i-1];
    }
    mean /= (sampleSize-1);
    double variance = 0.0f;
    if(sampleSize > 2)
    {
        for(size_t i=1; i < sampleSize; ++i)
        {
            variance += ((sampleValues[i] - sampleValues[i-1]) - mean)*((sampleValues[i] - sampleValues[i-1]) - mean);
        }
        variance /= (sampleSize-2);
    }
    else
    {
        variance = NAN;
    }
    if(NULL != pointEstimatesOfParams)
    {
        hyp = NULL;
        rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, pointEstimatesOfParams, hyp);
        if((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing IParams of Gaussian model parameters to put sample's point estimates in generateSample()");
            return _ERROR_WRONG_ARGUMENT_;
        }
        hyp->setInitialCondition(ic);
        hyp->setMean(mean);
        hyp->setSDeviation(sqrtf(variance));
    }
    return _RC_SUCCESS_;
/** TODO
    if(NULL != intervalEstimatesOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Interval estimates for Wiener model sample NOT IMPLEMENTED yet in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
*/
}

///����� ���������� estimateHypothesis �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsWiener!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelWiener_Impl::estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
    /* OLD
        IParamsWiener* hyp = NULL;
        int rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, h, hyp);
        if ((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in estimateHypothesis()");
            return NAN;
        }
    */
    unsigned particlesNum = particles.size();

    std::vector<double> particleProbability(particlesNum, 0);
    double expectationForPointOrdinata = 0;
    double pointProbability = 0;

    unsigned i;

    for (i = 0; i < particlesNum; ++i)
    {
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
/// ???? ����� ���������� ������� �����? ����� ��� ������� ������� ����������� ����?
//OLD        particleProbability[i] = Distributions::Phi_PDF(point - particles[i] - hyp->initialCondition(), hyp->mu(), hyp->sigma() * sqrt((double)t + 1.));
        int rc = PDF(point - particles[i], indexOfParams, h, particleProbability[i], (double)t );
        if(rc != _RC_SUCCESS_ )
        {
            return NAN;
        }
        pointProbability += particleProbability[i];
    }

    if (pointProbability < DBL_MIN)
    {
        return -LDBL_MAX;
    }

    for (i = 0; i < particlesNum; ++i)
    {
        double weightOfParticle = particleProbability[i] / pointProbability;
        expectationForPointOrdinata += weightOfParticle * particles[i];
    }
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
//OLD    double estimate = log(Distributions::Phi_PDF(point - expectationalPoint  - hyp->initialCondition(), hyp->mu(), hyp->sigma() * sqrt((double)t + 1.)));
//OLD    return estimate;
    double estimate = NAN;
    int rc = PDF(point - expectationForPointOrdinata, indexOfParams, h, estimate, (double)t );
    if(rc != _RC_SUCCESS_)
    {
        return NAN;
    }
    return log(estimate);
}

///����� ���������� estimatePointMove �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsWiener!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelWiener_Impl::estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
    /* OLD
        IParamsWiener* hyp = NULL;
        int rc = ptrAdditiveModelParamsToPtrParamsWiener(indexOfParams, h, hyp);
        if ((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in estimatePointMove()");
            return NAN;
        }
    */
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
//OLD    return Distributions::Phi_PDF(move - hyp->initialCondition(), hyp->mu(), hyp->sigma() * sqrt((double)t + 1.));
    double particleProbability = NAN;
    PDF(move, indexOfParams, h, particleProbability, (double)t );
    return particleProbability;
}
///TODO!!!
int IModelWiener_Impl::initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� initParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented initParticles() in Wiener model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented initParticles() in Wiener model");
    return _ERROR_WRONG_WORKFLOW_;
}
///TODO!!!
int IModelWiener_Impl::generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� generateParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?}
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented generateParticles() in Wiener model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented generateParticles() in Wiener model");
    return _ERROR_WRONG_WORKFLOW_;
}

/*****************************************************************
*   Pareto IModel class implementation
******************************************************************/
///static in IModelPareto!
/** ���������� ������ ����� ���� IParams */
int IModelPareto::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPareto* params = IParamsPareto::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::ParetoParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse ParamsPareto");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** always return ONLY ONE IParams */
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}

int IModelPareto_Impl::createEmptyParamsOfModel(IParams*& ptrToIParams) const
{
    /** for ParetoParams param 'size' DOES NOT matter!
      size = IParamsPareto::ParetoCDFParamsDimension;
    */
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::ParetoParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToIParams = ptrIParams;
    return _RC_SUCCESS_;
}

bool IModelPareto_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsPareto *>(params) != NULL);
}

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPareto_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::ParetoParams;
}

/** to parse DataInfo and transform double** to IParamsPareto */
int IModelPareto_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const
{
    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPareto * ptrIParamsPareto = dynamic_cast<IParamsPareto *>(IParams::createEmptyParams(m_logger, IParams::ParamsTypes::ParetoParams));
    if(ptrIParamsPareto == NULL)
    {
        delete ptrIParamsPareto;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsPareto->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsPareto;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� ParetoParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsPareto;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsPareto �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsPareto->setParam(j,tmpDbl);
        }
    }
    ptrToIParams = ptrIParamsPareto;
    return _RC_SUCCESS_;
}

IModelPareto_Impl::IModelPareto_Impl() :  IModel_Impl(IParamsPareto::ParetoCDFParamsDimension)
{
    m_modelType = ParetoModel;
}
IModelPareto_Impl* IModelPareto_Impl::createEmptyModel()
{
    return new IModelPareto_Impl();
}

int IModelPareto_Impl::ptrAdditiveModelParamsToPtrParamsPareto(int indexOfParams, const IParamsUnion* h, IParamsPareto*& pp) const
{
    if (h == NULL)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in ptrAdditiveModelParamsToPtrParamsPareto()");
        pp = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = h->getTotalOfItemsInParamsUnion();
    if ((int)size <= indexOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Pareto model parameters not found in ptrAdditiveModelParamsToPtrParamsPareto()");
        pp = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsPareto!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsPareto* hyp = dynamic_cast<IParamsPareto*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Pareto model parameters in ptrAdditiveModelParamsToPtrParamsPareto()");
        pp = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
    pp = hyp;
    return _RC_SUCCESS_;
}
/*****************************************************************************************************
*                  Pareto I_ProbabilisticModel Implementation
*****************************************************************************************************/
int IModelPareto_Impl::CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t) const
{
    IParamsPareto* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in CDF()");
        valueOfCDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfCDF = Distributions::Pareto_CDF(x, hyp->xM(), hyp->k());
    return _RC_SUCCESS_;
}

int IModelPareto_Impl::PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t) const
{
    IParamsPareto* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in PDF()");
        valueOfPDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfPDF = Distributions::Pareto_PDF(x, hyp->xM(), hyp->k() );
    return _RC_SUCCESS_;
}

int IModelPareto_Impl::getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const
{
    IParamsPareto* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in getMaxMode()");
        valueOfMaxMode = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfMaxMode = hyp->xM();
    return _RC_SUCCESS_;
}

int IModelPareto_Impl::generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t) const
{
    IParamsPareto* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in generateRV()");
        randomValue = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    randomValue = Distributions::Pareto(hyp->xM(), hyp->k());
    return _RC_SUCCESS_;
}

int IModelPareto_Impl::generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const
{
    if((NULL == sampleValues) || (sampleSize == 0))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing params to put sample of Pareto model in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPareto* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    double Xm = hyp->xM();
    double k = hyp->k();
    for(size_t i=0; i < sampleSize; ++i)
    {
        sampleValues[i] = Distributions::Pareto(Xm, k);
    }
    if((NULL == pointEstimatesOfParams) && (NULL == intervalEstimatesOfParams))
    {
        return _RC_SUCCESS_;
    }
    /** Xmin  MLE estimate  */
    double Xmin = sampleValues[0];
    for(size_t i=1; i < sampleSize; ++i)
    {
        if(Xmin > sampleValues[i])
            Xmin = sampleValues[i];
    }
    /** Xmin  MLE estimate  */
    k = 0.0f;
    for(size_t i=0; i < sampleSize; ++i)
    {
            k += (log(sampleValues[i]) - log(Xmin));
    }
    k = sampleSize / k;

    if(NULL != pointEstimatesOfParams)
    {
        hyp = NULL;
        rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, pointEstimatesOfParams, hyp);
        if((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing IParams of Pareto model parameters to put sample's point estimates in generateSample()");
            return _ERROR_WRONG_ARGUMENT_;
        }
        hyp->setParetoXm(Xmin);
        hyp->setParetoK(k);
    }
    return _RC_SUCCESS_;
/** TODO
    if(NULL != intervalEstimatesOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Interval estimates for Gaussian model sample NOT IMPLEMENTED yet in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
*/
}

/// ����� ���������� initParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsPareto!
 � ��� ������������� ����� ��������� � ���� ����!
*/
int IModelPareto_Impl::initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    /* OLD
        IParamsPareto* hyp = NULL;
        int rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, h, hyp);
        if ((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in initParticles()");
            return _ERROR_WRONG_ARGUMENT_;
        }
    */
    unsigned particlesNum = particles.size();
    int rc = _RC_SUCCESS_;
    for (unsigned i = 0; i < particlesNum; ++i)
    {
        /** ����� ������������ ���������� ������ generateRV */
//OLD        particles[i] = Distributions::Pareto(hyp->xM(), hyp->k());
        rc = generateRV(indexOfParams, h, particles[i]);
        if(rc != _RC_SUCCESS_)
        {
            break;
        }
    }
    return rc;
}

/// ����� ���������� generateParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
int IModelPareto_Impl::generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    /* OLD
        IParamsPareto* hyp = NULL;
        int rc = ptrAdditiveModelParamsToPtrParamsPareto(indexOfParams, h, hyp);
        if ((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in generateParticles()");
            return _ERROR_WRONG_ARGUMENT_;
        }
    */
    unsigned particlesNum = particles.size();
    int rc = _RC_SUCCESS_;
    for (unsigned i = 0; i < particlesNum; ++i)
    {
        /** ����� ������������ ���������� ������ generateRV */
//OLD        particles[i] += Distributions::Pareto(hyp->xM(), hyp->k());
        rc = generateRV(indexOfParams, h, particles[i]);
        if(rc != _RC_SUCCESS_)
        {
            break;
        }
    }
    return _RC_SUCCESS_;
}
///TODO!!!
double IModelPareto_Impl::estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimateHypothesis �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimateHypothesis() in Pareto model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimateHypothesis() in Pareto model");
    return NAN;
}
///TODO!!!
double IModelPareto_Impl::estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimatePointMove �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimatePointMove() in Pareto model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimatePointMove() in Pareto model");
    return NAN;
}
/*****************************************************************
*   Poisson IModel class implementation
******************************************************************/
///static in IModelPoisson!
/** ���������� ������ ����� ���� IParams */
int IModelPoisson::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPoisson* params = IParamsPoisson::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::PoissonParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse ParamsPoisson");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** always return ONLY ONE IParams */
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}

int IModelPoisson_Impl::createEmptyParamsOfModel(IParams*& ptrToIParams) const
{
    /** for PoissonParams param 'size' DOES NOT matter!
      size = IParamsPoisson::PoissonCDFParamsDimension;
    */
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::PoissonParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToIParams = ptrIParams;
    return _RC_SUCCESS_;
}

bool IModelPoisson_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsPoisson *>(params) != NULL);
}

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPoisson_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::PoissonParams;
}

/** to parse DataInfo and transform double** to IParamsPoisson */
int IModelPoisson_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const
{
    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPoisson * ptrIParamsPoisson = dynamic_cast<IParamsPoisson *>(IParams::createEmptyParams(m_logger, IParams::ParamsTypes::PoissonParams));
    if(ptrIParamsPoisson == NULL)
    {
        delete ptrIParamsPoisson;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsPoisson->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsPoisson;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� PoissonParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsPoisson;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsPoisson �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsPoisson->setParam(j,tmpDbl);
        }
    }
    ptrToIParams = ptrIParamsPoisson;
    return _RC_SUCCESS_;
}

IModelPoisson_Impl::IModelPoisson_Impl() :  IModel_Impl(IParamsPoisson::PoissonCDFParamsDimension)
{
    m_modelType = PoissonModel;
}
IModelPoisson_Impl* IModelPoisson_Impl::createEmptyModel()
{
    return new IModelPoisson_Impl();
}

int IModelPoisson_Impl::ptrAdditiveModelParamsToPtrParamsPoisson(int indexOfParams, const IParamsUnion* h, IParamsPoisson*& ps) const
{
    if (h == NULL)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Poisson model parameters in ptrAdditiveModelParamsToPtrParamsPoisson()");
        ps = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = h->getTotalOfItemsInParamsUnion();
    if ((int)size <= indexOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Poisson model parameters not found in ptrAdditiveModelParamsToPtrParamsPoisson()");
        ps = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsPoisson!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsPoisson* hyp = dynamic_cast<IParamsPoisson*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Poisson model parameters in ptrAdditiveModelParamsToPtrParamsPoisson()");
        ps = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
    ps = hyp;
    return _RC_SUCCESS_;
}
/*****************************************************************************************************
*                  Poisson I_ProbabilisticModel Implementation
*****************************************************************************************************/
int IModelPoisson_Impl::CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t) const
{
    IParamsPoisson* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoisson(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Poisson model parameters in CDF()");
        valueOfCDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfCDF = Distributions::Poisson_CDF((size_t)x, hyp->lambda());
    return _RC_SUCCESS_;
}

int IModelPoisson_Impl::PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t) const
{
    IParamsPoisson* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoisson(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Poisson model parameters in PDF()");
        valueOfPDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    valueOfPDF = Distributions::Poisson_PDF((size_t)x, hyp->lambda());
    return _RC_SUCCESS_;
}

int IModelPoisson_Impl::getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const
{
    IParamsPoisson* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoisson(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Poisson model parameters in getMaxMode()");
        valueOfMaxMode = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(hyp->lambda() < 1)
    {
        valueOfMaxMode = 0.0f;
    }
    else
    {
        valueOfMaxMode = floor(hyp->lambda());
    }
    return _RC_SUCCESS_;
}

int IModelPoisson_Impl::generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t) const
{
    IParamsPoisson* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoisson(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Poisson model parameters in getMaxMode()");
        randomValue = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    randomValue = Distributions::Poisson(hyp->lambda());
    return _RC_SUCCESS_;
}

int IModelPoisson_Impl::generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const
{
    if((NULL == sampleValues) || (sampleSize == 0))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing params to put sample of Poisson model in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPoisson* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoisson(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Poisson model parameters in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    double lambda = hyp->lambda();
    for(size_t i=0; i < sampleSize; ++i)
    {
        sampleValues[i] = Distributions::Poisson(lambda);
    }
    if((NULL == pointEstimatesOfParams) && (NULL == intervalEstimatesOfParams))
    {
        return _RC_SUCCESS_;
    }
    lambda = 0.0f;
    for(size_t i=0; i < sampleSize; ++i)
    {
        lambda += sampleValues[i];
    }
    lambda /=sampleSize;

    if(NULL != pointEstimatesOfParams)
    {
        hyp = NULL;
        rc = ptrAdditiveModelParamsToPtrParamsPoisson(indexOfParams, pointEstimatesOfParams, hyp);
        if((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing IParams of Poisson model parameters to put sample's point estimates in generateSample()");
            return _ERROR_WRONG_ARGUMENT_;
        }
        hyp->setPoissonLambda(lambda);
    }
    return _RC_SUCCESS_;
/** TODO
    if(NULL != intervalEstimatesOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Interval estimates for Gaussian model sample NOT IMPLEMENTED yet in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
*/
}
/*****************************************************************
*   Poisson-Pareto IModel class implementation
******************************************************************/
//OLD constexpr const char * IModelPoissonDrvnParetoJumpsExpMeanReverting::strExpMeanRevertingLambda;
///static in IModelPoissonDrvnParetoJumpsExpMeanReverting!
/** ���������� ������ ����� ���� IParams */
int IModelPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPoissonDrvnParetoJumpsExpMeanReverting* params = IParamsPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to par+se ParamsPoissonDrvnParetoJumpsExpMeanReverting");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** This is size of ptrs (ptrToPtrIParams) which have been initialized
    always return ONLY ONE IParams
    */
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}
/**
* PARAMS [OUT] IParams*& ptrToIParams -- array of ptrs to IPoissonDrvnParetoJumpsExpMeanRevertingParams
*/
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyParamsOfModel(IParams*& ptrToIParams) const
{
    /** for PoissonParams */
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToIParams = ptrIParams;
    return _RC_SUCCESS_;
}

bool IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting *>(params) != NULL);
}

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams;
}

/** to parse DataInfo
and transform double** to IParamsPoissonDrvnParetoJumpsExpMeanReverting
*/
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const
{
///TODO!!! Wrong! MAGIC const!
    if((totalOfDataInfo != 3) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPareto * ptrIParamsPareto = dynamic_cast<IParamsPareto *>(IParams::createEmptyParams(m_logger, IParams::ParamsTypes::ParetoParams));
    if(ptrIParamsPareto  == NULL)
    {
        delete ptrIParamsPareto;
        return _ERROR_NO_ROOM_;
    }
    /** ������ ����� ������ ���� DI, ������� �������� ������
    */
    int index = findModelNameInDataInfoArray(totalOfDataInfo, ptrToPtrDI, IModel::modelNames[ParetoModel]);
    if(index < 0 )
    {
        delete ptrIParamsPareto;
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    size_t dimOfDI= ptrToPtrDI[index]->m_dimension;
    if(dimOfDI != 1)
    {
        delete ptrIParamsPareto;
        return _ERROR_WRONG_ARGUMENT_;
    }
    int rc = ptrIParamsPareto->setParams( static_cast<size_t> (ptrToPtrDI[index]->m_dataSetSizes[0]), const_cast<const double *> (ptrToPtrDI[index]->m_dataSet[0]) );
    if(rc != _RC_SUCCESS_)
    {
        delete ptrIParamsPareto;
        return rc;
    }

    IParamsPoisson * ptrIParamsPoisson = dynamic_cast<IParamsPoisson *> (IParams::createEmptyParams(m_logger, IParams::ParamsTypes::PoissonParams));
    if(ptrIParamsPoisson  == NULL)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_NO_ROOM_;
    }
    /** ����� ������ ���� DI, ������� �������� ��������
    */
    index = findModelNameInDataInfoArray(totalOfDataInfo, ptrToPtrDI, IModel::modelNames[PoissonModel]);
    if(index < 0 )
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    dimOfDI= ptrToPtrDI[index]->m_dimension;
    if(dimOfDI != 1)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_WRONG_ARGUMENT_;
    }
    rc = ptrIParamsPoisson->setParams(static_cast<size_t>(ptrToPtrDI[index]->m_dataSetSizes[0]), const_cast<const double *>(ptrToPtrDI[index]->m_dataSet[0]));
    if(rc != _RC_SUCCESS_)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return rc;
    }

    IParams* ptrIExpMeanRevertingParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, 1);
    if(ptrIExpMeanRevertingParams  == NULL)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_NO_ROOM_;
    }
///TODO!!! �������� ���� ������� � ����������� ���� ������! ��� ����� �������
///TODO!!! ����� ������ ���� DI, ������� �������� MeanRevert
///TODO!!!                index = findModelNameInDataInfoArray(totalOfDataInfo, ptrToPtrDI, IModelPoissonDrvnParetoJumpsExpMeanReverting::strExpMeanRevertingLambda);
    /** ���� ����� ��� ����� ������� : �����������, ��� �������� ExpMeanRevertingLambda
    ������ � ����� ��������� DataInfo, ����������� ������������� ������� ������ � ������� ����� 1
     */
    index = -1;
    for(size_t i=0; i < totalOfDataInfo; ++i)
    {
        if(1 == ptrToPtrDI[i]->m_dataSetSizes[0])
            index = i;
    }
    if(index < 0 )
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    dimOfDI= ptrToPtrDI[index]->m_dimension;
    if(dimOfDI != 1)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return _ERROR_WRONG_ARGUMENT_;
    }
    rc = ptrIExpMeanRevertingParams->setParams(static_cast<size_t> (ptrToPtrDI[index]->m_dataSetSizes[0]), const_cast<const double *>(ptrToPtrDI[index]->m_dataSet[0]));
    if(rc != _RC_SUCCESS_)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return rc;
    }
    ptrToIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams);
    if(ptrToIParams == NULL)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return _ERROR_NO_ROOM_;
    }
    double tmpDbl=NAN;
    tmpDbl = ptrIParamsPareto->k();
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToIParams)->setParetoK(tmpDbl);
    tmpDbl=NAN;
    tmpDbl = ptrIParamsPareto->xM();
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToIParams)->setParetoXm(tmpDbl);
    delete ptrIParamsPareto;
    tmpDbl=NAN;
    tmpDbl = ptrIParamsPoisson->lambda();
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToIParams)->setPoissonLambda(tmpDbl);
    delete ptrIParamsPoisson;
    tmpDbl=NAN;
    ptrIExpMeanRevertingParams->getParam(0, tmpDbl);
    tmpDbl = (tmpDbl < -DBL_MIN) ? tmpDbl : -DBL_MIN;
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToIParams)->setMRLambda(tmpDbl);
    delete ptrIExpMeanRevertingParams;

    return _RC_SUCCESS_;
}

IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl() :  IModel_Impl(IParamsPoissonDrvnParetoJumpsExpMeanReverting::PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension)
{
    m_modelType = PoissonDrvnParetoJumpsExpMeanRevertingModel;
}

IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl* IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyModel()
{
    return new IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl();
}

int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(int indexOfParams, const IParamsUnion* h, IParamsPoissonDrvnParetoJumpsExpMeanReverting*& ppp) const
{
    if (h == NULL)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting()");
        ppp = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = h->getTotalOfItemsInParamsUnion();
    if ((int)size <= indexOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! PoissonDrvnParetoJumpsExpMeanReverting model parameters not found in ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting()");
        ppp = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsPoissonDrvnParetoJumpsExpMeanReverting!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting()");
        pp = NULL;
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
    ppp = hyp;
    return _RC_SUCCESS_;
}
/*****************************************************************************************************
*  PoissonDrvnParetoJumpsExpMeanReverting I_ProbabilisticModel Implementation
*****************************************************************************************************/
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t) const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in CDF()");
        valueOfCDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
///TODO!!! Implement in Distributions!    valueOfCDF = Distributions::PoissonDrivenParetoJumpsExpMeanRevert_CDF(x,...);
    return _ERROR_WRONG_WORKFLOW_;
}

int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t) const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in PDF()");
        valueOfPDF = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
///TODO!!!  Implement in Distributions!   valueOfPDF = Distributions::PoissonDrivenParetoJumpsExpMeanRevert_PDF(x,...);
    return _ERROR_WRONG_WORKFLOW_;
}

int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in getMaxMode()");
        valueOfMaxMode = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
///TODO!!!  Implement in Distributions!  valueOfMaxMode = ...;
    return _ERROR_WRONG_WORKFLOW_;
}

int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t) const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateRV()");
        randomValue = NAN;
        return _ERROR_WRONG_ARGUMENT_;
    }
    /**
    ���� ����� ���������� "������" �.�., �.�. � �����,
    ��� ��� ������ ���������� �������� ��� "��������" �� ��������
    */
    if(t == 0)
    {
        rc = generateFirstRV(hyp, randomValue);
    }
    /**
    ���� ����� ������ ��������� ��������� t,
    ������������� ��� (t) ��� ���������� ��������, ��������� �� 1-�� �������
    */
    else
    {
        rc = generateNextRV(hyp, randomValue, t);
    }
    return rc;
}

/** I_Probabilistic_Model interface PRIVATE implementation */
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::generateFirstRV(const IParamsPoissonDrvnParetoJumpsExpMeanReverting* const hyp, double& randomValue) const
{
    /** ����� � ������� ����������������� ������������� ���������� ��������,
    ����� �������������� ���� ����� ������ ��� ���������� �� ��������, ������
    �.�. �� ����������������� �������������, ������������� � ��������������, ������������ �������
    */
    /** ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC() */
    randomValue = Distributions::Pareto(hyp->xM(), hyp->k()) * exp(hyp->MR_lambda() * Distributions::Exponential(hyp->lambda()));
    return _RC_SUCCESS_;
}
/** PARAM [IN|OUT] double& randomValue = previous ordinata as IN, next ordinata as OUT
*/
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::generateNextRV(const IParamsPoissonDrvnParetoJumpsExpMeanReverting* const hyp, double& randomValue, unsigned dt) const
{
    // Time of spike is (t - poisson_tail), if poisson_tail < 1
    /** by default dt=1 */
    double poisson_tail = dt - Distributions::Exponential(hyp->lambda());
    /** ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC() */
    randomValue *= exp(hyp->MR_lambda());
    // Heviside(poisson_tail) to avoid inf trouble
    if (poisson_tail >= 0)
    {
        /** ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC() */
        randomValue += Distributions::Pareto(hyp->xM(), hyp->k()) * exp(hyp->MR_lambda() * poisson_tail);
    }
    return _RC_SUCCESS_;
}///end generateNextRV()

int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const
{
    if((NULL == sampleValues) || (sampleSize == 0))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing params to put sample of PoissonDrvnParetoJumpsExpMeanReverting model in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = NULL;

    int rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, h, hyp);
    if((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }

    double lambda = hyp->lambda();
    double firstJump = Distributions::Exponential(lambda);
    double nextJump  = Distributions::Exponential(lambda);
    while(firstJump - nextJump > 0)
    {
        firstJump = nextJump;
        nextJump  = Distributions::Exponential(lambda);
    }
    /** first generate Poisson driven jump as
    jumps times,
    Since we ignore the Poisson's random value itself
    and take only the time it came
    we use Exponential distribution to
    generate the sequence of such times.
    */
    /** the jump time/index right before the start
    of sample to be generated
    */
    firstJump *= -1;
    nextJump -= fabs(firstJump);
    std::vector<double> jumpsTime;
    jumpsTime.push_back(firstJump);

    double currentJumpTime= nextJump;
    while(currentJumpTime < sampleSize)
    {
        jumpsTime.push_back(currentJumpTime);
        nextJump  = Distributions::Exponential(lambda);
        currentJumpTime += nextJump;
    }
    /** second we generate Pareto rv as many
    as Poisson jumps
    */
    double Xmin = hyp->xM();
    double k = hyp->k();
    double MR_lambda = hyp->MR_lambda();

    std::vector<double> amplitudes;
    for(size_t i=0; i < jumpsTime.size(); ++i)
    {
        double randomValue = Distributions::Pareto(Xmin, k);
        amplitudes.push_back(randomValue);
    }
#ifdef _DEBUG_
std::cout << "jumps:" << jumpsTime.size() << std::endl;
    for(size_t i=0; i < jumpsTime.size(); ++i)
    {
       std::cout << i << ":" << jumpsTime[i] << std::endl;
    }
#endif
    /** finaly cartesian product of Poisson jumps
    and Pareto DF sample
    */
    sampleValues[0] = amplitudes[0] * exp(MR_lambda * fabs(jumpsTime[0]));

    for(size_t i = 1, j = 1, jumpsSize = jumpsTime.size(); (i < sampleSize) && (j < jumpsSize); ++i)
    {
        sampleValues[i] = sampleValues[i-1] * exp(MR_lambda);
        if(( (size_t)floorf(jumpsTime[j]) == (i-1)) && ( (size_t)ceilf(jumpsTime[j]) == i))
        {
            sampleValues[i] += amplitudes[j] * exp(MR_lambda * ((double)i - jumpsTime[j]));
            ++j;
        }
    }

    if((NULL == pointEstimatesOfParams) && (NULL == intervalEstimatesOfParams))
    {
        return _RC_SUCCESS_;
    }
    /**
    get MM-estimates of model parameters for samples we have generated
    */
    double meanOfLambda = 0.0f;
    for(size_t i=1; i < jumpsTime.size(); ++i)
    {
        meanOfLambda += jumpsTime[i] - jumpsTime[i-1];
    }
    meanOfLambda /= (jumpsTime.size()-1);
    meanOfLambda =  1.0f / meanOfLambda;
    /** Xmin  MLE estimate  */
    Xmin = amplitudes[0];
    for(size_t i=1; i < amplitudes.size(); ++i)
    {
        if(Xmin > amplitudes[i])
            Xmin = amplitudes[i];
    }
    /** k  MLE-estimate  */
    k = 0.0f;
    for(size_t i=0; i < amplitudes.size(); ++i)
    {
            k += (log(amplitudes[i]) - log(Xmin));
    }
    k = amplitudes.size() / k;

    if(NULL != pointEstimatesOfParams)
    {
        hyp = NULL;
        rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, pointEstimatesOfParams, hyp);
        if((rc != _RC_SUCCESS_) || (hyp == NULL))
        {
            m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing IParams of PoissonDrvnParetoJumpsExpMeanReverting model parameters to put sample's point estimates in generateSample()");
            return _ERROR_WRONG_ARGUMENT_;
        }
        hyp->setParetoXm(Xmin);
        hyp->setParetoK(k);
        hyp->setPoissonLambda(meanOfLambda);
        hyp->setMRLambda(MR_lambda);
    }
    return _RC_SUCCESS_;
/** TODO
    if(NULL != intervalEstimatesOfParams)
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Interval estimates for Gaussian model sample NOT IMPLEMENTED yet in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
*/
}///end generateSample()

/// ����� ���������� initParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }

    unsigned particlesNum = particles.size();

    for (unsigned i = 0; i < particlesNum; ++i)
    {
///TODO!!! ������ ��������� ������� generateRV()
        /** ����� � ������� ����������������� ������������� ���������� ��������,
        ����� �������������� ���� ����� ������ ��� ���������� �� ��������, ������
        �.�. �� ����������������� �������������, ������������� � ��������������, ������������ �������
        */
        /** ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC() */
//OLD        particles[i] = Distributions::Pareto(hyp->xM(), hyp->k()) * exp(hyp->lamC() * Distributions::Exponential(hyp->lam()));
        generateFirstRV(hyp, particles[i]);
    }
    return _RC_SUCCESS_;
}

/// ����� ���������� generateParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = NULL;
    int rc = ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(indexOfParams, h, hyp);
    if ((rc != _RC_SUCCESS_) || (hyp == NULL))
    {
        m_logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }

//OLD    double poisson_tail;
    unsigned particlesNum = particles.size();

    for (unsigned i = 0; i < particlesNum; ++i)
    {
        /* OLD
         Time of spike is (t - poisson_tail), if poisson_tail < 1
        poisson_tail = 1 - Distributions::Exponential(hyp->lam());
        / ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC()
        /
        particles[i] *= exp(hyp->lamC());
        // Heviside(poisson_tail) to avoid inf trouble
        if (poisson_tail >= 0)
        {
            / ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC()
            /
            particles[i] += Distributions::Pareto(hyp->xM(), hyp->k()) * exp(hyp->lamC() * poisson_tail);
        }
        */
        /** by default 3 parameter dt=1 <== time-step is 1 */
        generateNextRV(hyp, particles[i]);
    }
    return _RC_SUCCESS_;
}
///TOOD!!!
double IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimateHypothesis �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimateHypothesis() in PoissonDrvnParetoJumpsExpMeanReverting model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimateHypothesis() in PoissonDrvnParetoJumpsExpMeanReverting model");
    return NAN;
}
///TODO!!!
double IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimatePointMove �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    m_logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimatePointMove() in PoissonDrvnParetoJumpsExpMeanReverting model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimatePointMove() in PoissonDrvnParetoJumpsExpMeanReverting model");
    return NAN;
}

/******************************************************************
* � I_PF_Model ������� ������ ������� �������������� ������������� �� �����? ����� ��?
*******************************************************************/
/** Factory methods */
//OLD IModel* IModel::createModel(enum IModel::ModelsTypes modelType, size_t dim)
IModel* IModel::createModel(const enum IModel::ModelsTypes modelType, const IParams* ptrModelParams)
{
    switch (modelType)
    {
    case  PlainModel:
    {
        /** we may create plain model of dimension = 0  */
        size_t dim = 0;
        if(ptrModelParams)
        {
            dim = ptrModelParams->getSize();
        }
        return new IModelPlain_Impl(dim);
    }
    case  GaussianModel:
        return IModelGaussian_Impl::createEmptyModel();
    case WienerModel:
        return IModelWiener_Impl::createEmptyModel();
    case PoissonModel:
        return IModelPoisson_Impl::createEmptyModel();
    case ParetoModel:
        return IModelPareto_Impl::createEmptyModel();
    case PoissonDrvnParetoJumpsExpMeanRevertingModel:
        return IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyModel();
    case PolynomialModel:
    {
        /** we may create plain model of dimension = 0  */
        size_t dim = 0;
        if(ptrModelParams)
        {
            dim = ptrModelParams->getSize();
        }
        return IModelPolynomial_Impl::createEmptyModel(dim);
    }
    default:
        return NULL;
    }
}

//OLD IModel* IModel::createModel(const std::string& modelName, size_t dim)
IModel* IModel::createModel(const std::string& modelName, const IParams* ptrModelParams)
{
    for(size_t i=0; i < FAKE_MODEL; ++i)
    {
        if (modelName == IModel::modelNames[i])
        {
            if(ptrModelParams)
            {
                return createModel((const enum IModel::ModelsTypes )i, ptrModelParams);
            }
            else
            {
                return createModel((const enum IModel::ModelsTypes )i);
            }
        }
    }
    return createModel(FAKE_MODEL);
}
///TODO!!! � � ���������� ������, � � ������� ������ ����� ���� ���������� I_PF_Model ����������

/**************************************************
* Factory Implementation to make model's Params
***************************************************/
/** ���������� ������ ����� ���� IParams */
int IModel::parseAndCreateModelParams(const enum IModel::ModelsTypes modelType, std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log)
{
    switch (modelType)
    {
    case  GaussianModel:
        return IModelGaussian::parseAndCreateModelParams(mapParams, postfix, ptrToPtrIParams, log);
    case WienerModel:
        return IModelWiener::parseAndCreateModelParams(mapParams, postfix, ptrToPtrIParams, log);
    case ParetoModel:
        return IModelPareto::parseAndCreateModelParams( mapParams, postfix, ptrToPtrIParams, log);
    case PoissonDrvnParetoJumpsExpMeanRevertingModel:
        return IModelPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateModelParams( mapParams, postfix, ptrToPtrIParams, log);
    default:
        return IModel_Impl::parseAndCreateModelParams(mapParams, postfix, ptrToPtrIParams, log);
    } /// end switch()
}

/**************************************************
*        Comparator to compare  IModel models
***************************************************/
bool IModel::compareModels(const IModel* model_1, const IModel* model_2)
{
    if ((model_1 == NULL) || (model_2 == NULL))
    {
        return false;
    }
    enum IModel::ModelsTypes mt_1=FAKE_MODEL;
    model_1->getModelType(mt_1);
    enum IModel::ModelsTypes mt_2=FAKE_MODEL;
    model_2->getModelType(mt_2);
    if (mt_1 != mt_2)
    {
        return false;
    }
    return true;
}

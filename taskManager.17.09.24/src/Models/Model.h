#ifndef __MODEL_H__633569422542968750
#define __MODEL_H__633569422542968750

#include <string>
#include <vector>
#include <map>
#include <math.h>
#include "../Log/Log.h"
///TODO!!! What's the point #include "../DataCode/DataInfo.h"?
/** IAA : DON'T pollute global scope! NO WAY in h-file -> using namespace std; */

/** forward declarations */
/** Task ������� ������ �� �����! class Task; */
class IParams;
typedef struct tagDataInfo DataInfo;
/** abstract Base class - interface */
class IModel
{
public:
    enum ModelsTypes{
        PlainModel,
        PolynomialModel,
        GaussianModel,
        WienerModel,
        PoissonModel,
        ParetoModel,
        PoissonDrvnParetoJumpsExpMeanRevertingModel,
        ADDITIVE_MODEL,
        FAKE_MODEL
    };
/* OLD C++ < C++11    static const char* modelNames[FAKE_MODEL]; */
    static constexpr const char* modelNames[FAKE_MODEL]=
    {
        "PLAIN_MODEL",
        "POLYNOMIAL_MODEL",
        "GAUSSIAN",
        "WIENER",
        "POISSON",
        "PARETO",
        "POISSON_DRVN_PARETO_JUMPS_EXPMEANREVERTING_MODEL",
        "ADDITIVE_MODEL"
    };
    static enum IModel::ModelsTypes getModelTypeByModelName(char const * const modelName);

    /** � ����� ������ ������ ����� ������ factory,
    ����� ������� ��������� ������.
    �� ����� ��� �����? � ���������� IModel,
    ����� ����� � ������ ��������� ������ ���� ������?
    ����� ����� ����� �������������� ���������� ��� ���������
    ����������� ������ ������
    */

    /** ����� �������� � �������������� ��� PLAIN_MODEL
    �������� ����� �������� ���������� ��������� ptrModelParams,
    ������� �� ������� NULL!
    � ��� �������, � ������� ����������� ������ ����������,
    ��������� �������� ����� ��������������, ���� ���� �� �� NULL
    */
    static IModel* createModel(const enum IModel::ModelsTypes modelType, const IParams* ptrModelParams=NULL);
    static IModel* createModel(const std::string& modelName, const IParams* ptrModelParams=NULL);

    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    IModel(){};
    virtual ~IModel() {};
    virtual std::string toString() const =0;
    /** ������ ������� ���������������� ������ � ����������� ������
     ���������� ��� ������� ���������/������ ���������� ������ !
    ����� ���� ������� �� ���������� Task/Estimator!
    ������ ���� ��������� ������ ��� ������!
    �� �� ���������� � ���� ����������� ������� � ������ �� �����!
    ����� �� ��������� ��������� �� ��������� ������
    ������ �� ��������� ������ �� ��� � ��������� ����������
    � ������ �����, ������� �������� ����� ���������� ���� �� ����, ���� ������

    ������ ����� �������������� �� ������ ���������� � ���� ���������� ������!
    �.�. ��� ������-����� (������ ����� ���� ������� ��������� ���������� ��� �������� ���������� ������),
    �� ����� ������ �������� enum IModel::ModelsTypes modelType,
    ���������� ��� IParams** ����� �������� "������ ��������" ptrToPtrIParams, ������ � size ����� ������� ��������!
    � ��� ������� - ��������� �������
    */
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(const enum IModel::ModelsTypes mt, std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger);

    /** ��� � ���� ������� ��� ����������� ���������
    �� ��������� ������ ��� ���� ������ ����������
    */
    virtual bool canCastModelParams(IParams * params) const = 0;
    /** ����������, ������� ����� ������ ��� ��������
    ����������� � ParamsSet/ParamsCompact ����������:
    �� �� ���������, ����� � ���� ����������� ������
    ���������� ��������� ������ �������
    */
    static bool compareModels(const IModel* model_1, const IModel* model_2);
    /** ��� ��� ������ ����� ����������� � ������� ������! */
    /** NO WAY -> setModelType()!
     REDEFINITION of Model Type FORBIDDEN!
    */
    virtual void getModelType(enum IModel::ModelsTypes& modelType) const = 0;
    /** ��� � ���� ������� ��� ���� �����, ��� �����������
    ��� ������ ��� ���� ������ ����������
    */
    virtual size_t getModelParamsType() const = 0;
    /**
    ��� ������� ��������� ���������� DataInfo
    � ��������� ���������� ���� ������
    � �������� � ����������  DataInfo*->double **
    ������������� NAN'�
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToIParams) const = 0;

    /** Task ������� ������ �� �����! virtual void setTask(Task* const & task) =0; */

    virtual void setLog(Logger* const & log) =0;
/** ��������� ������ ������� - ��� ��� ����������! */
protected:
// ��� ����� ���� ����� static �� ������ ������, �� ��� �������������� ����� ��� � ������ ����������
//    virtual void parsingFailed(std::string msg) const =0;

private:
    /** assign and copy CTORs are disabled */
    IModel(const IModel&) = delete;
    void operator=(const IModel&) = delete;
};
/** forward declaration */
class IParamsUnion;
/** abstract class - interface for Probabilistic estimators */
class I_Probabilistic_Model /** : public virtual IModel <- ������������ �� IModel ��������! ��� ��������� ��������� ������ Log! */
{
public:
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    I_Probabilistic_Model() = default;
    virtual ~I_Probabilistic_Model() = default;

    /**
    ��������� ������� � ���������� ���������� ���������� I_Probabilistic_Model
    �������� ���������� ��� ���������� ������ ������� �� Distributions\Distributions.cpp!
    �.�. ���� ���������� ������ ���� � Distributions\Distributions.cpp
    � � ���� �������� ������ ���������/����������� ���������� ����� ����������,
    ����������� ��� ��������� � ���������� ������ ������� �� Distributions\Distributions.cpp!
    */
    virtual int CDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfCDF, unsigned t = 0) const = 0;
    virtual int PDF(double x, int indexOfParams, const IParamsUnion* const h, double& valueOfPDF, unsigned t = 0) const = 0;
    virtual int getMaxMode(int indexOfParams, const IParamsUnion* const h, double& valueOfMaxMode) const = 0;
    virtual int generateRV(int indexOfParams, const IParamsUnion* const h, double& randomValue, unsigned t = 0) const = 0;
    /** GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL
    is in use as first param indexOfParams of AdditiveModel::generateSample
    to define such mode of AdditiveModel::generateSample() ONLY!
    I.e. the const does not make sense for ordinary model (NOT AdditiveModel)
    */
    static const int GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL = -1;
    virtual int generateSample(int indexOfParams, const IParamsUnion* const h, double* & sampleValues, size_t sampleSize, IParamsUnion* pointEstimatesOfParams, IParamsUnion* intervalEstimatesOfParams[2]) const = 0;

    /// TODO:
    std::string toString() {
        return std::string("Probabilistic_Model");
    }

///TODO!!!
private:
    /** assign and copy CTORs are disabled */
    I_Probabilistic_Model(const I_Probabilistic_Model&);
    void operator=(const I_Probabilistic_Model&) = delete;
};

/** forward declaration */
/** ����� ������������ ������ ParamsUnion,
������ ��� � ����� ������ ������ ��� ����������
�������� ������ ������ ���� ����������,
�� �� �� ��������� � ������, ����� � ��� ���������� ������
������ ������ ���� ���������.
� ��������� ������ IParamsUnion ����� ���������
������ ���� ������� � ������� ���������� IParams ** _addends
(size_t _totalOfAddends = 1)
*/
class IParamsUnion;

/** abstract class - interface for PF-estimators */
/**
 Task ������� ������ �� �����!
 �� �����! ������������ �� IModel ��������� ��������� Task, Log
 �������� ��������� I_PF_Model � IModel
 � �������� Task, Log
*/

/** I_PF_Model ��������� �� I_Probabilistic_Model */
class I_PF_Model : public virtual I_Probabilistic_Model
/** �� ����� ��-�� Logger'� ����� ��� ������������ : public virtual IModel */
{
public:
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    I_PF_Model(){};
    virtual ~I_PF_Model(){};

    ///TODO!!! ��� ��� ���� ������ ���������� MODEL_PARAMS_SRC � ������ ������ � ������ �������� ptrToPtrIParams ������������ ������ ���� ������
    /// � ������ �� ��� ��� �����, ����� ��� ������� �� ����� ������� �������� �������, ������� ���������� � ����� PF-����������,
    /// ������ ��� ��� ��������� ����� ������� ������� �� ������!
    static int parseAndCreateModelParamsCompact(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * log);

    ///TODO!!! ������ ����� ������ �� ������� �� ���������� I_Probabilistic_Model
    virtual double estimateHypothesis(size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const = 0;
    ///TODO!!! ������ ����� ������ �� ������� �� ���������� I_Probabilistic_Model
    virtual double estimatePointMove(size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

    virtual int initParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const = 0;
    ///TODO!!! ������ ����� ������ �� ������� �� ���������� I_Probabilistic_Model
    virtual int generateParticles(size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const = 0;
private:
    /** assign and copy CTORs are disabled */
    I_PF_Model(const I_PF_Model&);
    void operator=(const I_PF_Model&);
};

/** forward declaration */
class IParams1D;

class IModelPlain : public virtual IModel
{
public:
    static IModelPlain* createEmptyModel(const size_t& dim);
///TODO!!! ����� ��� ������ �� �����???
    static int createEmptyParamsOfModel(size_t& dim, IParams*& ptrToIParams);
///TODO!!! ??? IParams** ptrToPtrIParams -> IParams*& ptrToIParams? Semantics of size?
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log);
    virtual ~IModelPlain(){};
    virtual void setDimension(const size_t& dim) = 0;
    virtual int getDimension(size_t& dim) const = 0;
    /** to make params of IModelPolynomial*/
protected:
    IModelPlain(){};
private:
    /** assign and copy CTORs are disabled */
    IModelPlain(const IModelPlain&);
    void operator=(const IModelPlain&);
};

class IModelPolynomial : public virtual IModel
{
public:
    static IModelPolynomial* createEmptyModel(const size_t& dim);
///TODO!!! ����� ��� ������ �� �����???
    static int createEmptyParamsOfModel(size_t& dim, IParams*& ptrToIParams);
///TODO!!! ??? IParams** ptrToPtrIParams -> IParams*& ptrToIParams? Semantics of size?
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log);
///��� ��� ��� ����� �� ���������� IUnivariateFunction    static int evaluatePolynom(const double arg, const IParams*& coeffs, double& value);
    virtual ~IModelPolynomial(){};
    virtual void setDimension(const size_t& dim) = 0;
    virtual int getDimension(size_t& dim) const = 0;
    /** to make params of IModelPolynomial*/

protected:
    IModelPolynomial(){};
private:
    /** assign and copy CTORs are disabled */
    IModelPolynomial(const IModelPolynomial&);
    void operator=(const IModelPolynomial&);
};

/** forward declaration */
class IParamsGaussian;
/** IModelGaussian implements interface I_Probabilistic_Model */
class IModelGaussian : public virtual IModel,
                       public virtual I_Probabilistic_Model
{
public:
    IModelGaussian(){};
    virtual ~IModelGaussian(){};
    /** to make params of IModelGaussian */
    static int createEmptyParamsOfModel(IParams*& ptrToIParams);
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log);
///TODO!!! -> IFunctor(), �.�. ������ ��� SimpleEstimator ���������, ��������� ��������� ����� Quantile ������ �������� ��������� mu, sigma, � ���������� p-value!
    static int Quantil(const std::vector<double>& sampleOfOrderedAbsItems, double pValue, double& quantile);

///TODO!!! �������� ������ ��� ������������� �������? ����� ���� ����� ����� � �����-�� ����������? ???    IModelGaussian* makeEmptyModel();
//OLD ������ �� �������� ���������� PF_Model ��� � ����������    virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������   virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelGaussian(const IModelGaussian&);
    void operator=(const IModelGaussian&);
};

/** forward declaration */
class IParamsWiener;
/** IModelWiener implements interface I_Probabilistic_Model */
class IModelWiener : public virtual IModel,
                     public virtual I_Probabilistic_Model
{
public:
    IModelWiener(){};
    virtual ~IModelWiener(){};
    /** to make params of IModelWiener */
    static int createEmptyParamsOfModel(IParams*& ptrToIParams);
//OLD!    virtual IParams* parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log);
/// �������� ������ ��� �������������� ����? � �������? ����� ���� ����� ����� � �����-�� ����������?
///???    IModelWiener* makeEmptyModel();

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelWiener(const IModelWiener&);
    void operator=(const IModelWiener&);
};

/** forward declaration */
class IParamsPoisson;
/** IModelPoisson implements interface I_Probabilistic_Model */
class IModelPoisson : public virtual IModel ,
                      public virtual I_Probabilistic_Model
{
public:
    IModelPoisson(){};
    virtual ~IModelPoisson(){};
    /** to make params of IModelPoisson */
    static int createEmptyParamsOfModel(IParams*& ptrToIParams);
//OLD!    virtual IParams* parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log);
/// �������� ������ ��� �������������� ����? � �������? ����� ���� ����� ����� � �����-�� ����������?
///???    IModelPoisson* makeEmptyModel();

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelPoisson(const IModelPoisson&);
    void operator=(const IModelPoisson&);
};

/** forward declaration */
class IParamsPareto;
/** IModelPareto implements interface I_Probabilistic_Model */
class IModelPareto : public virtual IModel,
                     public virtual I_Probabilistic_Model
{
public:
    IModelPareto(){};
    virtual ~IModelPareto(){};
    /** to make params of IModelPareto */
    static int createEmptyParamsOfModel(IParams*& ptrToIParams);
//OLD!    virtual IParams* parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * log);
/// �������� ������ ��� �������������� ����? � �������? ����� ���� ����� ����� � �����-�� ����������?
///???    IModelPareto* makeEmptyModel();

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelPareto(const IModelPareto&);
    void operator=(const IModelPareto&);
};

/** forward declaration */
class IParamsPoissonDrvnParetoJumpsExpMeanReverting;
/** IModelPoissonDrvnParetoJumpsExpMeanReverting implements interface I_Probabilistic_Model */
class IModelPoissonDrvnParetoJumpsExpMeanReverting : public virtual IModel,
                                                     public virtual I_Probabilistic_Model
{
public:
    /** ��� ��� �������� ������������� � ���������� ��������� ���������, ���������� �� ��� ������,
    ����� ��� ����� ����� ���� ����������� (IModel � I_Probabilistic_Model) ���� ������
    */
    /** DEFINED as "ExpMeanRevertingLambda" in implementation (see Model.cpp file) */
/// � ��� ���� � IParamsPoissonDrvnParetoJumpsExpMeanReverting ���� "MR_LAMBDA"!
//OLD    static constexpr const char * IModelPoissonDrvnParetoJumpsExpMeanReverting::strExpMeanRevertingLambda = "ExpMeanRevertingLambda";
    IModelPoissonDrvnParetoJumpsExpMeanReverting(){};
    virtual ~IModelPoissonDrvnParetoJumpsExpMeanReverting(){};
    /** to make params of IModelPoissonDrvnParetoJumpsExpMeanReverting */
    static int createEmptyParamsOfModel(IParams*& ptrToIParams);
    /** ���������� ������ ����� ���� IParams */
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, IParams** ptrToPtrIParams, Logger * logger);

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual int initParticles(int indexOfParams, IParamsUnion* h, vector<double>& particles) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual int generateParticles(int indexOfParams, IParamsUnion* h, vector<double>& particles) const = 0;
private:
    /** assign and copy CTORs are disabled */
    IModelPoissonDrvnParetoJumpsExpMeanReverting(const IModelPoissonDrvnParetoJumpsExpMeanReverting&);
    void operator=(const IModelPoissonDrvnParetoJumpsExpMeanReverting&);
};
#endif

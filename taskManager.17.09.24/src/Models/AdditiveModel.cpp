#include <cmath> //NAN
#include <sstream>

#include "../CommonStuff/Defines.h" //DELIMITER_TO_STRING_ITEMS
#include "../Log/errors.h"
#include "AdditiveModel.h"

#include "../Config/ConfigParser.h" //iteratorParamsMap
#include "../Params/Params.h"

#include "../Distributions/Distributions.h"

/** static in IAdditiveModel
*/
constexpr const char * IAdditiveModel::additiveModelsNames[FAKE_ADDITIVE_MODEL];

enum AdditiveModelsTypes getAdditiveModelsTypeByAdditiveModelName(const std::string & additiveModelName)
{
    for (auto i = 0u; i < FAKE_ADDITIVE_MODEL; ++i) {
        if (additiveModelName == IAdditiveModel::additiveModelsNames[i]) {
            return static_cast<enum AdditiveModelsTypes>(i);
        }
    }
    return FAKE_ADDITIVE_MODEL;
}

constexpr const char * IAdditiveModel::grammar[GrammarSizeForAdditiveModelAddends];

/** anonymous namespace to hide private implementations into this cpp-file */
namespace {
    /*******************************************************
    * Base abstract IAdditiveModel class implementation
    ********************************************************/
    class IAdditiveModel_Impl : public virtual IAdditiveModel
    {
    public:
        /** CTOR - create additive model by addends types */
        IAdditiveModel_Impl(size_t totalOfAddends, const enum IModel::ModelsTypes * addends,
                            Logger * const logger);

        /** DTOR */
        virtual ~IAdditiveModel_Impl();

        /**
        ���� ����� �� �� ���������� ����������,
        �� ����� ������ ��� �������� ���������� ������
        ����� �� ��� ����� �������������������,
        ��������, ��� ���������� ��� ������� ������
        ���������� ���� ������, ����������� ���������� �����������
        */
        /// TODO!!! //OLD    virtual IParamsUnion* createEmptyParamsOfModel() const;
        bool canCastModelParamsUnion(IParamsUnion * params) const override;

        /** Any subclass may have own copy algorithm */
        /// TODO!!! What's for?    virtual IAdditiveModel_Impl* clone() const;

        /** Every subclass MUST have own IMPLEMENTATION for toString()! */
        std::string toStringAllAddends() const override;
        /** Every subclass MUST have own IMPLEMENTATION for setTask()! */
        /// TODO!!!  Task ������� ������ �� �����!
        ///    virtual void setTask(Task* const & task);
        /** Every subclass MUST have own IMPLEMENTATION for setLog! */
        void setLog(Logger * const & logger) override;

        int getTotalOfAddends(size_t & size) const override;

        /// TODO!!! subclasses MIGHT define operator(i,...) to get access to params by index
        int setAddend(size_t index, IModel * addendModel) override;
        int setAddends(size_t size, IModel ** addendModels) override;

        int getAddendType(size_t index, enum IModel::ModelsTypes & addendModelType) const override;
        int getAddendsTypes(size_t size, enum IModel::ModelsTypes *& addendModelsTypes) const override;

        int getPtrToAddend(size_t index, IModel *& addendModel) const override;
        int getPtrsToAddends(size_t size, IModel **& addendModels) const override;

    protected:
        enum AdditiveModelsTypes m_additiveModelType;
        void getModelType(enum AdditiveModelsTypes & additiveModelType) const override;
        Logger * m_logger;

        static void parsingFailed(std::string msg);

        /** NO public ctor without params! Use static Factory instead! */
        IAdditiveModel_Impl() = default;

        /** data stuff */
        /** access to data stuff is required by subclasses */
        /** total of m_addends ptrs and m_addendsTypes ptrs */
        size_t m_totalOfAddends;
        IModel ** m_addends;
        enum IModel::ModelsTypes * m_addendsTypes;

    private:
        /** assign and copy CTORs are disabled */
        IAdditiveModel_Impl(const IAdditiveModel_Impl &) = delete;
        void operator=(const IAdditiveModel_Impl &) = delete;
        int reallocatePtrToAddends(size_t totalOfAddends);
        int reallocateAddends(size_t totalOfAddends, const enum IModel::ModelsTypes * addendsTypes);
    };

    /*******************************************************
    * Base abstract I_PF_AdditiveModel class implementation
    ********************************************************/
    class I_PF_AdditiveModel_Impl : public virtual IAdditiveModel_Impl,
                                    public virtual I_Probabilistic_Model,
                                    public virtual I_PF_AdditiveModel
    {
    public:
        /** CTOR - create additive model by TWO addends types */
        /// TODO!!! ���� ��� ����� �������������� ������� ������������ ������������ ����������� !
        /// I_PF_AdditiveModel_Impl(size_t size, const enum IModel::ModelsTypes* addends, Logger * const logger);
        I_PF_AdditiveModel_Impl(const enum IModel::ModelsTypes addends[dim_I_PF_AdditiveModel], Logger * const logger);

        /** DTOR */
        virtual ~I_PF_AdditiveModel_Impl();
        using IAdditiveModel_Impl::toStringAllAddends;
        virtual std::string toString() const;

        /// ������ ������ ������� �� ����������  I_PF_AdditiveModelParams :
        ///   static I_PF_AdditiveModelParams*  parseAndCreateParams(const I_PF_AdditiveModel* model,
        ///   std::map<std::string, std::string>& mapParams, std::string postfix);
        /// ������ ���������� ���������

        /// TODO!!! BUG! ���� ��� ������� ����� ������� � ptrToPtrIParamsUnion ��� � ����� ����������! �� SEGFAULT!
        int parseAndCreateModelParams(std::map<std::string, std::string> & mapParams, std::string postfix,
                                      IParamsUnion ** ptrToPtrIParamsUnion) const override;
        /** ��� ������� ����� ��� ������ � ��������� �� ��� ������� � ���������� IParamsUnion,
        ������� ������������� ������ �� ��������� PF ���������� ������ (m_noiseModel, m_particleGeneratingModel)
        ��� ���������� �������� � �������� ���������� ���������� �� ��������, � ������� ��� �������� - IParamsUnion,
        � ���������� ������ PFAdditiveModel- ������ � ���� ������������ ����������� ����������� ��� ����� ���������
        �� ��������� items � ���� ���������� IParamsUnion
        */
        IParams * getPtrToNoiseParams(const IParamsUnion *& ptrToIParamsUnion) const override;
        IParams * getPtrToParticleGeneratingParams(const IParamsUnion *& ptrToIParamsUnion) const override;

        /// TODO!!!
        using IAdditiveModel_Impl::canCastModelParamsUnion;
        /// TODO!!!
        using IAdditiveModel_Impl::getModelType;
        /// TODO!!!  Task ������� ������ �� �����!
        ///    void setTask(Task* const & task);
        void setLog(Logger * const & log) override;

        using IAdditiveModel_Impl::getTotalOfAddends;
        using IAdditiveModel_Impl::getAddendType;
        using IAdditiveModel_Impl::getAddendsTypes;

        using IAdditiveModel_Impl::getPtrToAddend;
        using IAdditiveModel_Impl::getPtrsToAddends;

        I_PF_Model const * getNoiseModel() const override;
        I_PF_Model const * getParticleGeneratingModel() const override;

        /** I_Probabilistic_Model interface implementation */
        int CDF(double x, int indexOfParams, const IParamsUnion * const h, double & valueOfCDF,
                unsigned t = 0) const override;
        int PDF(double x, int indexOfParams, const IParamsUnion * const h, double & valueOfPDF,
                unsigned t = 0) const override;
        int getMaxMode(int indexOfParams, const IParamsUnion * const h, double & valueOfMaxMode) const override;
        int generateRV(int indexOfParams, const IParamsUnion * const h, double & randomValue,
                       unsigned t = 0) const override;

        int generateSample(int indexOfParams, const IParamsUnion * const h, double *& sampleValues, size_t sampleSize,
                           IParamsUnion * pointEstimatesOfParams,
                           IParamsUnion * intervalEstimatesOfParams[2]) const override;

        /** I_PF_Model interface implementation */
        double estimateHypothesis(size_t indexOfParams, IParamsUnion * h, double point,
                                  std::vector<double> & particles, unsigned t) const override;
        int initParticles(size_t indexOfParams, IParamsUnion * h, std::vector<double> & particles) const override;
        int generateParticles(size_t indexOfParams, IParamsUnion * h,
                              std::vector<double> & particles) const override;

        double estimatePointMove(size_t indexOfParams, IParamsUnion * h, double move, unsigned t) const override;

    protected:
        static void parsingFailed(std::string msg);

    private:
        I_PF_Model * m_noiseModel;
        I_PF_Model * m_particleGeneratingModel;

        /** NO public ctor without params!*/
        I_PF_AdditiveModel_Impl() = default;

        /** assign and copy CTORs are disabled */
        I_PF_AdditiveModel_Impl(const I_PF_AdditiveModel_Impl &) = delete;
        void operator=(const I_PF_AdditiveModel_Impl &) = delete;
    };

} /// end namespace

/******************** IMPLEMENTATIONs ****************************/
/*****************************************************************
*                 IAdditiveModel_Impl Implementation!
*****************************************************************/
/// static in IAdditiveModel!
IAdditiveModel * IAdditiveModel::createModel(size_t totalOfAddends, const enum IModel::ModelsTypes * addends,
                                             Logger * const logger)
{
    if (!totalOfAddends || addends == nullptr) {
        logger->error(ALGORITHM_MODELDATA_ERROR,
                      std::string("No info to create AdditiveModel in IAdditiveModel::createModel()"));
        return nullptr;
    }

    return new IAdditiveModel_Impl(totalOfAddends, addends, logger);
}

/** static in IAdditiveModel */
IAdditiveModel * IAdditiveModel::parseAndCreateModel(const enum AdditiveModelsTypes amt, size_t totalOfAddends,
                                                     std::map<std::string, std::string> & addendsMap,
                                                     Logger * const logger)
{
    /** ������� ������� ������ �� ������ IParams, � ����� ���������������� �� � �����
    */
    auto ptrModelTypes = new enum IModel::ModelsTypes[totalOfAddends];

    std::vector<std::string> ModelAddensCfgNames;
    ModelAddensCfgNames.resize(totalOfAddends);

    /** ����� ���� �� ��������� ������� ��������� ������,
     � ������� ����� �������� �� cfg-�����
    ����� ���������������� ������ ��� ��������������� ��������� ���������� ������
    */
    for (auto i = 0u; i < totalOfAddends; ++i) {
        auto mt = IModel::ModelsTypes::FAKE_MODEL;
        ModelAddensCfgNames[i].clear();

        /** GRAMMAR! ����� �������� � ���� ��������� ���� ���������,
         ���� ��� ������ ������ � ���� �� ����, �������� ��� ���������,
         �������� ����� ���� ���������� ����������, ������������� ���������� ������ ����������,
         � �������� ���� ��������� ���������
        */
        std::stringstream ss;
        ss << IAdditiveModel::grammar[ModelName] << "_" << i; // REDUNDANT + postfix;

        auto tmpStr = ss.str();
        // DEBUG std::cout << tmpStr << std::endl;
        auto itParam = addendsMap.find(tmpStr);

        if (itParam != addendsMap.end()) {
            std::string delimiters(DEFAULT_DELIMITERS);
            auto modelName = itParam->second;
            TrimStr(modelName, delimiters);
            mt = IModel::getModelTypeByModelName(static_cast<char const * const>(modelName.c_str()));
            if (mt == IModel::ModelsTypes::FAKE_MODEL) {
                logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR,
                              std::string(itParam->second) +
                                  ": wrong AdditiveModel model addend's name in IAdditiveModel::parseAndCreateModel()");
                delete[] ptrModelTypes;
                return nullptr;
            }

            ptrModelTypes[i] = mt;

        } else {
            logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR,
                          std::string(tmpStr + " NOT FOUND in IAdditiveModel::parseAndCreateModel()"));
            delete[] ptrModelTypes;
            return nullptr;
        }

        ss.clear();
        ss << IAdditiveModel::grammar[ModelParamsSrc] << "_" << i; // REDUNDANT + postfix;
        tmpStr = ss.str();

        itParam = addendsMap.find(tmpStr);
        if (itParam != addendsMap.end()) {
            std::string delimiters = DEFAULT_DELIMITERS;
            ModelAddensCfgNames[i] = itParam->second;
            TrimStr(ModelAddensCfgNames[i], delimiters);
            /// TODO!!! ������ ���������������� ������ ��������� ������ ���� �� ����������!
        }
    }

    for (auto i = 0u; i < totalOfAddends; ++i) {
        if (!ModelAddensCfgNames[i].empty()) {
            logger->error(
                ErrorLevels::ALGORITHM_MODELDATA_ERROR,
                std::string(
                    "ModelParamsSrcs processing is NOT IMPLEMENTED yet in IAdditiveModel::parseAndCreateModel()"));
            delete[] ptrModelTypes;
            return nullptr;
        }
    }
    /** ������ ����� ��������� ���������� ������
    */
    switch (amt) {
    case PLAIN_ADDITIVE_MODEL: {
        auto additiveModel = IAdditiveModel::createModel(
            totalOfAddends, const_cast<const enum IModel::ModelsTypes *>(ptrModelTypes), logger);
        /** ������� ��������� ������
        */
        delete[] ptrModelTypes;
        ptrModelTypes = nullptr;

        return additiveModel;
    }
    case PF_ADDITIVE_MODEL: {
        std::stringstream ss;
        size_t dimOfPFAdditiveModel = I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::dim_I_PF_AdditiveModel;
        if (totalOfAddends != dimOfPFAdditiveModel) {
            ss << "Wrong number of addends for I_PF_AdditiveModel! So far implemented " << dimOfPFAdditiveModel
               << " ONLY!";
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, ss.str());
            return nullptr;
        }
        /** �������� ���������� ��� noiseModelName, particleGeneratingModelName, ����� ����� ���� �� ��������� �
        cfg-files,
        ������� ������ ���������� PF Additive Mode,
        */
        ss.clear();
        size_t defaultIndexOfNoiseModelAddend =
            I_PF_AdditiveModel::indexesOfGrammarPFAdditiveModelAddends::indexOfNoiseModelAddendInAdditiveModel;
        ss << I_PF_AdditiveModel::grammarPFAdditiveModelAddends[defaultIndexOfNoiseModelAddend];
        auto tmpStr = ss.str();
        // DEBUG std::cout << tmpStr << std::endl;
        auto itParam = addendsMap.find(tmpStr);
        if (itParam != addendsMap.end()) {
            std::string delimiters = DEFAULT_DELIMITERS;
            TrimStr(itParam->second, delimiters);
            const char * ptrTmpChar = (itParam->second).c_str();

            int indexOfNoiseModel = -1;
            /** we are pessimists */
            int rc = _ERROR_NO_INPUT_DATA_;
            rc = strTo<int>(ptrTmpChar, indexOfNoiseModel); //OLD int indexOfNoiseModel = atoi(ptrTmpChar);
            addendsMap.erase(itParam);

            if((rc != _RC_SUCCESS_) || ((indexOfNoiseModel < 0) || (indexOfNoiseModel > static_cast<int>(dimOfPFAdditiveModel)))) {
                delete[] ptrModelTypes;
                ptrModelTypes = nullptr;
                logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR,
                              ": Wrong index of noise model for PF ADDITIVE_MODEL : " + tmpStr);
                return nullptr;
            }
            /** ���� ������ ���������� PF Additive Mode,
            ������� ������� ���������������� ��� noiseModel,
            �� ������������� �����������,
            ������������� enum indexesOfAddendsPF_AdditiveModel,
            ������� ����������� ���������
            */
            if (indexOfNoiseModel != static_cast<int>(defaultIndexOfNoiseModelAddend)) {
                auto tmpPtrModelTypes = IModel::ModelsTypes::FAKE_MODEL;
                tmpPtrModelTypes = ptrModelTypes[defaultIndexOfNoiseModelAddend];
                ptrModelTypes[defaultIndexOfNoiseModelAddend] = ptrModelTypes[indexOfNoiseModel];
                ptrModelTypes[indexOfNoiseModel] = tmpPtrModelTypes;
            }
        } else {
            delete[] ptrModelTypes;
            ptrModelTypes = nullptr;
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR,
                          "PF ADDITIVE_MODEL noise model NOT DEFINED! " + tmpStr + " NOT FOUND!");
            return nullptr;
        }
        std::string noiseModelName =
            IModel::modelNames[ptrModelTypes[I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_noiseModelIndex]];
        std::string particleGeneratingModelName = IModel::modelNames
            [ptrModelTypes[I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_particleGeneratingModelIndex]];
        auto additiveModel = I_PF_AdditiveModel::createModel(noiseModelName, particleGeneratingModelName, logger);
        /** ������� ��������� ������
        */
        delete[] ptrModelTypes;
        ptrModelTypes = nullptr;

        return additiveModel;
    } break;
    default: {
        delete[] ptrModelTypes;
        ptrModelTypes = nullptr;
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR,
                      "Wrong ADDITIVE_MODEL_TYPE in IAdditiveModel::parseAndCreateModel()!");
        return nullptr;
    }
    }///end switch()
  return nullptr;
}

/** static in IAdditiveModel! */
int IAdditiveModel::parseAndCreateModelParamsUnion(size_t totalOfAddends,
                                                   const enum IModel::ModelsTypes *& addendModelsTypes,
                                                   std::map<std::string, std::string> & mapParams, std::string postfix,
                                                   /* BUG size_t& size, */ IParamsUnion ** ptrToPtrIParamsUnion,
                                                   Logger * log)
{
    /** ������� ������� ������ �� ������ IParams, � ����� ���������������� �� � �����
    */
    auto tmpAddends = new IParams *[totalOfAddends];
    /** ����� ���� �� ��������� ������� addendModelsTypes, � ������� ����� �������� ������ �����
    IModel::parseAndCreateModelParams
    */
    for (auto i = 0u; i < totalOfAddends; ++i) {
        const auto mt = addendModelsTypes[i];
        /// TODO!!! GRAMMAR! ������ �������� : ��� �������� � ���� ��������� ���� ���������, ���� ��� ������ ������ �
        /// ���� �� ����, �������� ��� ���������?
        ///������ ������� ����� ���� ���������� ����������, ������������� ���������� ������ ����������, � �������� ����
        ///��������� ���������
        std::stringstream ss;
        ss << "_" << i << postfix;
        auto tmpPostfix = ss.str(); // = "_" + i + postfix;

        /** � ����� ������� ��� ������� ���������� ���� IParams* */
        auto tmpPtrToPtrIparams = &(tmpAddends[i]);

        ///������ ���� ������� ����� ���������� � postfix static'� int parseAndCreateModelParams(enum
        /// IModel::ModelsTypes mt, std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size,
        /// IParams** ptrToPtrIParams, Logger * log);
        /** ���������� ������ ����� ���� IParams */
        auto rc = IModel::parseAndCreateModelParams(mt, mapParams, tmpPostfix, tmpPtrToPtrIparams, log);
        /// for each addends we expect the ONLY ONE IParams!
        /** tmpSize!= 1 -> to double-check we have got single IParams into tmpPtrToPtrIparams */

        if (rc) {
            std::string msg = IModel::modelNames[mt];
            msg = "Fail to parse " + msg + "-params! The rest of models' params were not initialized!";
            log->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, msg);

            for (auto j = 0u; j < totalOfAddends; ++j) {
                delete tmpAddends[j];
            }

            delete[] tmpAddends;

            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    /** ������ �� ����� ���������� � IParamsUnion** ptrToPtrIParamsUnion
    */
    /** ���������� ������ ����� ���� IParamsUnion */
    *ptrToPtrIParamsUnion =
        IParamsUnion::createParamsUnion(totalOfAddends, const_cast<const IParams **>(tmpAddends), log);
    /** ������� ��������� ������
    */

    for (auto i = 0u; i < totalOfAddends; ++i) {
        delete tmpAddends[i];
    }
    delete[] tmpAddends;

    ///  std::cout << "NOT implemented IAdditiveModel_Impl::parseAndCreateModelParams!" << std::endl;
    return _RC_SUCCESS_;
}

/****************************************************************
*   Base class - implementation
*****************************************************************/

/************************************************
* CTOR - create additive model by addends types *
************************************************/
IAdditiveModel_Impl::IAdditiveModel_Impl(size_t totalOfAddends, const enum IModel::ModelsTypes * addends,
                                         Logger * const logger)
{
    // OLD???    _modelType = IModel::ModelsTypes::ADDITIVE_MODEL;

    m_additiveModelType = PLAIN_ADDITIVE_MODEL;
    m_logger = logger;
    m_totalOfAddends = 0;
    m_addends = nullptr;
    m_addendsTypes = nullptr;

    if (!totalOfAddends || !addends) {
        return;
    }

    reallocateAddends(totalOfAddends, addends);
}

/*************************************************
*                  DTOR                          *
**************************************************/
IAdditiveModel_Impl::~IAdditiveModel_Impl()
{
#ifdef _DEBUG_
    std::cout << "into ~IAdditiveModel_Impl:" << std::endl;
#endif
    for (auto i = 0u; i < m_totalOfAddends; ++i) {
        if (m_addends[i])
            delete m_addends[i];
        m_addends[i] = nullptr;
    }

    delete[] m_addendsTypes;
    m_addendsTypes = nullptr;
    delete[] m_addends;
    m_addends = nullptr;
}
/*************************************************
*                  reallocatePtrToAddends        *
**************************************************/
int IAdditiveModel_Impl::reallocatePtrToAddends(size_t totalOfAddends)
{
    if (m_addends) {
        for (auto i = 0u; i < m_totalOfAddends; ++i) {
            if (m_addends[i])
                delete m_addends[i];
            m_addends[i] = nullptr;
        }
    }

    delete[] m_addends;
    m_addends = nullptr;

    delete[] m_addendsTypes;
    m_addendsTypes = nullptr;

    if (!totalOfAddends) {
        m_totalOfAddends = 0;
        return _RC_SUCCESS_;
    }

    m_addends = new IModel *[totalOfAddends];
    if (!m_addends) {
        return _ERROR_NO_ROOM_;
    }

    m_addendsTypes = new enum IModel::ModelsTypes[totalOfAddends];
    if (!m_addendsTypes) {
        delete[] m_addends;
        return _ERROR_NO_ROOM_;
    }

    for (auto i = 0u; i < totalOfAddends; ++i) {
        m_addends[i] = nullptr;
        m_addendsTypes[i] = IModel::ModelsTypes::FAKE_MODEL;
    }

    m_totalOfAddends = totalOfAddends;
    return _RC_SUCCESS_;
}
/*************************************************
*                  reallocateAddends             *
**************************************************/
int IAdditiveModel_Impl::reallocateAddends(size_t totalOfAddends, const enum IModel::ModelsTypes * addendsTypes)
{
    auto rc = reallocatePtrToAddends(totalOfAddends);
    if (rc) {
        return rc;
    }

    for (auto i = 0u; i < totalOfAddends; ++i) {
        auto addendType = addendsTypes[i];

        /// Init m_addends!
        if (addendType < IModel::ModelsTypes::FAKE_MODEL) {
            m_addends[i] = IModel::createModel(static_cast<const enum IModel::ModelsTypes>(addendType));
            m_addendsTypes[i] = addendType;
        } else {
            return _ERROR_WRONG_ARGUMENT_;
        }
    }

    return _RC_SUCCESS_;
}
/*************************************************
*                  getModelType                  *
**************************************************/
void IAdditiveModel_Impl::getModelType(enum AdditiveModelsTypes & additiveModelType) const
{
    additiveModelType = m_additiveModelType;
}
/* TODO!!!
int IAdditiveModel_Impl::createEmptyParamsOfModel(size_t& size, IParams** ptrToPtrIParams) const
{
  std::cout << "NOT implemented IAdditiveModel_Impl::createEmptyParamsOfModel!" << std::endl;

  I_PF_AdditiveModelParams* params = I_PF_AdditiveModelParams::parseAndCreateParams((const I_PF_AdditiveModel*)this, mapParams, postfix);
  return (IParamsUnion*)params;
}
*/
/** static in IAdditiveModel_Impl */
void IAdditiveModel_Impl::parsingFailed(std::string msg)
{
    /// TODO: we have to decide! Which addend did not get its params?
    std::cout << "NOT implemented IAdditiveModel_Impl::parsingFailed!" << std::endl;
}
/*************************************************
*                  canCastModelParamsUnion       *
**************************************************/
bool IAdditiveModel_Impl::canCastModelParamsUnion(IParamsUnion * params) const
{
    auto totalOfItems = params->getTotalOfItemsInParamsUnion();
    if (totalOfItems != m_totalOfAddends) {
        return false;
    }

    for (auto i = 0u; i < m_totalOfAddends; ++i) {
        /** first pull out next IParams from IParamsUnion */
        IParams * param = nullptr;
        params->getItemCopy(i, param);
        /** then try to cast this IParams to type
        of corresponding IModle params
        of this IAdditiveModel
        */

        bool rc = m_addends[i]->canCastModelParams(param);
        /** we have got copy of IParams
        so clean up it
        */
        delete param;

        if (!rc) {
            return false;
        }
    }

    return true;
}

/************************************************************
* Every subclass MUST have own IMPLEMENTATION for toString()!
*************************************************************/
std::string IAdditiveModel_Impl::toStringAllAddends() const
{
    std::stringstream ss;
    for (auto i = 0u; i < m_totalOfAddends; ++i) {
        ss << m_addends[i]->toString();
        if (i < m_totalOfAddends - 1) {
            ss << DELIMITER_TO_STRING_ITEMS;
        }
    }
    return ss.str();
}

/*OLD REDUNDANT
void IAdditiveModel_Impl::setTask(Task* const & task)
{
    for(size_t i=0; i< m_totalOfAddends; ++i)
    {
        m_addends[i]->setTask(task);
    }
    _task = task;
}
*/

/// Invariant for every model
void IAdditiveModel_Impl::setLog(Logger * const & logger)
{
    for (size_t i = 0; i < m_totalOfAddends; ++i) {
        m_addends[i]->setLog(logger);
    }
    m_logger = logger;
}

int IAdditiveModel_Impl::getTotalOfAddends(size_t & size) const
{
    size = m_totalOfAddends;
    return _RC_SUCCESS_;
}

/** for model we don't need to clone model instance
 assumed the ptr to new model instance we have got as addendModel
*/
int IAdditiveModel_Impl::setAddend(size_t index, IModel * addendModel)
{
    if (index >= m_totalOfAddends) {
        return _ERROR_WRONG_ARGUMENT_;
    }

    if (!addendModel) {
        return _ERROR_WRONG_ARGUMENT_;
    }

    delete m_addends[index];
    m_addends[index] = nullptr;

    /** for model we don't need to clone model instance
    assumed the ptr to new model instance we have got as addendModel
    */
    m_addends[index] = addendModel;
    return _RC_SUCCESS_;
}

int IAdditiveModel_Impl::setAddends(size_t size, IModel ** addendModels)
{
    if (!addendModels) {
        return _ERROR_WRONG_ARGUMENT_;
    }

    if (size != m_totalOfAddends) {
        auto rc = reallocatePtrToAddends(size);
        if (rc) {
            return rc;
        }
    }

    for (auto i = 0u; i < size; ++i) {
        if (addendModels[i]) {
            /** for model we don't need to clone model instance
             assumed the ptr to new model instance we have got as addendModel
            */
            setAddend(i, addendModels[i]);
        } else
            return _ERROR_WRONG_ARGUMENT_;
    }

    return _RC_SUCCESS_;
}

/** for model we don't need to clone model instance
 assumed the ptr to new model instance we have got as addendModel
*/
int IAdditiveModel_Impl::getAddendType(size_t index, enum IModel::ModelsTypes & addendModelType) const
{
    if (index >= m_totalOfAddends) {
        return _ERROR_WRONG_ARGUMENT_;
    }

#ifdef _DEBUG_
    std::cout << m_addendsTypes[index] << std::endl;
#endif

    /** for model we don't need to clone model instance
     assumed the ptr to new model instance we have got as addendModel
    */
    addendModelType = static_cast<enum IModel::ModelsTypes>(m_addendsTypes[index]);
    return _RC_SUCCESS_;
}

int IAdditiveModel_Impl::getPtrToAddend(size_t index, IModel *& addendModel) const
{
    if (index >= m_totalOfAddends) {
        return _ERROR_WRONG_ARGUMENT_;
    }

#ifdef _DEBUG_
    std::cout << m_addends[index]->toString() << std::endl;
#endif

    /** for model we don't need to clone model instance
     assumed the ptr to new model instance we have got as addendModel
    */
    addendModel = m_addends[index];
    return _RC_SUCCESS_;
}

int IAdditiveModel_Impl::getPtrsToAddends(size_t size, IModel **& addendModels) const
{
    if (!addendModels) {
        return _ERROR_WRONG_ARGUMENT_;
    }

    for (auto i = 0u; i < m_totalOfAddends && i < size; ++i) {
        getPtrToAddend(i, addendModels[i]);
    }

    return _RC_SUCCESS_;
}

int IAdditiveModel_Impl::getAddendsTypes(size_t size, enum IModel::ModelsTypes *& addendModelsTypes) const
{
    if (!addendModelsTypes) {
        return _ERROR_WRONG_ARGUMENT_;
    }

    for (auto i = 0u; i < m_totalOfAddends && i < size; ++i) {
        getAddendType(i, addendModelsTypes[i]);
    }

    return _RC_SUCCESS_;
}


/************************************************
*  Additive noise+process IAdditiveModel class *
************************************************/
/** static in I_PF_AdditiveModel */
constexpr const char * I_PF_AdditiveModel::grammarPFAdditiveModelAddends[GrammarSizeForPFAdditiveModelAddends];

/************************************************
*                      CTOR
*         NB SO FAR we expect size=2 ONLY!
************************************************/
/// TODO!!! ���� ��� ����� �������������� ������� ������������ ������������ �����������
/// TODO!! I_PF_AdditiveModel_Impl::I_PF_AdditiveModel_Impl(size_t size, const enum IModel::ModelsTypes* addends,
/// Logger * const logger) : IAdditiveModel_Impl(size, addends, logger)
I_PF_AdditiveModel_Impl::I_PF_AdditiveModel_Impl(const enum IModel::ModelsTypes addends[dim_I_PF_AdditiveModel],
                                                 Logger * const logger)
    : IAdditiveModel_Impl(dim_I_PF_AdditiveModel, addends, logger)
{
    // _noiseModelIndex = 0;
    IModel * ptrIModel = nullptr;
    auto rc = IAdditiveModel_Impl::getPtrToAddend(_noiseModelIndex, ptrIModel);
    if(_RC_SUCCESS_ != rc) {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, std::string(IAdditiveModel::additiveModelsNames[AdditiveModelsTypes::PF_ADDITIVE_MODEL]) + ": " + "Can not get ptr to noise model!");
        return;
    }
    m_noiseModel = dynamic_cast<I_PF_Model *>(ptrIModel);
    if(!m_noiseModel){
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, std::string(IAdditiveModel::additiveModelsNames[AdditiveModelsTypes::PF_ADDITIVE_MODEL]) + ": " + "Got wrong ptr to noise model!");
        return;
    }
#ifdef _DEBUG_
    std::cout << "In I_PF_AdditiveModel_Impl m_noiseModel :" << m_noiseModel->toString() << std::endl;
#endif

    rc = IAdditiveModel_Impl::getPtrToAddend(_particleGeneratingModelIndex, ptrIModel);
    if(_RC_SUCCESS_ != rc){
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, std::string(IAdditiveModel::additiveModelsNames[AdditiveModelsTypes::PF_ADDITIVE_MODEL]) + ": " + "Can not get ptr to particle generating model!");
        return;
    }
    m_particleGeneratingModel = dynamic_cast<I_PF_Model *>(ptrIModel);
    if(!m_particleGeneratingModel){
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, std::string(IAdditiveModel::additiveModelsNames[AdditiveModelsTypes::PF_ADDITIVE_MODEL]) + ": " + "Got wrong ptr to particle generating model!");
        return;
    }
#ifdef _DEBUG_
    std::cout << "In I_PF_AdditiveModel_Impl m_noiseModel :" << m_particleGeneratingModel->toString() << std::endl;
#endif
    m_additiveModelType = PF_ADDITIVE_MODEL;
}

/*************************************************
*                         DTOR
*************************************************/
I_PF_AdditiveModel_Impl::~I_PF_AdditiveModel_Impl()
{
    if (m_noiseModel != nullptr) {
        /// DON't delete m_noiseModel;
        m_noiseModel = nullptr;
    }

    if (m_particleGeneratingModel != nullptr) {
        /// DON't delete m_particleGeneratingModel;
        m_particleGeneratingModel = nullptr;
    }
}

/*OLD
void I_PF_AdditiveModel_Impl::getModelType(enum IModel::ModelsTypes& modelType) const
{
    modelType = _modelType;
}
*/

std::string I_PF_AdditiveModel_Impl::toString() const
{
    return toStringAllAddends();
}

void I_PF_AdditiveModel_Impl::parsingFailed(std::string msg)
{
    /// TODO!!! we have to decide! Which addend did not get its params?
    std::cout << "NOT implemented I_PF_AdditiveModel_Impl::parsingFailed!" << std::endl;
}

/// Invariant for every model
/*OLD REDUNDANT
void I_PF_AdditiveModel_Impl::setTask(Task* const & task)
{
    m_noiseModel->setTask(task);
    m_particleGeneratingModel->setTask(task);
    _task = task;
}
*/

/** Invariant for every model */
void I_PF_AdditiveModel_Impl::setLog(Logger * const & logger)
{
    // OLD    m_noiseModel->setLog(logger);
    // OLD    m_particleGeneratingModel->setLog(logger);
    m_logger = logger;
}

/* TODO!!!
int I_PF_AdditiveModel_Impl::createEmptyParamsOfModel(size_t& size, IParams** ptrToPtrIParams) const
{
  std::cout << "NOT implemented I_PF_AdditiveModel_Impl::parseAndCreateModelParams!" << std::endl;

  I_PF_AdditiveModelParams* params = I_PF_AdditiveModelParams::parseAndCreateParams((const I_PF_AdditiveModel*)this,
mapParams, postfix);
  return (IParamsUnion*)params;

}
*/

/// TODO!!! parseModelParams ��� ������� ������ ����� ���� �����,
/// TODO!!! � ��� ���������� - ��������� ������ parseModelParams ���������
/** ���������� ������ ����� ���� IParamsUnion */
int I_PF_AdditiveModel_Impl::parseAndCreateModelParams(std::map<std::string, std::string> & mapParams,
                                                       std::string postfix, IParamsUnion ** ptrToPtrIParamsUnion) const
{
    std::cout << "NOT implemented I_PF_AdditiveModel_Impl::parseAndCreateModelParams!" << std::endl;
    /*
    ///    I_PF_AdditiveModelParams* params = I_PF_AdditiveModelParams::parseAndCreateParams((const
    I_PF_AdditiveModel*)this, mapParams, postfix);
    //IParamsUnion* createAdditiveModelParams(size_t totalOfAddends, const IParams** addends)

        size_t size=0;
        int rc = getTotalOfAddends(size);
        IParams* addendParams[size];
        IParams** ptrAddendParams = addendParams;

        /// the clones/copies of existing m_addends retrieved
        IAdditiveModel_Impl * ptrIAdditiveModel = (IAdditiveModel_Impl *)this;
          if (rc)
        {
        }
    ///TODO!! Hard-code!
    //    addends[0] = noiseModel->parseAndCreateModelParams(mapParams, postfix);
    //    addends[1] = processModel->parseAndCreateModelParams(mapParams, postfix);

        I_PF_AdditiveModelParams* params = IParamsUnion::createAdditiveModelParams(size, (const
    IParams**)ptrAddendParams);
    ///TODO!!! ��������� ������ �� ������ �� ������ ��� ���������� ���������� ������!
    /// � ������ ���� ���� �� ��������� �� ��������! ��� ������ ��������� ��, �� ������ ����� ����������� ���������
    /// � ��� ��� ������ ������� ������ I_PF_AdditiveModelParams::parseParams(this, map<std::string, std::string>&
    mapParams, std::string postfix);
    //    params->noiseParams = noiseModel->parseAndCreateModelParams(mapParams, postfix);
    //    params->processParams = processModel->parseAndCreateModelParams(mapParams, postfix);
        return (IParamsUnion*)params;
    */
    return _ERROR_WRONG_WORKFLOW_;
}

IParams * I_PF_AdditiveModel_Impl::getPtrToNoiseParams(const IParamsUnion *& ptrToIParamsUnion) const
{
    if (!ptrToIParamsUnion) {
        return nullptr;
    }

    auto totalOfItems = ptrToIParamsUnion->getTotalOfItemsInParamsUnion();
    if (totalOfItems != I_PF_AdditiveModel::dim_I_PF_AdditiveModel) {
        return nullptr;
    }

    auto model = dynamic_cast<IModel *>(m_noiseModel);
    if (!model) {
        return nullptr;
    }

    auto ptrToParams = ptrToIParamsUnion->ptrIParamsUnionToPtrIParams(_noiseModelIndex);
    if (model->canCastModelParams(ptrToParams)) {
        return ptrToParams;
    }

    return nullptr;
}

IParams * I_PF_AdditiveModel_Impl::getPtrToParticleGeneratingParams(const IParamsUnion *& ptrToIParamsUnion) const
{
    if (!ptrToIParamsUnion) {
        return nullptr;
    }

    auto totalOfItems = ptrToIParamsUnion->getTotalOfItemsInParamsUnion();
    if (totalOfItems != I_PF_AdditiveModel::dim_I_PF_AdditiveModel) {
        return nullptr;
    }

    auto model = dynamic_cast<IModel *>(m_particleGeneratingModel);
    if (!model) {
        return nullptr;
    }

    auto ptrToParams = ptrToIParamsUnion->ptrIParamsUnionToPtrIParams(_particleGeneratingModelIndex);
    if (model->canCastModelParams(ptrToParams)) {
        return ptrToParams;
    }

    return nullptr;
}

/* USING IAdditiveModel_Impl so far
bool I_PF_AdditiveModel_Impl::canCastModelParams(IParamsUnion * params) const
{
    return false;
}
*/
/// non-invariant for every model
/* TODO!!!
I_PF_AdditiveModelParams* I_PF_AdditiveModel_Impl::makeEmptyModel()
{
///TODO!!! ����� �� ������. �� this, ������ ���������� ��������� � �� ����!
///TODO!!! PF_Params* params = new PF_Params();

    params->noiseParams = noiseModel->makeEmptyParamsOfModel();
    params->processParams = processModel->makeEmptyParamsOfModel();

    return params;
}
*/

/// I_PF_Model Interface requires IParamsUnion*
/// I_PF_AdditiveModelParams MUST provide
/// IParams* noiseParams
/// IParams* processParams
/// but models work with single IParams

I_PF_Model const * I_PF_AdditiveModel_Impl::getNoiseModel() const
{
    return static_cast<I_PF_Model const *>(m_noiseModel);
}

I_PF_Model const * I_PF_AdditiveModel_Impl::getParticleGeneratingModel() const
{
    return static_cast<I_PF_Model const *>(m_particleGeneratingModel);
}

int I_PF_AdditiveModel_Impl::CDF(double x, int indexOfParams, const IParamsUnion * const h, double & valueOfCDF,
                                 unsigned t) const
{
    /// TODO!!!
    /// �����, � ����������� �� ��������� indexOfParams ���� ������� CDF ���������������� ����������,
    /// ����, ���� ������� ���� ������ �������������, ������� CDF ��� ���� �����!!! �� ��� ��� ���!
    return _ERROR_WRONG_WORKFLOW_;
}

int I_PF_AdditiveModel_Impl::PDF(double x, int indexOfParams, const IParamsUnion * const h, double & valueOfPDF,
                                 unsigned t) const
{
    /// TODO!!!
    /// �����, � ����������� �� ��������� indexOfParams ���� ������� PDF ���������������� ����������,
    /// ����, ���� ������� ���� ������ �������������, ������� PDF ��� ���� �����!!! �� ��� ��� ���!
    return _ERROR_WRONG_WORKFLOW_;
}

int I_PF_AdditiveModel_Impl::getMaxMode(int indexOfParams, const IParamsUnion * const h, double & valueOfMaxMode) const
{
    /// TODO!!!
    /// �����, � ����������� �� ��������� indexOfParams ������� getMaxMode ���������������� ����������,
    /// ������ ������������� �� ����� ������! �.�. ������������ �������� - ������,
    return _ERROR_WRONG_WORKFLOW_;
}

int I_PF_AdditiveModel_Impl::generateRV(int indexOfParams, const IParamsUnion * const h, double & randomValue,
                                        unsigned t) const
{
    /// TODO!!!
    /// �����, � ����������� �� ��������� indexOfParams ���� ������� generateRV ���������������� ����������,
    /// ����, ���� ������� ���� ������
    /// I_Probabilistic_Model::GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL,
    /// ������� generateRV ��� ���� �����!!!
    return _ERROR_WRONG_WORKFLOW_;
}

int I_PF_AdditiveModel_Impl::generateSample(int indexOfParams, const IParamsUnion * const h, double *& sampleValues,
                                            size_t sampleSize, IParamsUnion * pointEstimatesOfParams,
                                            IParamsUnion * intervalEstimatesOfParams[2]) const
{
    if (!sampleValues || !sampleSize) {
        m_logger->error(ALGORITHM_MODELDATA_ERROR,
                        "Missing params to put sample of Gaussian model in generateSample()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** ���� ���� ������ GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL,
    ������� generateSample ��� ���� ��������� � ���� ���������� ������!
    */
    /** GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL means ALL addends */

    if (indexOfParams == I_Probabilistic_Model::GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL) {
        auto tmpSample = new double[sampleSize];
        if (!tmpSample) {
            return _ERROR_NO_ROOM_;
        }
        for (size_t i = 0; i < m_totalOfAddends; ++i) {
            std::memset(tmpSample, 0, sampleSize * sizeof(double));
            auto ptrPM = dynamic_cast<I_Probabilistic_Model *>(m_addends[i]);
            if (!ptrPM) {
                delete[] tmpSample;
                return _ERROR_NO_MODEL_;
            }

            auto rc =
                ptrPM->generateSample(i, h, tmpSample, sampleSize, pointEstimatesOfParams, intervalEstimatesOfParams);

            if (rc != _RC_SUCCESS_) {
                delete[] tmpSample;
                return rc;
            }

            for (auto j = 0u; j < sampleSize; ++j) {
                sampleValues[j] += tmpSample[j];
            }
        }

        delete[] tmpSample;
    }
    /// TODO!!! index != I_Probabilistic_Model::GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL
    /// �����, � ����������� �� ��������� indexOfParams ���� ������� generateRV ���������������� ����������,
    return _RC_SUCCESS_;
}

/** � ��������� ������������ ������������ ��� I_PF_Model ���������� ������
   �������� IParamsUnion* h
*/
double I_PF_AdditiveModel_Impl::estimatePointMove(size_t indexOfParams, IParamsUnion * h, double move,
                                                  unsigned t) const
{
    /// TODO!!! ?�� ����� ������� estimatePointMove �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    std::cout << "NOT Implemented  estimatePointMove() in I_PF_AdditiveModel_Impl model" << std::endl;
    /// TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimatePointMove() in I_PF_AdditiveModel_Impl model");
    return NAN;
}
/// TODO!!! ����� �� ����� ������� estimateHypothesis �� I_PF_Model ���������� ������
double I_PF_AdditiveModel_Impl::estimateHypothesis(size_t indexOfNoiseParams, IParamsUnion * h, double point,
                                                   std::vector<double> & particles, unsigned t) const
{
    // OLD    I_PF_AdditiveModelParams* hyp = (I_PF_AdditiveModelParams*)h;
    // OLD    return m_noiseModel->estimateHypothesis(indexOfParams, hyp->noiseParams, point, particles, t);
    return m_noiseModel->estimateHypothesis(indexOfNoiseParams, h, point, particles, t);
}

/// TODO!!! ����� �� ����� ������� initParticles �� I_PF_Model ���������� ������
int I_PF_AdditiveModel_Impl::initParticles(size_t indexOfParticleGeneratingParams, IParamsUnion * h,
                                           std::vector<double> & particles) const
{
    // OLD    I_PF_AdditiveModelParams* hyp = (I_PF_AdditiveModelParams*)h;
    // OLD    m_particleGeneratingModel->initParticles(indexOfParams, hyp->processParams, particles);
    m_particleGeneratingModel->initParticles(indexOfParticleGeneratingParams, h, particles);
    return _RC_SUCCESS_;
}

/// TODO!!! ����� �� ����� ������� generateParticles �� I_PF_Model ���������� ������
int I_PF_AdditiveModel_Impl::generateParticles(size_t indexOfParticleGeneratingParams, IParamsUnion * h,
                                               std::vector<double> & particles) const
{
    // OLD    I_PF_AdditiveModelParams* hyp = (I_PF_AdditiveModelParams*)h;
    // OLD    m_particleGeneratingModel->generateParticles(0, hyp->processParams, particles);
    m_particleGeneratingModel->generateParticles(indexOfParticleGeneratingParams, h, particles);
    return _RC_SUCCESS_;
}

/**************************************************
*  Static factory to create I_PF_AdditiveModel models
***************************************************/
/// NB ������ ������������� ���� ���������� � ���������� I_PF_Model ���������� ������!
///�.�. ��� ���������� ������, ��� �� �������������,
///���� ����������� � ������ ����������, �� ������������������� ��� ��������� ����������� �� ��������� ������ addends[0]
///� addends[1]
///����!
/// ����� ����� ��� ������ PF-���������� ������� setModel(),
/// �� ��� ��������� ����� ������ ������ setModel() ������� public ����� I_PF_Model ���������� ������,
/// � ����������� ����� ������� ������������� ���� ����������!
/// ����� ���� ���� �� ���� �� �����! ������� ���������� ������ ������ ����� ��� ��������� ������� ����� �����!
/// ���� ��������� ������ ������ ��� ������ setModel(), ������ ��� ��������� �� �� ������ (�� ����������, �� �����������
/// I_PF_Model, ...)
I_PF_AdditiveModel * I_PF_AdditiveModel::createModel(std::string noiseModelName,
                                                     std::string particleGeneratingModelName, Logger * const logger)
{
    auto addends = new enum IModel::ModelsTypes[dim_I_PF_AdditiveModel];
    addends[0] = IModel::getModelTypeByModelName(noiseModelName.c_str());
    addends[1] = IModel::getModelTypeByModelName(particleGeneratingModelName.c_str());
    /** �������� ���������� ������ � PF-����������� � ��������, ��� �� ��������� ���� ��������� ���� ���������
    */
    /// TODO!!! ����� �������������� ������� ���� �����������! �� ����������� ����! I_PF_AdditiveModel_Impl*
    /// additiveModel = new I_PF_AdditiveModel_Impl(dim_I_PF_AdditiveModel, (const enum IModel::ModelsTypes*)addends,
    /// logger);
    I_PF_AdditiveModel_Impl * additiveModel = new I_PF_AdditiveModel_Impl(addends, logger);

    delete[] addends;

    for (size_t i = 0; i < dim_I_PF_AdditiveModel; ++i) {
        I_PF_Model * model = nullptr;
        additiveModel->getPtrToAddend(i, reinterpret_cast<IModel *&>(model));
        if (dynamic_cast<I_PF_Model *>(model) == nullptr) {
            delete additiveModel;
            logger->error(ALGORITHM_MODELDATA_ERROR, "Can not cast AdditiveModel addend to I_PF_Model*.");
            return nullptr;
        }
    }
    /** additiveModel->m_noiseModel, additiveModel->m_particleGeneratingModel
    ��� ������������������� ���� � CTOR'�
    */
    return additiveModel;
} /// end static factory I_PF_AdditiveModel* I_PF_AdditiveModel::createModel()

/**************************************************
*  Static  Comparator to compare  IAdditiveModel models
***************************************************/
bool IAdditiveModel::compareModels(const IAdditiveModel * model_1, const IAdditiveModel * model_2)
{
    if (!model_1 || !model_2) {
        return false;
    }

    auto totalAddends_1 = 0u;
    auto totalAddends_2 = 0u;

    model_1->getTotalOfAddends(totalAddends_1);
    model_2->getTotalOfAddends(totalAddends_2);

    if (totalAddends_1 != totalAddends_2) {
        return false;
    }

    for (auto i = 0u; i < totalAddends_1; ++i) {
        enum IModel::ModelsTypes mt_1;
        model_1->getAddendType(i, mt_1);
        enum IModel::ModelsTypes mt_2;
        model_2->getAddendType(i, mt_2);
        if (mt_1 != mt_2) {
            return false;
        }
    }

    return true;
} /// end static bool IAdditiveModel::compareModels()

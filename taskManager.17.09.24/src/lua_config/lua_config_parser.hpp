#ifndef LUA_CONFIG_PARSER_HPP_INCLUDED
#define LUA_CONFIG_PARSER_HPP_INCLUDED

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include <luabridge.h>

#include <string>

class LuaConfigParser
{
public:
    LuaConfigParser(const std::string & filename)
        : cfg_name(filename)
        , L(luaL_newstate())
    {
        luaL_openlibs(L);
    }

    ~LuaConfigParser()
    {
        lua_close(L);
    }

    void bail(const char *msg) const {
        fprintf(stderr, "\nFATAL ERROR:\n  %s: %s\n\n",
            msg, lua_tostring(L, -1));
    }

    bool open_file() const 
    {
        if (luaL_loadfile(L, cfg_name.c_str())) {
            /* Load but don't run the Lua script */
            /* Error out if file can't be read */
            bail("luaL_loadfile() failed");
            return false;
        }

        if (lua_pcall(L, 0, 0, 0)) {
            /* Run the loaded Lua script */
            /* Error out if Lua file has an error */
            bail("lua_pcall() failed");
            return false;
        }
        return true;
    }

    template <typename T>
    T get(char const * name_param) const
    {
        luabridge::LuaRef s = luabridge::getGlobal(L, name_param);
        if (s.isNil()) {
            printf("Variable not found!\n");
            return T();
        }

        return s.cast<T>();
    }

private:
    std::string cfg_name;
    lua_State* L;

};

#endif

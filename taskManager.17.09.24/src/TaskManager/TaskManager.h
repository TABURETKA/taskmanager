#ifndef __TASK_MANAGER_H__633569422542968750
#define __TASK_MANAGER_H__633569422542968750

#include <string>
#include <map>
#include <list>
#include <thread>
#include <vector>
#include "mutex.h"
#include "../DataCode/DataManager.h"

#define DEFAULT_TASKMANAGER_INI_FILENAME "taskManager.ini"

// DON't pollute global scopr! using namespace std;
/** forward declaration */
class Task;

class TaskManager
{
public:
    enum TaskManagerParams{
        TASK_MANAGER_MODE,
        NUMBER_OF_PARALLEL_TASK,
        NUMBER_OF_OMP_THREADS,
        TIMER_INTERVAL_TO_BACKUP,
        TASK_LIST,
        FAKE_PARAMS_OF_TASK_MANAGER
    };
    static const char* grammarToParseParams[TaskManagerParams::FAKE_PARAMS_OF_TASK_MANAGER];
    TaskManager();
    ~TaskManager();

    void run();
    void taskFinished(int threadId, std::string finalMessage);
    bool fixOnly();
    DataManager* getDataManager() const;

private:
    enum TaskManagerModes{
        INTERACTIVE,
        BATCH,
        FAKE_MODE
    };
    TaskManagerModes _mode;
    bool isExit(const std::string& command);
    void parseCommand(const std::string& command);
    void printHelp();
    void printStat();
    void getTaskName();
    void startTask(Task* pTask);
    void backup(bool byPause);
    void resume();
    /** addTaskToTasksQueue ONLY create and add new Task to mapTask map
    */
    void addTaskToTasksQueue(const std::string& taskCfgFName, const std::string& taskName);
    void startTasksFromTasksQueue(size_t howManyStart = 0);
    void configureTaskManager();
    void loadTaskList();
    bool loadPaused();
    void startBackupTimer();

    static DWORD WINAPI backupTimerProc( LPVOID lpParam );

    std::map<std::string, Task *> _mapTasks;
    std::map<int, Task *> _mapRunningTasks;
    std::list<Task*> _taskQueue;

    int _numberOfRunningTasks;
    int _limitOfRunningTasks;
    unsigned _timerPeriodMinutes;

    int _ompProcNum;
//OLD    bool fixOnlyMode;

//OLD    mutex taskQueueMutex;
   Mutex _taskQueueMutex;

/** ISO C++ requires const int SpikeTailLengthMin to be static */
    std::string _resumeFileName; // = "resume.ini";
    std::string _taskListFileName;
//OLD    std::string estConfigFileName;
    DataManager* _dataManager;


};
#endif

#include <iostream>
#include <string>
#include <sstream>

#include <stdio.h>
#include <sys/stat.h>
#include "mutex.h"
#include <thread> /* sleep_for */
#include "TaskManager.h"
#include "../CommonStuff/Defines.h" /** delimiter macros */
#include "../StringStuff/StringStuff.h"
#include "../Task/Task.h"
#include "../Config/ConfigParser.h"
/** static in TaskManager */
const char* TaskManager::grammarToParseParams[TaskManagerParams::FAKE_PARAMS_OF_TASK_MANAGER]={
        "TASK_MANAGER_MODE",
        "NUMBER_OF_PARALLEL_TASK",
        "NUMBER_OF_OMP_THREADS",
        "TIMER_INTERVAL_TO_BACKUP",
        "TASK_LIST"
};


/*****************************************
*                 CTOR
*****************************************/
TaskManager::TaskManager() : _mode(TaskManagerModes::FAKE_MODE),
    _numberOfRunningTasks(0),
    _limitOfRunningTasks(1),
    _timerPeriodMinutes(OUR_MAX_UNSIGNED_INT),
    _ompProcNum(1),

    _resumeFileName("resume.ini") //OLD fixOnlyMode(false),  , estConfigFileName("config.ini")
{
    ///TODO!!! Hard-coded resume-file name
    this->_dataManager = new DataManager();
}

TaskManager::~TaskManager()
{
    /**
    delete maps with Tasks stuff
    */
    std::map<int, Task *>::iterator itr = _mapRunningTasks.begin();
    while (itr != _mapRunningTasks.end())
    {
        std::map<int, Task *>::iterator itrToErase = itr;
        ++itr;
        Task* task = itrToErase->second;
        std::map<std::string, Task *>::iterator it = _mapTasks.find(task->getTaskName());
        if (it != _mapTasks.end())
        {
            _mapTasks.erase(it);
        }
        delete task;
        _mapRunningTasks.erase(itrToErase);
    }

    /**
    delete repository
    */
    delete this->_dataManager;

}

void TaskManager::run()
{
    configureTaskManager();
    /** process TASK_LIST directive if exists in taskManager.ini
     Creates and adds Tasks to mapTask map.
     Fill the taskQueue with tasks to be started
    */
    loadTaskList();
    /**
    run Tasks from taskQueue up to _limitOfRunningTasks
    */
    startTasksFromTasksQueue(_limitOfRunningTasks);

    std::string command;

    if(_mode == INTERACTIVE)
    {
        while (1)
        {
            std::cout << "> ";
            std::cin >> command;
            std::cin.clear();
            toLower(command);
            if (isExit(command))
                break;

            parseCommand(command);
        }
    }
    else if(_mode == BATCH)
    {
/*
        _hThreadArray = new HANDLE[_numberOfRunningTasks];
///TODO!!! what if _ERROR_NO_ROOM_ ????
        int i=0;
        for (auto itTask = _mapRunningTasks.begin(); (i < _numberOfRunningTasks) && (itTask != _mapRunningTasks.end()); ++itTask)
        {
            Task* pTask = itTask->second;
            if ((pTask) && (pTask->_threadHandle))
            {
                _hThreadArray[i++] = pTask->_threadHandle;
                std::cout << "Add thread : " << _hThreadArray[i] << " to wait for." << std::endl;
            }
        }
       std::cout << "Hangs before WaitForMultipleObjects" << std::endl;
       /
       * Wait until all threads have terminated.
       /
       WaitForMultipleObjects(_numberOfRunningTasks, _hThreadArray, TRUE, INFINITE);
       std::cout << "Hangs after WaitForMultipleObjects" << std::endl;
*/
std::cout << "Hangs before while(_numberOfRunningTasks)" << std::endl;
      while(_numberOfRunningTasks)
      {
          /** dummy assign merely to start Task thread */
          int i= 0 + 1;
      }
    } else {
        std::cout << "Unknown TaskManager mode! Can not start!" << std::endl;
    }
}

void TaskManager::taskFinished(int threadId, std::string finalMessage)
{
    _taskQueueMutex.lock();
    if ( ! _mapRunningTasks.count(threadId) )
    {
        std::cout << std::endl << "Thread exit: " << threadId << std::endl << "Result: " << finalMessage << std::endl << "> ";
        return;
    }
    Task* task = _mapRunningTasks[threadId];
    std::cout << std::endl << "Task finished: " << task->getTaskName() << std::endl << "Result: " << finalMessage << std::endl << "> ";
/*OLD
    _mapRunningTasks.erase(threadId);

    delete task;

    --_numberOfRunningTasks;
*/
/** So far wE postpone delete task until TaskManager's DTOR is called
    std::map<int, Task *>::iterator itr = _mapRunningTasks.find(threadId);
    if (itr != _mapRunningTasks.end())
    {
        Task* task = itr->second;
        std::map<std::string, Task *>::iterator it = _mapTasks.find(task->getTaskName());
        if (it != _mapTasks.end())
        {
            _mapTasks.erase(it);
        }
///TODO!!! ������ ������� ��������� ����� � ���� �����! �� ������ � ���� ����� ��������������� �� ������ ����� ���������� Task'�:
/// TaskManager->startTask =>
/// => thread with Task::startTask => into startTask we call processTask => into processTask we call configureTask/ => into configureTask  we call Task::finish (into Task::finish we call this->taskFinished!)
///BUG HERE!        delete itr->second;
        _mapRunningTasks.erase(itr);
    }
*/
    --_numberOfRunningTasks;

    if (! _taskQueue.empty())
    {
        std::cout << std::endl << "Start task " << _taskQueue.front()->getTaskName() << std::endl;
        startTask(_taskQueue.front());
        _taskQueue.pop_front();
        std::cout << "> ";
    }
    if (! _numberOfRunningTasks)
    {
        remove(_resumeFileName.c_str());
    }
    _taskQueueMutex.unlock();
}
/* OLD
bool TaskManager::fixOnly()
{
    return fixOnlyMode;
}
*/

DataManager* TaskManager::getDataManager() const
{
    return this->_dataManager;
}


bool TaskManager::isExit(const std::string& command)
{
    return (command == "exit");
}

void TaskManager::parseCommand(const std::string& command)
{
///TODO!!! put magic "commands" into TaskManager cmd Interpretator
    if (command == "help")
        return printHelp();
    if (command == "start")
        return getTaskName();
    if (command == "stat")
        return printStat();
    if (command == "pause")
        return backup(true);
    if (command == "resume")
        return resume();
    else
    {
        std::cout << "unknown command" << std::endl;
    }
}

void TaskManager::printHelp()
{
    std::cout <<
              "help    print this info" << std::endl <<
              "start   start task" << std::endl <<
              "stat    show task states" << std::endl <<
              "pause   pause tasks and save their states" << std::endl <<
              "resume  resume paused tasks" << std::endl <<
              "exit    stop EsimatorTaskManager with all tasks" << std::endl;
}

void TaskManager::printStat()
{
    std::cout << "Id\tName\tState" << std::endl;
    for (auto i = _mapTasks.begin(); i != _mapTasks.end(); ++i)
    {
        std::cout << i->second->getThreadId() << "\t" << i->second->getTaskName() << "\t" << i->second->getTaskStateString() << std::endl;
    }
}

void TaskManager::getTaskName()
{
    std::string taskCfg;
    std::cin >> taskCfg;

    std::string taskName;
    std::cin >> taskName;

    addTaskToTasksQueue(taskCfg, taskName);
}

void TaskManager::startTasksFromTasksQueue(size_t howManyStart)
{
    if(0 == howManyStart)
    {
        howManyStart = _limitOfRunningTasks;
    }
    while (_numberOfRunningTasks < (int)howManyStart)
    {
        if (_taskQueue.empty())
        {
            return;
        }
        _taskQueueMutex.lock();
        std::cout << std::endl << "Start task " << _taskQueue.front()->getTaskName() << std::endl;
        startTask(_taskQueue.front());
        _taskQueue.pop_front();
        std::cout << "> ";
        /** side-effect in startTask
        if success ++_numberOfRunningTasks
         */
        _taskQueueMutex.unlock();
    }
std::cout << "Create ALL thread success" << std::endl;
}

void TaskManager::addTaskToTasksQueue(const std::string& taskCfgFName, const std::string& taskName)
{
    if (_mapTasks.count(taskName) > 0)
    {
        std::cout << "Task \"" << taskName << "\" already exists" << std::endl;
        return;
    }

    std::cout << "Create task: " << taskName << std::endl;

    Task* pTask = new Task();
    pTask->setConfig(taskCfgFName);
    pTask->setTaskName(taskName);
    pTask->setTaskManager(this);

    _taskQueueMutex.lock();
    _mapTasks[taskName] = pTask;
    _taskQueue.push_back(pTask);
    _taskQueueMutex.unlock();
}

void TaskManager::startTask(Task* pTask)
{
    DWORD threadId = 0;
    HANDLE handle = 0;
//    HANDLE handle = CreateThread(NULL, 0, Task::startTask, pTask, 0, &threadId);
//    if (handle == NULL)
//    {
//        std::cout << "Create thread failed" << std::endl << "> ";
//        return;
//    }
    pTask->_threadHandle = handle;
    pTask->_threadId = threadId;

    pTask->processTask();

    std::cout << "Thread id: " << threadId << std::endl;
    _mapRunningTasks[static_cast<int>(threadId)] = pTask;

//    ++_numberOfRunningTasks;
}

void TaskManager::backup(bool byPause)
{
    if (_numberOfRunningTasks == 0)
    {
        remove(_resumeFileName.c_str());
        return;
    }
    std::ofstream file(_resumeFileName.c_str());
    if (!file.is_open())
    {
        std::cout << "can not create file" << std::endl;
    }

    file << "NUM_PAUSED=" << _numberOfRunningTasks << std::endl << std::endl;

    int taskId = 0;
    for (auto itTask = _mapRunningTasks.begin(); itTask != _mapRunningTasks.end(); ++itTask)
    {
        if (byPause)
        {
            itTask->second->pause();
            std::cout << "Pause task " << itTask->second->getTaskName() << std::endl;
        }
        else
        {
            itTask->second->backup();
        }
///TODO!!! ��� ��-�� ����, ��� ������������ ���������� �� ���, ��� ��� ��������,
///�� ����� ��� ���� (� ����� ��� � �� ����)!
///����� ������ ������ ���� ���������, ���� (� ������� ������) ����!
///������ ����� ����� �������� ��� ����� ("resume.ini") � ����� ���� ����� ���� ���� �����!
///������������ � �������� �� ������ ����� � ��������� ����������� ������!
///��� ���� ���� �����, ���� ����������!
        file << "TASK_PATH_" << taskId << "=" << itTask->second->getConfig() << std::endl;
        file << "TASK_NAME_" << taskId << "=" << itTask->second->getTaskName() << std::endl;
///TODO!!! BUG because right after estimator thread has been paused the runEstimator code INCREMENTS "trackId"! So the WRONG "trackId" is stored into "resume.ini"!
///TODO!!!         file << "TRACK_ID_" << taskId << "=" << itTask->second->getTrackId() << endl;
///TODO!!!         file << "POINT_ID_" << taskId << "=" << itTask->second->getPointId() << endl << endl;

        ++taskId;
    }

    file << "NUM_PAUSED_QUEUE=" << _taskQueue.size() << std::endl << std::endl;

    taskId = 0;
    for (auto itTask = _taskQueue.begin(); itTask != _taskQueue.end(); ++itTask)
    {
        if (byPause)
        {
            std::cout << "Backup in queue task " << (*itTask)->getTaskName() << std::endl;
        }

        file << "TASK_QUEUE_PATH_" << taskId << "=" << (*itTask)->getConfig() << std::endl;
        file << "TASK_QUEUE_NAME_" << taskId << "=" << (*itTask)->getTaskName() << std::endl << std::endl;

        ++taskId;
    }
    file.close();
}

void TaskManager::resume()
{
    std::cout << "Resume tasks:" << std::endl;

    for (auto iTask = _mapTasks.begin(); iTask != _mapTasks.end(); ++iTask)
    {
        Task* task = iTask->second;
        if (task->isPaused())
        {
            std::cout << task->getTaskName() << "  " << iTask->second->getTaskStateString() << std::endl;
            startTask(iTask->second);
            --_numberOfRunningTasks;
        }
    }

    remove(_resumeFileName.c_str());
}

void TaskManager::configureTaskManager()
{
    ///TODO!!! Hard-coded taskManager ini-file name
    std::string iniFilename = DEFAULT_TASKMANAGER_INI_FILENAME;
    ConfigParser configParser(iniFilename);
    if (!configParser.fileExists())
    {
        std::cout << iniFilename + " ini-file is not set" << std::endl;
        return;
    }

    auto mapParams = configParser.parse();
    ///TODO!!! Loop through grammarToParseParams array of TaskManagerParams
    auto itParam = mapParams.find(grammarToParseParams[TASK_MANAGER_MODE]);
    int param;
    if (itParam != mapParams.end())
    {
        param = atoi(itParam->second.c_str());
        if ((param >= 0) && (param < TaskManagerModes::FAKE_MODE))
        {
            _mode = (enum TaskManager::TaskManagerModes)param;
        } else {
            std::cout << "Wrong TaskManager mode in :" << iniFilename << std::endl;
            return;
        }
    } else {
        std::cout << "Failed to init TaskManager mode from :" << iniFilename << std::endl;
        return;
    }

    itParam = mapParams.find(grammarToParseParams[NUMBER_OF_PARALLEL_TASK]);
    if (itParam != mapParams.end())
    {
        param = atoi(itParam->second.c_str());
        if (param > 0)
        {
            _limitOfRunningTasks = param;
        }
    }
    itParam = mapParams.find(grammarToParseParams[NUMBER_OF_OMP_THREADS]);
    if (itParam != mapParams.end())
    {
        param = atoi(itParam->second.c_str());
        if (param > 0)
        {
            _ompProcNum = param;
        }
    }
    itParam = mapParams.find(grammarToParseParams[TIMER_INTERVAL_TO_BACKUP]);
    if (itParam != mapParams.end())
    {
        param = atoi(itParam->second.c_str());
        if (param > 0)
        {
            _timerPeriodMinutes = param;
            startBackupTimer();
        }
    }

    itParam = mapParams.find(grammarToParseParams[TASK_LIST]);
    if (itParam != mapParams.end())
    {
        _taskListFileName = itParam->second;
    }
    /*OLD REDUNDANT DIRECTIVES in TASKMANAGER.INI!
        itParam = mapParams.find("MODE");
        if (itParam != mapParams.end() && itParam->second == "FIX_ONLY") {
            fixOnlyMode = true;
            cout << "Start FIX_ONLY mode" << endl;
        }

        itParam = mapParams.find("EST_CONFIG_FILE");
        if (itParam != mapParams.end()) {
            param = atoi(itParam->second.c_str());
            if (param > 0) {
                estConfigFileName = param;
            }
        }
    */
}

void TaskManager::loadTaskList()
{
    if (loadPaused())
        return;

    if (_taskListFileName.empty())
        return;

    std::ifstream tasklist(_taskListFileName.c_str());
    if (!tasklist || tasklist.bad())
    {
        std::cout << "Failed to load task list from " << _taskListFileName << std::endl;
        return;
    }
    std::string delimiters=DEFAULT_DELIMITERS;
    while (!tasklist.eof())
    {
        std::string line;
        std::string  config, name;;
///TODO!!! replace >> with tasklist.getline
//        tasklist >> config >> name;
        std::getline(tasklist, line);
        TrimStr(line,delimiters);
        line = removeComment(line, DELIMITER_COMMENT_STARTS_WITH);
        std::istringstream iss(line);
        iss >> config >> name;
        TrimStr(config,delimiters);
        TrimStr(name,delimiters);
        /**
         addTaskToTasksQueue ONLY create and add new Task to mapTask map
         */
        if (!config.empty() && !name.empty())
        {
            addTaskToTasksQueue(config, name);
        }
    }
}

bool TaskManager::loadPaused()
{
    ConfigParser parser(TaskManager::_resumeFileName);
    if (!parser.fileExists())
    {
        return false;
    }
    std::cout << "Estimator was paused" << std::endl;
    auto mapParams = parser.parse();

    auto itParam = mapParams.find("NUM_PAUSED");
    if (itParam == mapParams.end())
    {
        std::cout << "No paused tasks" << std::endl;
        return false;
    }

    unsigned index = atoi(itParam->second.c_str());
    if (index == 0)
    {
        std::cout << "No paused tasks" << std::endl;
        return false;
    }
///TODO!!! put magic literals "TASK_PATH_%i", etc. into TaskManger grammar definition
    char taskCfg[100];
    char taskName[100];
    char trackIdStr[100];
    char pointIdStr[100];
    for (unsigned i = 0; i < index; ++i)
    {
        sprintf(taskCfg, "TASK_PATH_%i", i);
        sprintf(taskName, "TASK_NAME_%i", i);
        sprintf(trackIdStr, "TRACK_ID_%i", i);
        sprintf(pointIdStr, "POINT_ID_%i", i);

        if ( mapParams.find(taskCfg) == mapParams.end() ||
                mapParams.find(taskName) == mapParams.end() ||
                mapParams.find(trackIdStr) == mapParams.end() ||
                mapParams.find(taskCfg) == mapParams.end() )
        {
            std::cout << "Failed to parse paused task " << i << std::endl;
        }
        /** ���� ��������� ���� ����� ����� �������������� if'� ( ��� ������, ��� � ��� �������� � �������� resume.ini!)
        ����� ���������� trackIdStr == TRACK_ID_0 � pointIdStr == POINT_ID_0
        */
        int trackId = atoi(mapParams[trackIdStr].c_str());
        int pointId = atoi(mapParams[pointIdStr].c_str());

        Task* pTask = new Task(PAUSED);
///TODO!!! BUG � ��� ����� ��� ���! �� ������������� ����������� taskCfg �� ���� ������ ( ��� ������, ��� � ��� �������� � �������� resume.ini!) !
        pTask->setConfig(mapParams[taskCfg]);
        pTask->setTaskName(mapParams[taskName]);
        pTask->setTaskStartPoint(trackId, pointId);
        pTask->setTaskManager(this);

        _mapTasks[taskName] = pTask;
        ++_numberOfRunningTasks;
    }

    itParam = mapParams.find("NUM_PAUSED_QUEUE");
    if (itParam == mapParams.end())
    {
        std::cout << "No paused in queue tasks" << std::endl;
        return true;
    }

    index = atoi(itParam->second.c_str());
    if (index == 0)
    {
        return true;
    }

    for (unsigned i = 0; i < index; ++i)
    {
        sprintf(taskCfg, "TASK_QUEUE_PATH_%i", i);
        sprintf(taskName, "TASK_QUEUE_NAME_%i", i);

        if (mapParams.find(taskCfg) == mapParams.end() ||
                mapParams.find(taskName) == mapParams.end() )
        {
            std::cout << "Failed to parse paused in queue task " << i << std::endl;
        }

        Task* pTask = new Task();
///TODO!!! BUG � ��� ����� ��� ���! �� ������������� ����������� taskCfg �� ���� ������ ( ��� ������, ��� � ��� �������� � �������� resume.ini!) !
        pTask->setConfig(mapParams[taskCfg]);
        pTask->setTaskName(mapParams[taskName]);
        pTask->setTaskManager(this);

        _mapTasks[taskName] = pTask;
        _taskQueue.push_back(pTask);
    }

    remove(_resumeFileName.c_str());

    return true;
}

void TaskManager::startBackupTimer()
{
    HANDLE handle = CreateThread(NULL, 0, TaskManager::backupTimerProc, this, 0, 0);
    if (handle == NULL)
    {
        std::cout << "Create thread for timer failed" << std::endl;
        return;
    }
}

DWORD WINAPI TaskManager::backupTimerProc( LPVOID lpParam )
{
    const auto tm = static_cast<TaskManager *>(lpParam);
    const auto period = tm->_timerPeriodMinutes * 60 * 1000;

    while (1)
    {
        Sleep(period);
        tm->backup(false);
    }
}

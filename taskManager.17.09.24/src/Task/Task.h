#ifndef __TASK_H__633569422542968750
#define __TASK_H__633569422542968750

#include <string>
#include <windows.h> /* DWORD WINAPI */

#include "../Log/Log.h"
#include "../Models/Model.h"
#include "../Models/AdditiveModel.h"
#include "../Params/ParamsSet.h"
#include "../Estimators/Estimator.h"
//OLD #include "../DataCode/DataManager.h"//enum RepoDataSemantics
// DON'T pollute global scope! using namespace std;
/** forward declaration */
class TaskManager;
namespace DataGenerators{
    class IDataGenerator;
}
class DataLoader;

enum TaskState
{
    NOT_STARTED,
    PROCESSING,
    PAUSED,
    FINISHED,
    LOG_ERROR,
    CONFIG_FAILED,
    MODEL_ERROR,
    ESTIMATOR_ERROR
};
/// ��� ��������� �� ����������� ������ ���� ����� ����� ���������� ��������������� �� ������!
/// �� ����� ���� ������� ����� ��������� � �����-�� ������ � ���������, � ������� ��� ��������� ������ �������
//OLD const size_t grammarSizeForTaskConfig = 11;

class Task
{
public:
    enum grammarTaskConfigItems
    {
        LOG_LEVEL_ITEM,
        OUTPUT_FILENAME_ITEM,
        DATA_GENERATOR_NAME_ITEM,
        DATA_GENERATOR_PARAMS_SRC_ITEM,
        DATA_LOADER_PARAMS_SRC_ITEM,
        ESTIMATOR_TYPE_ITEM, //"INTEVALESTIMATOR, POINTESTIMATOR
        ESTIMATOR_NAME_ITEM, //"INTEVALPARETO, ...
        ESTIMATOR_PARAMS_SRC_ITEM, // ...
        MODEL_TYPE_ITEM, //SIMPLE ONE, ADDITIVE_MODEL
        MODEL_NAME_ITEM, // PARETO, ...
        MODEL_PARAMS_SRC_ITEM, // ...
        REPO_ENTRIES_ITEM,
        FAKE_TASK_CONFIG_ITEM // entries in repo
    };
    enum taskStepExecutor
    {
        DATA_LOADER    = 1,
        DATA_GENERATOR = 2,
        ESTIMATOR      = 3,
        FAKE_EXECUTOR  = 4
    };
    /** tokens to parse TaskStepConfig map */
    static const char* grammarTaskStepConfig[FAKE_TASK_CONFIG_ITEM];

    int estimatorThreadInterruptSignal;
    Task(TaskState state = NOT_STARTED);
    ~Task();
    void setConfig(std::string taskCfg);
    void setTaskName(std::string taskCfg);
    void setTaskManager(TaskManager* taskManager);
    int getRepoDataForTargetObj(std::vector<std::string> &keys, size_t dimension, DataInfo** const di);
///TODO!!! ������ setTaskStep
    void setTaskStartPoint(unsigned trackId, unsigned pointId);

    std::string getConfig();
    std::string getTaskName();
    bool isPaused();
    std::string getTaskStateString();
    int getThreadId();
    unsigned getTrackId();

///TODO!!! remove dependencies on Windows!
    /** method to startTask in another thread */
    static DWORD WINAPI startTask( LPVOID lpParam );

    void pause();
    void backup();
    void finish(TaskState state, std::string message);

    DWORD _threadId;
    HANDLE _threadHandle;
	void processTask();

private:
    std::string _taskCfg;
    std::string _taskName;

    TaskState _state;

    TaskManager* _ptrTaskManager;
///TODO!!! ������� �� �������! �� ����� ������!
/// ������� ��� ��������� �� ��������� ���������, ������������ TaskManager?
    Logger _log;
    /** Task workflow methods */

    int initLogger();

    int configTaskStep(std::string taskStepCfg);
    int configDataGenerator(const std::map<std::string, std::string>& paramsMap, const std::string& dataGeneratorName);
    int configDataLoader(const std::map<std::string, std::string>& paramsMap, const std::string& taskStepCfg);
    int configEstimator(const std::map<std::string, std::string>& paramsMap, const std::string& taskStepCfg);

    int runExecutor(int rc, ptrInt threadInterruptSignal);
    int runEstimator(ptrInt threadInterruptSignal);
    int runDataGenerator(ptrInt threadInterruptSignal);
    int runDataLoader(ptrInt threadInterruptSignal);

    int saveExecutorResults(enum taskStepExecutor texe);
    int saveResults(enum taskStepExecutor texe);
    int saveDataLoaderResults(std::stringstream & resultLog);
    int saveEstimatorResults(std::stringstream & resultLog);
    void saveNanResults();

    void deleteModelAndExecutor(int rc);
    void deleteModelAndDataGenerator();
    void deleteModelAndEstimator();

    void dumpTaskParams();
    void resumeTaskParams();
///TODO!!! BAD: STL container!
    void setDataToEstimator(std::vector<std::string> &keys);

///TODO!!! ������ stuff ������������ �����������!
///TODO!!! void loadBoundaries(std::string fName);
///TODO!!! ������ ��� ������ ����������� void outputBoundaries(IParams* start, IParams* end);

///TODO!!! DataModel;! ��� ���� � ���!
    unsigned _trackId;

///TODO!!! ������-�� ��� unsigned pointId; - ��� ��������� PF'�, �.�. Task �� ���� pointId ����� ������ �� ������!
///TODO!!! ����� ����� ��� �����������!
/// ��� ������ ��������� ����� ��������! ������� ������� � ���
// OLD    IParams* result;//OLD PF_Params* result;
// ����� �������� ����� DataInfo**

///TODO!!! ������ � ���������! ������-�� ��� probability - ��� ��������� PF'�, �.�. Task �� ���� probability ����� ������ �� ������!
///TODO!!! ����� �������� ����� DataInfo**     double probability;

///TODO!!! ����� ����� ��� �����������!
    IModel* m_model;
    IAdditiveModel* _additiveModel;

///TODO!!! Task workflow data after config parsing!
///OLD!    list<IntervalEstimator*> listBoundEstimator;
///TODO!!!        list<IEstimator*> listOfEstimators;
///TODO!!!    std::string boundOutFName;

    bool _resultsRecorded;
    IEstimator* _estimator; // = NULL;
    DataGenerators::IDataGenerator* _dg;
    DataLoader* _dl;
    std::string _outputFName;

//OLD ����� ���������� ������� (�� ����������) �������� � ������������ �����������  IParamsSet* dataset;
///TODO!!! BAD: STL container! ������ �� ���������� STL!
///TODO!!! DataModel;!
    std::vector< std::vector<double> > tracks;
};
#endif

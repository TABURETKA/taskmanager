//#define _DEBUG_
#include <iostream> /** cout */
#include <sstream> /** stringstream */
#include <fstream> /** ifstream, ofstream */
#include <cfloat>
#include <math.h>

#include "Task.h" /* windows.h already included */
#include "../CommonStuff/Defines.h" /** delimiter macros */
#include "../TaskManager/TaskManager.h"
#include "../Config/ConfigParser.h"
#include "../DataGenerators/DataGenerators.h" /* IDataGenerator */
#include "../DataLoader/DataLoader.h"
#include "../Estimators/Estimator.h"
#include "../Estimators/AdditiveModelEstimator.h"
#include "../DataCode/DataModel/DataMetaInfo.h" /** DataInfo */
#include "../DataCode/Grammar/DataManagerGrammar.h"
#include "../Log/Errors.h" /** errors macros */
/** Following MACROs for DEBUGGING purposes ONLY!
 If release we MUST comment all of them!
*/

const std::string grammarTaskConfig[1]=
{
    "STEP_"
};

/** static in Task class*/
const char* Task::grammarTaskStepConfig[FAKE_TASK_CONFIG_ITEM]=
{
    "LOG_LEVEL",
    "OUTPUT_FILENAME",
    "DATA_GENERATOR_NAME",
    "DATA_GENERATOR_PARAMS_SRC",
    "DATA_LOADER_PARAMS_SRC_ITEM",
    "ESTIMATOR_TYPE", /** "INTEVALESTIMATOR", "POINTESTIMATOR" */
    "ESTIMATOR_NAME", /** "INTEVALPARETO, ... */
    "ESTIMATOR_PARAMS_SRC", // ...
    "MODEL_TYPE", /** "SIMPLE ONE", "ADDITIVE_MODEL" */
    "MODEL_NAME", // PARETO, ...
    "MODEL_PARAMS_SRC", // ...
    "REPO_ENTRIES" // entries in repo
};

/***********************************************
*                  CTOR
***********************************************/
Task::Task(TaskState startState) :
    estimatorThreadInterruptSignal(0x0),
    _threadId(0),
    _state(startState),
    _ptrTaskManager(0),
    _trackId(0),
    m_model(0),
    _additiveModel(0),
    _resultsRecorded(0),
    _estimator(0),
    _dg(0),
    _dl(0)
{}
/***********************************************
*                  DTOR
***********************************************/
Task::~Task()
{
    deleteModelAndEstimator();
}

void Task::setConfig(std::string taskCfg)
{
    this->_taskCfg = taskCfg;
}

void Task::setTaskName(std::string taskName)
{
    this->_taskName = taskName;
}

void Task::setTaskManager(TaskManager* taskManager)
{
    this->_ptrTaskManager = taskManager;
}

void Task::setTaskStartPoint(unsigned rTrackId, unsigned rPointId)
{
//OLD    trackId = rTrackId;
//OLD    pointId = rPointId;
}

std::string Task::getConfig()
{
    return _taskCfg;
}

std::string Task::getTaskName()
{
    return _taskName;
}

bool Task::isPaused()
{
    return (_state == PAUSED);
}

std::string Task::getTaskStateString()
{
    switch (_state)
    {
    case NOT_STARTED:
        return "Task not started";

    case PROCESSING:
    {
///TODO!!! ��� ��-�� ������ ����� � ���������� ��������������� ����� ������!
        std::stringstream state;
        state << "Processing: ";
///TODO!!! � ����� ��������� ������ ����������� ���������, � ����� ������ �� �� �����, ��������� �� ����������� � ������� ������ ����������
        /* OLD
                if (track.size() > 1) {
                    state << trackId << "/" << track.size() << " ";
                }
                state << ((float)(dataset->getCurrentId() * 100.0) / (float)(dataset->getSetSize())) << "%";
        */
        return state.str();
    }

    case PAUSED:
    {
        std::stringstream state;
        state << "Paused: ";
        return state.str();
    }
    /* OLD        if (dataset != 0) {
                std::stringstream state;
                state << "Paused: ";
    ///TODO!!! ������-�� � ����� ��������� ������ ����������� ���������, � ����� ������ �� �� �����, ��������� �� ����������� � ������� ������ ����������
                if (track.size() > 1) {
                    state << trackId << "/" << track.size() << " ";
                }
    //            state << dataset->getCurrentId() * 100 / dataset->getSetSize() << "%";
                state << ((float)(dataset->getCurrentId() * 100.0) / (float)(dataset->getSetSize())) << "%";
                return state.str();
            }
            else {
                return "paused";
            }
    */
    case FINISHED:
        return "Complete";

    case LOG_ERROR:
        return "Logger error";

    case CONFIG_FAILED:
        return "Config failed";

    case MODEL_ERROR:
        return "Model error";

    case ESTIMATOR_ERROR:
        return "Estimator error";
    }

    return "Unknown state";
}

int Task::getThreadId()
{
    return (int)_threadId;
}

unsigned Task::getTrackId()
{
    return _trackId;
}

int Task::initLogger()
{
    std::string path = "";

    unsigned pos = _taskCfg.rfind('/');
    if (pos != std::string::npos)
    {
        path = _taskCfg.substr(0, pos + 1);
    }
    path += _taskName + ".log";

    if (!_log.setLogFile(path))
    {
        finish(LOG_ERROR, "Failed to open log-file");
        return _ERROR_WRONG_ARGUMENT_;
    }
    _log.log(LogLevels::WORKFLOW_TASK_NOTIFICATION, "Log file " + path + " is opened");
    return _RC_SUCCESS_;
}
///TODO!!! ��������� ��������� ���, �� �������� ����� ���������� : ���� pthread, ���� <threads> �� STL
DWORD WINAPI Task::startTask( LPVOID lpParam )
{
    Task* thisTask = (Task*)lpParam;

    thisTask->processTask();

    return _RC_SUCCESS_;
}

void Task::pause()
{
    if (_state == PAUSED)
    {
        return;
    }
    _state = PAUSED;
    _threadId = 0;
    dumpTaskParams();
///TODO!!! ��� ����� �� ������-�� �� ����� ������������ ���������� ����� SIR-����������!
//    TerminateThread(threadHandle, 0);
    // Set estimatorThreadInterruptSignal to signal
    estimatorThreadInterruptSignal = 0x1;
///TODO!!! ����� ��� �����-�� �������, timer
/// � DEFAULT_THREAD_WAITING_TIMEOUT ����� �������� ����� �� ����-����
    while(estimatorThreadInterruptSignal);
}

void Task::backup()
{
    if (_state == PAUSED)
    {
        return;
    }
    dumpTaskParams();
}

void Task::finish(TaskState state, std::string message)
{
    _log.finish();
    deleteModelAndEstimator();
    this->_state = state;

    if (_ptrTaskManager != 0)
    {
        _ptrTaskManager->taskFinished((int)_threadId, message);
    }
    _threadId = 0;
///BUG!!!    TerminateThread(_threadHandle, 0);
}

void Task::processTask()
{
    int rc = initLogger();
    if(_RC_SUCCESS_ != rc) return;
    /** Environment adjustment */
    _log.log( WORKFLOW_TASK_NOTIFICATION, "Start parsing task config :" + _taskCfg);
    /** ������� ������ �������
    � ������ ��� ��� �������,
    ����� �������� ���
    */
    ConfigParser parser(_taskCfg);
#ifdef _DEBUG_
    std::cout<<std::endl<< "DEBUG:" <<_taskCfg<<std::endl;
#endif
    if (!parser.fileExists())
    {
        _log.error(WORKFLOW_TASKMANAGER_ERROR, "Task config " + _taskCfg + " does not exist!");
        finish(CONFIG_FAILED, "Task config " + _taskCfg + " does not exist!");
        return;
    }
///BUG with "auto"??? auto mapParams = parser.parse();
    std::map<std::string,std::string> mapParams = parser.parse();
    /** ����� �� ���� taskSteps
    here we fulfill task as sequence of task's steps
    So, let's loop through all the task's steps
    */
///TODO!!! ���� ��� ����� ������� ������ ��������� �� ���������� ��� ���������� ������� ��������� ������ ����������!
    ptrInt threadInterruptSignal =0;
    for(size_t i=0; i < mapParams.size(); ++i)
    {
        std::stringstream taskStepFNameStr;
        taskStepFNameStr << grammarTaskConfig[0] << i ;

        /** Get taskStepCfg filename */
        auto itParam = mapParams.find(taskStepFNameStr.str());
        if (itParam != mapParams.end())
        {
            std::string taskStepCfg;
            taskStepCfg = itParam->second;
            /** init executor and its stuff
            to fulfill current task's step
             */
            int executor = configTaskStep(taskStepCfg);
            if(executor < _RC_SUCCESS_)
            {
///WRONG approach ->  continue;
                deleteModelAndExecutor(executor);
                _log.error(ALGORITHM_ESTIMATOR_ERROR, "WRONG config! Can not configure executor!");
                finish(ESTIMATOR_ERROR, "WRONG config! Can not configure executor!");
                return ;
            }
            _state = PROCESSING;
            /**
            fulfill current taks's step
            */
            rc = runExecutor(executor, threadInterruptSignal);
            if (rc)
            {
                deleteModelAndExecutor(executor);
                _log.error(ALGORITHM_ESTIMATOR_ERROR, "WRONG input data or critical error of estimator!");
                finish(ESTIMATOR_ERROR, "WRONG input data or critical error of estimator!");
                return;
            }
            /**
            once current taks's step accomplished
            we must save estimates
            */
            rc = saveExecutorResults(static_cast<enum taskStepExecutor> (executor));
            if (rc)
            {
                deleteModelAndExecutor(executor);
                _log.error(ALGORITHM_ESTIMATOR_ERROR, "Can not save executor's results!");
                finish(ESTIMATOR_ERROR, "Can not save executor's results!");
                return;
            }
//OLD            saveResults();
//OLD            deleteModelAndEstimator();
            deleteModelAndExecutor(executor);

        }
        else
        {
            _log.error(WORKFLOW_TASKMANAGER_ERROR, "Task step config '" + taskStepFNameStr.str() + "' not found in taskCfg!");
            finish(CONFIG_FAILED, "Task step config '" + taskStepFNameStr.str() + "' not found in taskCfg!");
            return;
        }
    } /// ����� ���� �����
#ifdef _DEBUG_
    DataManager* dataManager = this->_ptrTaskManager->getDataManager();
    if(dataManager != NULL)
    {
        dataManager->showAllMap(true);
    }
#endif // DEBUG
    finish(FINISHED, "Success");
}

int Task::configDataLoader(const std::map<std::string, std::string>& paramsMap, const std::string& taskStepCfg)
{
    auto tmpItParam = paramsMap.find(grammarTaskStepConfig[DATA_LOADER_PARAMS_SRC_ITEM]);
    if (tmpItParam != paramsMap.end())
    {
        /** here we call Config in order to parse Params.cfg (if exists)
        and make map
        */
        std::string paramsCfgFileName = tmpItParam->second;
        ConfigParser paramsParser(paramsCfgFileName);
        /** We don't kill current task if config for model does not exist
        */
        if (paramsParser.fileExists())
        {
            Logger * logger = &_log;
            _dl = new DataLoader(logger);
            if(! _dl)
            {
                _log.log(WORKFLOW_TASK_NOTIFICATION, "Can not create data loader!");
                return _ERROR_WRONG_ARGUMENT_;
            }
            std::map<std::string, std::string> mapParams = paramsParser.parse();
            int rc = _dl->parseAndCreateParams(mapParams);
            if (_RC_SUCCESS_ != rc)
            {
                delete _dl; _dl = nullptr;
               _log.log(WORKFLOW_TASK_NOTIFICATION, "Can not create params for data loader");
                return _ERROR_WRONG_ARGUMENT_;
            }
            return  DATA_LOADER;
        } else {
            /** We don't kill current task if config for model does not exist
            */
            _log.warning(WORKFLOW_TASK_WARNING, "Data Loader config " + paramsCfgFileName + " does not exist!");
        }
    }
    return _ERROR_WRONG_ARGUMENT_;
}///end configDataLoader

int Task::runDataLoader(ptrInt threadInterruptSignal)
{
    if(!_dl)
    {
        _log.log(WORKFLOW_TASK_NOTIFICATION, "Somehow dataLoader is not initialized!");
        return _ERROR_WRONG_WORKFLOW_;
    }
    return _dl->run(threadInterruptSignal);
}
int Task::configDataGenerator(const std::map<std::string, std::string>& paramsMap, const std::string& dataGeneratorName)
{
    auto tmpItParam = paramsMap.find(grammarTaskStepConfig[DATA_GENERATOR_PARAMS_SRC_ITEM]);
    if (tmpItParam != paramsMap.end())
    {
        /** here we call Config in order to parse ModelParams.cfg (if exists)
        and make map
        */
        std::string modelParamsCfgFileName = tmpItParam->second;
//            int rc = configDataGenerator((const std::string&) itParam->second, (const std::string&) modelParamsCfgFileName);
        ConfigParser paramsParser(modelParamsCfgFileName);
        /** We don't kill current task if config for model does not exist
        */
        if (paramsParser.fileExists())
        {
            Logger * logger = &_log;
            _dg = DataGenerators::IDataGenerator::createDataGenerator( (const std::string&) dataGeneratorName, logger);
            if(! _dg)
            {
                _log.log(WORKFLOW_TASK_NOTIFICATION, "Can not create data generator " + dataGeneratorName);
                return _ERROR_WRONG_ARGUMENT_;
            }
            auto mapModelParams = paramsParser.parse();
            int rc = _dg->parseAndCreateGeneratorParams(mapModelParams);
            if (_RC_SUCCESS_ != rc)
            {
                delete _dg; _dg = NULL;
               _log.log(WORKFLOW_TASK_NOTIFICATION, "Can not create params for data generator: " + dataGeneratorName);
                return _ERROR_WRONG_ARGUMENT_;
            }
            return  DATA_GENERATOR;
        }
    }
    return _ERROR_WRONG_ARGUMENT_;
}///end configDataGenerator()

int Task::runDataGenerator(ptrInt threadInterruptSignal)
{
    if(!_dg)
    {
        _log.log(WORKFLOW_TASK_NOTIFICATION, "Somehow dataGenerator is not initialized!");
        return _ERROR_WRONG_WORKFLOW_;
    }
    return _dg->generate(threadInterruptSignal);
}

int Task::configEstimator(const std::map<std::string, std::string>& paramsMap, const std::string& taskStepCfg)
{
    /** If the TRACK_NAME not found in taskStepConfig
        we need to check if REPO_ENTRIES exist in taskStepConfig
    */
    auto itParam = paramsMap.find(grammarTaskStepConfig[REPO_ENTRIES_ITEM]);
    if (itParam == paramsMap.end())
    {
        _log.error(WORKFLOW_TASK_ERROR, "Neither TRACK_NAME nor REPO_ENTRIES set");
        finish(CONFIG_FAILED, "Neither TRACK_NAME nor REPO_ENTRIES set");
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** end first part of Data Model setup */

    /** Do Model initialization stuff */
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
    itParam = paramsMap.find(grammarTaskStepConfig[MODEL_TYPE_ITEM]);
    if (itParam != paramsMap.end())
    {
        mt = IModel::getModelTypeByModelName( (char const * const) itParam->second.c_str());
        if (mt == IModel::ModelsTypes::FAKE_MODEL)
        {
            _log.error(WORKFLOW_TASK_ERROR, "Wrong model type provided for Estimator : " + itParam->second);
            finish(CONFIG_FAILED, "Wrong model type provided for Estimator : " + itParam->second);
            return _ERROR_WRONG_WORKFLOW_;
        }
        /** Do Plain Model initialization stuff */
///TODO!!! ������, ��� ���� ����� �������� ����������� ��� ���������� ������� � �����������, ������� � ��� ��������
        if (mt != IModel::ModelsTypes::ADDITIVE_MODEL)
        {
///TODO!!! �������� ���, ������� ������ ��� ��������� ESTIMATOR_PARAMS_SRC
            /**
            ������, ��� ��������� ������,
            ����� ����������� ������� �� ���������,
            � ����� ��� ��������� ������ � ���������� �� ����������!
            */
            IParams* ptrToPtrIParams=NULL;
            itParam = paramsMap.find(grammarTaskStepConfig[MODEL_PARAMS_SRC_ITEM]);
            if (itParam != paramsMap.end())
            {
                /** here we call Config in order to parse ModelParams.cfg (if exists)
                and make map
                */
                std::string modelParamsCfgFileName = itParam->second;
                ConfigParser modelParamsParser(modelParamsCfgFileName);
                /** We don't kill current task if config for model does not exist
                */
                if (modelParamsParser.fileExists())
                {
                    auto mapModelParams = modelParamsParser.parse();
                    /** create HARD-CODED empty ParamsSet for ModelParams */
                    /** params input from file -
                    fill map with IModel Params
                    */
                    std::string postfix = "=";
                    int rc = IModel::parseAndCreateModelParams(mt, mapModelParams, postfix, &ptrToPtrIParams, &_log);
                    if(rc)
                    {
                        delete ptrToPtrIParams;
                        _log.error(WORKFLOW_TASK_ERROR, "Wrong map of model params provided when estimator " + _estimator->toString() + " is initialized.");
                        finish(CONFIG_FAILED, "Wrong map of model params provided when estimator " + _estimator->toString() + " is initialized.");
                        return _ERROR_WRONG_WORKFLOW_;
                    }
                }
            }///end if(MODEL_PARAMS_SRC)
            /** Now then we know model type and params stuff to create it
            let's create instance of NOT Additive Model
            */
            if(ptrToPtrIParams)
            {
///TODO!!! Here we MUST IMPLEMENT shared_ptr<IModel *> or wrap IModel* model with class-handler!
                m_model = IModel::createModel(mt,ptrToPtrIParams);
                delete ptrToPtrIParams;
            }
            else
            {
///TODO!!! Here we MUST IMPLEMENT shared_ptr<IModel *> or wrap IModel* model with class-handler!
                m_model = IModel::createModel(mt);
            }

            if(!m_model)
            {
                _log.error(WORKFLOW_TASK_ERROR, "Can not create instance of specified model type :" + itParam->second);
                ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
                finish(CONFIG_FAILED, "Can not create instance of specified model type :" + itParam->second);
                return _ERROR_WRONG_WORKFLOW_;
            }
        } ///end if(mt != ADDITIVE_MODEL)
        else ///TODO!!! "�������� ��� ��� �������� ���������� ADDITIVE_MODEL
            /** Do Additive Model initialization stuff */
            if (mt == IModel::ModelsTypes::ADDITIVE_MODEL)
            {
                itParam = paramsMap.find(grammarTaskStepConfig[MODEL_NAME_ITEM]);
                if (itParam == paramsMap.end())
                {
                    _log.error(WORKFLOW_TASK_ERROR, "Additive model name NOT FOUND in " + taskStepCfg);
                    ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
                    finish(CONFIG_FAILED, "Additive model name NOT FOUND in " + taskStepCfg);
                    return _ERROR_WRONG_WORKFLOW_;
                }
                const enum AdditiveModelsTypes amt = getAdditiveModelsTypeByAdditiveModelName( (const std::string&) itParam->second);
                if (amt == FAKE_ADDITIVE_MODEL)
                {
                    _log.error(WORKFLOW_TASK_ERROR, "Wrong additive model type provided for Estimator : " + itParam->second);
                    finish(CONFIG_FAILED, "Wrong additive model type provided for Estimator : " + itParam->second);
                    return _ERROR_WRONG_WORKFLOW_;
                }
///TODO!!! SPECIAL stuff to parse additive model config
                itParam = paramsMap.find(grammarTaskStepConfig[MODEL_PARAMS_SRC_ITEM]);
                if (itParam != paramsMap.end())
                {
                    /** here we call Config in order to parse AdditiveModelCfg.cfg (if exists)
                    and make map
                    */
                    std::string additiveModelParamsCfgFileName = itParam->second;
                    ConfigParser modelParamsParser(additiveModelParamsCfgFileName);
                    /** We don't kill current task if config for model does not exist
                    */
                    if (modelParamsParser.fileExists())
                    {
                        auto mapModelParams = modelParamsParser.parse();
                        iteratorParamsMap tmpItParam = mapModelParams.find(IAdditiveModel::grammar[TotalAddendsOfAdditiveModel]);
                        if (tmpItParam == mapModelParams.end())
                        {
                            _log.error(WORKFLOW_TASK_ERROR, "Total of addends NOT FOUND in : " + additiveModelParamsCfgFileName + " for additive model type provided for Estimator : " + itParam->second);
                            finish(CONFIG_FAILED, "Total of addends NOT FOUND in : " + additiveModelParamsCfgFileName + " for additive model type provided for Estimator : " + itParam->second);
                            return _ERROR_WRONG_WORKFLOW_;
                        }
                        int tmpInt;
                        int rc = strTo<int>(tmpItParam->second.c_str(), tmpInt); //OLD tmpInt = atoi(tmpItParam->second.c_str());
                        if ((rc != _RC_SUCCESS_) || (tmpInt <= 0))
                        {
                            _log.error(WORKFLOW_TASK_ERROR, "Total of addends WRONG in : " + additiveModelParamsCfgFileName + " for additive model type provided for Estimator : " + itParam->second);
                            finish(CONFIG_FAILED, "Total of addends WRONG in : " + additiveModelParamsCfgFileName + " for additive model type provided for Estimator : " + itParam->second);
                            return _ERROR_WRONG_WORKFLOW_;
                        }
                        size_t totalAddendsOfAdditiveModel = (size_t)tmpInt;
                        /** create HARD-CODED empty ParamsSet for ModelParams */
                        /** params input from file -
                        fill map with IModel Params
                        */
                        std::string postfix = "=";
///TODO!!! Here we MUST IMPLEMENT shared_ptr<IAdditiveModel *> or wrap IAdditiveModel* _additiveModel with class-handler!
                        _additiveModel = IAdditiveModel::parseAndCreateModel(amt, totalAddendsOfAdditiveModel, mapModelParams, &_log);
                        if(_additiveModel == NULL)
                        {
                            _log.error(WORKFLOW_TASK_ERROR, "Wrong map of additive model params provided in " + additiveModelParamsCfgFileName);
                            finish(CONFIG_FAILED, "Wrong map of additive model params provided in " + additiveModelParamsCfgFileName);
                            return _ERROR_WRONG_WORKFLOW_;
                        }
                    }
                }///end if(ADDITIVE MODEL_PARAMS_SRC)
                if(!_additiveModel)
                {
                    _log.error(WORKFLOW_TASK_ERROR, "Can not create instance of specified additive model type :" + std::string(IAdditiveModel::additiveModelsNames[amt]) + " because it's cfg-file has NOT beem PARSED!");
                    ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
                    finish(CONFIG_FAILED, "Can not create instance of specified additive model type :" + std::string(IAdditiveModel::additiveModelsNames[amt]) + " because it's cfg-file has NOT beem PARSED!");
                    return _ERROR_WRONG_WORKFLOW_;
                }
            }
    } ///end NODEL_TYPE stuff
    /** Do Estimator initialization stuff */
    enum EstimatorsTypes et = FAKE_ESTIMATOR_TYPE;
    /** first create Estimator instance */
    // DOES WORK as well    IIntervalEstimator* est = dynamic_cast<IIntervalEstimator*> (IEstimator::createEstimator(IntervalEstimator, "PARETO", log));
    itParam = paramsMap.find(grammarTaskStepConfig[ESTIMATOR_TYPE_ITEM]);
    if (itParam != paramsMap.end())
    {
        std::string estimatorTypeName = itParam->second;
        /** get meta-info on Estimator-type */
///TODO!!! this snippet is not yet polymorphic!
        if (itParam->second == IEstimator::estimatorTypesNames[PointEstimator])
        {
            et = PointEstimator;
        }
        else if (itParam->second == IEstimator::estimatorTypesNames[IntervalEstimator])
        {
            et = IntervalEstimator;
        }
        else if (itParam->second == IEstimator::estimatorTypesNames[SimpleEstimator])
        {
            et = SimpleEstimator;
        }
        else
        {
            _log.error(WORKFLOW_TASK_ERROR, "Wrong ESTIMATOR_TYPE set:" + estimatorTypeName);
            finish(CONFIG_FAILED, "Wrong ESTIMATOR_TYPE set:" + estimatorTypeName);
            return _ERROR_WRONG_WORKFLOW_;
        }
        /** get meta-info on Estimator-class of given Estimator-type
         and create instance of Estimator
        */
        itParam = paramsMap.find(grammarTaskStepConfig[ESTIMATOR_NAME_ITEM]);
        if (itParam != paramsMap.end())
        {
            const std::string estimatorName = itParam->second;
            Logger * logger = &_log;
            _estimator = IEstimator::createEstimator(static_cast<const enum EstimatorsTypes> (et), estimatorName, logger );
            if (_estimator == NULL)
            {
                _log.error(WORKFLOW_TASK_ERROR, "Wrong ESTIMATOR_NAME set:" + estimatorName);
                finish(CONFIG_FAILED, "Wrong ESTIMATOR_NAME set:" + estimatorName);
                return _ERROR_WRONG_WORKFLOW_;
            }
            _estimator->setTask(this);
        }
    }
    else
    {
        _log.error(WORKFLOW_TASK_ERROR, std::string(grammarTaskStepConfig[ESTIMATOR_TYPE_ITEM]) + " not set");
        finish(CONFIG_FAILED, std::string(grammarTaskStepConfig[ESTIMATOR_TYPE_ITEM]) + " not set");
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** � ����� ������ ������ �����������
     ��� ��������������� �� �������� ����� ����������� ParamsSet ��� ParamsCompact!
     ��� ���� �� � �������, � ���������!
     �� ��� ����� ����� ������ ���������� ��� � ������� ��� ������� ��� ������������� ParamsSet ��� ParamsCompact
     � ������, ������� ������� ���� ��� ����������.
     � ����� ��������� ��� ������ ���� ��������� ParamsSet ��� ParamsCompact
    */
    /**
     Before Estimator Params initialization stuff
     We MUST set its model
    */
    if(m_model)
    {
        int rc = _estimator->setModel(m_model);
        if(rc)
        {
            _log.error(WORKFLOW_TASK_ERROR, "Can not set specified model type to Estimator");
            ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
            finish(CONFIG_FAILED, "Can not set specified model type to Estimator");
            return _ERROR_WRONG_WORKFLOW_;
        }
    }
///TODO!!! ��� ����� ����� �������� ������������, �.�. ����� (����� ��������� �����, ������������ �����������) ����������� ������ � ��������� IEstimator ��� ���� ����� setModel(IAdditiveModel*&) !
    else if ((NULL != _additiveModel) && (NULL != dynamic_cast<IAdditiveModelEstimator*>(_estimator)))
    {
        int rc = dynamic_cast<IAdditiveModelEstimator*>(_estimator)->setModel(_additiveModel);
        if(rc)
        {
            _log.error(WORKFLOW_TASK_ERROR, "Can not set specified additive model type to Estimator");
            ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
            finish(CONFIG_FAILED, "Can not set specified additive model type to Estimator");
            return _ERROR_WRONG_WORKFLOW_;
        }
    }
    /** Do Estimator Params initialization stuff ("ESTIMATOR_PARAMS_SRC")
    */
    itParam = paramsMap.find(grammarTaskStepConfig[ESTIMATOR_PARAMS_SRC_ITEM]);
    if (itParam != paramsMap.end())
    {
        /** here we call Config in order to parse EstimatorParams.cfg (if exists)
        and make map
        */
        std::string estimatorParamsCfg = itParam->second;
        if(estimatorParamsCfg.length() > 0)
        {
            ConfigParser estimatorParamsParser(estimatorParamsCfg);
            /** We don't kill current task if config for estimator does not exist
            */
            if (estimatorParamsParser.fileExists())
            {
                std::map<std::string, std::string> mapEstimatorParams = estimatorParamsParser.parse();

                /** create HARD-CODED empty ParamsSet for EstimatorParams */
                /** params input from file -
                fill map with IEstimator Params
                */
                size_t size = 0;
                IParams* ptrToPtrIParams=NULL;
                std::string postfix = "=";
                int rc = _estimator->parseAndCreateEstimatorParams(mapEstimatorParams, postfix, size, &ptrToPtrIParams, &_log);
                if(rc)
                {
                    _log.error(WORKFLOW_TASK_ERROR, "Wrong map of estimator params provided for Estimator");
                    mapEstimatorParams.clear();
                    finish(CONFIG_FAILED, "Wrong map of estimator params provided for Estimator");
                    return _ERROR_WRONG_WORKFLOW_;
                }
            }
            else
            {
                _log.error(WORKFLOW_TASK_ERROR, "Not found file with estimator params:" + estimatorParamsCfg);
                finish(CONFIG_FAILED, "Not found file with estimator params:" + estimatorParamsCfg);
                return _ERROR_WRONG_WORKFLOW_;
            }
        }

    }

    /** following part of DataModel setup
    must be after the instance of estimator has been created!
    */
///TODO!!! ��������� ����� ���� ����������� � Estimator'�, � ����� ���-�� � � �����-�� Model'�
    /** ������ ���� ����� ������������ � configTask() ��� ����, ����� ��������
    �� ����������� ���������� ������ ��� ����������.
    ��� ����� �� ����� ����� � PW_PolynomialRegressionEstimatorForGivenSubsets::parseAndCreateEstimatorParams
    ����� ��������� ��� ���������� ���� ���������, ������ �� ������� ��������
    � ��� ������� ��� REPO_ENTRIES_ITEM
    � ����� ���� � ������ ���� ����� ���������� � tASK'� � ����� �� ��������!
    */
    /** Set up Data */
    std::vector<std::string> keys;
    itParam = paramsMap.find(grammarTaskStepConfig[REPO_ENTRIES_ITEM]);
    if (itParam != paramsMap.end())
    {
        std::string repoEntriesStr= itParam->second;
#ifdef _DEBUG_
        std::cout << repoEntriesStr << std::endl;
#endif
        _log.log(WORKFLOW_TASK_NOTIFICATION, "Get entries from repo with keys :  " + repoEntriesStr);
        /** �����, ���������� ��������� ��� ����, �������� ��������� ������ ����� �������
         ����-������� ������ � �� ���������!
         �� ��� ��� � ���� ����������, ������ ��� ��������� � ���������� ���� ����� �������� ��� ������!
         */
        if(DataManagerGrammar::splitAllRepoKeysAsSingleString(repoEntriesStr,keys) != _RC_SUCCESS_)
        {
            _log.error(WORKFLOW_TASK_ERROR, "No entries");
            finish(CONFIG_FAILED, "No entries");
            return _ERROR_WRONG_WORKFLOW_;
        }
    }
    else
    {
        _log.error(WORKFLOW_TASK_ERROR, "No entries");
        finish(CONFIG_FAILED, "No entries");
        return _ERROR_WRONG_WORKFLOW_;
    }
    this->setDataToEstimator(keys);
    /** End of Data Model */
    /**
     once we have all stuff required it's time to rock-n-roll
    */
    return ESTIMATOR;
}
/***************************************************
*  ��� ���� ����� - �������� �����,
* c �������� ������� �������� ������������ Scenario Parser&Interpreter � Task'�

* ���-�� ���� Dependencies Injection ����������� � ���� ������.
* Creates config-parser to process taskStepCfg-file and get taskStepCfg's Params
 Once map with Params ready Do estimator initialization stuff:
 - init log-level and output-file (if required)
 - get input dataset from input-file (if specified)
   or get input dataset from repository
 - get meta-data on Model and create its instance
 - get meta-data on Estimator and create its instance
 - set the Model instance as Estimator Model
 - init Estimator's own Params (if they are specified)
 NB here the order IS CRUCIAL:
  first set Model, then init Estimator's own Params (Model may help to init Estimator's Params)
 - set the Data to Estimator
PARAM [IN] taskStepCfg -- taskStepCfg-filename
RETURNS Executor's enum for current steo
otherwise error code
****************************************************/
int Task::configTaskStep(std::string taskStepCfg)
{
    /** Environment adjustment */
    _log.log( WORKFLOW_TASK_NOTIFICATION, "Start parsing task step config:" + taskStepCfg);
    /** create parser, passing cfg-filename */
    ConfigParser parser(taskStepCfg);
#ifdef _DEBUG_
    std::cout<<std::endl<< "DEBUG:" <<taskStepCfg<<std::endl;
#endif
    if (!parser.fileExists())
    {
        _log.error(WORKFLOW_TASKMANAGER_ERROR, "Task config " + taskStepCfg + " does not exist!");
        finish(CONFIG_FAILED, "Task config " + taskStepCfg + " does not exist!");
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** get map with Task step params from parser */
    std::map<std::string,std::string> paramsMap = parser.parse();
    /** Now then map with Task step params is ready
    let's interpret the map entries
    */
    /** Set up log level */
    std::map<std::string,std::string>::iterator itParam = paramsMap.find(grammarTaskStepConfig[LOG_LEVEL_ITEM]);
    if (itParam != paramsMap.end())
    {
        int logLevel;
        /** we are pessimists */
        int rc = _ERROR_NO_INPUT_DATA_;
        rc = strTo<int>(itParam->second.c_str(), logLevel); //OLD         logLevel = atoi(itParam->second.c_str());
        if(rc != _RC_SUCCESS_)
        {
            _log.warning(WORKFLOW_TASK_WARNING, "Log level is invalid " + itParam->second);
        }
        else if (_log.setLogLevel(logLevel))
        {
            _log.log(WORKFLOW_TASK_NOTIFICATION, "Log level is set to " + itParam->second);
        }
        else
        {
            _log.warning(WORKFLOW_TASK_WARNING, "Log level is invalid " + itParam->second);
        }
        paramsMap.erase(itParam);
    }///end LOG_LEVEL
    /** Set up output file name */
    itParam = paramsMap.find(grammarTaskStepConfig[OUTPUT_FILENAME_ITEM]);
    if (itParam != paramsMap.end())
    {
        std::string delimiters = DEFAULT_DELIMITERS;
        TrimStr(itParam->second, delimiters);
        _outputFName = itParam->second;
        if (!_outputFName.empty())
        {
            _log.log(WORKFLOW_TASK_NOTIFICATION, "Output filename is set to " + _outputFName);
        }
        else
        {
            _log.warning(WORKFLOW_TASK_WARNING, "Output filename is invalid " + _outputFName);
        }
    }///end Set up output file name
    /** End of environment adjustment */
///TODO!!! ������� DATA_SET_FILENAME_ITEM -> DATA_LOADER_NAME_ITEM
    /** Configure data loader if required */
    itParam = paramsMap.find(grammarTaskStepConfig[DATA_LOADER_PARAMS_SRC_ITEM]);
    if (itParam != paramsMap.end())
    {
        return configDataLoader(paramsMap, taskStepCfg);
    }///end DATA_LOADER

    /** Configure data generator if required */
    itParam = paramsMap.find(grammarTaskStepConfig[DATA_GENERATOR_NAME_ITEM]);
    if (itParam != paramsMap.end())
    {
        return configDataGenerator(paramsMap, (const std::string&) itParam->second);
    }///end DATA_GENERATOR

    /** Configure estimator if required */
    itParam = paramsMap.find(grammarTaskStepConfig[ESTIMATOR_TYPE_ITEM]);
    if (itParam != paramsMap.end())
    {
        return configEstimator(paramsMap, taskStepCfg);
    }///end ESTIMATOR
    /** Request to configure UNKNOWN executor! */
    _log.error(WORKFLOW_TASK_ERROR, taskStepCfg + " has been developed for UNKNOW executor! Please check it!");
    finish(CONFIG_FAILED, taskStepCfg + " has been developed for UNKNOW executor! Please check it!");
    return _ERROR_WRONG_WORKFLOW_;
}///end configTaskStep()

int Task::runExecutor(int rc, ptrInt threadInterruptSignal)
{
    switch(rc){
    case DATA_GENERATOR:
        return runDataGenerator(threadInterruptSignal);
    case DATA_LOADER:
///TODO!!! So far no run! Input file(s) has(have) been read into configDataLoader() code
        return runDataLoader(threadInterruptSignal);
    case ESTIMATOR:
        return runEstimator(threadInterruptSignal);
   default :
       return _ERROR_WRONG_ARGUMENT_;
   }
   return _ERROR_WRONG_ARGUMENT_;
}///end runExecutor()

void Task::setDataToEstimator(std::vector<std::string> &keys)
{
    int dimension = keys.size();
    DataManager* dataManager = this->_ptrTaskManager->getDataManager();
    if(dataManager == NULL)
    {
        _log.error(WORKFLOW_TASK_ERROR, "None DataManager available");
        finish(CONFIG_FAILED, "None DataManager available");
        return;
    }
///TODO4ARTYOM!!! ��� � ����� ����� �������� �������� ������������ �������� ����� ����������� DataInfo
    DataInfo** di = NULL;
    di = new DataInfo* [dimension];
    if(di == NULL)
    {
        _log.error(WORKFLOW_TASK_ERROR, "No room to allocate DataInfo** for input_data of Estimator");
        finish(CONFIG_FAILED, "No room to allocate DataInfo** for input_data of Estimator");
        return;
    }
    else
    {
        for(int i = 0; i < dimension; ++i)
            di[i]=NULL;
    }
    std::vector<int> sources;
    int rc = 0;
    for(int i = 0; i < dimension; ++i)
    {
        /** C��� � ���� ��������� STL-������� keys ������ �� configTask() ����-������� �������� � ���������,
        ������� (���� ���� ������� � cfg-�����) �������� ������ ������� �� keys[i]
        ������ �� � ����� ���� ��������� ������, ����� �� �� ��� ��� �������� � ������ ���������� di[i]!
        � ����� ������ ����� ��� �� ����� ��������� ������� � ������ ����������, ������������ ��������� ������!
        */
        rc = dataManager->getData(keys[i],di[i]);
        if(rc != 0)
        {
            /** �������� di[] */
///TODO!!! �����������, ��� � ��� �������            freeArrayOfDataInfo(dimension, di);
            switch(rc)
            {
            case _ERROR_NO_ROOM_:
                _log.error(WORKFLOW_TASK_ERROR, "No room to allocate DataInfo* for input_data of Estimator");
                finish(CONFIG_FAILED, "No room to allocate DataInfo* for input_data of Estimator");
                return;
            case _ERROR_WRONG_ARGUMENT_:
            case _ERROR_WRONG_INPUT_DATA_:
                _log.error(WORKFLOW_TASK_ERROR, "WRONG input_data Key :" + keys[i]);
                finish(CONFIG_FAILED, "WRONG input_data Key :" + keys[i]);
                return;
            default:
                _log.error(WORKFLOW_TASK_ERROR, "Unknown error in DataManager::getData() while processing config!");
                finish(CONFIG_FAILED, "Unknown error in DataManager::getData() while processing config!");
                return;
            }
        }
        /** ����� ����� ������� keys[i] � ����������� � ��������� � ���!
        ����� �� ����������� keys[i] �������� ������� � ������ ����������
        ��������� ��� ������� (�.�. ���������� ��� key) � �������
        */
        std::string repoKey= keys[i];
        std::string filters= "";
        /** So far splitRepoKeysAndFilers() allways returns _RC_SUCCESS_ */
        rc = DataManagerGrammar::splitRepoKeysAndFilers(keys[i], repoKey, filters);
        int inputDataIndex=-1;
        rc = dataManager->getIndexFromKey(repoKey, inputDataIndex);
        if (rc == _RC_SUCCESS_)
        {
            sources.push_back(inputDataIndex);
        }
        else /** wrong key! */
        {
            /** �������� di[] */
///TODO!!! �����������, ��� � ��� �������
            freeArrayOfDataInfo(dimension, di);
            _log.error(WORKFLOW_TASK_ERROR, "NO ROOM or WRONG input_data Key :" + keys[i]);
            finish(CONFIG_FAILED, "NO ROOM or WRONG input_data Key :" + keys[i]);
            return;
        }
    }
    rc = _estimator->setData(dimension, const_cast<const DataInfo**> (di) );
    /*OLD ��� ����� ������� ������ ������!
    ��� ����� ���-�� ����������!
    ����� ������� ���, ����� ptrs m_dataset!
     �������� di[] */
    /** ������ di[] ����� �����! ��������� ������ di[] ��� ������ �����.
    ������ ��������, ��� �� ������ ������ �������� � ���������� � ������� deep copy?
    */
    freeArrayOfDataInfo(dimension, di);
    if(rc != 0)
    {
        _log.error(WORKFLOW_TASK_ERROR, "Wrong input_data provided for Estimator");
        finish(CONFIG_FAILED, "Wrong input_data provided for Estimator");
        return;
    }
///TODO!!! refactor this stuff
    dataManager->setSourcesForEstimator(sources, _estimator->toString());

}///end setDataToEstimator()
/**********************************************************************
* PARAM [IN] repoDataSemantics - what type of data we have to retrieve
* PARAM [IN]  keys -- keys of DataInfo's DataCells where we have to search for data
* PARAM [IN] dimension -- dimension of DataInfo* array
* PARAM [OUT] di -- DataInfo* array
**********************************************************************/
int Task::getRepoDataForTargetObj(std::vector<std::string> & keys, size_t dimension, DataInfo** const di)
{
    DataManager* dataManager = this->_ptrTaskManager->getDataManager();
    if(dataManager == NULL)
    {
        _log.error(WORKFLOW_TASK_ERROR, "None DataManager available");
/**
    � ���� ������ ���� ������ �������� Task::finish(),
    ������ ��� ��������� ��� ����� �������� � ���������,
    � �� � Task::finish() ��� ����� ��� ��������� ������ � �������,
    ������� ������� ���������� ���������� ��� ������, �������
    ��������� � ���, ���������� SEGFAULT
       finish(CONFIG_FAILED, "None DataManager available");
*/
        return _ERROR_WRONG_WORKFLOW_;
    }
    if(di == NULL)
    {
        _log.error(WORKFLOW_TASK_ERROR, "No DataInfo** for Estimator's/Model's input_data");
/**
    � ���� ������ ���� ������ �������� Task::finish(),
    ������ ��� ��������� ��� ����� �������� � ���������,
    � �� � Task::finish() ��� ����� ��� ��������� ������ � �������,
    ������� ������� ���������� ���������� ��� ������, �������
    ��������� � ���, ���������� SEGFAULT
        finish(CONFIG_FAILED, "No DataInfo** for Estimator's/Model's input_data");
*/
        return _ERROR_WRONG_ARGUMENT_;
    }
///TODO!!! ����� �����?    std::vector<int> sources;
    int rc = 0;
    for(size_t i = 0; i < dimension; ++i)
    {
        /** C��� � ���� ��������� STL-������� keys ������ �� configTask() ����-������� � �������� � ���������,
        ������� (���� ���� ������� � cfg-�����) �������� ������ ������� �� keys[i]
        ������ �� � ����� ���� ��������� ������, ����� �� �� ��� ��� �������� � ������ ���������� di[i]!
        � ����� ������ ����� ��� �� ����� ��������� ������� � ������ ����������, ������������ ��������� ������!
        */
        rc = dataManager->getData(keys[i],di[i]);
        if(rc != 0)
        {
            /** �������� di[] */
///TODO!!! �����������, ��� � ��� �������            freeArrayOfDataInfo(dimension, di);
            switch(rc)
            {
            case _ERROR_NO_ROOM_:
                _log.error(WORKFLOW_TASK_ERROR, "No room to allocate DataInfo* for input_data of Estimator");
/**
    � ���� ������ ���� ������ �������� Task::finish(),
    ������ ��� ��������� ��� ����� �������� � ���������,
    � �� � Task::finish() ��� ����� ��� ��������� ������ � �������,
    ������� ������� ���������� ���������� ��� ������, �������
    ��������� � ���, ���������� SEGFAULT
                finish(CONFIG_FAILED, "No room to allocate DataInfo* for input_data of Estimator");
*/
                return _ERROR_NO_ROOM_;
            case _ERROR_WRONG_ARGUMENT_:
            case _ERROR_WRONG_INPUT_DATA_:
                _log.error(WORKFLOW_TASK_ERROR, "WRONG input_data Key :" + keys[i]);
/**
    � ���� ������ ���� ������ �������� Task::finish(),
    ������ ��� ��������� ��� ����� �������� � ���������,
    � �� � Task::finish() ��� ����� ��� ��������� ������ � �������,
    ������� ������� ���������� ���������� ��� ������, �������
    ��������� � ���, ���������� SEGFAULT
                finish(CONFIG_FAILED, "WRONG input_data Key :" + keys[i]);
*/
                return _ERROR_WRONG_ARGUMENT_;
            default:
                _log.error(WORKFLOW_TASK_ERROR, "Unknown error in DataManager::getData() while processing config!");
/**
    � ���� ������ ���� ������ �������� Task::finish(),
    ������ ��� ��������� ��� ����� �������� � ���������,
    � �� � Task::finish() ��� ����� ��� ��������� ������ � �������,
    ������� ������� ���������� ���������� ��� ������, �������
    ��������� � ���, ���������� SEGFAULT
                finish(CONFIG_FAILED, "Unknown error in DataManager::getData() while processing config!");
*/
                return _ERROR_WRONG_WORKFLOW_;
            }
        }
///TODO!!! ����� �����?
        int inputDataIndex=0;
        rc = dataManager->getIndexFromKey(keys[i], inputDataIndex);
        if (rc == _RC_SUCCESS_)
        {
///TODO!!! ����� �����?            sources.push_back(inputDataIndex);
        }
        else /** wrong key! */
        {
            /** �������� di[] */
///TODO!!! �����������, ��� � ��� �������            freeArrayOfDataInfo(dimension, di);
            _log.error(WORKFLOW_TASK_ERROR, "NO ROOM or WRONG input_data Key :" + keys[i]);
/**
    � ���� ������ ���� ������ �������� Task::finish(),
    ������ ��� ��������� ��� ����� �������� � ���������,
    � �� � Task::finish() ��� ����� ��� ��������� ������ � �������,
    ������� ������� ���������� ���������� ��� ������, �������
    ��������� � ���, ���������� SEGFAULT
            finish(CONFIG_FAILED, "NO ROOM or WRONG input_data Key :" + keys[i]);
*/
            return rc;
        }
    }
    return _RC_SUCCESS_;
///TODO!!! ���� ������� (const DataInfo**) di � ������ ���������� ����� ������, �.�. ���� (DataInfo**) di ����� ������� �� ����������,
/// �� ��������� ����� ��� �� �����!
///TODO!!! �����������, ��� � ��� �������    freeArrayOfDataInfo(dimension, di);
}///end getRepoDataForTargetObj()


int Task::runEstimator(ptrInt threadInterruptSignal)
{
    return IEstimator::runEstimator(_estimator, threadInterruptSignal);
}

void Task::deleteModelAndDataGenerator()
{
    delete m_model;
    m_model = nullptr;
    delete _additiveModel;
    _additiveModel = nullptr;
    delete _dg;
    _dg = nullptr;
    delete _dl;
    _dl = nullptr;
}

void Task::deleteModelAndEstimator()
{
    delete m_model;
    m_model = NULL;
    delete _additiveModel;
    _additiveModel = NULL;
    delete _estimator;
    _estimator = NULL;
    deleteModelAndDataGenerator();
}

void Task::deleteModelAndExecutor(int rc)
{
    switch(rc)
    {
    case ESTIMATOR:
        return deleteModelAndEstimator();
    case DATA_GENERATOR:
        return deleteModelAndDataGenerator();
    case DATA_LOADER:
        delete _dl; _dl = nullptr;
///NOTHING to delete!
        return;
    default:
///TODO!!!
        return;
    }
    return;
}
/***********************************************************
* saveDataLoaderResults
***********************************************************/
int Task::saveDataLoaderResults(std::stringstream & resultLog)
{
    if(!_dl)
    {
        _log.error(WORKFLOW_TASK_ERROR, "Data Loader not initialized!");
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
    size_t dimension =0;
    DataInfo** di = NULL;
    int rc = _dl->getDataInfoArray(dimension, di);
    if((rc != 0) || (dimension == 0))
    {
        _log.error(WORKFLOW_TASK_ERROR, "No results from data loader");
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
    for(size_t i=0; i < dimension; ++i)
    {
///TODO!!! BAD! Very BAD!
        rc = this->_ptrTaskManager->getDataManager()->setNewDataToRepository(di[i]);
        if(rc != _RC_SUCCESS_)
        {
            _log.error(WORKFLOW_TASK_ERROR, "Can not save data :" + di[i]->m_dataSetName + " from data loader!");
            finish(ESTIMATOR_ERROR, "Can not save data :" + di[i]->m_dataSetName + " from data loader!");
            return _ERROR_WRONG_OUTPUT_DATA_;
        }
    }
#ifdef _DEBUG_
    ///only for drawing
    this->_ptrTaskManager->getDataManager()->setDemonstrationStream(& resultLog);
    ///
    for(size_t i=0; i < dimension; ++i)
    {
///TODO!!! BAD! Very BAD!
        this->_ptrTaskManager->getDataManager()->showDataInfo(di[i]);
///TODO!!! ��� ������� ������ �� �����������?
        /** NB ACTUALLY we MUST keep unchanged the array of double
        referenced by di[i]->XXX to REPO DATA!
        If di[i]->XXX points to REPO DATA DON'T DELETE IT! ONLY ASSIGN NULL to di[i]->m_data
        */
    }
    ///only for drawing
    this->_ptrTaskManager->getDataManager()->setDefaultDemonstrationStream();
    ///
#endif
    /** �������� di[], ������� �������� ��������� */
    freeArrayOfDataInfo(dimension, di);
    return _RC_SUCCESS_;
}
/***********************************************************
* saveEstimatorResults
***********************************************************/
int Task::saveEstimatorResults(std::stringstream & resultLog)
{
    size_t dimension =0;
    DataInfo** di = NULL;
///TODO4ARTYOM!!! ��� ���� getEstimatesAsDataInfoArray(dimension, di); �������� ���������� � ���� �����������,
/// �� ������ ������� � ��������� ������ �������� ���� �������, ������� ��������� ���� (���� �� ������������) ������ � DataGenerator!
    int rc = _estimator->getEstimatesAsDataInfoArray(dimension, di);
    if((rc != 0) || (dimension == 0))
    {
        _log.warning(WORKFLOW_TASK_WARNING, "No results from estimator");
        return _ERROR_WRONG_OUTPUT_DATA_;
    }

    this->_resultsRecorded = true;
    std::string sources = this->_ptrTaskManager->getDataManager()->getSourcesForEstimator(_estimator->toString());
    for(size_t i=0; i < dimension; ++i)
    {
///TODO!!! BAD! Very BAD!
        if(sources != "error")
        {
            rc = this->_ptrTaskManager->getDataManager()->setNewDataToRepository(di[i],sources);
            if(rc != _RC_SUCCESS_)
            {
                _log.error(WORKFLOW_TASK_ERROR, "Can not save estimates :" + di[i]->m_dataSetName + " from estimator :" + di[i]->m_dataCreatorName);
                finish(ESTIMATOR_ERROR, "Can not save estimates :" + di[i]->m_dataSetName + " from estimator :" + di[i]->m_dataCreatorName);
                return _ERROR_WRONG_OUTPUT_DATA_;
            }
        }
    }
#ifdef _DEBUG_
    ///only for drawing
    this->_ptrTaskManager->getDataManager()->setDemonstrationStream(&resultLog);
    ///
    for(size_t i=0; i < dimension; ++i)
    {
///TODO!!! BAD! Very BAD!
        if(sources != "error")
        {
            this->_ptrTaskManager->getDataManager()->showDataInfo(di[i]);
        }
///TODO!!! ��� ������� ������ �� �����������?
        /** NB ACTUALLY we MUST keep unchanged the array of double
        referenced by di[i]->XXX to REPO DATA!
        If di[i]->XXX points to REPO DATA DON'T DELETE IT! ONLY ASSIGN NULL to di[i]->m_data
        */
    }
    ///only for drawing
    this->_ptrTaskManager->getDataManager()->setDefaultDemonstrationStream();
    ///
#endif
    /** �������� di[], ������� �������� ��������� */
    freeArrayOfDataInfo(dimension, di);
    return _RC_SUCCESS_;
}

int Task::saveExecutorResults(enum taskStepExecutor texe)
{
    switch(texe)
    {
    case DATA_LOADER:
    case ESTIMATOR:
        return saveResults(texe);
    case DATA_GENERATOR:
///TODO!!! �������� ����� ��� �������� ��������� ���������� �� ���������� DATA_GENERATOR � �����������!
        return _RC_SUCCESS_;
    default:
        return _ERROR_WRONG_ARGUMENT_;
    }
    return _ERROR_WRONG_ARGUMENT_;
}

int Task::saveResults(enum taskStepExecutor texe)
{
    std::stringstream resultLog;
    int rc = -OUR_MAX_UNSIGNED_INT;
    switch(texe)
    {
        case DATA_LOADER:
            rc = saveDataLoaderResults(resultLog);
        break;
        case ESTIMATOR:
            rc = saveEstimatorResults(resultLog);
        break;
        default:
            return _ERROR_WRONG_ARGUMENT_;
    }
    if(_RC_SUCCESS_ != rc)
    {
        _log.error(WORKFLOW_TASK_ERROR, "Can not save results of executor!");
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
    _log.log(WORKFLOW_TASK_NOTIFICATION, resultLog.str());
    ///TODO!!! ����� ���� ������ �� ���������� �� Params ������ ������� runEstimator! � ���, � ����� ������ ����� ���������� (process's params + noise's params)
    /// ������ ������ ���������� � ���� ������� ��� ��� process's params!
    //OLD    resultLog << "Finish estimation, Track: " << trackId << ", Result: {\n" << result->toString() << "\n";
    //OLD    resultLog << "PROBABILITY=" << probability << "\n}";

    if (_outputFName.empty())
    {
        _log.warning(WORKFLOW_TASK_WARNING, "No output file name set");
        return _RC_SUCCESS_;
    }

    std::string fName = ConfigParser::setTrackNumToFileName(_outputFName, _trackId, tracks.size());
    std::ofstream resultFile(fName.c_str(), std::ios::out);

    if (!resultFile)
    {
        _log.warning(WORKFLOW_TASK_WARNING, "Failed to write the result to file");
        return _RC_SUCCESS_;
    }
//DEBUG std::cout << resultLog.str() << std::endl;
    resultFile << resultLog.str();
    resultFile.close();
    return _RC_SUCCESS_;
}

void Task::saveNanResults()
{
    std::stringstream resultLog;
    resultLog << "Finish estimation, Track: " << _trackId << ", Result: {\nNAN\n}";
    _log.log(WORKFLOW_TASK_NOTIFICATION, resultLog.str());

    if (_outputFName.empty())
    {
        _log.warning(WORKFLOW_TASK_WARNING, "No output file name set");
        return;
    }

    std::string fName = ConfigParser::setTrackNumToFileName(_outputFName, _trackId, tracks.size());

    std::ofstream resultFile(fName.c_str(), std::ios::out);

    if (!resultFile)
    {
        _log.warning(WORKFLOW_TASK_WARNING, "Failed to write the result to file");
        return;
    }

    resultFile << "NAN";

}

///TODO!!! refactor this method!
///TODO!!! DataModel MUST be in use
void Task::dumpTaskParams()
{
    std::stringstream pauseLog;
//OLD    pauseLog << "Task was paused at track " << trackId << " point " << dataset->getCurrentId();
    _log.log(WORKFLOW_TASK_NOTIFICATION, pauseLog.str() );

    std::string path = "";

    unsigned pos = _taskCfg.rfind('/');
    if (pos != std::string::npos)
    {
        path = _taskCfg.substr(0, pos + 1);
    }
    std::stringstream resultFname;

    if (tracks.size() > 1)
    {
        resultFname << path << _taskName << "_" << _trackId << ".pause";
    }
    else
    {
        resultFname << path << _taskName << ".pause";
    }

    std::ofstream resultFile(resultFname.str().c_str());

    if (!resultFile)
    {
        _log.warning(WORKFLOW_TASK_WARNING, "Failed to write the result to file");
        pauseLog.clear();
//OLD        pauseLog << result->toString() << "\tPROBABILITY=" << probability;
        _log.log(WORKFLOW_TASK_NOTIFICATION, pauseLog.str() );
        return;
    }
    _log.log(WORKFLOW_TASK_NOTIFICATION, "See current optimum into:" + resultFname.str());
}

///TODO!!! refactor this method!
///TODO!!! DataModel MUST be in use
void Task::resumeTaskParams()
{
    std::string path = "";

    unsigned pos = _taskCfg.rfind('/');
    if (pos != std::string::npos)
    {
        path = _taskCfg.substr(0, pos + 1);
    }
    std::stringstream resultFname;
}

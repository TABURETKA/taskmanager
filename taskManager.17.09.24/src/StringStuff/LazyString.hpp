#pragma once
#ifndef LAZY_STRING_HPP_INCLUDED
#define LAZY_STRING_HPP_INCLUDED

#include <memory>
#include <vector>
#include <cctype> // toupper

namespace std_utils
{
    namespace details
    {
        struct ci_char_traits
            : public std::char_traits<char>
        {
            static bool eq(char c1, char c2)
            {
                return toupper(c1) == toupper(c2);
            }

            static bool lt(char c1, char c2)
            {
                return toupper(c1) < toupper(c2);
            }

            static int compare(char const * s1, char const * s2, size_t n)
            {
                return memicmp(s1, s2, n);
            }

            static char const * find(char const * s, int n, char a)
            {
                while (n-- > 0 && toupper(*s) != toupper(a)) {
                    ++s;
                }

                return n >= 0 ? s : nullptr;
            }

        private:
            static int memicmp(char const * s1, char const * s2, std::size_t n)
            {
                int d = 0;
                for (std::size_t i = 0; i < n; ++i) {
                    d = tolower(((unsigned char *)s1)[i]) -
                        tolower(((unsigned char *)s2)[i]);
                    if (d != 0) {
                        if (d > 0) {
                            return 1;
                        } else {
                            return -1;
                        }
                    }
                }
                return 0;
            }
        };
    }

    template<class charT, class traits = std::char_traits<charT>>
    class lazy_basic_string
    {
        struct Proxy;

    public:
        typedef traits      traits_type;
        typedef std::size_t size_type;

        static const size_type npos = size_type(-1);

        /* Constructors */
        // empty string constructor (default constructor)
        explicit lazy_basic_string()
            : str_{ std::make_shared<storage>(1, charT()) }
        {}

        // copy constructor
        lazy_basic_string(lazy_basic_string const & str)
            : str_{ str.str_ }
        {}

        // substring constructor
        lazy_basic_string(lazy_basic_string const & str, size_type const pos, size_type const len = npos)
            : str_{ std::make_shared<storage>(len + 1, charT()) }
        {
            traits_type::copy(str_->data(), str.c_str() + pos, len + 1);
        }

        // from c-string
        lazy_basic_string(charT const * const s)
            : lazy_basic_string(s, traits_type::length(s))
        {}

        // from sequence
        lazy_basic_string(charT const * const s, size_type const n)
            : str_{ std::make_shared<storage>(n + 1, charT()) }
        {
            traits_type::copy(str_->data(), s, n);
        }

        // fill
        lazy_basic_string(size_type const n, charT const c)
            : str_(std::make_shared<storage>(n + 1, charT()))
        {
            traits_type::assign(str_->data(), n, c);
        }

        // destructor
        ~lazy_basic_string()
        {}

        lazy_basic_string & operator=(lazy_basic_string const & other)
        {
            if (this != &other) {
                str_ = other.str_;
            }

            return *this;
        }

        lazy_basic_string & operator=(charT const * const s)
        {
            lazy_basic_string(s).swap(*this);
            return *this;
        }

        lazy_basic_string & operator=(charT c)
        {
            lazy_basic_string(1, c).swap(*this);
            return *this;
        }

        lazy_basic_string & operator+=(lazy_basic_string const & str)
        {
            return operator+=(str.c_str());
        }

        lazy_basic_string & operator+=(charT const * const s)
        {
            size_type len = size();
            size_type n = traits_type::length(s);

            std::shared_ptr<storage> tmp{ std::make_shared<storage>(len + n + 1, charT()) };
            traits_type::copy(tmp->data(), str_->data(), len);
            traits_type::copy(tmp->data() + len, s, n);
            str_.swap(tmp);

            return *this;
        }

        lazy_basic_string & operator+=(charT const c)
        {
            return operator+=(lazy_basic_string(1, c));
        }

        Proxy operator[](size_type const position)
        {
            return Proxy(this, position);
        }

        charT const & operator[](size_type const index) const
        {
            return (*str_)[index];
        }

        charT const & at(size_type const position) const
        {
            check(position);
            return (*str_)[position];
        }

        // get C-string equivalent
        charT const * c_str() const
        {
            return str_->data();
        }

        size_type size() const
        {
            return str_->size() - 1;
        }

        bool empty() const
        {
            return size() == 0;
        }

        void clear()
        {
            str_ = std::make_shared<storage>(1, charT());
        }

        // compare strings
        int compare(lazy_basic_string const & str) const
        {
            size_type lhs = size();
            size_type rhs = str.size();
            int result = traits_type::compare(c_str(), str.c_str(), (lhs > rhs ? rhs : lhs));
            if (result != 0)
                return result;
            if (lhs < rhs)
                return -1;
            if (lhs > rhs)
                return 1;
            return 0;
        }

        // swaps the contents of two strings
        void swap(lazy_basic_string & other)
        {
            str_.swap(other.str_);
        }

    private:
        using storage = std::vector<charT>;
        std::shared_ptr<storage> str_;

        charT & at(size_type const position)
        {
            check(position);
            return (*str_)[position];
        }

        void check(size_type const position) const
        {
            if (!(position >= 0 && position <= size())) {
                throw std::out_of_range{ "lazy_basic_string::at() : Index out of range" };
            }
        }

        void detach()
        {
            if (!str_.unique()) {
                size_type const len = str_->size();
                std::shared_ptr<storage> tmp{ std::make_shared<storage>(len, charT()) };
                traits_type::copy(tmp->data(), str_->data(), len);
                str_.swap(tmp);
            }
        }

        struct Proxy
        {
            lazy_basic_string * str{ nullptr };
            size_type pos{ 0 };

            Proxy(lazy_basic_string * str, size_type position)
                : str(str)
                , pos(position)
            {}

            // Invoked when proxy is used to modify the value.
            Proxy & operator=(const charT & rhs)
            {
                str->detach();
                str->at(pos) = rhs;

                return *this;
            }

            // Invoked when proxy is used to read the value.
            operator charT const & () const
            {
                return str->at(pos);
            }
        };
    };

    template<typename charT, class traits>
    void swap(lazy_basic_string<charT, traits> & a, lazy_basic_string<charT, traits> & b)
    {
        a.swap(b);
    }

    template<typename charT, class traits>
    lazy_basic_string<charT, traits> operator+(lazy_basic_string<charT, traits> str1, lazy_basic_string<charT, traits> const & str2)
    {
        return str1 += str2;
    }

    template<typename charT, class traits>
    lazy_basic_string<charT, traits> operator+(lazy_basic_string<charT, traits> str1, charT const * const s2)
    {
        return str1 += s2;
    }

    template<typename charT, class traits>
    lazy_basic_string<charT, traits> operator+(lazy_basic_string<charT, traits> str1, charT const c2)
    {
        return str1 += c2;
    }

    template<typename charT, class traits>
    lazy_basic_string<charT, traits> operator+(charT const * s1, lazy_basic_string<charT, traits> const & str2)
    {
        lazy_basic_string<charT, traits> str1(s1);
        return str1 += str2;
    }

    template<typename charT, class traits>
    lazy_basic_string<charT, traits> operator+(charT c1, lazy_basic_string<charT, traits> const & str2)
    {
        lazy_basic_string<charT, traits> str1(1, c1);
        return str1 += str2;
    }

    template<typename charT, class traits>
    bool operator==(lazy_basic_string<charT, traits> const & a, lazy_basic_string<charT, traits> const & b)
    {
        return a.compare(b) == 0;
    }

    template<typename charT, class traits>
    bool operator!=(lazy_basic_string<charT, traits> const & a, lazy_basic_string<charT, traits> const & b)
    {
        return a.compare(b) != 0;
    }

    template<typename charT, class traits>
    bool operator<(lazy_basic_string<charT, traits> const & a, lazy_basic_string<charT, traits> const & b)
    {
        return a.compare(b) < 0;
    }

    template<typename charT, class traits>
    bool operator>(lazy_basic_string<charT, traits> const & a, lazy_basic_string<charT, traits> const & b)
    {
        return a.compare(b) > 0;
    }

    template<typename charT, class traits>
    bool operator<=(lazy_basic_string<charT, traits> const & a, lazy_basic_string<charT, traits> const & b)
    {
        return a.compare(b) <= 0;
    }

    template<typename charT, class traits>
    bool operator>=(lazy_basic_string<charT, traits> const & a, lazy_basic_string<charT, traits> const & b)
    {
        return a.compare(b) >= 0;
    }

    template<typename charT, class traits>
    bool operator==(lazy_basic_string<charT, traits> const & a, charT const * const b)
    {
        lazy_basic_string<charT, traits> const str(b);
        return a.compare(str) == 0;
    }

    template<typename charT, class traits>
    bool operator!=(lazy_basic_string<charT, traits> const & a, charT const * const b)
    {
        return !(a == b);
    }

    template<typename charT, class traits>
    bool operator<(lazy_basic_string<charT, traits> const & a, charT const * const b)
    {
        lazy_basic_string<charT, traits> const str(b);
        return a.compare(str) < 0;
    }

    template<typename charT, class traits>
    bool operator>(lazy_basic_string<charT, traits> const & a, charT const * const b)
    {
        return b < a;
    }

    template<typename charT, class traits>
    bool operator<=(lazy_basic_string<charT, traits> const & a, charT const * const b)
    {
        return !(a > b);
    }

    template<typename charT, class traits>
    bool operator>=(lazy_basic_string<charT, traits> const & a, charT const * const b)
    {
        return !(a < b);
    }

    template<typename charT, class traits>
    bool operator==(charT const * const a, lazy_basic_string<charT, traits> const & b)
    {
        lazy_basic_string<charT, traits> const str(a);
        return str.compare(b) == 0;
    }

    template<typename charT, class traits>
    bool operator!=(charT const * const a, lazy_basic_string<charT, traits> const & b)
    {
        return !(a == b);
    }

    template<typename charT, class traits>
    bool operator<(charT const * const a, lazy_basic_string<charT, traits> const & b)
    {
        lazy_basic_string<charT, traits> const str(a);
        return str.compare(b) < 0;
    }

    template<typename charT, class traits>
    bool operator>(charT const * const a, lazy_basic_string<charT, traits> const & b)
    {
        return b < a;
    }

    template<typename charT, class traits>
    bool operator<=(charT const * const a, lazy_basic_string<charT, traits> const & b)
    {
        return !(a > b);
    }

    template<typename charT, class traits>
    bool operator>=(charT const * const a, lazy_basic_string<charT, traits> const & b)
    {
        return !(a < b);
    }

    typedef lazy_basic_string<char> lazy_string;
    typedef lazy_basic_string<wchar_t> lazy_wstring;
    typedef lazy_basic_string<char, details::ci_char_traits> lazy_istring;
}

#endif /* End of 'lazy_string.hpp' file */
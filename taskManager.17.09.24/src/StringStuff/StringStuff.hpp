#include <type_traits> /* is_pod<T> */
#include <sstream> /* istringstream */
#include "../Log/errors.h" /* errors macros */

template <typename T>
int strTo(char const * const src, T & numeric)
{
    if(std::is_pod<T>::value != true)
        return _ERROR_WRONG_ARGUMENT_;
    T tmp;
    std::istringstream buffer(src);
    try{
        buffer >> tmp;
    } catch(std::exception e) {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(buffer.fail()) return _ERROR_WRONG_ARGUMENT_;
    numeric = tmp;
    return _RC_SUCCESS_;
}

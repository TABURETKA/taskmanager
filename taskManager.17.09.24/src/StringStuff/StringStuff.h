#ifndef __STRING_STUFF_H__
#define __STRING_STUFF_H__

#include <string>
#include <cctype> // char
#include <cwctype> // wchar_t
#include <list>
/** merely declaration of template function */
template <typename T>
int strTo(char const * const src, T & numeric);
//static inline std::string TrimStr(const std::string& src, const std::string& c = " \r\n")
void TrimC_str(char*& src, char const * const delimiters);
//OLD std::string TrimStr(const std::string& src, const std::string& c);
void TrimStr(std::string& src, const std::string& c);
void toUpper(std::basic_string<char>& s);
void toUpper(std::basic_string<wchar_t>& s);
void toLower(std::basic_string<char>& s);
void toLower(std::basic_string<wchar_t>& s);
void splitStringByDelimiter(const std::string& srcString, std::string& firstPart, std::string& secondPart, char delim);
std::list<std::string> parseParamList(std::string params, char delimiter);
std::string removeComment(std::string srcString, char commentDelimiter);
/** here after implementation of template function included */
#include "./StringStuff.hpp"
#endif

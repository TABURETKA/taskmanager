#ifndef _STRINGSTUFF__H_
#define _STRINGSTUFF__H_

#include <cstring> /* std::strlen() */
#include <string>

#include "../CommonStuff/Defines.h" /** delimiter macros */
#include "../Log/errors.h" /* errors macros */
#include "StringStuff.h"
/* OLD
int toFloat(char const * const src, float & flt)
{
    float tmpFlt=0.0f;
    std::istringstream buffer(src);
    buffer >> tmpFlt;
    if(buffer.fail()) return _ERROR_WRONG_ARGUMENT_;
    flt = tmpFlt;
    return _RC_SUCCESS_;
}
*/

void TrimC_str(char*& src, char const * const delimiters)
{
    int offsetSrc=-1;
    size_t totalOfDelimiters = std::strlen(delimiters);
    bool delimiterFound=false;
    do{
        ++offsetSrc;
        delimiterFound=false;
        for(size_t i=0; i < totalOfDelimiters; ++i)
        {
            if(src[offsetSrc] == delimiters[i])
            {
                delimiterFound=true;
                break;
            }
        }
    } while((src[offsetSrc] != '\0') && (delimiterFound));
    size_t sizeOfSrc = std::strlen(src);
    for(size_t i=0; i < sizeOfSrc - 1; ++i)
    {
        src[i] = src[i+1];
    }

    delimiterFound=true; /** Merely to start loop */
    for(int i=sizeOfSrc-1; (i >= 0) && (delimiterFound); --i)
    {
        delimiterFound=false;
        for(size_t j=0; j < totalOfDelimiters; ++j)
        {
            if(src[i] == delimiters[j])
            {
                delimiterFound=true;
                src[i] = '\0';
                break;
            }
        }
    }
}
//static inline std::string TrimStr(const std::string& src, const std::string& c = " \r\n")
/* OLD
std::string TrimStr(const std::string& src, const std::string& delimiters)
{
  size_t stop = src.find_last_not_of(delimiters);
  if (stop == std::string::npos)
    return std::string();
  size_t start = src.find_first_not_of(delimiters);
  if (start == std::string::npos)
    start = 0;
  return src.substr(start, (stop-start)+1);
}
*/
void TrimStr(std::string& src, const std::string& delimiters)
{
  size_t stop = src.find_last_not_of(delimiters);
  if (stop == std::string::npos){
    src.clear();
    src = "";
    return;
  }
  size_t start = src.find_first_not_of(delimiters);
  if (start == std::string::npos)
    start = 0;
  src = src.substr(start, (stop-start)+1);
  return;
}

void toUpper(std::basic_string<char>& s) {
   for (std::basic_string<char>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = toupper(*p); // toupper is for char
   }
}

void toUpper(std::basic_string<wchar_t>& s) {
   for (std::basic_string<wchar_t>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = towupper(*p); // towupper is for wchar_t
   }
}

void toLower(std::basic_string<char>& s) {
   for (std::basic_string<char>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = tolower(*p);
   }
}

void toLower(std::basic_string<wchar_t>& s) {
   for (std::basic_string<wchar_t>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = towlower(*p);
   }
}

void splitStringByDelimiter(const std::string& srcString, std::string& firstPart, std::string& secondPart, char delim)
{
    firstPart = srcString;
    size_t pos = firstPart.find_first_of(delim);
    if (pos !=std::string::npos)
    {
        firstPart.erase(0,pos+1);
        TrimStr(firstPart, DEFAULT_DELIMITERS);
        secondPart = firstPart;
        firstPart  = srcString;
        firstPart.erase(pos);
        TrimStr(firstPart, DEFAULT_DELIMITERS);
    }
}

std::list<std::string> parseParamList(std::string params, char delimiter)
{
    std::list<std::string> result;
    unsigned pos = params.find(delimiter);
    while (1) {
        std::string param = params.substr(0, pos);
        TrimStr(param, DEFAULT_DELIMITERS);
        result.push_back(param);

        if (pos == std::string::npos)
            return result;

        params.erase(0, pos + 1);
    }
    return result;
}

std::string removeComment(std::string srcString, char commentDelimiter)
{
    size_t pos = srcString.find_first_of(commentDelimiter);
    if (pos !=std::string::npos)
    {
        TrimStr(srcString.erase(pos), DEFAULT_DELIMITERS);
    }
    return srcString;
}
#endif

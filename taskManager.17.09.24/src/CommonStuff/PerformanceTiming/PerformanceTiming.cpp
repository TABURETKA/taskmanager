#include "../Defines.h"
#include "PerformanceTiming.h"

/** Windows32 targets must have WIN32 (_WIN32)
*/
#if defined WINDOWOZ
PerformanceTiming::PerformanceTiming()
{
    QueryPerformanceFrequency(&m_frequency);
    QueryPerformanceCounter(&m_startingTime);
}

PerformanceTiming::~PerformanceTiming()
{}

float PerformanceTiming::getDurationAsMicroSecs() const
{
   /** We now have the elapsed number of ticks, along with the
   number of ticks-per-second. We use these values
   to convert to the number of elapsed microseconds.
   To guard against loss-of-precision, we convert
   to microseconds *before* dividing by ticks-per-second.
   */
    return getDuration(1000000);
}

float PerformanceTiming::getDurationAsNanoSecs() const
{
   /** We now have the elapsed number of ticks, along with the
   number of ticks-per-second. We use these values
   to convert to the number of elapsed nanoseconds.
   To guard against loss-of-precision, we convert
   to nanoseconds *before* dividing by ticks-per-second.
   */
    return getDuration(1000000000);
}

float PerformanceTiming::getDuration(size_t multiplier) const
{
    LARGE_INTEGER endingTime;
    LARGE_INTEGER elapsedTime;
    QueryPerformanceCounter(&endingTime);
    elapsedTime.QuadPart = endingTime.QuadPart - m_startingTime.QuadPart;
   /** We now have the elapsed number of ticks, along with the
   number of ticks-per-second. We use these values
   to convert to the number of elapsed XXXseconds.
   To guard against loss-of-precision, we convert
   to XXXseconds *before* dividing by ticks-per-second.
   */
    elapsedTime.QuadPart *= multiplier;
    elapsedTime.QuadPart /= m_frequency.QuadPart;
    return static_cast<float> (elapsedTime.QuadPart);
}

#else ///C++11 CROSS-PLATFORM SOLUTION

PerformanceTiming::PerformanceTiming()
{
    m_ct = FAKE_CLOCK_TYPE;

    if(std::chrono::high_resolution_clock::is_steady)
    {
        m_ct = HIGH_RESOLUTION_CLOCK;
        m_startingTimeHighResolution =  std::chrono::high_resolution_clock::now();
    }
    double steady_frequency = (std::chrono::steady_clock::period::num != 1) ? NAN :  (std::chrono::steady_clock::period::den / std::chrono::steady_clock::period::num);
    double high_resolution_frequency = (std::chrono::high_resolution_clock::period::num != 1) ? NAN :  (std::chrono::high_resolution_clock::period::den / std::chrono::high_resolution_clock::period::num);
    if(high_resolution_frequency > 10*steady_frequency)
    {
        m_ct = HIGH_RESOLUTION_CLOCK;
        m_startingTimeHighResolution = std::chrono::high_resolution_clock::now();
    }
    else
    {
        m_ct = STEADY_CLOCK;
        m_startingTimeSteady = std::chrono::steady_clock::now();
    };

}

PerformanceTiming::~PerformanceTiming()
{}

float PerformanceTiming::getDurationAsMicroSecs() const
{
    return getDuration(1000);
}

float PerformanceTiming::getDurationAsNanoSecs() const
{
    return getDuration(1);
}

float PerformanceTiming::getDuration(size_t denominator) const
{
    switch(m_ct)
    {
    case STEADY_CLOCK:
    {
        std::chrono::duration<float, std::nano> nowAsFlt = std::chrono::duration<float, std::nano> (std::chrono::steady_clock::now() - m_startingTimeSteady);
        return static_cast<float> ( denominator > 0 ? (nowAsFlt.count()/denominator) : NAN);
    }
    case HIGH_RESOLUTION_CLOCK:
    {
        std::chrono::duration<float, std::nano> nowAsFlt = std::chrono::duration<float, std::nano> (std::chrono::high_resolution_clock::now() - m_startingTimeHighResolution);
        return static_cast<float> ( denominator > 0 ? (nowAsFlt.count()/denominator) : NAN);
    }
    default :
         return NAN;
    }
}

#endif

#ifndef __PERFORMANCE_TIMING_H_633569422542968750
#define __PERFORMANCE_TIMING_H_633569422542968750


/** Windows32 targets must have WIN32 (_WIN32)
*/
//  #if defined(_WIN32) || defined(WIN32)
//    #define WINDOWOZ
//  #endif

  #if defined WINDOWOZ
  #include <windows.h> /*  */
  class PerformanceTiming{
  public:
    PerformanceTiming();
    ~PerformanceTiming();
    float getDurationAsMicroSecs() const;
    float getDurationAsNanoSecs() const;

  private:

    /** assign and copy CTORs are disabled */
    PerformanceTiming(const PerformanceTiming&);
    void operator=(const PerformanceTiming&);

    LARGE_INTEGER m_frequency;
    LARGE_INTEGER m_startingTime;

    float getDuration(size_t multiplier) const;
  };

  #else ///C++11 CROSS-PLATFORM SOLUTION

  #include <chrono>
  #include <ratio>
  class PerformanceTiming{
  public:
    enum ClockType
    {
        STEADY_CLOCK,
        HIGH_RESOLUTION_CLOCK,
        FAKE_CLOCK_TYPE
    };
    PerformanceTiming();
    ~PerformanceTiming();
    float getDurationAsMicroSecs() const;
    float getDurationAsNanoSecs() const;

  private:

    /** assign and copy CTORs are disabled */
    PerformanceTiming(const PerformanceTiming&);
    void operator=(const PerformanceTiming&);

    std::chrono::steady_clock::time_point          m_startingTimeSteady;
    std::chrono::high_resolution_clock::time_point m_startingTimeHighResolution;
    ClockType m_ct;

    float getDuration(size_t denominator) const;
  };
  #endif

#endif

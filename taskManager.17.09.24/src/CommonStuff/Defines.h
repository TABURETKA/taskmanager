#ifndef __DEFINES_H_633569422542968750
#define __DEFINES_H_633569422542968750

/** Windows32 targets must have WIN32 (_WIN32)
*/
#if defined(_WIN32) || defined(WIN32)
/** OUR Max unsigned long = 4294967295L
*/
#define OUR_MAX_UNSIGNED_LONG 4294967295L
#define OUR_MAX_UNSIGNED_INT  2147483647
#elif defined(_WIN64) || defined(WIN64)
/** Windows64 targets must have WIN64 (_WIN64)
*/
#define OUR_MAX_UNSIGNED_LONG 18446744073709551615L
#else
/** Linux or its flavor, and default value in case none WINXX defined */
/** OUR Max unsigned long = 4294967295L */
#define OUR_MAX_UNSIGNED_LONG 4294967295L
#endif

/*********************************
*  Simple grammar for Config-files
*********************************/
#define DEFAULT_DELIMITERS " \t\r\n"
#define DELIMITER_LIST_ITEMS ','
#define DELIMITER_COMMENT_STARTS_WITH '#'
#define DELIMITER_VALUE_CONTINUED_IN_NEXT_LINE '\\'
#define DELIMITER_NAME_VALUE '='
#define DELIMITER_FILENAME_FILEEXT '.'
#define DELIMITER_TO_STRING_ITEMS ";"

/** IAA: replaced with C++-conformant implementaion
 because g++ screws up (x != x) if compiler option -ffast-math is set!
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
 inline bool isNAN_double(double x) { return (x != x); }
#pragma GCC diagnostic pop
*/
/**
 IAA: merely to make g++ compile the following inline-function
 isNAN_double() has been replaced with cross-platform solution!
*/
#ifndef  __cplusplus
#define __cplusplus
#endif

#define DEFAULT_VALUE_TO_REPLACE_NAN_PARAM 0.0f
#include <cmath> /* C++11 std::isnan(double)  */
#include <limits> /* numeric_limits<double> */

///TODO!!! #define NAN std::numeric_limits<double>::quiet_NaN()
///TODO!!! Does own isNAN_double() make sense?
inline  bool isNAN_double(double x) { return std::isnan(x); }
//#undef __cplusplus
/**************************************************
* our infinities to make and check function support
**************************************************/
#define NEGATIVE_INFINITY   -DBL_MAX
#define POSITIVE_INFINITY   DBL_MAX

#endif //__DEFINES_H_633569422542968750

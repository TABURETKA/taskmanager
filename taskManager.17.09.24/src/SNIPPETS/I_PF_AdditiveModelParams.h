/***************************************************************************************
*                            PF Additive Model Params                                  *
****************************************************************************************/
/** forward declaration */
class I_PF_AdditiveModel;
/** ���� ����� �� ����� ��� ���������
    ��� ������� ����������/������ �� ��������� ���������
    ������� �� �� ����� ������������� �� IParams
    �.�. ������ ���������� �� Params ����� ������ ����
*/
class I_PF_AdditiveModelParams : public IParamsUnion
{
public:
// OLD	PF_Params();
/// already implemented interface I_PF_AdditiveModel!
///??? May be implement following method?
/// Here we can pull out I_PF_AdditiveModel the AdditiveModelParams' totalOfAddends and size_t* sizesOfAddends
/// private	I_PF_AdditiveModelParams(I_PF_AdditiveModel* model);
    ~I_PF_AdditiveModelParams();

/** ������ ������ � ��������� I_PF_AdditiveModel,
��� ��� ��������� ���������� I_PF_AdditiveModelParams
�� ������ ����� � ������������� ����������
����� ������ ���������� ���������� ���������� I_PF_AdditiveModel,
� ������� ���������� ���������� ����������
����������� ����
� ���� ����� - �� ����� ��� ��������� ��� ����������/������ �� ��������� ���������
TODO!!! � ���� ������ ����� ������������������� ��������� noiseParams � processParams
    static I_PF_AdditiveModelParams* parseAndCreateParams(const I_PF_AdditiveModel* model, std::map<std::string, std::string>& mapParams, std::string postfix);
*/
    virtual std::string toString() const;

/// ������������ clone() ������	virtual I_PF_AdditiveModelParams* copy();
/// ������������ clone() ������	virtual void copy(I_PF_AdditiveModelParams* src);
/// noiseParams and particleGeneratingParams has been moved to the addends of additive MODEL!
///Indeed, they are not Estimator's params!
///TODO!!! in clone don't forget to init noiseParams, particleGeneratingParams after the cloning of addends
    IParamsUnion const * const _noiseParams;
    IParamsUnion const * const _particleGeneratingParams;
};

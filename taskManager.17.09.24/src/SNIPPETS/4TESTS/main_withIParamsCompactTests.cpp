#include "TaskManager/TaskManager.h"
#include <iostream>
#include <string>
#include <vector>

#ifdef _DEBUG
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#endif

#include "Params/Params.h" //IParams
#include "Params/ParamsSet.h" //IParamsSet
#include "Params/ParamsCompact.h" //IParamsCompact
#include "Models/Model.h" //IModel
#include "Models/AdditiveModel.h" //IAdditiveModel
/** IAA: to change current directory (cross-platform solutions ONLY!) */

// Begin Maria's code
#include "Distributions/Distributions.h"
// End Maria's code

#ifdef _WIN32
#include <direct.h>
// MSDN recommends against using getcwd & chdir names
#define GET_CWD _getcwd
#define CD _chdir
#else /** POSIX-standard */
#include "unistd.h"
#define GET_CWD getcwd
#define CD chdir
#endif

int main(int argc, char *argv[])
{
    /*if (argc < 2) {
        std::cout << "To change path to ini-file run as:" << argv[0] << " path_to_dir_file_ini" << std::endl;
    }
    else
    {
        if (CD(argv[1]) == 0) {
            char buf[4086] = {};
            std::cout << "Current directory has been changed to =>" << GET_CWD(buf, sizeof(buf)) << std::endl;
        }
        else {
            std::cout << "Error while changing current directory! Will try to use default ./" << std::endl;
        }
    }

    TaskManager* taskManager = new TaskManager();
    taskManager->run();
    */

// Begin Maria's code
    const size_t paramsSize = 7;
    /** fill Params with data of Left points
    */
    IParams *ptrLeftBound= IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, paramsSize);
    for(int i = 0; i < paramsSize; i++)
    {
        ptrLeftBound->setParam(i,2*i);
    }
    std::cout << "LeftBoundaries:"  << ptrLeftBound->toString() << std::endl;

    /** fill Params with data of Right points
    */
    IParams *ptrRightBound= IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, paramsSize);
    for(int i=0; i < paramsSize; i++)
    {
        ptrRightBound->setParam(i,2*i+1);
    }
    std::cout << "RightBoundaries:" << ptrRightBound->toString() << std::endl;

    /** fill Params with data of Increments
    */
    IParams *ptrIncrements= IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, paramsSize);
    for(int i=0; i < paramsSize; i++)
    {
        ptrIncrements->setParam(i,0.5);
    }
    std::cout << "Increments:"  << ptrIncrements->toString() << std::endl;

    IModel *model = IModel::createModel(IModel::ModelsTypes::PlainModel, NULL);
    //enum IModel::ModelsTypes* addends = new enum IModel::ModelsTypes[2];
    //addends[0] = IModel::ModelsTypes::PlainModel;
    //addends[1] = IModel::ModelsTypes::PlainModel;
    //IAdditiveModel * am = IAdditiveModel::createModel(2, (const enum IModel::ModelsTypes*) addends, NULL);

    IParamsCompact *ptrCompact = IParamsCompact::createParamsCompact(model, ptrLeftBound, ptrRightBound, ptrIncrements, 1, NULL);


    //BEGIN IParamsCompact TEST

    //TEST_1
    std::cout << "\nTEST 1" <<std::endl;
    size_t id;

    std::cout<< "compact's size = " << ptrCompact->getSize() <<std::endl;
    /*for(size_t i = 0; i < ptrCompact->getSize(); ++i)
    {
        ptrCompact->getId_byIParams(ptrCompact->getParamsCopy(i), id);
        std::cout<<"id = " << id << std::endl;
    }*/
    std::cout<<std::endl;

    //TEST_2
    std::cout << "TEST 2" <<std::endl;
    IParams *point = ptrCompact->getFirstPtr();
    ptrCompact->getId_byIParams(point, id);
    std::cout<<"id = " <<  id << std::endl;
    std::cout<<std::endl;

    //TEST_3
    std::cout << "TEST 3" <<std::endl;
    IParams *pointWithRandomId;
    size_t randomId, step;

    /*for(size_t i = 0; i < 10; ++i)
    {
        randomId = (int)(Distributions::Uniform()*(ptrCompact->getSize() - 1) + 1);
        std::cout<<"random id = " <<  randomId << std::endl;
        pointWithRandomId = ptrCompact->getParamsCopy(randomId);

        ptrCompact->getId_byIParams(pointWithRandomId, id);
        std::cout<<"id = " << id << std::endl;
        ptrCompact->setCurrentId(randomId);

        step = (int)(Distributions::Uniform()*(ptrCompact->getSize() - 1 - randomId) + 1);
        std::cout<<"step = " <<  step << "  maxStep = " << ptrCompact->getSize() - 1 - randomId << std::endl;
        for(size_t j = 0; j < step; ++j)
        {
            point = ptrCompact->getNextPtr();
            //std::cout<<"curr id in compact after" << j << "next() = " << ptrCompact->getCurrentId() << std::endl;
        }
        std::cout<<"curr id in compact after " << step << " next() = " << ptrCompact->getCurrentId() << std::endl;
    }*/
    std::cout<<std::endl;

    //TEST_4
    std::cout << "TEST 4" <<std::endl;
    point = ptrCompact->getParamsCopy(ptrCompact->getSize() - 1);
    ptrCompact->getId_byIParams(point, id);
    std::cout<<"id = " <<  id << std::endl;
    std::cout<<std::endl;

    //TEST_5
    std::cout << "TEST 5" <<std::endl;
    size_t dim = ptrCompact->getDimOfParams(), l;
    size_t* order = new size_t[dim];
    std::cout << "dim of parametric space = " << dim <<std::endl;
for(size_t t = 0; t < 3; ++t)
{
    // ��������� ������ � ��������� ��������� � ��������� �������
    order[0] = (int)(Distributions::Uniform()*dim);
    std::cout << order[0] << std::endl;
    for(size_t k = 1; k < dim; ++k)
    {
        do
        {
            order[k] = (int)(Distributions::Uniform()*dim);
            for(l = 0; l < k; ++l)
            {
                if (order[k] == order[l])
                    break;
            }
            //std::cout << order[k] << " l = " << l << " k = " << k <<std::endl;
        }
        while (l != k);
        std::cout << order[k] <<std::endl;
    }

    // ����� ������� ������ ���������
    ptrCompact->setOrderOfCoordsToWalkThroughCompact(dim, order);

    /*for(size_t i = 0; i < ptrUnionCompact->getSize(); ++i)
    {
        point = ptrUnionCompact->getNextPtr();
        //std::cout<<"id = " << id << std::endl;
    }*/

    for(size_t i = 0; i < 3; ++i)
    {
        randomId = (int)(Distributions::Uniform()*(ptrCompact->getSize() - 2000) + 1);
        std::cout<<"random id = " <<  randomId << std::endl;
        pointWithRandomId = ptrCompact->getParamsCopy(randomId);

        ptrCompact->getId_byIParams(pointWithRandomId, id);
        ptrCompact->setCurrentId(randomId);

        step = (int)(Distributions::Uniform()*2000);
        std::cout<<"step = " <<  step << std::endl;
        for(size_t j = 0; j < step; ++j)
        {
            point = ptrCompact->getNextPtr();
        }
        std::cout<<"curr id in compact after " << step << " next() = " << ptrCompact->getCurrentId() << std::endl;
    }
}
    std::cout<<std::endl;

    //END

// End Maria's code

    return 0;
}

#include <iostream>
#include <queue>
#include <regex>
#include <string>

#define _RC_SUCCESS_ 0
#define _ERROR_WRONG_ARGUMENT_ -4
#define FILTER_TOKENS_DELIMITER '.'

int main() {
    auto check_replace_filter = [](std::string & currentFilter, std::string & newFilter) {
    static std::regex base_regex("(\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?.\\w+(\\("
                                 "\\w+\\))?.\\w+(\\(\\w+\\))?)|"
                                 "(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)).\\w+(\\("
                                 "\\w+\\))?.\\w+(\\(\\w+\\))?)"
                                 "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+("
                                 "\\(\\w+\\)).\\w+(\\(\\w+\\))?)"
                                 "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+("
                                 "\\(\\w+\\))?.\\w+(\\(\\w+\\)))");
    static std::regex r("(?=\\((\\w+)\\))");

    if (std::regex_match(currentFilter, base_regex)) {
        std::string oldFilter = currentFilter;
        newFilter = currentFilter;
        for (auto begin = std::sregex_iterator(currentFilter.begin(), currentFilter.end(), r),
            end = std::sregex_iterator(); begin != end; ++begin) {
            auto substring = (*begin)[1].str();
            std::cout << substring << std::endl;

            auto rex_for_old("\\(" + substring + "\\)");
            std::cout << rex_for_old << std::endl;

            std::regex e_for_old(rex_for_old);
            oldFilter = std::regex_replace(oldFilter, e_for_old, "");
            std::cout << oldFilter << std::endl;

            auto rex_for_new("\\w+\\(" + substring + "\\)");
            std::cout << rex_for_new << std::endl;
            std::regex e_for_new(rex_for_new);
            newFilter = std::regex_replace(newFilter, e_for_new, substring);
            std::cout << newFilter << std::endl;
        }
        currentFilter = oldFilter;
    }
  };

  std::string currentFilter = "SampleMeanIntervalEstimator.PLAIN_MODEL(ExpMeanReverting).SampleMeanBoundaries(ExpMeanRevertingParams).IntervalEstimatesRight";
  std::string newFilter = "";
  check_replace_filter(currentFilter, newFilter);
  std::cout << currentFilter << ":" << newFilter << std::endl;

  return 0;
}

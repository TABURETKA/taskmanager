/**
* Filter Grammar Stuff: loop through the filters and try to find out the matching dataset into DataCell,
* which DataInfo is provided.
* If any dataset's fullname matches current filter the index of the dataset populates vector<int>
PARAMS [IN] pDI -- ptr to metadata
PARAMS [IN] l -- ref to list of filter as std::list<std::string>
PARAMS [IN] Key* & key -- key of the data to retrieved from the repository
PARAMS [OUT] indexesOfDatasets -- the indexes (as std::vector<int>) of the datasets to be filtered from the DataCell
RETURNS _RC_SUCCESS_ if success
_ERROR_WRONG_INPUT_DATA_ if required datasets are not found for any filter
*/
int DataModel::fillVectorOfDatasetIndexesByFilter(const DataInfo *& pDI,
    std::list<std::string> & l,
    std::vector<int> & indexesOfDatasets,
    Key *& key)
{
    /**
    * Filter Grammar Stuff: makes filter prefix for given Metadata (DataInfo)
    * Namely, it concatenates dataCreatorName, dataModelName and dataSetName
    * into the string with default FILTER_TOKENS_DELIMITER between those tokens
    PARAMS [IN] pDI -- ptr to metadata
    RETURNS filterPrefix as std::string
    */
    static auto makeFilterPrefix = [](const DataInfo *& pDI) {
        std::string filterPrefix = pDI->m_dataCreatorName;
        filterPrefix += FILTER_TOKENS_DELIMITER;
        filterPrefix += pDI->m_dataModelName;
        filterPrefix += FILTER_TOKENS_DELIMITER;
        filterPrefix += pDI->m_dataSetName;
        return filterPrefix;
    };

    /** Filter Grammar Stuff : makes fullname of single dataset.
    * Namely, it concatenates given filter prefix for that dataset and its datasetTrackName
    * (filter prefix is concatenation of THREE tokens dataCreatorName, dataModelName and dataSetName
    into the string with default FILTER_TOKENS_DELIMITER between those tokens)
    PARAMS [IN] filterPrefix -- given filter prefix for that dataset
    PARAMS [IN] pDI -- ptr to metadata
    PARAMS [IN] datasetIndex -- the index of the dataset into DataCell.values, the full of which is required
    RETURNS full Dataset Name as std::string
    */
    static auto makeFullDatasetName =
        [](const DataInfo *& pDI, const std::string & filterPrefix, const size_t datasetIndex) {
        if (pDI->m_dimension <= datasetIndex) {
            return std::string();
        }

        return std::string(filterPrefix) + FILTER_TOKENS_DELIMITER + pDI->m_dataSetTrackNames[datasetIndex];
    };

    /** Validation new param of filter.
    * PARAMS [IN] this -- point to class

    * PARAMS [IN] key -- key of the data to retrieved from the repository
    * PARAMS [IN] subfilter -- new param in filter (new_param)
    * PARAMS [IN] indexSubfilter -- the index of the param in filter, the numbering starts with 1 and ends 4

    * RETURNS _RC_SUCCESS_ if subfilter is correct,
    * _ERROR_WRONG_INPUT_DATA_ otherwise
    */
    auto validation = [this](Key *& key, std::string & subfilter, int indexSubfilter) {
        std::queue<int> keysIndex;

        auto indexesOfData = key->getIndexesOfData();
        for (auto const & idx : indexesOfData) {
            keysIndex.push(idx);
        }

        while (!keysIndex.empty()) {
            auto keyIndex = keysIndex.front();
            keysIndex.pop();
            DataCell * pDataCell = nullptr;

            auto rc = getDataWithIndex(keyIndex - 1, pDataCell);
            if (rc != _RC_SUCCESS_) {
                return _ERROR_WRONG_ARGUMENT_;
            }

            std::string compareString;
            /// TODO!!! we BADLy need enum to do with single elements of metadata
            if (indexSubfilter == 1) {
                compareString = pDataCell->m_di->m_dataCreatorName;
            }
            else if (indexSubfilter == 2) {
                compareString = pDataCell->m_di->m_dataModelName;
            }
            else if (indexSubfilter == 3) {
                compareString = pDataCell->m_di->m_dataSetName;
            }
            else if (indexSubfilter == 4) {
                /// TODO!!! BUG : WHY 0 ONLY? there are more then 1 TRACK!
                compareString = pDataCell->m_di->m_dataSetTrackNames[0];
            }

            if (!compareString.compare(subfilter)) {
                return _RC_SUCCESS_;
            }

            indexesOfData = pDataCell->m_k->getIndexesOfData();
            for (auto const & idx : indexesOfData) {
                if (idx == 1) {
                    continue;
                }
                keysIndex.push(idx);
            }
        }

        return _ERROR_WRONG_ARGUMENT_;
    };

    auto checkFilterAndReplace = [validation](Key *& key, std::string & currentFilter) {
        static std::regex base_regex("(\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?)|"
            "(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?)"
            "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?)"
            "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)))");
        static std::regex r("(?=\\((\\w+)\\))");

        if (std::regex_match(currentFilter, base_regex)) {
            auto newFilter = currentFilter;
            for (auto begin = std::sregex_iterator(currentFilter.begin(), currentFilter.end(), r),
                 end = std::sregex_iterator();  begin != end; ++begin) {
                auto subfilter = (*begin)[1].str();
                std::cout << subfilter << std::endl;

                auto indexSubfilter = 1;
                for (auto i = 1u; i != std::string::npos; ++indexSubfilter, ++i) {
                    i = newFilter.find('.', i);
                    if (newFilter[i - 1] == ')') {
                        break;
                    }
                }

                auto rc = validation(key, subfilter, indexSubfilter);

                if (rc == _RC_SUCCESS_) {
                    std::regex e("\\w+\\(" + subfilter + "\\)");
                    newFilter = std::regex_replace(newFilter, e, subfilter);
                }
            }
            currentFilter = newFilter;
        }
    };

    auto filterPrefix = makeFilterPrefix(pDI);
    while (l.size()) {
        auto index = -1;
        auto currentFilter = l.front();

        checkFilterAndReplace(key, currentFilter);

        for (auto i = 0u; i < pDI->m_dimension; ++i) {
            auto fullNameOfDatasetTrack = makeFullDatasetName(pDI, filterPrefix, i);
            auto pos = std::string::npos;
            /// TODO: What the hell with comparison (CS or CI)???

            std::cout << "fullNameOfDatasetTrack:" << fullNameOfDatasetTrack << "; currentFilter:" << currentFilter
                << std::endl;

            if ((pos = fullNameOfDatasetTrack.find(currentFilter)) != std::string::npos) {
                index = i;
                break;
            }
        }

        if (index < 0) {
            return _ERROR_WRONG_INPUT_DATA_;
        }

        indexesOfDatasets.push_back(index);
        l.pop_front();
    }

    return _RC_SUCCESS_;
}
#ifndef _STRINGSTUFF__H_
#define _STRINGSTUFF__H_

#include <string>
#include "../CommonStuff/Defines.h" /** delimiter macros */
#include "StringStuff.h"

//static inline std::string TrimStr(const std::string& src, const std::string& c = " \r\n")
std::string TrimStr(const std::string& src, const std::string& delimiters)
{
  size_t stop = src.find_last_not_of(delimiters);
  if (stop == std::string::npos)
    return std::string();
  size_t start = src.find_first_not_of(delimiters);
  if (start == std::string::npos)
    start = 0;
  return src.substr(start, (stop-start)+1);
}


void toUpper(std::basic_string<char>& s) {
   for (std::basic_string<char>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = toupper(*p); // toupper is for char
   }
}

void toUpper(std::basic_string<wchar_t>& s) {
   for (std::basic_string<wchar_t>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = towupper(*p); // towupper is for wchar_t
   }
}

void toLower(std::basic_string<char>& s) {
   for (std::basic_string<char>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = tolower(*p);
   }
}

void toLower(std::basic_string<wchar_t>& s) {
   for (std::basic_string<wchar_t>::iterator p = s.begin();
        p != s.end(); ++p) {
      *p = towlower(*p);
   }
}

void splitStringByDelimiter(const std::string& srcString, std::string& firstPart, std::string& secondPart, const char delim)
{
    firstPart = srcString;
    size_t pos = firstPart.find_first_of(delim);
    if (pos !=std::string::npos)
    {
        secondPart = TrimStr(firstPart.erase(0,pos+1), DEFAULT_DELIMITERS);
        firstPart  = srcString;
        firstPart  = TrimStr(firstPart.erase(pos), DEFAULT_DELIMITERS);
    }
}

std::list<std::string> parseParamList(std::string params, const char delimiter)
{
    std::list<std::string> result;
    unsigned pos = params.find(delimiter);
    while (1) {
        std::string param = params.substr(0, pos);
        param = TrimStr(param, DEFAULT_DELIMITERS);
        result.push_back(param);

        if (pos == std::string::npos)
            return result;

        params.erase(0, pos + 1);
    }
    return result;
}

std::string removeComment(std::string srcString, const char commentDelimiter)
{
    size_t pos = srcString.find_first_of(commentDelimiter);
    if (pos !=std::string::npos)
    {
        srcString = TrimStr(srcString.erase(pos), DEFAULT_DELIMITERS);
    }
    return srcString;
}
#endif

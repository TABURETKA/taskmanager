#ifndef __STRING_STUFF_H__
#define __STRING_STUFF_H__

#include <string>
#include <cctype> // char
#include <cwctype> // wchar_t
#include <list>
//static inline std::string TrimStr(const std::string& src, const std::string& c = " \r\n")
std::string TrimStr(const std::string& src, const std::string& c);
void toUpper(std::basic_string<char>& s);
void toUpper(std::basic_string<wchar_t>& s);
void toLower(std::basic_string<char>& s);
void toLower(std::basic_string<wchar_t>& s);
void splitStringByDelimiter(const std::string& srcString, std::string& firstPart, std::string& secondPart, const char delim);
std::list<std::string> parseParamList(std::string params, const char delimiter);
std::string removeComment(std::string srcString, const char commentDelimiter);
#endif

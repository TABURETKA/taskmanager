#ifndef DATAINFO_H
#define DATAINFO_H
 /* std::memset */
#include <cstring>
#include <string>
#include "../../Log/errors.h" /** errors macros */

///structure for storing information about data
typedef struct tagDataInfo
{
///TODO!!! ����������� ������ ���� � ��� ����� ����������, m_dimension, �������, � ����� ��� � m_dataSetSizes!
    tagDataInfo (double ** p): ///TODO m_dimension(???)
                              m_dataSetTrackNames(NULL),
                              m_dataSetSizes(NULL),
                              m_dataSet((double const ** const)p)
///TODO!!! � ���� CTOR'� ���������, ��� ���! ��������� � p ��������!
    {}
    /// the name of data source, for example name of source file or name of Estimator
    std::string m_dataCreatorName;
    /// the name of data set, for example regression location or regression coefficients
    std::string m_dataSetName;
    /// the name of each dataset, for example first coefficient, second coefficient
///TODO!!! ��������� �������� ��� ����, ��������!
    std::string* m_dataSetTrackNames;
    /// the name of model
    std::string m_dataModelName;
    /// the sizes of each dataset
    size_t* m_dataSetSizes;
    /// number of dataset
    size_t m_dimension;
    /** we forbid any changes of dataset's items
    because _data is reference to the ORIGINAL DATA
    which MUST BE unchanged along the session!  */
///TODO!!! dataSet = NULL
    double const ** const m_dataSet; // = NULL;
} DataInfo;

inline int freeDataInfo(DataInfo*& di)
{
    if(di->m_dataSet)
    {
        /** NB ACTUALLY we MUST keep unchanged the array of double
        referenced by di[0]->_data!
        DON'T DELETE IT! ONLY ASSIGN NULL to di[0]->_data
        */
        for(size_t i = 0; i < di->m_dimension; ++i)
        {
            delete [] di->m_dataSet[i];
        }
        di->m_dimension = 0;
        if(di->m_dataSet) delete [] di->m_dataSet;

        if(di->m_dataSetSizes) delete [] di->m_dataSetSizes;
        di->m_dataSetSizes = NULL;

        if (di->m_dataSetTrackNames) delete [] di->m_dataSetTrackNames;
        di->m_dataSetTrackNames = NULL;
    }
    delete di;
    di = NULL;
    return _RC_SUCCESS_;
}

inline int freeArrayOfDataInfo(size_t dimension, DataInfo**& di)
{
    for(size_t j=0; j < dimension; ++j)
    {
        freeDataInfo(di[j]);
        di[j] = NULL;
    }
    delete [] di;
    return _RC_SUCCESS_;
}
/*!
    int DataManager::copyMetadataBetweenDataInfos(DataInfo* &di1, DataInfo* &di2)

     \brief:  create a copy of DataInfo
     \return: - 0, if success, else 1
     \params: DataInfo* &di1 - old data
              DataInfo* &di2 - new data
 */
inline int copyMetadataBetweenDataInfos(DataInfo* &di1, DataInfo* &di2)
{
    size_t dim = di1->m_dimension;
    size_t* ds = new size_t[dim];
    if (! ds)
    {
        return _ERROR_NO_ROOM_;
    }
    std::memset(ds, 0, dim*sizeof(size_t));
    std::string* names = new std::string[dim];
    if (! names)
    {
        delete[] ds;
        return _ERROR_NO_ROOM_;
    }
    for(size_t i = 0; i < dim ; ++i)
    {
        ds[i] = di1->m_dataSetSizes[i];
        names[i] = di1->m_dataSetTrackNames[i];
    }
    di2->m_dataCreatorName = di1->m_dataCreatorName;
    di2->m_dataModelName = di1->m_dataModelName;
    di2->m_dataSetName = di1->m_dataSetName;
    di2->m_dimension = dim;
    di2->m_dataSetSizes = ds;
    di2->m_dataSetTrackNames = names;
    return _RC_SUCCESS_;
}
#endif

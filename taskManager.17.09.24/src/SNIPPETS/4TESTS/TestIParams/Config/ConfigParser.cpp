#include "../CommonStuff/Defines.h" /** delimiters macro */
#include "ConfigParser.h"

#include <iostream>
#include <sstream> /* stringstream */

ConfigParser::ConfigParser(std::string cfgName)
{
///TODO!!! call ifstream CTOR : config(cfgName.c_str()) throws SEGFAULT somewhere around std::string::size()!
    config.open(cfgName.c_str(), std::ios_base::in);
};

ConfigParser::~ConfigParser()
{
    if (config)
        config.close();
}

bool ConfigParser::fileExists()
{
    if (config && !config.bad()) {
        return true;
    }
    return false;
}

std::map<std::string, std::string> ConfigParser::parse()
{
    /** create map to populate with key=PARAM_NAME, value=PARAM_VALUE
    parsing cfg-file contents
    */
    std::map<std::string, std::string> params = std::map<std::string, std::string>();
    /** default CTOR ="" */
    std::string line, name, value;
    std::string delimiters = DEFAULT_DELIMITERS;
    while (!config.eof()) {
        /** replaced
         onfig >> line;
        with
        std::getline();
        to retrieve file contents line by line
        */
        line.clear();
        name.clear();
        value.clear();
        std::getline(config, line);
        /** remove leading/trailing delimiters from line */
        line = TrimStr(line,delimiters);
        /** remove COMMENTs from line */
        line = removeComment(line, DELIMITER_COMMENT_STARTS_WITH);
        /** if current line ends with '\\' parse MULTI-LINE value
        */
        if((line.size() > 0) && (line[line.size()-1] == DELIMITER_VALUE_CONTINUED_IN_NEXT_LINE )){
            do
            {
                line = line.substr(0, line.size()-1);
                line = TrimStr(line,delimiters);
                /** default CTOR ="" */
                std::string tmpStr;
                std::getline(config, tmpStr);
                tmpStr = TrimStr(tmpStr,delimiters);
                tmpStr = removeComment(tmpStr, DELIMITER_COMMENT_STARTS_WITH);
                if(!tmpStr.empty())
                    line += tmpStr;
            } while(line[line.size()-1] == DELIMITER_VALUE_CONTINUED_IN_NEXT_LINE);
        }
        /** split line into name-value pair by '=' - delimiter */
        size_t pos = line.find(DELIMITER_NAME_VALUE);
        if (pos == std::string::npos) {
            continue;
        }
        name = line.substr(0, pos);
        name = TrimStr(name,delimiters);
        value = line.substr(pos + 1);
        value = TrimStr(value,delimiters);
        /** put name-value into map */
        if ((!name.empty()) && (!value.empty())) {
            params[name] = value;
        }
    }
    return params;
}

std::string ConfigParser::setTrackNumToFileName(std::string fName, int trackId, int trackNum)
{
    if (trackNum == 1) {
        return fName;
    }
    std::string fNameNoExt, extension;
    unsigned pos = fName.find(DELIMITER_FILENAME_FILEEXT);

    if (pos != std::string::npos) {
        fNameNoExt = fName.substr(0, pos);
        extension = fName.substr(pos);
    }
    std::stringstream newFNameStr;
    newFNameStr << fNameNoExt << "_" << trackId << extension;
    return newFNameStr.str();
}

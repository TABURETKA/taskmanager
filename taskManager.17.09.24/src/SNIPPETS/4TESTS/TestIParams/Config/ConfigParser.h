#ifndef __CONFIG_PARSER_H_633569422542968750
#define __CONFIG_PARSER_H_633569422542968750

#include <string>
#include <fstream>
//OLD #include <list>
#include <map>
#include "../StringStuff/StringStuff.h" //TrimStr(), etc

// DON'T pollute global scope! using namespace std;


class ConfigParser
{
public:
    ConfigParser(std::string cfgName);
    ~ConfigParser();

    bool fileExists();
    std::map<std::string, std::string> parse();

    static std::string setTrackNumToFileName(std::string fName, int trackId, int trackNum);

private:
    std::ifstream config;
};

typedef std::map<std::string, std::string> ParamsMap;
typedef std::map<std::string, std::string>::iterator iteratorParamsMap;
#endif

#include "Model.h"
#include "../CommonStuff/Defines.h" //DELIMITER_TO_STRING_ITEMS
#include "../Log/errors.h"
#include "../DataCode/DataModel/DataMetaInfo.h" /* dataInfo */
///TODO!!!  Task ������� ������ �� �����!
///#include "../Task/Task.h"

#include "../Params/Params.h"
//OLD #include "../Params/AdditiveModelParams.h"
#include "../Distributions/Distributions.h"
#include <math.h> //NAN
#include <windows.h> // debug estimation RT
#include <math.h>
#include <cfloat>
#include <sstream>
#include <algorithm>

const char* IModel::modelNames[FAKE_MODEL]=
{
    "PLAIN_MODEL",
    "POLYNOMIAL_MODEL",
    "GAUSSIAN",
    "WIENER",
    "POISSON",
    "PARETO",
    "POISSON_DRVN_PARETO_JUMPS_EXPMEANREVERTING_MODEL",
//OLD    "POISSON_PARETO",
    "ADDITIVE_MODEL"
};
///static in IModel
enum IModel::ModelsTypes IModel::getModelTypeByModelName(const std::string& modelName)
{
    size_t i=0;
    for(i=0; i < IModel::ModelsTypes::FAKE_MODEL; ++i)
    {
        if (modelName == IModel::modelNames[i])
        {
            return (enum IModel::ModelsTypes)i;
        }
    }
    return IModel::ModelsTypes::FAKE_MODEL;
};
///TODO!!! ���� ������ ��� �������? ��������������� �������, ������� ����� ��� ���������� DataInfo'�� � ������� IParams ��� IParamsUnion ���������� ������!
/// extra stuff for the following int IModel::parseDataInfoAndCreateModelParams(const enum IModel::ModelsTypes mt, size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams, Logger* const logger)
int findModelNameInDataInfoArray(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, const char * modelName)
{
    int index = -1;
    for(size_t i=0; i< totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dataModelName == modelName)
        {
            return i;
        }
    }
    return index;
}
/***********************************************
* Base abstract IModel class implementation
************************************************/
/** anonymous namespace to hide private implementations into this cpp-file */
namespace
{
/** class container */
/** implementation of abstract Base class IModel */
class IModel_Impl : public virtual IModel
{
public:
    /** Public CTOR ! BUT it is visible into this cpp-file only! */
    IModel_Impl(const size_t& dim);
    virtual ~IModel_Impl();
    /** ��� ��� ������ ����������� � ���� ������� ������! */
    virtual std::string toString() const;

///TODO!!!  Task ������� ������ �� �����!
///    virtual void setTask(Task* const & task);
    virtual void setLog(Logger* const & log);
    virtual void getModelType(enum IModel::ModelsTypes& modelType) const;
    /** to parse DataInfo
    and transform double** to IParams1D
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const;

    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
    /** ���� ������ �������� size, �.�. ���������� ���������� ������ ���� �����,
    ����� ������ ������� ���������� ��������� ������
    �� �������� �������� � �������������� �������,
    ��� ��� �������� ��������� � ������� ����� ������� ������ ������ ������!
    � ����������� ������ ���������� �������������� ������
    ����� ��������� ������ ������� ���� ������!
    */
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
protected:
    virtual int createEmptyParamsOfModel(IParams*& ptrToPtrIparams, const size_t dim) const;
    ///TOOD!!! ����� ����, ��� ����� ������ � ������ �����, ��� ������� ���� ����� ���� ������ ������������ � �������?
    void parsingFailed(std::string msg) const;
    /** dimension of Any Parametric Model's Params space */
    size_t _dimension;
///TODO!!!  Task ������� ������ �� �����!
/// Task* _task;
    Logger* _logger;
    /** _modelType must be defined ONLY ONCE!
    In CTOR()! _modelType REDEFINITION is forbidden!
    */
    enum IModel::ModelsTypes _modelType;
private:

    /** assign and copy CTORs are disabled */
    IModel_Impl(const IModel_Impl&);
    void operator=(const IModel_Impl&);
};

/** implementation of abstract Base class IModelPlain */
class IModelPlain_Impl : public virtual IModel_Impl,
    public virtual IModelPlain
{
public:
    /** Public CTOR ! BUT it is visible into this cpp-file only! */
    IModelPlain_Impl(const size_t& dim);
    virtual ~IModelPlain_Impl();
//factory
    /** ��� ��� ������ ����������� � ���� ������� ������! */
    using IModel_Impl::toString;
///TODO!!!  Task ������� ������ �� �����!
///    using IModel_Impl::setTask;
    using IModel_Impl::setLog;
    using IModel_Impl::getModelType;
    using IModel_Impl::parseAndCreateModelParams;
    /** ���� ������ �������� size, �.�. ���������� ���������� ������ ���� �����,
    ����� ������ ������� ���������� ��������� ������
    �� �������� �������� � �������������� �������,
    ��� ��� �������� ��������� � ������� ����� ������� ������ ������ ������!
    � ����������� ������ ���������� �������������� ������
    ����� ��������� ������ ������� ���� ������!
    */
    using IModel_Impl::canCastModelParams;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParams1D
    */
    using IModel_Impl::parseDataInfoAndCreateModelParams;

    void setDimension(const size_t& dim);
    int getDimension(size_t& dim) const;

protected:
    using IModel_Impl::createEmptyParamsOfModel;
    ///TOOD!!! ����� ����, ��� ����� ������ � ������ �����, ��� ������� ���� ����� ���� ������ ������������ � �������?
    using IModel_Impl::parsingFailed;
    /** _modelType must be defined ONLY ONCE!
    In CTOR()! _modelType REDEFINITION is forbidden!
    */
private:
    /** assign and copy CTORs are disabled */
    IModelPlain_Impl(const IModelPlain_Impl&);
    void operator=(const IModelPlain_Impl&);
};

/**************************************************
* Base abstract IModelPolynomial class implementation
***************************************************/
class IModelPolynomial_Impl : public virtual IModel_Impl,
    public virtual IModelPolynomial
{
public:
    static IModelPolynomial_Impl* createEmptyModel(const size_t& dim);
    virtual void setDimension(const size_t& dim);
    virtual int getDimension(size_t& dim) const;
    /** ���� ������ �������� size,
     �.�. ���������� ���������� �������������� ������
     ������ ���� �����, ����� ������ ������� ���������� ��������� ������
     �.�. ����������� �������������� ������ ������ ���� � size!
    */
    virtual int createEmptyParamsOfModel(IParams*& ptrToPtrIparams) const;
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParams1D
    */
    using IModel_Impl::parseDataInfoAndCreateModelParams;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;

private:
    /** No public ctor! Factory is in use! */
    IModelPolynomial_Impl(const size_t& dim);
    /** assign and copy CTORs are disabled */
    IModelPolynomial_Impl(const IModelPolynomial_Impl&);
    void operator=(const IModelPolynomial_Impl&);
};

/**************************************************
* Base abstract IModelGaussian class implementation
***************************************************/
class IModelGaussian_Impl : public virtual IModel_Impl,
    public virtual IModelGaussian,
    public virtual I_PF_Model
{
public:
    static IModelGaussian_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToPtrIparams) const;
//OLD    virtual int parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams) const;
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsGaussian
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_PF_Model interface implementation */
    virtual double estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
    virtual double estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;
///TODO!!!
    virtual int initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TODO!!!
    virtual int generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;

private:
    /** No public ctor! Factory is in use! */
    IModelGaussian_Impl();
    /** assign and copy CTORs are disabled */
    IModelGaussian_Impl(const IModelGaussian_Impl&);
    void operator=(const IModelGaussian_Impl&);
    int ptrAdditiveModelParamsToPtrParamsWiener(const IParamsUnion* h, IParamsGaussian*& pw);
};
/**************************************************
* Base abstract IModelWiener class implementation
***************************************************/
class IModelWiener_Impl : public virtual IModel_Impl,
    public virtual IModelWiener,
    public virtual I_PF_Model
{
public:
    static IModelWiener_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToPtrIparams) const;
//OLD    virtual int parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams) const;
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo and transform double** to IParamsWiener */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_PF_Model interface implementation */
    double estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
    double estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;
///TODO!!!
    virtual int initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TODO!!!
    virtual int generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;

private:
    /** No public ctor! Factory is in use! */
    IModelWiener_Impl();
    /** assign and copy CTORs are disabled */
    IModelWiener_Impl(const IModelWiener_Impl&);
    void operator=(const IModelWiener_Impl&);
    int ptrAdditiveModelParamsToPtrParamsWiener(const IParamsUnion* h, IParamsWiener*& pw);
};
/**************************************************
* Base abstract IModelPareto class implementation
***************************************************/
class IModelPareto_Impl : public virtual IModel_Impl,
    public virtual IModelPareto,
    public virtual I_PF_Model
{
public:
    static IModelPareto_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToPtrIparams) const;
//OLD    virtual int parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams) const;
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsPareto
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_PF_Model interface implementation */
    int initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
    int generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TOOD!!!
    virtual double estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
///TODO!!!
    virtual double estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;

///TODO!!! move to IntervalEstimator bool estimateAndFillParamBoundaries(const vector<double>& points, double deviation, double regression, IParams* start, IParams* end);
private:
    /** No public ctor! Factory is in use ! */
    IModelPareto_Impl();
    /** assign and copy CTORs are disabled */
    IModelPareto_Impl(const IModelPareto_Impl&);
    void operator=(const IModelPareto_Impl&);
    int ptrAdditiveModelParamsToPtrParamsPareto(const IParamsUnion* h, IParamsPareto*& ppp);
};

/**************************************************
* Base abstract IModelPoisson class implementation
***************************************************/
class IModelPoisson_Impl : public virtual IModel_Impl,
    public virtual IModelPoisson
{
public:
    static IModelPoisson_Impl* createEmptyModel();
    virtual int createEmptyParamsOfModel(IParams*& ptrToPtrIparams) const;
//OLD    virtual int parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams) const;

    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsPoisson
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
private:
    /** No public ctor! Factory is in use ! */
    IModelPoisson_Impl();
    /** assign and copy CTORs are disabled */
    IModelPoisson_Impl(const IModelPoisson_Impl&);
    void operator=(const IModelPoisson_Impl&);
};

/*******************************************************
* Base abstract IModelPoissonDrvnParetoJumpsExpMeanReverting class implementation
********************************************************/
class IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl : public virtual IModel_Impl,
    public virtual IModelPoissonDrvnParetoJumpsExpMeanReverting,
    public virtual I_PF_Model
{
public:
    static IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl* createEmptyModel();
//OLD    virtual int parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams) const;
    virtual int createEmptyParamsOfModel(IParams*& ptrToPtrIparams) const;
    virtual bool canCastModelParams(IParams * params) const;
    virtual size_t getModelParamsType() const;
    /** to parse DataInfo
    and transform double** to IParamsPoissonDrvnParetoJumpsExpMeanReverting
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const;

    using IModel_Impl::getModelType;
    using IModel_Impl::toString;
    /** I_PF_Model interface implementation */
    int initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
    int generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
///TOOD!!!
    virtual double estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
///TODO!!!
    virtual double estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;

///TODO!!! move to IntervalEstimator bool estimateAndFillParamBoundaries(const vector<double>& points, double deviation, double regression, IParams* start, IParams* end);
private:
    /** No public ctor! Factory is in use ! */
    IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    /** assign and copy CTORs are disabled */
    IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl(const IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
    void operator=(const IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
    int ptrAdditiveModelParamsToPtrParamsPoissonDrvnParetoJumpsExpMeanReverting(const IParamsUnion* h, IParamsPoissonDrvnParetoJumpsExpMeanReverting*& ppp);
};

}; ///end namespace

/******************** IMPLEMENTATIONs ****************************/
/****************************************************************
*   Base class - implementation
*****************************************************************/
///static IModel_Impl!
/** ���������� ������, ��� �������� ����������� Params */
int IModel_Impl::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPoissonDrvnParetoJumpsExpMeanReverting* params = IParamsPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::Params1D, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse Params1D");
        return _ERROR_WRONG_ARGUMENT_;
    }
    size =1;
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
};

IModel_Impl::IModel_Impl(const size_t& dim)
{
    _dimension = dim;
    _logger    = NULL;
    _modelType = FAKE_MODEL;
}

/***************************
*        DTOR()
***************************/
IModel_Impl::~IModel_Impl()
{
    /* NO WAY!
        if ( _task != 0) {
            delete _task;
        }
        if (_logger != 0) {
            delete _logger;
        }
    */
};

std::string IModel_Impl::toString() const
{
    /* OLD REDUNDANT    std::stringstream str;
           str <<  IModel::modelNames[_modelType]; //WRONG! << std::endl;
          return str.str();
    */
    return (std::string) IModel::modelNames[_modelType];
}
/// Invariant for every model
/*OLD REDUNDANT
void IModel_Impl::setTask(Task* const & task)
{
    this->_task = task;
}
*/
/// Invariant for every model
void IModel_Impl::setLog(Logger* const & log)
{
    this->_logger = log;
}

void IModel_Impl::getModelType(enum IModel::ModelsTypes& modelType) const
{
    modelType = this->_modelType;
};
/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModel_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::FAKE_PARAMS;
};

/** to parse DataInfo and transform double** to IParams1D */
int IModel_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const
{

    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = ptrToPtrDI[0]->m_dataSetSizes[0];
    IParams * ptrIParams1D = IParams::createEmptyParams(_logger, IParams::ParamsTypes::Params1D, size);
    if(ptrIParams1D == NULL)
    {
        delete ptrIParams1D;
        return _ERROR_NO_ROOM_;
    }

    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParams1D;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� ����������� ���� Params ���������!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParams1D;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
            if(!isNAN_double(tmpDbl))
                ptrIParams1D->setParam(j,tmpDbl);
        }
    }
    ptrToPtrIparams = ptrIParams1D;
    return _RC_SUCCESS_;
};
///TOOD!!! ����� ����, ��� ����� ������ � ������ �����, ��� ������� ���� ����� ���� ������ ������������ � �������?
void IModel_Impl::parsingFailed(std::string msg) const
{
//"Failed to parse parameters"
    this->_logger->error(ALGORITHM_MODELDATA_ERROR, msg);
///TODO!!!    this->_task->finish(CONFIG_FAILED, msg);
}

int IModel_Impl::createEmptyParamsOfModel(IParams*& ptrToPtrIParams, const size_t dim) const
{
    IParams * ptrIParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::Params1D, dim);
    if( ptrIParams == NULL )
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToPtrIParams = ptrIParams;
    return _RC_SUCCESS_;
};
/** IParams always to be casted to Params1D - the simplest Params reincarnation. */
bool IModel_Impl::canCastModelParams(IParams * params) const
{
    if (!params)
        return false;
    else
        return true;
};
/****************************************************************
* Plain IModel class implementation
****************************************************************/
/***************************
*        CTOR()
***************************/
IModelPlain_Impl::IModelPlain_Impl(const size_t& dim) : IModel_Impl(dim)
{
    _modelType = PlainModel;
}
/***************************
*        DTOR()
***************************/
IModelPlain_Impl::~IModelPlain_Impl()
{};

void IModelPlain_Impl::setDimension(const size_t& dim)
{
    _dimension = dim;
};
int IModelPlain_Impl::getDimension(size_t& dim) const
{
    dim = _dimension;
    return _RC_SUCCESS_;
};

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPlain_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::Params1D;
};

/****************************************************************
* Polynomial IModel class implementation
*****************************************************************/
///static in IModelPolynomial!
int IModelPolynomial::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * logger)
{
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::Params1D, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse Polynomial Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
///So far size == total of IParams  which have been initilaize
    size =1;
    *ptrToPtrIparams = params;
    return _RC_SUCCESS_;
}
///static in IModelPolynomial!
IModelPolynomial* IModelPolynomial::createEmptyModel(const size_t& dim)
{
    return IModelPolynomial_Impl::createEmptyModel(dim);
}

IModelPolynomial_Impl* IModelPolynomial_Impl::createEmptyModel(const size_t& dim)
{
    return new IModelPolynomial_Impl(dim);
}
void IModelPolynomial_Impl::setDimension(const size_t& dim)
{
    _dimension = dim;
};
int IModelPolynomial_Impl::getDimension(size_t& dim) const
{
    dim = _dimension;
    return _RC_SUCCESS_;
};

int IModelPolynomial_Impl::createEmptyParamsOfModel(IParams*& ptrToPtrIParams) const
{
    IParams * ptrIParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::Params1D, _dimension);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToPtrIParams = ptrIParams;
    return _RC_SUCCESS_;
};

bool IModelPolynomial_Impl::canCastModelParams(IParams * params) const
{
    return (params != NULL);
};

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPolynomial_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::Params1D;
};

IModelPolynomial_Impl::IModelPolynomial_Impl(const size_t& dim) : IModel_Impl(dim)
{
    _modelType = PolynomialModel;
}

///static in IModelPolynomial
int IModelPolynomial::evaluatePolynom(const double arg, const IParams*& coeffs, double& value)
{
    if(coeffs == NULL)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t dim = coeffs->getSize();
    if(dim == 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    value = 0;
    for (int i = dim-1; i >= 0; --i)
    {
        value *= arg;
        double coef=0;
        coeffs->getParam(i, coef);
        value += coef;
    }
    return _RC_SUCCESS_;
};

/****************************************************************
* Gaussian IModel class implementation
*****************************************************************/
///static in IModelGaussian!
int IModelGaussian::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * logger)
{
//    IParamsWiener* params = IParamsWiener::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::GaussianParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse GaussianParams");
        return _ERROR_WRONG_ARGUMENT_;
    }
    size =1;
    *ptrToPtrIparams = params;
    return _RC_SUCCESS_;
}

int IModelGaussian_Impl::createEmptyParamsOfModel(IParams*& ptrToPtrIParams) const
{
    /** for GaussianParams param 'size' DOES NOT matter!
    size = IParamsGaussian::GaussianCDFParamsDimension;
    */
    IParams * ptrIParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::GaussianParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToPtrIParams = ptrIParams;
    return _RC_SUCCESS_;
};

bool IModelGaussian_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsGaussian *>(params) != NULL);
};

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelGaussian_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::GaussianParams;
};

/** to parse DataInfo and transform double** to IParamsGaussian */
int IModelGaussian_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const
{

    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsGaussian * ptrIParamsGaussian = dynamic_cast<IParamsGaussian *>(IParams::createEmptyParams(_logger, IParams::ParamsTypes::GaussianParams));
    if(ptrIParamsGaussian == NULL)
    {
        delete ptrIParamsGaussian;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsGaussian->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsGaussian;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� GaussianParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsGaussian;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsGaussian �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsGaussian->setParam(j,tmpDbl);
        }
    }
    ptrToPtrIparams = ptrIParamsGaussian;
    return _RC_SUCCESS_;
};

IModelGaussian_Impl::IModelGaussian_Impl() : IModel_Impl(IParamsGaussian::GaussianCDFParamsDimension)
{
    _modelType = GaussianModel;
}

IModelGaussian_Impl* IModelGaussian_Impl::createEmptyModel()
{
    return new IModelGaussian_Impl();
}
///����� ���������� estimateHypothesis �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsGaussian!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelGaussian_Impl::estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
    if (h == 0)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in estimateHypothesis()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing Gaussian model parameters in estimateHypothesis()");
        return NAN;
    }
///OLD    IParamsGaussian* hyp = (IParamsGaussian*)h;
    size_t size=0;
    int rc = h->getTotalOfItemsInParamsUnion(size);
    if ((rc) || (size <= indexOfParams))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Gaussian model parameters not found in estimateHypothesis()");
///TODO!!!        _task->finish(MODEL_ERROR, "Wrong size of Gaussian model parameters in estimateHypothesis()");
        return NAN;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsGaussian!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsGaussian* hyp = dynamic_cast<IParamsGaussian*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Gaussian model parameters in estimateHypothesis()");
        _task->finish(MODEL_ERROR, "Wrong Gaussian model parameters in estimateHypothesis()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif

    unsigned particlesNum = particles.size();

    std::vector<double> particleProbability(particlesNum, 0);
    double expectationalPoint = 0;
    double pointProbability = 0;

    unsigned i;

    for (i = 0; i < particlesNum; ++i)
    {
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
        particleProbability[i] = Distributions::Phi_pdf(point - particles[i], hyp->mu(), hyp->sigma());
        pointProbability += particleProbability[i];
    }

    if (pointProbability == 0)
    {
        return -LDBL_MAX;
    }

    for (i = 0; i < particlesNum; ++i)
        expectationalPoint += particles[i] * particleProbability[i] / pointProbability;
///TODO!!! for GAUSSIAN  merely remove "* sqrt((double)t + 1.)" below
    double estimate = log(Distributions::Phi_pdf(point - expectationalPoint, hyp->mu(), hyp->sigma()));

    return estimate;
}

///����� ���������� estimatePointMove �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsGaussian!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelGaussian_Impl::estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
    if (h == 0)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Gaussian model parameters in estimateHypothesis()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing Gaussian model parameters in estimateHypothesis()");
        return NAN;
    }
///OLD    IParamsGaussian* hyp = (IParamsGaussian*)h;
    size_t size=0;
    int rc = h->getTotalOfItemsInParamsUnion(size);
    if ((rc) || (size <= indexOfParams))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Gaussian model parameters NOT FOUND in estimatePointMove()");
///TODO!!!        _task->finish(MODEL_ERROR, "Wrong size of Gaussian model parameters in estimateHypothesis()");
        return NAN;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsGaussian!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsGaussian* hyp = dynamic_cast<IParamsGaussian*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Gaussian model parameters in estimateHypothesis()");
        _task->finish(MODEL_ERROR, "Wrong Gaussian model parameters in estimateHypothesis()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif

///OLD    IParamsGaussian* hyp = (IParamsGaussian*)h;
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
    return Distributions::Phi_pdf(move, hyp->mu(), hyp->sigma());
}
///TODO!!!
int IModelGaussian_Impl::initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� initParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented initParticles() in Gaussian model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented initParticles() in Gaussian model");
};
///TODO!!!
int IModelGaussian_Impl::generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� generateParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?}
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented generateParticles() in Gaussian model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented generateParticles() in Gaussian model");
};
/**
* static method to estimate Gaussian's 0.999-quantile
* PARAMS [IN] sampleOfOrderedAbsItems -- abs values of gaussian r.v., NB ordered by ASC
* PARAMS [OUT]  quantile -- So far 0.999-quantile ONLY!
* Returns _RC_SUCCESS_ if success
         _ERROR_WRONG_INPUT_DATA_ otherwise
*/
int IModelGaussian::Quantil0999(const std::vector<double>& sampleOfOrderedAbsItems, double& quantile)
{
    for(size_t i=0; i < sampleOfOrderedAbsItems.size()-1; ++i)
    {
        if(sampleOfOrderedAbsItems[i] > sampleOfOrderedAbsItems[i+1])
        {
            quantile = NAN;
            return _ERROR_WRONG_INPUT_DATA_;
        }
    }
    int n = sampleOfOrderedAbsItems.size();
    double mediana; // Quantile 0.674 of N(0,1) is 0.75, which is mediana of N+(0, 1)

    if (n % 2 == 0)
    {
        mediana = sampleOfOrderedAbsItems[n / 2];
    }
    else
    {
        mediana = (sampleOfOrderedAbsItems[n / 2] + sampleOfOrderedAbsItems[n / 2 +1]) / 2.;
    }
    // Evaluate 0.999-quantile
    // 0.999-quantile for N(0,1) is equal 3.090, which is 0.998-quantile for N+(0, 1)
    // 3.090 / 0.674 = 4.5845 ~ 5.;
    quantile = mediana * 5.;
    return _RC_SUCCESS_;
};
/****************************************************************
* Wiener IModel class implementation
*****************************************************************/
///static in IModelWiener!
int IModelWiener::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * logger)
{
//    IParamsWiener* params = IParamsWiener::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::WienerParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse ParamsWiener");
        return _ERROR_WRONG_ARGUMENT_;
    }
///This size merely totla of ptrToPtrIparams which have been initialized
    size =1;
    *ptrToPtrIparams = params;
    return _RC_SUCCESS_;
}

int IModelWiener_Impl::createEmptyParamsOfModel(IParams*& ptrToPtrIParams) const
{

    /** for WienerParams param 'size' DOES NOT matter!
     size = IParamsWiener::WienerCDFParamsDimension;
     */
    IParams * ptrIParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::WienerParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToPtrIParams = ptrIParams;
    return _RC_SUCCESS_;
};

bool IModelWiener_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsWiener *>(params) != NULL);
};

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelWiener_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::WienerParams;
};
/** to parse DataInfo and transform double** to IParamsGaussian */
int IModelWiener_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const
{
    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsWiener * ptrIParamsWiener = dynamic_cast<IParamsWiener *>(IParams::createEmptyParams(_logger, IParams::ParamsTypes::WienerParams));
    if(ptrIParamsWiener == NULL)
    {
        delete ptrIParamsWiener;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsWiener->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsWiener;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� WienerParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsWiener;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsWiener �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsWiener->setParam(j,tmpDbl);
        }
    }
    ptrToPtrIparams = ptrIParamsWiener;
    return _RC_SUCCESS_;
};


IModelWiener_Impl::IModelWiener_Impl() : IModel_Impl(IParamsWiener::WienerCDFParamsDimension)
{
    _modelType = WienerModel;
}

IModelWiener_Impl* IModelWiener_Impl::createEmptyModel()
{
    return new IModelWiener_Impl();
}
///����� ���������� estimateHypothesis �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsWiener!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelWiener_Impl::estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
    if (h == 0)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in estimateHypothesis()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing Wiener model parameters in estimateHypothesis()");
        return NAN;
    }

///OLD    IParamsWiener* hyp = (IParamsWiener*)h;
    size_t size=0;
    int rc = h->getTotalOfItemsInParamsUnion(size);
    if ((rc) || (size <= indexOfParams))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Wiener model parameters NOT FOUND in estimateHypothesis()");
///TODO!!!        _task->finish(MODEL_ERROR, "Wrong size of Wiener model parameters in estimateHypothesis()");
        return NAN;
    }

    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsWiener!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsWiener* hyp = dynamic_cast<IParamsWiener*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Wiener model parameters in estimateHypothesis()");
        _task->finish(MODEL_ERROR, "Wrong Wiener model parameters in estimateHypothesis()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif

    unsigned particlesNum = particles.size();

    std::vector<double> particleProbability(particlesNum, 0);
    double expectationalPoint = 0;
    double pointProbability = 0;

    unsigned i;

    for (i = 0; i < particlesNum; ++i)
    {
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
        particleProbability[i] = Distributions::Phi_pdf(point - particles[i] - hyp->initialCondition(), hyp->mu(), hyp->sigma() * sqrt((double)t + 1.));
        pointProbability += particleProbability[i];
    }

    if (pointProbability == 0)
    {
        return -LDBL_MAX;
    }

    for (i = 0; i < particlesNum; ++i)
    {
        double weightOfParticle = particleProbability[i] / pointProbability;
        expectationalPoint += weightOfParticle * particles[i];
    }
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
    double estimate = log(Distributions::Phi_pdf(point - expectationalPoint, hyp->mu(), hyp->sigma() * sqrt((double)t + 1.)));

    return estimate;
}

///����� ���������� estimatePointMove �� I_PF_Model ���������� ����! ������
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsWiener!
 � ��� ������������� ����� ��������� � ���� ����!
*/
double IModelWiener_Impl::estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
    if (h == 0)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Wiener model parameters in estimateHypothesis()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing Wiener model parameters in estimateHypothesis()");
        return NAN;
    }
///OLD    IParamsWiener* hyp = (IParamsWiener*)h;
    size_t size=0;
    int rc = h->getTotalOfItemsInParamsUnion(size);
    if ((rc) || (size <= indexOfParams))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Wiener model parameters NOT FOUND in estimatePointMove()");
///TODO!!!        _task->finish(MODEL_ERROR, "Wrong size of Wiener model parameters in estimateHypothesis()");
        return NAN;
    }
    /**
     No clones!
     */
    /** �� ������� indexOfParams � IParamsUnion
      ������ ������ ��������� �� ��������� ���� ������ - IParamsWiener!
      � ��� ������������� ����� ����� ���������
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsWiener* hyp = dynamic_cast<IParamsWiener*> (ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong Wiener model parameters in estimateHypothesis()");
        _task->finish(MODEL_ERROR, "Wrong Wiener model parameters in estimateHypothesis()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
///OLD    IParamsWiener* hyp = (IParamsWiener*)h;
///TODO!!! ��� ����� ����� ���������� ���������� ������ evaluatePDF!!!
    return Distributions::Phi_pdf(move, hyp->mu(), hyp->sigma() * sqrt((double)t + 1.));
}
///TODO!!!
int IModelWiener_Impl::initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� initParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented initParticles() in Wiener model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented initParticles() in Wiener model");
};
///TODO!!!
int IModelWiener_Impl::generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
///TODO!!! ����� �� ����� ������� generateParticles �� I_PF_Model ���������� ������ ��� Using �� ������� ����������?}
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented generateParticles() in Wiener model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented generateParticles() in Wiener model");
};


/*****************************************************************
*   Pareto IModel class implementation
******************************************************************/
///static in IModelPareto!
int IModelPareto::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPareto* params = IParamsPareto::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::ParetoParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse ParamsPareto");
        return _ERROR_WRONG_ARGUMENT_;
    }
    size =1;
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}

int IModelPareto_Impl::createEmptyParamsOfModel(IParams*& ptrToPtrIParams) const
{
    /** for ParetoParams param 'size' DOES NOT matter!
      size = IParamsPareto::ParetoCDFParamsDimension;
    */
    IParams * ptrIParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::ParetoParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToPtrIParams = ptrIParams;
    return _RC_SUCCESS_;
};

bool IModelPareto_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsPareto *>(params) != NULL);
};

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPareto_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::ParetoParams;
};

/** to parse DataInfo and transform double** to IParamsPareto */
int IModelPareto_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const
{
    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPareto * ptrIParamsPareto = dynamic_cast<IParamsPareto *>(IParams::createEmptyParams(_logger, IParams::ParamsTypes::ParetoParams));
    if(ptrIParamsPareto == NULL)
    {
        delete ptrIParamsPareto;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsPareto->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsPareto;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� GaussianParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsPareto;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsPareto �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsPareto->setParam(j,tmpDbl);
        }
    }
    ptrToPtrIparams = ptrIParamsPareto;
    return _RC_SUCCESS_;
};

IModelPareto_Impl::IModelPareto_Impl() :  IModel_Impl(IParamsPareto::ParetoCDFParamsDimension)
{
    _modelType = ParetoModel;
}
IModelPareto_Impl* IModelPareto_Impl::createEmptyModel()
{
    return new IModelPareto_Impl();
}
/// ����� ���������� initParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
/** indexOfParams - ��� ������ ���� �������� � IParamsUnion
 �� �������� ������ ������ ��������� �� ��������� ���� ������ - IParamsPareto!
 � ��� ������������� ����� ��������� � ���� ����!
*/
int IModelPareto_Impl::initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    if (h == 0)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in initParticles()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size=0;
    int rc = h->getTotalOfItemsInParamsUnion(size);
    if ((rc) || (size <= indexOfParams))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! Pareto model parameters NOT FOUND in initParticles()");
///TODO!!!        _task->finish(MODEL_ERROR, "Wrong size of Pareto model parameters in initParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }

    /**
     No clones!
     */
    /** ������-�� ������ indexOfParams ������ � �������!
    ������ ��� ������ �������� ���� ���������
    � 0-� �������� IParamsUnion
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsPareto* hyp = dynamic_cast<IParamsPareto*>(ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        _task->finish(MODEL_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
    unsigned particlesNum = particles.size();

    for (unsigned i = 0; i < particlesNum; ++i)
    {
///TODO!!! ����� ������ ���� ����� ���������� generateRV
        particles[i] = Distributions::Pareto(hyp->xM(), hyp->k());
    }
    return _RC_SUCCESS_;
}

/// ����� ���������� generateParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
int IModelPareto_Impl::generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    if (h == NULL)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing Pareto model parameters in generateParticles()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing Pareto model parameters in generateParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }

///OLD    IParamsPareto* hyp = (IParamsPareto*)h;
    /**
     No clones!
     */
    /*** TODO!!! � �������� indexOfParams!
     �� ������ � �������!
      ������ ��� ������ �������� ���� ���������
      � 0-� �������� IParamsUnion
    TODO!!!
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsPareto* hyp = dynamic_cast<IParamsPareto*>(ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateParticles()");
        _task->finish(MODEL_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
    unsigned particlesNum = particles.size();

    for (unsigned i = 0; i < particlesNum; ++i)
    {
        particles[i] += Distributions::Pareto(hyp->xM(), hyp->k());
    }
    return _RC_SUCCESS_;
}
///TOOD!!!
double IModelPareto_Impl::estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimateHypothesis �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimateHypothesis() in Pareto model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimateHypothesis() in Pareto model");
};
///TODO!!!
double IModelPareto_Impl::estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimatePointMove �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimatePointMove() in Pareto model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimatePointMove() in Pareto model");

};
/*****************************************************************
*   Poisson IModel class implementation
******************************************************************/
///static in IModelPoisson!
int IModelPoisson::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPoisson* params = IParamsPoisson::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::PoissonParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to parse ParamsPoisson");
        return _ERROR_WRONG_ARGUMENT_;
    }
    size =1;
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}

int IModelPoisson_Impl::createEmptyParamsOfModel(IParams*& ptrToPtrIParams) const
{
    /** for PoissonParams param 'size' DOES NOT matter!
      size = IParamsPoisson::PoissonCDFParamsDimension;
    */
    IParams * ptrIParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::PoissonParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToPtrIParams = ptrIParams;
    return _RC_SUCCESS_;
};

bool IModelPoisson_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsPoisson *>(params) != NULL);
};

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPoisson_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::PoissonParams;
};

/** to parse DataInfo and transform double** to IParamsPoisson */
int IModelPoisson_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const
{
    if((totalOfDataInfo == 0) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPoisson * ptrIParamsPoisson = dynamic_cast<IParamsPoisson *>(IParams::createEmptyParams(_logger, IParams::ParamsTypes::PoissonParams));
    if(ptrIParamsPoisson == NULL)
    {
        delete ptrIParamsPoisson;
        return _ERROR_NO_ROOM_;
    }
    size_t size = ptrIParamsPoisson->getSize();
    for(size_t i =0; i < totalOfDataInfo; ++i)
    {
        if(ptrToPtrDI[i]->m_dimension != 1)
        {
            delete ptrIParamsPoisson;
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ���������, ��� PoissonParams!
        size_t trackSize = ptrToPtrDI[i]->m_dataSetSizes[0];
        if(trackSize != size)
        {
            delete ptrIParamsPoisson;
            return _ERROR_WRONG_ARGUMENT_;
        }
        for(size_t j=0; j < size; ++j)
        {
            double tmpDbl = ptrToPtrDI[i]->m_dataSet[0][j];
///TODO!!! ������ ��� ������ ������� �� ��������, ����� ��� ����� � ��������� �������� di[] ��������� ��-NAN �������� ��������� � ��������� �� ��, ��������, ��� ���� � IParamsPoisson �� �����
            if(!isNAN_double(tmpDbl))
                ptrIParamsPoisson->setParam(j,tmpDbl);
        }
    }
    ptrToPtrIparams = ptrIParamsPoisson;
    return _RC_SUCCESS_;
};

IModelPoisson_Impl::IModelPoisson_Impl() :  IModel_Impl(IParamsPoisson::PoissonCDFParamsDimension)
{
    _modelType = PoissonModel;
}
IModelPoisson_Impl* IModelPoisson_Impl::createEmptyModel()
{
    return new IModelPoisson_Impl();
}
/*****************************************************************
*   Poisson-Pareto IModel class implementation
******************************************************************/
const char * IModelPoissonDrvnParetoJumpsExpMeanReverting::strExpMeanRevertingLambda = "ExpMeanRevertingLambda";
///static in IModelPoissonDrvnParetoJumpsExpMeanReverting!
int IModelPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * logger)
{
//    IParamsPoissonDrvnParetoJumpsExpMeanReverting* params = IParamsPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateParams(mapParams, postfix);
    IParams* params = (IParams::parseAndCreateParams(IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams, mapParams, postfix, logger));
    if (params == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Failed to par+se ParamsPoissonDrvnParetoJumpsExpMeanReverting");
        return _ERROR_WRONG_ARGUMENT_;
    }
    size =1;
    *ptrToPtrIParams = params;
    return _RC_SUCCESS_;
}
/**
* PARAMS [OUT] IParams*& ptrToPtrIParams -- array of ptrs to IPoissonDrvnParetoJumpsExpMeanRevertingParams
*/
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyParamsOfModel(IParams*& ptrToPtrIParams) const
{
    /** for PoissonParams */
    IParams * ptrIParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams);
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ptrToPtrIParams = ptrIParams;
    return _RC_SUCCESS_;
};

bool IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::canCastModelParams(IParams * params) const
{
    return (dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting *>(params) != NULL);
};

/** ��� � ���� ������� ��� ���� �����, ��� �����������
��� ������ ��� ���� ������ ����������
*/
size_t IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::getModelParamsType() const
{
    return IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams;
};

/** to parse DataInfo
and transform double** to IParamsPoissonDrvnParetoJumpsExpMeanReverting
*/
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const
{
    if((totalOfDataInfo != 3) || (ptrToPtrDI == NULL))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParamsPareto * ptrIParamsPareto = dynamic_cast<IParamsPareto *>(IParams::createEmptyParams(_logger, IParams::ParamsTypes::ParetoParams));
    if(ptrIParamsPareto  == NULL)
    {
        delete ptrIParamsPareto;
        return _ERROR_NO_ROOM_;
    }
    /** ������ ����� ������ ���� DI, ������� �������� ������
    */
    int index = findModelNameInDataInfoArray(totalOfDataInfo, ptrToPtrDI, IModel::modelNames[ParetoModel]);
    if(index < 0 )
    {
        delete ptrIParamsPareto;
        return _ERROR_WRONG_MODEL_PARAMS_SET_;
    }
    size_t dimOfDI= ptrToPtrDI[index]->m_dimension;
    if(dimOfDI != 1)
    {
        delete ptrIParamsPareto;
        return _ERROR_WRONG_ARGUMENT_;
    }
    int rc = ptrIParamsPareto->setParams((const size_t)ptrToPtrDI[index]->m_dataSetSizes[0], (const double *) ptrToPtrDI[index]->m_dataSet[0]);
    if(rc != _RC_SUCCESS_)
    {
        delete ptrIParamsPareto;
        return rc;
    }

    IParamsPoisson * ptrIParamsPoisson = dynamic_cast<IParamsPoisson *> (IParams::createEmptyParams(_logger, IParams::ParamsTypes::PoissonParams));
    if(ptrIParamsPoisson  == NULL)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_NO_ROOM_;
    }
    /** ����� ������ ���� DI, ������� �������� ��������
    */
    index = findModelNameInDataInfoArray(totalOfDataInfo, ptrToPtrDI, IModel::modelNames[PoissonModel]);
    if(index < 0 )
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_WRONG_MODEL_PARAMS_SET_;
    }
    dimOfDI= ptrToPtrDI[index]->m_dimension;
    if(dimOfDI != 1)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_WRONG_ARGUMENT_;
    }
    rc = ptrIParamsPoisson->setParams((const size_t)ptrToPtrDI[index]->m_dataSetSizes[0], (const double *) ptrToPtrDI[index]->m_dataSet[0]);
    if(rc != _RC_SUCCESS_)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return rc;
    }

    IParams* ptrIExpMeanRevertingParams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::Params1D, 1);
    if(ptrIExpMeanRevertingParams  == NULL)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        return _ERROR_NO_ROOM_;
    }
///TODO!!! �������� ���� ������� � ����������� ���� ������! ��� ����� �������
///TODO!!! ����� ������ ���� DI, ������� �������� MeanRevert
///TODO!!!                index = findModelNameInDataInfoArray(totalOfDataInfo, ptrToPtrDI, IModelPoissonDrvnParetoJumpsExpMeanReverting::strExpMeanRevertingLambda);
    /** ���� ����� ��� ����� ������� : �����������, ��� �������� ExpMeanRevertingLambda
    ������ � ����� ��������� DataInfo, ����������� ������������� ������� ������ � ������� ����� 1
     */
    index = -1;
    for(size_t i=0; i < totalOfDataInfo; ++i)
    {
        if(1 == ptrToPtrDI[i]->m_dataSetSizes[0])
            index = i;
    }
    if(index < 0 )
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return _ERROR_WRONG_MODEL_PARAMS_SET_;
    }
    dimOfDI= ptrToPtrDI[index]->m_dimension;
    if(dimOfDI != 1)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return _ERROR_WRONG_ARGUMENT_;
    }
    rc = ptrIExpMeanRevertingParams->setParams((const size_t)ptrToPtrDI[index]->m_dataSetSizes[0], (const double *) ptrToPtrDI[index]->m_dataSet[0]);
    if(rc != _RC_SUCCESS_)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return rc;
    }
    ptrToPtrIparams = IParams::createEmptyParams(_logger, IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams);
    if(ptrToPtrIparams == NULL)
    {
        delete ptrIParamsPareto;
        delete ptrIParamsPoisson;
        delete ptrIExpMeanRevertingParams;
        return _ERROR_NO_ROOM_;
    }
    double tmpDbl=NAN;
    tmpDbl = ptrIParamsPareto->k();
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToPtrIparams)->setParetoK(tmpDbl);
    tmpDbl=NAN;
    tmpDbl = ptrIParamsPareto->xM();
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToPtrIparams)->setParetoXm(tmpDbl);
    delete ptrIParamsPareto;
    tmpDbl=NAN;
    tmpDbl = ptrIParamsPoisson->lambda();
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToPtrIparams)->setPoissonLambda(tmpDbl);
    delete ptrIParamsPoisson;
    tmpDbl=NAN;
    ptrIExpMeanRevertingParams->getParam(0, tmpDbl);
    rc = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrToPtrIparams)->setMRLambda(tmpDbl);
    delete ptrIExpMeanRevertingParams;

    return _RC_SUCCESS_;
};

IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl() :  IModel_Impl(IParamsPoissonDrvnParetoJumpsExpMeanReverting::PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension)
{
    _modelType = PoissonDrvnParetoJumpsExpMeanRevertingModel;
}
IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl* IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyModel()
{
    return new IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl();
}
/// ����� ���������� initParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    if (h == 0)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }

    size_t size=0;
    int rc = h->getTotalOfItemsInParamsUnion(size);
    if ((rc) || (size <= indexOfParams))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong size of ParamsUnion! PoissonDrvnParetoJumpsExpMeanReverting model parameters NOT FOUND in initParticles()");
///TODO!!!        _task->finish(MODEL_ERROR, "Wrong size of PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }

    /**
     No clones!
     */
    /*** ������ indexOfParams ������ � �������!
    ������ ��� ������ �������� ���� ���������
    � 0-� �������� IParamsUnion
        */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    /*DEBUG
    std::cout << h->toString() << std::endl;
    std::cout << ptrIParams->toString() << std::endl;
    */
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        _task->finish(MODEL_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in initParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif
///OLD    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = (IParamsPoissonDrvnParetoJumpsExpMeanReverting*)h;
///    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(h);

    unsigned particlesNum = particles.size();

    for (unsigned i = 0; i < particlesNum; ++i)
    {
///TODO!!! ������ ��������� ������� generateRV()
        /** ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC() */
        particles[i] = Distributions::Pareto(hyp->xM(), hyp->k()) * exp(hyp->lamC() * Distributions::Exponential(hyp->lam()));
    }
    return _RC_SUCCESS_;
}

/// ����� ���������� generateParticles �� I_PF_Model ���������� ���� ������
/// ��� ����� ������� � ������� ���������� ��� ���������? � Using �� ������� ����������?
int IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const
{
    if (h == NULL)
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateParticles()");
///TODO!!!        _task->finish(MODEL_ERROR, "Missing PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }

///OLD    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = (IParamsPoissonDrvnParetoJumpsExpMeanReverting*)h;
    /**
     No clones!
     */
    /*** TODO!!! � �������� indexOfParams!
     �� ������ � �������!
      ������ ��� ������ �������� ���� ���������
      � 0-� �������� IParamsUnion
    TODO!!!
    */
    IParams * ptrIParams = h->ptrIParamsUnionToPtrIParams(indexOfParams);
    IParamsPoissonDrvnParetoJumpsExpMeanReverting* hyp = dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting*>(ptrIParams);
#ifdef _DEBUG_
    if ((!hyp))
    {
        _logger->error(ALGORITHM_MODELDATA_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateParticles()");
        _task->finish(MODEL_ERROR, "Wrong PoissonDrvnParetoJumpsExpMeanReverting model parameters in generateParticles()");
        return _ERROR_WRONG_ARGUMENT_;
    }
#endif

    double poisson_tail;
    unsigned particlesNum = particles.size();

    for (unsigned i = 0; i < particlesNum; ++i)
    {
        // Time of spike is (t - poisson_tail), if poisson_tail < 1
        poisson_tail = 1 - Distributions::Exponential(hyp->lam());
        /** ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC() */
        particles[i] *= exp(hyp->lamC());
        // Heviside(poisson_tail) to avoid inf trouble
        if (poisson_tail >= 0)
        {
            /** ����� ��� ���������� � ��������� lambda_c, ������� �������� � ������� hyp->lamC() */
            particles[i] += Distributions::Pareto(hyp->xM(), hyp->k()) * exp(hyp->lamC() * poisson_tail);
        }
    }
    return _RC_SUCCESS_;
}
///TOOD!!!
double IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimateHypothesis �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimateHypothesis() in PoissonDrvnParetoJumpsExpMeanReverting model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimateHypothesis() in PoissonDrvnParetoJumpsExpMeanReverting model");
};
///TODO!!!
double IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimatePointMove �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    _logger->error(ALGORITHM_MODELDATA_ERROR, "NOT Implemented  estimatePointMove() in PoissonDrvnParetoJumpsExpMeanReverting model");
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimatePointMove() in PoissonDrvnParetoJumpsExpMeanReverting model");

};

/******************************************************************
* � I_PF_Model ������� ������ ������� �������������� ������������� �� �����? ����� ��?
*******************************************************************/
/** Factory methods */
//OLD IModel* IModel::createModel(enum IModel::ModelsTypes modelType, const size_t dim)
IModel* IModel::createModel(const enum IModel::ModelsTypes modelType, const IParams* ptrModelParams)
{
    switch (modelType)
    {
    case  PlainModel:
    {
        /** we may create plain model of dimension = 0  */
        size_t dim = 0;
        if(ptrModelParams)
        {
            dim = ptrModelParams->getSize();
        }
        return new IModelPlain_Impl(dim);
    }
    case  GaussianModel:
        return IModelGaussian_Impl::createEmptyModel();
    case WienerModel:
        return IModelWiener_Impl::createEmptyModel();
    case PoissonModel:
        return IModelPoisson_Impl::createEmptyModel();
    case ParetoModel:
        return IModelPareto_Impl::createEmptyModel();
    case PoissonDrvnParetoJumpsExpMeanRevertingModel:
        return IModelPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyModel();
    case PolynomialModel:
    {
        /** we may create plain model of dimension = 0  */
        size_t dim = 0;
        if(ptrModelParams)
        {
            dim = ptrModelParams->getSize();
        }
        return IModelPolynomial_Impl::createEmptyModel(dim);
    }
    default:
        return NULL;
    }
};

//OLD IModel* IModel::createModel(const std::string& modelName, const size_t dim)
IModel* IModel::createModel(const std::string& modelName, const IParams* ptrModelParams)
{
    for(size_t i=0; i < FAKE_MODEL; ++i)
    {
        if (modelName == IModel::modelNames[i])
        {
            if(ptrModelParams)
            {
                return createModel((const enum IModel::ModelsTypes )i, ptrModelParams);
            }
            else
            {
                return createModel((const enum IModel::ModelsTypes )i);
            }
        }
    }
    return createModel(FAKE_MODEL);
}
///TODO!!! � � ���������� ������, � � ������� ������ ����� ���� ���������� I_PF_Model ����������

/**************************************************
* Factory Implementation to make model's Params
***************************************************/
int IModel::parseAndCreateModelParams(const enum IModel::ModelsTypes modelType, std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * log)
{
    switch (modelType)
    {
    case  GaussianModel:
        return IModelGaussian::parseAndCreateModelParams(mapParams, postfix, size,  ptrToPtrIParams, log);
    case WienerModel:
        return IModelWiener::parseAndCreateModelParams(mapParams, postfix, size, ptrToPtrIParams, log);
    case ParetoModel:
        return IModelPareto::parseAndCreateModelParams( mapParams, postfix, size, ptrToPtrIParams, log);
    case PoissonDrvnParetoJumpsExpMeanRevertingModel:
        return IModelPoissonDrvnParetoJumpsExpMeanReverting::parseAndCreateModelParams( mapParams, postfix, size, ptrToPtrIParams, log);
    default:
        return IModel_Impl::parseAndCreateModelParams(mapParams, postfix, size, ptrToPtrIParams, log);
    } /// end switch()
};

/**************************************************
*        Comparator to compare  IModel models
***************************************************/
bool IModel::compareModels(const IModel* model_1, const IModel* model_2)
{
    if ((model_1 == NULL) || (model_2 == NULL))
    {
        return false;
    }
    enum IModel::ModelsTypes mt_1=FAKE_MODEL;
    model_1->getModelType(mt_1);
    enum IModel::ModelsTypes mt_2=FAKE_MODEL;
    model_2->getModelType(mt_2);
    if (mt_1 != mt_2)
    {
        return false;
    }
    return true;
};

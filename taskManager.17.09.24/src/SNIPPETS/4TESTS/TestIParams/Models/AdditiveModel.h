#ifndef __ADDITIVEMODEL_H__633569422542968750
#define __ADDITIVEMODEL_H__633569422542968750

#include <string>
#include <vector>
#include <map>
#include <math.h>
#include "Model.h" //enum IModel::ModelsTypes; modelNames[FAKE_MODEL]; getModelTypeByModelName()
#include "../Log/Log.h"
/** IAA : DON'T pollute global scope! NO WAY in h-file -> using namespace std; */

enum AdditiveModelsTypes{
  PLAIN_ADDITIVE_MODEL,
  PF_ADDITIVE_MODEL,
  FAKE_ADDITIVE_MODEL
};

enum indexesOfGrammarAdditiveModelAddends{
  TotalAddendsOfAdditiveModel,
  ModelName,
  ModelParamsSrc,
  GrammarSizeForAdditiveModelAddends
};

enum AdditiveModelsTypes getAdditiveModelsTypeByAdditiveModelName(const std::string& additiveModelName);

/** forward declarations */
/// class Task;
class IParams;

/** forward declaration */
/** ����� ������������ ������ ParamsUnion,
������ ��� � ����� ������ ������ ��� ����������
�������� ������ ������ ���� ����������,
�� �� �� ��������� � ������, ����� � ��� ���������� ������
������ ������ ���� ���������.
� ��������� ������ IParamsUnion ����� ���������
������ ���� ������� � ������� ���������� IParams ** _addends
(size_t _totalOfAddends = 1)
*/
class IParamsUnion;

/** abstract class - interface for AddtiveModel-estimators (e.g. PF-estimators)
*/
class IAdditiveModel //�� ����� �����-> : public virtual IModel
{
public:
    static const char* additiveModelsNames[FAKE_ADDITIVE_MODEL];
    static const char* grammarAdditiveModelAddends[GrammarSizeForAdditiveModelAddends];
    /** factories �������� �������� ���� ������ � ������� �������,
    ���� ������ � ������ �������, ���������� AdditiveModel
    */
    static IAdditiveModel* parseAndCreateModel(const enum AdditiveModelsTypes amt, const size_t totalOfAddends, std::map<std::string, std::string>& addendsMap, Logger* const logger);

    static IAdditiveModel* createModel(const size_t totalOfAddends, const enum IModel::ModelsTypes* addends, Logger* const logger);

    /** ����������, ������� ����� ������ ��� ��������
    ����������� � ParamsUnionSet/ParamsUnionCompact ����������:
    �� �� ���������, ����� � ���� ����������� ������
    ���������� ��������� ������ �������
    */
    static bool compareModels(const IAdditiveModel* model_1, const IAdditiveModel* model_2);

    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    IAdditiveModel(){};
    virtual ~IAdditiveModel(){};

    virtual void getModelType(enum AdditiveModelsTypes& additiveModelType) const = 0;
    /** ������������ �� IModel ��������� ��
    ��������� Task, Log, �� ��� �� ����� ������� ������.
    */
///TODO!!!  Task ������� ������ �� �����!
/// virtual void setTask(Task* const & task) =0;
    virtual void setLog(Logger* const & log) =0;

    /** ��������� IAdditiveModelParams ����������� ����������� ��� ������! */
///TODO!!!    virtual IAdditiveModelParams* createEmptyParamsOfModels() const = 0;
///TODO!!!    virtual IAdditiveModelParams* parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
    static int parseAndCreateModelParamsUnion(const size_t totalOfAddends, const enum IModel::ModelsTypes** addendModelsTypes, std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParamsUnion** ptrToPtrIParamsUnion, Logger * logger);
    virtual bool canCastModelParamsUnion(IParamsUnion * params) const = 0;

    /** Any subclass may have own copy algorithm */
///TODO!!! What's for?    virtual IAdditiveModel* clone() const =0;
    /** Every subclass MUST have own IMPLEMENTATION for toString()! */
    virtual std::string toStringAllAddends() const =0;

    virtual int getTotalOfAddends(size_t& size) const =0;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MIGHT define operator(i,...) to get access to params by index
    virtual int setAddend(const size_t index, IModel* addendModel) =0;
    virtual int setAddends(const size_t size, IModel** addendModels) =0;

    virtual int getAddendType(size_t index, enum IModel::ModelsTypes& addendModelType) const =0;
    virtual int getAddendsTypes(const size_t size, enum IModel::ModelsTypes**& addendModelsTypes) const =0;

    virtual int getAddend(size_t index, IModel*& addendModel) const =0;
    virtual int getAddends(const size_t size, IModel**& addendModels) const =0;


private:
    /** assign and copy CTORs are disabled */
    IAdditiveModel(const IAdditiveModel&);
    void operator=(const IAdditiveModel&);
};

/** forward declarations */
class I_PF_AdditiveModelParams;

///TODO??? I_PF_AdditiveModel ������ ����������� ��� � �� I_Probabilistic_Model
class I_PF_AdditiveModel : public virtual IAdditiveModel,
///TODO!!! ��������� ���������� ��� PF-��������,
/// �� ����� �� ��� ����������� �� I_PF_Model?
/** ���� ����� ��� ��-�� ���� ��������� ������
    I_PF_Model*   noiseModel;
    I_PF_Model* processModel;
��� ��-�� ���� ������ ������ (�� ���� ����� ������� � IModel)
�������� I_PF_Model ������������� �� IModel
*/
                           public virtual I_PF_Model
{
public:
    enum indexesOfAddendsPF_AdditiveModel{
        _noiseModelIndex=0,
        _particleGeneratingModelIndex =1,
        dim_I_PF_AdditiveModel = 2
    };

    enum indexesOfGrammarPFAdditiveModelAddends{
      indexOfNoiseModelAddendInAdditiveModel,
      GrammarSizeForPFAdditiveModelAddends
    };
    static const char* grammarPFAdditiveModelAddends[GrammarSizeForPFAdditiveModelAddends];

    I_PF_AdditiveModel() {};
    virtual ~I_PF_AdditiveModel(){};
    /** ������ ����� ��� ��������� I_PF_AdditiveModel,
    ??? �������� ��� ������ ��������� �������
    */
    static I_PF_AdditiveModel* createModel(std::string noiseModelName, std::string particleGeneratingModelName, Logger * const logger);

/// ����     virtual PF_Params* parseModelParams(map<std::string, std::string>& mapParams, std::string postfix = "");
///����� �� �� static!
///������ ����� ����� static int IAdditiveModel::parseAndCreateModelParamUnion(...);
    virtual int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParamsUnion** ptrToPtrIParamsUnion) const =0;
    /** ��� ������� ����� ��� ��������� �� ��� ������� � ���������� IParamsUnion,
    ������� ������������� ���������� PF ���������� ������ (_noiseModel, _particleGeneratingModel)
    ��� ���������� �������� � �������� ���������� ���������� �� ��������, � ������� ��� �������� - IParamsUnion,
    � ���������� ������ PFAdditiveModel- ������ � ���� ������������ ����������� ����������� ��� ����� ���������
    �� ��������� items � ���� ���������� IParamsUnion
    */
    virtual IParams* getPtrToNoiseParams(const IParamsUnion*& ptrToIParamsUnion) const =0;
    virtual IParams* getPtrToParticleGeneratingParams(const IParamsUnion*& ptrToIParamsUnion) const =0;


    using IAdditiveModel::getTotalOfAddends;

    using IAdditiveModel::getAddendType;
    using IAdditiveModel::getAddendsTypes;

    using IAdditiveModel::getAddend;
    using IAdditiveModel::getAddends;

/// ����     PF_Params* makeEmptyModelParams();
///����� �� static!
///������ �����     static I_PF_AdditiveModelParams* parseParams(I_PF_AdditiveModel* model, map<std::string, std::string>& mapParams, std::string postfix);
///TODO!!!     virtual IAdditiveModelParams* createEmptyParamsOfAdditiveModel() const =0;

    /** � ���������� ��������� ������� �������� ����� ���
    ������ ��� �� ������� �� ���������� I_PF_Model!
    ��� (� ���������� I_PF_Model) ������ ������ �� ���������, ����� ���� ��������
    ���������� � ���� I_PF_AdditiveModelParams
    */
    virtual double estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const =0;
    virtual double estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

    virtual int initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const =0;
    virtual int generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const =0;

    /** ��� �� �������������� ���������,
     ������� ����� ����� ������������������� � ���������� ���������� I_PF_AdditiveModel (���������� ������!),
     ����� ����� ���� ������� ������� ������ ���������� I_PF_Model
     ���������, �� ����� ����� �� dynamic_cast<I_PF_Model*>
    /**
     ��� ��-�� ���� ������ ������
     (����� �� �������������������, ���� ����� ����� ������� ��������� ������ (��� IModel *) � I_PF_Model*)
    */
    virtual I_PF_Model const * const   getNoiseModel() const =0;
    virtual I_PF_Model const * const   getParticleGeneratingModel() const =0;
private:
    /** assign and copy CTORs are disabled */
    I_PF_AdditiveModel(const I_PF_AdditiveModel&);
    void operator=(const I_PF_AdditiveModel&);
};

/// � ��� ��� ����� ������ ������ � Params2D (��� ���� ������� ��� ������������ ��������)
/// ���� �������������� ������ ������ ��������! � ���������������� - ��������,
/// ����� � ������ �� ��� Params1D
/*** TODO!!
class ProcessInfo
{

    public:

        vector<double> lambda;
        vector<double> start;
        vector<int> polynomDegree;
        vector<double> coef0;
        vector<double> x, y;

        ProcessInfo() {};
        ~ProcessInfo() {};

        double getVal(double x, int ind) const
        {
            return coef0[ind] * exp(-lambda[ind] * x);
        }

        void resize()
        {
            lambda.push_back(0);
            start.push_back(0);
            polynomDegree.push_back(5);
            coef0.push_back(0);
        }
};
*/
#endif

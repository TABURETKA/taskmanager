#include "AdditiveModel.h"
#include "../CommonStuff/Defines.h" //DELIMITER_TO_STRING_ITEMS
#include "../Log/errors.h"
///TODO!!!  Task ������� ������ �� �����!
///#include "../Task/Task.h"
#include "../Params/Params.h"
#include "../Config/ConfigParser.h"//iteratorParamsMap
//OLD #include "../Params/AdditiveModelParams.h"
#include "../Distributions/Distributions.h"
#include <math.h> //NAN
#include <windows.h> // debug estimation RT
#include <math.h>
#include <cfloat>
#include <sstream>
#include <algorithm>

///static
const char* IAdditiveModel::additiveModelsNames[FAKE_ADDITIVE_MODEL]=
{
    "PLAIN_ADDITIVE_MODEL",
    "PF_ADDITIVE_MODEL"
};
enum AdditiveModelsTypes getAdditiveModelsTypeByAdditiveModelName(const std::string& additiveModelName)
{
  size_t i=0;
  for(i=0; i < FAKE_ADDITIVE_MODEL; ++i)
  {
      if (additiveModelName == IAdditiveModel::additiveModelsNames[i])
      {
          return (enum AdditiveModelsTypes)i;
      }
  }
  return FAKE_ADDITIVE_MODEL;
};

const char* IAdditiveModel::grammarAdditiveModelAddends[GrammarSizeForAdditiveModelAddends]={
  "TOTAL_ADDENDS_OF_ADDITIVE_MODEL",
  "MODEL_NAME_OF_ADDEND",
  "MODEL_PARAMS_SRC_OF_ADDEND"
};

///static in I_PF_AdditiveModel
const char* I_PF_AdditiveModel::grammarPFAdditiveModelAddends[GrammarSizeForPFAdditiveModelAddends]={
  "INDEX_OF_NOISE_MODEL_ADDEND_IN_ADDITIVE_MODEL"
};

/** anonymous namespace to hide private implementations into this cpp-file */
namespace{
/*******************************************************
* Base abstract IAdditiveModel class implementation
********************************************************/
class IAdditiveModel_Impl : public virtual IAdditiveModel
{
public:
    /** CTOR - create additive model by addends types */
    IAdditiveModel_Impl(const size_t totalOfAddends, const enum IModel::ModelsTypes* addends, Logger* const logger);
    /** DTOR */
    virtual ~IAdditiveModel_Impl();
    /**
    ���� ����� �� �� ���������� ����������,
    �� ����� ������ ��� �������� ���������� ������
    ����� �� ��� ����� �������������������,
    ��������, ��� ���������� ��� ������� ������
    ���������� ���� ������, ����������� ���������� �����������
    */
///TODO!!! //OLD    virtual IParamsUnion* createEmptyParamsOfModel() const;
    virtual bool canCastModelParamsUnion(IParamsUnion * params) const;
    /** Any subclass may have own copy algorithm */
///TODO!!! What's for?    virtual IAdditiveModel_Impl* clone() const;

    /** Every subclass MUST have own IMPLEMENTATION for toString()! */
    virtual std::string toStringAllAddends() const;
    /** Every subclass MUST have own IMPLEMENTATION for setTask()! */
///TODO!!!  Task ������� ������ �� �����!
///    virtual void setTask(Task* const & task);
/** Every subclass MUST have own IMPLEMENTATION for setLog! */
    virtual void setLog(Logger* const & logger);

    virtual int getTotalOfAddends(size_t& size) const;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MIGHT define operator(i,...) to get access to params by index
    virtual int setAddend(const size_t index, IModel* addendModel);
    virtual int setAddends(const size_t size, IModel** addendModels);

    virtual int getAddendType(size_t index, enum IModel::ModelsTypes& addendModelType) const;
    virtual int getAddendsTypes(const size_t size, enum IModel::ModelsTypes**& addendModelsTypes) const;

    virtual int getAddend(size_t index, IModel*& addendModel) const;
    virtual int getAddends(const size_t size, IModel**& addendModels) const;

protected:
    enum AdditiveModelsTypes _additiveModelType;
    virtual void getModelType(enum AdditiveModelsTypes& additiveModelType) const;
    Logger * _logger;
///TODO!!!  Task ������� ������ �� �����!
///    Task * _task;
    ///TOOD!!! ����� ����, ��� ����� ������ � ������ �����, ��� ������� ���� ����� ���� ������ ������������ � �������?
   void parsingFailed(std::string msg) const;
    /** NO public ctor without params! Use static Factory instead! */
    IAdditiveModel_Impl();
private:
    /** assign and copy CTORs are disabled */
    IAdditiveModel_Impl(const IAdditiveModel_Impl&);
    void operator=(const IAdditiveModel_Impl&);
    /** data stuff */
    size_t _totalOfAddends;
    IModel ** _addends;
    enum IModel::ModelsTypes * _addendsTypes;
    int reallocatePtrToAddends(const size_t totalOfAddends);
    int reallocateAddends(const size_t totalOfAddends, const enum IModel::ModelsTypes * addendsTypes);
};
/*******************************************************
* Base abstract I_PF_AdditiveModel class implementation
********************************************************/
class I_PF_AdditiveModel_Impl : public virtual IAdditiveModel_Impl,
                                public virtual I_PF_AdditiveModel
{
public:
    /** CTOR - create additive model by TWO addends types */
///TODO!!! ���� ��� ����� �������������� ������� ������������ ������������ ����������� !    I_PF_AdditiveModel_Impl(const size_t size, const enum IModel::ModelsTypes* addends, Logger * const logger);
    I_PF_AdditiveModel_Impl(const enum IModel::ModelsTypes addends[dim_I_PF_AdditiveModel], Logger * const logger);
    /** DTOR */
    virtual ~I_PF_AdditiveModel_Impl();
    using IAdditiveModel_Impl::toStringAllAddends;
    virtual std::string toString() const;

/// ������ ������ ������� �� ����������  I_PF_AdditiveModelParams :
///   static I_PF_AdditiveModelParams*  parseAndCreateParams(const I_PF_AdditiveModel* model, std::map<std::string, std::string>& mapParams, std::string postfix);
/// ������ ���������� ���������
    virtual int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParamsUnion** ptrToPtrIParamsUnion) const;
    /** ��� ������� ����� ��� ��������� �� ��� ������� � ���������� IParamsUnion,
    ������� ������������� ���������� PF ���������� ������ (_noiseModel, _particleGeneratingModel)
    ��� ���������� �������� � �������� ���������� ���������� �� ��������, � ������� ��� �������� - IParamsUnion,
    � ���������� ������ PFAdditiveModel- ������ � ���� ������������ ����������� ����������� ��� ����� ���������
    �� ��������� items � ���� ���������� IParamsUnion
    */
    virtual IParams* getPtrToNoiseParams(const IParamsUnion*& ptrToIParamsUnion) const;
    virtual IParams* getPtrToParticleGeneratingParams(const IParamsUnion*& ptrToIParamsUnion) const;

///TODO!!!
//OLD! use from IAdditiveModel_Impl::  virtual IParamsUnion* createEmptyParamsOfModel() const;
    using IAdditiveModel_Impl::canCastModelParamsUnion;
///TODO!!!
//OLD! REDUNDANT    virtual IParamsUnion* parseAndCreateAdditiveModelParams(map<std::string, std::string>& mapParams, std::string postfix) const;
    using IAdditiveModel_Impl::getModelType;
///TODO!!!  Task ������� ������ �� �����!
///    void setTask(Task* const & task);
    void setLog(Logger* const & log);

    using IAdditiveModel_Impl::getTotalOfAddends;
    using IAdditiveModel_Impl::getAddendType;
    using IAdditiveModel_Impl::getAddendsTypes;

    using IAdditiveModel_Impl::getAddend;
    using IAdditiveModel_Impl::getAddends;

    virtual I_PF_Model const * const   getNoiseModel() const ;
    virtual I_PF_Model const * const   getParticleGeneratingModel() const ;

    virtual double estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const;
    virtual int initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;
    virtual int generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const;

    virtual double estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const;
protected:
   void parsingFailed(std::string msg) const;
private:
    I_PF_Model * _noiseModel;
    I_PF_Model * _particleGeneratingModel;
    /** NO public ctor without params!*/
    I_PF_AdditiveModel_Impl() {};
    /** assign and copy CTORs are disabled */
    I_PF_AdditiveModel_Impl(const I_PF_AdditiveModel_Impl&);
    void operator=(const I_PF_AdditiveModel_Impl&);
};

}; ///end namespace

/******************** IMPLEMENTATIONs ****************************/
/*****************************************************************
*                 IAdditiveModel_Impl Implementation!
*****************************************************************/
///static in IAdditiveModel!
IAdditiveModel* IAdditiveModel::createModel(const size_t totalOfAddends, const enum IModel::ModelsTypes* addends, Logger* const logger)
{
    if ((!totalOfAddends) || (addends==NULL))
    {
        logger->error(ALGORITHM_MODELDATA_ERROR, std::string("No info to create AdditiveModel in IAdditiveModel::createModel()"));
        return NULL;
    }
    return new IAdditiveModel_Impl(totalOfAddends, addends, logger);
};

///static in IAdditiveModel
IAdditiveModel* IAdditiveModel::parseAndCreateModel(const enum AdditiveModelsTypes amt, const size_t totalOfAddends, std::map<std::string, std::string>& addendsMap, Logger* const logger)
{
    /** ������� ������� ������ �� ������ IParams, � ����� ���������������� �� � �����
    */
    enum IModel::ModelsTypes* ptrModelTypes = new enum IModel::ModelsTypes[totalOfAddends];
    std::string ModelAddensCfgNames[totalOfAddends];
    /** ����� ���� �� ��������� ������� addendModelsTypes, � ������� ����� �������� ������ �����
    IModel::parseAndCreateModelParams
    */
    for(size_t i=0; i < totalOfAddends; ++i)
    {
        enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
        ModelAddensCfgNames[i].clear();
        /** GRAMMAR! ����� �������� � ���� ��������� ���� ���������,
         ���� ��� ������ ������ � ���� �� ����, �������� ��� ���������,
         �������� ����� ���� ���������� ����������, ������������� ���������� ������ ����������,
         � �������� ���� ��������� ���������
        */
        std::stringstream ss;
        ss << IAdditiveModel::grammarAdditiveModelAddends[ModelName] << "_" << i; //REDUNDANT + postfix;
        std::string tmpStr     = ss.str();
//DEBUG std::cout << tmpStr << std::endl;
        iteratorParamsMap itParam = addendsMap.find(tmpStr);
        if (itParam != addendsMap.end()) {
            std::string delimiters = DEFAULT_DELIMITERS;
            mt = IModel::getModelTypeByModelName( (const std::string&)( TrimStr(itParam->second, delimiters) ));
            if(mt == IModel::ModelsTypes::FAKE_MODEL)
            {
                logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(itParam->second) + ": wrong AdditiveModel model addend's name in IAdditiveModel::parseAndCreateModel()");
                delete [] ptrModelTypes;
                return NULL;
            } else {
                ptrModelTypes[i]=mt;
            }
        }
        else {
            logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(tmpStr + " NOT FOUND in IAdditiveModel::parseAndCreateModel()") );
            delete [] ptrModelTypes;
            return NULL;
        }
        ss.clear();
        ss << IAdditiveModel::grammarAdditiveModelAddends[ModelParamsSrc] << "_" << i; //REDUNDANT + postfix;
        tmpStr     = ss.str();
//DEBUG std::cout << tmpStr << std::endl;
        itParam = addendsMap.find(tmpStr);
        if (itParam != addendsMap.end())
        {
            std::string delimiters = DEFAULT_DELIMITERS;
            ModelAddensCfgNames[i] = TrimStr(itParam->second, delimiters);
///TODO!!! ������ ���������������� ������ ��������� ������ ���� �� ����������!
        }
    }

    for(size_t i=0; i < totalOfAddends; ++i)
    {
        if(! ModelAddensCfgNames[i].empty())
        {
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, std::string("ModelParamsSrcs processing is NOT IMPLEMENTED yet in IAdditiveModel::parseAndCreateModel()"));
            delete [] ptrModelTypes;
            return NULL;
        }
    }
    /** ������ ����� ��������� ���������� ������
    */
    switch(amt)
    {
    case PLAIN_ADDITIVE_MODEL:
        {
            IAdditiveModel* additiveModel  = IAdditiveModel::createModel(totalOfAddends, (const enum IModel::ModelsTypes*) ptrModelTypes, logger);
            /** ������� ��������� ������
            */
            delete [] ptrModelTypes;
            ptrModelTypes = NULL;

            return additiveModel;
        }
        break;
    case PF_ADDITIVE_MODEL:
        {
            std::stringstream ss;
            size_t dimOfPFAdditiveModel = I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::dim_I_PF_AdditiveModel;
            if(totalOfAddends != dimOfPFAdditiveModel)
            {
                ss << "Wrong number of addends for I_PF_AdditiveModel! So far implemented "
                << dimOfPFAdditiveModel << " ONLY!";
                logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, ss.str());
                return NULL;
            }
            /** �������� ���������� ��� noiseModelName, particleGeneratingModelName, ����� ����� ���� �� ��������� � cfg-files,
            ������� ������ ���������� PF Additive Mode,
            */
            ss.clear();
            size_t defaultIndexOfNoiseModelAddend = I_PF_AdditiveModel::indexesOfGrammarPFAdditiveModelAddends::indexOfNoiseModelAddendInAdditiveModel;
            ss << I_PF_AdditiveModel::grammarPFAdditiveModelAddends[defaultIndexOfNoiseModelAddend];
            std::string tmpStr     = ss.str();
//DEBUG std::cout << tmpStr << std::endl;
            iteratorParamsMap itParam = addendsMap.find(tmpStr);
            if (itParam != addendsMap.end())
            {
                std::string delimiters = DEFAULT_DELIMITERS;

                int indexOfNoiseModel = atoi(TrimStr(itParam->second, delimiters).c_str());
                if((indexOfNoiseModel < 0) || (indexOfNoiseModel > dimOfPFAdditiveModel))
                {
                    delete [] ptrModelTypes;
                    ptrModelTypes = NULL;
                    logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Qrong index of noise model for PF ADDITIVE_MODEL : " + tmpStr);
                    return NULL;
                }
                /** ���� ������ ���������� PF Additive Mode,
                ������� ������� ���������������� ��� noiseModel,
                �� ������������� �����������,
                ������������� enum indexesOfAddendsPF_AdditiveModel,
                ������� ����������� ���������
                */
                if(indexOfNoiseModel != defaultIndexOfNoiseModelAddend)
                {
                    enum IModel::ModelsTypes tmpPtrModelTypes = IModel::ModelsTypes::FAKE_MODEL;
                    tmpPtrModelTypes = ptrModelTypes[defaultIndexOfNoiseModelAddend];
                    ptrModelTypes[defaultIndexOfNoiseModelAddend] = ptrModelTypes[indexOfNoiseModel];
                    ptrModelTypes[indexOfNoiseModel] = tmpPtrModelTypes;
                }
            } else {
                delete [] ptrModelTypes;
                ptrModelTypes = NULL;
                logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "PF ADDITIVE_MODEL noise model NOT DEFINED! " + tmpStr + " NOT FOUND!");
                return NULL;
            }
            std::string noiseModelName = IModel::modelNames[ptrModelTypes[I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_noiseModelIndex]];
            std::string particleGeneratingModelName = IModel::modelNames[ptrModelTypes[I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_particleGeneratingModelIndex]];
            I_PF_AdditiveModel* additiveModel  = I_PF_AdditiveModel::createModel( noiseModelName, particleGeneratingModelName, logger);
            /** ������� ��������� ������
            */
            delete [] ptrModelTypes;
            ptrModelTypes = NULL;

            return additiveModel;
        }
        break;
        default:
        {
            delete [] ptrModelTypes;
            ptrModelTypes = NULL;
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong ADDITIVE_MODEL_TYPE in IAdditiveModel::parseAndCreateModel()!");
            return NULL;
        }
    }///end switch()
  return NULL;
}

///static in IAdditiveModel!
int IAdditiveModel::parseAndCreateModelParamsUnion(const size_t totalOfAddends, const enum IModel::ModelsTypes** addendModelsTypes, std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParamsUnion** ptrToPtrIParamsUnion, Logger * log)
{
    /** ������� ������� ������ �� ������ IParams, � ����� ���������������� �� � �����
    */
    IParams** tmpAddends = new IParams*[totalOfAddends];
    /** ����� ���� �� ��������� ������� addendModelsTypes, � ������� ����� �������� ������ �����
    IModel::parseAndCreateModelParams
    */
    for(size_t i=0; i < totalOfAddends; ++i)
    {
        const enum IModel::ModelsTypes mt = *addendModelsTypes[i];
///TODO!!! GRAMMAR! ������ �������� : ��� �������� � ���� ��������� ���� ���������, ���� ��� ������ ������ � ���� �� ����, �������� ��� ���������?
///������ ������� ����� ���� ���������� ����������, ������������� ���������� ������ ����������, � �������� ���� ��������� ���������
        std::string tmpPostfix = "_" + i + postfix;
        /** tmpSize is in use to double-check we have got single IParams into tmpPtrToPtrIparams */
        size_t tmpSize=0;
        /** � ����� ������� ��� ������� ���������� ���� IParams* */
        IParams** tmpPtrToPtrIparams = &(tmpAddends[i]);

///������ ���� ������� ����� ���������� � postfix static'� int parseAndCreateModelParams(enum IModel::ModelsTypes mt, std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
        int rc = IModel::parseAndCreateModelParams(mt, mapParams, tmpPostfix, tmpSize, tmpPtrToPtrIparams, log);
/// for each addends we expect the ONLY ONE IParams!
        /** tmpSize!= 1 -> to double-check we have got single IParams into tmpPtrToPtrIparams */
        if ((rc) || (tmpSize != 1))
        {
            std::string msg = IModel::modelNames[mt];
            msg = "Fail to parse " + msg + "-params! The rest of models' params were not initialized!";
            log->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, msg);
            delete [] tmpAddends;
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    /** ������ �� ����� ���������� � IParamsUnion** ptrToPtrIParamsUnion
    */
    *ptrToPtrIParamsUnion  = IParamsUnion::createParamsUnion(totalOfAddends,(const IParams**) tmpAddends, log);
    size = totalOfAddends;
    /** ������� ��������� ������
    */
    delete [] tmpAddends;
///  std::cout << "NOT implemented IAdditiveModel_Impl::parseAndCreateModelParams!" << std::endl;
    return _RC_SUCCESS_;
}

/****************************************************************
*   Base class - implementation
*****************************************************************/
/************************************************
* CTOR - create additive model by addends types *
************************************************/
IAdditiveModel_Impl::IAdditiveModel_Impl(const size_t totalOfAddends, const enum IModel::ModelsTypes* addends, Logger* const logger)
{
//OLD???    _modelType = IModel::ModelsTypes::ADDITIVE_MODEL;

    _additiveModelType = PLAIN_ADDITIVE_MODEL;
    _logger = logger;
    _totalOfAddends=0;
    _addends=NULL;
    _addendsTypes=NULL;
    if((! totalOfAddends) || (! addends))
    {
        return;
    }
    reallocateAddends(totalOfAddends, addends);
};
/*************************************************
*                  DTOR                          *
**************************************************/
IAdditiveModel_Impl::~IAdditiveModel_Impl()
{
#ifdef _DEBUG_
std::cout << "into ~IAdditiveModel_Impl:" <<  std::endl;
#endif
  for(size_t i=0; i<_totalOfAddends; ++i)
  {
      if(_addends[i]) delete _addends[i];
      _addends[i]=NULL;
  }
  delete [] _addendsTypes;
  _addendsTypes = NULL;
  delete [] _addends;
  _addends = NULL;
};

int IAdditiveModel_Impl::reallocatePtrToAddends(const size_t totalOfAddends)
{
    if (_addends)
    {
        for(size_t i=0; i<_totalOfAddends; ++i)
        {
            if(_addends[i]) delete _addends[i];
           _addends[i]=NULL;
        }
    }
    delete [] _addends;
    _addends=NULL;
    delete [] _addendsTypes;
    _addendsTypes=NULL;
    if(! totalOfAddends)
    {
        _totalOfAddends=0;
        return _RC_SUCCESS_;
    }
    _addends = new IModel*[totalOfAddends];
    if (!_addends)
    {
        return _ERROR_NO_ROOM_;
    };

    _addendsTypes = new enum IModel::ModelsTypes[totalOfAddends];
    if (!_addendsTypes)
    {
        delete [] _addends;
        return _ERROR_NO_ROOM_;
    }

    for (size_t i=0; i < totalOfAddends; ++i)
    {
        _addends[i]     = NULL;
        _addendsTypes[i]= IModel::ModelsTypes::FAKE_MODEL;
    }
    _totalOfAddends = totalOfAddends;
    return _RC_SUCCESS_;
};

int IAdditiveModel_Impl::reallocateAddends(const size_t totalOfAddends, const enum IModel::ModelsTypes* addendsTypes)
{
    int rc = reallocatePtrToAddends(totalOfAddends);
    if (rc)
    {
        return rc;
    }
    for(size_t i=0; i<totalOfAddends; ++i)
    {
        IModel::ModelsTypes addendType = addendsTypes[i];
/// Init _addends!
        if (addendType < IModel::ModelsTypes::FAKE_MODEL)
        {
            _addends[i]=IModel::createModel((const enum IModel::ModelsTypes)addendType);
            _addendsTypes[i]=addendType;
        }
        else
            return _ERROR_WRONG_ARGUMENT_;
    }
    return _RC_SUCCESS_;
};

void IAdditiveModel_Impl::getModelType(enum AdditiveModelsTypes& additiveModelType) const
{
    additiveModelType = _additiveModelType;
};
/* TODO!!!
int IAdditiveModel_Impl::createEmptyParamsOfModel(size_t& size, IParams** ptrToPtrIParams) const
{
  std::cout << "NOT implemented IAdditiveModel_Impl::createEmptyParamsOfModel!" << std::endl;

  I_PF_AdditiveModelParams* params = I_PF_AdditiveModelParams::parseAndCreateParams((const I_PF_AdditiveModel*)this, mapParams, postfix);
  return (IParamsUnion*)params;
};
*/
///TODO!!! ����� �������� � ������� ������ parseAndCreateParamsUnion ���� ������ �������� ���� ����� ��������� � �������
void IAdditiveModel_Impl::parsingFailed(std::string msg) const
{
///TODO!!! we have to decide! Which addend did not get its params?
  std::cout << "NOT implemented IAdditiveModel_Impl::parsingFailed!" << std::endl;
};

bool IAdditiveModel_Impl::canCastModelParamsUnion(IParamsUnion * params) const
{
    size_t totalOfItems=0;
    params->getTotalOfItemsInParamsUnion(totalOfItems);
    if (totalOfItems != _totalOfAddends)
    {
        return false;
    }

    for(size_t i=0; i < _totalOfAddends; ++i)
    {
        /** first pull out next IParams from IParamsUnion */
        IParams * param = NULL;
        params->getItemCopy(i, param);
        /** then try to cast this IParams to type
        of corresponding IModle params
        of this IAdditiveModel
        */
        bool rc = _addends[i]->canCastModelParams(param);
        /** we have got copy of IParams
        so clean up it
        */
        delete param;
        if (! rc)
            return false;
    }
    return true;
};
/** Every subclass MUST have own IMPLEMENTATION for toString()! */
std::string IAdditiveModel_Impl::toStringAllAddends() const
{
    std::stringstream ss;
    for(size_t i=0; i<_totalOfAddends; ++i)
    {
           ss <<  _addends[i]->toString();
           if(i < _totalOfAddends - 1)
           {
               ss << DELIMITER_TO_STRING_ITEMS;
           }
    }
      return ss.str();
};
/*OLD REDUNDANT
void IAdditiveModel_Impl::setTask(Task* const & task)
{
    for(size_t i=0; i< _totalOfAddends; ++i)
    {
        _addends[i]->setTask(task);
    }
    _task = task;
}
*/
/// Invariant for every model
void IAdditiveModel_Impl::setLog(Logger* const & logger)
{
  for(size_t i=0; i< _totalOfAddends; ++i)
  {
      _addends[i]->setLog(logger);
  }
  _logger = logger;
}

int IAdditiveModel_Impl::getTotalOfAddends(size_t& size) const
{
    size = _totalOfAddends;
    return _RC_SUCCESS_;
};

/** for model we don't need to clone model instance
 assumed the ptr to new model instance we have got as addendModel
*/
int IAdditiveModel_Impl::setAddend(const size_t index, IModel* addendModel)
{
    if(index >= _totalOfAddends)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(!addendModel)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    delete _addends[index];
    _addends[index] = NULL;
    /** for model we don't need to clone model instance
    assumed the ptr to new model instance we have got as addendModel
    */
    _addends[index] = addendModel;
    return _RC_SUCCESS_;
};

int IAdditiveModel_Impl::setAddends(const size_t size, IModel** addendModels)
{
    if(!addendModels)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(size != _totalOfAddends)
    {
        int rc =reallocatePtrToAddends(size);
        if(rc)
        {
            return rc;
        }
    }
    for(size_t i=0; i<size; ++i)
    {
        if(addendModels[i])
        {
/** for model we don't need to clone model instance
 assumed the ptr to new model instance we have got as addendModel
*/
            setAddend(i, addendModels[i]);
        }
        else
            return _ERROR_WRONG_ARGUMENT_;
    }
    return _RC_SUCCESS_;
};
/** for model we don't need to clone model instance
 assumed the ptr to new model instance we have got as addendModel
*/
int IAdditiveModel_Impl::getAddendType(size_t index, enum IModel::ModelsTypes& addendModelType) const
{
    if(index >= _totalOfAddends)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
#ifdef _DEBUG_
std::cout << _addendsTypes[index] << std::endl;
#endif // DEBUG
/** for model we don't need to clone model instance
 assumed the ptr to new model instance we have got as addendModel
*/
    addendModelType = (enum IModel::ModelsTypes)(_addendsTypes[index]);
    return _RC_SUCCESS_;
};

int IAdditiveModel_Impl::getAddend(size_t index, IModel*& addendModel) const
{
    if(index >= _totalOfAddends)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
#ifdef _DEBUG_
std::cout << _addends[index]->toString() << std::endl;
#endif // DEBUG
/** for model we don't need to clone model instance
 assumed the ptr to new model instance we have got as addendModel
*/
    addendModel = _addends[index];
    return _RC_SUCCESS_;
};

int IAdditiveModel_Impl::getAddends(const size_t size, IModel**& addendModels) const
{
    std::cout << "IAdditiveModel_Impl::getAddends not implemented!" << std::endl;
    return _ERROR_WRONG_ARGUMENT_;
};

int IAdditiveModel_Impl::getAddendsTypes(const size_t size, enum IModel::ModelsTypes**& addendModelsTypes) const
{
    std::cout << "IAdditiveModel_Impl::getAddendsTypes not implemented!" << std::endl;
    return _ERROR_WRONG_ARGUMENT_;
};

/************************************************
*  Additional noise-process double IModel class *
************************************************/
/************************************************
*                      CTOR
*         NB SO FAR we expect size=2 ONLY!
************************************************/
///TODO!!! ���� ��� ����� �������������� ������� ������������ ������������ �����������
///TODO!! I_PF_AdditiveModel_Impl::I_PF_AdditiveModel_Impl(const size_t size, const enum IModel::ModelsTypes* addends, Logger * const logger) : IAdditiveModel_Impl(size, addends, logger)
I_PF_AdditiveModel_Impl::I_PF_AdditiveModel_Impl(const enum IModel::ModelsTypes addends[dim_I_PF_AdditiveModel], Logger * const logger) : IAdditiveModel_Impl(dim_I_PF_AdditiveModel, addends, logger)
{
  // _noiseModelIndex = 0;
  IModel* ptrIModel=NULL;
  int rc = getAddend(_noiseModelIndex, ptrIModel);
  _noiseModel = dynamic_cast<I_PF_Model*>(ptrIModel);

#ifdef _DEBUG_
std::cout << "In I_PF_AdditiveModel_Impl _noiseModel :" << _noiseModel->toString() << std::endl;
#endif
  // _particleGeneratingModelIndex = 1;
  rc = getAddend(_particleGeneratingModelIndex, ptrIModel);
  _particleGeneratingModel = dynamic_cast<I_PF_Model*>(ptrIModel);

#ifdef _DEBUG_
std::cout << "In I_PF_AdditiveModel_Impl _noiseModel :" << _particleGeneratingModel->toString() << std::endl;
#endif
//OLD???  _modelType = ADDITIVE_MODEL;
  _additiveModelType = PF_ADDITIVE_MODEL;
};
/*************************************************
*                         DTOR
*************************************************/
I_PF_AdditiveModel_Impl::~I_PF_AdditiveModel_Impl()
{
    if (_noiseModel != NULL) {
///DON't        delete _noiseModel;
        _noiseModel = NULL;
    }
    if (_particleGeneratingModel != NULL) {
///DON't        delete _particleGeneratingModel;
        _particleGeneratingModel = NULL;
    }
}
/*OLD
void I_PF_AdditiveModel_Impl::getModelType(enum IModel::ModelsTypes& modelType) const
{
    modelType = _modelType;
};
*/
std::string I_PF_AdditiveModel_Impl::toString() const
{
    return this->toStringAllAddends();
}

void I_PF_AdditiveModel_Impl::parsingFailed(std::string msg) const
{
///YODO!!! we have to decide! Which addend did not get its params?
  std::cout << "NOT implemented I_PF_AdditiveModel_Impl::parsingFailed!" << std::endl;
};

/// Invariant for every model
/*OLD REDUNDANT
void I_PF_AdditiveModel_Impl::setTask(Task* const & task)
{
    _noiseModel->setTask(task);
    _particleGeneratingModel->setTask(task);
    _task = task;
}
*/
/// Invariant for every model
void I_PF_AdditiveModel_Impl::setLog(Logger* const & logger)
{
//OLD    _noiseModel->setLog(logger);
//OLD    _particleGeneratingModel->setLog(logger);
    _logger = logger;
}
/* TODO!!!
int I_PF_AdditiveModel_Impl::createEmptyParamsOfModel(size_t& size, IParams** ptrToPtrIParams) const
{
  std::cout << "NOT implemented I_PF_AdditiveModel_Impl::parseAndCreateModelParams!" << std::endl;

  I_PF_AdditiveModelParams* params = I_PF_AdditiveModelParams::parseAndCreateParams((const I_PF_AdditiveModel*)this, mapParams, postfix);
  return (IParamsUnion*)params;

};
*/
///TODO!!! parseModelParams ��� ������� ������ ����� ���� �����,
///TODO!!! � ��� ���������� - ��������� ������ parseModelParams ���������
int I_PF_AdditiveModel_Impl::parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParamsUnion** ptrToPtrIParamsUnion) const
{
  std::cout << "NOT implemented I_PF_AdditiveModel_Impl::parseAndCreateModelParams!" << std::endl;
/*
///    I_PF_AdditiveModelParams* params = I_PF_AdditiveModelParams::parseAndCreateParams((const I_PF_AdditiveModel*)this, mapParams, postfix);
//IParamsUnion* createAdditiveModelParams(const size_t totalOfAddends, const IParams** addends)

    size_t size=0;
    int rc = this->getTotalOfAddends(size);
    IParams* addendParams[size];
    IParams** ptrAddendParams = addendParams;

    /// the clones/copies of existing _addends retrieved
    IAdditiveModel_Impl * ptrIAdditiveModel = (IAdditiveModel_Impl *)this;
      if (rc)
    {
    }
///TODO!! Hard-code!
//    addends[0] = noiseModel->parseAndCreateModelParams(mapParams, postfix);
//    addends[1] = processModel->parseAndCreateModelParams(mapParams, postfix);

    I_PF_AdditiveModelParams* params = IParamsUnion::createAdditiveModelParams(size, (const IParams**)ptrAddendParams);
///TODO!!! ��������� ������ �� ������ �� ������ ��� ���������� ���������� ������!
/// � ������ ���� ���� �� ��������� �� ��������! ��� ������ ��������� ��, �� ������ ����� ����������� ���������
/// � ��� ��� ������ ������� ������ I_PF_AdditiveModelParams::parseParams(this, map<std::string, std::string>& mapParams, std::string postfix);
//    params->noiseParams = noiseModel->parseAndCreateModelParams(mapParams, postfix);
//    params->processParams = processModel->parseAndCreateModelParams(mapParams, postfix);
    return (IParamsUnion*)params;
*/

}

IParams* I_PF_AdditiveModel_Impl::getPtrToNoiseParams(const IParamsUnion*& ptrToIParamsUnion) const
{
    if(NULL == ptrToIParamsUnion)
    {
        return NULL;
    }
    size_t totalOfItems=0;
    ptrToIParamsUnion->getTotalOfItemsInParamsUnion(totalOfItems);
    if(totalOfItems != I_PF_AdditiveModel::dim_I_PF_AdditiveModel)
    {
        return NULL;
    }
    IModel * model = dynamic_cast<IModel*>(_noiseModel);
    if(NULL == model)
    {
        return NULL;
    }
    IParams * ptrToParams = ptrToIParamsUnion->ptrIParamsUnionToPtrIParams(_noiseModelIndex);
    if(model->canCastModelParams(ptrToParams))
    {
        return ptrToParams;
    }
    return NULL;
};

IParams* I_PF_AdditiveModel_Impl::getPtrToParticleGeneratingParams(const IParamsUnion*& ptrToIParamsUnion) const
{
    if(NULL == ptrToIParamsUnion)
    {
        return NULL;
    }
    size_t totalOfItems=0;
    ptrToIParamsUnion->getTotalOfItemsInParamsUnion(totalOfItems);
    if(totalOfItems != I_PF_AdditiveModel::dim_I_PF_AdditiveModel)
    {
        return NULL;
    }
    IModel* model = dynamic_cast<IModel*>(_particleGeneratingModel);
    if(NULL == model)
    {
        return NULL;
    }
    IParams * ptrToParams = ptrToIParamsUnion->ptrIParamsUnionToPtrIParams(_particleGeneratingModelIndex);
    if(model->canCastModelParams(ptrToParams))
    {
        return ptrToParams;
    }
    return NULL;
}

/* USING IAdditiveModel_Impl so far
bool I_PF_AdditiveModel_Impl::canCastModelParams(IParamsUnion * params) const
{
    return false;
};
*/
/// non-invariant for every model
/* TODO!!!
I_PF_AdditiveModelParams* I_PF_AdditiveModel_Impl::makeEmptyModel()
{
///TODO!!! ����� �� ������. �� this, ������ ���������� ��������� � �� ����!
///TODO!!! PF_Params* params = new PF_Params();

    params->noiseParams = noiseModel->makeEmptyParamsOfModel();
    params->processParams = processModel->makeEmptyParamsOfModel();

    return params;
}
*/

///I_PF_Model Interface requires IParamsUnion*
///I_PF_AdditiveModelParams MUST provide
/// IParams* noiseParams
/// IParams* processParams
/// but models work with single IParams

I_PF_Model const * const I_PF_AdditiveModel_Impl::getNoiseModel() const
{
    return (I_PF_Model const * const) _noiseModel;
};

I_PF_Model const * const I_PF_AdditiveModel_Impl::getParticleGeneratingModel() const
{
    return (I_PF_Model const * const) _particleGeneratingModel;
};
/** � ��������� ������������ ������������ ��� I_PF_Model ���������� ������
   �������� IParamsUnion* h
*/
double I_PF_AdditiveModel_Impl::estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const
{
///TODO!!! ?�� ����� ������� estimatePointMove �� I_PF_Model ���������� ������  ��� Using �� ������� ����������?
    std::cout << "NOT Implemented  estimatePointMove() in I_PF_AdditiveModel_Impl model" << std::endl;
///TODO!!!    _task->finish(MODEL_ERROR, "NOT Implemented  estimatePointMove() in I_PF_AdditiveModel_Impl model");
};
///TODO!!! ����� �� ����� ������� estimateHypothesis �� I_PF_Model ���������� ������
double I_PF_AdditiveModel_Impl::estimateHypothesis(const size_t indexOfNoiseParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const
{
//OLD    I_PF_AdditiveModelParams* hyp = (I_PF_AdditiveModelParams*)h;
//OLD    return _noiseModel->estimateHypothesis(indexOfParams, hyp->noiseParams, point, particles, t);
    return _noiseModel->estimateHypothesis(indexOfNoiseParams, h, point, particles, t);
}
///TODO!!! ����� �� ����� ������� initParticles �� I_PF_Model ���������� ������
int I_PF_AdditiveModel_Impl::initParticles(const size_t indexOfParticleGeneratingParams, IParamsUnion* h, std::vector<double>& particles) const
{
//OLD    I_PF_AdditiveModelParams* hyp = (I_PF_AdditiveModelParams*)h;
//OLD    _particleGeneratingModel->initParticles(indexOfParams, hyp->processParams, particles);
    _particleGeneratingModel->initParticles(indexOfParticleGeneratingParams, h, particles);
    return _RC_SUCCESS_;
}
///TODO!!! ����� �� ����� ������� generateParticles �� I_PF_Model ���������� ������
int I_PF_AdditiveModel_Impl::generateParticles(const size_t indexOfParticleGeneratingParams, IParamsUnion* h, std::vector<double>& particles) const
{
//OLD    I_PF_AdditiveModelParams* hyp = (I_PF_AdditiveModelParams*)h;
//OLD    _particleGeneratingModel->generateParticles(0, hyp->processParams, particles);
    _particleGeneratingModel->generateParticles(indexOfParticleGeneratingParams, h, particles);
    return _RC_SUCCESS_;
}

/**************************************************
*  Static factory to create I_PF_AdditiveModel models
***************************************************/
/// NB ������ ������������� ���� ���������� � ���������� I_PF_Model ���������� ������!
///�.�. ��� ���������� ������, ��� �� �������������,
///���� ����������� � ������ ����������, �� ������������������� ��� ��������� ����������� �� ��������� ������ addends[0] � addends[1]
///����!
/// ����� ����� ��� ������ PF-���������� ������� setModel(),
/// �� ��� ��������� ����� ������ ������ setModel() ������� public ����� I_PF_Model ���������� ������,
/// � ����������� ����� ������� ������������� ���� ����������!
/// ����� ���� ���� �� ���� �� �����! ������� ���������� ������ ������ ����� ��� ��������� ������� ����� �����!
/// ���� ��������� ������ ������ ��� ������ setModel(), ������ ��� ��������� �� �� ������ (�� ����������, �� ����������� I_PF_Model, ...)
I_PF_AdditiveModel* I_PF_AdditiveModel::createModel(std::string noiseModelName, std::string particleGeneratingModelName, Logger * const logger)
{
    enum IModel::ModelsTypes* addends = new enum IModel::ModelsTypes[dim_I_PF_AdditiveModel];
    addends[0]= IModel::getModelTypeByModelName(noiseModelName);
    addends[1]= IModel::getModelTypeByModelName(particleGeneratingModelName);
    /** �������� ���������� ������ � PF-����������� � ��������, ��� �� ��������� ���� ��������� ���� ���������
    */
///TODO!!! ����� �������������� ������� ���� �����������! �� ����������� ����! I_PF_AdditiveModel_Impl* additiveModel = new I_PF_AdditiveModel_Impl(dim_I_PF_AdditiveModel, (const enum IModel::ModelsTypes*)addends, logger);
    I_PF_AdditiveModel_Impl* additiveModel = new I_PF_AdditiveModel_Impl(addends, logger);

    for(size_t i=0; i < dim_I_PF_AdditiveModel; ++i)
    {
        I_PF_Model* model = NULL;
        additiveModel->getAddend(i, (IModel *&)model);
        if(dynamic_cast<I_PF_Model*>(model) == NULL)
        {
            delete additiveModel;
            logger->error(ALGORITHM_MODELDATA_ERROR, "Can not cast AdditiveModel addend to I_PF_Model*.");
            return NULL;
        }
    }
    /** additiveModel->_noiseModel, additiveModel->_particleGeneratingModel
    ��� ������������������� ���� � CTOR'�
    */
    return additiveModel;
};///end static factory I_PF_AdditiveModel* I_PF_AdditiveModel::createModel()
/**************************************************
*  Static  Comparator to compare  IAdditiveModel models
***************************************************/
bool IAdditiveModel::compareModels(const IAdditiveModel* model_1, const IAdditiveModel* model_2)
{
    if ((model_1 == NULL) || (model_2 == NULL))
    {
        return false;
    }
    size_t totalAddends_1=0;
    model_1->getTotalOfAddends(totalAddends_1);
    size_t totalAddends_2=0;
    model_2->getTotalOfAddends(totalAddends_2);
    if (totalAddends_1 != totalAddends_2)
    {
        return false;

    }
    for(size_t i=0; i < totalAddends_1; ++i)
    {
        enum IModel::ModelsTypes mt_1;
        model_1->getAddendType(i, mt_1);
        enum IModel::ModelsTypes mt_2;
        model_2->getAddendType(i, mt_2);
        if (mt_1 != mt_2)
        {
            return false;
        }
    }
    return true;
};///end static bool IAdditiveModel::compareModels()

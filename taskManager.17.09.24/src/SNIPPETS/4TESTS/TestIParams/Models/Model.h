#ifndef __MODEL_H__633569422542968750
#define __MODEL_H__633569422542968750

#include <string>
#include <vector>
#include <map>
#include <math.h>
#include "../Log/Log.h"

/** IAA : DON'T pollute global scope! NO WAY in h-file -> using namespace std; */

/** forward declarations */
///TODO!!!  Task ������� ������ �� �����!
///class Task;
class IParams;
struct tagDataInfo;
typedef tagDataInfo DataInfo;
/** abstract Base class - interface */
class IModel
{
public:
    enum ModelsTypes{
        PlainModel,
        PolynomialModel,
        GaussianModel,
        WienerModel,
        PoissonModel,
        ParetoModel,
        PoissonDrvnParetoJumpsExpMeanRevertingModel,
        ADDITIVE_MODEL,
        FAKE_MODEL
    };

    static enum ModelsTypes getModelTypeByModelName(const std::string& modelName);
    static const char* modelNames[FAKE_MODEL];
    /** � ����� ������ ������ ����� ������ factory,
    ����� ������� ��������� ������.
    �� ����� ��� �����? � ���������� IModel,
    ����� ����� � ������ ��������� ������ ���� ������?
    ����� ����� ����� �������������� ���������� ��� ���������
    ����������� ������ ������
    */
    /** ����� �������� � �������������� ��� PLAIN_MODEL
    �������� ����� �������� ���������� ��������� ptrModelParams,
    ������� �� ������� NULL!
    � ��� �������, � ������� ����������� ������ ����������,
    ��������� �������� ����� ��������������, ���� ���� �� �� NULL
    */
//OLD    static IModel* createModel(const enum IModel::ModelsTypes modelType, const size_t dim=0);
//OLD    static IModel* createModel(const std::string& modelName, const size_t dim=0);
    static IModel* createModel(const enum IModel::ModelsTypes modelType, const IParams* ptrModelParams=NULL);
    static IModel* createModel(const std::string& modelName, const IParams* ptrModelParams=NULL);

    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    IModel(){};
    virtual ~IModel() {};
    virtual std::string toString() const =0;
    /** ������ ������� ���������������� ������ � ����������� ������
     ����������� ��� ������� ���������!
    ����� ���� ������� �� ���������� Task/Estimator!
    ������ ���� ��������� ������ ��� ������!
    �� �� ���������� � ���� ����������� ������� � ������ �� �����!
    ����� �� ��������� ��������� �� ��������� ������
    ������ �� ��������� ������ �� ��� � ��������� ����������
    � ������ �����, ������� �������� ����� ���������� ���� �� ����, ���� ������ this
        static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams) const =0;
    is required by ParamsSet's method :
    bool fillParams(std::string srcName, IModel* model);
  */

///������ ����� �������������� �� ������ ���������� � ���� ���������� ������!
/// ���� ������, �� ����� ������ �������� enum IModel::ModelsTypes modelType,
///���������� ��� IParams** ����� �������� "������ ��������" ptrToPtrIparams, ������ � size ����� ������� ��������!
///� ��� ������� - ��������� �������
    static int parseAndCreateModelParams(const enum IModel::ModelsTypes mt, std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * logger);
///static factory
//OLD    static int parseDataInfoAndCreateModelParams(const enum IModel::ModelsTypes mt, size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams, Logger* const logger);

    /** ��� � ���� ������� ��� ����������� ���������
    �� ��������� ������ ��� ���� ������ ����������
    */
    virtual bool canCastModelParams(IParams * params) const = 0;
    /** ����������, ������� ����� ������ ��� ��������
    ����������� � ParamsSet/ParamsCompact ����������:
    �� �� ���������, ����� � ���� ����������� ������
    ���������� ��������� ������ �������
    */
    static bool compareModels(const IModel* model_1, const IModel* model_2);
    /** ��� ��� ������ ����� ����������� � ������� ������! */
    /** NO WAY -> setModelType()!
     REDEFINITION of Model Type FORBIDDEN!
    */
    virtual void getModelType(enum IModel::ModelsTypes& modelType) const = 0;
    /** ��� � ���� ������� ��� ���� �����, ��� �����������
    ��� ������ ��� ���� ������ ����������
    */
    virtual size_t getModelParamsType() const = 0;
    /**
    ��� ������� ��������� ���������� DataInfo
    � ��������� ���������� ���� ������
    � �������� � ����������  DataInfo*->doub;e **
    ������������� NAN'�
    */
    virtual int parseDataInfoAndCreateModelParams(size_t totalOfDataInfo, const DataInfo ** const ptrToPtrDI, IParams*& ptrToPtrIparams) const = 0;
///TODO!!!  Task ������� ������ �� �����!
///    virtual void setTask(Task* const & task) =0;

    virtual void setLog(Logger* const & log) =0;
/** ��������� ������ ������� - ��� ��� ����������! */
protected:
// ��� ����� ���� ����� static �� ������ ������, �� ��� �������������� ����� ��� � ������ ����������
//    virtual void parsingFailed(std::string msg) const =0;

private:
    /** assign and copy CTORs are disabled */
    IModel(const IModel&);
    void operator=(const IModel&);
};
/** forward declaration */
class IParamsUnion;
/** abstract class - interface for Probabilistic estimators */
/** ������������ �� IModel ��������� ��������� Task, Log */
class I_Probabilistic_Model : public virtual IModel
{
public:
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    I_Probabilistic_Model(){};
    virtual ~I_Probabilistic_Model(){};

    /**
    ��������� ������� � ���������� ���������� ���������� I_Probabilistic_Model
    �������� ���������� ��� ���������� ������ ������� �� Distributions\Distributions.cpp!
    �.�. ���� ���������� ������ ���� � Distributions\Distributions.cpp
    � � ���� �������� ������ ���������/����������� ���������� ����� ����������,
    ����������� ��� ��������� � ���������� ������ ������� �� Distributions\Distributions.cpp!
    */
    virtual int CDF(int indexOfParams, IParamsUnion* h, double& valueOfCDF, unsigned t) const = 0;
    virtual int PDF(int indexOfParams, IParamsUnion* h, double& valueOfPDF, unsigned t) const = 0;
    virtual int getMaxMode(int indexOfParams, IParamsUnion* h, double& valueOfMaxMode) const = 0;
    virtual int generateRV(int indexOfParams, IParamsUnion* h, double& randomValue) const = 0;
private:
    /** assign and copy CTORs are disabled */
    I_Probabilistic_Model(const I_Probabilistic_Model&);
    void operator=(const I_Probabilistic_Model&);
};

/** forward declaration */
/** ����� ������������ ������ ParamsUnion,
������ ��� � ����� ������ ������ ��� ����������
�������� ������ ������ ���� ����������,
�� �� �� ��������� � ������, ����� � ��� ���������� ������
������ ������ ���� ���������.
� ��������� ������ IParamsUnion ����� ���������
������ ���� ������� � ������� ���������� IParams ** _addends
(size_t _totalOfAddends = 1)
*/
class IParamsUnion;

/** abstract class - interface for PF-estimators */
/**
 Task ������� ������ �� �����!
 �� �����! ������������ �� IModel ��������� ��������� Task, Log
 �������� ��������� I_PF_Model � IModel
 � �������� Task, Log

*/

///TODO!!! I_PF_Model ������ ����������� �� I_Probabilistic_Model
class I_PF_Model // �� ����� ��-�� Logger'� ����� ��� ������������ : public virtual IModel
{
public:
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    I_PF_Model(){};
    virtual ~I_PF_Model(){};

    ///TODO!!! ��� ��� ���� ������ ���������� MODEL_PARAMS_SRC � ������ ������ � ������ �������� ptrToPtrIparams ������������ ������ ���� ������
    /// � ������ �� ��� ��� �����, ����� ��� ������� �� ����� ������� �������� �������, ������� ���������� � ����� PF-����������,
    /// ������ ��� ��� ��������� ����� ������� ������� �� ������!
    static int parseAndCreateModelParamsCompact(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);

    ///TODO!!! ������ ����� ������ �� ������� �� ���������� I_Probabilistic_Model
    virtual double estimateHypothesis(const size_t indexOfParams, IParamsUnion* h, double point, std::vector<double>& particles, unsigned t) const = 0;
    ///TODO!!! ������ ����� ������ �� ������� �� ���������� I_Probabilistic_Model
    virtual double estimatePointMove(const size_t indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

    virtual int initParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const = 0;
    ///TODO!!! ������ ����� ������ �� ������� �� ���������� I_Probabilistic_Model
    virtual int generateParticles(const size_t indexOfParams, IParamsUnion* h, std::vector<double>& particles) const = 0;
private:
    /** assign and copy CTORs are disabled */
    I_PF_Model(const I_PF_Model&);
    void operator=(const I_PF_Model&);
};

/** forward declaration */
class IParams1D;

class IModelPlain : public virtual IModel
{
public:
    static IModelPlain* createEmptyModel(const size_t& dim);
///TODO!!! ����� ��� ������ �� �����???
    static int createEmptyParamsOfModel(size_t& dim, IParams*& ptrToPtrIparams);
///TODO!!! ??? IParams** ptrToPtrIparams -> IParams*& ptrToPtrIparams? Semantics of size?
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
    virtual ~IModelPlain(){};
    virtual void setDimension(const size_t& dim) = 0;
    virtual int getDimension(size_t& dim) const = 0;
    /** to make params of IModelPolynomial*/
protected:
    IModelPlain(){};
private:
    /** assign and copy CTORs are disabled */
    IModelPlain(const IModelPlain&);
    void operator=(const IModelPlain&);
};

class IModelPolynomial : public virtual IModel
{
public:
    static IModelPolynomial* createEmptyModel(const size_t& dim);
///TODO!!! ����� ��� ������ �� �����???
    static int createEmptyParamsOfModel(size_t& dim, IParams*& ptrToPtrIparams);
///TODO!!! ??? IParams** ptrToPtrIparams -> IParams*& ptrToPtrIparams? Semantics of size?
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
    static int evaluatePolynom(const double arg, const IParams*& coeffs, double& value);
    virtual ~IModelPolynomial(){};
    virtual void setDimension(const size_t& dim) = 0;
    virtual int getDimension(size_t& dim) const = 0;
    /** to make params of IModelPolynomial*/

protected:
    IModelPolynomial(){};
private:
    /** assign and copy CTORs are disabled */
    IModelPolynomial(const IModelPolynomial&);
    void operator=(const IModelPolynomial&);
};

/** forward declaration */
class IParamsGaussian;
///TODO!!! IModelGaussian ������ ����������� ��� � �� I_Probabilistic_Model
class IModelGaussian : public virtual IModel
{
public:
    IModelGaussian(){};
    virtual ~IModelGaussian(){};
    /** to make params of IModelGaussian */
    static int createEmptyParamsOfModel(IParams*& ptrToPtrIparams);
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
///TODO!!! ������ ��� ���� ���������, ��������� ��������� ����� Quantile ������ �������� ��������� mu, sigma, � ���������� p-value!
    static int Quantil0999(const std::vector<double>& sampleOfOrderedAbsItems, double& quantile);
/// �������� ������ ��� ������������� ����? � �������? ����� ���� ����� ����� � �����-�� ����������?
///???    IModelGaussian* makeEmptyModel();

//OLD ������ �� �������� ���������� PF_Model ��� � ����������    virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������   virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelGaussian(const IModelGaussian&);
    void operator=(const IModelGaussian&);
};

/** forward declaration */
class IParamsWiener;
///TODO!!! IModelWiener ������ ����������� ��� � �� I_Probabilistic_Model
class IModelWiener : public virtual IModel
{
public:
    IModelWiener(){};
    virtual ~IModelWiener(){};
    /** to make params of IModelWiener */
    static int createEmptyParamsOfModel(IParams*& ptrToPtrIparams);
//OLD!    virtual IParams* parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
/// �������� ������ ��� �������������� ����? � �������? ����� ���� ����� ����� � �����-�� ����������?
///???    IModelWiener* makeEmptyModel();

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelWiener(const IModelWiener&);
    void operator=(const IModelWiener&);
};

/** forward declaration */
class IParamsPoisson;
///TODO!!! IModelPoisson ������ ����������� ��� � �� I_Probabilistic_Model
class IModelPoisson : public virtual IModel
{
public:
    IModelPoisson(){};
    virtual ~IModelPoisson(){};
    /** to make params of IModelPoisson */
    static int createEmptyParamsOfModel(IParams*& ptrToPtrIparams);
//OLD!    virtual IParams* parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
/// �������� ������ ��� �������������� ����? � �������? ����� ���� ����� ����� � �����-�� ����������?
///???    IModelPoisson* makeEmptyModel();

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelPoisson(const IModelPoisson&);
    void operator=(const IModelPoisson&);
};

/** forward declaration */
class IParamsPareto;
///TODO!!! IModelPareto ������ ����������� ��� � �� I_Probabilistic_Model
class IModelPareto : public virtual IModel
{
public:
    IModelPareto(){};
    virtual ~IModelPareto(){};
    /** to make params of IModelPareto */
    static int createEmptyParamsOfModel(IParams*& ptrToPtrIparams);
//OLD!    virtual IParams* parseAndCreateModelParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * log);
/// �������� ������ ��� �������������� ����? � �������? ����� ���� ����� ����� � �����-�� ����������?
///???    IModelPareto* makeEmptyModel();

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimateHypothesis(int indexOfParams, IParamsUnion* h, double point, vector<double>& particles, unsigned t) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual double estimatePointMove(int indexOfParams, IParamsUnion* h, double move, unsigned t) const = 0;

private:
    /** assign and copy CTORs are disabled */
    IModelPareto(const IModelPareto&);
    void operator=(const IModelPareto&);
};

/** forward declaration */
class IParamsPoissonDrvnParetoJumpsExpMeanReverting;
///TODO!!! IModelPoissonDrvnParetoJumpsExpMeanReverting ������ ����������� ��� � �� I_Probabilistic_Model
class IModelPoissonDrvnParetoJumpsExpMeanReverting : public virtual IModel
{
public:
    /** ��� ��� �������� ������������� � ���������� ��������� ���������, ���������� �� ��� ������,
    ����� ��� ����� ����� ���� ���������� ���� ������
    */
    /** DEFINED as "ExpMeanRevertingLambda" in implementation (see Model.cpp file) */
    static const char * strExpMeanRevertingLambda;
    IModelPoissonDrvnParetoJumpsExpMeanReverting(){};
    virtual ~IModelPoissonDrvnParetoJumpsExpMeanReverting(){};
    /** to make params of IModelPoissonDrvnParetoJumpsExpMeanReverting */
    static int createEmptyParamsOfModel(IParams*& ptrToPtrIparams);
    static int parseAndCreateModelParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIparams, Logger * logger);

//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual int initParticles(int indexOfParams, IParamsUnion* h, vector<double>& particles) const = 0;
//OLD ������ �� �������� ���������� PF_Model ��� � ����������virtual int generateParticles(int indexOfParams, IParamsUnion* h, vector<double>& particles) const = 0;
private:
    /** assign and copy CTORs are disabled */
    IModelPoissonDrvnParetoJumpsExpMeanReverting(const IModelPoissonDrvnParetoJumpsExpMeanReverting&);
    void operator=(const IModelPoissonDrvnParetoJumpsExpMeanReverting&);
};
#endif

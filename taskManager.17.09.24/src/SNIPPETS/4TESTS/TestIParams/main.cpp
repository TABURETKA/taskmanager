#include <iostream>
#include <string>
#include <vector>
#ifdef _DEBUG
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#endif
#include "Params/Params.h" //IParams

/** IAA: to change current directory (cross-platform solutions ONLY!) */
#ifdef _WIN32
#include <direct.h>
// MSDN recommends against using getcwd & chdir names
#define GET_CWD _getcwd
#define CD _chdir
#else /** POSIX-standard */
#include "unistd.h"
#define GET_CWD getcwd
#define CD chdir
#endif
int main(int argc, char *argv[])
{

///begin DEBUG
    /** create empty Params1D
    */
    IParams * ptr= IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, 10);
    /** fill Params1D with data
    */
    for(int i=0; i < 10; i++)
    {
        ptr->setParam(i,i*2);
    }
    /** get data from Params1D
    */
    IParams * ptr_copy = ptr->clone();
    delete ptr;
    size_t size = ptr_copy->getSize();
    for(int i=0; i < size; i++)
    {
        double value;
        ptr_copy->getParam(i,value);
        std::cout << i << ":" << value<< std::endl;
    }

    IParamsGaussian * ptrGauss= dynamic_cast<IParamsGaussian *>(IParams::createEmptyParams(NULL, IParams::ParamsTypes::GaussianParams));
    ptrGauss->setMean(1.1);
    ptrGauss->setSDeviation(2.2);
    IParamsGaussian * ptrGauss_copy= dynamic_cast<IParamsGaussian *>(ptrGauss->clone());
    delete ptrGauss;
    size =ptrGauss_copy->getSize();
    for(int i=0; i < size; i++)
    {
        double value;
        ptrGauss_copy->getParam(i,value);
        std::cout << i << ":" << value<< std::endl;
    }

    IParamsPoissonDrvnParetoJumpsExpMeanReverting * ptrPDPJ= dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting *>(IParams::createEmptyParams(NULL, IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams));
    ptrPDPJ->setPoissonLambda(1.1);
    ptrPDPJ->setMRLambda(2.2);
    ptrPDPJ->setParetoXm(0.5);
    ptrPDPJ->setParetoK(3.0);

    IParamsPoissonDrvnParetoJumpsExpMeanReverting * ptrPDPJ_copy= dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting *>(ptrPDPJ->clone());
    delete ptrPDPJ;
    size = ptrGauss_copy->getSize();
    for(int i=0; i < size; i++)
    {
        double value;
        ptrPDPJ_copy->getParam(i,value);
        std::cout << i << ":" << value<< std::endl;
    }

    ptrPDPJ= dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting *>(IParams::createEmptyParams(NULL, IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams));
    ptrPDPJ->setPoissonLambda(1.1);
    ptrPDPJ->setMRLambda(2.2);
    ptrPDPJ->setParetoXm(0.5);
    ptrPDPJ->setParetoK(3.0);

    ptrPDPJ_copy= dynamic_cast<IParamsPoissonDrvnParetoJumpsExpMeanReverting *>(ptrPDPJ->clone());
    delete ptrPDPJ;
    size = ptrPDPJ_copy->getSize();
    for(int i=0; i < size; i++)
    {
        double value;
        ptrPDPJ_copy->getParam(i,value);
        std::cout << i << ":" << value<< std::endl;
    }
///end DEBUG
  return 0;
}

#ifndef _DISTRIBUTIONS_H_
#define _DISTRIBUTIONS_H_
#include "gsl/gsl_rng.h"

class Distributions
{
public:
    Distributions();
    ~Distributions();
    static int InitGenerator();
    static int FreeGenerator();
    static double Uniform();
    static double Normal(double mean, double sigma);
    static double Exponential(double lam);
    static double Pareto(double x_mean, double k);
    static double Heviside(double x);
    static double Phi(double x, double mu, double sigma);
    static double Phi_pdf(double x, double mu, double sigma);

private:
    static gsl_rng * generator;
};

#endif

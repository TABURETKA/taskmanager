//#include <stdlib.h>
#include "distributions.h"

#define _USE_MATH_DEFINES
#include "../CommonStuff/MathDefines.h" /* M_PI, etc */
#include <cmath>
#include <time.h>
#include "gsl/gsl_randist.h"
#include "../Log/errors.h" // rc codes

Distributions::Distributions()
{}
Distributions::~Distributions()
{}
/** We use GSL::mersenne-twister generator */
gsl_rng * Distributions::generator = 0;

int Distributions::InitGenerator()
{
    if (! Distributions::generator)
    {
        Distributions::generator = gsl_rng_alloc(gsl_rng_mt19937);
        if (!Distributions::generator)
        {
///TODO!!! handle allocation error somewhow!
///For example: Throw user-defined exception!
        }
        else
            gsl_rng_set(Distributions::generator, (unsigned long)time(NULL));
    }
  return _RC_SUCCESS_;
}

int Distributions::FreeGenerator()
{
    if (Distributions::generator)
    {
        gsl_rng_free (Distributions::generator);
    }
    return _RC_SUCCESS_;
}

double Distributions::Uniform()
{
  /** Merely for safety-sake! */
  if (! Distributions::generator)
      Distributions::InitGenerator();
  return gsl_ran_flat(Distributions::generator, 0, 1);
/// DON'T USE! ((rand() + 0.5) / (RAND_MAX + 1.0));
}

double Distributions::Normal(double mean, double sigma)
{
  double x1 = Uniform();
  double x2 = Uniform();
  double norm = sqrt(-2 * log(x1)) * sin(2 * M_PI * x2);

  return mean + sigma * norm ;
}

double Distributions::Exponential(double lam)
{
  return -1 / lam * log(1 - Uniform());
}

double Distributions::Pareto(double x_mean, double k)
{
  return x_mean / pow(1 - Uniform(), 1 / k);
}

double Distributions::Heviside(double x)
{
  if (x > 0)
    return 1.;
  return 0;
}

static const double rel_error= 1E-12;

/*
double erfc(double x);

double erf(double x)
{
  static const double two_sqrtpi=  1.128379167095512574;        // 2/sqrt(pi)
  if (fabs(x) > 2.2) {
    return 1.0 - erfc(x);        //use continued fraction when fabs(x) > 2.2
  }
  double sum= x, term= x, xsqr= x*x;
  int j= 1;

  do {
    term*= xsqr/j;
    sum-= term/(2*j+1);
    ++j;
    term*= xsqr/j;
    sum+= term/(2*j+1);
    ++j;
  } while (fabs(term/sum) > rel_error);   // CORRECTED LINE

  return two_sqrtpi*sum;
}


double erfc(double x)
{
  static const double one_sqrtpi=  0.564189583547756287;        // 1/sqrt(pi)

  if (fabs(x) < 2.2) {
    return 1.0 - erf(x);        //use series when fabs(x) < 2.2
  }
  if (x < 0) {               //continued fraction only valid for x>0
    return 2.0 - erfc(-x);
  }
  double a=1, b=x;                //last two convergent numerators
  double c=x, d=x*x+0.5;          //last two convergent denominators
  double q1, q2= b/d;             //last two convergents (a/c and b/d)
  double n= 1.0, t;

  do {
    t= a*n+b*x;
    a= b;
    b= t;
    t= c*n+d*x;
    c= d;
    d= t;
    n+= 0.5;
    q1= q2;
    q2= b/d;
  } while (fabs(q1-q2)/q2 > rel_error);

  return one_sqrtpi*exp(-x*x)*q2;
}
*/

double Distributions::Phi(double x, double mu, double sigma)
{
  return 0.5 * ( 1. + erf( (x - mu) / (sqrt(2.) * sigma) ) );
}


double Distributions::Phi_pdf(double x, double mu, double sigma)
{
  return 1 / (sqrt(2. * M_PI) * sigma) * exp(-(x - mu) * (x - mu) / (2. * sigma * sigma));
}

#include "ParamSet.h"
#include "Params.h"
#include "../Config/ConfigParser.h"
#include "../Log/errors.h" /* rc codes */
/**
��-��������, ����������� �����, ������� ����� � ���� �����, ����� � ������� �� �����!
��, �� �����������, ��� � ����� � ��� �� ���� �� ����� ������ Params/ParamsUnion �� ������ �������.
����� ������������ �� �� ������� ���������.
*/
#include "../Models/Model.h"/* IModel */
#include "../Models/AdditiveModel.h"/* IAdditiveModel */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>


namespace{
/** IParamsSet Implementation MOVED here
 ��������� ��� �������� ������ ������ �����
 ���������� (�� ����������) ������!
 �� ��� �� ����� ������������ � ��� ������������ ������
 ������� (�� ����������) ������
 ������ ����� ���������� ��������� ��������� ����� Set'�,
 ����� ����� ���� ������, ��� ������ �������, � ��� - �������.
 ??? ������/�������� �������� ������� points?

 �������� �������� ������: �� ����������� � ���� ����
 ��������� ������ ����� ���������� ������?
 ��� � ���� ����� ������ ��������� ����� ������?
*/
/*************************************************************
*               Set of Points in Euclidean Space
*************************************************************/
class IParamsSet_Impl : public virtual IParamsSet
{
public:
///TODO!!! ������� const IModel* _model, ��� ����� ���������� �� setModel!
/// �.�. ���������������� ��� ���� ����� ��, � CTOR'�
/// � ����� � ������� ������� ����� ��� �� ������
    IParamsSet_Impl(const IModel* model);
    ~IParamsSet_Impl();
    virtual void clear();
    /**
     �������� �������� ������: �� ����������� � ���� ����
     ��������� ������ ����� ���������� ������?
     ��� � ���� ����� ������ ��������� ����� ������?
     � ��������� ������ ��������� �������� �������������
     ��������� ���������� ����������.

    */
    /** ��������� ��������� ������
    � ���������� vector<IParams*> points,
    ����� ��������� ��� (��������� � ����)
    �� ����� ����� �� ��������! */
    /** this method requires virtual castModelParams() in IModel-interface */
    virtual bool addParamsCopyToSet(const IModel* model, const IParams* params, Logger* log);
    /** this method requires virtual createModelParams() in IModel-interface */
    virtual bool fillParamsSet(const IModel* model, const std::string& srcName, Logger * log);

///REDUNDANT virtual int setModel(const IModel* model, Logger* log);
    virtual size_t getSetSize() const;
    virtual int getCurrentId() const;
    virtual int setCurrentId(const size_t id);
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getNextPtr();
    virtual IParams* getParamsCopy(const size_t id) const;
    /** check if end is reached */
    virtual bool end() const;

private:
    /** container to keep points (IParams) of this SET */
    /** ���������� ��� ������ ����� �����,
    � �������� ������ ����� � ������� getSetSize()
    */
    std::vector<IParams*> _points;
    /** id of current IParams in this Set */
    int _id;
    const IModel* _model;
};
/*************************************************************
*    Set of Points in Cartesian Product of Euclidean Space
*************************************************************/
/************************************************
IParamsUnionSet Implementation MOVED here
 ��������� ��� �������� ������ ������ �����
 ���������� ������!
 �� ��� �� ����� ������������ � ��� ������������ ������
 ���������� ������
  ������ ����� ���������� ��������� ��������� ����� Set'�,
 ����� ����� ���� ������, ��� ������ �������, � ��� - �������.
??? ������/�������� �������� ������� points?
*/
class IParamsUnionSet_Impl : public virtual IParamsUnionSet
{
public:
///TODO!!! ������� const IAdditiveModel* _additiveModel, ��� ����� ���������� �� setModel!
/// �.�. ���������������� ��� ���� ����� ��, � CTOR'�
/// � ����� � ������� ������� ����� ��� �� ������
    IParamsUnionSet_Impl(const IAdditiveModel* additiveModel);
    ~IParamsUnionSet_Impl();

    /** this method requires virtual compareModels() in IAdditiveModel-interface */
    virtual bool addParamsUnion(const IAdditiveModel* model, IParamsUnion* params, Logger* log);

    virtual bool fillParamsUnionSet(const IAdditiveModel* model, const std::string& srcName, Logger * log);

///REDUNDANT virtual int setModel(const IAdditiveModel* additiveModel, Logger* log);
    virtual size_t getSetSize() const;
    virtual int getCurrentId() const;
    virtual int setCurrentId(unsigned id);
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getNextPtr();
    virtual IParamsUnion* getParams(unsigned id);

    virtual bool end() const;

private:
    /** container to keep items (IParamsUnion) of this SET */
    std::vector<IParamsUnion*> _points;
    /** id of current ARRAY IParamsUnion in this Set */
    int _id;
    const IAdditiveModel* _additiveModel;
};

}; ///end of anonymous namespace
/***********************************************
*                 IMPLEMENTATION
************************************************/
IParamsSet_Impl::IParamsSet_Impl(const IModel* model) : _id(-1),
                                                  _model(model)
{};

IParamsSet_Impl::~IParamsSet_Impl()
{
    this->clear();
};

void IParamsSet_Impl::clear()
{
    _points.clear();
    _id=-1;
};
/***
* Fill set with params values from file/db.
*********************************************/
//TODO!!!
bool IParamsSet_Impl::fillParamsSet(const IModel* model, const std::string& srcName, Logger * log)
{
    /** ������ ��������� ������������ ��� IModel,
    ����� ���������, ��� ������ ��������� ������,
    ��� ������� �� ���� ��������� � �������
    */
    bool rc = true;
    if (_model != NULL)
    {
        rc = IModel::compareModels(_model, model);
        if (! rc)
        {
            log->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong model type in ParamsSet::fillParams()!");
            return false;
        }
    }
    else
    {
        /** This Cheating happens once for all ParamsSet life-time ! */
        _model = const_cast<IModel *> (model);
    }
    ConfigParser config(srcName);
    if (!config.fileExists()) {
        return false;
    }

    ParamsMap mapParams = config.parse();
    iteratorParamsMap itParam = mapParams.find("NUMBER");
    if (itParam == mapParams.end()) {
        return false;
    }
    /** total of parameter to be parsed and added to the Set
    according to directive NUMBER=XXX into cfg-file
    */
    unsigned index = atoi(itParam->second.c_str());
    if (index == 0) {
        return false;
    }
    /** assume in file all IModel's params defined as follows:
     PARAM_1 = value
     ...
     PARAM_index = value
    */
    for (unsigned i = 1; i <= index; ++i) {
        char postfix[100];
        sprintf(postfix, "_%i", i);
///TODO!!! ����� ����������� ������
        size_t size =0;
        enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
        model->getModelType(mt);
        IParams** ptrToPtrIParams = NULL;
        int rc = IModel::parseAndCreateModelParams(mt, mapParams, postfix, size, ptrToPtrIParams, log);
        if (!rc)
        {
            break;
        }
        IParams* ptrToIParams = ptrToPtrIParams[size-1];
///TODO!!! ������������� ��� : "Overflow" <- !
        _points.push_back(ptrToIParams);
    }

    if (_points.size() < index) {
        for (unsigned i = 0; i < _points.size(); ++i) {
            delete _points[i];
        }
        return false;
    }

    return true;
}

bool IParamsSet_Impl::addParamsCopyToSet(const IModel* model, const IParams* params, Logger* log)
{
///TODO!!! � ����� �������� ���� ����� � params==NULL ???
    if (!params)
    {
        return false;
    }
    /** ������ ��������� ������������ ��� IModel,
    ����� ���������, ��� ������ ��������� ������,
    ��� ������� �� ���� ��������� � �������
    */
    bool rc = true;
    if (_model != NULL)
    {
        rc = IModel::compareModels(_model, model);
        if (! rc)
        {
            log->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong model type in ParamsSet::fillParams()!");
            return false;
        }
    }
///TODO!!! �����!
/* ��� ����, ����� �������������� ������ � ���������� ParamsSet,
   ������� ������������ ��� ������� setModel()
    else
    {
        /** This Cheating happens once for all ParamsSet life-time !
        /
        if(model) _model = const_cast<IModel *> (model);
    }
*/
    IParams * copyOfParams = params->clone();
///TODO!!! ������������� ��� : "Overflow" <- !
    _points.push_back(copyOfParams);
    return true;
};
/*OLD
int IParamsSet_Impl::setModel(const IModel* model, Logger* log)
{
    if(!_model)
    {
        log->log(LogLevels::ALGORITHM_1_LEVEL_INFO, "NOT NULL model is changed for ParamsSet!");
    }
    _model = const_cast<IModel *> (model);
    return _RC_SUCCESS_;
};
*/
size_t IParamsSet_Impl::getSetSize() const
{
    return _points.size();
}

int IParamsSet_Impl::getCurrentId() const
{
    return _id;
}

int IParamsSet_Impl::setCurrentId(const size_t id)
{
    if(id < _points.size())
    {
        this->_id = id;
        return _RC_SUCCESS_;
    }
    return _ERROR_WRONG_ARGUMENT_;
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParams* IParamsSet_Impl::getFirstPtr()
{
    if (_points.size() < 1)
        return NULL;

    _id = 0;
    return _points[_id];
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParams* IParamsSet_Impl::getNextPtr()
{
    ++_id;

    if (_id >= (int)_points.size())
    {
        _id = -1;
        return NULL;
    }
    return _points[_id];
}

IParams* IParamsSet_Impl::getParamsCopy(const size_t id) const
{
/** TODO!!! �������� ������ : ����� ��� ��������� ����� ������� ���������� �� ���� �� ��� � id �������� ������ ������?
    if (id < 0 || id >= points.size()) {
        this->id = -1;
        return NULL;
    }
    this->id = id;
*/
    if (id < _points.size())
    {
      IParams * paramsCopy = _points[id]->clone();
      return paramsCopy;
    }
    return NULL;
}

bool IParamsSet_Impl::end() const
{
    return (_id == -1);
}

/***************************************************
              IParamsUnionSet_Impl
****************************************************/
IParamsUnionSet_Impl::IParamsUnionSet_Impl(const IAdditiveModel* additiveModel) : _id(-1),
                                               _additiveModel(additiveModel)
{}
IParamsUnionSet_Impl::~IParamsUnionSet_Impl()
{
  _points.clear();
};

bool IParamsUnionSet_Impl::fillParamsUnionSet(const IAdditiveModel* model, const std::string& srcName, Logger * log)
{
// bool IAdditiveModel::compareModels(const IAdditiveModel* model_1, const IAdditiveModel* model_2)
///TODO!!! ��������� � ������ ����� ����� �� IAdditiveModel ����������
}

bool IParamsUnionSet_Impl::addParamsUnion(const IAdditiveModel* model, IParamsUnion* pu, Logger* log)
{
  bool isAdditiveModelsEqual = IAdditiveModel::compareModels(model, _additiveModel);
  if ((isAdditiveModelsEqual) && (_additiveModel->canCastModelParamsUnion(pu)))
  {
      IParamsUnion * pu_copy = pu->clone();
///TODO!!! ������������� ��� : "Overflow" <- ����� ������ ���������� ������� ���������� ������!
      _points.push_back(pu_copy);
      return true;
  }
  return false;
};
/* OLD
int IParamsUnionSet_Impl::setModel(const IAdditiveModel* additiveModel, Logger* log)
{
    if(!_additiveModel)
    {
        log->log(LogLevels::ALGORITHM_1_LEVEL_INFO, "NOT NULL additive model is changed for ParamsUnionSet!");
    }
    _additiveModel = const_cast<IAdditiveModel *> (additiveModel);
    return _RC_SUCCESS_;
};
*/
size_t IParamsUnionSet_Impl::getSetSize() const
{
    return _points.size();
}

int IParamsUnionSet_Impl::getCurrentId() const
{
    return _id;
}

int IParamsUnionSet_Impl::setCurrentId(unsigned id)
{
    if(id < _points.size())
    {
        this->_id = id;
        return _RC_SUCCESS_;
    }
    return _ERROR_WRONG_ARGUMENT_;
}
/**
 NB returns ptr to existing item into Set!
 It does Not copy! => Don't free that ptr!
*/
IParamsUnion* IParamsUnionSet_Impl::getFirstPtr()
{
    if (_points.size() < 1)
        return NULL;

    _id = 0;
    return _points[_id];
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParamsUnion* IParamsUnionSet_Impl::getNextPtr()
{
    ++_id;

    if (_id >= (int)_points.size())
    {
        _id = -1;
        return NULL;
    }
    return _points[_id];
}

IParamsUnion* IParamsUnionSet_Impl::getParams(unsigned id)
{
    if (id < 0 || id >= _points.size()) {
        this->_id = -1;
        return NULL;
    }
    this->_id = id;
    return _points[id];
}

bool IParamsUnionSet_Impl::end() const
{
    return (_id == -1);
}

///TODO!!! WE NEED FACTORY to make ParamsSet!
IParamsSet* IParamsSet::createEmptyParamsSet(const IModel* model, Logger* log)
{
///TODO!!! ������ ������ ����� ��� ����� ����������
    IParamsSet_Impl* pst = new IParamsSet_Impl(model);
///TODO!!! ��������: ����� �������� addParams, �� ����������� ������ ����������� (NULL), ������ ��� ���� ����� ��� ������ � �������������� ������ � IParamsSet!
///TODO!!! � IParamsSet ����� ��������� ������������ �����,
/// � ������� �������� ���������������� ��������� �� ������, ��������� ������� ���� IParamsSet ��������!
/// � �� ��������� ���� ��������� �� ������ � IParamsSet! = NULL
/// ��������� ����� - ��������!: ������ �� ���� IParamsSet->addParams ������� ������������� ������ IParamsSet!!!
/*
    int rc = pst->addParams(model, NULL, log);
    if (!rc)
    {
        delete pst;
        log->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "IParamsSet::createEmptyParamsSet : Failed to create empty Params Set!");
        return NULL;
    }
*/
    return pst;
};
/** TODO!!! � ������ �� ��������� ���� ����������� ������������ ���������� ������� ��� ����� ����������, �������� PLAIN_MODEL? ����� ��������� ������ ������������ IParamsSet �� IEstimator!
IParamsSet* IParamsSet::createEmptyParamsSet(const IEstimator* estimator, Logger* log)
{
///TODO!!! ��������� ������ ����� ��� ����� ����������
    log->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "createEmptyParamsSet is not implemented yet for const IEstimator* estimator!");
    return NULL;
};
*/

IParamsUnionSet* IParamsUnionSet::createEmptyParamsUnionSet(const IAdditiveModel* additiveModel, Logger* logger)
{
    /** ������ ������ ����� ��� ����� ����������
    � ���� �� ����� � ���� ���������� �������
    �������������� ������ (� �� ������ Params1D ������� ����������� ����������),
    ����� � ����������� ����� ���������� ����� �������� ��������� �� ������.
    */
    IParamsUnionSet_Impl* pust = new IParamsUnionSet_Impl (additiveModel);
    if(pust != NULL)
    {
//OLD        pust->setModel(additiveModel, logger);
        return pust;
    }
///TODO!!! ��������: ����� �������� addParams, �� ����������� ������ ����������� (NULL), ������ ��� ���� ����� ��� ������ � �������������� ������ � IParamsSet!
///TODO!!! � IParamsSet ����� ��������� ������������ �����,
/// � ������� �������� ���������������� ��������� �� ������, ��������� ������� ���� IParamsSet ��������!
/// � �� ��������� ���� ��������� �� ������ � IParamsSet! = NULL
/// ��������� ����� - ��������!: ������ �� ���� IParamsSet->addParams ������� ������������� ������ IParamsSet!!!

    return NULL;
};

#ifndef __PARAMS_H__633569422542968750
#define __PARAMS_H__633569422542968750
#include <iostream> /* cout */
#include <math.h> /* NAN */
#include <cstring> /* memcpy */
#include <string> /* std::string */
#include <map> /* std::map */

// DON'T pollute global scope! using namespace std;
/** forward declaration */
class Logger;
/** classes interfaces */

/**
���� ����� ����������� ��������� IParams,
� �� ��� ������ ����� ����������� ������ ������� ���������� ����������,
������� ����� ����������� ������ �������/�����������!
��������� ��� ������� ���������� � ���������� ������ ������������ �� ������ ������,
�������� I_PF_Model.
�� ��������� ���������� ������ ���������� ������� ��������� ��������
*/
class IParams
{
public:
/** enum must be populated to provide factory info,
 which Param type instance is required
*/
    enum ParamsTypes
    {
        Params1D,
        GaussianParams,
        WienerParams,
        PoissonParams,
        ParetoParams,
        PoissonDrvnParetoJumpsExpMeanRevertingParams,
        FAKE_PARAMS
    };
    static const char * paramsNames[IParams::ParamsTypes::FAKE_PARAMS];
    static enum ParamsTypes getParamsTypeByParamsName(const std::string& paramsName);

    /** GRAMMAR stuff for parseAndCreateParams(...)
    ��� ��������� �� ����������� ������ ���� �����
    ����� ���������� ��������������� �� ������!
    �� ����� ���� ������� ����� ���������
    � �����-�� ������ � ���������, � ������� ��� ��������� ������ �������
    */
    enum indexesOfGrammarForParams1D
    {
        indexOfDimension,
        FAKE_INDEX_GRAMMAR_PARAMS1D
    };
    /** tokens to parse map for Params1D */
    static const char * grammar[FAKE_INDEX_GRAMMAR_PARAMS1D];
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    IParams() {};
    virtual ~IParams() {};
    /** Static Factories!
    ��� ������������ ������ ������� ����� ���������
    ������, ������������ ���� �� ����������� � ���� h-�����
    */
    /** enum IParams::ParamsTypes is in use to specify required Param type instance */
    static IParams* createEmptyParams(Logger* logger, enum IParams::ParamsTypes pt, size_t size = 0);
    /** enum IParams::ParamsTypes is in use to specify which parser is required to create Param type instance */
    static IParams* parseAndCreateParams(enum IParams::ParamsTypes pt, std::map<std::string, std::string>& mapParams, std::string postfix, Logger* logger);

    /** Any subclass may have own copy of clone
    DON'T forget to DELETE after all
    the IParams instance the cone() created
    */
    virtual IParams* clone() const =0;
    /** Every subclass MUST have own IMPLEMENTATION of toString()! */
    virtual std::string toString() const =0;

    virtual size_t getSize() const =0;
/// DOES NOT??? MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MUST define operator(i,...) to get access to params by index
    /** ��� ������ ����� ���������� ������
     �� ���������� ���������� ���������� ���������� ������,
     ������� ����� ������������� �� IParams � ��� �����!
     ��� � ���������� ���������� IParams_Impl,
      � �������� setParam/setParams � getParam/getParams,
     � ����� ���������� ���������� ������������� ������� */
    virtual int setParam(size_t index, double value)=0;
    virtual int setParams(const size_t size, const double * params, const size_t offset=0)=0;
    virtual int getParam(size_t index, double& param) const =0;
    virtual int getParams(const size_t size, double * params, const size_t offset=0) const =0;

private:
    /** assign and copy CTORs are disabled */
    IParams(const IParams&);
    void operator=(const IParams&);
};

///TODO!!! class IParams2D
/***************************************************************************************
*                                 Gaussian Params                                      *
****************************************************************************************/
class IParamsGaussian : public virtual IParams
{
public:
    /// GaussianCDFParamsDimension<=2
    static const size_t GaussianCDFParamsDimension;
    /** This public ctor DOES NOT MATTER - this is abstract class !
     To create Gaussian implementation factory is in use!
    */

    /** enum for grammarOfGaussianParams <=2
    */
    enum indexesOfGrammarForGaussianParams{
        MU=0,
        SIGMA=1,
        FAKE_INDEX_GRAMMAR_GAUSSIAN
    };
    /** tokens to parse map for GaussianParams */
    static const char * grammar[FAKE_INDEX_GRAMMAR_GAUSSIAN];

    IParamsGaussian() {};
    virtual ~IParamsGaussian() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//  ???  virtual double operator()(size_t index);
    virtual IParams* clone() const =0;

    virtual std::string toString() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for IParamsGaussian model */
    /** ��� � ������������� ������� ��� ��� � ���������/
     ����� ���������� ������� setParam/setParams � getParam/getParams
     ���� ���������������� ����������� ����� ����������,
     ���� � ���������������� �� ������ ���������� ����� ����������
     ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�. */
    virtual int setMean(double newMean)=0;
    virtual int setSDeviation(double newSD)=0;
    virtual double mu() const=0;
    virtual double sigma() const=0;
private:
    /** public ctor exists BUT Factory is in use instead! */
    /** assign and copy CTORs are disabled */
    IParamsGaussian(const IParamsGaussian&);
    void operator=(const IParamsGaussian&);
};
/******************************************************
*                    Wiener Params                    *
*******************************************************/
class IParamsWiener : public virtual IParams
{
public:
    ///=>3
    static const size_t WienerCDFParamsDimension;

    /** enum for grammarOfWienerParams
    =>3
    */
    enum indexesOfGrammarForWienerParams{
        INITIAL_CONDITION=0,
        MU=1,
        SIGMA=2,
        FAKE_INDEX_GRAMMAR_WIENER
    };
    /** tokens to parse map for WienerParams */
    static const char * grammar[FAKE_INDEX_GRAMMAR_WIENER];
    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsWiener() {};
    virtual ~IParamsWiener() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    virtual std::string toString() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Wiener model */
    /** ��� � ������������� ������� ��� ��� � ���������/
    ����� ���������� ������� setParam/setParams � getParam/getParams
    ���� ���������������� ����������� ����� ����������,
    ���� � ���������������� �� ������ ���������� ����� ����������
    ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    /** ��� ��������� ������ ��������������� �.�. - ����� ��������� ��� � ���.�������
    �.�.WienerCDFParamsDimension = 3;
    */
    virtual int setInitialCondition(double initCondition) = 0;
    virtual int setMean(double newMean)=0;
    virtual int setSDeviation(double newSD)=0;
    virtual double initialCondition() const = 0;
    virtual double mu() const=0;
    virtual double sigma() const=0;
private:
    /** public ctor exists BUT Factory is in use instead! */
    /** assign and copy CTORs are disabled */
    IParamsWiener(const IParamsWiener&);
    void operator=(const IParamsWiener&);
};
/******************************************************
*                    Poisson Params                   *
*******************************************************/
class IParamsPoisson : public virtual IParams
{
public:
    ///=>1
    static const size_t PoissonCDFParamsDimension;
    /** enum for grammarOfPoisonParams
    =>3
    */
    enum indexesOfGrammarForPoissonParams{
        LAMBDA=0,
        FAKE_INDEX_GRAMMAR_POISSON
    };
    /** tokens to parse map for PoissonParams */
//OLD    extern const std::string grammarPoissonParams[grammarSizeForPoissonParams];
    static const char * grammar[FAKE_INDEX_GRAMMAR_POISSON];
    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsPoisson() {};
    virtual ~IParamsPoisson() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense!    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Poisson model */
    /** ��� � ������������� ������� ��� ��� � ���������/
    ����� ���������� ������� setParam/setParams � getParam/getParams
    ���� ���������������� ����������� ����� ����������,
    ���� � ���������������� �� ������ ���������� ����� ����������
    ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    virtual int setPoissonLambda(double PoissonLambda)=0;
    virtual double lambda() const=0;

private:
    IParamsPoisson(const IParamsPoisson&);
    void operator=(const IParamsPoisson&);
};
/*****************************************************
*                    Pareto Params                    *
******************************************************/
class IParamsPareto : public virtual IParams
{
public:
    ///=>2
    static const size_t ParetoCDFParamsDimension;
    /** enum for grammarOfParetoParams
    =>5
    */
    enum indexesOfGrammarForParetoParams{
        X_MIN,
        K,
        K_MIN,
        K_MAX,
        X_MIN_THRESHOLD,
        FAKE_INDEX_GRAMMAR_PARETO
    };
    /** tokens to parse map for ParetoParams */
//OLD    extern const std::string grammarParetoParams[grammarSizeForParetoParams];
    static const char * grammar[FAKE_INDEX_GRAMMAR_PARETO];


    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsPareto() {};
    virtual ~IParamsPareto() {};

    /** IParams stuff */
    using IParams::getSize;
    virtual std::string toString() const =0;
///DOES NOT MAKE sense!    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Pareto model */
    /** ��� � ������������� ������� ��� ��� � ���������/
    ����� ���������� ������� setParam/setParams � getParam/getParams
    ���� ���������������� ����������� ����� ����������,
    ���� � ���������������� �� ������ ���������� ����� ����������
    ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    virtual int setParetoXm(double xM)=0;
    virtual int setParetoK(double k)=0;
    virtual double xM() const=0;
    virtual double k() const=0;

private:
    IParamsPareto(const IParamsPareto&);
    void operator=(const IParamsPareto&);
};

/******************************************************
*                PoissonDrvnParetoJumpsExpMeanReverting Params                 *
******************************************************/
class IParamsPoissonDrvnParetoJumpsExpMeanReverting : public virtual IParams,
    public virtual IParamsPareto
{
public:
    ///=>4
    static const size_t PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension;

    /** enum for grammarOfPoissonDrvnParetoJumpsExpMeanRevertingParams
    =>4
    */
    enum indexesOfGrammarForPoissonDrvnParetoJumpsExpMeanRevertingParams{
        LAMBDA,
        LAM_C,
        X_min,
        K_,
        FAKE_INDEX_GRAMMAR_POISSONDRVN_PARETOJUMPS_EXPMR
    };
    /** tokens to parse map for PoissonDrvnParetoJumpsExpMeanRevertingParams */
//OLD    extern const std::string grammarPoissonDrvnParetoJumpsExpMeanRevertingParams[grammarSizeForPoissonDrvnParetoJumpsExpMeanRevertingParams];
    static const char * grammar[FAKE_INDEX_GRAMMAR_POISSONDRVN_PARETOJUMPS_EXPMR];

    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsPoissonDrvnParetoJumpsExpMeanReverting() {};
    virtual ~IParamsPoissonDrvnParetoJumpsExpMeanReverting() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense!    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Poisson-Pareto model */
    /** ��� � ������������� ������� ��� ��� � ���������/
     ����� ���������� ������� setParam/setParams � getParam/getParams
     ���� ���������������� ����������� ����� ����������,
     ���� � ���������������� �� ������ ���������� ����� ����������
     ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    virtual int setPoissonLambda(double PoissonLambda)=0;
    virtual int setMRLambda(double lambda_c)=0;
    virtual int setParetoXm(double xM)=0;
    virtual int setParetoK(double k)=0;
    virtual double lam() const=0;
    virtual double lamC() const=0;
    virtual double xM() const=0;
    virtual double k() const=0;

private:
    IParamsPoissonDrvnParetoJumpsExpMeanReverting(const IParamsPoissonDrvnParetoJumpsExpMeanReverting&);
    void operator=(const IParamsPoissonDrvnParetoJumpsExpMeanReverting&);
};

/******************************************************
*                     Params Union/Array                    *
*******************************************************/
/** class- interface as container for arrays of params */
/** IParamsUnion DOESN'T INHERIT  : public virtual IParams !
����� ����������� �� ���������� IParams,
����� ����������� setParam/getParam,
setParams/getParams,
�������������� ���������� ���������
��������� � ���� ���������� ������,
 � �������, �� ����������� ������� ������
 ������������ ��� ����� �� ���������� �������
 ���������
*/
class IParamsUnion // array of IParams
{
public:
    /** Factories for ParamsUnion */
    /** here we create ParamsUnion of general purposes Params1D only! */
    /** Logger ����� ������� � ������ ����������,
    �� ����� ���������� � ��� ���� �������, �� ��� �� ������ ������������.
    ������� ����� ������ ��������� Logger* �������� NULL */
    static IParamsUnion* createParamsUnion(const size_t totalOfItems, const size_t* sizesOfItems, Logger * logger);
    /** here we create ParamsUnion by cloning addends ! */
    static IParamsUnion* createParamsUnion(const size_t totalOfItems, const IParams** items, Logger * logger);

    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    IParamsUnion() {};
    virtual ~IParamsUnion() {};
    /** Any subclass may have own copy algorithm */
    virtual IParamsUnion* clone() const =0;
    /** Every subclass MUST have own IMPLEMENTATION for toString()! */
    virtual std::string toString() const =0;

    /**
    ����� �� ���������� ��������
    "items - ���������� ������� ����������"
    */
    virtual int getTotalOfItemsInParamsUnion(size_t& size) const =0;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MIGHT define operator(i,...) to get access to params by index
    virtual int setItem(const size_t index, const IParams* itemParams) =0;
    virtual int setItems(const size_t size, const IParams** itemsParams) =0;
    virtual IParams* ptrIParamsUnionToPtrIParams(const size_t index) const =0;
    virtual int getItemCopy(const size_t index, IParams*& itemParams) const =0;
    virtual int getItemsCopies(const size_t size, IParams**& itemsParams) const =0;

private:
    /** assign and copy CTORs are disabled */
    IParamsUnion(const IParamsUnion&);
    void operator=(const IParamsUnion&);
};

#endif

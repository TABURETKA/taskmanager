#ifndef __PARAM_COMPACT_H__633569422542968750
#define __PARAM_COMPACT_H__633569422542968750

#include <string>
#include <vector>

// DON'T pollute global scope! using namespace std;

#include "Params.h"
#include "../Log/Log.h"

/** forward declarations */
class IModel;
/** Interface declaration here!
    Implementation (private) MOVEd to cpp
*/
/**
Реализация IParamsCompact не имеет собственного контейнера для хранения точек компакта,
потому как мощность этого множества может быть более 10^6!
да их все и не нужно хранить, т.к. любая точка определяется ее id
(сетка с заданным шагом по кажой координате)
НО можно отнаследоваться от IParamsSet
и хранить подмножества компакта
в контейнере IParamsSet (у него есть private вектор points)
*/
class IParamsCompact //пока это не нужно : public virtual IParamsSet
{
public:
    enum indexesOfGrammarForCompact
    {
        indexOfIncrementType, ///STEP_VALUE, TOTAL_STEPS
        indexOfIncrementValue,
        FAKE_INDEX_GRAMMAR
    };

    static const char * grammar[FAKE_INDEX_GRAMMAR];
    /** static factory для создания экземпляров IParamsCompact
    */
    static IParamsCompact* createParamsCompact(const IModel* model,  IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger);

    IParamsCompact() {};
    ~IParamsCompact() {};
    virtual size_t getSize() const =0;
    virtual int getCurrentId() const =0;
    virtual int setCurrentId(const size_t id) =0;
    /**
    экземпляр, доступный по любому из этих ДВУХ УКАЗАТЕЛЕЙ,
    на самом деле лежит внутри компакта самостоятельной копией
    которая уничтожится при вызове любой из этих функций
    т.е. эту копию НЕТ необходимости САМОСТОЯТЕЛЬНО! удалить по завершении алгоритма
    */
    virtual IParams* getFirstPtr() =0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getNextPtr() =0;
    /**
    экземпляр, возвращаемый этой функцией,
    копия, ее необходимо САМОСТОЯТЕЛЬНО! удалить по завершении алгоритма
    */
    virtual IParams* getParamsCopy(const size_t id) const =0;
    virtual bool end() const =0;
};
/** Interface declaration here!
    Implementation (private) MOVEd to cpp
 */
/**
Реализация IParamsUnionCompact не имеет собственного контейнера для хранения точек компакта,
потому как мощность этого множества может быть более 10^6!
да их все и не нужно хранить, т.к. любая точка определяется ее id
(сетка с заданным шагом по кажой координате)
НО можно отнаследоваться от IParamsUnionSet
и  хранить подмножества компакта
в контейнере IParamsUnionSet (у него есть private вектор points)
*/
/** forward declaration */
class IAdditiveModel;

class IParamsUnionCompact //пока не нужно : public virtual IParamsUnionSet
{
public:
    /** static factory для создания экземпляров IParamsUnionCompact
    */
    static IParamsUnionCompact* createParamsUnionCompact(const IAdditiveModel* additiveModel,  IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool stepValue, Logger* logger);

    IParamsUnionCompact() {};
    ~IParamsUnionCompact() {};

    virtual size_t getSize() const =0;
    virtual int getCurrentId() const =0;
    virtual int setCurrentId(const size_t id) =0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getFirstPtr() =0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getNextPtr() =0;
    virtual IParamsUnion* getParamsUnionCopy(const size_t id) const =0;
    virtual bool end() const =0;
};

#endif

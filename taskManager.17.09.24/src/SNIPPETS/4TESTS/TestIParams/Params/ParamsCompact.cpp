#include "../CommonStuff/Defines.h" /* isNAN_double() */
#include "ParamsCompact.h"
#include "Params.h"
#include "../Models/Model.h"//IModel
#include "../Models/AdditiveModel.h"//IAdditiveModel
#include "../Log/errors.h" //rc codes
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include <float.h> /* DBL_MIN */
/**************************************
* Params Compact class implementation *
***************************************/
///static
const char * IParamsCompact::grammar[FAKE_INDEX_GRAMMAR]=
{
    "IncrementType", ///STEP_VALUE, TOTAL_STEPS
    "IncrementValue"
};

namespace
{
/**
    IParamsCompact Implementation (private) MOVE here
    Компакт из точек IParams
*/
/** в этом контейнере не хранятся сами точки из компакта в параметрическом пространстве,
    здесь лежат только границы компакта, шаг, с которым строится сетка!
*/
class IParamsCompact_Impl : //этого пока не надо public virtual IParamsSet_Impl,
    public virtual IParamsCompact
{
public:
///TODO!!! сделать const IModel* _model, тем самым избавиться от setModel!
/// т.е. инициализировать это поле сразу же, в CTOR'е
/// и далее в течение времени жизни уже не менять
    IParamsCompact_Impl(IModel* model);
    ~IParamsCompact_Impl();
    /**
    здесь, в этих методах,
    мы гарантируем, что в контейнер будут добавлены параметры от единственной и конкретной модели
    */
    /** both must be public because IParamsCompact::createParamsCompact()
    calls them
    */
    static bool areValidCTORparams(const IModel* model, IParams* leftBound, IParams* rightBound, IParams* increments, bool absStep, Logger* logger);
    virtual bool fillParamsCompact(const IModel* model,  IParams* leftBound, IParams* rightBound, IParams* increments, bool absStep, Logger* logger);

    virtual size_t getSize() const;
    virtual int getCurrentId() const;
    virtual int setCurrentId(const size_t id);

    /** в этом контейнере сами параметры не хранятся,
    здесь лежат только границы компакта, шаг, с которым строится сетка!
    Поэтому следующие три метода по сути перезаполняют currentParams
    на каждом вызове.
    */
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getNextPtr();
    virtual IParams* getParamsCopy(const size_t id) const;

    virtual bool end() const;

private:
    /** dimension of each IParams :
    startParams, endParams, increment;
     */
    unsigned _paramsSpaceDimension;
    IParams* _startParams;
    IParams* _endParams;
    IParams* _increment;

    /** vector of size _paramsSpaceDimension
    to keep total of points along each coordinate axis
    */
///TODO!!! зачем нам здесь вектор? можно обойтись массивом!
    std::vector<unsigned> _pointNum;
    /** ptr to current IParams in this Compact */
    IParams* _currentParams;
    /** id of current IParams in this Compact */
    int _id;
    /** vector of size _paramsSpaceDimension
    to keep ids of current IParams along each coordinate axis
    in this Compact */
///TODO!!! зачем нам здесь вектор? можно обойтись массивом!
    std::vector<unsigned> _coordsOfCurrentParamsInCompact;
    IModel* _model;
};

/**
   IParamsUnionCompact Implementation (private) MOVE here
*/
/** в этом контейнере не хранятся сами точки из компакта в параметрическом пространстве,
    здесь лежат только границы компакта, шаг, с которым строится сетка!
*/
class IParamsUnionCompact_Impl //это пока не нужно : public virtual IParamsUnionSet_Impl,
    : public virtual IParamsUnionCompact
{
public:
///TODO!!! сделать const IAdditiveModel* _additiveModel, тем самым избавиться от setModel!
/// т.е. инициализировать это поле сразу же, в CTOR'е
/// и далее в течение времени жизни уже не менять
    IParamsUnionCompact_Impl(IAdditiveModel* additiveModel);
    ~IParamsUnionCompact_Impl();

    /**
    здесь, в этих методах,
    мы гарантируем, что в контейнер будут добавлены параметры от единственной и конкретной модели
    */
    /** both must be public because IParamsCompact::createParamsUnionCompact()
    calls them
    */
    static bool areValidCTORparams(const IAdditiveModel* additiveModel, IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool absStep, Logger* logger);
    virtual bool fillParamsUnionCompact(const IAdditiveModel* additiveModel,  IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool absStep, Logger* logger);

    virtual size_t getSize() const;
    virtual int getCurrentId() const;
    virtual int setCurrentId(const size_t id);
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    IParamsUnion* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    IParamsUnion* getNextPtr();
    IParamsUnion* getParamsUnionCopy(const size_t id) const;
    bool end() const;

private:
    /** total of IParams into each IParamsUnion
    */
    size_t _totalOfItemsInParamsUnion;
    /** dimension of compact as result of
    IParamsUnion's items concatenation
    OR
    dimention of ech IParamsUnion
    startParams, endParams, increment;
    */
    size_t _paramsSpaceDimension;
    /** dimentionS of items in IParamsUnion
    startParams, endParams, increment;
    the array of size _totalOfItemsInParamsUnion
    */
    size_t* _paramsUnionItemsDimensions;
    IParamsUnion* _startParamsUnion;
    IParamsUnion* _endParamsUnion;
    IParamsUnion* _increment;
    /** vector of size _paramsSpaceDimension
    to keep total of points along each coordinate axis  */
///TODO!!! зачем нам здесь вектор? можно обойтись массивом!
    std::vector<unsigned> _pointNum;
    /** ptr to current ARRAY IParamsUnion in this Compact */
    IParamsUnion* _currentParamsUnion;
    /** id of current ARRAY IParamsUnion in this Compact */
    int _id;
    /** vector of size _paramsSpaceDimension
    to keep ids of current IParamsUnion along each coordinate axis
    in this Compact */
///TODO!!! зачем нам здесь вектор? можно обойтись массивом!
    std::vector<unsigned> _coordsOfCurrentParamsUnionInCompact;
    IAdditiveModel* _additiveModel;
};

}; ///end of anonymous namespace
/***********************************************
*                 IMPLEMENTATION
************************************************/

/***************************************************
              IParamsCompact_Impl
****************************************************/
/************************************************
*                     CTOR                      *
************************************************/
IParamsCompact_Impl::IParamsCompact_Impl(IModel* model) : _model(model)
{
    _startParams = NULL;
    _endParams = NULL;
    _increment = NULL;
    _paramsSpaceDimension =0;
    _currentParams = NULL;
    _id = -1;
}
/************************************************
*                     DTOR                      *
************************************************/
IParamsCompact_Impl::~IParamsCompact_Impl()
{
    delete _startParams;
    delete _endParams;
    delete _increment;
    delete _currentParams;
    _coordsOfCurrentParamsInCompact.clear();
    _pointNum.clear();
}

/*************************************************
* static in IParamsCompact_Impl
* because IParamsUnionCompact_Impl uses it ad well
**************************************************/
bool IParamsCompact_Impl::areValidCTORparams(const IModel* model, IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger)
{
    if(model == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Model NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(leftBound == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(rightBound == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(increments == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's increments NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    bool isParamsFitModel = model->canCastModelParams(leftBound);
    if(isParamsFitModel == false)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT MATCH model in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    isParamsFitModel = model->canCastModelParams(rightBound);
    if(isParamsFitModel == false)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT MATCH model in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    isParamsFitModel = model->canCastModelParams(increments);
    if(isParamsFitModel == false)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's increments NOT MATCH model in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    return true;
}
/***********************************************
*
************************************************/
bool IParamsCompact_Impl::fillParamsCompact(const IModel* model, IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger)
{
    bool bRC = true;
    if (_model != NULL)
    {
        bRC = IModel::compareModels(_model, model);
        if (! bRC)
        {
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong model type in ParamsCompact::fillParams()!");
            return false;
        }
    }
    /*
        else
        {
            /** This Cheating happens once for all ParamsCompact life-time !
            /
            _model = const_cast<IModel *> (model);
        }
    */
        /** first validate params */
    if(false == IParamsCompact_Impl::areValidCTORparams(model, leftBound, rightBound, increments, stepValue, logger))
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong params in IParamsCompact::createParamsCompact()!");
        return NULL;
    }
    _startParams   = leftBound->clone();
    _endParams     = rightBound->clone();
    _increment     = increments->clone();
    _currentParams = _startParams->clone();
    _id = -1;

    _paramsSpaceDimension = _startParams->getSize();

    _pointNum.resize(_paramsSpaceDimension,0);
    _coordsOfCurrentParamsInCompact.resize(_paramsSpaceDimension,0);

    for (unsigned i = 0; i < _paramsSpaceDimension; ++i)
    {
        double startParamsElement =NAN;
        double endParamsElement   =NAN;
        int rc = _startParams->getParam(i, startParamsElement);
        rc = _endParams->getParam(i, endParamsElement);

        if (startParamsElement > endParamsElement)
        {
            rc = _startParams->setParam(i, endParamsElement);
            rc = _endParams->setParam(i, startParamsElement);
        }
        if (startParamsElement == endParamsElement)
        {
            _pointNum[i] = 1;
            continue;
        }
        double incrementParamsElement =NAN;
        rc = _increment->getParam(i, incrementParamsElement);
        /** increment is stepValue */
        if (stepValue)
        {
            double parts = ceil((endParamsElement - startParamsElement) / incrementParamsElement) + 1;
            _pointNum[i] = (unsigned)parts;
        }
        else /** increment is totalSteps */
        {
            double pointsNum = incrementParamsElement;
            _pointNum[i] = (unsigned)pointsNum;

            incrementParamsElement = pointsNum > 1 ? (endParamsElement - startParamsElement) / (pointsNum - 1) : incrementParamsElement;
            rc = _increment->setParam(i, incrementParamsElement);
        }

        std::stringstream logStr;
        logStr << "Fill compact for param " << i << std::endl <<
        "Begin: " << startParamsElement << std::endl <<
        "End: " << endParamsElement << std::endl <<
        "Increment: " << incrementParamsElement << std::endl <<
        "Num of points: " << _pointNum[i];
        logger->log(ALGORITHM_GENERAL_INFO, logStr.str());
    }
    size_t size = this->getSize();
    std::stringstream logStr;
    logStr << "Total size of compact: " << size;
    logger->log(ALGORITHM_GENERAL_INFO, logStr.str());

    return true;
}
/**********************************************
*            Get total of points in Compact
***********************************************/
size_t IParamsCompact_Impl::getSize() const
{
    unsigned sizeOfSet = 1;
    for (unsigned i = 0; i < _paramsSpaceDimension; ++i)
    {
        sizeOfSet *= _pointNum[i];
    }

    return sizeOfSet;
}

int IParamsCompact_Impl::getCurrentId() const
{
    return _id;
}

int IParamsCompact_Impl::setCurrentId(const size_t id)
{
    size_t size = this->getSize();
    if(size == 0)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if(id >= size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    size_t tmpId = id;
    for (unsigned i = 0; i < _paramsSpaceDimension; ++i)
    {
        _coordsOfCurrentParamsInCompact[i] = tmpId % _pointNum[i];
        tmpId /= _pointNum[i];
        double currentParamsElement   =NAN;
        int rc = _startParams->getParam(i, currentParamsElement);
        if(rc)
        {
///TODO!!! очень плохо, побочный эффект _id = -1;
            return _ERROR_WRONG_ARGUMENT_;
        };
        _currentParams->setParam(i, currentParamsElement);
    }
    this->_id = id;
    return _RC_SUCCESS_;
}

IParams* IParamsCompact_Impl::getFirstPtr()
{
    _id = 0;
    for (unsigned i = 0; i < _paramsSpaceDimension; ++i)
    {
/// клонируем вместо (*currentParams)[i] = (*startParams)[i];
        double currentParamsElement   =NAN;
        int rc = _startParams->getParam(i, currentParamsElement);
        if(rc)
        {
            _id = -1;
            return NULL;
        };
        _currentParams->setParam(i, currentParamsElement);
        _coordsOfCurrentParamsInCompact[i] = 0;
    }
    return _currentParams;
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
///TODO!!! проверить этот алгоритм!
IParams* IParamsCompact_Impl::getNextPtr()
{
    ++_id;
    for (unsigned i = 0; i < _paramsSpaceDimension; ++i)
    {
        /**
        Двигаясь в цикле последовательно вдоль
        каждой из координатных осей параметрического пространства
        размерности _paramsDimension,
        мы ищем первую координату, по которой
        еще не уперлись в правую границу компакта по этой координате.
        Обнаружив такую координатную ось,
        мы инкрементируем смещение вдоль нее и
        достаем в incrementParamsElement значение по этой координате,
        пишем его в вектор параметров _currentParams
        по индексу, соответствующему этой координатной оси.
        Возвращаем затребованный вектор параметров.
        */
        if ((int)_coordsOfCurrentParamsInCompact[i] < (int)_pointNum[i] - 2)
        {
            _coordsOfCurrentParamsInCompact[i]++;
            double incrementParamsElement   =NAN;
            /** здесь вынули ТОЛЬКО инкремент!
            */
            int rc = _increment->getParam(i, incrementParamsElement);
            if(rc)
            {
///TODO!!! очень плохо, побочный эффект  _id = -1;
                return NULL;
            }
///BUG БЫЛ нужно _currentParams[i] +=incrementParamsElement!
            double currentParamsElement   =NAN;
            _currentParams->getParam(i, currentParamsElement);
            currentParamsElement += incrementParamsElement;
            _currentParams->setParam(i, currentParamsElement);
            return _currentParams;
        }
        /** случай, когда должны получить последний элемент компакта */
        if (_coordsOfCurrentParamsInCompact[i] == _pointNum[i] - 2)
        {
            _coordsOfCurrentParamsInCompact[i]++;
            double endParamsElement   =NAN;
            int rc = _endParams->getParam(i, endParamsElement);
            if(rc)
            {
///TODO!!! очень плохо, побочный эффект
                _id = -1;
                return NULL;
            }
            _currentParams->setParam(i, endParamsElement);
            return _currentParams;
        }
        /** пошли по кругу! */
        if (_coordsOfCurrentParamsInCompact[i] == _pointNum[i] - 1)
        {
            _coordsOfCurrentParamsInCompact[i] = 0;
            double startParamsElement   =NAN;
            int rc = _startParams->getParam(i, startParamsElement);
            if(rc)
            {
///TODO!!! очень плохо, побочный эффект
                _id = -1;
                return NULL;
            }
            _currentParams->setParam(i, startParamsElement);
/// NO WAY! return _currentParams; <- иначе сломаем обход компакта!
        }
    }
    _id = -1;
    return NULL;
}

/**
 The IParams returned by this method
 MUST BE DELETED after all!
*/
///TODO!!! по сути мы здесь дублируем код getNextPtr(), НО НЕ ИНКРЕМЕНТИРУЯ _id
///дак может завернуть инкремент/декремент _id, вместе с инкремент/декремент элементов vecId, в отдельную функцию, и дергать уже ее в зависимости от контекста
/// либо как getNextPtr(), либо как getParamsCopy()
/** ЗДЕСЬ НЕЛЬЗЯ КЛОНИРОВАТЬ IParams с затребованным id, т.к физически в компакте его точки не храняться! */
IParams* IParamsCompact_Impl::getParamsCopy(const size_t id) const
{
    size_t size = this->getSize();
    if(size == 0)
    {
        return NULL;
    }

    if (id < 0 || id >= size)
    {
///TODO!!! зачем побочный эффект? id = -1;
        return NULL;
    }
///TODO!!! РЕАЛИЗОВАНА ОЧЕНЬ ПЛОХАЯ ИДЕЯ! ПОБОЧНЫЙ ЭФфект : Вот здесь был декремент потому, что хотели выкрутиться с получением искомой точки из компакта путем установки текущего _id в компакте, равного значению предшествующего затребованному
    size_t tmpId = id;
    /**
    Here we pass NULL instead of Logger* because Params1D type already implemented
    So, NO ERROR expected.
    */
    IParams * ptrIParams = IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, _paramsSpaceDimension);
    if(ptrIParams == NULL)
    {
        return NULL;
    }
    for (unsigned i = 0; i < _paramsSpaceDimension; ++i)
    {
        size_t iCoord = tmpId % _pointNum[i];
        tmpId /= _pointNum[i];
        /**
        Двигаясь в цикле последовательно вдоль
        каждой из координатных осей параметрического пространства
        размерности _paramsDimension,
        мы ищем первую координату, по которой
        еще не уперлись в правую границу компакта по этой координате.
        Обнаружив такую координатную ось,
        мы инкрементируем смещение вдоль нее и
        достаем в incrementParamsElement значение по этой координате,
        пишем его в вектор параметров _currentParams
        по индексу, соответствующему этой координатной оси.
        Возвращаем затребованный вектор параметров.
        */
        if ((int)iCoord < (int)_pointNum[i] - 1)
        {
            double currentParamsElement   =NAN;
            double incrementParamsElement   =NAN;
            /** здесь вынули ТОЛЬКО ИНКРЕМЕНТ! */
            int rc = _increment->getParam(i, incrementParamsElement);
            if(rc)
            {
                delete ptrIParams;
                return NULL;
            }
            /** вынем начальную координату вдоль этой i-й оси */
            double startParamsElement   =NAN;
            _startParams->getParam(i, startParamsElement);
            /**  а теперь умножим ее на _vecId[i] и прибавим к начальной координате */
            currentParamsElement = startParamsElement + (iCoord * incrementParamsElement);
            ptrIParams->setParam(i, currentParamsElement);
        }
        /** случай, когда должны получить последний элемент компакта */
        if ((int)iCoord == _pointNum[i] - 1)
        {
            double endParamsElement   =NAN;
            int rc = _endParams->getParam(i, endParamsElement);
            if(rc)
            {
                delete ptrIParams;
                return NULL;
            }
            ptrIParams->setParam(i, endParamsElement);
        }
        /** пошли по кругу! */
        if ((int)iCoord == 0)
        {
            double startParamsElement   =NAN;
            int rc = _startParams->getParam(i, startParamsElement);
            if(rc)
            {
                delete ptrIParams;
                return NULL;
            }
            ptrIParams->setParam(i, startParamsElement);
        }
    }
    return ptrIParams;
}

bool IParamsCompact_Impl::end() const
{
    return (_id == -1);
}

/***************************************************
              IParamsUnionCompact_Impl
****************************************************/
/***************************************************
*                    CTOR
***************************************************/
IParamsUnionCompact_Impl::IParamsUnionCompact_Impl(IAdditiveModel* additiveModel) : _additiveModel(additiveModel)
{
    /** total of IParams into each IParamsUnion
    */
    _totalOfItemsInParamsUnion=0;
    /** dimension of compact as result of
    IParamsUnion's items concatenation
    OR
    dimention of ech IParamsUnion
    startParams, endParams, increment;
    */
    _paramsSpaceDimension = 0;
    /** dimentions of items in IParamsUnion
    startParams, endParams, increment;
    the array of size _totalOfItemsInParamsUnion
    */
    _paramsUnionItemsDimensions=NULL;
    _startParamsUnion = NULL;
    _endParamsUnion = NULL;
    _increment = NULL;
    /** ptr to current ARRAY IParamsUnion in this Compact */
    _currentParamsUnion = NULL;
    /** id of current ARRAY IParamsUnion in this Compact */
    _id = -1;
};
/***************************************************
*                    DTOR
***************************************************/
IParamsUnionCompact_Impl::~IParamsUnionCompact_Impl()
{
    delete [] _paramsUnionItemsDimensions;
    delete _startParamsUnion;
    delete _endParamsUnion;
    delete _increment;
    /** ptr to current ARRAY IParamsUnion in this Compact */
    delete _currentParamsUnion;
    _pointNum.clear();
    _coordsOfCurrentParamsUnionInCompact.clear();
///TODO???    delete _additiveModel;
};
/***************************************************
*    static in IParamsUnionCompact_Impl
***************************************************/
bool IParamsUnionCompact_Impl::areValidCTORparams(const IAdditiveModel* additiveModel, IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool absStep, Logger* logger)
{
    if(additiveModel == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Additive Model NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    size_t totalOfModelAddends = 0;
    additiveModel->getTotalOfAddends(totalOfModelAddends);
    if(leftBound == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    size_t totalOfItems=0;
    leftBound->getTotalOfItemsInParamsUnion(totalOfItems);
    if(totalOfModelAddends != totalOfItems)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact leftBound's dim NOT EQUAL total of model addends in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(rightBound == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    totalOfItems=0;
    rightBound->getTotalOfItemsInParamsUnion(totalOfItems);
    if(totalOfModelAddends != totalOfItems)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact rightBound's dim NOT EQUAL total of model addends in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }

    if(increments == NULL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's increments NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    totalOfItems=0;
    increments->getTotalOfItemsInParamsUnion(totalOfItems);
    if(totalOfModelAddends != totalOfItems)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact increments' dim NOT EQUAL total of model addends in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    else
    {
        for(size_t i=0; i < totalOfItems; ++i)
        {
            size_t sizeLBitem = leftBound->ptrIParamsUnionToPtrIParams(i)->getSize();
            size_t sizeIncrItem = increments->ptrIParamsUnionToPtrIParams(i)->getSize();
            if(sizeLBitem != sizeIncrItem)
            {
                std::stringstream ss;
                ss << "Compact leftBound's " << i << "-th item size NOT MATCH the size of the same item of LeftBoundary in IParamsUnionCompact_Impl::areValidCTORparams()!";
                logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, ss.str());
                return false;
            }
        }
    }
    /** try to cast leftBound */
    bool isParamsFitModel = additiveModel->canCastModelParamsUnion(leftBound);
    if(isParamsFitModel == false)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT MATCH model in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    /** try to cast rightBound */
    isParamsFitModel = additiveModel->canCastModelParamsUnion(rightBound);
    if(isParamsFitModel == false)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT MATCH model in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    /** DON't validate increments with
    additiveModel->canCastModelParamsUnion(increments);
    increments has been created as Union of Params1D!
    None Model's Params was in use!
    */
    return true;
};
/*************************************************
*
*************************************************/
bool IParamsUnionCompact_Impl::fillParamsUnionCompact(const IAdditiveModel* additiveModel, IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool stepValue, Logger* logger)
{
    /** total of IParams into each IParamsUnion
    */
    additiveModel->getTotalOfAddends(_totalOfItemsInParamsUnion);
    /** dimentionS of items in IParamsUnion
    startParams, endParams, increment;
    the array of size _totalOfItemsInParamsUnion
    */
    _paramsUnionItemsDimensions = new size_t[_totalOfItemsInParamsUnion];
    if(_paramsUnionItemsDimensions == NULL)
    {
        _totalOfItemsInParamsUnion=0;
        logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "NO ROOM in IParamsUnionCompact_Impl::fillParamsUnionCompact!");
        return false;
    }
    for(size_t i=0; i < _totalOfItemsInParamsUnion; ++i)
    {
        IParams * ptrIParams = leftBound->ptrIParamsUnionToPtrIParams(i);
        if(ptrIParams == NULL)
        {
            _totalOfItemsInParamsUnion=0;
            delete [] _paramsUnionItemsDimensions;
            _paramsUnionItemsDimensions = NULL;
            logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "In leftBound the IParams item NOT FOUND in  IParamsUnionCompact_Impl::fillParamsUnionCompact.");
            return false;
        }
        _paramsUnionItemsDimensions[i] = ptrIParams->getSize();
        /** dimension of compact as result of
        IParamsUnion's items concatenation
        OR
        dimention of ech IParamsUnion
        startParams, endParams, increment;
        */
        _paramsSpaceDimension += _paramsUnionItemsDimensions[i];
    }
    _startParamsUnion = leftBound->clone();
    _endParamsUnion   = rightBound->clone();
    _increment = increments->clone();
    /** ptr to current ARRAY IParamsUnion in this Compact */
    _currentParamsUnion = _startParamsUnion->clone();
    /** id of current ARRAY IParamsUnion in this Compact */
    _id = 0;
    _additiveModel = const_cast<IAdditiveModel*> (additiveModel);

    _pointNum.resize(_paramsSpaceDimension,0);
    _coordsOfCurrentParamsUnionInCompact.resize(_paramsSpaceDimension, 0);

    for(size_t i=0, index=0; (i < _totalOfItemsInParamsUnion) && (index < _paramsSpaceDimension); ++i)
    {
        IParams * ptrStartParams = _startParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrEndParams   = _endParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrIncrement   = _increment->ptrIParamsUnionToPtrIParams(i);
        for(size_t j=0; j < _paramsUnionItemsDimensions[i]; ++j)
        {
            double startParamsElement =NAN;
            double endParamsElement   =NAN;
            int rc = ptrStartParams->getParam(j, startParamsElement);
            rc = ptrEndParams->getParam(j, endParamsElement);

            if (startParamsElement > endParamsElement)
            {
                rc = ptrStartParams->setParam(j, endParamsElement);
                rc = ptrEndParams->setParam(j, startParamsElement);
            }
            /** if startParamsElement == endParamsElement */
            if (DBL_MIN > fabs(startParamsElement - endParamsElement))
            {
                _pointNum[index] = 1;
            }
            else if((isNAN_double(startParamsElement)) || (isNAN_double(endParamsElement)))
            {
                if((isNAN_double(startParamsElement)) && (!isNAN_double(endParamsElement)))
                {
                    rc = ptrStartParams->setParam(j, endParamsElement);
                }
                if((!isNAN_double(startParamsElement)) && (isNAN_double(endParamsElement)))
                {
                    rc = ptrEndParams->setParam(j, startParamsElement);
                }
                else
                {
                    rc = ptrStartParams->setParam(j, DEFAULT_VALUE_TO_REPLACE_NAN_PARAM);
                    rc = ptrEndParams->setParam(j,   DEFAULT_VALUE_TO_REPLACE_NAN_PARAM);
                    startParamsElement = endParamsElement = DEFAULT_VALUE_TO_REPLACE_NAN_PARAM;
                    _pointNum[index] = 1;
                }
            }
            double incrementParamsElement = NAN;
            rc = ptrIncrement->getParam(j, incrementParamsElement);

            /** if startParamsElement == endParamsElement */
            if(_pointNum[index] != 1)
            {
                /** increment is stepValue */
                if (stepValue)
                {
                    double parts = ceil((endParamsElement - startParamsElement) / incrementParamsElement) + 1;
                    _pointNum[index] = (unsigned)parts;
                }
                else /** increment is totalOfSteps */
                {
                    double pointsNum = incrementParamsElement;
                    _pointNum[index] = (unsigned)pointsNum;

                    incrementParamsElement = pointsNum > 1 ? (endParamsElement - startParamsElement) / (pointsNum - 1) : incrementParamsElement;
                    rc = ptrIncrement->setParam(j, incrementParamsElement);
                }
            }


            std::stringstream logStr;
            logStr << "Fill compact for param " << index << std::endl <<
            "Begin: " << startParamsElement << std::endl <<
            "End: " << endParamsElement << std::endl <<
            "Increment: " << incrementParamsElement << std::endl <<
            "Num of points: " << _pointNum[index];
            logger->log(ALGORITHM_GENERAL_INFO, logStr.str());

            /** get to the next item in _pointNum[] */
            ++index;
        }///end for-loop over all items of single item in ParamsUnion
    }///end for-loop over all items in ParamsUnion
    size_t size = this->getSize();
    std::stringstream logStr;
    logStr << "Total size of compact: " << size;
    logger->log(ALGORITHM_GENERAL_INFO, logStr.str());

    return true;

};
/**********************************************
*  Get total of points in ParamsUnion Compact
***********************************************/
size_t IParamsUnionCompact_Impl::getSize() const
{
    unsigned sizeOfSet = 1;
    for (unsigned i = 0; i < _paramsSpaceDimension; ++i)
    {
        sizeOfSet *= _pointNum[i];
    }
    return sizeOfSet;
};
/**********************************************
*  Get Id of current point in ParamsUnion Compact
***********************************************/
int IParamsUnionCompact_Impl::getCurrentId() const
{
    return _id;
};
/******************************************************************
*  setCurrentId(const size_t id)
 changes inner _id and _currentParamsUnion into ParamsUnion Compact
*******************************************************************/
int IParamsUnionCompact_Impl::setCurrentId(const size_t id)
{
    size_t size = this->getSize();
    if(size == 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }

    if (id < 0 || id >= size)
    {
        return NULL;
    }
    size_t tmpId = id;
    /**
    Here we pass NULL instead of Logger* because that pointer is ignored into the code of IParamsUnion::createParamsUnion()
    So, NO ERROR expected.
    */
    /** Сначала проинициализируем временный IParamsUnion !
    А уже в самом конце, если все ОК, то перенесем из него даблы в _currentParamsUnion
    и удалим временный ptrIParamsUnion!
    */
    IParamsUnion * ptrTmpIParamsUnion = _startParamsUnion->clone(); // BAD! IParamsUnion::createParamsUnion( _totalOfItemsInParamsUnion, (const size_t*) _paramsUnionItemsDimensions, NULL);
    if(ptrTmpIParamsUnion == NULL)
    {
        return NULL;
    }
    /** NB : We INCREMENT index into the innermost for-loop */
    for(size_t i=0, index=0; (i < _totalOfItemsInParamsUnion) && (index < _paramsSpaceDimension); ++i)
    {
        IParams * ptrStartParams         = _startParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrEndParams           = _endParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrIncrement           = _increment->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrTmpIParamsUnionItem = ptrTmpIParamsUnion->ptrIParamsUnionToPtrIParams(i);
        for(size_t j=0; j < _paramsUnionItemsDimensions[i]; ++j)
        {
            _coordsOfCurrentParamsUnionInCompact[index] = tmpId % _pointNum[index];
            tmpId /= _pointNum[index];
            /**
            Двигаясь в цикле последовательно вдоль
            каждой из координатных осей параметрического пространства
            размерности _paramsSpaceDimension,
            мы ищем первую координату, по которой
            еще не уперлись в правую границу компакта по этой координате.
            Обнаружив такую координатную ось,
            мы инкрементируем смещение вдоль нее и
            достаем в incrementParamsElement значение по этой координате,
            пишем его в вектор параметров _currentParams
            по индексу, соответствующему этой координатной оси.
            Вынимаем очередную координату и пишем ее в соответствующее место затребованного ParamsUnion.
            */

            if ((int)_coordsOfCurrentParamsUnionInCompact[index] < (int)_pointNum[index] - 1)
            {
                double startParamsElement       =NAN;
                double incrementParamsElement   =NAN;
                /** здесь вынули ТОЛЬКО инкремент!
                */
                int rc = ptrIncrement->getParam(j, incrementParamsElement);
                if(rc)
                {
                    delete ptrTmpIParamsUnion;
                    return _ERROR_WRONG_MODEL_PARAMS_SET_;
                }
///BUG БЫЛ нужно _currentParams[i] +=incrementParamsElement!
                ptrStartParams->getParam(j, startParamsElement);
                double currentParamsElement   =NAN;
                currentParamsElement = startParamsElement + (_coordsOfCurrentParamsUnionInCompact[index] * incrementParamsElement);
                ptrTmpIParamsUnionItem->setParam(j, currentParamsElement);
            }
            /** случай, когда должны получить последний элемент компакта на index-ой кооридинатной оси
            */
            if ((int)_coordsOfCurrentParamsUnionInCompact[index] == (int)_pointNum[index] - 1)
            {
                double endParamsElement   =NAN;
                int rc = ptrEndParams->getParam(j, endParamsElement);
                if(rc)
                {
                    delete ptrTmpIParamsUnion;
                    return _ERROR_WRONG_MODEL_PARAMS_SET_;
                }
                ptrTmpIParamsUnionItem->setParam(j, endParamsElement);
            }
            /** случай, когда должны получить последний элемент компакта на index-ой кооридинатной оси
            */
            if ((int)_coordsOfCurrentParamsUnionInCompact[index] == 0)
            {
                double startParamsElement   = NAN;
                int rc = ptrStartParams->getParam(j, startParamsElement);
                if(rc)
                {
                    delete ptrTmpIParamsUnion;
                    return _ERROR_WRONG_MODEL_PARAMS_SET_;
                }
                ptrTmpIParamsUnionItem->setParam(j, startParamsElement);
            }
            ++index;
        }///end for loop over i-th item of ParamsUnion
    }///end for all items/all params coords
    /** если успешно создали временную копию для нового текущего ParamsUnion
    То аккуратно перенесем (чтобы не фрагментировать память) все параметры из временной копии в текущую
    */
    /** NB : We INCREMENT index into the innermost for-loop */
    for(size_t i=0, index=0; (i < _totalOfItemsInParamsUnion) && (index < _paramsSpaceDimension); ++i)
    {
        IParams * ptrTmpCurrParams      = ptrTmpIParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrCurrParams         = _currentParamsUnion->ptrIParamsUnionToPtrIParams(i);
        for(size_t j=0; j < _paramsUnionItemsDimensions[i]; ++j)
        {
            double paramsElement   = NAN;
            ptrTmpCurrParams->getParam(j, paramsElement);
            ptrCurrParams->setParam(j, paramsElement);
            ++index;
        }
    }
    delete ptrTmpIParamsUnion;
    _id = id;
    return _RC_SUCCESS_;
};

/***************************************************
* getFirstPtr() mimic iterator over ParamsUnion Compact
***************************************************/
IParamsUnion* IParamsUnionCompact_Impl::getFirstPtr()
{
    _id = 0;
    /** не хочется здесь дергать delete в комбинации с clone,
    чтобы лишний раз не порождать дефрагментацию памяти
    */
    for(size_t i=0, index=0; (i < _totalOfItemsInParamsUnion) && (index < _paramsSpaceDimension); ++i)
    {
        IParams * ptrStartParams     = _startParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrCurrentParams   = _currentParamsUnion->ptrIParamsUnionToPtrIParams(i);

        for(size_t j=0; j < _paramsUnionItemsDimensions[i]; ++j)
        {
            double startParamsElement =NAN;
            int rc = ptrStartParams->getParam(j, startParamsElement);
            rc = ptrCurrentParams->setParam(j, startParamsElement);
            _coordsOfCurrentParamsUnionInCompact[index] = 0;
            /** get to the next item in _pointNum[] */
            ++index;
        }///end for-loop over all items of single item in ParamsUnion
    }///end for-loop over all items in ParamsUnion
    return _currentParamsUnion;
};
/****************************************************
*  getNextPtr() mimic iterator over ParamsUnion Compact
****************************************************/
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParamsUnion* IParamsUnionCompact_Impl::getNextPtr()
{
    ++_id;
    /** не хочется здесь дергать delete в комбинации с clone,
    чтобы лишний раз не порождать дефрагментацию памяти
    */
    /** NB : We INCREMENT index into the innermost for-loop */
    for(size_t i=0, index=0; (i < _totalOfItemsInParamsUnion) && (index < _paramsSpaceDimension); ++i)
    {
        IParams * ptrStartParams = _startParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrEndParams   = _endParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrIncrement   = _increment->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrCurrParams  = _currentParamsUnion->ptrIParamsUnionToPtrIParams(i);
        for(size_t j=0; j < _paramsUnionItemsDimensions[i]; ++j)
        {
            /**
            Двигаясь в цикле последовательно вдоль
            каждой из координатных осей параметрического пространства
            размерности _paramsSpaceDimension,
            мы ищем первую координату, по которой
            еще не уперлись в правую границу компакта по этой координате.
            Обнаружив такую координатную ось,
            мы инкрементируем смещение вдоль нее и
            достаем в incrementParamsElement значение по этой координате,
            пишем его в вектор параметров _currentParams
            по индексу, соответствующему этой координатной оси.
            Возвращаем затребованный вектор параметров.
            */
            if ((int)_coordsOfCurrentParamsUnionInCompact[index] < (int)_pointNum[index] - 2)
            {
                double incrementParamsElement   =NAN;
                /** здесь вынули ТОЛЬКО инкремент!
                */
                int rc = ptrIncrement->getParam(j, incrementParamsElement);
                if(rc)
                {
///TODO!!! очень плохо, побочный эффект  _id = -1;
                    return NULL;
                }
///BUG БЫЛ нужно _currentParams[i] +=incrementParamsElement!
                double currentParamsElement   =NAN;
                ptrCurrParams->getParam(j, currentParamsElement);
                currentParamsElement += incrementParamsElement;
                ptrCurrParams->setParam(j, currentParamsElement);
                _coordsOfCurrentParamsUnionInCompact[index]++;
                return _currentParamsUnion;
            }
            /** случай, когда должны получить последний элемент компакта на index-ой кооридинатной оси
            */
            if (_coordsOfCurrentParamsUnionInCompact[index] == _pointNum[index] - 2)
            {
                double endParamsElement   =NAN;
                int rc = ptrEndParams->getParam(j, endParamsElement);
                if(rc)
                {
///TODO!!! очень плохо, побочный эффект  _id = -1;
                    return NULL;
                }
                ptrCurrParams->setParam(j, endParamsElement);
                _coordsOfCurrentParamsUnionInCompact[index]++;
                return _currentParamsUnion;
            }
            /** пошли по кругу! */
            if (_coordsOfCurrentParamsUnionInCompact[index] == _pointNum[index] - 1)
            {

                double startParamsElement   =NAN;
                int rc = ptrStartParams->getParam(j, startParamsElement);
                if(rc)
                {
///                    _id = -1;
                    return NULL;
                }
                ptrCurrParams->setParam(j, startParamsElement);
                _coordsOfCurrentParamsUnionInCompact[index] = 0;
                ++index;
/// NO WAY! return _currentParamsUnion; <- иначе сломаем обход компакта!
            }
        }///end for loop over i-th item of ParamsUnion
    }///end for all items/all params coords
    _id = -1;
    return NULL;
};
/******************************************************************************************************
 ЗДЕСЬ НЕЛЬЗЯ КЛОНИРОВАТЬ IParams с затребованным id, т.к физически в компакте его точки не храняться!
*******************************************************************************************************/
IParamsUnion* IParamsUnionCompact_Impl::getParamsUnionCopy(const size_t id) const
{
    size_t size = this->getSize();
    if(size == 0)
    {
        return NULL;
    }

    if (id < 0 || id >= size)
    {
        return NULL;
    }
    size_t tmpId = id;
    /**
    Here we pass NULL instead of Logger* because that pointer is ignored into the code of IParamsUnion::createParamsUnion()
    So, NO ERROR expected.
    */
    IParamsUnion * ptrIParamsUnion = _startParamsUnion->clone(); // BAD! IParamsUnion::createParamsUnion( _totalOfItemsInParamsUnion, (const size_t*) _paramsUnionItemsDimensions, NULL);
    if(ptrIParamsUnion == NULL)
    {
        return NULL;
    }
    /** NB : We INCREMENT index into the innermost for-loop */
    for(size_t i=0, index=0; (i < _totalOfItemsInParamsUnion) && (index < _paramsSpaceDimension); ++i)
    {
        IParams * ptrStartParams        = _startParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrEndParams          = _endParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrIncrement          = _increment->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrIParamsUnionItem   = ptrIParamsUnion->ptrIParamsUnionToPtrIParams(i);
        for(size_t j=0; j < _paramsUnionItemsDimensions[i]; ++j)
        {
            size_t iCoord = tmpId % _pointNum[index];
            tmpId /= _pointNum[index];
            /**
            Двигаясь в цикле последовательно вдоль
            каждой из координатных осей параметрического пространства
            размерности _paramsSpaceDimension,
            мы ищем первую координату, по которой
            еще не уперлись в правую границу компакта по этой координате.
            Обнаружив такую координатную ось,
            мы инкрементируем смещение вдоль нее и
            достаем в incrementParamsElement значение по этой координате,
            пишем его в вектор параметров _currentParams
            по индексу, соответствующему этой координатной оси.
            Вынимаем очередную координату и пишем ее в соответствующее место затребованного ParamsUnion.
            */

            if ((int)iCoord < (int)_pointNum[index] - 1)
            {
                double startParamsElement       =NAN;
                double incrementParamsElement   =NAN;
                /** здесь вынули ТОЛЬКО инкремент!
                */
                int rc = ptrIncrement->getParam(j, incrementParamsElement);
                if(rc)
                {
                    delete ptrIParamsUnion;
                    return NULL;
                }
///BUG БЫЛ нужно _currentParams[i] +=incrementParamsElement!
                ptrStartParams->getParam(j, startParamsElement);
                double currentParamsElement   =NAN;
                currentParamsElement = startParamsElement + (iCoord * incrementParamsElement);
                ptrIParamsUnionItem->setParam(j, currentParamsElement);
            }
            /** случай, когда должны получить последний элемент компакта на index-ой кооридинатной оси
            */
            if ((int)iCoord == (int)_pointNum[index] - 1)
            {
                double endParamsElement   =NAN;
                int rc = ptrEndParams->getParam(j, endParamsElement);
                if(rc)
                {
                    delete ptrIParamsUnion;
                    return NULL;
                }
                ptrIParamsUnionItem->setParam(j, endParamsElement);
            }
            /** случай, когда должны получить последний элемент компакта на index-ой кооридинатной оси
            */
            if ((int)iCoord == 0)
            {
                double startParamsElement   = NAN;
                int rc = ptrStartParams->getParam(j, startParamsElement);
                if(rc)
                {
                    delete ptrIParamsUnion;
                    return NULL;
                }
                ptrIParamsUnionItem->setParam(j, startParamsElement);
            }
            ++index;
        }///end for loop over i-th item of ParamsUnion
    }///end for all items/all params coords
///TODO!!!

    return ptrIParamsUnion;
};

bool IParamsUnionCompact_Impl::end() const
{
    return (_id == -1);
};

/****************************************************************************************
*                 Static factories implementation                                       *
****************************************************************************************/
/****************************************************************************************
* static  public interface to make IParamsCompact* by IParams* bounds, increment
****************************************************************************************/
IParamsCompact* IParamsCompact::createParamsCompact(const IModel* model,  IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger)
{
    /** first validate params */
    if(false == IParamsCompact_Impl::areValidCTORparams(model, leftBound, rightBound, increments, stepValue, logger))
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong params in IParamsCompact::createParamsCompact()!");
        return NULL;
    }
    /** second validate model type */
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::ADDITIVE_MODEL;
    model->getModelType(mt);
    if(mt == IModel::ModelsTypes::ADDITIVE_MODEL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "For AdditiveModel use IParamsUnionCompact::createParamsUnionCompact()!");
        return NULL;
    }
    /** finally create empty compact */
    IParamsCompact_Impl* compact = new IParamsCompact_Impl(const_cast<IModel*> (model));
    /** and fill/init the compact with params */
    bool rc = compact->fillParamsCompact(model, leftBound, rightBound, increments, stepValue, logger);
    if(rc)
    {
        return (IParamsCompact*) compact;
    }
    else
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Can not fill Compact with leftBound, rightBound, increments in IParamsCompact::createParamsCompact()!");
        delete compact;
        return NULL;
    }
};
/***********************************************************************
* static  public interface to make IParamsUnionCompact* by IParamsUnion* bounds, increment
************************************************************************/
IParamsUnionCompact* IParamsUnionCompact::createParamsUnionCompact(const IAdditiveModel* additiveModel,  IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool stepValue, Logger* logger)
{
    /** first validate params */
    if(false == IParamsUnionCompact_Impl::areValidCTORparams(additiveModel, leftBound, rightBound, increments, stepValue, logger))
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong params in IParamsUnionCompact::createParamsUnionCompact()!");
        return NULL;
    }
    /** second validate model type */
    enum AdditiveModelsTypes amt = FAKE_ADDITIVE_MODEL;
    additiveModel->getModelType(amt);
    if(amt == FAKE_ADDITIVE_MODEL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong AdditiveModel in IParamsUnionCompact::createParamsUnionCompact()!");
        return NULL;
    }
    /** finally create empty compact */
    IParamsUnionCompact_Impl* compact = new IParamsUnionCompact_Impl(const_cast<IAdditiveModel*>(additiveModel));
    /** and fill/init the compact with params */
    bool rc = compact->fillParamsUnionCompact(additiveModel, leftBound, rightBound, increments, stepValue, logger);
    if(rc)
    {
        return (IParamsUnionCompact*) compact;
    }
    else
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Can not fill Compact with leftBound, rightBound, increments in IParamsUnionCompact::createParamsUnionCompact()!");
        delete compact;
        return NULL;
    }
};

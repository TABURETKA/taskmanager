#define _DEBUG_
#include "Params.h"
#include "../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../CommonStuff/Defines.h" /* DELIMITER_TO_STRING_ITEMS */
#include "../Log/errors.h"
#include "../Log/Log.h"
#include <sstream>
#include <stdlib.h>

///static in IParams
const char * IParams::paramsNames[IParams::ParamsTypes::FAKE_PARAMS]=
{
  "Params1D",
  "GaussianParams",
  "WienerParams",
  "PoissonParams",
  "ParetoParams",
  "PoissonDrvnParetoJumpsExpMeanRevertingParams"
};

///static in IParams
enum IParams::ParamsTypes IParams::getParamsTypeByParamsName(const std::string& paramsName)
{
  size_t i=0;
  for(i=0; i < FAKE_PARAMS; ++i)
  {
      if (paramsName == IParams::paramsNames[i])
      {
          return (enum IParams::ParamsTypes)i;
      }
  }
  return FAKE_PARAMS;
};
///static in IParams
const char * IParams::grammar[FAKE_INDEX_GRAMMAR_PARAMS1D]={
  "DIMENSION"
};

///static in IParamsGaussian
const size_t IParamsGaussian::GaussianCDFParamsDimension = 2;
/** tokens to parse map for GaussianParams */
///static in IParamsGaussian
const char * IParamsGaussian::grammar[IParamsGaussian::indexesOfGrammarForGaussianParams::FAKE_INDEX_GRAMMAR_GAUSSIAN] ={
  "MU",
  "SIGMA"
};

///static in
const size_t IParamsWiener::WienerCDFParamsDimension = 3;
/** tokens to parse map for WienerParams */
///static in IParamsWiener
const char * IParamsWiener::grammar[IParamsWiener::indexesOfGrammarForWienerParams::FAKE_INDEX_GRAMMAR_WIENER]={
  "INITIAL_CONDITION",
  "MU",
  "SIGMA"
};

///static in IParamsPoisson
const size_t IParamsPoisson::PoissonCDFParamsDimension = 1;
/** tokens to parse map for PoissonParams */
///static in IParamsPoisson
const char * IParamsPoisson::grammar[IParamsPoisson::indexesOfGrammarForPoissonParams::FAKE_INDEX_GRAMMAR_POISSON]={
  "LAMBDA",
};

///static in IParamsPareto
const size_t IParamsPareto::ParetoCDFParamsDimension = 2;
/** tokens to parse map for ParetoParams */
///static in IParamsPareto
const char * IParamsPareto::grammar[FAKE_INDEX_GRAMMAR_PARETO]={
  "X_MIN",
  "K",
  "K_MIN",
  "K_MAX",
  "X_MIN_THRESHOLD"
};

///static in IParamsPoissonDrvnParetoJumpsExpMeanReverting
const size_t IParamsPoissonDrvnParetoJumpsExpMeanReverting::PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension = 4;
/** tokens to parse map for PoissonDrvnParetoJumpsExpMeanRevertingParams */
///static in IParamsPoissonDrvnParetoJumpsExpMeanReverting
const char * IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[FAKE_INDEX_GRAMMAR_POISSONDRVN_PARETOJUMPS_EXPMR]={
  "LAM",
  "LAM_C",
  "X_MIN",
  "K"
};

/** anonymous namespace to hide private implementations into this cpp-file */
namespace{
/** class container */
/******************************************************
* Base Params class
*******************************************************/
class IParams_Impl : public virtual IParams
{
public:
     /** PUBLIC CTORs (in the scope of this file ONLY)
     to call from CTORs of derived classes
     */
     IParams_Impl(size_t size);
     IParams_Impl(const IParams_Impl&);
     IParams_Impl& operator=(const IParams_Impl&);
     /** Default DTOR */
     ~IParams_Impl();
     /** Default factory method merely
     to create empty Params1D implementation
     */
     static IParams_Impl* createEmptyParams();
     /** Default factory method merely
     to create empty Params1D implementation of given size
     */
     static IParams_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);

///TODO!!! WHY? Any subclass may have own clone algorithm???
    virtual IParams_Impl* clone() const;
    virtual std::string toString() const;

    virtual size_t getSize() const;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MUST define operator(i,...) to set access to params by index
    virtual int setParam(size_t index, double value);
    virtual int setParams(const size_t size, const double * params, const size_t offset=0);
    virtual int getParam(size_t index, double& param) const;
    virtual int getParams(const size_t size, double * params, const size_t offset=0) const;
protected:
    int reallocateArray(size_t size);

private:
    /** data stuff */
    size_t _size;
    double * _array;
};

/******************************************************
* IParamsGaussian interface implementation
*******************************************************/
class IParamsGaussian_Impl : public virtual IParams_Impl,
                           public virtual IParamsGaussian
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsGaussian_Impl();
    /** Factory methods */
    static IParamsGaussian_Impl* createEmptyParams();
    static IParamsGaussian_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsGaussian_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Gaussian model */
    virtual int setParam(size_t index, double value);
    virtual int setParams(const size_t size, const double * params, const size_t offset =0);
    virtual int setMean(double newMean);
    virtual int setSDeviation(double newSD);
    virtual double mu() const;
       virtual double sigma() const;
/** TODO!!!
�� ������ ������ ������������ ��������� ��� ������� ���������� � ����������!
���� ����� ����������� ��������� IParams, � ����� ����������� �� ��� ������
������ ������� ����������, ������� ����� ����������� ������ �����������! */

private:
    /** NO public ctor! Factory is in use instead! */
      IParamsGaussian_Impl();
    /** assign and copy CTORs are disabled */
    IParamsGaussian_Impl(const IParamsGaussian_Impl&);
    IParamsGaussian_Impl& operator=(const IParamsGaussian_Impl&);
};

/******************************************************
* IParamsWiener interface implementation
*******************************************************/
class IParamsWiener_Impl : public virtual IParams_Impl,
                           public virtual IParamsWiener
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsWiener_Impl();
    /** Factory methods */
    static IParamsWiener_Impl* createEmptyParams();
    static IParamsWiener_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsWiener_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Wiener model */
    virtual int setParam(size_t index, double value);
    virtual int setParams(const size_t size, const double * params, const size_t offset =0);
    virtual int setInitialCondition(double initCondition);
    virtual int setMean(double newMean);
    virtual int setSDeviation(double newSD);
    virtual double initialCondition() const;
    virtual double mu() const;
       virtual double sigma() const;
/** TODO!!!
 �� ������ ������ ������������ ��������� ��� ������� ���������� � ����������!
���� ����� ����������� ��������� IParams, � ����� ����������� �� ��� ������
������ ������� ����������, ������� ����� ����������� ������ �����������! */

private:
    /** NO public ctor! Factory is in use instead! */
      IParamsWiener_Impl();
    /** assign and copy CTORs are disabled */
    IParamsWiener_Impl(const IParamsWiener_Impl&);
    IParamsWiener_Impl& operator=(const IParamsWiener_Impl&);
};

/******************************************************
* IParamsPoisson interface implementation
*******************************************************/
class IParamsPoisson_Impl : public virtual IParams_Impl,
                            public virtual IParamsPoisson
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsPoisson_Impl();
    /** Factory uses following methods */
    static IParamsPoisson_Impl* createEmptyParams();
    static IParamsPoisson_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsPoisson_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Pareto model */
    virtual int setParam(size_t index, double value);
    virtual int setParams(const size_t size, const double * params, const size_t offset=0);

    virtual int setPoissonLambda(double lambda);
    virtual double lambda() const;

private:
    /** NO public ctor! Factory is in use instead! */
    IParamsPoisson_Impl();
    /** assign and copy CTORs are disabled */
    IParamsPoisson_Impl(const IParamsPoisson_Impl&);
    IParamsPoisson_Impl& operator=(const IParamsPoisson_Impl&);
};
/******************************************************
* IParamsPareto interface implementation
*******************************************************/
class IParamsPareto_Impl : public virtual IParams_Impl,
                                  public virtual IParamsPareto
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsPareto_Impl();
    /** Factory uses following methods */
    static IParamsPareto_Impl* createEmptyParams();
    static IParamsPareto_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsPareto_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Pareto model */
    virtual int setParam(size_t index, double value);
    virtual int setParams(const size_t size, const double * params, const size_t offset=0);

    virtual int setParetoXm(double xM);
    virtual int setParetoK(double k);
    virtual double xM() const;
    virtual double k() const;

private:
    /** NO public ctor! Factory is in use instead! */
    IParamsPareto_Impl();
    /** assign and copy CTORs are disabled */
    IParamsPareto_Impl(const IParamsPareto_Impl&);
    IParamsPareto_Impl& operator=(const IParamsPareto_Impl&);
};
/******************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting interface implementation
*******************************************************/
class IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl : public virtual IParams_Impl,
                                  public virtual IParamsPoissonDrvnParetoJumpsExpMeanReverting
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    /** Factory uses following methods */
    static IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* createEmptyParams();
    static IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Poisson-Pareto model */
    virtual int setParam(size_t index, double value);
    virtual int setParams(const size_t size, const double * params, const size_t offset=0);

    virtual int setPoissonLambda(double PoissonLambda);
    virtual int setMRLambda(double lambda_c);
    virtual int setParetoXm(double xM);
    virtual int setParetoK(double k);
    virtual double lam() const;
    virtual double lamC() const;
    virtual double xM() const;
    virtual double k() const;

private:
    /** NO public ctor! Factory is in use instead! */
      IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    /** assign and copy CTORs are disabled */
    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& operator=(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
};

/******************************************************
* IParamsUnion interface implementation
*******************************************************/
/** Merely array of IParams - class container */
class     IParamsUnion_Impl : public virtual IParamsUnion
{
public:
    /** plain params UNION for additive model params */
    IParamsUnion_Impl(const size_t totalOfItems, const size_t* sizesOfItems);
    /** make by instances of additive model params */
    IParamsUnion_Impl(const size_t totalOfItems, const IParams** items);
///TODO??? Does it make sense to do them public?
    IParamsUnion_Impl(const IParamsUnion_Impl&);
    IParamsUnion_Impl& operator=(const IParamsUnion_Impl&);
    virtual ~IParamsUnion_Impl();
    /** IParams stuff */
      /** Any subclass may have own copy algorithm */
    virtual IParamsUnion_Impl* clone() const;
    /** Every subclass MUST have own IMPLEMENTATION for toString()! */
    virtual std::string toString() const;

    /** IParamsUnion stuff */
    virtual int getTotalOfItemsInParamsUnion(size_t& total) const;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MUST define operator(i,...) to get access to params by index
    virtual int setItem(const size_t index, const IParams* itemParams);
    virtual int setItems(const size_t size, const IParams** itemsParams);
    /** the clone/copy of existing _items[index] */
    virtual int getItemCopy(const size_t index, IParams*& itemParams) const;
    IParams* ptrIParamsUnionToPtrIParams(const size_t index) const;
    /** to clone/copy the entire ParamsUnion instance/all its _items */
    virtual int getItemsCopies(const size_t size, IParams**& itemsParams) const;
private:
    /** data stuff */
    size_t _totalOfItems;
    IParams ** _items;
    int reallocatePtrToItems(const size_t totalOfItems);
    /** for plain addends only */
    int reallocateItems(const size_t totalOfItems, const size_t* sizesOfItems);
};

}; ///end anonymous namespace

/**************** IMPLEMENTATIONs ***************************/
/*************************************************************
*           IParams Private Implementation
*************************************************************/
int IParams_Impl::reallocateArray(size_t size)
{
    delete [] _array;
    _array = NULL;
    if(! size)
    {
        _size=0;
        return _RC_SUCCESS_;
    }
    _array = new double[size];
    if (_array)
    {
        _size = size;
        std::memset(_array, 0, size*sizeof(double));
        return _RC_SUCCESS_;
    }
    return _ERROR_NO_ROOM_;
};
/***************************************************
* IParams_Impl CTOR
****************************************************/
IParams_Impl::IParams_Impl(size_t size) : IParams()
{
  _size=0;
  _array=NULL;
  if (size)
  {
      int rc = reallocateArray(size);
      if (rc)
      {
          /** HWO LOG error?
           Clean up members for SURE!
          */
          _size=0;
          _array=NULL;
      }
  }
};
/***************************************************
* IParams_Impl Copy CTOR
****************************************************/
IParams_Impl::IParams_Impl(const IParams_Impl& paramsImplSrc) // : IParams_Impl(0)
{
    _size=0;
    _array=NULL;
       /** deep copy */
    if (! paramsImplSrc._size)
        return;
    const double * dblArray= (const double *)paramsImplSrc._array;
    if (!dblArray)
    {
        return ;
    }
    /** finalize deep copy */
    int rc = setParams(paramsImplSrc._size, dblArray);
      if (rc)
    {
///TODO!!! HOW Log?
    }
};
/***************************************************
* IParams_Impl Assignment CTOR
****************************************************/
IParams_Impl& IParams_Impl::operator=(const IParams_Impl& paramsImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsImplSrc)
        return *this;
       /** deep copy */
    if (!_size){
        reallocateArray(0);
    }
    const double * dblArray= (const double *)paramsImplSrc._array;
    if (!dblArray)
    {
        _size=0;
        _array=NULL;
        return *this;
    }
    /** finalize deep copy */
    int rc = setParams(paramsImplSrc._size, dblArray);
      if (rc)
    {
///TODO!!! HOW Log?
    }
    return *this;
};
/***************************************************
* IParams_Impl DTOR
****************************************************/
IParams_Impl::~IParams_Impl()
{
#ifdef _DEBUG_
std::cout << "into ~IParams_Impl :" <<  std::endl;
#endif
    if(_size)
    {
        delete [] _array;
        _array = NULL;
        _size=0;
    }
};
/***************************************************
* IParams_Impl toString
****************************************************/
std::string IParams_Impl::toString() const
{
    return " In IParams_Impl class (Base class) toString() not implemented";
}
/***************************************************
* IParams_Impl getSize
****************************************************/
size_t IParams_Impl::getSize() const
{
    return this->_size;
}
/** ��� ������ ����� ���������� ������
 �� ���������� ���������� ���������� ���������� ������,
������� ����� ������������� �� IParams!
� ��� � ���������� ���������� IParams_Impl,
� �������� setParam/setParams � getParam/getParams
� ����� ������ ������������� ������� */
/***************************************************
* IParams_Impl getParam
****************************************************/
int IParams_Impl::getParam(size_t index, double& param) const
{
    if (index >= this->_size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    param = this->_array[index];
    return _RC_SUCCESS_;
};
/***************************************************
* IParams_Impl setParam
****************************************************/
int IParams_Impl::setParam(size_t index, double value)
{
    if (index > this->_size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    this->_array[index] = value;
    return _RC_SUCCESS_;
};
/***************************************************
* IParams_Impl getParams
****************************************************/
int IParams_Impl::getParams(const size_t size, double * params, const size_t offset) const
{
    if ((offset + size) > _size)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    std::memcpy(params, (this->_array + offset), size*sizeof(double));
    return _RC_SUCCESS_;
}
/***************************************************
* IParams_Impl setParams
****************************************************/
int IParams_Impl::setParams(const size_t size, const double* params, const size_t offset)
{
    if(this->_size < (offset + size))
    {
       int rc = reallocateArray(offset + size);
       if (rc)
       {
           return rc;
       }
    };
    std::memcpy(this->_array + offset, params, size*sizeof(double));
    return _RC_SUCCESS_;
}

/***************************************************
* IParams_Impl clone
****************************************************/
IParams_Impl* IParams_Impl::clone() const
{
    /** Now it's DEEP! Not Shallow copy */
     IParams_Impl* params = new IParams_Impl(*this);
      /** follows by deep copy
      size_t size=this->_size;
    if (!size)
        return NULL;// nullptr;
    if (!params)
        return NULL; // nullptr;
    const double * dblArray=this->_array;
    if (!dblArray)
        return NULL; // nullptr;
    /** finalize deep copy
    int rc = params->setParams(size, dblArray);
      if (rc)
    {
        delete params;
        params = NULL;
        return NULL; // nullptr;
    }
    */
    return params;
}

/*** Factory method
 merely to create empty Params1D implementation of size 0
*/
IParams_Impl* IParams_Impl::createEmptyParams()
{
    return new IParams_Impl(0);
};

/*** Factory method
 merely to create empty Params1D implementation of given size
*/
IParams_Impl* IParams_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
//OLD    iteratorParamsMap itParam = mapParams.find(grammarParams1D[indexOfDimension]);
    iteratorParamsMap itParam = mapParams.find(IParams::grammar[indexOfDimension]);
    size_t dimension=0;
    if (itParam != mapParams.end()) {
        dimension = (size_t) atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }
    IParams_Impl* params = new IParams_Impl(dimension);
    return params;
}

/***************************************************
* IParamsGaussian_Impl DTOR
****************************************************/
IParamsGaussian_Impl::~IParamsGaussian_Impl()
{
#ifdef _DEBUG_
    std::cout<< "ParamsGaussian is deleting" << std::endl;
#endif
};
/***************************************************
* Private IParamsGaussian_Impl CTOR
****************************************************/
IParamsGaussian_Impl::IParamsGaussian_Impl() : IParams_Impl(GaussianCDFParamsDimension)
{
    setMean(NAN);
    setSDeviation(NAN);
}
/***************************************************
* Private IParamsGaussian_Impl Copy CTOR
****************************************************/
IParamsGaussian_Impl::IParamsGaussian_Impl(const IParamsGaussian_Impl& paramsGaussianImplSrc) : IParams_Impl(GaussianCDFParamsDimension)
{
       /** deep copy */
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsGaussianImplSrc;
    size_t size = getSize();
     double * ptrDblAray =new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
};
/***************************************************
* Private IParamsGaussian_Impl Assignment
****************************************************/
IParamsGaussian_Impl& IParamsGaussian_Impl::operator=(const IParamsGaussian_Impl& paramsGaussianImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsGaussianImplSrc)
        return *this;
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsGaussianImplSrc;
    size_t size = getSize();
     double * ptrDblAray = new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
     ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
     return *this;
};
/***************************************************
* IParamsGaussian_Impl toString
****************************************************/
std::string IParamsGaussian_Impl::toString() const
{
    std::stringstream ss;
#ifdef _DEBUG_
std::cout << mu() << std::endl;
std::cout << sigma() << std::endl;
#endif
    ss << grammar[MU] << DELIMITER_NAME_VALUE << mu() << DELIMITER_TO_STRING_ITEMS <<
             grammar[SIGMA] << DELIMITER_NAME_VALUE << sigma();

    return ss.str();
}
/***************************************************
* IParamsGaussian_Impl setParam
****************************************************/
int IParamsGaussian_Impl::setParam(size_t index, double value)
{
    size_t size = getSize();
    if (index > size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index == SIGMA) && (value <= 0))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
};

/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsGaussian_Impl setParams
****************************************************/
int IParamsGaussian_Impl::setParams(const size_t newSize, const double* params, const size_t offset)
{
    size_t size = getSize();
    if (newSize != size)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if (params[SIGMA] <= 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParams(size, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsGaussian_Impl clone
****************************************************/
IParamsGaussian_Impl* IParamsGaussian_Impl::clone() const
{
    IParamsGaussian_Impl* params = new IParamsGaussian_Impl(*this);
    return params;
}
/***************************************************
* IParamsGaussian_Impl mu() == getParam(0)
****************************************************/
double IParamsGaussian_Impl::mu() const
{
   double Mu=NAN;
   int rc = getParam(MU, Mu);
///TODO!!! HOW Log if error?
   return Mu;
}
/***************************************************
* IParamsGaussian_Impl mu() == getParam(1)
****************************************************/
double IParamsGaussian_Impl::sigma() const
{
    double Sigma = NAN;
    int rc = getParam(SIGMA, Sigma);
///TODO!!! HOW Log if error?
    return Sigma;
}
/***************************************************
* IParamsGaussian_Impl setMean() == setParam(0)
****************************************************/
int IParamsGaussian_Impl::setMean(double newMean)
{
    return setParam(MU, newMean);
}
/***************************************************
* IParamsGaussian_Impl setSDeviation() == setParam(1)
****************************************************/
int IParamsGaussian_Impl::setSDeviation(double newSD)
{
    if (newSD <= 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    return setParam(SIGMA, newSD);
}

/***************** IParamsGaussian_Impl Factory *****************************/
IParamsGaussian_Impl* IParamsGaussian_Impl::createEmptyParams()
{
    return new IParamsGaussian_Impl();
};
/***************** IParamsGaussian_Impl Factory *****************************/
IParamsGaussian_Impl* IParamsGaussian_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    iteratorParamsMap itParam = mapParams.find(IParamsGaussian::grammar[MU] + postfix);
    double mu=NAN;
    if (itParam != mapParams.end()) {
        mu = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    double sigma=NAN;
    itParam = mapParams.find(IParamsGaussian::grammar[SIGMA] + postfix);
    if (itParam != mapParams.end()) {
        sigma = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    IParamsGaussian_Impl* params = new IParamsGaussian_Impl();
    params->setMean(mu);
    params->setSDeviation(sigma);
    return params;
}
/***************************************************
* IParamsWiener_Impl DTOR
****************************************************/
IParamsWiener_Impl::~IParamsWiener_Impl()
{
#ifdef _DEBUG_
    std::cout<< "ParamsWiener is deleting" << std::endl;
#endif
};
/***************************************************
* Private IParamsWiener_Impl CTOR
****************************************************/
IParamsWiener_Impl::IParamsWiener_Impl() : IParams_Impl(WienerCDFParamsDimension)
{
    setInitialCondition(NAN);
    setMean(NAN);
    setSDeviation(NAN);
}
/***************************************************
* Private IParamsWiener_Impl Copy CTOR
****************************************************/
IParamsWiener_Impl::IParamsWiener_Impl(const IParamsWiener_Impl& paramsWienerImplSrc) : IParams_Impl(WienerCDFParamsDimension)
{
       /** deep copy */
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsWienerImplSrc;
    size_t size = getSize();
     double * ptrDblAray =new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
};
/***************************************************
* Private IParamsWiener_Impl Assignment
****************************************************/
IParamsWiener_Impl& IParamsWiener_Impl::operator=(const IParamsWiener_Impl& paramsWienerImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsWienerImplSrc)
        return *this;
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsWienerImplSrc;
    size_t size = getSize();
     double * ptrDblAray = new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
     ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
     return *this;
};
/***************************************************
* IParamsWiener_Impl toString
****************************************************/
std::string IParamsWiener_Impl::toString() const
{
    std::stringstream ss;
    ss << grammar[INITIAL_CONDITION] << DELIMITER_NAME_VALUE << initialCondition() << DELIMITER_TO_STRING_ITEMS <<
    grammar[MU] << DELIMITER_NAME_VALUE << mu() << DELIMITER_TO_STRING_ITEMS <<
    grammar[SIGMA] << DELIMITER_NAME_VALUE << sigma();

    return ss.str();
}
/***************************************************
* IParamsWiener_Impl setParam
****************************************************/
int IParamsWiener_Impl::setParam(size_t index, double value)
{
    size_t size = getSize();
    if (index > size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index == SIGMA) && (value <= 0))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
};

/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsWiener_Impl setParams
****************************************************/
int IParamsWiener_Impl::setParams(const size_t newSize, const double* params, const size_t offset)
{
    size_t size = getSize();
    if (newSize != size)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if (params[SIGMA] <= 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParams(size, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsWiener_Impl clone
****************************************************/
IParamsWiener_Impl* IParamsWiener_Impl::clone() const
{
    IParamsWiener_Impl* params = new IParamsWiener_Impl(*this);
    return params;
}
/***************************************************
* IParamsWiener_Impl initialCondition() == getParam(0)
****************************************************/
double IParamsWiener_Impl::initialCondition() const
{
   double initialCondition=NAN;
   int rc = getParam(INITIAL_CONDITION, initialCondition);
///TODO!!! HOW Log if error?
   return initialCondition;
}
/***************************************************
* IParamsWiener_Impl mu() == getParam(1)
****************************************************/
double IParamsWiener_Impl::mu() const
{
   double Mu=NAN;
   int rc = getParam(MU, Mu);
///TODO!!! HOW Log if error?
   return Mu;
}
/***************************************************
* IParamsWiener_Impl mu() == getParam(2)
****************************************************/
double IParamsWiener_Impl::sigma() const
{
    double Sigma = NAN;
    int rc = getParam(SIGMA, Sigma);
///TODO!!! HOW Log if error?
    return Sigma;
}
/***************************************************
* IParamsWiener_Impl setInitialCondition() == setParam(0)
****************************************************/
int IParamsWiener_Impl::setInitialCondition(double initCondition)
{
    return setParam(INITIAL_CONDITION, initCondition);
}
/***************************************************
* IParamsWiener_Impl setMean() == setParam(1)
****************************************************/
int IParamsWiener_Impl::setMean(double newMean)
{
    return setParam(MU, newMean);
}
/***************************************************
* IParamsWiener_Impl setSDeviation() == setParam(2)
****************************************************/
int IParamsWiener_Impl::setSDeviation(double newSD)
{
    return setParam(SIGMA, newSD);
}
/***************** IParamsWiener Factory *****************************/
IParamsWiener_Impl* IParamsWiener_Impl::createEmptyParams()
{
    return new IParamsWiener_Impl();
};
/***************** IParamsWiener Factory *****************************/
IParamsWiener_Impl* IParamsWiener_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    iteratorParamsMap itParam = mapParams.find(IParamsWiener::grammar[INITIAL_CONDITION] + postfix);
    double ic=NAN;
    if (itParam != mapParams.end()) {
        ic = atof(itParam->second.c_str());
    }
    /** NO else - we assume the initial conditions may be non-initialized???
    */
    itParam = mapParams.find(IParamsWiener::grammar[MU] + postfix);
    double mu=NAN;
    if (itParam != mapParams.end()) {
        mu = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    double sigma=NAN;
    itParam = mapParams.find(IParamsWiener::grammar[SIGMA] + postfix);
    if (itParam != mapParams.end()) {
        sigma = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    IParamsWiener_Impl* params = new IParamsWiener_Impl();
    if(! isNAN_double(ic))
    {
        params->setInitialCondition(ic);
    }
    params->setMean(mu);
    params->setSDeviation(sigma);
    return params;
}

/***************************************************
* IParamsPoisson_Impl DTOR
****************************************************/
IParamsPoisson_Impl::~IParamsPoisson_Impl()
{
#ifdef _DEBUG_
    std::cout<< "ParamsPoisson is deleting" << std::endl;
#endif
};
/***************************************************
* Private IParamsPoisson_Impl CTOR
****************************************************/
IParamsPoisson_Impl::IParamsPoisson_Impl() : IParams_Impl(PoissonCDFParamsDimension)
{
   setPoissonLambda(NAN);
}
/***************************************************
* Private IParamsPoisson_Impl Copy CTOR
****************************************************/
IParamsPoisson_Impl::IParamsPoisson_Impl(const IParamsPoisson_Impl& paramsPoissonImplSrc) : IParams_Impl(PoissonCDFParamsDimension)
{
    /** deep copy */
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonImplSrc;
    size_t size = getSize();
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
    delete [] ptrDblAray;
    ptrDblAray = NULL;
};
/***************************************************
* Private IParamsPoisson_Impl Assignment
****************************************************/
IParamsPoisson_Impl& IParamsPoisson_Impl::operator=(const IParamsPoisson_Impl& paramsPoissonImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsPoissonImplSrc)
        return *this;
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonImplSrc;
    size_t size = getSize();
    double * ptrDblAray = new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
    delete [] ptrDblAray;
    ptrDblAray = NULL;
    return *this;
};
/***************************************************
* IParamsPoisson_Impl toString
****************************************************/
std::string IParamsPoisson_Impl::toString() const
{
   std::stringstream ss;
   ss << grammar[LAMBDA] << DELIMITER_NAME_VALUE << lambda();
   return ss.str();
}
/***************************************************
* IParamsPoisson_Impl setParam
****************************************************/
int IParamsPoisson_Impl::setParam(size_t index, double value)
{
    size_t size = getSize();
    if (index > size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index == 0) && (value <= 0))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
};

/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsPoisson_Impl setParams
****************************************************/
int IParamsPoisson_Impl::setParams(const size_t newSize, const double* params, const size_t offset)
{
    size_t size = getSize();
    if (newSize != size)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if (params[LAMBDA] <= 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParams(size, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsPoisson_Impl clone
****************************************************/
IParamsPoisson_Impl* IParamsPoisson_Impl::clone() const
{
   IParamsPoisson_Impl* params = new IParamsPoisson_Impl(*this);

   return params;
}
/***************************************************
* IParamsPoisson_Impl lambda() == getParam(0)
****************************************************/
double IParamsPoisson_Impl::lambda() const
{
   double Lambda=NAN;
   int rc = getParam(LAMBDA, Lambda);
///TODO!!! HOW Log if error?
   return Lambda;
}
/***************************************************
* IParamsPoisson_Impl setPoissonLambda() == setParam(0)
****************************************************/
int IParamsPoisson_Impl::setPoissonLambda(double newPoissonLambda)
{
    return setParam(LAMBDA, newPoissonLambda);
}
/***************** IParamsPoisson Factory *****************************/
IParamsPoisson_Impl* IParamsPoisson_Impl::createEmptyParams()
{
    return new IParamsPoisson_Impl();
};
/***************** IParamsPoisson Factory *****************************/
IParamsPoisson_Impl* IParamsPoisson_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
   iteratorParamsMap itParam = mapParams.find(IParamsPoisson::grammar[LAMBDA] + postfix);
   double lambda=NAN;
   if (itParam != mapParams.end()) {
      lambda = atof(itParam->second.c_str());
   }
   else {
      return NULL;
   }

   IParamsPoisson_Impl* params = new IParamsPoisson_Impl();
    params->setPoissonLambda(lambda);
   return params;
}

/***************************************************
* IParamsPareto_Impl DTOR
****************************************************/
IParamsPareto_Impl::~IParamsPareto_Impl()
{
#ifdef _DEBUG_
    std::cout<< "ParamsPareto is deleting" << std::endl;
#endif
};
/***************************************************
* Private IParamsPareto_Impl CTOR
****************************************************/
IParamsPareto_Impl::IParamsPareto_Impl() : IParams_Impl(ParetoCDFParamsDimension)
{
    setParetoXm(NAN);
    setParetoK(NAN);
};

/***************************************************
* Private IParamsPareto_Impl Copy CTOR
****************************************************/
IParamsPareto_Impl::IParamsPareto_Impl(const IParamsPareto_Impl& paramsParetoImplSrc) : IParams_Impl(ParetoCDFParamsDimension)
{
       /** deep copy */
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsParetoImplSrc;
    size_t size = getSize();
     double * ptrDblAray =new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
     ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
};

/***************************************************
* Private IParamsPareto_Impl Assignment
****************************************************/
IParamsPareto_Impl& IParamsPareto_Impl::operator=(const IParamsPareto_Impl& paramsParetoImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsParetoImplSrc)
        return *this;
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsParetoImplSrc;
    size_t size = getSize();
     double * ptrDblAray =new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
     ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
     return *this;
};
/***************************************************
* IParamsPareto_Impl toString
****************************************************/
std::string IParamsPareto_Impl::toString() const
{
    std::stringstream ss;
    ss << grammar[X_MIN] << DELIMITER_NAME_VALUE << xM() <<
    DELIMITER_TO_STRING_ITEMS <<
    grammar[K] << DELIMITER_NAME_VALUE << k();
    return ss.str();
};
/***************************************************
* IParamsPareto_Impl setParam
****************************************************/
int IParamsPareto_Impl::setParam(size_t index, double value)
{
    size_t size = getSize();
    if (index > size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if (value <= 0)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
};

/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsPareto_Impl setParams
****************************************************/
int IParamsPareto_Impl::setParams(const size_t newSize, const double* params, const size_t offset)
{
    size_t size = getSize();
    if (newSize != size)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    for(size_t i=0; i < size; ++i)
    {
        if (params[i] <= 0)
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    IParams_Impl::setParams(size, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsPareto_Impl setParetoXm
****************************************************/
int IParamsPareto_Impl::setParetoXm(double X_min)
{
    return setParam(X_MIN, X_min);
};
/***************************************************
* IParamsPareto_Impl setParetoK
****************************************************/
int IParamsPareto_Impl::setParetoK(double k)
{
    return setParam(K, k);
};
/***************************************************
* IParamsPareto_Impl getParetoXm
****************************************************/
double IParamsPareto_Impl::xM() const
{
   double X_min=NAN;
   int rc = getParam(X_MIN, X_min);
///TODO!!! Log if error
   return X_min;
};
/***************************************************
* IParamsPareto_Impl getParetoK
****************************************************/
double IParamsPareto_Impl::k() const
{
   double k=NAN;
   int rc = getParam(K, k);
///TODO!!! HOW Log if error?
   return k;
};
/***************************************************
* IParamsPareto_Impl clone
****************************************************/
IParamsPareto_Impl* IParamsPareto_Impl::clone() const
{
    IParamsPareto_Impl* params = new IParamsPareto_Impl(*this);
    return params;
};
/***************** IParamsPareto Factory *****************************/
IParamsPareto_Impl* IParamsPareto_Impl::createEmptyParams()
{
    return new IParamsPareto_Impl();
};
/***************** IParamsPareto Factory *****************************/
IParamsPareto_Impl* IParamsPareto_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
//    ParamsPareto params;
    double xM;
    iteratorParamsMap itParam = mapParams.find(IParamsPareto::grammar[X_MIN] + postfix);
    if (itParam != mapParams.end()) {
        xM = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    double k;
    itParam = mapParams.find(IParamsPareto::grammar[K] + postfix);
    if (itParam != mapParams.end()) {
        k = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    IParamsPareto_Impl* params = new IParamsPareto_Impl();
    params->setParetoXm(xM);
    params->setParetoK(k);
    return params;
};

/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl DTOR
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::~IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl()
{
#ifdef _DEBUG_
    std::cout<< "ParamsPoissonDrvnParetoJumpsExpMeanReverting is deleting" << std::endl;
#endif
};

/***************************************************
* Private IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl CTOR
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl() : IParams_Impl(PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension)
{
    setPoissonLambda(NAN);
    setMRLambda(NAN);
    setParetoXm(NAN);
    setParetoK(NAN);
};

/***************************************************
* Private IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl Copy CTOR
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc) : IParams_Impl(PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension)
{
       /** deep copy */
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc;
    size_t size = getSize();
     double * ptrDblAray =new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
     ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
};
/***************************************************
* Private IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl Assignment
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::operator=(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc)
        return *this;
       IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
     IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc;
    size_t size = getSize();
     double * ptrDblAray =new double[size];
     std::memset(ptrDblAray, 0, size*sizeof(double));
     int rc = ptrParamsImplSrc->getParams(size, ptrDblAray);
     ptrParamsImpl->setParams(size, ptrDblAray);
     delete [] ptrDblAray;
     ptrDblAray = NULL;
     return *this;
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl toString
****************************************************/
std::string IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::toString() const
{
    std::stringstream ss;
    ss << grammar[LAMBDA] << DELIMITER_NAME_VALUE << lam()
    << DELIMITER_TO_STRING_ITEMS <<
    grammar[LAM_C] << DELIMITER_NAME_VALUE << lamC()
    << DELIMITER_TO_STRING_ITEMS <<
    grammar[X_min] << DELIMITER_NAME_VALUE << xM()
    << DELIMITER_TO_STRING_ITEMS <<
    grammar[K_] << DELIMITER_NAME_VALUE << k();

    return ss.str();
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl clone
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::clone() const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* params = new IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl(*this);
    return params;
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParam
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParam(size_t index, double value)
{
    size_t size = getSize();
    if (index > size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index != LAM_C) && (value <= 0))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
};
/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParams
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParams(const size_t newSize, const double* params, const size_t offset)
{
    size_t size = getSize();
    if (newSize != size)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    for(size_t i=0; i < size; ++i)
    {
        if ((i != LAM_C) && (params[i] <= 0))
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    IParams_Impl::setParams(size, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setPoissonLambda
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setPoissonLambda(double lambda)
{
   return setParam(LAMBDA, lambda);
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setMeanRevertLambda
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setMRLambda(double lambda_c)
{
    return setParam(LAM_C, lambda_c);
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParetoXm
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParetoXm(double x_min)
{
    return setParam(X_min, x_min);
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParetoK
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParetoK(double k)
{
    return setParam(K_, k);
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl getPoissonLambda
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::lam() const
{
   double lambda=NAN;
   int rc = getParam(LAMBDA, lambda);
///TODO!!! Log if error
   return lambda;
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl getMeanRevertLambda
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::lamC() const
{
   double lambdaC=NAN;
   int rc = getParam(LAM_C, lambdaC);
///TODO!!! HOW Log if error?
   return lambdaC;
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl getParetoXm
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::xM() const
{
   double xMin=NAN;
   int rc = getParam(X_min, xMin);
///TODO!!! HOW Log if error?
   return xMin;
};
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParetoK
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::k() const
{
   double k=NAN;
   int rc = getParam(K_, k);
///TODO!!! HOW Log if error?
   return k;
};
/***************** IParamsPoissonDrvnParetoJumpsExpMeanReverting Factory *****************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyParams()
{
    return new IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
};
/***************** IParamsPoissonDrvnParetoJumpsExpMeanReverting Factory *****************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
//    ParamsPoissonDrvnParetoJumpsExpMeanReverting params;
    double lambda;
    iteratorParamsMap itParam = mapParams.find(IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[LAMBDA] + postfix);
    if (itParam != mapParams.end()) {
        lambda = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    double lambda_c;
    itParam = mapParams.find(IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[LAM_C] + postfix);
    if (itParam != mapParams.end()) {
        lambda_c = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }
    double xM;
    itParam = mapParams.find(IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[X_min] + postfix);
    if (itParam != mapParams.end()) {
        xM = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    double k;
    itParam = mapParams.find(IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[K_] + postfix);
    if (itParam != mapParams.end()) {
        k = atof(itParam->second.c_str());
    }
    else {
        return NULL;
    }

    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* params = new IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    params->setPoissonLambda(lambda);
    params->setMRLambda(lambda_c);
    params->setParetoXm(xM);
    params->setParetoK(k);
    return params;
};

/*****************************************
*   IParamsUnion IMPLEMENTATION
*****************************************/
/*****************************************
*       IParamsUnion_Impl CTOR()
*****************************************/
IParamsUnion_Impl::IParamsUnion_Impl(const size_t totalOfItems, const size_t* sizesOfItems)
{
    _totalOfItems=0;
    _items=NULL;
    if((! totalOfItems) || (! sizesOfItems))
    {
        return;
    }
    reallocateItems(totalOfItems, sizesOfItems);
};
/*****************************************
*       IParamsUnion_Impl CTOR()
*****************************************/
IParamsUnion_Impl::IParamsUnion_Impl(const size_t totalOfItems, const IParams** items) : IParamsUnion()
{
    _totalOfItems=0;
    _items=NULL;
    if((! totalOfItems) || (! items))
    {
        return;
    }
    int rc = this->setItems(totalOfItems, items);
    if (_RC_SUCCESS_ != rc)
    {
        reallocatePtrToItems(0);
    }
    return;
};
/*****************************************
*       IParamsUnion_Impl DTOR()
*****************************************/
IParamsUnion_Impl::~IParamsUnion_Impl()
{
#ifdef _DEBUG_
std::cout << "into ~IParamsUnion_Impl :" <<  std::endl;
#endif
  for(size_t i=0; i<_totalOfItems; ++i)
  {
      delete _items[i];
      _items[i]=NULL;
  }
  delete [] _items;
  _items = NULL;
};

int IParamsUnion_Impl::reallocatePtrToItems(const size_t totalOfItems)
{
    if (NULL != this->_items)
    {
        for(size_t i=0; i< this->_totalOfItems; ++i)
        {
           if (NULL != _items[i])
               delete _items[i];
           _items[i]=NULL;
        }
    }
    delete [] _items;
    _items=NULL;
    _totalOfItems=0;

    if(! totalOfItems)
    {
        return _RC_SUCCESS_;
    }
    _items = new IParams*[totalOfItems];
    if (_items)
    {
        for (size_t i=0; i < totalOfItems; ++i)
        {
            _items[i]=NULL;
        }
        _totalOfItems = totalOfItems;
        return _RC_SUCCESS_;
    }
    return _ERROR_NO_ROOM_;
};

int IParamsUnion_Impl::reallocateItems(const size_t totalOfItems, const size_t* sizesOfItems)
{
    int rc = reallocatePtrToItems(totalOfItems);
    if (rc)
    {
        return rc;
    }
    for(size_t i=0; i<totalOfItems; ++i)
    {
/// Init _items!
        if (sizesOfItems[i])
        {
            /**
            Here we pass NULL because Params1D type already implemented
            So, NO ERROR expected.
            */
            _items[i]= IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, sizesOfItems[i]);
        }
        else
            return _ERROR_WRONG_ARGUMENT_;
    }
    return _RC_SUCCESS_;
};

int IParamsUnion_Impl::setItem(const size_t index, const IParams* itemParams)
{
    if(index >= _totalOfItems)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(!itemParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    delete _items[index];
    _items[index] = NULL;
    _items[index] = itemParams->clone();
    return _RC_SUCCESS_;
};

int IParamsUnion_Impl::setItems(const size_t size, const IParams** itemsParams)
{
    if(!itemsParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(size != _totalOfItems)
    {
        int rc = this->reallocatePtrToItems(size);
        if(rc != _RC_SUCCESS_)
        {
            return rc;
        }
    }
    for(size_t i=0; i<size; ++i)
    {
        if(itemsParams[i])
        {
            setItem(i, itemsParams[i]);
        }
        else
            return _ERROR_WRONG_ARGUMENT_;
    }
    return _RC_SUCCESS_;
};
/**
* to clone/copy of existing _items[index]
*/
int IParamsUnion_Impl::getItemCopy(const size_t index, IParams*& itemParams) const
{
    if(index >= _totalOfItems)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
/* the reference PROVE addendParams is NOT NULL
BUT VALUE of PTR may be NULL!
    if(!addendParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
*/
    itemParams = _items[index]->clone();
    return _RC_SUCCESS_;
};

IParams* IParamsUnion_Impl::ptrIParamsUnionToPtrIParams(const size_t index) const
{
    IParams * ptrIParams=NULL;
    if(index >= _totalOfItems)
    {
        return NULL;
    }
    ptrIParams = _items[index];

    return ptrIParams;
};
/**
* to clone/copy of existing _items
*/
int IParamsUnion_Impl::getItemsCopies(const size_t size, IParams**& itemsParams) const
{
    if(!itemsParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(size > _totalOfItems)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    for(size_t i=0; i<size; ++i)
    {
        /** to clone/copy of existing _items[i] */
        int rc = getItemCopy(i, itemsParams[i]);
        if (rc)
        {
            return rc;
        }
    }
    return _RC_SUCCESS_;
};
std::string IParamsUnion_Impl::toString() const
{
    std::stringstream ss;
    for(size_t i=0; i<_totalOfItems; ++i)
    {
           ss <<  _items[i]->toString();
           if(i < _totalOfItems-1)
           {
               ss << DELIMITER_TO_STRING_ITEMS;
           }
    }
      return ss.str();
};

int IParamsUnion_Impl::getTotalOfItemsInParamsUnion(size_t& size) const
{
    size = _totalOfItems;
    return _RC_SUCCESS_;
};

IParamsUnion_Impl* IParamsUnion_Impl::clone() const
{
    /** It's DEEP! Not Shallow copy */
     IParamsUnion_Impl* params = new IParamsUnion_Impl(*this);
     return params;
};
/***************************************************
* Public IParamsUnion_Impl Copy CTOR
****************************************************/
IParamsUnion_Impl::IParamsUnion_Impl(const IParamsUnion_Impl& paramsUnionImplSrc) : IParamsUnion()
{
    this->_totalOfItems = 0;
    this->_items        = NULL;
    size_t size;
    int rc = paramsUnionImplSrc.getTotalOfItemsInParamsUnion(size);

      /** deep copy */
    if (!size)
        return;// nullptr;

    rc = this->reallocatePtrToItems(size);
//    IParams* itemParams[size];
//    IParams** ptrItemParams = itemParams;
    IParams** ptrItemParams = new IParams*[size];
    /** to clone/copy existing _items */
    rc = paramsUnionImplSrc.getItemsCopies(size, ptrItemParams);
      if (rc)
    {
///TODO!!! HOW Log?
    }
    /** finalize deep copy */
    rc = this->setItems(size, (const IParams**)ptrItemParams);
///TODO!!! check that cloned copies deleted
    for(size_t i=0; i< size; ++i)
    {
        delete ptrItemParams[i];
        ptrItemParams[i] = NULL;
    }
    delete [] ptrItemParams;
      if (rc)
    {
///TODO!!! HOW Log?
    }
};
/***************************************************
* Public IParamsUnion_Impl Assignment
****************************************************/
IParamsUnion_Impl& IParamsUnion_Impl::operator=(const IParamsUnion_Impl& paramsUnionImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsUnionImplSrc)
        return *this;
    size_t size;
    int rc = paramsUnionImplSrc.getTotalOfItemsInParamsUnion(size);

      /** deep copy */
    if (!size)
        return *this;

    rc = this->reallocatePtrToItems(size);
    IParams** itemParams = new IParams*[size];
    /** the clones/copies of existing _items retrieved */
    rc = paramsUnionImplSrc.getItemsCopies(size, itemParams);
    /** finalize deep copy */
    rc = this->setItems(size, (const IParams**)itemParams);
    for(size_t i=0; i< size; ++i)
    {
        delete itemParams[i];
        itemParams[i] = NULL;
    }
    delete [] itemParams;
    itemParams = NULL;
      if (rc)
    {
///TODO!!! HOW Log?
    }
     return *this;
};
/***********************************************************************
* static public interface to make IParamsUnion* of Params1D
************************************************************************/
IParamsUnion* IParamsUnion::createParamsUnion(const size_t totalOfItems, const size_t* sizesOfItems, Logger * logger)
{
    return new IParamsUnion_Impl(totalOfItems, sizesOfItems);
};
/***********************************************************************
* static  public interface to make IParamsUnion* by cloning IParams** addends
************************************************************************/
IParamsUnion* IParamsUnion::createParamsUnion(const size_t totalOfItems, const IParams** items, Logger * logger)
{
    for(size_t i=0; i < totalOfItems; ++i)
    {
        if (items[i] == NULL)
        {
            logger->error(WORKFLOW_TASK_ERROR, i + "-th Params NOT FOUND in createParamsUnion() to create ParamsUnion!");
            return NULL;
        }
    }
    return new IParamsUnion_Impl(totalOfItems, items);
};
/*****************************************************************
* Static factory : Empty Params Interface Factory
*****************************************************************/
/*** NB size_t size is in use for Params1D ONLY!
*/
IParams* IParams::createEmptyParams(Logger* logger, enum IParams::ParamsTypes pt, size_t size)
{
    switch(pt)
    {
      case Params1D:
      {
        return new IParams_Impl(size);
      }
      case GaussianParams:
      {
        return IParamsGaussian_Impl::createEmptyParams();
      }
      case WienerParams:
      {
        return IParamsWiener_Impl::createEmptyParams();
      }
      case PoissonParams:
      {
        return IParamsPoisson_Impl::createEmptyParams();
      }
      case ParetoParams:
      {
        return IParamsPareto_Impl::createEmptyParams();
      }
      case PoissonDrvnParetoJumpsExpMeanRevertingParams:
      {
        return IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyParams();
      }
      default:
///TODO!!! log->error!
        logger->error(WORKFLOW_TASK_ERROR, pt + "-th Params type is UNKNOWN! Can not create Empty Params of that type in IParams::createEmptyParams()!");
        return NULL;
   };
};
/*****************************************************************
* Static factory : All Params Interface Factory
*****************************************************************/
IParams* IParams::parseAndCreateParams(enum IParams::ParamsTypes pt, std::map<std::string, std::string>& mapParams, std::string postfix, Logger* logger)
{
   switch(pt)
   {
     case Params1D:
     {
         return IParams_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case GaussianParams:
     {
         return IParamsGaussian_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case WienerParams:
     {
         return IParamsWiener_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case ParetoParams:
     {
         return IParamsPareto_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case PoissonDrvnParetoJumpsExpMeanRevertingParams:
     {
         return IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::parseAndCreateParams( mapParams, postfix);
     }
     default:
       logger->error(WORKFLOW_TASK_ERROR, pt + "-th Params type is UNKNOWN! Can not parse map and create Params of that type in IParams::parseAndCreateParams()!");
       return NULL;
   };
};

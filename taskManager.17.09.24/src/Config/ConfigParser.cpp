#include "../CommonStuff/Defines.h" /** delimiters macro */
#include "ConfigParser.h"

#include <iostream>
#include <sstream> /* stringstream */

ConfigParser::ConfigParser(std::string cfgName)
{
///TODO!!! call ifstream CTOR : config(cfgName.c_str()) throws SEGFAULT somewhere around std::string::size()!
    config.open(cfgName.c_str(), std::ios_base::in);
}

ConfigParser::~ConfigParser()
{
    if (config)
        config.close();
}

bool ConfigParser::fileExists()
{
    if (config && !config.bad()) {
        return true;
    }
    return false;
}

std::map<std::string, std::string> ConfigParser::parse()
{
    /** create map to populate with key=PARAM_NAME, value=PARAM_VALUE
    parsing cfg-file contents
    */
    std::map<std::string, std::string> params; //OLD = std::map<std::string, std::string>();
    /** default CTOR ="" */
    std::string str, name, value;
    std::string delimiters = DEFAULT_DELIMITERS;
    while (!config.eof()) {
        /** replaced
         onfig >> str;
        with
        std::getline();
        to retrieve file contents str by str
        */
        str.clear();
        name.clear();
        value.clear();
        std::getline(config, str);
        /** remove leading/trailing delimiters from str */
        TrimStr(str,delimiters);
        /** remove COMMENTs from str */
        str = removeComment(str, DELIMITER_COMMENT_STARTS_WITH);
        /** if current str ends with '\\'
        we parse MULTI-LINE value
        */
        if((str.size() > 0) && (str[str.size()-1] == DELIMITER_VALUE_CONTINUED_IN_NEXT_LINE ))
        {
            while(str[str.size()-1] == DELIMITER_VALUE_CONTINUED_IN_NEXT_LINE)
            {
                /** get next part of MULTI-LINE value into nextStr
                 (default CTOR ="")
                */
                std::string nextStr;
                std::getline(config, nextStr);
                TrimStr(nextStr,delimiters);
                /** if next str is empty -
                we have got current MULTI-LINE config directive!
                Break!
                */
                if(nextStr.empty())
                {
                    str = str.substr(0, str.size()-1);
                    break;
                }
                nextStr = removeComment(nextStr, DELIMITER_COMMENT_STARTS_WITH);
                /** if comment inside input dataset definition -
                get next string from config file
                */
                if(nextStr.empty()) continue;
                /** remove current '\\' */
                str = str.substr(0, str.size()-1);
                TrimStr(str,delimiters);
                /** if next string starts with '\\'
                 we faced cfg-syntax error!
                 Skip next lines of MULTI-LINE value
                */
                if(nextStr[0] == DELIMITER_VALUE_CONTINUED_IN_NEXT_LINE)
                {
                    break;
                }
                else
                {
                    str += nextStr;
                }
                /** after nextStr has been added
                check for next '\\'
                which may come at the end of tmpStr
                */
            };
        }
        /** split str into name-value pair by '=' - delimiter */
        size_t pos = str.find(DELIMITER_NAME_VALUE);
        if (pos == std::string::npos) {
            continue;
        }
        name = str.substr(0, pos);
        TrimStr(name,delimiters);
        value = str.substr(pos + 1);
        TrimStr(value,delimiters);
        /** put name-value into map */
        if ((!name.empty()) && (!value.empty())) {
            params[name] = value;
        }
    }
    return params;
}

std::string ConfigParser::setTrackNumToFileName(std::string fName, int trackId, int trackNum)
{
	/*
    if (trackNum == 1) {
        return fName;
    }
	*/
    std::string fNameNoExt, extension;
    unsigned pos = fName.find(DELIMITER_FILENAME_FILEEXT);

    if (pos != std::string::npos)
	{
        fNameNoExt = fName.substr(0, pos);
        extension = fName.substr(pos);
    }
	else
	{
		fNameNoExt = fName;
	}
    std::stringstream newFNameStr;
    newFNameStr << fNameNoExt << "_" << trackId << extension;
    return newFNameStr.str();
}

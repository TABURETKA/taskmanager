#ifndef __SIMPLE_DATA_GENERATOR_H__633569422542968750
#define __SIMPLE_DATA_GENERATOR_H__633569422542968750
#include <string> /* std::string */
#include <map>    /* std::map */
#include "../Params/Params.h" /* IParams */
#include "../Models/Model.h" /* IModels, I_Probabilistic_Model */

#define ptrInt int*

/** forward declarations */
class Task;
class Logger;

/** NON-anonymous namespace to expose interface for corresponding cpp-file */
namespace DataGenerators
{

    enum DataGeneratorsTypes
    {
        SIMPLE_GENERATOR,
        FAKE_GENERATOR
    };

/** static in namespace DataGenerators */
    static const char * generatorsNames[FAKE_GENERATOR]={
        "SIMPLE_GENERATOR"
    };

static DataGeneratorsTypes getDataGeneratorTypeByGeneratorName(const std::string& generatorName);

/** abstract Base class - interface */
/** implementation of basic methods
 The methods setTask, setLog, toString
 have to be inherited all DataGenerators
*/
class IDataGenerator
{
public:
    enum DataGeneratorConfigItems
    {
        MODEL_TYPE_ITEM, //SIMPLE ONE, ADDITIVE_MODEL
        MODEL_NAME_ITEM, // PARETO, ...
        MODEL_PARAMS_SRC_ITEM, // Params to create model instance itself
        ADDITIVE_MODEL_PARAMS_SRC_ITEM, // Params to create model instance itself
        PARAMS_TO_GENERATE_SAMPLE_ITEM, // Model Params to generate sample
        SAMPLE_SIZE_ITEM,
        DATASET_OUTPUT_FILENAME_ITEM,
        FAKE_DATAGENERATOR_CONFIG_ITEM
    };
    /** tokens to parse DataGenerator Config map */
/* OLD C++ < C++11    static const char* grammarToParseConfig[FAKE_DATAGENERATOR_CONFIG_ITEM]; */
    static constexpr const char* grammarToParseConfig[FAKE_DATAGENERATOR_CONFIG_ITEM]=
    {
        "MODEL_TYPE", //SIMPLE ONE, ADDITIVE_MODEL
        "MODEL_NAME", // PARETO, ...
        "MODEL_PARAMS_SRC",  // Params to create model instance itself
        "ADDITIVE_MODEL_PARAMS_SRC",
        "PARAMS_TO_GENERATE_SAMPLE", // Model Params to generate sample
        "SAMPLE_SIZE_ITEM",
        "DATASET_OUTPUT_FILENAME"
    };
/** � ����� ������ ������ - factory,
 ����� ������, ����� ������� ��������� ����������.
 ���� ������-��������� ����� ������� �����, � ���������� IDataGenerator,
 ����� �� ������ �������������� �������� ��������
 � ������ ���������� ���������-������ ����� ���� ������ ���� ������,
 ����� ����� ����� �������������� ���������� ��� ���������
 ����������� ������� ����������.
*/
    /** log is mandatory! Any DataGenerator MUST HAVE ptr to Logger */
    static IDataGenerator* createDataGenerator(const std::string& generatorName, Logger*& log);

    IDataGenerator(Logger*& log){};
    virtual ~IDataGenerator(){};
    /** Common stuff */
    virtual void setTask(Task* const & task) =0;
    virtual void setLog(Logger* const & log) =0;

    virtual std::string toString() const =0;

    virtual int parseAndCreateGeneratorParams(std::map<std::string, std::string>& mapParams) =0;
///TODO!!!    virtual int setGeneratorParams(IParams*& params);
///TODO!!! ����� ��������� ��� cfg-params � ������� ���� ������ ���!  virtual int setModel(I_Probabilistic_Model* probModel);
    virtual int generate(ptrInt threadInterruptSignal) = 0;

private:
    /** assign and copy CTORs are disabled */
    IDataGenerator(const IDataGenerator&);
    void operator=(const IDataGenerator&);
};
} /// end namespace DataGenerators
#endif

#define _BOUNDARIES_TEST_

//OLD  #include <stdlib.h> /* atoi */
#include "../CommonStuff/Defines.h" /** delimiters */
#include "../Log/Log.h"
#include "../Log/errors.h"
#include "../Config/ConfigParser.h" /* ConfigParser */
#include "../Models/AdditiveModel.h" /* AdditiveModel */
#include "DataGenerators.h"
#include <sstream> /* stringstream */


/** static in DataGenerators namespace */
DataGenerators::DataGeneratorsTypes DataGenerators::getDataGeneratorTypeByGeneratorName(const std::string& generatorName)
{
    for(size_t i=0; i < DataGenerators::DataGeneratorsTypes::FAKE_GENERATOR; ++i)
    {
        if (generatorName == DataGenerators::generatorsNames[i])
        {
            return static_cast<enum DataGeneratorsTypes>(i);
        }
    }
    return FAKE_GENERATOR;
}

/** tokens to parse DataGenerator Config map */
/** static in IDataGenerator */
constexpr const char* DataGenerators::IDataGenerator::grammarToParseConfig[FAKE_DATAGENERATOR_CONFIG_ITEM];


/***********************************************
* Base abstract IDataGenerator class implementation
************************************************/
/** anonymous namespace to hide private implementations into this cpp-file */
namespace
{
/** implementation of abstract Base class class IDataGenerator */
class IDataGenerator_Impl : public virtual DataGenerators::IDataGenerator
{
public:
    IDataGenerator_Impl(Logger*& log);
    virtual ~IDataGenerator_Impl();
    /** Common stuff */
    virtual void setTask(Task* const & task);
    virtual void setLog(Logger* const & log);
    virtual std::string toString() const;

    virtual int parseAndCreateGeneratorParams(std::map<std::string, std::string>& mapParams);
///TODO!!!    virtual int setGeneratorParams(IParams*& params);
///TODO!!! ����� ��������� ��� cfg-params � ������� ���� ������ ���!  virtual int setModel(I_Probabilistic_Model* probModel);
    virtual int generate(ptrInt threadInterruptSignal);

protected:
    int logErrMsgAndReturn(int rc, std::string msg) const;
    virtual int parsingFailed(std::string msg) const;
    enum DataGenerators::DataGeneratorsTypes m_dgt;
    Task* m_task;
    Logger* m_logger;
private:
    /** assign and copy CTORs are disabled */
    IDataGenerator_Impl(const IDataGenerator_Impl&);
    void operator=(const IDataGenerator_Impl&);
    I_Probabilistic_Model* m_ptrProbModel;
    /** here we get samples, which are produced by generator */
    IParamsUnion* m_modelParamsUnion;
    double * m_data;
    std::string m_filenameToSaveSample;
    size_t m_sampleSize;
    IParamsUnion* m_pointEstimatesOfParams;
};///end IDataGenerator_Impl
}///end anonymous namespace

/******************** IMPLEMENTATIONs ****************************/
/****************************************************************
*   Base class - implementation
*****************************************************************/
/****************************************************************
*             CTOR
****************************************************************/
IDataGenerator_Impl::IDataGenerator_Impl(Logger*& log) : DataGenerators::IDataGenerator(log)
{
    /** so far noting to allocate */
    m_dgt              = DataGenerators::DataGeneratorsTypes::SIMPLE_GENERATOR;
    m_task             = NULL;
    m_logger           = log;
    m_ptrProbModel     = NULL;
    m_modelParamsUnion = NULL;
    m_sampleSize       = 0;
    m_data             = NULL;
    m_pointEstimatesOfParams = NULL;
}
/****************************************************************
*             DTOR
****************************************************************/
IDataGenerator_Impl::~IDataGenerator_Impl()
{
    delete m_ptrProbModel;
    /** here we get samples, which are produced by generator */
    delete m_modelParamsUnion;

///TODO!!! ugly output implementation of generated sample
    if(m_data)
    {
        std::ofstream dataFile(m_filenameToSaveSample.c_str(), std::ios::out);
        if (dataFile)
        {
            /**
              first line MUST keep total size!
            */
            dataFile << m_sampleSize << std::endl;
            for(size_t i=0; i < m_sampleSize; ++i)
            {
                dataFile << m_data[i] << std::endl;
            }
        }
        dataFile.close();
    }
    delete m_data;
///TODO!!! ugly output implementation of estimates retrieved for generated sample
    if(m_pointEstimatesOfParams)
    {
        std::string estimatesFilename = m_filenameToSaveSample + ".estimates";
        std::ofstream estimatesFile(estimatesFilename.c_str(), std::ios_base::out);
        if (estimatesFile)
        {
            estimatesFile << "SAMPLE_SIZE" <<  DELIMITER_NAME_VALUE << m_sampleSize  << DELIMITER_TO_STRING_ITEMS;
            estimatesFile << m_pointEstimatesOfParams->toString() << std::endl;
        }
        estimatesFile.close();
    }
#ifdef _BOUNDARIES_TEST_
std::string boundaries = "boundaries";
    if(m_pointEstimatesOfParams)
    {
        std::ofstream estimatesFile(boundaries.c_str(), std::ios_base::app);
        if (estimatesFile)
        {
            estimatesFile << "SAMPLE_SIZE" <<  DELIMITER_NAME_VALUE << m_sampleSize  << DELIMITER_TO_STRING_ITEMS;
            estimatesFile << m_pointEstimatesOfParams->toString() << std::endl;
        }
        estimatesFile.close();
    }
#endif
    delete m_pointEstimatesOfParams;
}

void IDataGenerator_Impl::setTask(Task* const & task)
{
    m_task = task;
}

void IDataGenerator_Impl::setLog(Logger* const & log)
{
    m_logger = log;
}

int IDataGenerator_Impl::logErrMsgAndReturn(int rc, std::string msg) const
{
    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(DataGenerators::generatorsNames[m_dgt]) + ": " + msg);
    return _ERROR_WRONG_WORKFLOW_;
}

std::string IDataGenerator_Impl::toString() const
{
    return (std::string) DataGenerators::generatorsNames[m_dgt];
}

/** ����� ������, ����������� ��� ������� ���������! */
/** SimpleGenerator stuff */
int IDataGenerator_Impl::parseAndCreateGeneratorParams(std::map<std::string, std::string>& paramsMap)
{
///TODO!!! ���� ���������� ���� ��� �� ������ Task::configTask()
    /** Do Model initialization stuff */
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
    auto itParam = paramsMap.find(DataGenerators::IDataGenerator::grammarToParseConfig[MODEL_TYPE_ITEM]);
    if (itParam != paramsMap.end())
    {
        mt = IModel::getModelTypeByModelName( (char const * const) itParam->second.c_str());
        if (mt == IModel::ModelsTypes::FAKE_MODEL)
        {
            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong model type provided for Generator : " + itParam->second);
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** Do Plain Model initialization stuff */
///TODO!!! ������, ��� ���� ����� �������� ����������� ��� ���������� ������� � �����������, ������� � ��� ��������
        if (mt != IModel::ModelsTypes::ADDITIVE_MODEL)
        {
///TODO!!! �������� ���, ������� ������ ��� ��������� ESTIMATOR_PARAMS_SRC
            /**
            ������, ��� ��������� ������,
            ����� ����������� ������� �� ���������,
            � ����� ��� ��������� ������ � ���������� �� ����������!
            */
            IParams* ptrToPtrIParams=NULL;
            itParam = paramsMap.find(DataGenerators::IDataGenerator::grammarToParseConfig[MODEL_PARAMS_SRC_ITEM]);
            if (itParam != paramsMap.end())
            {
                /** here we call Config in order to parse ModelParams.cfg (if exists)
                and make map
                */
                std::string modelParamsCfgFileName = itParam->second;
                ConfigParser modelParamsParser(modelParamsCfgFileName);
                /** We don't kill current task if config for model does not exist
                */
                if (modelParamsParser.fileExists())
                {
                    auto mapModelParams = modelParamsParser.parse();
                    /** create HARD-CODED empty ParamsSet for ModelParams */
                    /** params input from file -
                    fill map with IModel Params
                    */
                    std::string postfix = "";
                    /** ���������� ������ ����� ���� IParams */
                    int rc = IModel::parseAndCreateModelParams(mt, mapModelParams, postfix, &ptrToPtrIParams, m_logger);
                    if(rc)
                    {
                        delete ptrToPtrIParams;
                        m_logger->error(WORKFLOW_TASK_ERROR, "Wrong map of model params provided when estimator " + this->toString() + " is initialized.");
                        return _ERROR_WRONG_ARGUMENT_;
                    }
                }
            }///end if(MODEL_PARAMS_SRC)
            /** Now then we know model type and params stuff to create it
            let's create instance of NOT Additive Model
            */
            IModel * model = NULL;
            if(ptrToPtrIParams)
            {
///TODO!!! Here we MUST IMPLEMENT shared_ptr<IModel *> or wrap IModel* model with class-handler!
                model = IModel::createModel(mt,ptrToPtrIParams);
                delete ptrToPtrIParams;
            }
            else
            {
///TODO!!! Here we MUST IMPLEMENT shared_ptr<IModel *> or wrap IModel* model with class-handler!
                model = IModel::createModel(mt);
            }
            if(! model)
            {
                m_logger->error(WORKFLOW_TASK_ERROR, "Can not create instance of specified model type :" + itParam->second);
                ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
                return _ERROR_NO_MODEL_;
            }
            m_ptrProbModel = dynamic_cast<I_Probabilistic_Model *> (model);
            if(NULL == m_ptrProbModel)
            {
                delete model;
                m_logger->error(WORKFLOW_TASK_ERROR, "Can not create instance of specified model type :" + itParam->second);
                ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
                return _ERROR_NO_MODEL_;
            }
        } ///end if(mt != ADDITIVE_MODEL)
        else ///TODO!!! "�������� ��� ��� �������� ���������� ADDITIVE_MODEL
            /** Do Additive Model initialization stuff */
            if (mt == IModel::ModelsTypes::ADDITIVE_MODEL)
            {
                itParam = paramsMap.find(grammarToParseConfig[MODEL_NAME_ITEM]);
                if (itParam == paramsMap.end())
                {
                    m_logger->error(WORKFLOW_TASK_ERROR, "Additive model name NOT FOUND in Generator cfg-file");
                    ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
                    return _ERROR_WRONG_ARGUMENT_;
                }
                const enum AdditiveModelsTypes amt = getAdditiveModelsTypeByAdditiveModelName( (const std::string&) itParam->second);
                if (amt == FAKE_ADDITIVE_MODEL)
                {
                    m_logger->error(WORKFLOW_TASK_ERROR, "Wrong additive model type provided for Generator : " + itParam->second);
                    return _ERROR_WRONG_ARGUMENT_;
                }
///TODO!!! SPECIAL stuff to parse additive model config
                itParam = paramsMap.find(grammarToParseConfig[ADDITIVE_MODEL_PARAMS_SRC_ITEM]);
                if (itParam != paramsMap.end())
                {
                    /** here we call Config in order to parse AdditiveModelCfg.cfg (if exists)
                    and make map
                    */
                    std::string additiveModelParamsCfgFileName = itParam->second;
                    ConfigParser modelParamsParser(additiveModelParamsCfgFileName);
                    /** We don't kill current task if config for model does not exist
                    */
                    if (modelParamsParser.fileExists())
                    {
                        auto mapModelParams = modelParamsParser.parse();
                        iteratorParamsMap tmpItParam = mapModelParams.find(IAdditiveModel::grammar[TotalAddendsOfAdditiveModel]);
                        if (tmpItParam == mapModelParams.end())
                        {
                            m_logger->error(WORKFLOW_TASK_ERROR, "Total of addends NOT FOUND in : " + additiveModelParamsCfgFileName + " for additive model type provided for Generator : " + itParam->second);
                            return _ERROR_WRONG_ARGUMENT_;
                        }
                        int tmpInt = -1;
                        /** we are pessimists */
                        int rc = _ERROR_NO_INPUT_DATA_;
                        rc = strTo<int>(tmpItParam->second.c_str(), tmpInt); //OLD          float tmpInt = atoi(tmpItParam->second.c_str());
                        mapModelParams.erase(tmpItParam);

                        if((rc != _RC_SUCCESS_) || (tmpInt <= 0))
                        {
                            m_logger->error(WORKFLOW_TASK_ERROR, "Total of addends WRONG in : " + additiveModelParamsCfgFileName + " for additive model type provided for Generator : " + itParam->second);
                            return _ERROR_WRONG_ARGUMENT_;
                        }
                        size_t totalAddendsOfAdditiveModel = (size_t)tmpInt;
                        /** create HARD-CODED empty ParamsSet for ModelParams */
                        /** params input from file -
                        fill map with IModel Params
                        */
///TODO!!! Here we MUST IMPLEMENT shared_ptr<IAdditiveModel *> or wrap IAdditiveModel* _additiveModel with class-handler!
                        IAdditiveModel* additiveModel = IAdditiveModel::parseAndCreateModel(amt, totalAddendsOfAdditiveModel, mapModelParams, m_logger);
                        if(NULL == additiveModel)
                        {
                            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong map of additive model params provided in " + additiveModelParamsCfgFileName);
                            return _ERROR_NO_MODEL_;
                        }
                        m_ptrProbModel = dynamic_cast<I_Probabilistic_Model*>(additiveModel);
                        if(NULL == m_ptrProbModel)
                        {
                            delete additiveModel;
                            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong map of additive model params provided in " + additiveModelParamsCfgFileName);
                            return _ERROR_NO_MODEL_;
                        }
                    }
                }///end if(ADDITIVE MODEL_PARAMS_SRC)
                else /** if( NOT FOUND ADDITIVE MODEL_PARAMS_SRC) */
                {
                    m_logger->error(WORKFLOW_TASK_ERROR, "Additive model params cfg-file NOT specified in DataGenerator's cfg-file.");
                }
                if(! m_ptrProbModel)
                {
                    m_logger->error(WORKFLOW_TASK_ERROR, "Can not create instance of specified additive model type :" + std::string(IAdditiveModel::additiveModelsNames[amt]) + " because it's cfg-file has NOT beem PARSED!");
                    ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
                    return _ERROR_NO_MODEL_;
                }
            }///emd if ADDITIVE_MODEL
    } ///end NODEL_TYPE stuff

///TODO!!! ���������� MODEL_PARAMS_SRC
    itParam = paramsMap.find(DataGenerators::IDataGenerator::grammarToParseConfig[PARAMS_TO_GENERATE_SAMPLE_ITEM]);
    if (itParam == paramsMap.end())
    {
        delete m_ptrProbModel; m_ptrProbModel=NULL;
        m_logger->error(WORKFLOW_TASK_ERROR, "model params NOT FOUND in cfg-file to generate sample. " + this->toString() + " is not initialized.");
        return _ERROR_WRONG_ARGUMENT_;
    }///end if(! PARAMS_TO_GENERATE_SAMPLE)
    /** here we call Config in order to parse ModelParams.cfg (if exists)
    and make map
    */
    std::string modelParamsCfgFileName = itParam->second;
    ConfigParser modelParamsParser(modelParamsCfgFileName);
    /** We don't kill current task if config for model does not exist
    */
    if (! modelParamsParser.fileExists())
    {
        delete m_ptrProbModel; m_ptrProbModel=NULL;
        m_logger->error(WORKFLOW_TASK_ERROR, "Wrong map of model params provided to generate sample. " + this->toString() + " is not initialized.");
        return _ERROR_WRONG_ARGUMENT_;
    }
    auto mapModelParams = modelParamsParser.parse();
    /** create HARD-CODED empty ParamsSet for ModelParams */
    /** params input from file -
    fill map with IModel Params
    */

    /** Let's get model params to generate sample */
    if (mt == IModel::ModelsTypes::ADDITIVE_MODEL)
    {
        IAdditiveModel* ptrAD = dynamic_cast<IAdditiveModel*> (m_ptrProbModel);
        if(! ptrAD)
        {
            delete m_ptrProbModel; m_ptrProbModel=NULL;
            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong additive model params provided to generate sample. " + this->toString() + " is not initialized.");
            return _ERROR_WRONG_MODEL_PARAMS_;
        }
        size_t totalOfAddends = 0;
        ptrAD->getTotalOfAddends(totalOfAddends);
        enum IModel::ModelsTypes* addendModelsTypes = new enum IModel::ModelsTypes[totalOfAddends];
        if(! addendModelsTypes)
        {
            delete m_ptrProbModel; m_ptrProbModel=NULL;
            m_logger->error(WORKFLOW_TASK_ERROR, " NO ROOM to create additive model params provided to generate sample. " + this->toString() + " is not initialized.");
            return _ERROR_WRONG_MODEL_PARAMS_;
        }
        ptrAD->getAddendsTypes(totalOfAddends, addendModelsTypes);

        IParamsUnion* ptrToIParamsUnion = NULL;
        int rc = IAdditiveModel::parseAndCreateModelParamsUnion(static_cast<size_t> (totalOfAddends), (const enum IModel::ModelsTypes*&) addendModelsTypes, mapModelParams, std::string(""), &ptrToIParamsUnion, m_logger);
        delete [] addendModelsTypes;

        if(rc != _RC_SUCCESS_)
        {
            delete m_ptrProbModel; m_ptrProbModel=NULL;
            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong model params provided to generate sample. " + this->toString() + " is not initialized.");
            return _ERROR_WRONG_MODEL_PARAMS_;
        }
        m_modelParamsUnion = ptrToIParamsUnion;

    }
    else /** NOT IModel::ModelsTypes::ADDITIVE_MODEL */
    {
        IParams* ptrToParams = NULL;
        int rc = IModel::parseAndCreateModelParams((const enum IModel::ModelsTypes) mt, mapModelParams, std::string(""), &ptrToParams, m_logger);
        if(rc != _RC_SUCCESS_)
        {
            delete m_ptrProbModel; m_ptrProbModel=NULL;
            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong model params provided to generate sample. " + this->toString() + " is not initialized.");
            return _ERROR_WRONG_MODEL_PARAMS_;
        }
        m_modelParamsUnion = IParamsUnion::createParamsUnion(1, (const IParams**) &ptrToParams, m_logger);
        delete ptrToParams;
        if(! m_modelParamsUnion)
        {
            delete m_ptrProbModel; m_ptrProbModel=NULL;
            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong model params provided to generate sample. " + this->toString() + " is not initialized.");
            return _ERROR_WRONG_MODEL_PARAMS_;
        }
    }
    m_pointEstimatesOfParams = m_modelParamsUnion->clone();

    itParam = paramsMap.find(DataGenerators::IDataGenerator::grammarToParseConfig[DATASET_OUTPUT_FILENAME_ITEM]);
    if (itParam == paramsMap.end())
    {
        delete m_ptrProbModel; m_ptrProbModel=NULL;
        delete m_modelParamsUnion;
        m_logger->error(WORKFLOW_TASK_ERROR, "OUTPUT_FILENAME NOT FOUND in cfg-file to generate sample. " + this->toString() + " is not initialized.");
        return _ERROR_WRONG_ARGUMENT_;
    }///end if(! DATASET_OUTPUT_FILENAME)

    m_filenameToSaveSample = itParam->second;

    itParam = paramsMap.find(DataGenerators::IDataGenerator::grammarToParseConfig[SAMPLE_SIZE_ITEM]);
    if (itParam == paramsMap.end())
    {
        delete m_ptrProbModel; m_ptrProbModel=NULL;
        delete m_modelParamsUnion;
        m_logger->error(WORKFLOW_TASK_ERROR, "SAMPLE_SIZE NOT FOUND in cfg-file to generate sample. " + this->toString() + " is not initialized.");
        return _ERROR_WRONG_ARGUMENT_;
    }///end if(! SAMPLE_SIZE)
    int tmpInt = -1;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    rc = strTo<int>(itParam->second.c_str(), tmpInt); //OLD          float int tmpInt = atoi(itParam->second.c_str());
    paramsMap.erase(itParam);

    if((rc != _RC_SUCCESS_) || (tmpInt <= 0))
    {
        delete m_ptrProbModel; m_ptrProbModel=NULL;
        delete m_modelParamsUnion;
        m_logger->error(WORKFLOW_TASK_ERROR, "WRONG SAMPLE_SIZE in " + this->toString()  + " Generator : " + itParam->second);
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_sampleSize = static_cast<size_t>(tmpInt);

    return _RC_SUCCESS_;
}
/*
int DataGenerators::IDataGenerator_Impl::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int DataGenerators::IDataGenerator_Impl::setEstimatorParams(IParams*& params)
{
///TODO!!!
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
*/
/*OLD
int IDataGenerator_Impl::setModel(I_Probabilistic_Model * probModel)
{
///TODO!!!
    if(NULL == probModel)
    {
        std::stringstream ss;
        ss << "NULL is NOT valid ptr to Probabilistic Model to  setModel in ";
        ss << generatorsNames[_dgt];
        return logErrMsgAndReturn(_ERROR_WRONG_ARGUMENT_, ss.str());
    }
    _ptrProbModel = probModel;
    return _RC_SUCCESS_;
}
*/
///TODO!!! ��� ���� � setData()???
/** No need so far
int DataGenerators::IDataGenerator_Impl::setParamsCompact(IParamsCompact * pst)
{
///TODO!!!
    m_logger->log(ALGORITHM_1_LEVEL_INFO, "IDataGenerator_Impl::setParamsCompact is not implemented yet!");
}

int DataGenerators::IDataGenerator_Impl::setParamsSet()
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setParamsSet is not implemented yet!");
}
*/
int IDataGenerator_Impl::generate(ptrInt threadInterruptSignal)
{
///TODO!!! ???
    if ((NULL == m_ptrProbModel) || (NULL == m_modelParamsUnion) || (0 == m_sampleSize))
        return _ERROR_WRONG_WORKFLOW_;

    m_data = new double[m_sampleSize];
    if(!m_data)
    {
        return _ERROR_NO_ROOM_;
    }
    std::memset(m_data, 0, m_sampleSize*sizeof(double));
    int indexOfParams = 0;
    if(NULL != dynamic_cast<IAdditiveModel*>(m_ptrProbModel))
    {
        /** -1 MEANS ALL adends MUST generate their samples */
        indexOfParams = I_Probabilistic_Model::GENERATE_AND_SUM_UP_SAMPLES_FOR_ALL_ADDENDS_OF_ADDITIVE_MODEL;
    }
    int rc = m_ptrProbModel->generateSample(indexOfParams, m_modelParamsUnion, m_data, m_sampleSize, m_pointEstimatesOfParams, NULL);
    if(rc != _RC_SUCCESS_)
    {
        delete [] m_data;
        return rc;
    }
    return _RC_SUCCESS_;
}

int IDataGenerator_Impl::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}
///////////////////////////////
/**
*  static method to create instances of ALL generators (factory)
*/
DataGenerators::IDataGenerator* DataGenerators::IDataGenerator::createDataGenerator(const std::string& generatorName, Logger*& log)
{
    /** generatorName -> DataGeneratorsTypes */
    enum DataGeneratorsTypes generatorType = getDataGeneratorTypeByGeneratorName(generatorName);
    switch (generatorType)
    {
    case SIMPLE_GENERATOR:
    {
        return new IDataGenerator_Impl(log);
    }
    default:
        return NULL;
    }
}

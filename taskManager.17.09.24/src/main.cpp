#include "TaskManager/TaskManager.h"
#include <iostream>
#include <string>
#include <vector>

#ifdef _DEBUG
#include <crtdbg.h>
#define _CRTDBG_MAP_ALLOC
#endif

/** if VSXXXX include Visual Leak Detection library
*/
#if defined(_MSC_VER) && (_MSC_VER >= 1600)
#include <vld.h>
#endif

#include "Params/Params.h" //IParams
#include "Params/ParamsSet.h" //IParamsSet
#include "Params/ParamsCompact.h" //IParamsCompact
#include "Models/Model.h" //IModel
#include "Models/AdditiveModel.h" //IAdditiveModel
/** IAA: to change current directory (cross-platform solutions ONLY!) */

#ifdef _WIN32
#include <direct.h>
// MSDN recommends against using getcwd & chdir names
#define GET_CWD _getcwd
#define CD _chdir
#else /** POSIX-standard */
#include "unistd.h"
#define GET_CWD getcwd
#define CD chdir
#endif

#include "Distributions/Distributions.h"

int main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cout << "To change path to ini-file run as:" << argv[0] << " path_to_dir_file_ini" << std::endl;
    }
    else
    {
        if (CD(argv[1]) == 0) {
            char buf[4086] = {};
            std::cout << "Current directory has been changed to =>" << GET_CWD(buf, sizeof(buf)) << std::endl;
        }
        else {
            std::cout << "Error while changing current directory! Will try to use default ./" << std::endl;
        }
    }

    TaskManager* taskManager = new TaskManager();

    taskManager->run();

    delete taskManager;


    Distributions::FreeGenerator();

    return 0;
}

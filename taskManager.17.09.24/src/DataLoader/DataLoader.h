#ifndef __TRACK_LOADER_H__633569422542968750
#define __TRACK_LOADER_H__633569422542968750

#include <string>
#include <vector>
#include <map>

#define ptrInt int*

#include "../Log/Log.h"

//OLD const size_t grammarSizeForDataLoader = 2;
/** tokens to parse config */
//OLD extern const std::string grammarDataLoader[grammarSizeForDataLoader];

// DON'T pollute global scope! using namespace std;
/** forward declaration */
class DataManager;
/** forward declarations */
typedef struct tagDataInfo DataInfo;

class DataLoader
{
public:
    enum grammarDataLoaderConfigItems{
        DATA_SET_TYPE_ITEM, //  "SINGLE",  "MULTIPLE",
        DATA_SET_FILENAME_ITEM,
        FAKE_DATA_LOADER_CONFIG_ITEM
    };
    /** tokens to parse DataLoaderConfig map */
/* OLD C++ < C++11    static const char* grammarDataLoaderConfig[FAKE_DATA_LOADER_CONFIG_ITEM]; */
    static constexpr const char* grammarDataLoaderConfig[FAKE_DATA_LOADER_CONFIG_ITEM]=
    {
        "DATA_SET_TYPE_ITEM", //  "SINGLE",  "MULTIPLE",
        "DATA_SET_FILENAME_ITEM"
    };

    enum Mode{
        SINGLE_TRACK,
        MULTIPLE_TRACK,
        FAKE_DATALOADER_MODE
    };

    static constexpr const char* grammarDataLoaderMode[FAKE_DATALOADER_MODE] =
    {
        "SINGLE",
        "MULTIPLE"
    };

    DataLoader(Logger*& m_logger);
    virtual ~DataLoader();
    static int loadSingleTrack(const std::string & fName, std::vector<double> & track, Logger* log);
    static int loadMultipleTrack(const std::string & fName, std::vector<std::vector<double> > & track, std::vector<std::string> &file_names, Logger* log);
    static int loadFromDB(const std::string & fName, std::vector<double> & track, Logger* log);

    virtual int parseAndCreateParams(std::map<std::string, std::string>& mapParams);
    virtual int run(ptrInt threadInterruptSignal);
    virtual int getDataInfoArray(size_t & dimension, DataInfo** & ptrToPtrDI) const;

private:
    /** assign and copy CTORs are disabled */
    DataLoader(const DataLoader&);
    void operator=(const DataLoader&);

    static bool isValidSizeOfTrack(double tmpDbl);

    Logger* m_logger;

    enum Mode m_mode;
    std::string m_trackNamesFile;
    std::vector<std::string> m_file_names;
    std::vector<std::vector<double> > m_tracks;
};
#endif

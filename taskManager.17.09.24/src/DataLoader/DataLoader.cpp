#include "DataLoader.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <conio.h>
#include <stdlib.h>
#include <math.h> /* floor */
#include <float.h> /* DBL_MIN */
#include "../CommonStuff/Defines.h" /* DEFAULT_DELIMITER */
#include "../Log/errors.h" /* error macros */
#include "../StringStuff/StringStuff.h" //TrimStr(), etc

///����� ����� ������������ saveResults(DataManager*)
#include "../DataCode/DataManager.h"
/** tokens to parse DataLoaderConfig map */
constexpr const char* DataLoader::grammarDataLoaderConfig[FAKE_DATA_LOADER_CONFIG_ITEM];

constexpr const char* DataLoader::grammarDataLoaderMode[FAKE_DATALOADER_MODE];

bool DataLoader::isValidSizeOfTrack(double tmpDbl)
{
    if(tmpDbl < 0.0f)
        return false;
    if(tmpDbl - floor(tmpDbl) > DBL_MIN)
        return false;
    return true;
}
int DataLoader::loadSingleTrack(const std::string & fName, std::vector<double> & track, Logger* log)
{
/*OLD
    std::ifstream srcFile(fName.c_str());
    if (!srcFile || srcFile.fail()) {
        log->error(WORKFLOW_TASK_ERROR, "No track file \"" + fName + "\" exists");
        return false;
    }

    std::string sParam("");
    double params;
    track.clear();
    while (!srcFile.eof())
    {
        sParam = "";
///TODO!!! >> with srcFile.getline
        srcFile >> sParam;
        if (sParam.empty())
            continue;
        params = atof(sParam.c_str());
///TODO!!! ������������� ��� : "Overflow" <- !
        track.push_back(params);
    }
    srcFile.close();

    std::stringstream strLog;
    strLog << "Load data, file: \"" << fName << "\", size: " << track.size();
    log->log(WORKFLOW_TASK_NOTIFICATION, strLog.str());
*/
    /**
    try to open file with with current track file name
    */
    std::ifstream srcFile(fName.c_str());
    if (srcFile.fail())
    {
        int c = srcFile.peek();  // peek character
        if ( c == EOF )
        {
            log->warning(WORKFLOW_TASK_WARNING, "Track file \"" + fName + "\" is empty!");
            return _ERROR_WRONG_ARGUMENT_;
        }
        log->warning(WORKFLOW_TASK_WARNING, "No track file \"" + fName);
        return _ERROR_WRONG_ARGUMENT_;
    }
    unsigned long totalOfItems=0;
    unsigned long counterItems=0;
    /**
     assume the very first line keeps track size
    */
    double tmpDbl=NAN;
    srcFile >> tmpDbl;
    if (srcFile.fail())
    {
        log->error(WORKFLOW_TASK_ERROR, "Track size not found in first line of " + fName);
        return _ERROR_WRONG_ARGUMENT_;
    }
    if (!isValidSizeOfTrack(tmpDbl))
    {
        log->error(WORKFLOW_TASK_ERROR, "Track size can not be double! Double-check first line of " + fName);
        return _ERROR_WRONG_ARGUMENT_;
    }
    totalOfItems = (unsigned long) tmpDbl;
    std::string sItem("");
    std::stringstream sstr;
    sstr.str((sItem));
    double value;
    track.clear();
    track.resize(totalOfItems);
    /**
     read track's values from current track file
    */
    while (!srcFile.eof())
    {
        srcFile >> value;
        if (srcFile.fail())
        {
            ++counterItems;
            if(counterItems < totalOfItems)
            {
                sItem = "";
                sstr.clear();
                sstr.str(sItem);
                sstr << "Declared Track size= " << totalOfItems;
                sstr << " GT actual track size=" << counterItems;
                sstr << " in " << fName;
                log->warning(WORKFLOW_TASK_WARNING, sstr.str());
                track.resize(counterItems);
            }
            break;
        }
        if(counterItems == totalOfItems)
        {
            log->error(WORKFLOW_TASK_ERROR, "Track size LT actual size in " + fName + " Double-check its first line!");
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO!!! ������������� ��� : "Overflow" <- !
        track[counterItems++]= value;
    }
    srcFile.close();
    return _RC_SUCCESS_;
}

int DataLoader::loadMultipleTrack(const std::string & fName, std::vector<std::vector<double> > & tracks,  std::vector<std::string> &file_names, Logger * log)
{
    std::ifstream trackNamesFile(fName.c_str());
    if (!trackNamesFile) {
        log->error(WORKFLOW_TASK_ERROR, "No tracks source file \"" + fName + "\" exists");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    std::string srcFileName("");
    tracks.clear();
    while (!trackNamesFile.eof())
    {
        std::stringstream sstr;

        srcFileName.clear();
        /**
         get next track file name from tracksFile
        */
        std::getline(trackNamesFile, srcFileName);
        TrimStr(srcFileName,DEFAULT_DELIMITERS);
        /** remove COMMENTs from line */
        srcFileName = removeComment(srcFileName, DELIMITER_COMMENT_STARTS_WITH);
        if (srcFileName.empty())
            continue;
        std::vector<double> track;
        int rc = loadSingleTrack(srcFileName, track, log);
        if(_RC_SUCCESS_ != rc)
        {
            continue;
        }
//!!!!!!!!!!!
        std::stringstream strLog;
        strLog << "Load data, file: \"" << srcFileName << "\", size: " << track.size();
        log->log(WORKFLOW_TASK_NOTIFICATION, strLog.str());
///TODO!!! ������������� ��� : "Overflow" <- !
        tracks.push_back(track);
        /**
         populate file_names with current track file name
        */
        file_names.push_back(srcFileName);
    }
    trackNamesFile.close();
    return _RC_SUCCESS_;
}

int DataLoader::loadFromDB(const std::string & fName, std::vector<double> & track, Logger * log)
{
    log->error(WORKFLOW_TASK_ERROR, "Loading datafrom DB is not supported");
    return false;
}
/**************************************************
*   CTOR
/**************************************************/
DataLoader::DataLoader(Logger*& log) : m_mode(FAKE_DATALOADER_MODE)
{
    m_logger=log;
}
/**************************************************
*   DTOR
/**************************************************/
DataLoader::~DataLoader()
{
    m_file_names.clear();
    m_tracks.clear();
}
/****************************************
*
*****************************************/
int DataLoader::parseAndCreateParams(std::map<std::string, std::string>& paramsMap)
{
    /** Do first part of Data Loader initialization stuff:
     Let's get input data type (single/1D array or multiple/2D array)
    */
    auto itParam = paramsMap.find(grammarDataLoaderConfig[DATA_SET_TYPE_ITEM]);
    m_mode = FAKE_DATALOADER_MODE;
    if (itParam != paramsMap.end())
    {
        if (itParam->second == grammarDataLoaderMode[SINGLE_TRACK])
        {
            m_mode = SINGLE_TRACK;
            m_logger->log(WORKFLOW_TASK_NOTIFICATION, "Load single track");
        }
        else if (itParam->second == grammarDataLoaderMode[MULTIPLE_TRACK])
        {
            m_mode = MULTIPLE_TRACK;
            m_logger->log(WORKFLOW_TASK_NOTIFICATION, "Load vector of tracks");
        }
        else
        {
            m_logger->error(WORKFLOW_TASK_ERROR, "DATA_SET_TYPE is invalid");
//            finish(CONFIG_FAILED, "DATA_SET_TYPE is invalid");
            return _ERROR_WRONG_WORKFLOW_;
        }
    }
    else
    {
        m_logger->error(WORKFLOW_TASK_ERROR, "DATA_SET_TYPE not set");
//        finish(CONFIG_FAILED, "DATA_SET_TYPE not set");
        return _ERROR_WRONG_WORKFLOW_;
    }

///TODO!!! ��� ����� ���������� � ������ ������! ����� TRACK_NAME ������!
    /** Do first part of Data Loader initialization stuff:
     Let's get input data file name
    */
    itParam = paramsMap.find(grammarDataLoaderConfig[DATA_SET_FILENAME_ITEM]);
    if (itParam != paramsMap.end())
    {
        m_file_names.clear();
        TrimStr(itParam->second, DEFAULT_DELIMITERS);
        m_trackNamesFile = itParam->second;
#ifdef _DEBUG_
            std::cout << itParam->second << std::endl;
#endif
    } ///end "TRACK_NAME has been found"
    else
    {
        m_logger->error(WORKFLOW_TASK_ERROR, "DATA_SET_FILENAME not set");
//        finish(CONFIG_FAILED, "DATA_SET_FILENAME not set");
        return _ERROR_WRONG_WORKFLOW_;
    }
    return _RC_SUCCESS_;
}

int DataLoader::run(ptrInt threadInterruptSignal)
{
    int rc;
    if (m_mode == SINGLE_TRACK)
    {
        m_logger->log(WORKFLOW_TASK_NOTIFICATION, "Loading track from " + m_trackNamesFile);
        m_tracks.resize(1);
        m_file_names.push_back(m_trackNamesFile);
        rc = DataLoader::loadSingleTrack(m_trackNamesFile, m_tracks[0], m_logger);
        if(_RC_SUCCESS_ != rc)
        {
            m_file_names.clear();
            m_tracks.clear();
        }
    }
    else
    {
#ifdef _DEBUG_
        std::cout << m_trackNamesFile << std::endl;
#endif
        m_logger->log(WORKFLOW_TASK_NOTIFICATION, "Loading tracks from source list in " + m_trackNamesFile);
        rc = DataLoader::loadMultipleTrack(m_trackNamesFile, m_tracks, m_file_names, m_logger);
    }
//OLD ����� ��� �� �����! ��� ����� �������� ������ ������ �� �����������!
    if (_RC_SUCCESS_ != rc)
    {
        /** data load from file failed!
        */
        m_logger->error(WORKFLOW_TASK_ERROR, "Can not load data from files specified with TRACK_NAME!");
//             finish(CONFIG_FAILED, "Can not load data from files specified with TRACK_NAME!");
        return _ERROR_WRONG_WORKFLOW_;
//OLD            itParam = paramsMap.find(grammarTaskStepConfig[REPO_ENTRIES_ITEM]);
//OLD            if (itParam == paramsMap.end())
//OLD            {
//OLD                _log.error(WORKFLOW_TASK_ERROR, "Neither TRACK_NAME nor REPO_ENTRIES set");
//OLD                finish(CONFIG_FAILED, "Neither TRACK_NAME nor REPO_ENTRIES set");
//OLD            }
    }
    return _RC_SUCCESS_;
}///end run()
/***************************************************************************
* PARAM [OUT] dimension -- dimension of ptrToPtrDI array of DataInfo*
* PARAM [OUT] ptrToPtrDI -- array of DataInfo* initialized with data from files
* RETURNs _RC_SUCCESS_ if success
_ERROR_WRONG_ARGUMENT_ or _ERROR_NO_ROOM_ otherwise
/**************************************************************************/
int DataLoader::getDataInfoArray(size_t & dimension, DataInfo** & ptrToPtrDI) const
{
    if ((!m_tracks.size()) || (!m_file_names.size()) || (m_tracks.size() != m_file_names.size()) )
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = m_tracks.size();
    size_t* datasetSizes = new size_t[size];
    if (!datasetSizes)
    {
        return _ERROR_NO_ROOM_;
    }
    std::memset(datasetSizes, 0, size*sizeof(size_t));
    std::string* datasetnames = new std::string[size];
    if (!datasetnames)
    {
        delete [] datasetSizes;
        return _ERROR_NO_ROOM_;
    }
    double** ppDblData = new double*[size];
    if (!ppDblData)
    {
        delete [] datasetSizes;
        delete [] datasetnames;
        return _ERROR_NO_ROOM_;
    }
    std::memset(ppDblData, 0, size*sizeof(double*));

    for(unsigned int i = 0; i < size; ++i)
    {
        datasetSizes[i] = m_tracks[i].size();
///TODO! possible bug! here implies map 1-to-1: tracks[i] <-> file_names[i]
// file_names contains datasetnames (ind = 0..dimension-1) and dataSetName (ind = dimension)
        datasetnames[i] = m_file_names[i];
        ppDblData[i] = new double[datasetSizes[i]];
        if(!ppDblData[i])
        {
            while(i)
            {
                i--;
                delete ppDblData[i];
            }
            delete [] ppDblData;
            ppDblData = NULL;
            delete [] datasetSizes;
            datasetSizes = NULL;
            delete [] datasetnames;
            datasetnames = NULL;
            return _ERROR_NO_ROOM_;
        }
        std::memset(ppDblData[i], 0, datasetSizes[i]*sizeof(double));
        for(size_t j = 0; j < datasetSizes[i]; ++j)
        {
            ppDblData[i][j] = m_tracks[i][j];
        }
    }
///TODO!!! ��� � ����� ����� � ��     di->m_dataSetTrackNames = datasetnames; ��� ����� ��������� � �����-�� ����������� ������� � ������ DataInfo
///TODO!!! � ����� DataManager �� ����� ������ ����� �� ���������� DataInfo
    dimension = 1;
    ptrToPtrDI = new tagDataInfo * [dimension];
    if(!ptrToPtrDI)
    {
        for(size_t i=0; i < size; ++i)
        {
            delete [] ppDblData[i];
        }
        delete [] ppDblData;
        ppDblData = nullptr;
        delete [] datasetSizes;
        datasetSizes = nullptr;
        delete [] datasetnames;
        datasetnames = nullptr;
        dimension = 0;
        return _ERROR_NO_ROOM_;
    }
    ptrToPtrDI[0] = new tagDataInfo(ppDblData);
    if(! ptrToPtrDI[0])
    {
        delete [] ptrToPtrDI;
        ptrToPtrDI = nullptr;
        for(size_t i=0; i < size; ++i)
        {
            delete [] ppDblData[i];
        }
        delete [] ppDblData;
        ppDblData = nullptr;
        delete [] datasetSizes;
        datasetSizes = nullptr;
        delete [] datasetnames;
        datasetnames = nullptr;
        dimension = 0;
        return _ERROR_NO_ROOM_;
    }
    ptrToPtrDI[0]->m_dimension = size;
    ptrToPtrDI[0]->m_dataCreatorName = "Task";
///TODO! possible bug! here implies the size of tracks == (size of file_names - 1)
    ptrToPtrDI[0]->m_dataSetName = m_trackNamesFile;///BUG file_names[size];
    ptrToPtrDI[0]->m_dataModelName = "-";
    ptrToPtrDI[0]->m_dataSetSizes = datasetSizes;
    ptrToPtrDI[0]->m_dataSetTrackNames = datasetnames;
    return _RC_SUCCESS_;
}///end getDataInfoArray()

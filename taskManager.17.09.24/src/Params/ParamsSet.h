#ifndef __PARAM_SET_H__633569422542968750
#define __PARAM_SET_H__633569422542968750

#include <string>
#include <vector>

// DON'T pollute global scope! using namespace std;
#include "Params.h"
#include "../Log/Log.h"

enum ParamsSrcType{
    FILE_SRC, DB
};

/** forward declarations */
class IModel;

/** Interface declaration - abstract class */
class IParamsSet
{
public:

    static IParamsSet* createEmptyParamsSet(const IModel* model, Logger* log);
/** TODO!!! � ������ �� ��������� ���� ����������� ������������ ���������� ������� ��� ����� ����������, �������� PLAIN_MODEL? ����� ��������� ������ ������������ IParamsSet �� IEstimator!
    static IParamsSet* createEmptyParamsSet(const IEstimator* estimator, Logger* log);
*/
    /** virtual DTOR */
    virtual ~IParamsSet() {};
    virtual void clear() = 0;
    /**
    �����, � ���� �������,
    �� �����������, ��� � ��������� ����� ��������� ��������� �� ������������ � ���������� ������
    */
    /** this method requires virtual castModelParams in IModel!
    */
    virtual bool addParamsCopyToSet(const IModel* model, const IParams* params, Logger* log) = 0;
///TODO!!!! afraid for Estimators' paramsSet we MUST implement the same:
///    virtual bool addParams(const IEstimator* estimator, IParams* params, Logger* log)  =0;

    /** this method requires virtual parseAndCreateModelParams in IModel! */
    virtual bool fillParamsSet(const IModel* model, const std::string& srcName, Logger* log) = 0;

///REDUNDANT virtual int setModel(const IModel* model, Logger* log) = 0;
    virtual size_t getSetSize() const = 0;
    virtual int getCurrentId() const = 0;
    virtual int setCurrentId(size_t id) = 0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getFirstPtr() = 0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getNextPtr() = 0;
    virtual IParams* getParamsCopy(size_t id) const = 0;
    virtual bool isEnd() const = 0;
};

/** forward declaration */
class IAdditiveModel;
/** Interface declaration - abstract class */
class IParamsUnionSet
{
public:
    static IParamsUnionSet* createEmptyParamsUnionSet(const IAdditiveModel* additiveModel, Logger* log);

    virtual ~IParamsUnionSet(){};
    /**
    �����, � ���� �������,
    �� �����������, ��� � ��������� ����� ��������� ��������� �� ������������ � ���������� ������
    */
    /** this method requires virtual castModelParams in IModel! */
    virtual bool addParamsUnionCopy(const IAdditiveModel* model, IParamsUnion* params, Logger* log) = 0;

///TODO!!!! afraid for Estimators' paramsSet we MUST implement the same:
///    virtual bool addParams(const IEstimator* estimator, IParamsUnion* params, Logger* log)  =0;

    /** this method requires virtual createModelParams in IAdditiveModel! */
    virtual bool fillParamsUnionSet(const IAdditiveModel* model, const std::string& srcName, Logger* log) = 0;
///REDUNDANT virtual int setModel(const IAdditiveModel* additiveModel, Logger* log) = 0;
    virtual size_t getSetSize() const = 0;
    virtual int getCurrentId() const = 0;
    virtual int setCurrentId(unsigned id) = 0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getFirstPtr() = 0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getNextPtr() = 0;
    virtual IParamsUnion* getParams(unsigned id) = 0;
    virtual bool isEnd() const = 0;
};
#endif

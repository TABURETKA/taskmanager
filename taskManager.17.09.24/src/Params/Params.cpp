//#define _DEBUG_

#include <sstream>
//OLD #include <stdlib.h> /* atof() */
#include <float.h> /* DBL_MIN */
#include "../CommonStuff/Defines.h" /* DELIMITER_TO_STRING_ITEMS */
#include "../Log/errors.h"
#include "../Log/Log.h"
#include "../Config/ConfigParser.h" /* iteratorParamsMap */
#include "Params.h"

/** static in IParams interface
 OLD C++ < C++11
const char * IParams::paramsNames[IParams::ParamsTypes::FAKE_PARAMS]=
{
  "Params1D",
  "GaussianParams",
  "WienerParams",
  "PoissonParams",
  "ParetoParams",
  "PoissonDrvnParetoJumpsExpMeanRevertingParams"
};
*/
/** DEFINITION! Not declaration!
 Declaration and init are inside the class definition (in h-file),
 but (C++11's flaw!) the class member definition has to be separate (in cpp).
*/
constexpr const char * IParams::paramsNames[IParams::ParamsTypes::FAKE_PARAMS_TYPES];

/** static in IParams interface */
enum IParams::ParamsTypes IParams::getParamsTypeByParamsName(char const * const paramsName)
{
  if(NULL == paramsName) return FAKE_PARAMS_TYPES;
  size_t i=0;
  for(i=0; i < FAKE_PARAMS_TYPES; ++i)
  {
      size_t pos = std::strcmp(paramsName, IParams::paramsNames[i]);
      if (0 == pos)
      {
          return (enum IParams::ParamsTypes)i;
      }
  }
  return FAKE_PARAMS_TYPES;
}
/** static in IParams interface
 OLD C++ < C++11
const char * IParams::grammar[FAKE_INDEX_GRAMMAR_PARAMS1D]={
  "DIMENSION"
};
*/
/** DEFINITION! Not declaration! */
constexpr const char * IParams::grammar[FAKE_INDEX_GRAMMAR_PARAMS1D];

/** anonymous namespace to hide private implementations into this cpp-file */
namespace{
/** class container */
/******************************************************
*               Base Params class
*******************************************************/
class IParams_Impl : public virtual IParams
{
public:
    /** PUBLIC CTORs (in the scope of this file ONLY)
    to call from CTORs of derived classes
    */
    IParams_Impl(size_t size);
    IParams_Impl(const IParams_Impl&);
    IParams_Impl& operator=(const IParams_Impl&);
    /** Default DTOR */
    virtual ~IParams_Impl();
    /** Default factory method merely
    to create empty Params1D implementation
    */
    static IParams_Impl* createEmptyParams();
    /** Default factory method
    merely to create empty Params1D implementation of given size
    */
    static IParams_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);

///TODO!!! WHY? Any subclass may have own clone algorithm???
    virtual IParams_Impl* clone() const;
    virtual std::string toString() const;
    virtual IParams::ParamsTypes getParamsType() const override;

    virtual size_t getSize() const;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MUST define operator(i,...) to set access to params by index
    virtual int setParam(size_t index, double value);
    virtual int setParams(size_t size, const double * params, size_t offsetInDst=0);
    virtual int getParam(size_t index, double& param) const;
    virtual int getParams(size_t size, double * params, size_t offsetInSrc=0) const;
protected:
    int reallocateArray(size_t size);
    IParams::ParamsTypes m_paramsType{IParams::ParamsTypes::FAKE_PARAMS_TYPES};
private:
    /** data stuff */
    size_t m_size;
    double * m_array;
};

/******************************************************
* IParamsGaussian interface implementation
*******************************************************/
class IParamsGaussian_Impl : public virtual IParams_Impl,
                             public virtual IParamsGaussian
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsGaussian_Impl();
    /** Factory methods */
    static IParamsGaussian_Impl* createEmptyParams();
    static IParamsGaussian_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsGaussian_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Gaussian model */
    virtual int setParam(size_t index, double value);
    /** Here offsetInDst is ignored */
    virtual int setParams(size_t size, const double * params, size_t offsetInDst =0);
    virtual int setMean(double meanNew);
    virtual int setSDeviation(double sdNew);
    virtual double mu() const;
    virtual double sigma() const;

private:
    /** NO public ctor! Factory is in use instead! */
    IParamsGaussian_Impl();
    /** assign and copy CTORs are disabled */
    IParamsGaussian_Impl(const IParamsGaussian_Impl&);
    IParamsGaussian_Impl& operator=(const IParamsGaussian_Impl&);
};

/******************************************************
* IParamsWiener interface implementation
*******************************************************/
class IParamsWiener_Impl : public virtual IParams_Impl,
                           public virtual IParamsWiener
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsWiener_Impl();
    /** Factory methods */
    static IParamsWiener_Impl* createEmptyParams();
    static IParamsWiener_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsWiener_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Wiener model */
    virtual int setParam(size_t index, double value);
    /** Here offsetInDst ignored */
    virtual int setParams(size_t size, const double * params, size_t offsetInDst =0);
    virtual int setInitialCondition(double initConditionNew);
    virtual int setMean(double meanNew);
    virtual int setSDeviation(double sdNew);
    virtual double initialCondition() const;
    virtual double mu() const;
    virtual double sigma() const;

private:
    /** NO public ctor! Factory is in use instead! */
    IParamsWiener_Impl();
    /** assign and copy CTORs are disabled */
    IParamsWiener_Impl(const IParamsWiener_Impl&);
    IParamsWiener_Impl& operator=(const IParamsWiener_Impl&);
};

/******************************************************
* IParamsPoisson interface implementation
*******************************************************/
class IParamsPoisson_Impl : public virtual IParams_Impl,
                            public virtual IParamsPoisson
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsPoisson_Impl();
    /** Factory uses following methods */
    static IParamsPoisson_Impl* createEmptyParams();
    static IParamsPoisson_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsPoisson_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Pareto model */
    virtual int setParam(size_t index, double value);
    /** Here offsetInDst is ignored */
    virtual int setParams(size_t size, const double * params, size_t offsetInDst=0);

    virtual int setPoissonLambda(double lambdaNew);
    virtual double lambda() const;

private:
    /** NO public ctor! Factory is in use instead! */
    IParamsPoisson_Impl();
    /** assign and copy CTORs are disabled */
    IParamsPoisson_Impl(const IParamsPoisson_Impl&);
    IParamsPoisson_Impl& operator=(const IParamsPoisson_Impl&);
};
/******************************************************
* IParamsPareto interface implementation
*******************************************************/
class IParamsPareto_Impl : public virtual IParams_Impl,
                           public virtual IParamsPareto
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsPareto_Impl();
    /** Factory uses following methods */
    static IParamsPareto_Impl* createEmptyParams();
    static IParamsPareto_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsPareto_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Pareto model */
    virtual int setParam(size_t index, double value);
    /** Here offsetInDst is ignored */
    virtual int setParams(size_t size, const double * params, size_t offsetInDst=0);

    virtual int setParetoXm(double xMNew);
    virtual int setParetoK(double kNew);
    virtual double xM() const;
    virtual double k() const;

private:
    /** NO public ctor! Factory is in use instead! */
    IParamsPareto_Impl();
    /** assign and copy CTORs are disabled */
    IParamsPareto_Impl(const IParamsPareto_Impl&);
    IParamsPareto_Impl& operator=(const IParamsPareto_Impl&);
};
/******************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting interface implementation
*******************************************************/
class IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl : public virtual IParams_Impl,
                                  public virtual IParamsPoissonDrvnParetoJumpsExpMeanReverting
{
public:
    /** NO public ctor! Factory is in use instead! */
    virtual ~IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    /** Factory uses following methods */
    static IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* createEmptyParams();
    static IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix);
    /** IParams stuff */
    virtual std::string toString() const;
    using IParams_Impl::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
///TODO!!! WHY Any subclass may have own clone algorithm???
    virtual IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* clone() const;
    using IParams_Impl::getParam;
    using IParams_Impl::getParams;

    /** Specific stuff - WRAPPERS for Poisson-Pareto model */
    virtual int setParam(size_t index, double value);
    /** Here offsetInDst is ignored */
    virtual int setParams(size_t size, const double * params, size_t offsetInDst=0);

    virtual int setPoissonLambda(double poissonLambdaNew);
    virtual int setMRLambda(double MR_lambdaNew);
    virtual int setParetoXm(double xMNew);
    virtual int setParetoK(double kNew);
    virtual double lambda() const;
    virtual double MR_lambda() const;
    virtual double xM() const;
    virtual double k() const;

private:
    /** NO public ctor! Factory is in use instead! */
    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    /** assign and copy CTORs are disabled */
    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& operator=(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl&);
};

/******************************************************
* IParamsUnion interface implementation
*******************************************************/
/** Merely array of IParams - class container */
class IParamsUnion_Impl : public virtual IParamsUnion
{
public:
    /** plain params UNION for additive model params */
    IParamsUnion_Impl(size_t totalOfItems, const size_t* sizesOfItems);
    /** make by instances of additive model params */
    IParamsUnion_Impl(size_t totalOfItems, const IParams** items);
///TODO??? Does it make sense to copy them public?
    IParamsUnion_Impl(const IParamsUnion_Impl&);
///TODO??? Does it make sense to assign them public?
    IParamsUnion_Impl& operator=(const IParamsUnion_Impl&);
    virtual ~IParamsUnion_Impl();
    /** IParams stuff */
    /** Any subclass may have own copy algorithm */
    virtual IParamsUnion_Impl* clone() const;
    /** Every subclass MUST have own IMPLEMENTATION for toString()! */
    virtual std::string toString() const;

    /** IParamsUnion stuff */
    virtual size_t getTotalOfItemsInParamsUnion() const;
    virtual size_t getSize() const;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MUST define operator(i,...) to get access to params by index
    virtual int setItem(size_t index, const IParams* itemParams);
    virtual int setItems(size_t size, const IParams** itemsParams);
    /** the clone/copy of existing m_items[index] */
    virtual int getItemCopy(size_t index, IParams*& itemParams) const;
    IParams* ptrIParamsUnionToPtrIParams(size_t index) const;
    /** to clone/copy the entire ParamsUnion instance/all its m_items */
    virtual int getItemsCopies(size_t size, IParams**& itemsParams) const;
private:
    /** data stuff */
    size_t m_totalOfItems;
    IParams ** m_items;
    int reallocatePtrToItems(size_t totalOfItems);
    /** for plain addends only */
    int reallocateItems(size_t totalOfItems, const size_t* sizesOfItems);
};

} ///end anonymous namespace

/**************** IMPLEMENTATIONs ***************************/
/*************************************************************
*           IParams Private Implementation
*************************************************************/
int IParams_Impl::reallocateArray(size_t size)
{
    delete [] m_array;
    m_array = NULL;
    if(! size)
    {
        m_size=0;
        return _RC_SUCCESS_;
    }
    m_array = new double[size];
    if (m_array)
    {
        m_size = size;
        std::memset(m_array, 0, size*sizeof(double));
        return _RC_SUCCESS_;
    }
    return _ERROR_NO_ROOM_;
}
/***************************************************
* IParams_Impl CTOR
****************************************************/
IParams_Impl::IParams_Impl(size_t size) : IParams()
{
  m_size=0;
  m_array=NULL;
  if (size)
  {
      int rc = reallocateArray(size);
      if (_RC_SUCCESS_ != rc)
      {
          /** HOW LOG error?
           Clean up members for SURE!
          */
          m_size=0;
          m_array=NULL;
      }
  }
  m_paramsType = IParams::ParamsTypes::Params1D;
}
/***************************************************
* IParams_Impl Copy CTOR
****************************************************/
IParams_Impl::IParams_Impl(const IParams_Impl& paramsImplSrc) // : IParams_Impl(0)
{
    m_size=0;
    m_array=NULL;
       /** deep copy */
    if (! paramsImplSrc.m_size)
        return;
    const double * dblArray= (const double *)paramsImplSrc.m_array;
    if (!dblArray)
    {
        return ;
    }
    /** finalize deep copy */
    int rc = setParams(paramsImplSrc.m_size, dblArray);
    if(_RC_SUCCESS_ != rc)
    {
///TODO!!! HOW Log?
    }
    m_paramsType = paramsImplSrc.m_paramsType;
}
/***************************************************
* IParams_Impl Assignment CTOR
****************************************************/
IParams_Impl& IParams_Impl::operator=(const IParams_Impl& paramsImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsImplSrc)
        return *this;
       /** deep copy */
    if (!m_size){
        reallocateArray(0);
    }
    const double * dblArray= (const double *)paramsImplSrc.m_array;
    if (!dblArray)
    {
        m_size=0;
        m_array=NULL;
        return *this;
    }
    /** finalize deep copy */
    int rc = setParams(paramsImplSrc.m_size, dblArray);
    if (rc)
    {
///TODO!!! HOW Log?
    }
    m_paramsType = paramsImplSrc.m_paramsType;
    return *this;
}
/***************************************************
* IParams_Impl DTOR
****************************************************/
IParams_Impl::~IParams_Impl()
{
#ifdef _DEBUG_
// std::cout << "into ~IParams_Impl :" <<  std::endl;
#endif
    if((m_size) || (m_array))
    {
        delete [] m_array;
        m_array = NULL;
        m_size=0;
    }
}
/***************************************************
* IParams_Impl toString
****************************************************/
std::string IParams_Impl::toString() const
{
    if(m_size)
    {
        std::stringstream ss;
        for(size_t i=0; i < m_size; ++i)
        {
            ss << "PARAM" << i << DELIMITER_NAME_VALUE << m_array[i];
            if (i < m_size-1) ss << DELIMITER_TO_STRING_ITEMS;
        }
        return ss.str();
    }
    else
        return " In IParams_Impl class (Base class) toString() nothing toString";
}
/***************************************************
* IParams_Impl getParamsType
****************************************************/
IParams::ParamsTypes IParams_Impl::getParamsType() const
{
    return m_paramsType;
}
/***************************************************
* IParams_Impl getSize
****************************************************/
size_t IParams_Impl::getSize() const
{
    return m_size;
}
/** ��� ������ ����� ���������� ������
 �� ���������� ���������� ���������� ���������� ������,
������� ����� ������������� �� IParams!
� ��� � ���������� ���������� IParams_Impl,
� �������� setParam/setParams � getParam/getParams
� ����� ������ ������������� ������� */
/***************************************************
* IParams_Impl getParam
****************************************************/
int IParams_Impl::getParam(size_t index, double& param) const
{
    if (index > m_size - 1)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    param = m_array[index];
    return _RC_SUCCESS_;
}
/***************************************************
* IParams_Impl setParam
****************************************************/
int IParams_Impl::setParam(size_t index, double value)
{
    if (index > m_size - 1)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    m_array[index] = value;
    return _RC_SUCCESS_;
}
/***************************************************
* IParams_Impl getParams
****************************************************/
int IParams_Impl::getParams(size_t size, double * params, size_t offsetInSrc) const
{
    if ((offsetInSrc + size) > m_size)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    std::memcpy(params, (m_array + offsetInSrc), size*sizeof(double));
    return _RC_SUCCESS_;
}
/***************************************************
* IParams_Impl setParams
****************************************************/
int IParams_Impl::setParams(size_t size, const double* params, size_t offsetInDst)
{
    if(m_size < (offsetInDst + size))
    {
       int rc = reallocateArray(offsetInDst + size);
       if (rc)
       {
           return rc;
       }
    }
    std::memcpy(m_array + offsetInDst, params, size*sizeof(double));
    return _RC_SUCCESS_;
}

/***************************************************
* IParams_Impl clone
****************************************************/
IParams_Impl* IParams_Impl::clone() const
{
    /** Now it's DEEP! Not Shallow copy */
     IParams_Impl* params = new IParams_Impl(*this);
      /** follows by deep copy
      size_t size=this->m_size;
    if (!size)
        return NULL;// nullptr;
    if (!params)
        return NULL; // nullptr;
    const double * dblArray=this->m_array;
    if (!dblArray)
        return NULL; // nullptr;
    / finalize deep copy
    int rc = params->setParams(size, dblArray);
      if (rc)
    {
        delete params;
        params = NULL;
        return NULL; // nullptr;
    }
    */
    return params;
}

/** Factory method
 merely to create empty Params1D implementation of size 0
*/
IParams_Impl* IParams_Impl::createEmptyParams()
{
    return new IParams_Impl(0);
}

/*************************************************************
*                         Factory method
* merely to create empty Params1D implementation of given size
**************************************************************/
IParams_Impl* IParams_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    iteratorParamsMap itParam = mapParams.find(IParams::grammar[indexOfDimension]);
    size_t dimension=0;
    double dimAsDbl = -DBL_MIN;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), dimAsDbl); //OLD          float dimAsFloat = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if((rc != _RC_SUCCESS_) || (dimAsDbl < DBL_MIN))
        return nullptr;

    dimension = static_cast<size_t> (dimAsDbl);

    IParams_Impl* params = new IParams_Impl(dimension);
    return params;
}

/***************************************************
*        IParamsGaussian_Impl Implementation
****************************************************/
/** DEFINITION! Not declaration! */
const size_t IParamsGaussian::GaussianCDFParamsDimension;

/** tokens to parse map for GaussianParams
 static in IParamsGaussian interface
 OLD C++ < C++11
const char * IParamsGaussian::grammar[IParamsGaussian::indexesOfGrammarForGaussianParams::FAKE_INDEX_GRAMMAR_GAUSSIAN] ={
  "MU",
  "SIGMA"
};
*/
/** DEFINITION! Not declaration! */
constexpr const char * IParamsGaussian::grammar[IParamsGaussian::indexesOfGrammarForGaussianParams::FAKE_INDEX_GRAMMAR_GAUSSIAN];

/***************************************************
* IParamsGaussian_Impl DTOR
****************************************************/
IParamsGaussian_Impl::~IParamsGaussian_Impl()
{
#ifdef _DEBUG_
//    std::cout<< "ParamsGaussian is deleting" << std::endl;
#endif
}
/***************************************************
* Private IParamsGaussian_Impl CTOR
****************************************************/
IParamsGaussian_Impl::IParamsGaussian_Impl() : IParams_Impl(GaussianCDFParamsDimension)
{
    setMean(NAN);
    setSDeviation(NAN);
    m_paramsType = IParams::ParamsTypes::GaussianParams;
}
/***************************************************
* Private IParamsGaussian_Impl Copy CTOR
****************************************************/
IParamsGaussian_Impl::IParamsGaussian_Impl(const IParamsGaussian_Impl& paramsGaussianImplSrc) : IParams_Impl(GaussianCDFParamsDimension)
{
       /** deep copy */
    IParams_Impl * ptrParamsImpl = (IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc = (IParams_Impl *)&paramsGaussianImplSrc;
    size_t size = GaussianCDFParamsDimension;
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
    m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
}
/***************************************************
* Private IParamsGaussian_Impl Assignment
****************************************************/
IParamsGaussian_Impl& IParamsGaussian_Impl::operator=(const IParamsGaussian_Impl& paramsGaussianImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsGaussianImplSrc)
        return *this;
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsGaussianImplSrc;
    size_t size = GaussianCDFParamsDimension;
    double * ptrDblAray = new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
    m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
    return *this;
}
/***************************************************
* IParamsGaussian_Impl toString
****************************************************/
std::string IParamsGaussian_Impl::toString() const
{
    std::stringstream ss;
#ifdef _DEBUG_
std::cout << mu() << std::endl;
std::cout << sigma() << std::endl;
#endif
    ss << grammar[MU] << DELIMITER_NAME_VALUE << mu() << DELIMITER_TO_STRING_ITEMS <<
    grammar[SIGMA] << DELIMITER_NAME_VALUE << sigma();

    return ss.str();
}
/***************************************************
* IParamsGaussian_Impl setParam
****************************************************/
int IParamsGaussian_Impl::setParam(size_t index, double value)
{
    if (index > GaussianCDFParamsDimension - 1)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index == SIGMA) && (value < DBL_MIN))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
}

/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsGaussian_Impl setParams
****************************************************/
/** Here offsetInDst is ignored */
int IParamsGaussian_Impl::setParams(size_t newSize, const double* params, size_t offsetInDst)
{
    if (newSize != GaussianCDFParamsDimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if (params[SIGMA] < DBL_MIN)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParams(GaussianCDFParamsDimension, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsGaussian_Impl clone
****************************************************/
IParamsGaussian_Impl* IParamsGaussian_Impl::clone() const
{
    IParamsGaussian_Impl* params = new IParamsGaussian_Impl(*this);
    return params;
}
/***************************************************
* IParamsGaussian_Impl mu() == getParam(0)
****************************************************/
double IParamsGaussian_Impl::mu() const
{
   double Mu=NAN;
   /** Here rc check DOES NOT make sense */
   getParam(MU, Mu);
   return Mu;
}
/***************************************************
* IParamsGaussian_Impl mu() == getParam(1)
****************************************************/
double IParamsGaussian_Impl::sigma() const
{
    double Sigma = NAN;
    /** Here rc check DOES NOT make sense */
    getParam(SIGMA, Sigma);
    return Sigma;
}
/***************************************************
* IParamsGaussian_Impl setMean() == setParam(0)
****************************************************/
int IParamsGaussian_Impl::setMean(double meanNew)
{
    return setParam(MU, meanNew);
}
/***************************************************
* IParamsGaussian_Impl setSDeviation() == setParam(1)
****************************************************/
int IParamsGaussian_Impl::setSDeviation(double sdNew)
{
    if (sdNew < DBL_MIN)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    return setParam(SIGMA, sdNew);
}

/*********************************
*   IParamsGaussian_Impl Factory
*********************************/
IParamsGaussian_Impl* IParamsGaussian_Impl::createEmptyParams()
{
    return new IParamsGaussian_Impl();
}
/*************************************
*   IParamsGaussian_Impl Factory
*************************************/
IParamsGaussian_Impl* IParamsGaussian_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    std::string tmpStr = IParamsGaussian::grammar[MU];
    if (postfix != "") tmpStr += postfix;
    iteratorParamsMap itParam = mapParams.find(tmpStr);
    double mu=NAN;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), mu); //OLD          mu = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    tmpStr = IParamsGaussian::grammar[SIGMA];
    if (postfix != "") tmpStr += postfix;
    itParam = mapParams.find(tmpStr);
    double sigma=NAN;
     /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), sigma); //OLD          sigma = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    IParamsGaussian_Impl* params = new IParamsGaussian_Impl();
    rc = params->setMean(mu);
    /**
     We may ignore above rc! Because Mean is ANY Double! No constrains!
    */
    rc = params->setSDeviation(sigma);
    /**
     We can not ignore above rc! Because Sigma MUST be positive!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=NULL;
        return NULL;
    }
    return params;
}

/***************************************************
*     IParamsWiener_Impl Implementation
****************************************************/
/** static in IParamsWiener interface */
/** DEFINITION! Not declaration! */
const size_t IParamsWiener::WienerCDFParamsDimension;

/** tokens to parse map for WienerParams
 static in IParamsWiener interface
*/
/** DEFINITION! Not declaration! */
constexpr const char * IParamsWiener::grammar[IParamsWiener::indexesOfGrammarForWienerParams::FAKE_INDEX_GRAMMAR_WIENER];

/***************************************************
* IParamsWiener_Impl DTOR
****************************************************/
IParamsWiener_Impl::~IParamsWiener_Impl()
{
#ifdef _DEBUG_
//    std::cout<< "ParamsWiener is deleting" << std::endl;
#endif
}
/***************************************************
* Private IParamsWiener_Impl CTOR
****************************************************/
IParamsWiener_Impl::IParamsWiener_Impl() : IParams_Impl(WienerCDFParamsDimension)
{
    setInitialCondition(NAN);
    setMean(NAN);
    setSDeviation(NAN);
    m_paramsType = IParams::ParamsTypes::WienerParams;
}
/***************************************************
* Private IParamsWiener_Impl Copy CTOR
****************************************************/
IParamsWiener_Impl::IParamsWiener_Impl(const IParamsWiener_Impl& paramsWienerImplSrc) : IParams_Impl(WienerCDFParamsDimension)
{
    /** deep copy */
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsWienerImplSrc;
    size_t size = WienerCDFParamsDimension;
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
   m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
}
/***************************************************
* Private IParamsWiener_Impl Assignment
****************************************************/
IParamsWiener_Impl& IParamsWiener_Impl::operator=(const IParamsWiener_Impl& paramsWienerImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsWienerImplSrc)
        return *this;
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsWienerImplSrc;
    size_t size = WienerCDFParamsDimension;
    double * ptrDblAray = new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
   m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
    return *this;
}
/***************************************************
* IParamsWiener_Impl toString
****************************************************/
std::string IParamsWiener_Impl::toString() const
{
    std::stringstream ss;
    ss << grammar[INITIAL_CONDITION] << DELIMITER_NAME_VALUE << initialCondition() << DELIMITER_TO_STRING_ITEMS <<
    grammar[MU] << DELIMITER_NAME_VALUE << mu() << DELIMITER_TO_STRING_ITEMS <<
    grammar[SIGMA] << DELIMITER_NAME_VALUE << sigma();

    return ss.str();
}
/***************************************************
* IParamsWiener_Impl setParam
****************************************************/
int IParamsWiener_Impl::setParam(size_t index, double value)
{
    if (index > WienerCDFParamsDimension - 1)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index == SIGMA) && (value < DBL_MIN))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
}

/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsWiener_Impl setParams
****************************************************/
/** Here offsetInDst is ignored */
int IParamsWiener_Impl::setParams(size_t sizeNew, const double* params, size_t offsetInDst)
{
    if (sizeNew != WienerCDFParamsDimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if (params[SIGMA] < DBL_MIN)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParams(WienerCDFParamsDimension, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsWiener_Impl clone
****************************************************/
IParamsWiener_Impl* IParamsWiener_Impl::clone() const
{
    IParamsWiener_Impl* params = new IParamsWiener_Impl(*this);
    return params;
}
/***************************************************
* IParamsWiener_Impl initialCondition() == getParam(0)
****************************************************/
double IParamsWiener_Impl::initialCondition() const
{
    double initialCondition=NAN;
    /** Here rc check DOES NOT make sense */
    getParam(INITIAL_CONDITION, initialCondition);
    return initialCondition;
}
/***************************************************
* IParamsWiener_Impl mu() == getParam(1)
****************************************************/
double IParamsWiener_Impl::mu() const
{
    double Mu=NAN;
    /** Here rc check DOES NOT make sense */
    getParam(MU, Mu);
    return Mu;
}
/***************************************************
* IParamsWiener_Impl mu() == getParam(2)
****************************************************/
double IParamsWiener_Impl::sigma() const
{
    double Sigma = NAN;
    /** Here rc check DOES NOT make sense */
    getParam(SIGMA, Sigma);
    return Sigma;
}
/***************************************************
* IParamsWiener_Impl setInitialCondition() == setParam(0)
****************************************************/
int IParamsWiener_Impl::setInitialCondition(double initConditionNew)
{
    return setParam(INITIAL_CONDITION, initConditionNew);
}
/***************************************************
* IParamsWiener_Impl setMean() == setParam(1)
****************************************************/
int IParamsWiener_Impl::setMean(double meanNew)
{
    return setParam(MU, meanNew);
}
/***************************************************
* IParamsWiener_Impl setSDeviation() == setParam(2)
****************************************************/
int IParamsWiener_Impl::setSDeviation(double sdNew)
{
    return setParam(SIGMA, sdNew);
}
/******************************
    IParamsWiener Factory
******************************/
IParamsWiener_Impl* IParamsWiener_Impl::createEmptyParams()
{
    return new IParamsWiener_Impl();
}
/*****************************
 IParamsWiener Factory
*****************************/
IParamsWiener_Impl* IParamsWiener_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    std::string tmpStr = IParamsWiener::grammar[INITIAL_CONDITION];
    if (postfix != "") tmpStr += postfix;
    iteratorParamsMap itParam = mapParams.find(tmpStr);
    double ic=NAN;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), ic); //OLD          ic = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    /** NO WAY return NULL - we assume the initial conditions may be non-initialized???
    if(rc != _RC_SUCCESS_)
        return nullptr;
    */

    tmpStr = IParamsWiener::grammar[MU];
    if (postfix != "") tmpStr += postfix;
    itParam = mapParams.find(tmpStr);
    double mu=NAN;
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), mu); //OLD          mu = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    tmpStr = IParamsWiener::grammar[SIGMA];
    if (postfix != "") tmpStr += postfix;
    itParam = mapParams.find(tmpStr);
    double sigma = NAN;
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), sigma); //OLD          sigma = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    IParamsWiener_Impl* params = new IParamsWiener_Impl();
    if(! params)
    {
        return NULL;
    }
    if(! isNAN_double(ic))
    {
        params->setInitialCondition(ic);
    }
    rc = params->setMean(mu);
    /**
     We may ignore above rc! Because Mean is ANY Double! No constrains!
    */
    rc = params->setSDeviation(sigma);
    /**
     We can not ignore above rc! Because Sigma MUST be positive!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params = NULL;
        return NULL;
    }
    return params;
}

/***************************************************
*         IParamsPoisson_Impl Implementation
****************************************************/
/** static in IParamsPoisson interface */
/** DEFINITION! Not declaration! */
const size_t IParamsPoisson::PoissonCDFParamsDimension;

/** tokens to parse map for PoissonParams
 static in IParamsPoisson interface
*/
/** DEFINITION! Not declaration! */
constexpr const char * IParamsPoisson::grammar[IParamsPoisson::indexesOfGrammarForPoissonParams::FAKE_INDEX_GRAMMAR_POISSON];

/***************************************************
* IParamsPoisson_Impl DTOR
****************************************************/
IParamsPoisson_Impl::~IParamsPoisson_Impl()
{
#ifdef _DEBUG_
//    std::cout<< "ParamsPoisson is deleting" << std::endl;
#endif
}
/***************************************************
* Private IParamsPoisson_Impl CTOR
****************************************************/
IParamsPoisson_Impl::IParamsPoisson_Impl() : IParams_Impl(PoissonCDFParamsDimension)
{
   setPoissonLambda(NAN);
}
/***************************************************
* Private IParamsPoisson_Impl Copy CTOR
****************************************************/
IParamsPoisson_Impl::IParamsPoisson_Impl(const IParamsPoisson_Impl& paramsPoissonImplSrc) : IParams_Impl(PoissonCDFParamsDimension)
{
    /** deep copy */
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonImplSrc;
    size_t size = PoissonCDFParamsDimension;
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
   m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
}
/***************************************************
* Private IParamsPoisson_Impl Assignment
****************************************************/
IParamsPoisson_Impl& IParamsPoisson_Impl::operator=(const IParamsPoisson_Impl& paramsPoissonImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsPoissonImplSrc)
        return *this;
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonImplSrc;
    size_t size = PoissonCDFParamsDimension;
    double * ptrDblAray = new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
   m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
    return *this;
}
/***************************************************
* IParamsPoisson_Impl toString
****************************************************/
std::string IParamsPoisson_Impl::toString() const
{
   std::stringstream ss;
   ss << grammar[LAMBDA] << DELIMITER_NAME_VALUE << lambda();
   return ss.str();
}
/***************************************************
* IParamsPoisson_Impl setParam
****************************************************/
int IParamsPoisson_Impl::setParam(size_t index, double value)
{
    if (index > PoissonCDFParamsDimension - 1)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index == 0) && (value < DBL_MIN))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
}
/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsPoisson_Impl setParams
****************************************************/
/** Here offsetInDst is ignored */
int IParamsPoisson_Impl::setParams(size_t sizeNew, const double* params, size_t offsetInDst)
{
    if (sizeNew != PoissonCDFParamsDimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if (params[LAMBDA] < DBL_MIN)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParams(PoissonCDFParamsDimension, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsPoisson_Impl clone
****************************************************/
IParamsPoisson_Impl* IParamsPoisson_Impl::clone() const
{
   IParamsPoisson_Impl* params = new IParamsPoisson_Impl(*this);
   return params;
}
/***************************************************
* IParamsPoisson_Impl lambda() == getParam(0)
****************************************************/
double IParamsPoisson_Impl::lambda() const
{
   double Lambda=NAN;
   /** check rc DOES NOT make sense */
   getParam(LAMBDA, Lambda);
   return Lambda;
}
/***************************************************
* IParamsPoisson_Impl setPoissonLambda() == setParam(0)
****************************************************/
int IParamsPoisson_Impl::setPoissonLambda(double poissonLambdaNew)
{
    return setParam(LAMBDA, poissonLambdaNew);
}
/***************** IParamsPoisson Factory *****************************/
IParamsPoisson_Impl* IParamsPoisson_Impl::createEmptyParams()
{
    return new IParamsPoisson_Impl();
}
/***************** IParamsPoisson Factory *****************************/
IParamsPoisson_Impl* IParamsPoisson_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    std::string tmpStr = IParamsPoisson::grammar[LAMBDA];
    if (postfix != "") tmpStr += postfix;
    iteratorParamsMap itParam = mapParams.find(tmpStr);
    double lambda=NAN;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), lambda); //OLD          lambda = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

   IParamsPoisson_Impl* params = new IParamsPoisson_Impl();
   if(! params)
   {
       return NULL;
   }
   rc = params->setPoissonLambda(lambda);
   if(_RC_SUCCESS_ != rc)
   {
       delete params; params = NULL;
       return NULL;
   }
   return params;
}

/***************************************************
*       IParamsPareto_Impl Implementation
****************************************************/
/** static in IParamsPareto interface */
/** DEFINITION! Not declaration! */
const size_t IParamsPareto::ParetoCDFParamsDimension;
/** tokens to parse map for ParetoParams
 static in IParamsPareto interface
*/
/** DEFINITION! Not declaration! */
constexpr const char * IParamsPareto::grammar[FAKE_INDEX_GRAMMAR_PARETO];

/***************************************************
* IParamsPareto_Impl DTOR
****************************************************/
IParamsPareto_Impl::~IParamsPareto_Impl()
{
#ifdef _DEBUG_
//    std::cout<< "ParamsPareto is deleting" << std::endl;
#endif
}
/***************************************************
* Private IParamsPareto_Impl CTOR
****************************************************/
IParamsPareto_Impl::IParamsPareto_Impl() : IParams_Impl(ParetoCDFParamsDimension)
{
    setParetoXm(NAN);
    setParetoK(NAN);
    m_paramsType = IParams::ParamsTypes::ParetoParams;
}

/***************************************************
* Private IParamsPareto_Impl Copy CTOR
****************************************************/
IParamsPareto_Impl::IParamsPareto_Impl(const IParamsPareto_Impl& paramsParetoImplSrc) : IParams_Impl(ParetoCDFParamsDimension)
{
       /** deep copy */
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsParetoImplSrc;
    size_t size = ParetoCDFParamsDimension;
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
   m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
}

/***************************************************
* Private IParamsPareto_Impl Assignment
****************************************************/
IParamsPareto_Impl& IParamsPareto_Impl::operator=(const IParamsPareto_Impl& paramsParetoImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsParetoImplSrc)
        return *this;
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsParetoImplSrc;
    size_t size = ParetoCDFParamsDimension;
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
   m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
    return *this;
}
/***************************************************
* IParamsPareto_Impl toString
****************************************************/
std::string IParamsPareto_Impl::toString() const
{
    std::stringstream ss;
    ss << grammar[X_MIN] << DELIMITER_NAME_VALUE << xM() <<
    DELIMITER_TO_STRING_ITEMS <<
    grammar[K] << DELIMITER_NAME_VALUE << k();
    return ss.str();
}
/***************************************************
* IParamsPareto_Impl setParam
****************************************************/
int IParamsPareto_Impl::setParam(size_t index, double value)
{
    if (index > ParetoCDFParamsDimension - 1)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if (value < DBL_MIN)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
}

/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsPareto_Impl setParams
****************************************************/
/** Here offsetInDst is ignored */
int IParamsPareto_Impl::setParams(size_t sizeNew, const double* params, size_t offsetInDst)
{
    if (sizeNew != ParetoCDFParamsDimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    for(size_t i=0; i < ParetoCDFParamsDimension; ++i)
    {
        if (params[i] < DBL_MIN)
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    IParams_Impl::setParams(ParetoCDFParamsDimension, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsPareto_Impl setParetoXm
****************************************************/
int IParamsPareto_Impl::setParetoXm(double XmNew)
{
    return setParam(X_MIN, XmNew);
}
/***************************************************
* IParamsPareto_Impl setParetoK
****************************************************/
int IParamsPareto_Impl::setParetoK(double kNew)
{
    return setParam(K, kNew);
}
/***************************************************
* IParamsPareto_Impl getParetoXm
****************************************************/
double IParamsPareto_Impl::xM() const
{
    double X_min=NAN;
    /** Here rc check DOES NOT make sense */
    getParam(X_MIN, X_min);
    return X_min;
}
/***************************************************
* IParamsPareto_Impl getParetoK
****************************************************/
double IParamsPareto_Impl::k() const
{
    double k=NAN;
    /** Here rc check DOES NOT make sense */
    getParam(K, k);
    return k;
}
/***************************************************
* IParamsPareto_Impl clone
****************************************************/
IParamsPareto_Impl* IParamsPareto_Impl::clone() const
{
    IParamsPareto_Impl* params = new IParamsPareto_Impl(*this);
    return params;
}
/***************** IParamsPareto Factory *****************************/
IParamsPareto_Impl* IParamsPareto_Impl::createEmptyParams()
{
    return new IParamsPareto_Impl();
}
/***************** IParamsPareto Factory *****************************/
IParamsPareto_Impl* IParamsPareto_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    double xM = NAN;
    std::string tmpStr = IParamsPareto::grammar[X_MIN];
    if (postfix != "") tmpStr += postfix;

    iteratorParamsMap itParam = mapParams.find(tmpStr);
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), xM); //OLD          xM = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    double k = NAN;
    tmpStr = IParamsPareto::grammar[K];
    if (postfix != "") tmpStr += postfix;
    itParam = mapParams.find(tmpStr);
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), k); //OLD          k = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    IParamsPareto_Impl* params = new IParamsPareto_Impl();
    if(! params)
    {
        return NULL;
    }
    rc = params->setParetoXm(xM);
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params = NULL;
        return NULL;
    }
    rc = params->setParetoK(k);
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params = NULL;
        return NULL;
    }
    return params;
}

/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl
****************************************************/
/**  static in IParamsPoissonDrvnParetoJumpsExpMeanReverting interface */
/** DEFINITION! Not declaration! */
const size_t IParamsPoissonDrvnParetoJumpsExpMeanReverting::PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension;
/** tokens to parse map for PoissonDrvnParetoJumpsExpMeanRevertingParams
 static in IParamsPoissonDrvnParetoJumpsExpMeanReverting interface
*/
/** DEFINITION! Not declaration! */
constexpr const char * IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[FAKE_INDEX_GRAMMAR_POISSONDRVN_PARETOJUMPS_EXPMR];

/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl DTOR
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::~IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl()
{
#ifdef _DEBUG_
//    std::cout<< "ParamsPoissonDrvnParetoJumpsExpMeanReverting is deleting" << std::endl;
#endif
}

/***************************************************
* Private IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl CTOR
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl() : IParams_Impl(PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension)
{
    setPoissonLambda(NAN);
    setMRLambda(NAN);
    setParetoXm(NAN);
    setParetoK(NAN);
    m_paramsType = IParams::ParamsTypes::PoissonDrvnParetoJumpsExpMeanRevertingParams;
}
/***************************************************
* Private IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl Copy CTOR
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc) : IParams_Impl(PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension)
{
    /** deep copy */
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc;
    size_t size = PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension;
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
   m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
}
/***************************************************
* Private IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl Assignment
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::operator=(const IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl& paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc)
        return *this;
    IParams_Impl * ptrParamsImpl=(IParams_Impl *)this;
    IParams_Impl * ptrParamsImplSrc= (IParams_Impl *)&paramsPoissonDrvnParetoJumpsExpMeanRevertingImplSrc;
    size_t size = PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension;
    double * ptrDblAray =new double[size];
    std::memset(ptrDblAray, 0, size*sizeof(double));
    /** Here rc check DOES NOT make sense */
    ptrParamsImplSrc->getParams(size, ptrDblAray);
    ptrParamsImpl->setParams(size, ptrDblAray);
    m_paramsType = ptrParamsImplSrc->getParamsType();
    delete [] ptrDblAray;
    ptrDblAray = NULL;
    return *this;
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl toString
****************************************************/
std::string IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::toString() const
{
    std::stringstream ss;
    ss << grammar[LAMBDA] << DELIMITER_NAME_VALUE << lambda()
    << DELIMITER_TO_STRING_ITEMS <<
    grammar[MR_LAMBDA] << DELIMITER_NAME_VALUE << MR_lambda()
    << DELIMITER_TO_STRING_ITEMS <<
    grammar[X_min] << DELIMITER_NAME_VALUE << xM()
    << DELIMITER_TO_STRING_ITEMS <<
    grammar[K_] << DELIMITER_NAME_VALUE << k();

    return ss.str();
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl clone
****************************************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::clone() const
{
    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* params = new IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl(*this);
    return params;
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParam
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParam(size_t index, double value)
{
    if (index > PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension - 1)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if ((index != MR_LAMBDA) && (value < DBL_MIN))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams_Impl::setParam(index, value);
    return _RC_SUCCESS_;
}
/***
* offset = 0 by default and is redundant here!
*/
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParams
****************************************************/
/** Here offsetInDst is ignored */
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParams(size_t sizeNew, const double* params, size_t offsetInDst)
{
    if (sizeNew != PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    for(size_t i=0; i < PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension; ++i)
    {
        if ((i != MR_LAMBDA) && (params[i] < DBL_MIN))
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    IParams_Impl::setParams(PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension, params);
    return _RC_SUCCESS_;
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setPoissonLambda
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setPoissonLambda(double poissonLambdaNew)
{
   return setParam(LAMBDA, poissonLambdaNew);
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setMeanRevertLambda
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setMRLambda(double MR_lambdaNew)
{
    return setParam(MR_LAMBDA, MR_lambdaNew);
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParetoXm
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParetoXm(double xMNew)
{
    return setParam(X_min, xMNew);
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParetoK
****************************************************/
int IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::setParetoK(double kNew)
{
    return setParam(K_, kNew);
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl getPoissonLambda
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::lambda() const
{
   double lambda=NAN;
   /** Here rc check DOES NOT make sense */
   getParam(LAMBDA, lambda);
   return lambda;
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl getMeanRevertLambda
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::MR_lambda() const
{
   double MR_lambda=NAN;
   /** Here rc check DOES NOT make sense */
   getParam(MR_LAMBDA, MR_lambda);
   return MR_lambda;
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl getParetoXm
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::xM() const
{
   double xMin=NAN;
   /** Here rc check DOES NOT make sense */
   getParam(X_min, xMin);
   return xMin;
}
/***************************************************
* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl setParetoK
****************************************************/
double IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::k() const
{
   double k=NAN;
   /** Here rc check DOES NOT make sense */
   getParam(K_, k);
   return k;
}
/***************** IParamsPoissonDrvnParetoJumpsExpMeanReverting Factory *****************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyParams()
{
    return new IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
}
/***************** IParamsPoissonDrvnParetoJumpsExpMeanReverting Factory *****************************/
IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::parseAndCreateParams(std::map<std::string, std::string>& mapParams, std::string postfix)
{
    double lambda = NAN;
    std::string tmpStr = IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[LAMBDA];
    if (postfix != "") tmpStr += postfix;
    iteratorParamsMap itParam = mapParams.find(tmpStr);
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), lambda); //OLD          lambda = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    double MR_lambda = NAN;
    tmpStr = IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[MR_LAMBDA];
    if (postfix != "") tmpStr += postfix;
    itParam = mapParams.find(tmpStr);
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), MR_lambda); //OLD          MR_lambda = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    double xM = NAN;
    tmpStr = IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[X_min];
    if (postfix != "") tmpStr += postfix;
    itParam = mapParams.find(tmpStr);
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), xM); //OLD          xM = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    double k = NAN;
    tmpStr = IParamsPoissonDrvnParetoJumpsExpMeanReverting::grammar[K_];
    if (postfix != "") tmpStr += postfix;
    itParam = mapParams.find(tmpStr);
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), k); //OLD          k = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
        return nullptr;

    IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl* params = new IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl();
    if(!params)
        return nullptr;
    rc = params->setPoissonLambda(lambda);
    /**
     We can not ignore above rc! Because lambda MUST be positive!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=nullptr;
        return nullptr;
    }
    rc = params->setMRLambda(MR_lambda);
    /**
     We can not ignore above rc! Because MR_lambda MUST be positive!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=nullptr;
        return nullptr;
    }
    rc = params->setParetoXm(xM);
    /**
     We can not ignore above rc! Because xM MUST be positive!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=nullptr;
        return nullptr;
    }
    rc = params->setParetoK(k);
    /**
     We can not ignore above rc! Because k MUST be positive!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=nullptr;
        return nullptr;
    }
    return params;
}

/****************************************************************************************
*                 IParamsUnion IMPLEMENTATION
****************************************************************************************/
/*****************************************
*       IParamsUnion_Impl CTOR()
*****************************************/
IParamsUnion_Impl::IParamsUnion_Impl(size_t totalOfItems, const size_t* sizesOfItems)
{
    m_totalOfItems=0;
    m_items=NULL;
    if((! totalOfItems) || (! sizesOfItems))
    {
        return;
    }
    reallocateItems(totalOfItems, sizesOfItems);
}
/*****************************************
*       IParamsUnion_Impl CTOR()
*****************************************/
IParamsUnion_Impl::IParamsUnion_Impl(size_t totalOfItems, const IParams** items) : IParamsUnion()
{
    m_totalOfItems=0;
    m_items=NULL;
    if((! totalOfItems) || (! items))
    {
        return;
    }
    int rc = this->setItems(totalOfItems, items);
    if (_RC_SUCCESS_ != rc)
    {
        reallocatePtrToItems(0);
    }
    return;
}
/*****************************************
*       IParamsUnion_Impl DTOR()
*****************************************/
IParamsUnion_Impl::~IParamsUnion_Impl()
{
#ifdef _DEBUG_
std::cout << "into ~IParamsUnion_Impl :" <<  std::endl;
#endif
  for(size_t i=0; i<m_totalOfItems; ++i)
  {
      delete m_items[i];
      m_items[i]=NULL;
  }
  delete [] m_items;
  m_items = NULL;
  m_totalOfItems = 0;
}
/*****************************************
*  IParamsUnion_Impl reallocatePtrToItems
*****************************************/
int IParamsUnion_Impl::reallocatePtrToItems(size_t totalOfItems)
{
    if (NULL != m_items)
    {
        for(size_t i=0; i < m_totalOfItems; ++i)
        {
           if (NULL != m_items[i])
               delete m_items[i];
           m_items[i]=NULL;
        }
    }
    delete [] m_items;
    m_items=NULL;
    m_totalOfItems=0;

    if(! totalOfItems)
    {
        return _RC_SUCCESS_;
    }
    m_items = new IParams*[totalOfItems];
    if (! m_items)
    {
        return _ERROR_NO_ROOM_;
    }

    for (size_t i=0; i < totalOfItems; ++i)
    {
        m_items[i]=NULL;
    }
    m_totalOfItems = totalOfItems;
    return _RC_SUCCESS_;
}
/*****************************************
*  IParamsUnion_Impl reallocateItems
*****************************************/
int IParamsUnion_Impl::reallocateItems(size_t totalOfItems, const size_t* sizesOfItems)
{
    int rc = reallocatePtrToItems(totalOfItems);
    if (rc)
    {
        return rc;
    }
    for(size_t i=0; i<totalOfItems; ++i)
    {
/// Init m_items!
        if (sizesOfItems[i])
        {
            /**
            Here we pass NULL because Params1D type already implemented
            So, NO ERROR expected.
            */
            m_items[i]= IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, sizesOfItems[i]);
        }
        else
            return _ERROR_WRONG_ARGUMENT_;
    }
    return _RC_SUCCESS_;
}
/*****************************************
*  IParamsUnion_Impl setItem
*****************************************/
int IParamsUnion_Impl::setItem(size_t index, const IParams* itemParams)
{
    if(index >= m_totalOfItems)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(!itemParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    delete m_items[index];
    m_items[index] = NULL;
    m_items[index] = itemParams->clone();
    return _RC_SUCCESS_;
}
/*****************************************
*  IParamsUnion_Impl setItems
*****************************************/
int IParamsUnion_Impl::setItems(size_t size, const IParams** itemsParams)
{
    if(!itemsParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(size != m_totalOfItems)
    {
        int rc = reallocatePtrToItems(size);
        if(rc != _RC_SUCCESS_)
        {
            return rc;
        }
    }
    for(size_t i=0; i<size; ++i)
    {
        if(itemsParams[i])
        {
            setItem(i, itemsParams[i]);
        }
        else
            return _ERROR_WRONG_ARGUMENT_;
    }
    return _RC_SUCCESS_;
}
/*****************************************
*  IParamsUnion_Impl getItemCopy
* to clone/copy of existing m_items[index]
*****************************************/
int IParamsUnion_Impl::getItemCopy(size_t index, IParams*& itemParams) const
{
    if(index >= m_totalOfItems)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
/* the reference PROVE addendParams is NOT NULL
BUT VALUE of PTR may be NULL!
    if(!addendParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
*/
    itemParams = m_items[index]->clone();
    return _RC_SUCCESS_;
}
/*******************************************************************
*         IParamsUnion_Impl ptrIParamsUnionToPtrIParams
* Get ptr IParams into IParamsUnion by it's index
i.e. PARAM [IN] index -- the index of IParams* in this IParamsUnion
*******************************************************************/
IParams* IParamsUnion_Impl::ptrIParamsUnionToPtrIParams(size_t index) const
{
    if(index >= m_totalOfItems)
    {
        return NULL;
    }
    IParams * ptrIParams = m_items[index];
    return ptrIParams;
}
/*******************************************************************
*         IParamsUnion_Impl getItemsCopies
* to clone/copy of existing m_items
*******************************************************************/
int IParamsUnion_Impl::getItemsCopies(size_t size, IParams**& itemsParams) const
{
    if(!itemsParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(size > m_totalOfItems)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    for(size_t i=0; i<size; ++i)
    {
        /** to clone/copy of existing m_items[i] */
        int rc = getItemCopy(i, itemsParams[i]);
        if (rc)
        {
            return rc;
        }
    }
    return _RC_SUCCESS_;
}
/*******************************************************************
*         IParamsUnion_Impl toString
*******************************************************************/
std::string IParamsUnion_Impl::toString() const
{
    std::stringstream ss;
    for(size_t i=0; i<m_totalOfItems; ++i)
    {
        ss <<  m_items[i]->toString();
        if(i < m_totalOfItems-1)
        {
            ss << DELIMITER_TO_STRING_ITEMS;
        }
    }
    return ss.str();
}
/*******************************************************************
*         IParamsUnion_Impl getTotalOfItemsInParamsUnion
*******************************************************************/
size_t IParamsUnion_Impl::getTotalOfItemsInParamsUnion() const
{
    return m_totalOfItems;
}
/*******************************************************************
*         IParamsUnion_Impl getSize
*******************************************************************/
size_t IParamsUnion_Impl::getSize() const
{
    size_t size = 0;
    for(size_t i = 0; i < m_totalOfItems; ++i)
    {
        size +=  m_items[i]->getSize();
    }
    return size;
}
/*******************************************************************
*         IParamsUnion_Impl clone
*  makes deep copy
*******************************************************************/
IParamsUnion_Impl* IParamsUnion_Impl::clone() const
{
    /** It's DEEP! Not Shallow copy */
     IParamsUnion_Impl* pu = new IParamsUnion_Impl(*this);
     return pu;
}
/***************************************************
* Very BAD!!! Public IParamsUnion_Impl Copy CTOR
****************************************************/
IParamsUnion_Impl::IParamsUnion_Impl(const IParamsUnion_Impl& paramsUnionImplSrc) : IParamsUnion()
{
    m_totalOfItems = 0;
    m_items        = NULL;
    size_t size = paramsUnionImplSrc.getTotalOfItemsInParamsUnion();
    /** deep copy */
///TODO!!! Very BAD!!!
    if (!size)
        return;// nullptr;

    int rc = this->reallocatePtrToItems(size);

    IParams** ptrItemParams = new IParams*[size];
    /** to clone/copy existing m_items */
    rc = paramsUnionImplSrc.getItemsCopies(size, ptrItemParams);
    if (rc)
    {
///TODO!!! HOW Log?
    }
    /** finalize deep copy */
    rc = this->setItems(size, (const IParams**)ptrItemParams);
///TODO!!! check that cloned copies deleted
    for(size_t i=0; i< size; ++i)
    {
        delete ptrItemParams[i];
        ptrItemParams[i] = NULL;
    }
    delete [] ptrItemParams;
    if (rc)
    {
///TODO!!! HOW Log?
    }
}
/***************************************************
* Very BAD!!! Public IParamsUnion_Impl Assignment
****************************************************/
IParamsUnion_Impl& IParamsUnion_Impl::operator=(const IParamsUnion_Impl& paramsUnionImplSrc)
{
    /** check for self-assignment */
    if (this == &paramsUnionImplSrc)
        return *this;
    size_t size = paramsUnionImplSrc.getTotalOfItemsInParamsUnion();
      /** deep copy */
    if (!size)
        return *this;

    int rc = this->reallocatePtrToItems(size);
    IParams** itemParams = new IParams*[size];
    /** the clones/copies of existing m_items retrieved */
    rc = paramsUnionImplSrc.getItemsCopies(size, itemParams);
    /** finalize deep copy */
    rc = this->setItems(size, (const IParams**)itemParams);
    for(size_t i=0; i< size; ++i)
    {
        delete itemParams[i];
        itemParams[i] = NULL;
    }
    delete [] itemParams;
    itemParams = NULL;
    if (rc)
    {
///TODO!!! HOW Log?
    }
     return *this;
}

/***********************************************************************
* static public interface to make IParamsUnion* of Params1D
************************************************************************/
IParamsUnion* IParamsUnion::createParamsUnion(size_t totalOfItems, const size_t* sizesOfItems, Logger * logger)
{
    return new IParamsUnion_Impl(totalOfItems, sizesOfItems);
}
/***********************************************************************
* static  public interface to make IParamsUnion* by cloning IParams** addends
************************************************************************/
IParamsUnion* IParamsUnion::createParamsUnion(size_t totalOfItems, const IParams** items, Logger * logger)
{
    for(size_t i=0; i < totalOfItems; ++i)
    {
        if (items[i] == NULL)
        {
            logger->error(WORKFLOW_TASK_ERROR, i + "-th Params NOT FOUND in createParamsUnion() to create ParamsUnion!");
            return NULL;
        }
    }
    return new IParamsUnion_Impl(totalOfItems, items);
}
/*****************************************************************
* Static factory : Empty Params Interface Factory
*****************************************************************/
/*** NB size_t size is in use for Params1D ONLY!
*/
IParams* IParams::createEmptyParams(Logger* logger, enum IParams::ParamsTypes pt, size_t size)
{
    switch(pt)
    {
      case Params1D:
      {
        return new IParams_Impl(size);
      }
      case GaussianParams:
      {
        return IParamsGaussian_Impl::createEmptyParams();
      }
      case WienerParams:
      {
        return IParamsWiener_Impl::createEmptyParams();
      }
      case PoissonParams:
      {
        return IParamsPoisson_Impl::createEmptyParams();
      }
      case ParetoParams:
      {
        return IParamsPareto_Impl::createEmptyParams();
      }
      case PoissonDrvnParetoJumpsExpMeanRevertingParams:
      {
        return IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::createEmptyParams();
      }
      default:
///TODO!!! log->error!
        logger->error(WORKFLOW_TASK_ERROR, pt + "-th Params type is UNKNOWN! Can not create Empty Params of that type in IParams::createEmptyParams()!");
        return NULL;
   }
}
/*****************************************************************
* Static factory : All Params Interface Factory
*****************************************************************/
IParams* IParams::parseAndCreateParams(enum IParams::ParamsTypes pt, std::map<std::string, std::string>& mapParams, std::string postfix, Logger* logger)
{
   switch(pt)
   {
     case Params1D:
     {
         return IParams_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case GaussianParams:
     {
         return IParamsGaussian_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case WienerParams:
     {
         return IParamsWiener_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case ParetoParams:
     {
         return IParamsPareto_Impl::parseAndCreateParams( mapParams, postfix);
     }
     case PoissonDrvnParetoJumpsExpMeanRevertingParams:
     {
         return IParamsPoissonDrvnParetoJumpsExpMeanReverting_Impl::parseAndCreateParams( mapParams, postfix);
     }
     default:
       logger->error(WORKFLOW_TASK_ERROR, pt + "-th Params type is UNKNOWN! Can not parse map and create Params of that type in IParams::parseAndCreateParams()!");
       return NULL;
   }
}

/**********************************************************************
* static math stuff for IParams
***********************************************************************/
int IParams::norm2(const IParams* point1, const IParams* point2, double& norm)
{
    size_t dim = point1->getSize();
    if(dim != point2->getSize())
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    for(size_t i=0; i < dim; ++i)
    {
        double tmpVal1=0.0f, tmpVal2=0.0f;
        point1->getParam(i, tmpVal1);
        point2->getParam(i, tmpVal2);
        norm +=(tmpVal1 - tmpVal2)*(tmpVal1 - tmpVal2);
    }
    norm = sqrt(norm);
    return _RC_SUCCESS_;
}
/**********************************************************************
* static math stuff for IParamsUnion
***********************************************************************/
// ������ �� ����� ��������� ���������
int IParamsUnion::norm2(const IParamsUnion *point1, const IParamsUnion *point2, double& dist)
{
    if((!point1) || (!point2))
    {
        return _ERROR_NO_INPUT_DATA_;
    }
    size_t dim = point1->getTotalOfItemsInParamsUnion();
    if (dim != point2->getTotalOfItemsInParamsUnion()){
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams * ptrIParams1;
    IParams * ptrIParams2;
    for(size_t i = 0; i < dim; ++i)
    {
        ptrIParams1 = point1->ptrIParamsUnionToPtrIParams(i);
        ptrIParams2 = point2->ptrIParamsUnionToPtrIParams(i);
        if(ptrIParams1->getSize() != ptrIParams2->getSize())
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    for(size_t i = 0; i < dim; ++i)
    {
        ptrIParams1 = point1->ptrIParamsUnionToPtrIParams(i);
        ptrIParams2 = point2->ptrIParamsUnionToPtrIParams(i);
        double tmpVal=0.0f;
        if( _RC_SUCCESS_!= IParams::norm2( (const IParams*) ptrIParams1, (const IParams*) ptrIParams2, tmpVal))
        {
            return _ERROR_WRONG_MODEL_PARAMS_;
        }
        dist += (tmpVal)*(tmpVal);
    }
    dist = sqrt(dist);
    return _RC_SUCCESS_;
}

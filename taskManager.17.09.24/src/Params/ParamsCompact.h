#ifndef __PARAM_COMPACT_H__633569422542968750
#define __PARAM_COMPACT_H__633569422542968750

#include <string>
#include <vector>

// DON'T pollute global scope! using namespace std;

#include "Params.h"
#include "../Log/Log.h"

/** forward declarations */
class IModel;
/** Interface declaration here!
    Implementation (private) MOVEd to cpp
*/
/**
���������� IParamsCompact �� ����� ������������ ���������� ��� �������� ����� ��������,
������ ��� �������� ����� ��������� ����� ���� ����� 10^6!
�� �� ��� � �� ����� �������, �.�. ����� ����� ������������ �� id
(����� � �������� ����� �� ����� ����������)
�� ����� ��������������� �� IParamsSet
� ������� ������������ ��������
� ���������� IParamsSet (� ���� ���� private ������ points)
*/
class IParamsCompact //���� ��� �� ����� : public virtual IParamsSet
{
public:
    enum indexesOfGrammarForCompact
    {
        indexOfIncrementType, /** 0 = STEP_VALUE, 1 = TOTAL_STEPS */
        indexOfIncrementValue,
        FAKE_INDEX_GRAMMAR
    };

/* OLD C++ < C++11    static const char * grammar[FAKE_INDEX_GRAMMAR]; */
    static constexpr const char * grammar[FAKE_INDEX_GRAMMAR]=
    {
        "IncrementType", /** 0 = STEP_VALUE, 1 = TOTAL_STEPS */
        "IncrementValue"
    };
    /** static factory ��� �������� ����������� IParamsCompact
    */
    static IParamsCompact* createParamsCompact(const IModel* model,  IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger);

    IParamsCompact() {};
    virtual ~IParamsCompact() {};
    virtual size_t getDimOfParams() const =0;
    virtual size_t getSize() const =0;

    virtual std::string toString() const =0;

    static const int IMPOSSIBLE_RELATIVE_COORDINATE = -1;
    virtual int    evaluateRelativeCoordAlongGivenAxis(size_t indexOfAxis, const IParams * const point, size_t& relativeCoord) const =0;
    virtual size_t getMinRelativeSizeAlongAxis() const =0;
    virtual size_t getMaxRelativeSizeAlongAxis() const =0;

    virtual size_t getCurrentId() const =0;
    virtual int getId_byIParams(const IParams * const point, size_t& id) const =0;
    virtual int setCurrentId(size_t id) =0;
    /**
    � ������� ���� ������� �� ������ ����� ������� ������ ��������,
    �.�.����� ��������� ������� ���������� (���������) � �� ��������������� ������������
    */
    virtual int setOrderOfCoordsToWalkThroughCompact(size_t size, const size_t * newOrder) = 0;
    /**
    ���������, ��������� �� ������ �� ���� ���� ����������,
    �� ����� ���� ����� ������ �������� ��������������� ������
    ������� ����������� ��� ������ ����� �� ���� �������
    �.�. ��� ����� ��� ������������� ��������������! ������� �� ���������� ���������
    */
    virtual IParams* getFirstPtr() =0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getNextPtr() =0;
    /**
    ���������, ������������ ���� ��������,
    �����, �� ���������� ��������������! ������� �� ���������� ���������
    */
    virtual IParams* getParamsCopy(size_t id) const =0;
    /**
    ���������, ������������ ���� ��������,
    �����, �� ���������� ��������������! ������� �� ���������� ���������
    */
    virtual IParams* getIncrementsCopy() const =0;
    virtual bool isEnd() const =0;
    virtual bool isPointInCompact(const IParams* const point) const =0;
};
/** Interface declaration here!
    Implementation (private) MOVEd to cpp
 */
/**
���������� IParamsUnionCompact �� ����� ������������ ���������� ��� �������� ����� ��������,
������ ��� �������� ����� ��������� ����� ���� ����� 10^6!
�� �� ��� � �� ����� �������, �.�. ����� ����� ������������ �� id
(����� � �������� ����� �� ����� ����������)
�� ����� ��������������� �� IParamsUnionSet
�  ������� ������������ ��������
� ���������� IParamsUnionSet (� ���� ���� private ������ points)
*/
/** forward declaration */
class IAdditiveModel;

class IParamsUnionCompact //���� �� ����� : public virtual IParamsUnionSet
{
public:
    /** static factory ��� �������� ����������� IParamsUnionCompact
    */
    static IParamsUnionCompact* createParamsUnionCompact(const IAdditiveModel* additiveModel,  IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool stepValue, Logger* logger);

    IParamsUnionCompact() {};
    virtual ~IParamsUnionCompact() {};

    virtual size_t getDimOfParamsUnion() const =0;
    virtual size_t getSize() const =0;

    virtual std::string toString() const =0;

    static const int IMPOSSIBLE_RELATIVE_COORDINATE = -1;
    virtual int    evaluateRelativeCoordAlongGivenAxis(size_t indexOfAxis, const IParamsUnion * const point, size_t& relativeCoord) const =0;
    virtual size_t getMinRelativeSizeAlongAxis() const =0;
    virtual size_t getMaxRelativeSizeAlongAxis() const =0;

    virtual size_t getCurrentId() const =0;
    virtual int getId_byIParamsUnion(const IParamsUnion * const point, size_t& id) const =0;
    virtual int setCurrentId(size_t id) =0;
    /**
    � ������� ���� ������� �� ������ ����� ������� ������ ��������,
    �.�.����� ��������� ������� ���������� (���������) � �� ��������������� ������������
    */
    virtual int setOrderOfCoordsToWalkThroughCompact(size_t size, const size_t * newOrder) = 0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getFirstPtr() =0;
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getNextPtr() =0;
    /**
    ���������, ������������ ���� ��������,
    �����, �� ���������� ��������������! ������� �� ���������� ���������
    */
    virtual IParamsUnion* getParamsUnionCopy(size_t id) const =0;
    /**
    ���������, ������������ ���� ��������,
    �����, �� ���������� ��������������! ������� �� ���������� ���������
    */
    virtual IParamsUnion* getIncrementsCopy()const =0;
    virtual bool isEnd() const =0;
    virtual bool isPointInCompact(const IParamsUnion* const point) const =0;
};

#endif

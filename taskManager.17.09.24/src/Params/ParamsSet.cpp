#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include "Params.h"
#include "ParamsSet.h"
#include "../Log/errors.h" /* rc codes */
#include "../Config/ConfigParser.h"
/**
��-��������, ����������� �����, ������� ����� � ���� �����,
����� � ������� �� �����!
��, �� �����������, ��� � ����� � ��� �� ���� �� ����� ������ Params/ParamsUnion �� ������ �������.
����� ������������ �� �� ������� ���������.
*/
#include "../Models/Model.h"/* IModel */
#include "../Models/AdditiveModel.h"/* IAdditiveModel */

namespace{
/** IParamsSet Implementation MOVED here
 ��������� ��� �������� ������ ������ �����
 ���������� (�� ����������) ������!
 �� ��� �� ����� ������������ � ��� ������������ ������
 ������� (�� ����������) ������
 ������ ����� ���������� ��������� ��������� ����� Set'�,
 ����� ����� ���� ������, ��� ������ �������, � ��� - �������.
 ??? ������/�������� �������� ������� points?

 �������� �������� ������: �� ����������� � ���� ����
 ��������� ������ ����� ���������� ������?
 ��� � ���� ����� ������ ��������� ����� ������?
*/
/*************************************************************
*               Set of Points in Euclidean Space
*************************************************************/
class IParamsSet_Impl : public virtual IParamsSet
{
public:
///TODO!!! ������� const IModel* m_model, ��� ����� ���������� �� setModel!
/// �.�. ���������������� ��� ���� ����� ��, � CTOR'�
/// � ����� � ������� ������� ����� ��� �� ������
    IParamsSet_Impl(const IModel* model);
    ~IParamsSet_Impl();
    virtual void clear();
    /**
     �������� �������� ������: �� ����������� � ���� ����
     ��������� ������ ����� ���������� ������?
     ��� � ���� ����� ������ ��������� ����� ������?
     � ��������� ������ ��������� �������� �������������
     ��������� ���������� ����������.

    */
    /** ��������� ��������� ������
    � ���������� vector<IParams*> points,
    ����� ��������� ��� (��������� � ����)
    �� ����� ����� �� ��������! */
    /** this method requires virtual castModelParams() in IModel-interface */
    virtual bool addParamsCopyToSet(const IModel* model, const IParams* params, Logger* log);
    /** this method requires virtual createModelParams() in IModel-interface */
    virtual bool fillParamsSet(const IModel* model, const std::string& srcName, Logger * log);

///REDUNDANT virtual int setModel(const IModel* model, Logger* log);
    virtual size_t getSetSize() const;
    virtual int getCurrentId() const;
    virtual int setCurrentId(size_t id);
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getNextPtr();
    virtual IParams* getParamsCopy(size_t id) const;
    /** check if end is reached */
    virtual bool isEnd() const;

private:
    /** container to keep points (IParams) of this SET */
    /** ���������� ��� ������ ����� �����,
    � �������� ������ ����� � ������� getSetSize()
    */
    std::vector<IParams*> m_points;
    /** id of current IParams in this Set */
    int m_id;
    /** while walking with getNextPtr()
    the very Last Params has been retrieved from Set
    */
    bool m_end;
    const IModel* m_model;
};
/*************************************************************
*    Set of Points in Cartesian Product of Euclidean Space
*************************************************************/
/************************************************
IParamsUnionSet Implementation MOVED here
 ��������� ��� �������� ������ ������ �����
 ���������� ������!
 �� ��� �� ����� ������������ � ��� ������������ ������
 ���������� ������
  ������ ����� ���������� ��������� ��������� ����� Set'�,
 ����� ����� ���� ������, ��� ������ �������, � ��� - �������.
??? ������/�������� �������� ������� points?
*/
class IParamsUnionSet_Impl : public virtual IParamsUnionSet
{
public:
///TODO!!! ������� const IAdditiveModel* m_additiveModel, ��� ����� ���������� �� setModel!
/// �.�. ���������������� ��� ���� ����� ��, � CTOR'�
/// � ����� � ������� ������� ����� ��� �� ������
    IParamsUnionSet_Impl(const IAdditiveModel* additiveModel);
    ~IParamsUnionSet_Impl();

    /** this method requires virtual compareModels() in IAdditiveModel-interface */
    virtual bool addParamsUnionCopy(const IAdditiveModel* model, IParamsUnion* params, Logger* log);

    virtual bool fillParamsUnionSet(const IAdditiveModel* model, const std::string& srcName, Logger * log);

///REDUNDANT virtual int setModel(const IAdditiveModel* additiveModel, Logger* log);
    virtual size_t getSetSize() const;
    virtual int getCurrentId() const;
    virtual int setCurrentId(size_t id);
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParamsUnion* getNextPtr();
    virtual IParamsUnion* getParams(size_t id);

    virtual bool isEnd() const;

private:
    /** container to keep items (IParamsUnion) of this SET */
    std::vector<IParamsUnion*> m_points;
    /** id of current ARRAY IParamsUnion in this Set */
    int m_id;
    /** while walking with getNextPtr()
    the very Last Params has been retrieved from Set
    */
    bool m_end;
    const IAdditiveModel* m_additiveModel;
};

} ///end of anonymous namespace
/***********************************************
*                 IMPLEMENTATION
************************************************/
IParamsSet_Impl::IParamsSet_Impl(const IModel* model) : m_id(-1),
                                                       m_end(true),
                                                       m_model(model)

{}

IParamsSet_Impl::~IParamsSet_Impl()
{
    this->clear();
}

void IParamsSet_Impl::clear()
{
    for(size_t i=0; i < m_points.size(); ++i)
    {
        if(m_points[i]){
            delete m_points[i]; m_points[i]=nullptr;
        }
    }
    m_points.clear();
    m_id=-1;
}
/***
* Fill set with params values from file/db.
*********************************************/
//TODO!!!
bool IParamsSet_Impl::fillParamsSet(const IModel* model, const std::string& srcName, Logger * log)
{
    /** ������ ��������� ������������ ��� IModel,
    ����� ���������, ��� ������ ��������� ������,
    ��� ������� �� ���� ��������� � �������
    */
    bool bRC = true;
    if (m_model != NULL)
    {
        bRC = IModel::compareModels(m_model, model);
        if (! bRC)
        {
            log->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong model type in ParamsSet::fillParams()!");
            return false;
        }
    }
    else
    {
        /** This Cheating happens once for all ParamsSet life-time ! */
        m_model = const_cast<IModel *> (model);
    }
    ConfigParser config(srcName);
    if (!config.fileExists()) {
        return false;
    }

    ParamsMap mapParams = config.parse();
    iteratorParamsMap itParam = mapParams.find("NUMBER");
    if (itParam == mapParams.end()) {
        return false;
    }
    /** total of parameter to be parsed and added to the Set
    according to directive NUMBER=XXX into cfg-file
    */
    size_t index = 0;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    rc = strTo<size_t>(itParam->second.c_str(), index); //OLD size_t index = atoi(itParam->second.c_str());
    mapParams.erase(itParam);

    if((rc != _RC_SUCCESS_) || (index == 0))
    {
        return false;
    }
    /** assume in file all IModel's params defined as follows:
     PARAM_1 = value
     ...
     PARAM_index = value
    */
    for (size_t i = 1; i <= index; ++i) {
        char postfix[100];
        sprintf(postfix, "_%i", i);
///TODO!!! ����� ����������� ������
        enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
        model->getModelType(mt);
        IParams* ptrToIParams = NULL;
        int rc = IModel::parseAndCreateModelParams(mt, mapParams, postfix, &ptrToIParams, log);
        if (!rc)
        {
            break;
        }
//OLD        IParams* ptrToIParams = ptrToPtrIParams[0];
///TODO!!! ������������� ��� : "Overflow" <- !
        m_points.push_back(ptrToIParams);
    }

    if (m_points.size() < index) {
        for (size_t i = 0; i < m_points.size(); ++i) {
            delete m_points[i];
            m_points[i]=nullptr;
        }
        return false;
    }

    return true;
}

bool IParamsSet_Impl::addParamsCopyToSet(const IModel* model, const IParams* params, Logger* log)
{
///TODO!!! � ����� �������� ���� ����� � params==NULL ???
    if (!params)
    {
        return false;
    }
    /** ������ ��������� ������������ ��� IModel,
    ����� ���������, ��� ������ ��������� ������,
    ��� ������� �� ���� ��������� � �������
    */
    bool bRC = true;
    if (m_model != NULL)
    {
        bRC = IModel::compareModels(m_model, model);
        if (! bRC)
        {
            log->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong model type in ParamsSet::fillParams()!");
            return false;
        }
    }
///TODO!!! �����!
/* ��� ����, ����� �������������� ������ � ���������� ParamsSet,
   ������� ������������ ��� ������� setModel()
    else
    {
        / This Cheating happens once for all ParamsSet life-time !
        /
        if(model) m_model = const_cast<IModel *> (model);
    }
*/
    IParams * copyOfParams = params->clone();
///TODO!!! ������������� ��� : "Overflow" <- !
    m_points.push_back(copyOfParams);
    return true;
}
/*OLD
int IParamsSet_Impl::setModel(const IModel* model, Logger* log)
{
    if(!m_model)
    {
        log->log(LogLevels::ALGORITHM_1_LEVEL_INFO, "NOT NULL model is changed for ParamsSet!");
    }
    m_model = const_cast<IModel *> (model);
    return _RC_SUCCESS_;
}
*/
size_t IParamsSet_Impl::getSetSize() const
{
    return m_points.size();
}

int IParamsSet_Impl::getCurrentId() const
{
    return m_id;
}

int IParamsSet_Impl::setCurrentId(size_t id)
{
    if(id < m_points.size())
    {
        m_id = id;
        return _RC_SUCCESS_;
    }
    return _ERROR_WRONG_ARGUMENT_;
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParams* IParamsSet_Impl::getFirstPtr()
{
    if (m_points.size() < 1)
        return NULL;

    m_id = 0;
    if(m_points.size() > 1)
        m_end = false;
    return m_points[m_id];
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParams* IParamsSet_Impl::getNextPtr()
{
    ++m_id;

    if (m_id >= (int)m_points.size())
    {
        m_id = -1;
        return NULL;
    }
    /** we reached the very last Params in the set,
    So this is the END!
    */
    else if(m_id == (int)m_points.size() - 1)
    {
        m_end = true;
    }
    return m_points[m_id];
}

IParams* IParamsSet_Impl::getParamsCopy(size_t id) const
{
/** TODO!!! �������� ������ : ����� ��� ��������� ����� ������� ���������� �� ���� �� ��� � id �������� ������ ������?
    if (id < 0 || id >= points.size()) {
        m_id = -1;
        return NULL;
    }
    m_id = id;
*/
    if (id < m_points.size())
    {
      IParams * paramsCopy = m_points[id]->clone();
      return paramsCopy;
    }
    return NULL;
}

bool IParamsSet_Impl::isEnd() const
{
    return m_end;
}

/***************************************************
              IParamsUnionSet_Impl
****************************************************/
IParamsUnionSet_Impl::IParamsUnionSet_Impl(const IAdditiveModel* additiveModel) : m_id(-1),
                                               m_end(true),
                                               m_additiveModel(additiveModel)

{}
IParamsUnionSet_Impl::~IParamsUnionSet_Impl()
{
    for(size_t i=0; i < m_points.size(); ++i)
    {
        if(m_points[i]){
            delete m_points[i];
            m_points[i]=nullptr;
        }
    }
    m_points.clear();
    m_id=-1;
}

bool IParamsUnionSet_Impl::fillParamsUnionSet(const IAdditiveModel* model, const std::string& srcName, Logger * log)
{
// bool IAdditiveModel::compareModels(const IAdditiveModel* model_1, const IAdditiveModel* model_2)
///TODO!!! ��������� � ������ ����� ����� �� IAdditiveModel ����������
  return false;
}

bool IParamsUnionSet_Impl::addParamsUnionCopy(const IAdditiveModel* model, IParamsUnion* pu, Logger* log)
{
  bool isAdditiveModelsEqual = IAdditiveModel::compareModels(model, m_additiveModel);
  if ((isAdditiveModelsEqual) && (m_additiveModel->canCastModelParamsUnion(pu)))
  {
      IParamsUnion * pu_copy = pu->clone();
///TODO!!! ������������� ��� : "Overflow" <- ����� ������ ���������� ������� ���������� ������!
      m_points.push_back(pu_copy);
      return true;
  }
  return false;
}
/* OLD
int IParamsUnionSet_Impl::setModel(const IAdditiveModel* additiveModel, Logger* log)
{
    if(!m_additiveModel)
    {
        log->log(LogLevels::ALGORITHM_1_LEVEL_INFO, "NOT NULL additive model is changed for ParamsUnionSet!");
    }
    m_additiveModel = const_cast<IAdditiveModel *> (additiveModel);
    return _RC_SUCCESS_;
}
*/
size_t IParamsUnionSet_Impl::getSetSize() const
{
    return m_points.size();
}

int IParamsUnionSet_Impl::getCurrentId() const
{
    return m_id;
}

int IParamsUnionSet_Impl::setCurrentId(size_t id)
{
    if(id < m_points.size())
    {
        m_id = id;
        return _RC_SUCCESS_;
    }
    return _ERROR_WRONG_ARGUMENT_;
}
/**
 NB returns ptr to existing item into Set!
 It does Not copy! => Don't free that ptr!
*/
IParamsUnion* IParamsUnionSet_Impl::getFirstPtr()
{
    if (m_points.size() < 1)
        return NULL;

    m_id = 0;
    if(m_points.size() > 1)
        m_end = false;
    return m_points[m_id];
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParamsUnion* IParamsUnionSet_Impl::getNextPtr()
{
    ++m_id;

    if (m_id >= (int)m_points.size())
    {
        m_id = -1;
        return NULL;
    }
    /** we reached the very last Params in the set,
    So this is the END!
    */
    else if(m_id == (int)m_points.size() - 1)
    {
        m_end = true;
    }
    return m_points[m_id];
}

IParamsUnion* IParamsUnionSet_Impl::getParams(size_t id)
{
    if ( (int)id > ((int)m_points.size() - 1) ) {
//OLD        m_id = -1;
        return NULL;
    }
//OLD    m_id = id;
    return m_points[id];
}

bool IParamsUnionSet_Impl::isEnd() const
{
    return m_end;
}

///TODO!!! WE NEED FACTORY to make ParamsSet!
IParamsSet* IParamsSet::createEmptyParamsSet(const IModel* model, Logger* log)
{
///TODO!!! ������ ������ ����� ��� ����� ����������
    IParamsSet_Impl* pst = new IParamsSet_Impl(model);
///TODO!!! ��������: ����� �������� addParams, �� ����������� ������ ����������� (NULL), ������ ��� ���� ����� ��� ������ � �������������� ������ � IParamsSet!
///TODO!!! � IParamsSet ����� ��������� ������������ �����,
/// � ������� �������� ���������������� ��������� �� ������, ��������� ������� ���� IParamsSet ��������!
/// � �� ��������� ���� ��������� �� ������ � IParamsSet! = NULL
/// ��������� ����� - ��������!: ������ �� ���� IParamsSet->addParams ������� ������������� ������ IParamsSet!!!
/*
    int rc = pst->addParams(model, NULL, log);
    if (!rc)
    {
        delete pst;
        log->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "IParamsSet::createEmptyParamsSet : Failed to create empty Params Set!");
        return NULL;
    }
*/
    return pst;
}
/** TODO!!! � ������ �� ��������� ���� ����������� ������������ ���������� ������� ��� ����� ����������, �������� PLAIN_MODEL? ����� ��������� ������ ������������ IParamsSet �� IEstimator!
IParamsSet* IParamsSet::createEmptyParamsSet(const IEstimator* estimator, Logger* log)
{
///TODO!!! ��������� ������ ����� ��� ����� ����������
    log->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "createEmptyParamsSet is not implemented yet for const IEstimator* estimator!");
    return NULL;
}
*/

IParamsUnionSet* IParamsUnionSet::createEmptyParamsUnionSet(const IAdditiveModel* additiveModel, Logger* logger)
{
    /** ������ ������ ����� ��� ����� ����������
    � ���� �� ����� � ���� ���������� �������
    �������������� ������ (� �� ������ Params1D ������� ����������� ����������),
    ����� � ����������� ����� ���������� ����� �������� ��������� �� ������.
    */
    IParamsUnionSet_Impl* pust = new IParamsUnionSet_Impl (additiveModel);
    if(pust != NULL)
    {
//OLD        pust->setModel(additiveModel, logger);
        return pust;
    }
///TODO!!! ��������: ����� �������� addParams, �� ����������� ������ ����������� (NULL), ������ ��� ���� ����� ��� ������ � �������������� ������ � IParamsSet!
///TODO!!! � IParamsSet ����� ��������� ������������ �����,
/// � ������� �������� ���������������� ��������� �� ������, ��������� ������� ���� IParamsSet ��������!
/// � �� ��������� ���� ��������� �� ������ � IParamsSet! = NULL
/// ��������� ����� - ��������!: ������ �� ���� IParamsSet->addParams ������� ������������� ������ IParamsSet!!!

    return NULL;
}

#include "../CommonStuff/Defines.h" /* isNAN_double() */
#include "ParamsCompact.h"
#include "Params.h"
#include "../Models/Model.h"//IModel
#include "../Models/AdditiveModel.h"//IAdditiveModel
#include "../Log/errors.h" //rc codes
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sstream>
#include <float.h> /* DBL_MIN */

#include <assert.h>

#define BAD_ID OUR_MAX_UNSIGNED_LONG
/**************************************
* stupid static copy of IParamsCompact_Impl's member
* evaluateRelativeCoord()
* to call from static functions
***************************************/
static int staticEvaluateRelativeCoord(double curCoord, double startCoord, double increment, size_t& relativeCoord)
{
    assert(increment > DBL_MIN);
    if((curCoord < startCoord) || (increment < DBL_MIN))
    {
       return _ERROR_WRONG_ARGUMENT_;
    }

//    double tmp = roundl((curCoord - startCoord) / increment);
    double tmpStartCoord = startCoord;
    size_t tmpRelativeCoord = 0;
    while((curCoord - tmpStartCoord) > (increment/2.0f))
    {
        tmpStartCoord += increment;
        tmpRelativeCoord++;
    }
    /** ��� �������� - ����� �����, ��� ����� �������,
    ����� ������������� ���������� ������������� ����������,
    ������� � �������� �� ����� ���� � ����� �� �� �� ���� ����� ����� ���������
    */
//    if(tmp >= (double)BAD_ID){
//       return _ERROR_WRONG_ARGUMENT_;
//    }
    relativeCoord =  tmpRelativeCoord; //BUG (size_t)tmp;
//DEBUG std::cout << std::endl << curCoord << "-" << startCoord << "/" << increment << "=" << relativeCoord << std::endl;
    return _RC_SUCCESS_;
}
/**************************************
* Params Compact class implementation *
***************************************/
/* static
OLD C++ < C++11
const char * IParamsCompact::grammar[FAKE_INDEX_GRAMMAR]=
{
    "IncrementType", ///STEP_VALUE, TOTAL_STEPS
    "IncrementValue"
};
*/
constexpr const char * IParamsCompact::grammar[FAKE_INDEX_GRAMMAR];

namespace
{
/**
    IParamsCompact Implementation (private) MOVE here
    ������� �� ����� IParams
*/
/** � ���� ���������� �� �������� ���� ����� �� �������� � ��������������� ������������,
    ����� ����� ������ ������� ��������, ���, � ������� �������� �����!
*/
class IParamsCompact_Impl : //����� ���� �� ���� public virtual IParamsSet_Impl,
    public virtual IParamsCompact
{
public:
///TODO!!! ������� const IModel* _model, ��� ����� ���������� �� setModel!
/// �.�. ���������������� ��� ���� ����� ��, � CTOR'�
/// � ����� � ������� ������� ����� ��� �� ������
    IParamsCompact_Impl(IModel* model);
    virtual ~IParamsCompact_Impl();
    /**
    �����, � ���� �������,
    �� �����������, ��� � ��������� ����� ��������� ��������� �� ������������ � ���������� ������
    */
    /** both must be public because static IParamsCompact::createParamsCompact()
    calls them
    */
    static bool staticIsValidCompactSize(IParams const * const leftBound, IParams const * const rightBound, IParams const * const increments, bool stepValue, Logger* logger);
    static bool staticAreValidCTORparams(const IModel* model, IParams* leftBound, IParams* rightBound, IParams* increments, bool absStep, Logger* logger);
    /**************************************
    * stupid non-static copy of
    * staticEvaluateRelativeCoord()
    * to call from non-static functions
    ***************************************/
    virtual int evaluateRelativeCoord(double curCoord, double startCoord, double increment, size_t& relativeCoord) const;
//    virtual bool isValidCompactSize(IParams const * const leftBound, IParams const * const rightBound, IParams const * const increments, bool stepValue, Logger* logger) const;
    virtual bool initParamsCompact(const IModel* model, IParams* leftBound, IParams* rightBound, IParams* increments, bool absStep, Logger* logger);

    virtual size_t getDimOfParams() const;
    virtual size_t getSize() const;
    virtual std::string toString() const;

    virtual int    evaluateRelativeCoordAlongGivenAxis(size_t indexOfAxis, const IParams * const point, size_t& relativeCoord) const;
    virtual size_t getMinRelativeSizeAlongAxis() const;
    virtual size_t getMaxRelativeSizeAlongAxis() const;

    virtual size_t getCurrentId() const;
    virtual int getId_byIParams(const IParams * const point, size_t& id) const;

    virtual int setCurrentId(size_t id);
    /**
    � ������� ���� ������� �� ������ ����� ������� ������ ��������,
    �.�.����� ��������� ������� ���������� (���������) � �� ��������������� ������������
    */
    virtual int setOrderOfCoordsToWalkThroughCompact(size_t size, const size_t * newOrder);
    /** � ���� ���������� ���� ��������� �� ��������,
    ����� ����� ������ ������� ��������, ���, � ������� �������� �����!
    ������� ��������� ��� ������ �� ���� ������������� currentParams
    �� ������ ������.
    */
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    virtual IParams* getNextPtr();
    virtual IParams* getParamsCopy(size_t id) const;
    virtual IParams* getIncrementsCopy() const;
    virtual bool isEnd() const;
    virtual bool isPointInCompact(const IParams* const point) const;

private:
    virtual void onCriticalErrorCleanUpParamsCompact();
    /** dimension of each IParams :
    startParams, endParams, increment;
     */
    size_t m_paramsSpaceDimension;
    IParams* m_startParams;
    IParams* m_endParams;
    IParams* m_increment;

    /** vector of size _paramsSpaceDimension
    to keep total of points along each coordinate axis
    */
///TODO!!! ����� ��� ����� ������? ����� �������� ��������!
    std::vector<size_t> m_pointNum;
    int initPointsPerSingleUnitOfRelativeCoordinate();
    std::vector<size_t> m_pointsPerSingleUnitOfRelativeCoordinate;
    /** ptr to current IParams in this Compact */
    IParams * m_currentParams;
    /** id of current IParams in this Compact */
    size_t m_id;
    /** vector of size _paramsSpaceDimension
    to keep ids of current IParams along each coordinate axis
    in this Compact */
///TODO!!! ����� ��� ����� ������? ����� �������� ��������!
    std::vector<size_t> m_coordsOfCurrentParamsInCompact;
    size_t * m_orderOfCoordsToWalkThroughCompact;
    /** while walking with getNextPtr()
    the very Last Params has been retrieved from Compact
    */
    bool m_end;
    bool m_isEmpty;
///TODO!!! ����� shared_ptr<IModel>
    IModel* m_model;
};

/**
   IParamsUnionCompact Implementation (private) MOVE here
*/
/** � ���� ���������� �� �������� ���� ����� �� �������� � ��������������� ������������,
    ����� ����� ������ ������� ��������, ���, � ������� �������� �����!
*/
class IParamsUnionCompact_Impl //��� ���� �� ����� : public virtual IParamsUnionSet_Impl,
    : public virtual IParamsUnionCompact
{
public:
///TODO!!! ������� const IAdditiveModel* _additiveModel, ��� ����� ���������� �� setModel!
/// �.�. ���������������� ��� ���� ����� ��, � CTOR'�
/// � ����� � ������� ������� ����� ��� �� ������
    IParamsUnionCompact_Impl(IAdditiveModel* additiveModel);
    virtual ~IParamsUnionCompact_Impl();

    /**
    �����, � ���� �������,
    �� �����������, ��� � ��������� ����� ��������� ��������� �� ������������ � ���������� ������
    */
    /** both must be public because IParamsCompact::createParamsUnionCompact()
    calls them
    */
    static bool staticAreValidCTORparams(const IAdditiveModel* additiveModel, IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool absStep, Logger* logger);
    /**************************************
    * stupid non-static copy of
    * staticEvaluateRelativeCoord()
    * to call from non-static functions
    ***************************************/
    virtual int evaluateRelativeCoord(double curCoord, double startCoord, double increment, size_t& relativeCoord) const;

    virtual bool initParamsUnionCompact(const IAdditiveModel* additiveModel, IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool absStep, Logger* logger);
    virtual size_t getDimOfParamsUnion() const;
    virtual size_t getSize() const;

    virtual std::string toString() const;

    virtual int    evaluateRelativeCoordAlongGivenAxis(size_t indexOfAxis, const IParamsUnion * const point, size_t& relativeCoord) const;
    virtual size_t getMinRelativeSizeAlongAxis() const;
    virtual size_t getMaxRelativeSizeAlongAxis() const;

    virtual size_t getCurrentId() const;
    virtual int getId_byIParamsUnion(const IParamsUnion * const point, size_t& id) const;
    virtual int setCurrentId(size_t id);
    /**
    � ������� ���� ������� �� ������ ����� ������� ������ ��������,
    �.�.����� ��������� ������� ���������� (���������) � �� ��������������� ������������
    */
    virtual int setOrderOfCoordsToWalkThroughCompact(size_t size, const size_t * newOrder);
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    IParamsUnion* getFirstPtr();
    /**
    NB returns ptr to existing item into Set!
    It does Not copy! => Don't free that ptr!
    */
    IParamsUnion* getNextPtr();
    IParamsUnion* getParamsUnionCopy(size_t id) const;
    IParamsUnion* getIncrementsCopy()const;
    virtual bool isEnd() const;
    virtual bool isPointInCompact(const IParamsUnion* const point) const;

private:

    virtual void onCriticalErrorCleanUpParamsUnionCompact();

    int  indexInParamsUnionToIndexInIParams(size_t& index) const;
    int  getIndexOfUnionItem_byIndexOfParam(size_t indexOfParam, size_t& indexOfUnionItem) const;
    /** total of IParams into each IParamsUnion,
    i.e. TOTAL of IParams in IParamsUnion
    */
    size_t m_totalOfItemsInParamsUnion;
    /** dimension of compact as result of
    IParamsUnion's items concatenation
    OR
    TOTAL dimension of each single IParamsUnion:
    TOTAL of params in startParams, endParams, increment;
    */
    size_t m_paramsSpaceDimension;
    /** dimentionS of items in IParamsUnion
    startParams, endParams, increment;
    the array of size _totalOfItemsInParamsUnion
    */
    size_t* m_paramsUnionItemsDimensions;
    IParamsUnion* m_startParamsUnion;
    IParamsUnion* m_endParamsUnion;
    IParamsUnion* m_increment;
    /** vector of size _paramsSpaceDimension
    to keep total of points along each coordinate axis  */
///TODO!!! ����� ��� ����� ������? ����� �������� ��������!
    std::vector<size_t> m_pointNum;
    int initPointsPerSingleUnitOfRelativeCoordinate();
    std::vector<size_t> m_pointsPerSingleUnitOfRelativeCoordinate;
    /** ptr to current ARRAY IParamsUnion in this Compact */
    IParamsUnion* m_currentParamsUnion;
    /** id of current ARRAY IParamsUnion in this Compact */
    size_t m_id;

    /** vector of size _paramsSpaceDimension
    to keep ids of current IParamsUnion along each coordinate axis
    in this Compact */
///TODO!!! ����� ��� ����� ������? ����� �������� ��������!
    std::vector<size_t> m_coordsOfCurrentParamsUnionInCompact;
    size_t * m_orderOfCoordsToWalkThroughCompact;
    /** while walking with getNextPtr()
    the very Last Params has been retrieved from Compact
    */
    bool m_end;
    bool m_isEmpty;
    IAdditiveModel* m_additiveModel;
};

} ///end of anonymous namespace
/***********************************************
*                 IMPLEMENTATION
************************************************/

/***************************************************
              IParamsCompact_Impl
****************************************************/
/************************************************
*                     CTOR                      *
************************************************/
IParamsCompact_Impl::IParamsCompact_Impl(IModel* model) : m_model(model)
{
    m_startParams = nullptr;
    m_endParams = nullptr;
    m_increment = nullptr;
    m_paramsSpaceDimension =0;
    m_currentParams = nullptr;
    m_id = BAD_ID;
    m_end    = true;
    m_isEmpty= true;
    m_orderOfCoordsToWalkThroughCompact = nullptr;
}
/************************************************
*                     DTOR                      *
************************************************/
IParamsCompact_Impl::~IParamsCompact_Impl()
{
    m_paramsSpaceDimension =0;
    delete m_startParams;                         m_startParams = nullptr;
    delete m_endParams;                           m_endParams = nullptr;
    delete m_increment;                           m_increment = nullptr;
    delete m_currentParams;                       m_currentParams = nullptr;
    delete [] m_orderOfCoordsToWalkThroughCompact; m_orderOfCoordsToWalkThroughCompact = nullptr;
    m_coordsOfCurrentParamsInCompact.clear();
    m_pointNum.clear();
    m_pointsPerSingleUnitOfRelativeCoordinate.clear();

    m_id = BAD_ID;
    m_end    = true;
    m_isEmpty= true;
///???
    m_model = nullptr;
}
/****************************************
* static member to be called from static functions
*****************************************/
bool IParamsCompact_Impl::staticIsValidCompactSize(IParams const * const leftBound, IParams const * const rightBound, IParams const * const increments, bool stepValue, Logger* logger)
{
    size_t paramsSpaceDimension = leftBound->getSize();

    std::vector<size_t> pointNum;
    pointNum.resize(paramsSpaceDimension,0);
    for (size_t i = 0; i < paramsSpaceDimension; ++i)
    {
        double startParamsElement =NAN;
        double endParamsElement   =NAN;
        /** we treat leftBound as startParams
        */
        int rc = leftBound->getParam(i, startParamsElement);
        if(_RC_SUCCESS_ != rc){
            pointNum.clear();
            return false;
        }
        /** we treat rightBound as endParams
        */
        rc = rightBound->getParam(i, endParamsElement);
        if(_RC_SUCCESS_ != rc){
            pointNum.clear();
            return false;
        }
        if (fabs(startParamsElement - endParamsElement) < DBL_MIN)
        {
            pointNum[i] = 1;
            continue;
        }
        double incrementParamsElement =NAN;
        /** increments
        */
        rc = increments->getParam(i, incrementParamsElement);
        if(_RC_SUCCESS_ != rc){
            pointNum.clear();
            return false;
        }
        /** if increments semantics is stepValue */
        if (stepValue)
        {
            size_t parts =0;
            rc = staticEvaluateRelativeCoord(endParamsElement, startParamsElement, incrementParamsElement, parts);
            pointNum[i] = parts + 1;
        }
        else /** increments semantics is totalSteps */
        {
            double pointsNum = incrementParamsElement;
            pointNum[i] = (size_t)pointsNum;
            /** recalculate increment to take into account round-off */
            incrementParamsElement = pointsNum > 1 ? (endParamsElement - startParamsElement) / (pointsNum - 1) : incrementParamsElement;
        }
    }///end for()
    /**
    Now then the total of points along each axis has been evaluated
    let's check the size of the compact to be constructed
    */
    size_t size = 1;
    for (size_t i = 0; i < paramsSpaceDimension; ++i)
    {
        if(size > (double)BAD_ID / (double) pointNum[i])
        {
            pointNum.clear();
            return false;
        }
        size *= pointNum[i];
    }
    pointNum.clear();
    return true;
}
/*************************************************
* static in IParamsCompact_Impl
* to call it from static functions
* because IParamsUnionCompact_Impl uses it as well
**************************************************/
bool IParamsCompact_Impl::staticAreValidCTORparams(const IModel* model, IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger)
{
    if(model == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Model NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(leftBound == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(rightBound == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(increments == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's increments NOT FOUND in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    bool isParamsFitModel = model->canCastModelParams(leftBound);
    if(isParamsFitModel == false)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT MATCH model in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    isParamsFitModel = model->canCastModelParams(rightBound);
    if(isParamsFitModel == false)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT MATCH model in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    isParamsFitModel = model->canCastModelParams(increments);
    if(isParamsFitModel == false)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's increments NOT MATCH model in IParamsCompact_Impl::areValidCTORparams()!");
        return false;
    }
    bool isCompactSizeValid = staticIsValidCompactSize( static_cast<IParams const * const> (leftBound), static_cast<IParams const * const> (rightBound), static_cast<IParams const * const> (increments), stepValue, logger);
    if(isCompactSizeValid == false)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "The size of the compact to be constructed exceeds compact size limit!");
        return false;
    }
    return true;
}

void IParamsCompact_Impl::onCriticalErrorCleanUpParamsCompact()
{
    m_paramsSpaceDimension =0;
    if(m_startParams)
    {
        delete m_startParams;
        m_startParams = nullptr;
    }
    if(m_endParams)
    {
        delete m_endParams;
        m_endParams = nullptr;
    }
    if(m_increment)
    {
        delete m_increment;
        m_increment = nullptr;
    }
    if(m_currentParams)
    {
        delete m_currentParams;
        m_currentParams = nullptr;
    }
    if(m_orderOfCoordsToWalkThroughCompact)
    {
        delete [] m_orderOfCoordsToWalkThroughCompact;
        m_orderOfCoordsToWalkThroughCompact = nullptr;
    }

    m_coordsOfCurrentParamsInCompact.clear();
    m_pointNum.clear();
    m_pointsPerSingleUnitOfRelativeCoordinate.clear();

    m_id = BAD_ID;
    m_end    = true;
    m_isEmpty= true;
///???
    if(m_model)
    {
         delete m_model;
         m_model = nullptr;
    }
}///end OnCriticalErrorCleanUpParamsCompact

/**************************************
* stupid non-static copy of
* staticEvaluateRelativeCoord()
* to call from non-static functions
***************************************/
int IParamsCompact_Impl::evaluateRelativeCoord(double curCoord, double startCoord, double increment, size_t& relativeCoord) const
{
    assert(increment > DBL_MIN);
    if((curCoord < startCoord) || (increment < DBL_MIN))
    {
       return _ERROR_WRONG_ARGUMENT_;
    }

//    double tmp = roundl((curCoord - startCoord) / increment);
    double tmpStartCoord = startCoord;
    size_t tmpRelativeCoord = 0;
    while((curCoord - tmpStartCoord) > (increment/2.0f))
    {
        tmpStartCoord += increment;
        tmpRelativeCoord++;
    }
    /** ��� �������� - ����� �����, ��� ����� �������,
    ����� ������������� ���������� ������������� ����������,
    ������� � �������� �� ����� ���� � ����� �� �� �� ���� ����� ����� ���������
    */
//    if(tmp >= (double)BAD_ID){
//       return _ERROR_WRONG_ARGUMENT_;
//    }
    relativeCoord = tmpRelativeCoord; //BUG (size_t)tmp;
//DEBUG std::cout << std::endl << curCoord << "-" << startCoord << "/" << increment << "=" << relativeCoord << std::endl;
    return _RC_SUCCESS_;
}///end evaluateRelativeCoord

/***********************************************
* non-static member
************************************************/
int IParamsCompact_Impl::initPointsPerSingleUnitOfRelativeCoordinate()
{
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t tmpTotalOfPOints = 1;
        for(int j=i-1; j >= 0; --j){
            size_t trueIndex = m_orderOfCoordsToWalkThroughCompact[j];
            assert(BAD_ID/m_pointNum[trueIndex] > tmpTotalOfPOints);
            if(BAD_ID/m_pointNum[trueIndex] <= tmpTotalOfPOints)
            {
                m_pointsPerSingleUnitOfRelativeCoordinate.clear();
                return _ERROR_WRONG_INPUT_DATA_;
            }
            tmpTotalOfPOints *= m_pointNum[trueIndex];
        }
        assert(BAD_ID/m_pointNum[m_orderOfCoordsToWalkThroughCompact[i]] > tmpTotalOfPOints);
        if(BAD_ID/m_pointNum[m_orderOfCoordsToWalkThroughCompact[i]] <= tmpTotalOfPOints)
        {
            m_pointsPerSingleUnitOfRelativeCoordinate.clear();
            return _ERROR_WRONG_INPUT_DATA_;
        }
        m_pointsPerSingleUnitOfRelativeCoordinate[m_orderOfCoordsToWalkThroughCompact[i]] = tmpTotalOfPOints;
    }
//DEBUG
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
std::cout << m_pointsPerSingleUnitOfRelativeCoordinate[m_orderOfCoordsToWalkThroughCompact[i]] << "\t";
    }
    std::cout << "\n";
    return _RC_SUCCESS_;
//DEBUG END
}
/***********************************************
* non-static member
************************************************/
bool IParamsCompact_Impl::initParamsCompact(const IModel* model, IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger)
{
    bool bRC = true;
    if (m_model != NULL)
    {
        bRC = IModel::compareModels(m_model, model);
        if (! bRC)
        {
            if(logger)
                logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong model type in ParamsCompact::fillParams()!");
            return false;
        }
    }
    /*
        else
        {
            / This Cheating happens once for all ParamsCompact life-time !
            /
            _model = const_cast<IModel *> (model);
        }
    */
    /** first validate params */
    /** Validate params models, they sizes, the size of compact to be constructed
    */
    if(false == IParamsCompact_Impl::staticAreValidCTORparams(model, leftBound, rightBound, increments, stepValue, logger))
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong params in IParamsCompact::createParamsCompact()!");
        return false;
    }

    m_paramsSpaceDimension = leftBound->getSize();
    m_orderOfCoordsToWalkThroughCompact = new size_t[m_paramsSpaceDimension];
    if(!m_orderOfCoordsToWalkThroughCompact)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "No room to allocate orderOfCoords in ParamsCompact::fillParams()!");
        return false;
    }
    for (unsigned i = 0; i < m_paramsSpaceDimension; ++i)
    {
        m_orderOfCoordsToWalkThroughCompact[i] = i;
    }
    m_startParams   = leftBound->clone();
    m_endParams     = rightBound->clone();
    m_increment     = increments->clone();

    m_currentParams = m_startParams->clone();

    m_pointNum.resize(m_paramsSpaceDimension,0);
    m_pointsPerSingleUnitOfRelativeCoordinate.resize(m_paramsSpaceDimension,0);
    m_coordsOfCurrentParamsInCompact.resize(m_paramsSpaceDimension,0);
    /** init m_pointNum array */
    for (unsigned i = 0; i < m_paramsSpaceDimension; ++i)
    {
        double startParamsElement =NAN;
        double endParamsElement   =NAN;
        int rc = m_startParams->getParam(i, startParamsElement);
        if(_RC_SUCCESS_ != rc){
            onCriticalErrorCleanUpParamsCompact();
            return false;
        }
        rc = m_endParams->getParam(i, endParamsElement);
        if(_RC_SUCCESS_ != rc){
            onCriticalErrorCleanUpParamsCompact();
            return false;
        }
        if (startParamsElement > endParamsElement)
        {
            rc = m_startParams->setParam(i, endParamsElement);
            if(_RC_SUCCESS_ != rc){
                onCriticalErrorCleanUpParamsCompact();
                return false;
            }
            rc = m_endParams->setParam(i, startParamsElement);
            if(_RC_SUCCESS_ != rc){
                onCriticalErrorCleanUpParamsCompact();
                return false;
            }
        }
        if (fabs(startParamsElement - endParamsElement) < DBL_MIN)
        {
            m_pointNum[i] = 1;
            continue;
        }
        double incrementParamsElement =NAN;
        rc = m_increment->getParam(i, incrementParamsElement);
        if(_RC_SUCCESS_ != rc){
            onCriticalErrorCleanUpParamsCompact();
            return false;
        }
        /** increment is stepValue */
        if (stepValue)
        {
            size_t parts =0;
            rc = evaluateRelativeCoord(endParamsElement, startParamsElement, incrementParamsElement, parts);
            if(_RC_SUCCESS_ != rc){
                onCriticalErrorCleanUpParamsCompact();
                return false;
            }
            m_pointNum[i] = parts + 1;
        }
        else /** increment is totalSteps */
        {
            double pointsNum = incrementParamsElement;
            m_pointNum[i] = (size_t)pointsNum;
            /** recalculate increment to take into account round-off */
            incrementParamsElement = pointsNum > 1 ? (endParamsElement - startParamsElement) / (pointsNum - 1) : incrementParamsElement;
            rc = m_increment->setParam(i, incrementParamsElement);
            if(_RC_SUCCESS_ != rc){
                onCriticalErrorCleanUpParamsCompact();
                return false;
            }
        }
        if(logger)
        {
            std::stringstream logStr;
            logStr << "Fill compact for param " << i << std::endl <<
            "Begin: " << startParamsElement << std::endl <<
            "End: " << endParamsElement << std::endl <<
            "Increment: " << incrementParamsElement << std::endl <<
            "Num of points: " << m_pointNum[i];
            logger->log(ALGORITHM_GENERAL_INFO, logStr.str());
        }
#ifdef _DEBUG_
std::stringstream logStr;
logStr << "Fill compact for param " << i << std::endl <<
"Begin: " << startParamsElement << std::endl <<
"End: " << endParamsElement << std::endl <<
"Increment: " << incrementParamsElement << std::endl <<
"Num of points: " << _pointNum[i];
std::cout << logStr.str() << std::endl;
#endif
    }///end init m_pointNum array
    /**
     ����� �� ���������, ��� �������� ��������� �����, ������������� ����������������� ��������,
     �� ����������� ����������� BAD_ID
    */
    int rc = initPointsPerSingleUnitOfRelativeCoordinate();
    if(rc != _RC_SUCCESS_)
    {
        onCriticalErrorCleanUpParamsCompact();
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Too many points into compact, in ParamsCompact::initParamsCompact()!");
        return false;
    }

    if(getSize()){
        m_id = 0;
        m_isEmpty = false;
    }
    if(logger)
    {
        size_t size = getSize();
        std::stringstream logStr;
        logStr << "Total size of compact: " << size;
        logger->log(ALGORITHM_GENERAL_INFO, logStr.str());
    }
    return true;
}

size_t IParamsCompact_Impl::getDimOfParams() const
{
    return m_paramsSpaceDimension;
}
/**********************************************
*            Get total of points in Compact
***********************************************/
size_t IParamsCompact_Impl::getSize() const
{
    size_t sizeOfSet = 1;
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        sizeOfSet *= m_pointNum[i];
    }

    return sizeOfSet;
}
/**********************************************
* Get main metrics of ParamsCompact as string
***********************************************/
std::string IParamsCompact_Impl::toString() const
{
    std::stringstream sStr;
    sStr << m_startParams->toString() << DELIMITER_LIST_ITEMS
    << m_endParams->toString() << DELIMITER_LIST_ITEMS
    << m_increment->toString();
    return sStr.str();
}
/**********************************************
* Get total of increments to the given point
* along given axis in Params Compact
***********************************************/
int IParamsCompact_Impl::evaluateRelativeCoordAlongGivenAxis(size_t indexOfAxis, const IParams * const point, size_t& relativeCoord) const
{
    if(!isPointInCompact(point))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t trueIndex = m_orderOfCoordsToWalkThroughCompact[indexOfAxis];
    double pointParam=NAN;
    int rc = point->getParam(trueIndex, pointParam);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    double startPointParam = NAN;
    rc = m_startParams->getParam(trueIndex, startPointParam);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    double increment=NAN;
    rc = m_increment->getParam(trueIndex, increment);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** Assume startPint == endPoint, i.e. there is single point along given axis */
    relativeCoord = 0;
    if( (pointParam - startPointParam) > DBL_MIN)
    {
        /**
        ������ ��� �� ������-�������,
        ��������� � ��� �������, evaluateRelativeCoordAlongGivenAxis,
        ������� ������  ������ PF_Estimator'�
        */
        rc = evaluateRelativeCoord(pointParam, startPointParam, increment, relativeCoord);
        if(_RC_SUCCESS_ != rc)
        {
            /** assign -1 to size_t to retrieve the max unsigned int == impossible relative coordinate */
            relativeCoord = IMPOSSIBLE_RELATIVE_COORDINATE;
            return rc;
        }
    }
    return _RC_SUCCESS_;
}
/**********************************************
* Get min total of increments
* along some axis in Params Compact
***********************************************/
size_t IParamsCompact_Impl::getMinRelativeSizeAlongAxis() const
{
    size_t minRelativeSize = UINT_MAX;
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t tmpRelativeSize = (((double)m_pointNum[i] - 1.0f) > - 1.0f ) ? (m_pointNum[i] - 1) : 0;
        if (tmpRelativeSize){
            minRelativeSize = minRelativeSize < tmpRelativeSize ? minRelativeSize : tmpRelativeSize;
        } else {
            /** 0-th size is the smallest (single point) */
            minRelativeSize = 0;
            break;
        }
    }
    return minRelativeSize;
}
/**********************************************
* Get max total of increments
* along some axis in Params Compact
***********************************************/
size_t IParamsCompact_Impl::getMaxRelativeSizeAlongAxis() const
{
    size_t maxRelativeSize = 0;
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t tmpRelativeSize = (((double)m_pointNum[i] - 1.0f) > - 1.0f ) ? (m_pointNum[i] - 1) : 0;
        if (maxRelativeSize < tmpRelativeSize){
            maxRelativeSize = tmpRelativeSize;
        }
    }
    return maxRelativeSize;
}

size_t IParamsCompact_Impl::getCurrentId() const
{
     return (m_isEmpty) || (m_id == BAD_ID) ? (size_t)-1 : m_id;
}

bool IParamsCompact_Impl::isPointInCompact(const IParams* const point) const
{
    if(m_isEmpty){
        return false;
    }
    if(m_paramsSpaceDimension != point->getSize())
    {
        return false;
    }
///TODO!!! develop code for Interval class (like light-weight ParamsUnion) to move following predicate over there!
    for(size_t i=0; i < m_paramsSpaceDimension; ++i)
    {
        double pointParam;
        point->getParam(i, pointParam);
        double boundaryPointParam;
        m_startParams->getParam(i, boundaryPointParam);
        if(pointParam < boundaryPointParam) return false;
        m_endParams->getParam(i, boundaryPointParam);
        if(pointParam > boundaryPointParam) return false;
    }
    return true;
}

int IParamsCompact_Impl::getId_byIParams(const IParams * const point, size_t& id) const
{
    if(m_isEmpty){
        return getCurrentId();
    }
    if(!isPointInCompact(point))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    int rc;
    size_t tmpId = 0;
    /** real index of param */
//OLD    size_t index;
//OLD    double pointParam=NAN;
//OLD    double startPointParam=NAN;
//OLD    double increment =NAN;
    size_t relativeCoord = 0;
    for (int i = m_paramsSpaceDimension - 1; i > 0; --i)
    {
        relativeCoord = 0;
        rc = evaluateRelativeCoordAlongGivenAxis(m_orderOfCoordsToWalkThroughCompact[i], point, relativeCoord);
        if(rc)
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
//DEBUG
// coordsRelative[index]=relativeCoord;
//END DEBUG
        assert(relativeCoord < m_pointNum[m_orderOfCoordsToWalkThroughCompact[i]]);

        if(relativeCoord){
//            size_t tmpPointsPerSingleUnitOfRelativeCoordinate = 1;
//            for(int j=i-1; j >=0; --j)
//            {
//                size_t tmpIndex=m_orderOfCoordsToWalkThroughCompact[j];
//                assert(BAD_ID/m_pointNum[tmpIndex] > tmpPointsPerSingleUnitOfRelativeCoordinate);
//                tmpPointsPerSingleUnitOfRelativeCoordinate *= m_pointNum[tmpIndex];
//            }
//            assert(tmpPointsPerSingleUnitOfRelativeCoordinate == m_pointsPerSingleUnitOfRelativeCoordinate[m_orderOfCoordsToWalkThroughCompact[i]]);
//            relativeCoord *= tmpPointsPerSingleUnitOfRelativeCoordinate;
            relativeCoord *= m_pointsPerSingleUnitOfRelativeCoordinate[m_orderOfCoordsToWalkThroughCompact[i]];
        }
        tmpId += relativeCoord;
    }
    /**
    Finally process coordinate
    along the very first (according to walk through order) axis
    */
    relativeCoord = 0;
    rc = evaluateRelativeCoordAlongGivenAxis(m_orderOfCoordsToWalkThroughCompact[0], point, relativeCoord);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
//DEBUG
// coordsRelative[0]=relativeCoord;
//END DEBUG
    tmpId += relativeCoord;
    assert(tmpId < getSize());
    id = tmpId;
    return _RC_SUCCESS_;
}

int IParamsCompact_Impl::setCurrentId(size_t id)
{
    if(m_isEmpty)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    size_t size = getSize();
    if(id >= size)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    size_t tmpId = id;
    for (unsigned i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t index = m_orderOfCoordsToWalkThroughCompact[i];
        m_coordsOfCurrentParamsInCompact[index] = tmpId % m_pointNum[index];
        tmpId /= m_pointNum[index];
        double currentParamsElement   =NAN;
        int rc = m_startParams->getParam(index, currentParamsElement);
        if(rc)
        {
///TODO!!! ����� �����, �������� ������ _id = -1;
            m_id = BAD_ID;
            return _ERROR_WRONG_ARGUMENT_;
        }
        m_currentParams->setParam(index, currentParamsElement);
    }
    m_id = id;
    return _RC_SUCCESS_;
}

int IParamsCompact_Impl::setOrderOfCoordsToWalkThroughCompact(size_t size, const size_t * newOrder)
{
    if(size != m_paramsSpaceDimension)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    /** validate newOrder */
    size_t sum1 = 0;
    size_t sum2 = 0;
    for(size_t i=0; i < m_paramsSpaceDimension; ++i)
    {
        sum1 += i;
        sum2 += newOrder[i];
    }
    if(sum1 != sum2)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** set new order Of Coords To Walk Through Compact */
    std::memcpy(m_orderOfCoordsToWalkThroughCompact, newOrder, size*sizeof(size_t));

    int rc = initPointsPerSingleUnitOfRelativeCoordinate();
    if(rc != _RC_SUCCESS_)
    {
        onCriticalErrorCleanUpParamsCompact();
//        if(logger)
//            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Too many points into compact, in ParamsCompact::setOrderOfCoordsToWalkThroughCompact()!");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    return _RC_SUCCESS_;
}

IParams* IParamsCompact_Impl::getFirstPtr()
{
    if(m_isEmpty){
        return nullptr;
    }
    m_id = 0;
    for (unsigned i = 0; i < m_paramsSpaceDimension; ++i)
    {
        /** startParams �� ���������, ����� �� ��������������� ������
        */
        double currentParamsElement   =NAN;
        int rc = m_startParams->getParam(i, currentParamsElement);
        if(rc)
        {
            m_id = BAD_ID;
            return nullptr;
        }
        m_currentParams->setParam(i, currentParamsElement);
        m_coordsOfCurrentParamsInCompact[i] = 0;
    }
    if(getSize() > 1)
        m_end = false;
///TODO!!! ��������� � shared_ptr!
    return m_currentParams;
}
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
///TODO!!! ��������� ���� ��������!
IParams* IParamsCompact_Impl::getNextPtr()
{
    if(m_isEmpty){
        return nullptr;
    }

    ++m_id;

    for (unsigned i = 0; i < m_paramsSpaceDimension; ++i)
    {
        /**
        �������� � ����� ��������������� �����
        ������ �� ������������ ���� ���������������� ������������
        ����������� _paramsDimension,
        �� ���� ������ ����������, �� �������
        ��� �� �������� � ������ ������� �������� �� ���� ����������.
        ��������� ����� ������������ ���,
        �� �������������� �������� ����� ��� �
        ������� � incrementParamsElement �������� �� ���� ����������,
        ����� ��� � ������ ���������� _currentParams
        �� �������, ���������������� ���� ������������ ���.
        ���������� ������������� ������ ����������.
        */
        size_t index = m_orderOfCoordsToWalkThroughCompact[i];
        if ((m_pointNum[index] > 1) && (m_coordsOfCurrentParamsInCompact[index] < (m_pointNum[index] - 2) ))
        {
            m_coordsOfCurrentParamsInCompact[index]++;
            double incrementParamsElement   =NAN;
            /** ����� ������ ������ ���������!
            */
            int rc = m_increment->getParam(index, incrementParamsElement);
            if(rc)
            {
///TODO!!! ����� �����, �������� ������  _id = -1;
                m_id = BAD_ID;
                return NULL;
            }
///BUG ��� ����� _currentParams[i] +=incrementParamsElement!
            double currentParamsElement   =NAN;
            m_currentParams->getParam(index, currentParamsElement);
            currentParamsElement += incrementParamsElement;
            m_currentParams->setParam(index, currentParamsElement);
            if ((getSize()) && ( (m_id + 1) == getSize()))
                m_end=true;
///TODO!!! ��������� � shared_ptr!
            return m_currentParams;
        }
        /** ������, ����� ������ �������� ��������� ������� ��������
        * !��� ���� �� ������������� ��������:
          _coordsOfCurrentParamsInCompact[index] + 2) == _pointNum[index]
        */
        if ((m_pointNum[index] > 1) && ( (m_coordsOfCurrentParamsInCompact[index] + 2) == m_pointNum[index]))
        {
            m_coordsOfCurrentParamsInCompact[index]++;
            double endParamsElement   =NAN;
            int rc = m_endParams->getParam(index, endParamsElement);
            if(rc)
            {
///TODO!!! ����� �����, �������� ������ _id = -1;
                m_id = BAD_ID;
                return NULL;
            }
            m_currentParams->setParam(index, endParamsElement);
            if ((getSize()) && ((m_id + 1) == getSize()))
                m_end=true;
///TODO!!! ��������� � shared_ptr!
            return m_currentParams;
        }
        /** ����� �� �����! */
        if ((m_coordsOfCurrentParamsInCompact[index] + 1) == m_pointNum[index])
        {
            m_coordsOfCurrentParamsInCompact[index] = 0;
            double startParamsElement   =NAN;
            int rc = m_startParams->getParam(index, startParamsElement);
            if(rc)
            {
///TODO!!! ����� �����, �������� ������ _id = -1;
                m_id = BAD_ID;
                return NULL;
            }
            m_currentParams->setParam(index, startParamsElement);
/// NO WAY! return _currentParams; <- ����� ������� ����� ��������!
        }
    }
    m_id = BAD_ID;
    return nullptr;
}

/**
 The IParams returned by this method
 MUST BE DELETED after all!
*/
///TODO!!! �� ���� �� ����� ��������� ��� getNextPtr(), �� �� ������������� _id
///��� ����� ��������� ���������/��������� _id, ������ � ���������/��������� ��������� vecId, � ��������� �������, � ������� ��� �� � ����������� �� ���������
/// ���� ��� getNextPtr(), ���� ��� getParamsCopy()
/** ����� ������ ����������� IParams � ������������� id, �.� ��������� � �������� ��� ����� �� ���������! */
IParams* IParamsCompact_Impl::getParamsCopy(size_t id) const
{
    if(m_isEmpty){
        return nullptr;
    }
    size_t size = getSize();

    if (id > size - 1)
    {
        return nullptr;
    }
///TODO!!! ����������� ����� ������ ����! �������� ������ : ��� ����� ��� ��������� ������, ��� ������ ����������� � ���������� ������� ����� �� �������� ����� ��������� �������� _id � ��������, ������� �������� ��������������� ��������������
    size_t tmpId = id;
    /**
    Here we pass NULL instead of Logger* because Params1D type already implemented
    So, NO ERROR expected.
    */
    IParams * ptrIParams = IParams::createEmptyParams(NULL, IParams::ParamsTypes::Params1D, m_paramsSpaceDimension);
    if(ptrIParams == NULL)
    {
        return nullptr;
    }
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t index = m_orderOfCoordsToWalkThroughCompact[i];
        size_t iCoord = tmpId % m_pointNum[index];
        tmpId /= m_pointNum[index];
        /**
        �������� � ����� ��������������� �����
        ������ �� ������������ ���� ���������������� ������������
        ����������� _paramsDimension,
        �� ���� ������ ����������, �� �������
        ��� �� �������� � ������ ������� �������� �� ���� ����������.
        ��������� ����� ������������ ���,
        �� �������������� �������� ����� ��� �
        ������� � incrementParamsElement �������� �� ���� ����������,
        ����� ��� � ������ ���������� _currentParams
        �� �������, ���������������� ���� ������������ ���.
        ���������� ������������� ������ ����������.
        */
        if ((m_pointNum[index]) && ((iCoord + 1) < m_pointNum[index]))
        {
            double currentParamsElement   =NAN;
            double incrementParamsElement   =NAN;
            /** ����� ������ ������ ���������! */
            int rc = m_increment->getParam(index, incrementParamsElement);
            if(rc)
            {
                delete ptrIParams;
                return NULL;
            }
            /** ����� ��������� ���������� ����� ���� i-� ��� */
            double startParamsElement   =NAN;
            m_startParams->getParam(index, startParamsElement);
            /**  � ������ ������� �� �� _vecId[i] � �������� � ��������� ���������� */
            currentParamsElement = startParamsElement + (iCoord * incrementParamsElement);
            ptrIParams->setParam(index, currentParamsElement);
        }
        /** ������, ����� ������ �������� ��������� ������� ��������
        */
        if ((m_pointNum[index]) && ((iCoord + 1) == m_pointNum[index]))
        {
            double endParamsElement   =NAN;
            int rc = m_endParams->getParam(index, endParamsElement);
            if(rc)
            {
                delete ptrIParams;
                return NULL;
            }
            ptrIParams->setParam(index, endParamsElement);
        }
        /** ����� �� �����! */
        if (iCoord == 0)
        {
            double startParamsElement   =NAN;
            int rc = m_startParams->getParam(index, startParamsElement);
            if(rc)
            {
                delete ptrIParams;
                return NULL;
            }
            ptrIParams->setParam(index, startParamsElement);
        }
    }
    return ptrIParams;
}
/**
 ���������, ������������ ���� ��������,
 �����, �� ���������� ��������������! ������� �� ���������� ���������
*/
IParams* IParamsCompact_Impl::getIncrementsCopy() const
{
    return m_increment->clone();
}

bool IParamsCompact_Impl::isEnd() const
{
    return m_end;
}
/***************************************************
              IParamsUnionCompact_Impl
****************************************************/
/***************************************************
*                    CTOR
***************************************************/
IParamsUnionCompact_Impl::IParamsUnionCompact_Impl(IAdditiveModel* additiveModel) : m_additiveModel(additiveModel)
{
    /** total of IParams into each IParamsUnion
    */
    m_totalOfItemsInParamsUnion=0;
    /** dimension of compact as result of
    IParamsUnion's items concatenation
    OR
    dimention of ech IParamsUnion
    startParams, endParams, increment;
    */
    m_paramsSpaceDimension = 0;
    /** dimentions of items in IParamsUnion
    startParams, endParams, increment;
    the array of size _totalOfItemsInParamsUnion
    */
    m_paramsUnionItemsDimensions=NULL;
    m_startParamsUnion = NULL;
    m_endParamsUnion = NULL;
    m_increment = NULL;
    /** ptr to current ARRAY IParamsUnion in this Compact */
    m_currentParamsUnion = NULL;
    /** id of current ARRAY IParamsUnion in this Compact */
    m_id = BAD_ID;
    m_end     =true;
    m_isEmpty = true;
    m_orderOfCoordsToWalkThroughCompact = NULL;
}
/***************************************************
*                    DTOR
***************************************************/
IParamsUnionCompact_Impl::~IParamsUnionCompact_Impl()
{
    delete [] m_paramsUnionItemsDimensions;
    delete m_startParamsUnion;
    delete m_endParamsUnion;
    delete m_increment;
    /** ptr to current ARRAY IParamsUnion in this Compact */
    delete m_currentParamsUnion;
    delete [] m_orderOfCoordsToWalkThroughCompact;
    m_pointNum.clear();
    m_pointsPerSingleUnitOfRelativeCoordinate.clear();
    m_coordsOfCurrentParamsUnionInCompact.clear();
///TODO???    delete _additiveModel;
}

void IParamsUnionCompact_Impl::onCriticalErrorCleanUpParamsUnionCompact()
{
    m_totalOfItemsInParamsUnion = 0;
    m_paramsSpaceDimension      = 0;
    /** dimentionS of items in IParamsUnion
    startParams, endParams, increment;
    the array of size _totalOfItemsInParamsUnion
    */
    if(m_paramsUnionItemsDimensions)
    {
        delete [] m_paramsUnionItemsDimensions;
        m_paramsUnionItemsDimensions = nullptr;
    }
    if(m_startParamsUnion)
    {
        delete m_startParamsUnion;
        m_startParamsUnion = nullptr;
    }
    if(m_endParamsUnion)
    {
        delete m_endParamsUnion;
        m_endParamsUnion = nullptr;
    }
    if(m_increment)
    {
        delete m_increment;
        m_increment = nullptr;
    }
    if(m_currentParamsUnion)
    {
        delete m_currentParamsUnion;
        m_currentParamsUnion = nullptr;
    }
    m_pointNum.clear();
    m_pointsPerSingleUnitOfRelativeCoordinate.clear();
    m_id = BAD_ID;

    m_coordsOfCurrentParamsUnionInCompact.clear();

    if(m_orderOfCoordsToWalkThroughCompact)
    {
        delete [] m_orderOfCoordsToWalkThroughCompact;
        m_orderOfCoordsToWalkThroughCompact = nullptr;
    }

    m_end     = true;
    m_isEmpty = true;
    if(m_additiveModel)
    {
///??? delete m_additiveModel;
        m_additiveModel = nullptr;
    }
}
/***************************************************
*    static in IParamsUnionCompact_Impl
***************************************************/
bool IParamsUnionCompact_Impl::staticAreValidCTORparams(const IAdditiveModel* additiveModel, IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool absStep, Logger* logger)
{
    if(additiveModel == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Additive Model NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    size_t totalOfModelAddends = 0;
    additiveModel->getTotalOfAddends(totalOfModelAddends);
    if(leftBound == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    size_t totalOfItems = leftBound->getTotalOfItemsInParamsUnion();
    if(totalOfModelAddends != totalOfItems)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact leftBound's dim NOT EQUAL total of model addends in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    if(rightBound == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    totalOfItems = rightBound->getTotalOfItemsInParamsUnion();
    if(totalOfModelAddends != totalOfItems)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact rightBound's dim NOT EQUAL total of model addends in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }

    if(increments == NULL)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's increments NOT FOUND in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    totalOfItems = increments->getTotalOfItemsInParamsUnion();
    if(totalOfModelAddends != totalOfItems)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact increments' dim NOT EQUAL total of model addends in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    else
    {
        for(size_t i=0; i < totalOfItems; ++i)
        {
            size_t sizeLBitem = leftBound->ptrIParamsUnionToPtrIParams(i)->getSize();
            size_t sizeIncrItem = increments->ptrIParamsUnionToPtrIParams(i)->getSize();
            if(sizeLBitem != sizeIncrItem)
            {
                if(logger)
                {
                    std::stringstream ss;
                    ss << "Compact leftBound's " << i << "-th item size NOT MATCH the size of the same item of LeftBoundary in IParamsUnionCompact_Impl::areValidCTORparams()!";
                    logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, ss.str());
                }
                return false;
            }
        }
    }
    totalOfItems = increments->getTotalOfItemsInParamsUnion();
    for(size_t i=0; i < totalOfItems; ++i)
    {
        size_t incrementsSize = increments->ptrIParamsUnionToPtrIParams(i)->getSize();
        for(size_t j=0; j<incrementsSize; ++j){
            double inc=NAN;
            increments->ptrIParamsUnionToPtrIParams(i)->getParam(j, inc);
            //DEBUG BEGIN
            assert(inc > -DBL_MIN);
            //DEBUG END
        }
    }
    /** try to cast leftBound */
    bool isParamsFitModel = additiveModel->canCastModelParamsUnion(leftBound);
    if(isParamsFitModel == false)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's leftBound NOT MATCH model in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    /** try to cast rightBound */
    isParamsFitModel = additiveModel->canCastModelParamsUnion(rightBound);
    if(isParamsFitModel == false)
    {
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Compact's rightBound NOT MATCH model in IParamsUnionCompact_Impl::areValidCTORparams()!");
        return false;
    }
    /** DON't validate increments with
    additiveModel->canCastModelParamsUnion(increments);
    increments has been created as Union of Params1D!
    None Model's Params was in use!
    */

    return true;
}

/**************************************
* stupid non-static copy of
* staticEvaluateRelativeCoord()
* to call from non-static functions
***************************************/
int IParamsUnionCompact_Impl::evaluateRelativeCoord(double curCoord, double startCoord, double increment, size_t& relativeCoord) const
{
    assert(increment > DBL_MIN);
    if((curCoord < startCoord) || (increment < DBL_MIN))
    {
       return _ERROR_WRONG_ARGUMENT_;
    }

//    double tmp = (curCoord - startCoord) / increment; //BUG floor((curCoord - startCoord) / increment); //BUG with roundl((curCoord - startCoord) / increment);

    double tmpStartCoord = startCoord;
    size_t tmpRelativeCoord = 0;
    while((curCoord - tmpStartCoord) > (increment/2.0f))
    {
        tmpStartCoord += increment;
        tmpRelativeCoord++;
    }
    /** ��� �������� - ����� �����, ��� ����� �������,
    ����� ������������� ���������� ������������� ����������,
    ������� � �������� �� ����� ���� � ����� �� �� �� ���� ����� ����� ���������
    */
//    if(tmp >= (double)BAD_ID){
//       return _ERROR_WRONG_ARGUMENT_;
//    }
//if(tmpRelativeCoord != static_cast<size_t>(tmp))
//{
//std::cout << "start:" << startCoord << " curr:" << curCoord << ":" << increment << "=>" << tmpRelativeCoord << ":" << static_cast<size_t>(tmp) << " " << (size_t)tmp << std::endl;
//   assert(tmpRelativeCoord == (size_t)(tmp));
//}
    relativeCoord = tmpRelativeCoord;//BUG static_cast<size_t>(tmp);
//DEBUG std::cout << std::endl << curCoord << "-" << startCoord << "/" << increment << "=" << relativeCoord << std::endl;
    return _RC_SUCCESS_;
}///end evaluateRelativeCoord
/*************************************************
* non-static member
*************************************************/
int IParamsUnionCompact_Impl::initPointsPerSingleUnitOfRelativeCoordinate()
{
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t tmpTotalOfPOints = 1;
        for(int j=i-1; j >= 0; --j){
            size_t trueIndex = m_orderOfCoordsToWalkThroughCompact[j];
            assert(BAD_ID/m_pointNum[trueIndex] > tmpTotalOfPOints);
            if(BAD_ID/m_pointNum[trueIndex] <= tmpTotalOfPOints)
            {
                m_pointsPerSingleUnitOfRelativeCoordinate.clear();
                return _ERROR_WRONG_INPUT_DATA_;
            }
            tmpTotalOfPOints *= m_pointNum[trueIndex];
        }
        assert(BAD_ID/m_pointNum[m_orderOfCoordsToWalkThroughCompact[i]] > tmpTotalOfPOints);
        if(BAD_ID/m_pointNum[m_orderOfCoordsToWalkThroughCompact[i]]  <= tmpTotalOfPOints)
        {
            m_pointsPerSingleUnitOfRelativeCoordinate.clear();
            return _ERROR_WRONG_INPUT_DATA_;
        }
        m_pointsPerSingleUnitOfRelativeCoordinate[m_orderOfCoordsToWalkThroughCompact[i]] = tmpTotalOfPOints;
    }
//DEBUG
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
std::cout << m_pointsPerSingleUnitOfRelativeCoordinate[m_orderOfCoordsToWalkThroughCompact[i]] << "\t";
    }
    std::cout << "\n";
    return _RC_SUCCESS_;
//DEBUG END
}

/*************************************************
* non-static member
*************************************************/
bool IParamsUnionCompact_Impl::initParamsUnionCompact(const IAdditiveModel* additiveModel, IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool stepValue, Logger* logger)
{
///TODO!!! Validation leftBound, rightBound, increments NOT nullptr ???
///TODO!!! Validate the size of compact < OUR_MAX_UNSIGNED_LONG!
    /** total of IParams into each IParamsUnion
    */
    additiveModel->getTotalOfAddends(m_totalOfItemsInParamsUnion);
    /** dimentionS of items in IParamsUnion
    startParams, endParams, increment;
    the array of size m_totalOfItemsInParamsUnion
    */
    m_paramsUnionItemsDimensions = new size_t[m_totalOfItemsInParamsUnion];
    if(m_paramsUnionItemsDimensions == NULL)
    {
        onCriticalErrorCleanUpParamsUnionCompact();
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "NO ROOM in IParamsUnionCompact_Impl::fillParamsUnionCompact!");
        return false;
    }
    for(size_t i=0; i < m_totalOfItemsInParamsUnion; ++i)
    {
        /** get ptr to i-th IParams into leftBound */
        IParams * ptrIParams = leftBound->ptrIParamsUnionToPtrIParams(i);
        if(ptrIParams == NULL)
        {
            onCriticalErrorCleanUpParamsUnionCompact();
            if(logger)
                logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, "In leftBound the IParams item NOT FOUND in  IParamsUnionCompact_Impl::fillParamsUnionCompact.");
            return false;
        }
        m_paramsUnionItemsDimensions[i] = ptrIParams->getSize();
        /** dimension of compact as result of
        IParamsUnion's items concatenation
        OR
        dimention of ech IParamsUnion
        startParams, endParams, increment;
        */
        m_paramsSpaceDimension += m_paramsUnionItemsDimensions[i];
    }
    m_orderOfCoordsToWalkThroughCompact = new size_t[m_paramsSpaceDimension];
    if(!m_orderOfCoordsToWalkThroughCompact)
    {
        onCriticalErrorCleanUpParamsUnionCompact();
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "No room to allocate orderOfCoords in ParamsUnionCompact::fillParamsUnionCompact()!");
        return false;
    }
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        m_orderOfCoordsToWalkThroughCompact[i] = i;
    }

    m_startParamsUnion = leftBound->clone();
    m_endParamsUnion   = rightBound->clone();
    m_increment = increments->clone();
    /** ptr to current ARRAY IParamsUnion in this Compact */
    m_currentParamsUnion = m_startParamsUnion->clone();
    m_additiveModel = const_cast<IAdditiveModel*> (additiveModel);

    m_pointNum.resize(m_paramsSpaceDimension,0);
    m_pointsPerSingleUnitOfRelativeCoordinate.resize(m_paramsSpaceDimension,0);

    m_coordsOfCurrentParamsUnionInCompact.resize(m_paramsSpaceDimension, 0);
    /** init m_pointNum array */
    for(size_t i=0, index=0; (i < m_totalOfItemsInParamsUnion) && (index < m_paramsSpaceDimension); ++i)
    {
        IParams * ptrStartParams = m_startParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrEndParams   = m_endParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrIncrement   = m_increment->ptrIParamsUnionToPtrIParams(i);
        for(size_t j=0; j < m_paramsUnionItemsDimensions[i]; ++j)
        {
            double startParamsElement =NAN;
            double endParamsElement   =NAN;
            int rc = ptrStartParams->getParam(j, startParamsElement);
            if(rc != _RC_SUCCESS_)
            {
                onCriticalErrorCleanUpParamsUnionCompact();
                if(logger)
                    logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Can not get value from start params in ParamsUnionCompact::fillParamsUnionCompact()!");
                return false;
            }
            rc = ptrEndParams->getParam(j, endParamsElement);
            if(rc != _RC_SUCCESS_)
            {
                onCriticalErrorCleanUpParamsUnionCompact();
                if(logger)
                    logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Can not get value from end params in ParamsUnionCompact::fillParamsUnionCompact()!");
                return false;
            }
            if (startParamsElement > endParamsElement)
            {
                double tmpDbl = startParamsElement;
                startParamsElement = endParamsElement;
                rc = ptrStartParams->setParam(j, startParamsElement);
                if(rc != _RC_SUCCESS_)
                {
                    onCriticalErrorCleanUpParamsUnionCompact();
                    return false;
                }
                endParamsElement = tmpDbl;
                rc = ptrEndParams->setParam(j, endParamsElement);
                if(rc != _RC_SUCCESS_)
                {
                    onCriticalErrorCleanUpParamsUnionCompact();
                    return false;
                }
            }
            /** if startParamsElement == endParamsElement */
            if (DBL_MIN > fabs(startParamsElement - endParamsElement))
            {
                m_pointNum[index] = 1;
            }
            else if((isNAN_double(startParamsElement)) || (isNAN_double(endParamsElement)))
            {
                if((isNAN_double(startParamsElement)) && (!isNAN_double(endParamsElement)))
                {
                    rc = ptrStartParams->setParam(j, endParamsElement);
                    if(rc != _RC_SUCCESS_)
                    {
                        onCriticalErrorCleanUpParamsUnionCompact();
                        return false;
                    }
                }
                if((!isNAN_double(startParamsElement)) && (isNAN_double(endParamsElement)))
                {
                    rc = ptrEndParams->setParam(j, startParamsElement);
                    if(rc != _RC_SUCCESS_)
                    {
                        onCriticalErrorCleanUpParamsUnionCompact();
                        return false;
                    }
                }
                else
                {
                    rc = ptrStartParams->setParam(j, DEFAULT_VALUE_TO_REPLACE_NAN_PARAM);
                    if(rc != _RC_SUCCESS_)
                    {
                        onCriticalErrorCleanUpParamsUnionCompact();
                        return false;
                    }
                    rc = ptrEndParams->setParam(j,   DEFAULT_VALUE_TO_REPLACE_NAN_PARAM);
                    if(rc != _RC_SUCCESS_)
                    {
                        onCriticalErrorCleanUpParamsUnionCompact();
                        return false;
                    }
                    rc = ptrIncrement->setParam(j, DEFAULT_VALUE_TO_REPLACE_NAN_PARAM);
                    if(rc != _RC_SUCCESS_)
                    {
                        onCriticalErrorCleanUpParamsUnionCompact();
                        return false;
                    }
                    startParamsElement = endParamsElement = DEFAULT_VALUE_TO_REPLACE_NAN_PARAM;
                    m_pointNum[index] = 1;
                }
            }
            double incrementParamsElement = NAN;
            rc = ptrIncrement->getParam(j, incrementParamsElement);
            if(rc != _RC_SUCCESS_)
            {
                onCriticalErrorCleanUpParamsUnionCompact();
                return false;
            }
            /** if startParamsElement == endParamsElement */
            if(m_pointNum[index] != 1)
            {
                /** increment is stepValue */
                if (stepValue)
                {
                    size_t parts =0;
                    rc = evaluateRelativeCoord(endParamsElement, startParamsElement, incrementParamsElement, parts);
                    if(rc != _RC_SUCCESS_)
                    {
                        onCriticalErrorCleanUpParamsUnionCompact();
                        return false;
                    }
                    m_pointNum[index] = parts + 1;
                }
                else /** increment is totalOfSteps */
                {
                    double pointsNum = incrementParamsElement;
                    m_pointNum[index] = (unsigned)pointsNum;
                    /** recalculate increment to take into account round-off */
                    incrementParamsElement = pointsNum > 1 ? (endParamsElement - startParamsElement) / (pointsNum - 1) : incrementParamsElement;
                    //DEBUG BEGIN
                    assert(incrementParamsElement > -DBL_MIN);
                    //DEBUG END
                    rc = ptrIncrement->setParam(j, incrementParamsElement);
                    if(rc != _RC_SUCCESS_)
                    {
                        onCriticalErrorCleanUpParamsUnionCompact();
                        return false;
                    }
                }
            }
            if (logger)
            {
                std::stringstream logStr;
                logStr << "Fill compact for param " << index << std::endl <<
                "Begin: " << startParamsElement << std::endl <<
                "End: " << endParamsElement << std::endl <<
                "Increment: " << incrementParamsElement << std::endl <<
                "Num of points: " << m_pointNum[index];
                logger->log(ALGORITHM_GENERAL_INFO, logStr.str());
            }
#ifdef _DEBUG_
std::stringstream logStr;
logStr << "Fill compact for param " << index << std::endl <<
"Begin: " << startParamsElement << std::endl <<
"End: " << endParamsElement << std::endl <<
"Increment: " << incrementParamsElement << std::endl <<
"Num of points: " << _pointNum[index];
std::cout << logStr.str() << std::endl;
#endif
            /** get to the next item in _pointNum[] */
            ++index;
        }///end for-loop over all items of single item in ParamsUnion
    }///end for-loop over all items in ParamsUnion to init m_pointNum array
    /**
     ����� �� ���������, ��� �������� ��������� �����, ������������� ����������������� ��������,
     �� ����������� ����������� BAD_ID
    */
    int rc = initPointsPerSingleUnitOfRelativeCoordinate();
    if(rc != _RC_SUCCESS_)
    {
        onCriticalErrorCleanUpParamsUnionCompact();
        if(logger)
            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Too many points into compact, in ParamsCompact::initParamsUnionCompact()!");
        return false;
    }
    /** id of current ARRAY IParamsUnion in this Compact */
    if(getSize()){
        m_id = 0;
        m_isEmpty = false;
    }
    if(logger)
    {
        size_t size = getSize();
        std::stringstream logStr;
        logStr << "Total size of compact: " << size;
        logger->log(ALGORITHM_GENERAL_INFO, logStr.str());
    }
    return true;
}
size_t IParamsUnionCompact_Impl::getDimOfParamsUnion() const
{
    return m_paramsSpaceDimension;
}
/**********************************************
*  Get total of points in ParamsUnion Compact
***********************************************/
size_t IParamsUnionCompact_Impl::getSize() const
{
    size_t sizeOfSet = 1;
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        sizeOfSet *= m_pointNum[i];
    }
    return sizeOfSet;
}

/**********************************************
* Get main metrics of ParamsCompact as string
***********************************************/
std::string IParamsUnionCompact_Impl::toString() const
{
    std::stringstream sStr;
    sStr << m_startParamsUnion->toString() << DELIMITER_LIST_ITEMS
    << m_endParamsUnion->toString() << DELIMITER_LIST_ITEMS
    << m_increment->toString();
    return sStr.str();
}
/**********************************************
* Evaluate relative coordinate of given point
along indexOfAxis- direction/model param
***********************************************/
int IParamsUnionCompact_Impl::evaluateRelativeCoordAlongGivenAxis(size_t indexOfAxis, const IParamsUnion * const point, size_t& relativeCoord) const
{
    if(m_isEmpty){
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(!isPointInCompact(point))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t index = m_orderOfCoordsToWalkThroughCompact[indexOfAxis];
    size_t indexOfUnionItem;
    int rc = getIndexOfUnionItem_byIndexOfParam(index, indexOfUnionItem);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t indexInIParams = index;
    rc = indexInParamsUnionToIndexInIParams(indexInIParams);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    double pointParam = NAN;
    rc = point->ptrIParamsUnionToPtrIParams(indexOfUnionItem)->getParam(indexInIParams, pointParam);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    double startPointParam=NAN;
    rc = m_startParamsUnion->ptrIParamsUnionToPtrIParams(indexOfUnionItem)->getParam(indexInIParams, startPointParam);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    double increment = NAN;
    rc = m_increment->ptrIParamsUnionToPtrIParams(indexOfUnionItem)->getParam(indexInIParams, increment);
    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    relativeCoord = 0;
    if( (pointParam - startPointParam) > DBL_MIN)
    {
        rc = evaluateRelativeCoord(pointParam, startPointParam, increment, relativeCoord);
        if(_RC_SUCCESS_ != rc)
        {
            /** assign -1 to size_t to retrieve the max unsigned int == impossible relative coordinate */
            relativeCoord = IMPOSSIBLE_RELATIVE_COORDINATE;
            return rc;
        }
    }
    return _RC_SUCCESS_;
}
/**********************************************
* Get min total of increments
* along some axis in ParamsUnion Compact
***********************************************/
size_t IParamsUnionCompact_Impl::getMinRelativeSizeAlongAxis() const
{
    size_t minRelativeSize = UINT_MAX;
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t tmpRelativeSize = (((double)m_pointNum[i] - 1.0f) > - 1.0f ) ? (size_t)(m_pointNum[i] - 1) : (size_t)0;
        if (tmpRelativeSize){
            minRelativeSize = minRelativeSize < tmpRelativeSize ? minRelativeSize : tmpRelativeSize;
        } else {
            /** 0-th size is the smallest (single point) */
            minRelativeSize = 0;
            break;
        }
    }
    return minRelativeSize;
}
/**********************************************
* Get max total of increments
* along some axis in ParamsUnion Compact
***********************************************/
size_t IParamsUnionCompact_Impl::getMaxRelativeSizeAlongAxis() const
{
    size_t maxRelativeSize = 0;
    for (size_t i = 0; i < m_paramsSpaceDimension; ++i)
    {
        size_t tmpRelativeSize = (((double)m_pointNum[i] - 1.0f) > - 1.0f ) ? (size_t)(m_pointNum[i] - 1) : (size_t)0;
        if (maxRelativeSize < tmpRelativeSize){
            maxRelativeSize = tmpRelativeSize;
        }
    }
    return maxRelativeSize;
}
/**********************************************
*  Get Id of current point in ParamsUnion Compact
***********************************************/
size_t IParamsUnionCompact_Impl::getCurrentId() const
{
    return ((m_isEmpty) || (m_id == BAD_ID)) ? (size_t)-1 : m_id;
}

bool IParamsUnionCompact_Impl::isPointInCompact(const IParamsUnion* const point) const
{
    if(m_isEmpty){
        return false;
    }
    /**
     First validate the dimension of IParamsUnion
    */
    if(m_totalOfItemsInParamsUnion != point->getTotalOfItemsInParamsUnion())
    {
        return false;
    }
    /**
     Second validate the dimensions of IParams in IParamsUnion
    */
    for(size_t i =0; i < m_totalOfItemsInParamsUnion; ++i)
    {
        if(m_paramsUnionItemsDimensions[i] != point->ptrIParamsUnionToPtrIParams(i)->getSize())
        {
            return false;
        }
    }
    /**
     Finally validate the point is into compact
    */
    for(size_t i =0; i < m_totalOfItemsInParamsUnion; ++i)
    {
        for(size_t j=0; j < m_paramsUnionItemsDimensions[i]; ++j)
        {
            double pointParam;
            point->ptrIParamsUnionToPtrIParams(i)->getParam(j, pointParam);
            double boundaryPointParam;
            m_startParamsUnion->ptrIParamsUnionToPtrIParams(i)->getParam(j, boundaryPointParam);
            if(fabs(pointParam - boundaryPointParam) > DBL_MIN)
            {
                if(pointParam < boundaryPointParam)
                    return false;
            }
            m_endParamsUnion->ptrIParamsUnionToPtrIParams(i)->getParam(j, boundaryPointParam);
            if(fabs(pointParam - boundaryPointParam) > DBL_MIN)
            {
                if(pointParam > boundaryPointParam)
                    return false;
            }
        }
    }
    return true;
}

/**
* PARAMS [IN/OUT] index -- index in ParamsUnion as input,
                           index in item of ParamsUnion (in some IParams) as output
*/
int IParamsUnionCompact_Impl::indexInParamsUnionToIndexInIParams(size_t& index) const
{
    if(m_isEmpty)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(index > m_paramsSpaceDimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** �������� ������ IParams � ParamsUnion,
    �.�. ��� IParams � ParamsUnion ����������� �������� �� ������� � ������ �������� ParamsUnion
    � ����� indexOfParam - ��� ���������� ����� ����������
    (���������� Params � ParamsUnion, ����� ��������� ���������� ��� ������������ �������� � ���� IParams)
    */
    long indexOfParam = index;
    size_t indexOfItemInParamsUnion=0;
    for(indexOfItemInParamsUnion=0; indexOfItemInParamsUnion < m_totalOfItemsInParamsUnion; ++indexOfItemInParamsUnion)
    {
        if(indexOfParam < (long)m_paramsUnionItemsDimensions[indexOfItemInParamsUnion])
        {
            break;
        }
        indexOfParam -= m_paramsUnionItemsDimensions[indexOfItemInParamsUnion];
    }
    /** ���� �������� ��������� ������ IParams � ParamsUnion,
    � ������� ��������� Param � �������� index
    */
    if(indexOfItemInParamsUnion == m_totalOfItemsInParamsUnion)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    index = indexOfParam;
    return _RC_SUCCESS_;
}
/***************************
* PARAM [IN]   indexOfParam -- index of param in concatenated IParamsUnion
* PARAM [OUT] indexOfUnionItem -- index of IParamsUnion's item with param of given index
****************************/
int IParamsUnionCompact_Impl::getIndexOfUnionItem_byIndexOfParam(size_t indexOfParam, size_t& indexOfUnionItem) const
{
    if(m_isEmpty)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(indexOfParam > m_paramsSpaceDimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** �������� ������ IParams � ParamsUnion,
    �.�. ��� IParams � ParamsUnion ����������� �������� �� ������� � ������ �������� ParamsUnion
    � ����� indexOfParam - ��� ���������� ����� ����������
    (���������� Params � ParamsUnion, ����� ��������� ���������� ��� ������������ �������� � ���� IParams)
    */
    long index = indexOfParam;
    size_t indexOfItemInParamsUnion=0;

    for(indexOfItemInParamsUnion=0; indexOfItemInParamsUnion < m_totalOfItemsInParamsUnion; ++indexOfItemInParamsUnion)
    {
        if(index < (long)m_paramsUnionItemsDimensions[indexOfItemInParamsUnion])
        {
            break;
        }
        index -= m_paramsUnionItemsDimensions[indexOfItemInParamsUnion];
    }
    /** ���� �������� ��������� ������ IParams � ParamsUnion,
    � ������� ��������� Param � �������� index
    */
    if(indexOfItemInParamsUnion == m_totalOfItemsInParamsUnion)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    indexOfUnionItem = indexOfItemInParamsUnion;
    return _RC_SUCCESS_;
}

int IParamsUnionCompact_Impl::getId_byIParamsUnion(const IParamsUnion * const point, size_t& id) const
{
    if(m_isEmpty)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(!isPointInCompact(point))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
//DEBUG
//    std::stringstream sstr;
    std::vector<size_t> coordsRelative;
    coordsRelative.resize(m_paramsSpaceDimension);
//END DEBUG
    int rc;
    size_t tmpId = 0;
    size_t trueIndex;
    size_t relativeCoord = 0;
//PREV
    for (int i = m_paramsSpaceDimension - 1; i > 0; --i)
    {
        relativeCoord = 0;
        trueIndex = m_orderOfCoordsToWalkThroughCompact[i];
        rc = evaluateRelativeCoordAlongGivenAxis( trueIndex, point, relativeCoord);

        if(rc)
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
//DEBUG
coordsRelative[trueIndex] = relativeCoord;
assert(m_pointNum[trueIndex] > relativeCoord);
//END DEBUG
        if(relativeCoord)
        {
//            size_t tmpPointsPerSingleUnitOfRelativeCoordinate = 1;
//            for(int j=i-1; j >=0; --j)
//            {
//                size_t tmpTrueIndex = m_orderOfCoordsToWalkThroughCompact[j];
//                assert(BAD_ID/m_pointNum[tmpTrueIndex] > tmpPointsPerSingleUnitOfRelativeCoordinate);
//                tmpPointsPerSingleUnitOfRelativeCoordinate *= m_pointNum[tmpTrueIndex];
//            }
//DEBUG
//if(tmpPointsPerSingleUnitOfRelativeCoordinate != m_pointsPerSingleUnitOfRelativeCoordinate[trueIndex])
//{
//sstr << "point:" << point->toString() << std::endl;
//sstr << "org_compact:" << this->toString() << std::endl;
//sstr << "index:" << trueIndex << ":" << tmpPointsPerSingleUnitOfRelativeCoordinate << ":" << m_pointsPerSingleUnitOfRelativeCoordinate[trueIndex] << std::endl;
//for(int k = 0; k < m_paramsSpaceDimension; ++k)
//{
//sstr << coordsRelative[k] << "\t";
//}
//sstr << std::endl;
//std::cout << sstr.str();
//assert(tmpPointsPerSingleUnitOfRelativeCoordinate != m_pointsPerSingleUnitOfRelativeCoordinate[trueIndex]);
//}
//DEBUG END
//            relativeCoord *= tmpPointsPerSingleUnitOfRelativeCoordinate;
            relativeCoord *= m_pointsPerSingleUnitOfRelativeCoordinate[trueIndex];
//DEBUG
//if(relativeCoord > getSize())
//{
//sstr << "point:" << point->toString() << std::endl;
//sstr << "org_compact:" << this->toString() << std::endl;
//sstr << "index:" << trueIndex << ":" << "m_pointNum[trueIndex]=" << m_pointNum[trueIndex] << " relativeCoord=" << relativeCoord << std::endl;
//sstr << "index:" << trueIndex << ":" << tmpPointsPerSingleUnitOfRelativeCoordinate << ":" << m_pointsPerSingleUnitOfRelativeCoordinate[trueIndex] << std::endl;
//for(int k = 0; k < m_paramsSpaceDimension; ++k)
//{
//sstr << coordsRelative[k] << "\t";
//}
//sstr << std::endl;
//std::cout << sstr.str();
//            assert((relativeCoord < getSize()));
//}
//DEBUG END
        }
        tmpId += relativeCoord;
//DEBUG
//if(tmpId >= getSize())
//{
//sstr << "point:" << point->toString() << std::endl;
//sstr << "org_compact:" << this->toString() << std::endl;
//sstr << "index:" << trueIndex << ":" << "m_pointNum[trueIndex]=" << m_pointNum[trueIndex] << " relativeCoord=" << relativeCoord << std::endl;
//sstr << "index:" << trueIndex << ":" << ":" << m_pointsPerSingleUnitOfRelativeCoordinate[trueIndex] << std::endl;
//sstr << "index:" << trueIndex << ":" << tmpId << ":" << getSize() << std::endl;
//for(int k = 0; k < m_paramsSpaceDimension; ++k)
//{
//sstr << coordsRelative[k] << "\t";
//}
//sstr << std::endl;
//std::cout << sstr.str();
//        assert(tmpId < getSize());
//}
//DEBUG END
    }///end for loop over all axes  except the very first (according to walk through order) axis (0-th axis)
    /**
    Finally process coordinate
    along the very first (according to walk through order) axis
    */
    relativeCoord = 0;
    trueIndex = m_orderOfCoordsToWalkThroughCompact[0];
    rc = evaluateRelativeCoordAlongGivenAxis(trueIndex, point, relativeCoord);

//DEBUG
//if( m_pointNum[trueIndex] < relativeCoord )
//{
//sstr << "point:" << point->toString() << std::endl;
//sstr << "org_compact:" << this->toString() << std::endl;
//sstr << "index:" << trueIndex << ":" << "m_pointNum[trueIndex]=" << m_pointNum[trueIndex] << " relativeCoord=" << relativeCoord << std::endl;
//sstr << "index:" << trueIndex << ":" << tmpId << ":" << getSize() << std::endl;
//for(int k = 0; k < m_paramsSpaceDimension; ++k)
//{
//sstr << coordsRelative[k] << "\t";
//}
//sstr << std::endl;
//std::cout << sstr.str();
//    assert(m_pointNum[trueIndex] > relativeCoord);
//}
//DEBUG END

    if(rc)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    tmpId += relativeCoord;

//DEBUG
//if( tmpId > getSize() )
//{
//sstr << "index:" << trueIndex << ":" << "m_pointNum[trueIndex]=" << m_pointNum[trueIndex] << " relativeCoord=" << relativeCoord << std::endl;
//sstr << "index:" << trueIndex << ":" << tmpId << ":" << getSize() << std::endl;
//for(int k = 0; k < m_paramsSpaceDimension; ++k)
//{
//sstr << coordsRelative[k] << "\t";
//}
//sstr << std::endl;
//std::cout << sstr.str();
//        assert(tmpId < getSize());
//}
//DEBUG END

    id = tmpId;
    return _RC_SUCCESS_;
}


/******************************************************************
*  setCurrentId(size_t id)
 changes inner _id and _currentParamsUnion into ParamsUnion Compact
*******************************************************************/
int IParamsUnionCompact_Impl::setCurrentId(size_t id)
{
    if(m_isEmpty)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t size = getSize();

    if (id > (size - 1))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t tmpId = id;
    /**
    Here we pass NULL instead of Logger* because that pointer is ignored into the code of IParamsUnion::createParamsUnion()
    So, NO ERROR expected.
    */
    /** ������� ����������������� ��������� IParamsUnion !
    � ��� � ����� �����, ���� ��� ��, �� ��������� �� ���� ����� � _currentParamsUnion
    � ������ ��������� ptrIParamsUnion!
    */
    IParamsUnion * ptrTmpIParamsUnion = m_startParamsUnion->clone(); // BAD! IParamsUnion::createParamsUnion( _totalOfItemsInParamsUnion, (const size_t*) _paramsUnionItemsDimensions, NULL);
    if(ptrTmpIParamsUnion == NULL)
    {
        return _ERROR_NO_ROOM_;
    }
    /** NB : We INCREMENT index into the innermost for-loop */
    size_t i=0;
    /**
    i to walk through compact's axis as result of
    IParamsUnion's items concatenation
    */
    while(i < m_paramsSpaceDimension)
    {
        /** index of the i-th Param in IParamsUnion */
        size_t index = m_orderOfCoordsToWalkThroughCompact[i];
        /** �������� ������ IParams � ParamsUnion,
        �.�. ��� IParams � ParamsUnion ����������� �������� �� ������� � ������ �������� ParamsUnion
        � ����� index - ��� ���������� ����� ����������
        (���������� Params � ParamsUnion, ����� ��������� ���������� ��� ������������ �������� � ���� IParams)
        */
        size_t indexOfItemInParamsUnion=0;
        int rc = getIndexOfUnionItem_byIndexOfParam(index, indexOfItemInParamsUnion);
        if(_RC_SUCCESS_ != rc)
        {
            delete ptrTmpIParamsUnion; ptrTmpIParamsUnion=NULL;
            return rc;
        }
        /** index of the i-th Param in its IParams */
        size_t indexInParamsItem=index;
        rc = indexInParamsUnionToIndexInIParams(indexInParamsItem);
        if(_RC_SUCCESS_ != rc)
        {
            delete ptrTmpIParamsUnion; ptrTmpIParamsUnion=NULL;
            return rc;
        }
        /** �������� ��� IParams � ParamsUnion,
        � ������� ��������� Param � �������� index,
        ������, �����, ��������� ��� �����������
        */
        IParams * ptrStartParams         = m_startParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrEndParams           = m_endParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrIncrement           = m_increment->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrTmpIParamsUnionItem = ptrTmpIParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        m_coordsOfCurrentParamsUnionInCompact[index] = tmpId % m_pointNum[index];
        tmpId /= m_pointNum[index];
        /**
        �������� � ����� ��������������� �����
        ������ �� ������������ ���� ���������������� ������������
        ����������� _paramsSpaceDimension,
        �� ���� ������ ����������, �� �������
        ��� �� �������� � ������ ������� �������� �� ���� ����������.
        ��������� ����� ������������ ���,
        �� �������������� �������� ����� ��� �
        ������� � incrementParamsElement �������� �� ���� ����������,
        ����� ��� � ������ ���������� _currentParams
        �� �������, ���������������� ���� ������������ ���.
        �������� ��������� ���������� � ����� �� � ��������������� ����� �������������� ParamsUnion.
        */
        if ((m_pointNum[index]) && (m_coordsOfCurrentParamsUnionInCompact[index] < (m_pointNum[index] - 1)))
        {
            double startParamsElement       =NAN;
            double incrementParamsElement   =NAN;
            /** ����� ������ ������ ���������!
            */
            rc = ptrIncrement->getParam(indexInParamsItem, incrementParamsElement);
            if(rc)
            {
                delete ptrTmpIParamsUnion;ptrTmpIParamsUnion=NULL;
                return _ERROR_WRONG_MODEL_PARAMS_;
            }
///BUG ��� ����� _currentParams[i] +=incrementParamsElement!
            ptrStartParams->getParam(indexInParamsItem, startParamsElement);
            double currentParamsElement   =NAN;
            currentParamsElement = startParamsElement + (m_coordsOfCurrentParamsUnionInCompact[index] * incrementParamsElement);
            ptrTmpIParamsUnionItem->setParam(indexInParamsItem, currentParamsElement);
        }

        /** ������, ����� ������ �������� ��������� ������� �������� �� index-�� ������������ ���
        */
        if ((m_pointNum[index]) && (m_coordsOfCurrentParamsUnionInCompact[index] == (m_pointNum[index] - 1)))
        {
            double endParamsElement   =NAN;
            rc = ptrEndParams->getParam(indexInParamsItem, endParamsElement);
            if(rc)
            {
                delete ptrTmpIParamsUnion; ptrTmpIParamsUnion=NULL;
                return _ERROR_WRONG_MODEL_PARAMS_;
            }
            ptrTmpIParamsUnionItem->setParam(indexInParamsItem, endParamsElement);
        }
        /** ������, ����� ������ ��� �������� ��������� ������� �������� �� index-�� ������������� ���
        */
        if (m_coordsOfCurrentParamsUnionInCompact[index] == 0)
        {
            double startParamsElement   = NAN;
            rc = ptrStartParams->getParam(indexInParamsItem, startParamsElement);
            if(rc)
            {
                delete ptrTmpIParamsUnion; ptrTmpIParamsUnion=NULL;
                return _ERROR_WRONG_MODEL_PARAMS_;
            }
            ptrTmpIParamsUnionItem->setParam(indexInParamsItem, startParamsElement);
        }
        ++i;
    }///end loop over all params coords

    /** ���� ������� ������� ��������� ����� ��� ������ �������� ParamsUnion
    �� ��������� ��������� (����� �� ��������������� ������) ��� ��������� �� ��������� ����� ptrTmpIParamsUnion
    � ������� _currentParamsUnion
    */
    /** NB : We INCREMENT index into the innermost for-loop */
    i=0;
    while(i < m_paramsSpaceDimension)
    {
        /** index of the i-th Param in the entire IParamsUnion */
        size_t index = m_orderOfCoordsToWalkThroughCompact[i];
        /** �������� ������ ���� IParams  � IParamsUnion,
        � ������� ����� ���� �������� */
        size_t indexOfItemInParamsUnion = 0;
        int rc = getIndexOfUnionItem_byIndexOfParam(index, indexOfItemInParamsUnion);
        if(_RC_SUCCESS_ != rc)
        {
            delete ptrTmpIParamsUnion; ptrTmpIParamsUnion=NULL;
            return rc;
        }
        /** index of the i-th Param in its IParams */
        size_t indexInParamsItem=index;
        rc = indexInParamsUnionToIndexInIParams(indexInParamsItem);
        if(_RC_SUCCESS_ != rc)
        {
            delete ptrTmpIParamsUnion; ptrTmpIParamsUnion=NULL;
            return rc;
        }

        IParams * ptrTmpCurrParams      = ptrTmpIParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrCurrParams         = m_currentParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        double paramsElement   = NAN;
        ptrTmpCurrParams->getParam(indexInParamsItem, paramsElement);
        ptrCurrParams->setParam(indexInParamsItem, paramsElement);
        ++i;
    }
    delete ptrTmpIParamsUnion; ptrTmpIParamsUnion=NULL;
    m_id = id;
    return _RC_SUCCESS_;
}///end setCurrentId()

int IParamsUnionCompact_Impl::setOrderOfCoordsToWalkThroughCompact(size_t size, const size_t * newOrder)
{
    if(m_isEmpty)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    if(size != m_paramsSpaceDimension)
    {
        return _ERROR_OUT_OF_ARRAY_BOUNDARY_;
    }
    /** validate newOrder */
    size_t sum1 = 0;
    size_t sum2 = 0;
    for(size_t i=0; i < m_paramsSpaceDimension; ++i)
    {
        sum1 += i;
        sum2 += newOrder[i];
    }
    if(sum1 != sum2)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** set new order Of Coords To Walk Through Compact */
    std::memcpy(m_orderOfCoordsToWalkThroughCompact, newOrder, size*sizeof(size_t));

    int rc = initPointsPerSingleUnitOfRelativeCoordinate();
    if(rc != _RC_SUCCESS_)
    {
        onCriticalErrorCleanUpParamsUnionCompact();
//        if(logger)
//            logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Too many points into compact, in ParamsCompact::setOrderOfCoordsToWalkThroughCompact()!");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    return _RC_SUCCESS_;
}

/***************************************************
* getFirstPtr() mimic iterator over ParamsUnion Compact
***************************************************/
IParamsUnion* IParamsUnionCompact_Impl::getFirstPtr()
{
    if(m_isEmpty){
        return nullptr;
    }
    m_id = 0;
    /** �� ������� ����� ������� delete � ���������� � clone,
    ����� ������ ��� �� ��������� �������������� ������
    */
    for(size_t i=0, index=0; (i < m_totalOfItemsInParamsUnion) && (index < m_paramsSpaceDimension); ++i)
    {
        IParams * ptrStartParams     = m_startParamsUnion->ptrIParamsUnionToPtrIParams(i);
        IParams * ptrCurrentParams   = m_currentParamsUnion->ptrIParamsUnionToPtrIParams(i);

        for(size_t j=0; j < m_paramsUnionItemsDimensions[i]; ++j)
        {
            double startParamsElement =NAN;
            int rc = ptrStartParams->getParam(j, startParamsElement);
            if(rc)
            {
                m_id = BAD_ID;
                return nullptr;
            }
            /** ������������� ������ ���������� "������� ����� ��������"
            ���������� ������� "��������� ����� ��������"
            */
            rc = ptrCurrentParams->setParam(j, startParamsElement);
            if(rc)
            {
                m_id = BAD_ID;
                return nullptr;
            }
            /** make id =0 for all elements/items of current IParamsUnion
             (vector of size _paramsSpaceDimension to keep ids of current IParamsUnion along each coordinate axis)
            in this Compact
            �� ���� �������� ������������� ���������� �������, ���������������� ������� ���������� "������� ����� ��������"
            */
            m_coordsOfCurrentParamsUnionInCompact[index] = 0;
            /** get to the next item in _pointNum[] */
            ++index;
        }///end for-loop over all items of single item in ParamsUnion
    }///end for-loop over all items in ParamsUnion
    if(getSize() > 1)
        m_end = false;
#ifdef _DEBUG_
std::cout << _id << ":" << _end << std::endl;
#endif // _DEBUG_
///TODO!!! ��������� � shared_ptr
    return m_currentParamsUnion;
}
/****************************************************
*  getNextPtr() mimic iterator over ParamsUnion Compact
****************************************************/
/**
NB returns ptr to existing item into Set!
It does Not copy! => Don't free that ptr!
*/
IParamsUnion* IParamsUnionCompact_Impl::getNextPtr()
{
    if(m_isEmpty){
        return nullptr;
    }
    ++m_id;
    /** �� ������� ����� ������� delete � ���������� � clone,
    ����� ������ ��� �� ��������� �������������� ������
    */
    size_t i=0;
    while(i < m_paramsSpaceDimension)
    {
        size_t index = m_orderOfCoordsToWalkThroughCompact[i];
        /** �������� ������ Params � ParamsUnion */
        long indexInParamsItem = index;
        size_t indexOfItemInParamsUnion=0;
        for(indexOfItemInParamsUnion=0; indexOfItemInParamsUnion < m_totalOfItemsInParamsUnion; ++indexOfItemInParamsUnion)
        {
            if(indexInParamsItem < (long)m_paramsUnionItemsDimensions[indexOfItemInParamsUnion])
            {
                break;
            }
            indexInParamsItem -= m_paramsUnionItemsDimensions[indexOfItemInParamsUnion];
        }
        /** �� �����-�� �������� ������ � _orderOfCoordsToWalkThroughCompact,
        ������������ ������������ ������� ������ ��������� ����� ��������,
         �������� ����������!
        */
        if(indexOfItemInParamsUnion == m_totalOfItemsInParamsUnion)
        {
            m_id = BAD_ID;
            return nullptr;
        }
        IParams * ptrStartParams = m_startParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrEndParams   = m_endParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrIncrement   = m_increment->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrCurrParams  = m_currentParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        /**
        �������� � ����� ��������������� �����
        ������ �� ������������ ���� ���������������� ������������
        ����������� _paramsSpaceDimension,
        �� ���� ������ ����������, �� �������
        ��� �� �������� � ������ ������� �������� �� ���� ����������.
        ��������� ����� ������������ ���,
        �� �������������� �������� ����� ��� �
        ������� � incrementParamsElement �������� �� ���� ����������,
        ����� ��� � ������ ���������� _currentParams
        �� �������, ���������������� ���� ������������ ���.
        �������� ��������� ���������� � ����� �� � ��������������� ����� �������������� ParamsUnion.
        */
        /**
        ���� ������� ���������� �� ������������� ����� index-���
        */
        if ((m_pointNum[index] > 1) && ( (m_coordsOfCurrentParamsUnionInCompact[index] + 2) < m_pointNum[index]))
        {
            double incrementParamsElement   =NAN;
            /** ����� ������ ������ ���������!
            */
            int rc = ptrIncrement->getParam(indexInParamsItem, incrementParamsElement);
            if(rc)
            {
///TODO!!! ����� �����, �������� ������  _id = -1;
                m_id = BAD_ID;
                return nullptr;
            }
///BUG ��� ����� _currentParams[i] +=incrementParamsElement!
            double currentParamsElement   =NAN;
            ptrCurrParams->getParam(indexInParamsItem, currentParamsElement);
            currentParamsElement += incrementParamsElement;
            ptrCurrParams->setParam(indexInParamsItem, currentParamsElement);
            m_coordsOfCurrentParamsUnionInCompact[index]++;
            /**
            ����� getSize() �������������� ������ 1-��
            �.�. (_pointNum[index] > 1)
            */
            if((getSize()) && ((m_id + 1) == getSize()))
                m_end = true;
///TODO!!! ��������� � shared_ptr
            return m_currentParamsUnion;
        }
        /** ������, ����� ������� ���������� - �������������,
        � ������ �������� ��������� ������� �������� �� index-�� ������������� ���
        */
        if ((m_coordsOfCurrentParamsUnionInCompact[index] + 2) == m_pointNum[index])
        {
            double endParamsElement   =NAN;
            int rc = ptrEndParams->getParam(indexInParamsItem, endParamsElement);
            if(rc)
            {
///TODO!!! ����� �����, �������� ������  _id = -1;
                m_id = BAD_ID;
                return NULL;
            }
            ptrCurrParams->setParam(indexInParamsItem, endParamsElement);
            m_coordsOfCurrentParamsUnionInCompact[index]++;
            /**
            ����� getSize() �������������� ������ ��� ����� 2
            �.�. (_coordsOfCurrentParamsUnionInCompact[index] + 2) == _pointNum[index]
            */
            if((getSize()) && ((m_id + 1) == getSize() ))
                m_end = true;
///TODO!!! ��������� � shared_ptr
            return m_currentParamsUnion;
        }
        /** ����� �� �����!
        ������, ����� ������� ���������� - ���������,
        � ������ �������� ������ ������� �������� �� index-�� ������������ ���
        */
        if ((m_coordsOfCurrentParamsUnionInCompact[index] + 1) == m_pointNum[index])
        {
            double startParamsElement   = NAN;
            int rc = ptrStartParams->getParam(indexInParamsItem, startParamsElement);
            if(rc)
            {
///                    _id = -1;
                m_id = BAD_ID;
                return nullptr;
            }
            ptrCurrParams->setParam(indexInParamsItem, startParamsElement);
            m_coordsOfCurrentParamsUnionInCompact[index] = 0;
            ++i;
/// NO WAY! return _currentParamsUnion; <- ����� ������� ����� ��������!
        }
    }///end loop over all params coords
    m_id = BAD_ID;
    return nullptr;
}
/******************************************************************************************************
 ����� ������ ����������� IParams � ������������� id, �.� ��������� � �������� ��� ����� �� ���������!
*******************************************************************************************************/
/**
* ���������� ��� ������ ��������� ��� ������!
*/
IParamsUnion* IParamsUnionCompact_Impl::getParamsUnionCopy(size_t id) const
{
//DEBUG
    std::vector<size_t> coordsRelative;
    coordsRelative.resize(m_paramsSpaceDimension);
//END DEBUG
    if(m_isEmpty){
        return nullptr;
    }
    size_t size = getSize();

    if (id > (size-1))
    {
        return NULL;
    }
    size_t tmpId = id;
    /**
    Here we pass NULL instead of Logger* because that pointer is ignored into the code of IParamsUnion::createParamsUnion()
    So, NO ERROR expected.
    */
    /** NB : We INCREMENT index into the innermost for-loop */
    IParamsUnion * ptrTmpIParamsUnion = m_startParamsUnion->clone(); // BAD! IParamsUnion::createParamsUnion( _totalOfItemsInParamsUnion, (const size_t*) _paramsUnionItemsDimensions, NULL);
    if(ptrTmpIParamsUnion == NULL)
    {
        return nullptr;
    }
    /** NB : We INCREMENT index 'i' into the innermost for-loop */
    size_t i=0;
    while(i < m_paramsSpaceDimension)
    {
        size_t index = m_orderOfCoordsToWalkThroughCompact[i];
        /** �������� ������ Params � ParamsUnion */
        size_t indexInParamsItem = index;
        size_t indexOfItemInParamsUnion=0;
        int rc = getIndexOfUnionItem_byIndexOfParam(indexInParamsItem, indexOfItemInParamsUnion);
        if(_RC_SUCCESS_ != rc)
        {
            delete ptrTmpIParamsUnion;
            return nullptr;
        }
        rc = indexInParamsUnionToIndexInIParams(indexInParamsItem);
        if(_RC_SUCCESS_ != rc)
        {
            delete ptrTmpIParamsUnion;
            return nullptr;
        }
        IParams * ptrStartParams         = m_startParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrEndParams           = m_endParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrIncrement           = m_increment->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        IParams * ptrTmpIParamsUnionItem = ptrTmpIParamsUnion->ptrIParamsUnionToPtrIParams(indexOfItemInParamsUnion);
        size_t coordsOfCurrentParamsUnionInCompact = tmpId % m_pointNum[index];
//DEBUG
coordsRelative[index]=coordsOfCurrentParamsUnionInCompact;
//END DEBUG
        tmpId /= m_pointNum[index];
        /**
        �������� � ����� ��������������� �����
        ������ �� ������������ ���� ���������������� ������������
        ����������� _paramsSpaceDimension,
        �� ���� ������ ����������, �� �������
        ��� �� �������� � ������ ������� �������� �� ���� ����������.
        ��������� ����� ������������ ���,
        �� �������������� �������� ����� ��� �
        ������� � incrementParamsElement �������� �� ���� ����������,
        ����� ��� � ������ ���������� _currentParams
        �� �������, ���������������� ���� ������������ ���.
        �������� ��������� ���������� � ����� �� � ��������������� ����� �������������� ParamsUnion.
        */
        if ((m_pointNum[index]) && (coordsOfCurrentParamsUnionInCompact) && (coordsOfCurrentParamsUnionInCompact < (m_pointNum[index] - 1) ))
        {
            double startParamsElement       =NAN;
            double incrementParamsElement   =NAN;
            /** ����� ������ ������ ���������!
            */
            rc = ptrIncrement->getParam(indexInParamsItem, incrementParamsElement);
            if(rc)
            {
                delete ptrTmpIParamsUnion;
                return nullptr;
            }
///BUG ��� ����� _currentParams[i] +=incrementParamsElement!
            ptrStartParams->getParam(indexInParamsItem, startParamsElement);
            double currentParamsElement   = NAN;
            currentParamsElement = startParamsElement + (coordsOfCurrentParamsUnionInCompact * incrementParamsElement);
            ptrTmpIParamsUnionItem->setParam(indexInParamsItem, currentParamsElement);
        }

        /** ������, ����� ������ �������� ��������� ������� �������� �� index-�� ������������ ���
        */
        if ((m_pointNum[index]) && (coordsOfCurrentParamsUnionInCompact == (m_pointNum[index] - 1)) )
        {
            double endParamsElement   =NAN;
            rc = ptrEndParams->getParam(indexInParamsItem, endParamsElement);
            if(rc)
            {
                delete ptrTmpIParamsUnion;
                return nullptr;
            }
            ptrTmpIParamsUnionItem->setParam(indexInParamsItem, endParamsElement);
        }

        /** ������, ����� ������ �������� ��������� ������� �������� �� index-�� ������������� ���
        */
        if (coordsOfCurrentParamsUnionInCompact == 0)
        {
            double startParamsElement   = NAN;
            rc = ptrStartParams->getParam(indexInParamsItem, startParamsElement);
            if(rc)
            {
                delete ptrTmpIParamsUnion;
                return nullptr;
            }
            ptrTmpIParamsUnionItem->setParam(indexInParamsItem, startParamsElement);
        }
        ++i;
    }///end loop over all params coords
#ifdef _DEBUG_//DEBUG
std::cout << std::endl;
for (i = 0; i < _paramsSpaceDimension; ++i)
  std::cout << coordsRelative[i] << ";";
#endif //END DEBUG
    return ptrTmpIParamsUnion;
}
/**
* ���������� ��� ������ ��������� ��� ������!
*/
IParamsUnion* IParamsUnionCompact_Impl::getIncrementsCopy() const
{
    return m_increment->clone();
}
bool IParamsUnionCompact_Impl::isEnd() const
{
    return m_end;
}

/****************************************************************************************
*                 Static factories implementation                                       *
****************************************************************************************/
/****************************************************************************************
* static  public interface to make IParamsCompact* by IParams* bounds, increment
****************************************************************************************/
IParamsCompact* IParamsCompact::createParamsCompact(const IModel* model,  IParams* leftBound, IParams* rightBound, IParams* increments, bool stepValue, Logger* logger)
{
    /** first validate params */
    if(false == IParamsCompact_Impl::staticAreValidCTORparams(model, leftBound, rightBound, increments, stepValue, logger))
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong params in IParamsCompact::createParamsCompact()!");
        return NULL;
    }
    /** second validate model type */
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::ADDITIVE_MODEL;
    model->getModelType(mt);
    if(mt == IModel::ModelsTypes::ADDITIVE_MODEL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "For AdditiveModel use IParamsUnionCompact::createParamsUnionCompact()!");
        return NULL;
    }
    /** finally create empty compact */
    IParamsCompact_Impl* compact = new IParamsCompact_Impl(const_cast<IModel*> (model));
    /** and fill/init the compact with params */
    bool rc = compact->initParamsCompact(model, leftBound, rightBound, increments, stepValue, logger);
    if(rc)
    {
        return (IParamsCompact*) compact;
    }
    else
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Can not fill Compact with leftBound, rightBound, increments in IParamsCompact::createParamsCompact()!");
        delete compact;
        return NULL;
    }
}
/***********************************************************************
* static  public interface to make IParamsUnionCompact* by IParamsUnion* bounds, increment
************************************************************************/
IParamsUnionCompact* IParamsUnionCompact::createParamsUnionCompact(const IAdditiveModel* additiveModel,  IParamsUnion* leftBound, IParamsUnion* rightBound, IParamsUnion* increments, bool stepValue, Logger* logger)
{
    /** first validate params */
    if(false == IParamsUnionCompact_Impl::staticAreValidCTORparams(additiveModel, leftBound, rightBound, increments, stepValue, logger))
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong params in IParamsUnionCompact::createParamsUnionCompact()!");
        return NULL;
    }
    /** second validate model type */
    enum AdditiveModelsTypes amt = FAKE_ADDITIVE_MODEL;
    additiveModel->getModelType(amt);
    if(amt == FAKE_ADDITIVE_MODEL)
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Wrong AdditiveModel in IParamsUnionCompact::createParamsUnionCompact()!");
        return NULL;
    }
    /** finally create empty compact */
    IParamsUnionCompact_Impl* compact = new IParamsUnionCompact_Impl(const_cast<IAdditiveModel*>(additiveModel));
    /** and fill/init the compact with params */
    bool rc = compact->initParamsUnionCompact(additiveModel, leftBound, rightBound, increments, stepValue, logger);
    if(rc)
    {
        return (IParamsUnionCompact*) compact;
    }
    else
    {
        logger->error(ErrorLevels::ALGORITHM_MODELDATA_ERROR, "Can not fill Compact with leftBound, rightBound, increments in IParamsUnionCompact::createParamsUnionCompact()!");
        delete compact;
        return NULL;
    }
}

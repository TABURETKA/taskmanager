#ifndef __PARAMS_H__633569422542968750
#define __PARAMS_H__633569422542968750
#include <iostream> /* cout */
#include <math.h> /* NAN */
#include <cstring> /* memcpy */
#include <string> /* std::string */
#include <map> /* std::map */

// DON'T pollute global scope! using namespace std;
/** forward declaration */
class Logger;
/** classes interfaces */

/**
���� ����� ����������� ��������� IParams,
� �� ��� ������ ����� ����������� ������ ������� ���������� ����������,
������� ����� ����������� ������ �������/�����������!
��������� ��� ������� ���������� � ���������� ������ ������������ �� ������ ������,
�������� I_PF_Model.
�� ��������� ���������� ������ ���������� ������� ��������� ��������
*/
class IParams
{
public:
/** enum must be populated to provide factory info,
 which Param type instance is required
*/
    enum ParamsTypes
    {
        Params1D,
        GaussianParams,
        WienerParams,
        PoissonParams,
        ParetoParams,
        PoissonDrvnParetoJumpsExpMeanRevertingParams,
        FAKE_PARAMS_TYPES
    };
/* OLD C++ < C++11
    static const char * paramsNames[IParams::ParamsTypes::FAKE_PARAMS];
*/
    static constexpr const char * paramsNames[FAKE_PARAMS_TYPES]=
    {
        "Params1D",
        "GaussianParams",
        "WienerParams",
        "PoissonParams",
        "ParetoParams",
        "PoissonDrvnParetoJumpsExpMeanRevertingParams"
    };

    static enum IParams::ParamsTypes getParamsTypeByParamsName(char const * const paramsName);

    /** GRAMMAR stuff for parseAndCreateParams(...)
    ��� ��������� �� ����������� ������ ���� �����
    ����� ���������� ��������������� �� ������!
    �� ����� ���� ������� ����� ���������
    � �����-�� ������ � ���������, � ������� ��� ��������� ������ �������
    */
    enum indexesOfGrammarForParams1D
    {
        indexOfDimension,
        FAKE_INDEX_GRAMMAR_PARAMS1D
    };
    /** tokens to parse config/map for Params1D
    and to produce string from toString()
    */
/*    static const char * grammar[FAKE_INDEX_GRAMMAR_PARAMS1D];
*/
    static constexpr const char * grammar[FAKE_INDEX_GRAMMAR_PARAMS1D]={
        "DIMENSION"
    };

    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    IParams() {};

    virtual ~IParams() {};

    /** Static Factories!
    ��� ������������ ������ ������� ����� ���������
    ������, ������������ ���� �� ����������� � ���� h-�����
    */
    /** enum IParams::ParamsTypes is in use to specify required Param type instance */
    static IParams* createEmptyParams(Logger* logger, enum IParams::ParamsTypes pt, size_t size = 0);
    /** enum IParams::ParamsTypes is in use to specify which parser is required to create Param type instance */
    static IParams* parseAndCreateParams(enum IParams::ParamsTypes pt, std::map<std::string, std::string>& mapParams, std::string postfix, Logger* logger);

    /** Any subclass may have own copy of clone
    DON'T forget to DELETE after all
    the IParams instance the cone() created
    */
    virtual IParams* clone() const =0;
    /** Every subclass MUST have own IMPLEMENTATION of toString()! */
    virtual std::string toString() const =0;

    virtual IParams::ParamsTypes getParamsType() const =0;

    virtual size_t getSize() const =0;
/// DOES NOT??? MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MUST define operator(i,...) to get access to params by index
    /** ��� ������ ����� ���������� ������
      * �� ���������� ���������� ���������� ���������� ������,
      * ������� ����� ������������� �� IParams � ��� �����!
      * ��� � ���������� ���������� IParams_Impl,
      * � �������� setParam/setParams � getParam/getParams,
      * � ����� ���������� ���������� ������������� �������
    */
    virtual int setParam(size_t index, double value)=0;
    virtual int setParams(size_t size, const double * params, size_t offsetInDst=0)=0;
    virtual int getParam(size_t index, double& param) const =0;
    virtual int getParams(size_t size, double * params, size_t offsetInSrc=0) const =0;

    /**********************************************************************
    * static math stuff for IParams
    ***********************************************************************/
    static int norm2(const IParams* point1, const IParams* point2, double& norm);
private:
    /** assign and copy CTORs are disabled */
    IParams(const IParams&);
    void operator=(const IParams&);
};

///TODO!!! class IParams2D
/***************************************************************************************
*                                 Gaussian Params                                      *
****************************************************************************************/
class IParamsGaussian : public virtual IParams
{
public:
    /// GaussianCDFParamsDimension<=2
/** static in IParamsGaussian interface */
    static const size_t GaussianCDFParamsDimension = 2;

    /** enum for grammarOfGaussianParams <=2
    */
    enum indexesOfGrammarForGaussianParams{
        MU=0,
        SIGMA=1,
        FAKE_INDEX_GRAMMAR_GAUSSIAN
    };
    /** tokens to parse config/map for GaussianParams
    and to produce string from toString()
    */
/* OLD C++ < C++11  static const char * grammar[FAKE_INDEX_GRAMMAR_GAUSSIAN];
*/
    static constexpr const char * grammar[FAKE_INDEX_GRAMMAR_GAUSSIAN] ={
        "MU",
        "SIGMA"
    };
    /** This public ctor DOES NOT MATTER - this is abstract class !
     To create Gaussian implementation factory is in use!
    */
    IParamsGaussian() {};
    virtual ~IParamsGaussian() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//  ???  virtual double operator()(size_t index);
    virtual IParams* clone() const =0;

    virtual std::string toString() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for IParamsGaussian model
    */
    /** ��� � ������������� ������� ��� ��� � ���������/
     ����� ���������� ������� setParam/setParams � getParam/getParams
     ���� ���������������� ����������� ����� ����������,
     ���� � ���������������� �� ������ ���������� ����� ����������
     ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    virtual int setMean(double meanNew)=0;
    virtual int setSDeviation(double sdNew)=0;
    virtual double mu() const=0;
    virtual double sigma() const=0;
private:
    /** public ctor exists BUT Factory is in use instead! */
    /** assign and copy CTORs are disabled */
    IParamsGaussian(const IParamsGaussian&);
    void operator=(const IParamsGaussian&);
};
/******************************************************
*                    Wiener Params                    *
*******************************************************/
class IParamsWiener : public virtual IParams
{
public:
    static const size_t WienerCDFParamsDimension = 3;

    /** enum for grammarOfWienerParams
    =>3
    */
    enum indexesOfGrammarForWienerParams{
        INITIAL_CONDITION=0,
        MU=1,
        SIGMA=2,
        FAKE_INDEX_GRAMMAR_WIENER
    };
    /** tokens to parse config/map for WienerParams
    and to produce string from toString()
    */
    static constexpr  const char * grammar[FAKE_INDEX_GRAMMAR_WIENER]={
        "INITIAL_CONDITION",
        "MU",
        "SIGMA"
    };
    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsWiener() {};
    virtual ~IParamsWiener() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    virtual std::string toString() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Wiener model */
    /** ��� � ������������� ������� ��� ��� � ���������/
    ����� ���������� ������� setParam/setParams � getParam/getParams
    ���� ���������������� ����������� ����� ����������,
    ���� � ���������������� �� ������ ���������� ����� ����������
    ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    /** ��� ��������� ������ ��������������� �.�. - ����� ��������� ��� � ���.�������
    �.�.WienerCDFParamsDimension = 3;
    */
    virtual int setInitialCondition(double initConditionNew) = 0;
    virtual int setMean(double meanNew)=0;
    virtual int setSDeviation(double sdNew)=0;
    virtual double initialCondition() const = 0;
    virtual double mu() const=0;
    virtual double sigma() const=0;
private:
    /** public ctor exists BUT Factory is in use instead! */
    /** assign and copy CTORs are disabled */
    IParamsWiener(const IParamsWiener&);
    void operator=(const IParamsWiener&);
};
/******************************************************
*                    Poisson Params                   *
*******************************************************/
class IParamsPoisson : public virtual IParams
{
public:

    static const size_t PoissonCDFParamsDimension = 1;
    /** enum of grammar Config Parser for Poison Params
    */
    enum indexesOfGrammarForPoissonParams{
        LAMBDA=0,
        FAKE_INDEX_GRAMMAR_POISSON
    };
    /** tokens to parse config/map for PoissonParams
    and to produce string from toString()
    */
    static constexpr const char * grammar[FAKE_INDEX_GRAMMAR_POISSON]={
        "LAMBDA"
    };
    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsPoisson() {};
    virtual ~IParamsPoisson() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense!    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Poisson model */
    /** ��� � ������������� ������� ��� ��� � ���������/
    ����� ���������� ������� setParam/setParams � getParam/getParams
    ���� ���������������� ����������� ����� ����������,
    ���� � ���������������� �� ������ ���������� ����� ����������
    ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    virtual int setPoissonLambda(double poissonLambdaNew)=0;
    virtual double lambda() const=0;

private:
    IParamsPoisson(const IParamsPoisson&);
    void operator=(const IParamsPoisson&);
};
/*****************************************************
*                    Pareto Params                    *
******************************************************/
class IParamsPareto : public virtual IParams
{
public:
    static const size_t ParetoCDFParamsDimension = 2;
    /** enum of grammar Config Parser for Pareto Params
    */
    enum indexesOfGrammarForParetoParams{
        X_MIN,
        K,
        K_MIN,
        K_MAX,
        X_MIN_THRESHOLD,
        FAKE_INDEX_GRAMMAR_PARETO
    };
    /** tokens to parse config/map for ParetoParams
    and to produce string from toString()
    */
    static constexpr const char * grammar[FAKE_INDEX_GRAMMAR_PARETO] = {
        "X_MIN",
        "K",
        "K_MIN",
        "K_MAX",
        "X_MIN_THRESHOLD"
    };

    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsPareto() {};
    virtual ~IParamsPareto() {};

    /** IParams stuff */
    using IParams::getSize;
    virtual std::string toString() const =0;
///DOES NOT MAKE sense!    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Pareto model */
    /** ��� � ������������� ������� ��� ��� � ���������/
    ����� ���������� ������� setParam/setParams � getParam/getParams
    ���� ���������������� ����������� ����� ����������,
    ���� � ���������������� �� ������ ���������� ����� ����������
    ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    virtual int setParetoXm(double xMNew)=0;
    virtual int setParetoK(double kNew)=0;
    virtual double xM() const=0;
    virtual double k() const=0;

private:
    IParamsPareto(const IParamsPareto&);
    void operator=(const IParamsPareto&);
};

/******************************************************
*                PoissonDrvnParetoJumpsExpMeanReverting Params                 *
******************************************************/
class IParamsPoissonDrvnParetoJumpsExpMeanReverting : public virtual IParams,
    public virtual IParamsPareto
{
public:

    static const size_t PoissonDrvnParetoJumpsExpMeanRevertingCDFParamsDimension = 4;
    /** enum of grammar Config Parser for PoissonDrvnParetoJumpsExpMeanRevertingParams
    */
    enum indexesOfGrammarForPoissonDrvnParetoJumpsExpMeanRevertingParams{
        LAMBDA,
        MR_LAMBDA,
        X_min,
        K_,
        FAKE_INDEX_GRAMMAR_POISSONDRVN_PARETOJUMPS_EXPMR
    };
    /** tokens to parse config/map for PoissonDrvnParetoJumpsExpMeanRevertingParams
    and to produce string from toString()
    */
    static constexpr const char * grammar[FAKE_INDEX_GRAMMAR_POISSONDRVN_PARETOJUMPS_EXPMR]={
        "LAM",
        "MR_LAMBDA",
        "X_MIN",
        "K"
    };
    /** This public ctor DOES NOT MATTER - this is abstract class !
     For implementation  Factory is in use instead! */
    IParamsPoissonDrvnParetoJumpsExpMeanReverting() {};
    virtual ~IParamsPoissonDrvnParetoJumpsExpMeanReverting() {};

    /** IParams stuff */
    using IParams::getSize;
///DOES NOT MAKE sense!    virtual double operator[] (unsigned index);
//    virtual double operator()(size_t index);
    virtual IParams* clone() const =0;
    using IParams::setParam;
    using IParams::setParams;
    using IParams::getParam;
    using IParams::getParams;

    /** Specific stuff - WRAPPERS for Poisson-Pareto model */
    /** ��� � ������������� ������� ��� ��� � ���������/
     ����� ���������� ������� setParam/setParams � getParam/getParams
     ���� ���������������� ����������� ����� ����������,
     ���� � ���������������� �� ������ ���������� ����� ����������
     ����� ������ �� ���������� IParams, �.�. IParams::setParam � �.�.
    */
    virtual int setPoissonLambda(double poissonLambdaNew)=0;
    virtual int setMRLambda(double MR_lambdaNew)=0;
    virtual int setParetoXm(double xMNew)=0;
    virtual int setParetoK(double kNew)=0;
    virtual double lambda() const=0;
    virtual double MR_lambda() const=0;
    virtual double xM() const=0;
    virtual double k() const=0;

private:
    IParamsPoissonDrvnParetoJumpsExpMeanReverting(const IParamsPoissonDrvnParetoJumpsExpMeanReverting&);
    void operator=(const IParamsPoissonDrvnParetoJumpsExpMeanReverting&);
};

/******************************************************
*                     Params Union/Array                    *
*******************************************************/
/** class- interface as container for arrays of params */
/** IParamsUnion DOESN'T INHERIT  : public virtual IParams !
����� ����������� �� ���������� IParams,
����� ����������� setParam/getParam,
setParams/getParams,
�������������� ���������� ���������
��������� � ���� ���������� ������,
 � �������, �� ����������� ������� ������
 ������������ ��� ����� �� ���������� �������
 ���������
*/
class IParamsUnion // array of IParams
{
public:
    /** Factories for ParamsUnion */
    /** here we create ParamsUnion of general purposes Params1D only! */
    /** Logger ����� ������� � ������ ����������,
    �� ����� ���������� � ��� ���� �������, �� ��� �� ������ ������������.
    ������� ����� ������ ��������� Logger* �������� NULL */
    static IParamsUnion* createParamsUnion(size_t totalOfItems, const size_t* sizesOfItems, Logger * logger);
    /** here we create ParamsUnion by cloning addends ! */
    static IParamsUnion* createParamsUnion(size_t totalOfItems, const IParams** items, Logger * logger);

    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class! */
    IParamsUnion() {};
    virtual ~IParamsUnion() {};
    /** Any subclass may have own copy algorithm */
    virtual IParamsUnion* clone() const =0;
    /** Every subclass MUST have own IMPLEMENTATION for toString()! */
    virtual std::string toString() const =0;

    /**
    ����� �� ���������� ��������
    "items - ���������� ������� ����������"
    */
    virtual size_t getTotalOfItemsInParamsUnion() const =0;

    virtual size_t getSize() const =0;
// DOES NOT MAKE sense    virtual const double& operator[] (unsigned int index);
///TODO!!! subclasses MIGHT define operator(i,...) to get access to params by index
    /** if index-th item exists
    beforehand setItem() deletes it!
    But this setItem() can not put into IParamsUnion
    the item with index > Total Of Items In IParamsUnion
     */
    virtual int setItem(size_t index, const IParams* itemParams) =0;
    /** if any items exist in the IParamsUnion
    beforehand setItems() deletes ALL the items in the IParamsUnion
    This setItems() can put into IParamsUnion
    any number of of items in the IParamsUnion
    i.e. setItems() can put into the IParamsUnion
    more then CURRENT Total Of Items In IParamsUnion
     */
    virtual int setItems(size_t size, const IParams** itemsParams) =0;
    /**
    * Get ptr IParams into IParamsUnion by it's index
    i.e. PARAM [IN] index -- the index of IParams* in this IParamsUnion
    */
    virtual IParams* ptrIParamsUnionToPtrIParams(size_t index) const =0;
    virtual int getItemCopy(size_t index, IParams*& itemParams) const =0;
    virtual int getItemsCopies(size_t size, IParams**& itemsParams) const =0;

    /**********************************************************************
    * static math stuff for IParamsUnion
    ***********************************************************************/
    /** ��������� �����, �.�. ������ �� ����� ��������� ���������
    */
    static int norm2(const IParamsUnion *point1, const IParamsUnion *point2, double& dist);

private:
    /** assign and copy CTORs are disabled */
    IParamsUnion(const IParamsUnion&);
    void operator=(const IParamsUnion&);
};

#endif

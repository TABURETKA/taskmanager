#include "../../CommonStuff/Defines.h" /* isNAN_double */
#include "../../Params/ParamsSet.h"//IParams
#include "../../Models/Model.h" //ModelTypes
#include "../../Log/errors.h"
#include "../../DataCode/DataModel/DataMetaInfo.h" //DataInfo
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../../Params/ParamsSet.h"//IParams
#include <limits> //infinity()
#include "LinearTransformationPointEstimator.h"

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace PointEstimators;

PointEstimators::LinearTransformationPointEstimator::LinearTransformationPointEstimator(Logger*& log) : FakePointEstimator(log)
{
  m_pet = PointEstimatorsTypes::LinearTransformationPointEstimator;
  m_model  = NULL;
  m_estimatorParams  = NULL;
  m_setOfParamsEstimates = NULL;

}

PointEstimators::LinearTransformationPointEstimator::~LinearTransformationPointEstimator()
{
    m_data.clear();
    delete m_setOfParamsEstimates;
}
    /** IEstimator stuff */
/** ����� ������, ����������� ��� ������� ���������! */
int PointEstimators::LinearTransformationPointEstimator::setModel(IModel* model)
{
    if(!model)
        return _ERROR_WRONG_ARGUMENT_;

    m_model = model;
    setParamsSet();
    return _RC_SUCCESS_;
}

int PointEstimators::LinearTransformationPointEstimator::setParamsSet()
{
    /**
    � ���� ���������� ����� ����������� ������ �������� ������ ������ � �������� [0,1]

    ������� ��� ����� ���������� �� ����� ������ ���������� ������,
    �.�. ��� ����� ���������� ������������ �������� ����������� PLAIN_MODEL
    (������ ����������������� ������)
    � � ��������� ��� ������� ������ ������ ��������� �� ��� �� ����� ������,
    �.�. ������� ��������� Params1D, ����������� � ���� ���,
    �� �����!
    */

    const IModel* pstModel = NULL;
    IParamsSet * pst = IParamsSet::createEmptyParamsSet(pstModel, m_logger);
    /** This way at least so far. */
    if (pst)
    {
        m_setOfParamsEstimates = pst;
    }
    return _RC_SUCCESS_;
}

int PointEstimators::LinearTransformationPointEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();

    const DataInfo* di = ptrToPtrDataInfo[0];
    if(di->m_dimension != 1)///  � ���� ��������� ���������� ���������� ������ ���������� �����
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}

int PointEstimators::LinearTransformationPointEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}
/** generate result DataInfo */
int PointEstimators::LinearTransformationPointEstimator::getEstimatesAsDataInfoArray(size_t & dimension, DataInfo** & di) const
{
    ///�� ������ ��������� ������ ���� ��� ��������� ���� ������������ � ��������� �������
    size_t set_dim = m_setOfParamsEstimates->getSetSize();
    if (!set_dim)
    {
        return _ERROR_WRONG_OUTPUT_DATA_;
    }

    dimension = 1; /// ���� ����� ������, ���������� �� ���������� ������ ���� DataInfo*
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }

    size_t di_0_dimension = set_dim;
    /** the size of _trackSizes (the ??? index of single-dimension sets) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if (!di_0_dataSetSizes)
    {
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetSizes for estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));

    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));


    IParams* ptrIParams = m_setOfParamsEstimates->getFirstPtr();
    size_t id = m_setOfParamsEstimates->getCurrentId();
    while(id < di_0_dimension)
    {
        size_t size = ptrIParams->getSize();
        if (!size)
        {
            return _ERROR_WRONG_OUTPUT_DATA_;
        }
        di_0_dataSetSizes[id] = size;

        data[id] = new double[di_0_dataSetSizes[id]];
        if (!data[id])
        {
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[id], 0, di_0_dataSetSizes[id]*sizeof(double));
        for(size_t j=0; j < di_0_dataSetSizes[id]; ++j)
        {
            double tmp=0;
            ptrIParams->getParam(j, tmp);
            data[id][j] = tmp;
        }
        ptrIParams = m_setOfParamsEstimates->getNextPtr();
        id = m_setOfParamsEstimates->getCurrentId();
    }
    /** Here we put ptr to data as double const ** const
    to the DataInfo CTOR
    */
    di[0] = new DataInfo(data);

    if (!di[0])
    {
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = pointEstimatorNames[PointEstimatorsTypes::LinearTransformationPointEstimator];

    /// the name of data set
    di[0]->m_dataSetName = LINEARTRANSFORMATION_POINTESTIMATOR_DATASETNAME;
    if (m_model)
    {
        di[0]->m_dataModelName = m_model->toString();
    } else
    {
        di[0]->m_dataModelName = IModel::modelNames[IModel::ModelsTypes::PlainModel];
    }

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate dataSetTrackNames for estimates");
        return _ERROR_NO_ROOM_;
    }

    di[0]->m_dataSetSizes = di_0_dataSetSizes;
    for(size_t j=0; j < di_0_dimension; ++j)
    {
        di[0]->m_dataSetTrackNames[j]=LINEARTRANSFORMATION_POINTESTIMATOR_TRACKNAME_0;
    }
    return _RC_SUCCESS_;
}
/** IPointEstimator stuff */
int PointEstimators::LinearTransformationPointEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parseAndCreateEstimatorParams is not implemented yet!");
}
int PointEstimators::LinearTransformationPointEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int PointEstimators::LinearTransformationPointEstimator::setEstimatorParams(IParams*& params)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int PointEstimators::LinearTransformationPointEstimator::estimatePoint(ptrInt threadInterruptSignal)
{
     size_t size = m_data.size();
     if (size <= 0)
     {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_OUTPUT_DATA_;
     }
     IParams * compressedValues= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, size);
     if (!compressedValues)
     {
         m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to keep Estimator's estimates");
         return _ERROR_NO_ROOM_;
     }

    double m = std::numeric_limits<double>::infinity();
    double M = -m;

    for(size_t i = 0; i < size; ++i)
    {
        if(m_data[i] < m)
            m = m_data[i];

        if(m_data[i] > M)
            M = m_data[i];
    }
     for(size_t i = 0; i< size; ++i)
     {
         compressedValues->setParam(i, (m_data[i] - m)/(M - m));
     }

     if(!m_setOfParamsEstimates)
     {
         this->setParamsSet();
     }

     if (m_setOfParamsEstimates->addParamsCopyToSet(NULL, compressedValues, m_logger) != true )
     {
         delete compressedValues;
         compressedValues = NULL;
         m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Can not put derivatives into Estimator's m_setOfParamsEstimates.");
         return _ERROR_WRONG_OUTPUT_DATA_;
     }
     /** ptr to allocated params is stored in ParamsSet,
     anyway delete ptrToParams
     */
     delete compressedValues;
     return _RC_SUCCESS_;;
}

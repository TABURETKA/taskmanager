#ifndef __PF_POINTESTIMATOR_H__633569422542968750
#define __PF_POINTESTIMATOR_H__633569422542968750
#include <vector>
#include <mutex>
#include "../../Estimator.h"
#include "../FakePointEstimator.h"
#include "../../AdditiveModelEstimator.h"

#define DEFAULT_NUMBER_OF_PARTICLES 1000
///BEGIN Maria's code
//OLD #define DEFAULT_NUMBER_OF_THREADS 2
//OLD #define DEFAULT_THRESHOLD_DISTANCE_VALUE 0.5
//OLD #define SIZE_OF_SET_OF_LOGLIKELIHOODS 10

/** forward declarations */
class IParams;
class IParamsUnion;
class IParamsUnionCompact;

/** forward declarations */
typedef struct tagPointEstimatesData tPointEstimatesData;
typedef struct tagTimeTraj tTimeTrajectory;
typedef struct tagArgsThread  tArgsThread;
///END Maria's code


/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace PointEstimators{
    /** class container */

/** implementation of basic methods
 declared in abstract Base class IPointEstimator
 The methods setTask, setLog, toString
 have to be inherited all PointEstimators
*/
class PF_PointEstimator : public virtual IPointEstimator,
                          public virtual IAdditiveModelEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakePointEstimator
*/
                                      public virtual FakePointEstimator
{
public:
    enum IndexesOfGrammarPF_PointEstimator{
        indexMode,
        indexNumberOfParticles,
        indexNumberOfThreads,
        indexThresholdDistanceValue,
        indexAlgorithmToMakeSubcompacts,
        indexSubcompactSize,
        FakeIndex_IndexesOfGrammarPF_PointEstimator
    };
    static const char* grammar[FakeIndex_IndexesOfGrammarPF_PointEstimator];

    enum IndexesOfAlgorithmsToMakeSubcompacts{
        RANDOM_ALGORITHM,
        DETERMINISTIC_ALGORITHM,
        FakeIndex_AlgorithmsToMakeSubcompacts
    };
    static const char* grammarAlgorithmsToMakeSubcompacts[FakeIndex_AlgorithmsToMakeSubcompacts];

    enum PF_PointEstimatorModes{
        INDEX_ST,
        INDEX_MT,
        FakeIndex_PF_PointEstimatorMode
    };

    PF_PointEstimator(Logger*& log);
    virtual ~PF_PointEstimator();
    /** IEstimator stuff */
    using FakePointEstimator::setTask;
    using FakePointEstimator::setLog;

    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakePointEstimator::toString;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IPointEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimatePoint(ptrInt threadInterruptSignal);

    /** IAdditiveModelEstimator stuff */
    virtual int setModel(IAdditiveModel* model);
protected:
    using   FakePointEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
    enum STATISTICS_ESTIMATE_MODE
    {
        REPLACE_MODE,
        AVERAGE_MODE
    };
    bool passStatistics(unsigned t, double logLikelihood, std::vector<double>& statistics);
    void evaluateNewStatistics(enum STATISTICS_ESTIMATE_MODE mode, unsigned int& counterBestStatisticsFound, std::vector<double>& statistics, std::vector<double>& tmp_statistics);

    int getPF_EstimatorMode(enum PF_PointEstimatorModes& mode) const;
    int getNumberOfParticlesFromEstimatorParams(size_t& numberOfParticles) const;
///BEGIN Maria's code
    int getAlgorithmToMakeSubcompactFromEstimatorParams(short& algorithmToMakeSubcompacts) const;

    int getSubcompactsSizeFromEstimatorParams(unsigned short& subcompactsSize) const;

    int getNumberOfThreadsFromEstimatorParams(size_t& numberOfThreads) const;

    int getThresholdDistanceValueFromEstimatorParams(double& thresholdDistanceValue) const;

    int cloneSubCompact(const IParamsUnionCompact * const orgCompact, short relSizeOfClonedCompact, IParamsUnion *startPoint, IParamsUnionCompact **compactCloned) const;

    bool isRighBoundaryCorrect(const IParamsUnionCompact * const paramsCompact, const IParamsUnion * const leftBoundary, short relSizeOfClonedCompact) const;

    //Begin Maria's code 20.10
    int makeDeterministicStartPoints(IParamsUnionCompact const * const paramsCompact, unsigned short countStartPoints, std::vector<IParamsUnion*>& startPoints);
    //End Maria's code 20.10
    int makeRandomStartPoints(IParamsUnionCompact const * const paramsCompact, unsigned short countStartPoints, std::vector<IParamsUnion*>& startPoints);
    int makeStartPoints(IParamsUnionCompact const * const paramsCompact, unsigned short countStartPoints, std::vector<IParamsUnion*>& startPoints);
//OLD    int findCompactsSize(IParamsUnionCompact *paramsCompact, std::vector<IParamsUnion*> startPoints, size_t cntThreads, size_t *compactsSize);
    void estimateLogLikelihoodOfParamsUnionCompact(tArgsThread& threadMethodArgs);
///END Maria's code
    /** ORIGINAL IMPLEMENTATION Single Thread PointEstimators::PF_PointEstimator::estimatePoint */
    int estimatePoint_ST(ptrInt threadInterruptSignal);
    /** IMPLEMENTATION of Multi Threads (STL threads) PointEstimators::PF_PointEstimator::estimatePoint */
    int estimatePoint_MT(ptrInt threadInterruptSignal);

    virtual int setParamsUnionSet();
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    PF_PointEstimator(const PF_PointEstimator&);
    void operator=(const PF_PointEstimator&);
/** enum PointEstimatorsTypes m_pet; has been Moved to FakePointEstimator */
/** Task* m_task; has been Moved to FakePintEstimator */
/** Logger* m_logger; has been Moved to FakePointEstimator */
    IModel* m_model;
    IAdditiveModel* m_additiveModel;
    /** here we keep additive model's params compact
    */
    IParamsUnionCompact* m_puc;
    /** we put here the estimates, which are produced by estimator */
    IParamsUnionSet * m_setOfParamsUnionEstimates;
    /** we put here the estimates' loglikelihood, which are produced by estimator */
    IParamsSet * m_setOfLogLikelihoods;
    /** Estimator's Params */
    IParams* m_estimatorParams;
///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;

///BEGIN Maria's code
//OLD pthread_mutex mutexReduction;
//OLD std::mutex mutexReduction;
    std::vector<std::vector<tPointEstimatesData>> m_compactEstimatesData;
    std::vector<tTimeTrajectory> m_timeTrajectory;
    std::vector<double> m_bestLogLikelihood;
///END Maria's code
};

} /// end namespace PointEstimators
#endif

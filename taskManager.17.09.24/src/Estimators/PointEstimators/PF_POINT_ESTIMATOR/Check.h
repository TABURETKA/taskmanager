#ifndef __CHECK_H__1234
#define __CHECK_H__1234
#include "../../../Log/Log.h"
#include "../../../Models/Model.h"
#include "../../../Models/AdditiveModel.h"
#include "../../../Params/Params.h"
#include <time.h>

#include <sstream>
///TODO!!! ���������� �� ����� �������! ����������� �������� �������� �� IParamsUnion!
#define COUNT_PARAM 7

#define DEFAULT_DATE_TIME_FORMAT "%Y-%m-%d %H:%M:%S"
class Check
{
    public:
///TODO!!! ���������� �� param! �������� �� const IParamsUnion* const!
        Check(size_t sizeOfData, size_t sizeOfParams, std::string fileName, const IParamsUnion* const etalon_param);
        virtual ~Check();
        bool writeEstimatesVals(IParamsUnion* PF_param, size_t arg, double logLikelihood);
        bool writeEstInfo(bool isRejected, double pointRejectedTrack, size_t id, int index);
        bool checkFutility (size_t t, size_t size, double logLikelihood,double bestLoglikelihood,
                            IParamsUnion* PF_param, I_PF_AdditiveModel * model,
                            int indexOfParams,std::vector<double> particles,std::vector<double> m_data, double& log);
protected:
private:

        std::string getDate();
        bool writeEstimates ();

        bool writeModelParamVal(const IParamsUnion* const etalon_params);

        bool writeSeries(size_t size);
        bool writeSeriesModel();

        bool setLogFile (std::string fileName);

        std::ofstream _logFile;
        size_t _sizeOfParams;
        time_t _time_est;
        time_t _time_series;
        time_t _tmStartTime, _tmStopTime;
};
#endif // CHECK_H

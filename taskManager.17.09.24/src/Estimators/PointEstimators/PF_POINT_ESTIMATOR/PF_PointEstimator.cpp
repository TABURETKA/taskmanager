#define _BOUNDARIES_TEST_ /** ���������� ��� ����,
�������� ����� ������ ����� ����������, ����� ������ ������ �������,
�� �� ����������, ��������� �������� ������, ���� �� �������.
���� ����� ����� ��� ����,
����� ����������� �������� �������������� ����������� ������������ ������
*/
// #include <windows.h> /* Sleep() to stop estimator's thread while boundaries testing */
#include <sstream> /* stringstream */
#include <float.h> //DBL_MIN, DBL_MAX
#include <thread>
#include "../../../CommonStuff/Defines.h" /* DELIMITERS */
#include "../../../Log/Log.h"
#include "../../../Log/errors.h"
#include "PF_PointEstimator.h"
#include "../../../Params/ParamsSet.h"//IParamsUnionSet
#include "../../../Params/ParamsCompact.h"//IParamsCompact
#include "../../../Models/AdditiveModel.h" //AdditiveModel
#include "../../../Config/ConfigParser.h" //iteratorParamsMap
#include "../../../DataCode/Grammar/DataManagerGrammar.h" //DataManagerGrammar
#include "../../../Task/Task.h"
#include "../../../DataCode/DataModel/DataMetaInfo.h" /* DataInfo */
#include "../../../Distributions/Distributions.h" // to generate "startPoints", "id"
#include "../../../CommonStuff/PerformanceTiming/PerformanceTiming.h" /* PerformanceTiming */
#include "check.h"
#include <fstream> //ifstream

#include <assert.h>     /** assert */

/** "���� � ������ ������" */
#define DEFAULT_MIN_SIZE_OF_DATA_TO_BE_PROCESSED 7


///BEGIN Maria's code
#define DEFAULT_SUBCOMPACTS_SIZE         3
#define DEFAULT_NUMBER_OF_THREADS        2
#define DEFAULT_THRESHOLD_DISTANCE_VALUE 0.5
#define DEFAULT_THRESHOLD_ATTEMPTS_TO_MAKE_START_POINTS  OUR_MAX_UNSIGNED_INT
#define SIZE_OF_SET_OF_LOGLIKELIHOODS    10

#define THREAD_LOCAL_LOG_FILE_NAME_PREFIX "estimate"
#define THREAD_LOCAL_LOG_FILE_NAME_EXT    ".log"
/**
* container to keep estimate and its metadata
*/
typedef struct tagPointEstimatesData
{
  tagPointEstimatesData(){
      isNeedToConsider = true;
  }
   ~tagPointEstimatesData(){}

  tagPointEstimatesData(size_t valId, double valLogLikelihood, bool valIsNeedToConsider){
      //std::cout << "here\n";
      id = valId;
      logLikelihood = valLogLikelihood;
      isNeedToConsider = valIsNeedToConsider;
  }

  /** id ���� ����� � ����� �������� */
  size_t id;
  /** ������ ��������� ������������� � ���� ����� �������� */
  double logLikelihood;
  /** ����, ������� ����������, ���� �� ��������� ������, ���������� ��� ������ ����� ��������,
  ��� ������������ ��������������� ���������� � �������� � ����� ������.
  �� �� ������� ���������� ����������� ��������� ����������.
  */
  bool isNeedToConsider;
///TODO!!! ����� ����������! � �����? ����� ������ ���� �� ���� �� ������� statistics ��� std::vector<double>, � ����� ������� resize(dataSize)?
//OLD std::vector<double> tmp_statistics; // ��������� ��� ������ �� ����� ������� �� ������ ����������
//OLD std::vector<double> statistics; // ��������� ��� ��������� ������� ����������� ������ ��������� ������������� �� ���������� ������� ���� ����������
} tPointEstimatesData;

/**
* container to keep timing estimate and its metadata
*/
typedef struct tagTimeTraj
{
    /** ������� ����� ��������� ���������� ��� ���������� ����� ��������,
    */
    float averageTime;
    /** ���������� ������� ����� cnt
    */
    size_t cnt;
} tTimeTrajectory;

/** ���������, ��� ������ ������� � ��������� �������
*/
typedef struct tagArgsThread
{
    tagArgsThread(){
        paramsCompact = nullptr;
    }
    ~tagArgsThread(){
        if (paramsCompact){
            delete paramsCompact;
            paramsCompact = nullptr;
        }
    }
    unsigned threadIndex;
    /** ��� �������� �� ������
    */
    int rc;
    /**��������� ��� ������ �� ����� ������� �� ������ ����������
    */
    std::vector<double> tmp_statistics;
    /** ��������� ��� ��������� ������� ����������� ������ ��������� ������������� �� ���������� ������� ���� ����������
    */
    std::vector<double> statistics;
    void setPUC(IParamsUnionCompact * ptrPUC){
        if(paramsCompact)
            delete paramsCompact;
        paramsCompact = ptrPUC;
    }
    IParamsUnionCompact * getPtrPUC(){
        return paramsCompact;
    }
    private:
    IParamsUnionCompact *paramsCompact;
} tArgsThread;
///END Maria's code

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */

using namespace PointEstimators;
///TODO!!! why here???
std::mutex mutexReduction; /** to manage access to threads into class-scope containers */

///static in PF_PointEstimator
const char* PF_PointEstimator::grammar[FakeIndex_IndexesOfGrammarPF_PointEstimator]=
{
    "PF_EstimatorMode",
    "NumberOfParticles",
    "NumberOfThreads",
    "ThresholdDistanceValue",
    "AlgorithmToMakeSubcompacts",
    "SubcompactSize"
};
const char* PF_PointEstimator::grammarAlgorithmsToMakeSubcompacts[FakeIndex_AlgorithmsToMakeSubcompacts]=
{
    "RANDOM",
    "DETERMINISTIC"
};

/**************************************************************************************************************************
* Get info on estimator mode
* PARAM [OUT] mode -- estimator mode
* returns _RC_SUCCESS_ if success
 or _ERROR_NO_ESTIMATOR_PARAMS_
**************************************************************************************************************************/
int PF_PointEstimator::getPF_EstimatorMode(enum PF_PointEstimatorModes& mode) const
{
    std::stringstream sstr;
    if(!m_estimatorParams )
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's params NOT set!");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    double tmpDbl = NAN;
    m_estimatorParams->getParam(indexMode, tmpDbl);
    if(tmpDbl < -DBL_MIN)
    {
        sstr.clear(); sstr.str(std::string());
        sstr << "Wrong PF_Estimator Mode : " << (int)tmpDbl;
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + sstr.str());
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    mode = static_cast<enum PF_PointEstimatorModes>((size_t) tmpDbl);
    return _RC_SUCCESS_;
}

///BEGIN Maria's code
int PF_PointEstimator::getAlgorithmToMakeSubcompactFromEstimatorParams(short& algorithmToMakeSubcompacts) const
{
    std::stringstream sstr;
    if(!m_estimatorParams )
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's params NOT set!");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    double tmpDbl = NAN;
    m_estimatorParams->getParam(indexAlgorithmToMakeSubcompacts, tmpDbl);
    if ((tmpDbl < -DBL_MIN) || (static_cast<short>(tmpDbl) >= FakeIndex_AlgorithmsToMakeSubcompacts))
    {
        sstr.clear(); sstr.str(std::string());
        sstr << "Wrong subcompacts' algorithm : " << static_cast<short>(tmpDbl);
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + sstr.str());
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    algorithmToMakeSubcompacts = static_cast<short>(tmpDbl);
    return _RC_SUCCESS_;
}///end getAlgorithmToMakeSubcompactFromEstimatorParams

int PF_PointEstimator::getSubcompactsSizeFromEstimatorParams(unsigned short& subcompactsSize) const
{
    std::stringstream sstr;
    if(!m_estimatorParams )
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's params NOT set!");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    double tmpDbl = NAN;
    m_estimatorParams->getParam(indexSubcompactSize, tmpDbl);
    if ((tmpDbl < DBL_MIN) || (tmpDbl > USHRT_MAX))
    {
        sstr.clear(); sstr.str(std::string());
        sstr << "Wrong subcompacts' size : " << static_cast<short>(tmpDbl);
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + sstr.str());
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    subcompactsSize = static_cast<unsigned short>(tmpDbl);
    return _RC_SUCCESS_;
}///end getSubcompactSizeFromEstimatorParams

int PF_PointEstimator::getThresholdDistanceValueFromEstimatorParams(double& thresholdDistanceValue) const
{
    std::stringstream sstr;
    if(!m_estimatorParams )
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's params NOT set!");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    double tmpDbl = NAN;
    m_estimatorParams->getParam(indexThresholdDistanceValue, tmpDbl);
    if(tmpDbl < DBL_MIN)
    {
        sstr.clear(); sstr.str(std::string());
        sstr << "Wrong ThresholdDistanceValue : " << tmpDbl;
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + sstr.str());
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    thresholdDistanceValue = tmpDbl;
    return _RC_SUCCESS_;
}

int PF_PointEstimator::getNumberOfThreadsFromEstimatorParams(size_t& numberOfThreads) const
{
    std::stringstream sstr;
    if(!m_estimatorParams )
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's params NOT set!");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    double tmpDbl = NAN;
    m_estimatorParams->getParam(indexNumberOfThreads, tmpDbl);
    if(tmpDbl < DBL_MIN)
    {
        sstr.clear(); sstr.str(std::string());
        sstr << "Wrong Number of threads : " << static_cast<int>(tmpDbl);
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + sstr.str());
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    numberOfThreads = static_cast<size_t>(tmpDbl);
    return _RC_SUCCESS_;
}

bool PF_PointEstimator::isRighBoundaryCorrect(const IParamsUnionCompact * const paramsCompact, const IParamsUnion * const leftBoundary, short relSizeOfClonedCompact) const
{
//DEBUG
// std::cout << "left_boundary:" << leftBoundary->toString() << std::endl;
//END DEBUG
    IParamsUnion * rightBounds = leftBoundary->clone();
    IParamsUnion* incrementsOriginal = paramsCompact->getIncrementsCopy();
// �.�. compactSize == 2!!!
    for(size_t i = 0; i < rightBounds->getTotalOfItemsInParamsUnion(); ++i)
    {
        double value;
        size_t size = rightBounds->ptrIParamsUnionToPtrIParams(i)->getSize();
        for(size_t j = 0; j < size; ++j)
        {
            int rc = rightBounds->ptrIParamsUnionToPtrIParams(i)->getParam(j, value);
            if(rc)
            {
                delete rightBounds;
                delete incrementsOriginal;
                return false;
            }
            double inc = 0.0f;
            rc = incrementsOriginal->ptrIParamsUnionToPtrIParams(i)->getParam(j, inc);
            if(rc)
            {
                delete rightBounds;
                delete incrementsOriginal;
                return false;
            }
            rc = rightBounds->ptrIParamsUnionToPtrIParams(i)->setParam(j, value + inc*relSizeOfClonedCompact);
            if(rc)
            {
                delete rightBounds;
                delete incrementsOriginal;
                return false;
            }
        }
    }
//DEBUG
// std::cout << "right_boundary:" << rightBounds->toString() << std::endl;
//END DEBUG
    bool rc = false;
    if(paramsCompact->isPointInCompact(rightBounds))
    {
        rc = true;
    }
    delete rightBounds;
    delete incrementsOriginal;
    return rc;
}

// Begin Maria's code 20.10
int PF_PointEstimator::makeDeterministicStartPoints(IParamsUnionCompact const * const paramsCompact, unsigned short countStartPoints, std::vector<IParamsUnion*>& startPoints)
{
    if ((!paramsCompact) || (!countStartPoints))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    unsigned short relSizeOfClonedCompact = 0;
    int rc = getSubcompactsSizeFromEstimatorParams(relSizeOfClonedCompact);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
    IParamsUnion* increments = paramsCompact->getIncrementsCopy();
    IParamsUnion* lastPoint = paramsCompact->getParamsUnionCopy(paramsCompact->getSize()-1);
    /** �������������� ��������� ����� ������������ ����� ������ ����� ��������
    */
    for (size_t i = 0; i < countStartPoints; ++i)
    {
        /** left-bottom corner (id=0) is in use to init startPoint
         for the first time
        */
        startPoints.at(i) = paramsCompact->getParamsUnionCopy(0);//BUG paramsCompact->getFirstPtr()->clone();
    }

    size_t relativeCoord;
    double inc, value, startPointCoordShift;
    size_t totalOfItemsInParamsUnion = startPoints[0]->getTotalOfItemsInParamsUnion();
    for(size_t i = 0; i < totalOfItemsInParamsUnion; ++i)
    {
        for(size_t j = 0, size = startPoints[0]->ptrIParamsUnionToPtrIParams(i)->getSize(); j < size; ++j)
        {
            relativeCoord = 0;
            //��������� ������ �������� ����� ����� ���
            rc = paramsCompact->evaluateRelativeCoordAlongGivenAxis(j+i*size, lastPoint, relativeCoord);
            if(rc)
            {
                for (size_t i = 0; i < countStartPoints; ++i)
                {
                    delete  startPoints[i];
                }
                delete increments;
                delete lastPoint;
                return rc;
            }
            if (relativeCoord >= relSizeOfClonedCompact*countStartPoints)
                startPointCoordShift = ceil(relativeCoord/countStartPoints);
            else
                startPointCoordShift = floor(relativeCoord/countStartPoints);
            rc = increments->ptrIParamsUnionToPtrIParams(i)->getParam(j, inc);
            if(rc)
            {
                for (size_t i = 0; i < countStartPoints; ++i)
                {
                    delete  startPoints[i];
                }
                delete increments;
                delete lastPoint;
                return rc;
            }

            //����� ������� ���������� � ���� ��������� �����
            for (size_t iStartPoint = 0; iStartPoint < countStartPoints; ++iStartPoint)
            {
                rc = startPoints.at(iStartPoint)->ptrIParamsUnionToPtrIParams(i)->getParam(j, value);
                if(rc)
                {
                    for (size_t i = 0; i < countStartPoints; ++i)
                    {
                        delete  startPoints[i];
                    }
                    delete increments;
                    delete lastPoint;
                    return rc;
                }
                rc = startPoints.at(iStartPoint)->ptrIParamsUnionToPtrIParams(i)->setParam(j, value + iStartPoint*startPointCoordShift*inc);
                if(rc)
                {
                    for (size_t i = 0; i < countStartPoints; ++i)
                    {
                        delete  startPoints[i];
                    }
                    delete increments;
                    delete lastPoint;
                    return rc;
                }
            }
        }
    }
    delete increments;
    delete lastPoint;
    return _RC_SUCCESS_;
}///end makeDeterministicStartPoints
// End Maria's code 20.10

int PF_PointEstimator::makeRandomStartPoints(IParamsUnionCompact const * const paramsCompact, unsigned short countStartPoints, std::vector<IParamsUnion*>& startPoints)
{
    /** ����������� ��������� ���������� ����� ���������� ������� ������������
    * (�������� ����������������� �����) � ���������� ��������
    */
    double minDistance = 0;
    int rc = getThresholdDistanceValueFromEstimatorParams(minDistance);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
    /** ������������� ������ ������� ����������� (��������� ����� ���������� "����� ������ ����" ���� ������������)
    * (�������� ����������������� �����) � ������������� ��������
    */
    unsigned short relSizeOfClonedCompact = 0;
    rc = getSubcompactsSizeFromEstimatorParams(relSizeOfClonedCompact);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
///TODO!!! �������� ������������������ ���� minDistance, relSizeOfClonedCompact �� ��� ��� �� �����������?
    size_t counterOfAttempts = 0;
    size_t currId;
    int i = 0;
    IParamsUnion *currPoint = nullptr;
    /**
    init very first start point
    */
    do{
        currId = (size_t)(Distributions::Uniform()*(paramsCompact->getSize() - 1) + 1);
		delete currPoint;
        currPoint = paramsCompact->getParamsUnionCopy(currId);
        counterOfAttempts++;
    }while ((!isRighBoundaryCorrect(paramsCompact, currPoint, relSizeOfClonedCompact))
             &&
             (counterOfAttempts < DEFAULT_THRESHOLD_ATTEMPTS_TO_MAKE_START_POINTS));
    if(counterOfAttempts == DEFAULT_THRESHOLD_ATTEMPTS_TO_MAKE_START_POINTS){
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + "Wrong params to generate start points for multi-threads mode!");
        return _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }
    startPoints.at(i) = (currPoint);
    i++;
    counterOfAttempts = 0;
    while( i < countStartPoints )
    {
        int j = 0;
        bool isValidAllDistancesBetweenStartPoints = false;
///TODO!!! �� �� ����� ����� ���� ����� � �� ��������� �������?
        while ((!isValidAllDistancesBetweenStartPoints) && (counterOfAttempts < DEFAULT_THRESHOLD_ATTEMPTS_TO_MAKE_START_POINTS))
        {
            counterOfAttempts++;
            currId = (size_t)(Distributions::Uniform()*(paramsCompact->getSize() - 1) + 1);
			///DON't DELETE delete currPoint;!~
            currPoint = paramsCompact->getParamsUnionCopy(currId);
/** /DEBUG
while(1){
        currId = (int)(Distributions::Uniform()*(paramsCompact->getSize() - 1) + 1);
        currPoint = paramsCompact->getParamsUnionCopy(currId);
        size_t tmpID = 0;

int rc = paramsCompact->getId_byIParamsUnion(currPoint, tmpID);
std::cout << std::endl << currId << " => Point:" << "=>" << tmpID << std::endl;

       IParamsUnionCompact * pc = const_cast<IParamsUnionCompact *> (paramsCompact);
       tmpID = currId-1;
       pc->setCurrentId(tmpID);
std::cout << pc->getCurrentId() << " changed-Point:" << pc->getNextPtr()->toString() << std::endl;
}
*/ //END DEBUG
            /** � ����� ��������� ���������� ����� ����������
            * ���������� ������� � ����� i-�� ������
            */
            for(j = 0; j < i; ++j)
            {
                    double distance = 0.0f;
                    if( _RC_SUCCESS_ != IParamsUnion::norm2( const_cast<const IParamsUnion*> (startPoints.at(j)), const_cast<const IParamsUnion*> (currPoint), distance))
                    {
                        delete currPoint;
                        /** ��������� ������ � ��� ���������������������� �������
                        */
                        startPoints.clear();
                        return _ERROR_WRONG_MODEL_PARAMS_;
                    }
                    if (distance < minDistance){
                        isValidAllDistancesBetweenStartPoints = false;
                        delete currPoint;
                        break;
                    } else {
                        isValidAllDistancesBetweenStartPoints = true;
                    }
            }
            /** OLD ���� �����-�� ���������� �� ��� ��������������������� ����� �� i-��
            * ��������� ������ ����������,
            * �������� ����� i-�� �����
            /
            if (!isValidAllDistancesBetweenStartPoints /OLD j != i /)
            {
                delete currPoint;
                currId = (size_t)(Distributions::Uniform()*(paramsCompact->getSize() - 1) + 1);
                currPoint = paramsCompact->getParamsUnionCopy(currId);
            }
            */
        }///end while (!isValidAllDistancesBetweenStartPoints)

        if(counterOfAttempts == DEFAULT_THRESHOLD_ATTEMPTS_TO_MAKE_START_POINTS){
            for(size_t i = 0, size = startPoints.size(); i < size; ++i){
                delete startPoints[i];
                startPoints[i] = nullptr;
            }
            startPoints.clear();
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + "Wrong params to generate start points for multi-threads mode!");
            return _ERROR_WRONG_ESTIMATOR_PARAMS_;
        }
        counterOfAttempts = 0;
//DEBUG BEGIN
// std::cout << currPoint->toString() << std::endl;
//DEBUG END
        /** ����� i-�� ����� ������ ��������
        * "���������� �� ��� �� ����� ����������� ��������� ����� ������ ����������"
        *  ������ ����� ������ ��� ���� ��������
        "������ ������� �����������, ������� �������� � ������� ����� i-�� �����, �������� � �������� �����������"
        */
        if(isRighBoundaryCorrect(paramsCompact, currPoint, relSizeOfClonedCompact))
        {
            startPoints.at(i) = (currPoint);
            i++;
			currPoint = nullptr;
        }
///BUG! was here
        else{
            delete currPoint;
        }
    }///end loop over all start points
    return _RC_SUCCESS_;
}///end makeRandomStartPoints

/**************************************************************************************************************************
* make startPoints to clone/create SubCompact
* PARAM [IN] ptrOrgCompact -- �������� ����������,
* PARAM [IN] countStartPoints -- ���������� ��������� ����� (==���������� ������� ������������)
* PARAM [OUT] ptrStartPoints -- ��������� �����, ������-���� ������� ������� ����������� � ��� �� �����
* returns _RC_SUCCESS_ if success
 or _ERROR_NO_ESTIMATOR_PARAMS_
**************************************************************************************************************************/
int PF_PointEstimator::makeStartPoints(IParamsUnionCompact const * const ptrOrgCompact, unsigned short countStartPoints, std::vector<IParamsUnion*>& ptrStartPoints)
{
    short algorithmToMakeSubcompacts = -1;
    int rc = getAlgorithmToMakeSubcompactFromEstimatorParams(algorithmToMakeSubcompacts);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
    switch(static_cast<IndexesOfAlgorithmsToMakeSubcompacts> (algorithmToMakeSubcompacts) )
    {
    case RANDOM_ALGORITHM:
        rc = makeRandomStartPoints(ptrOrgCompact, countStartPoints, ptrStartPoints);
        break;
    case DETERMINISTIC_ALGORITHM:
        rc = makeDeterministicStartPoints(ptrOrgCompact, countStartPoints, ptrStartPoints);
        break;
    default:
        rc = _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }
    return rc;
}

/**************************************************************************************************************************
* clone/create SubCompact
* PARAM [IN] ptrOrgCompact -- �������� ����������,
* PARAM [IN] relSizeOfClonedCompact -- ������������� �������� ������ ����������� ��������,
* PARAM [IN] ptrStartPoint -- ��������� �����, ������-���� ������� ������� ����� ����������
* PARAM [OUT] ptrToPtrCompactCloned -- ������������� ����������
* returns _RC_SUCCESS_ if success
 or _ERROR_NO_ESTIMATOR_PARAMS_
**************************************************************************************************************************/
///TODO!!! ���������� � ����� ����??? ���������??? � IParamsUnionCompact
int PF_PointEstimator::cloneSubCompact(const IParamsUnionCompact * const ptrOrgCompact, short relSizeOfClonedCompact,
                IParamsUnion *ptrStartPoint, IParamsUnionCompact **ptrToPtrCompactCloned) const
{
    // ������ ����� ������� ��� ������ createParamsUnionCompact
    // ��� ����� ������� ����� ������������ IParamsUnion ��� ����� ��������� ��������, ������ � �����������
    // ����� �������� ����� ��������� ��������, ���� ������� �� ��������� ��������� ����� ������ ��������.
    // ����� �������� ������ - ��������������, ��������� � ����������� ��������� ����� ������.
    // ���������� ������ ���� ��� � ��������� ��������
    //size_t dim = ptrOrgCompact->getDimOfParamsUnion();
    IParamsUnion * leftBounds = ptrStartPoint;
    IParamsUnion * rightBounds = ptrStartPoint->clone();
    IParamsUnion* incrementsOriginal = ptrOrgCompact->getIncrementsCopy();
    for(size_t i = 0; i < rightBounds->getTotalOfItemsInParamsUnion(); ++i)
    {
        double value;
        for(size_t j = 0, size = rightBounds->ptrIParamsUnionToPtrIParams(i)->getSize(); j < size; ++j)
        {
            int rc = rightBounds->ptrIParamsUnionToPtrIParams(i)->getParam(j, value);
            if(rc)
            {
                delete rightBounds;
                delete incrementsOriginal;
                return rc;
            }
            double inc;
            rc = incrementsOriginal->ptrIParamsUnionToPtrIParams(i)->getParam(j, inc);
            if(rc)
            {
                delete rightBounds;
                delete incrementsOriginal;
                return rc;
            }
            rc = rightBounds->ptrIParamsUnionToPtrIParams(i)->setParam(j, value + inc*relSizeOfClonedCompact);
            if(rc)
            {
                delete rightBounds;
                delete incrementsOriginal;
                return rc;
            }
        }
    }
    if(ptrOrgCompact->isPointInCompact(rightBounds))
    {
        *ptrToPtrCompactCloned = IParamsUnionCompact::createParamsUnionCompact( (const IAdditiveModel*) m_additiveModel, leftBounds, rightBounds, incrementsOriginal, 1, m_logger);
    }
//DEBUG BEGIN
// std::cout << leftBounds->toString() << std::endl;
// std::cout << rightBounds->toString() << std::endl;
//DEBUG END
    delete rightBounds;
    delete incrementsOriginal;
    if(*ptrToPtrCompactCloned)
    {
        return _RC_SUCCESS_;
    }
    else
        return _ERROR_WRONG_ARGUMENT_;
}///end cloneCompact

/*********************************************************
* Entry point for each thread we create in estimatePoint_MT()
* PARAMS [IN|OUT] threadMethodArgs -- container to exchange
 data between calling estimate_MT and thread's method estimateLogLikelihoodOfParamsUnionCompact
**********************************************************/
void PF_PointEstimator :: estimateLogLikelihoodOfParamsUnionCompact(tArgsThread& threadMethodArgs)
{
    IParamsUnionCompact* ptrPUC = threadMethodArgs.getPtrPUC();
//DEBUG BEGIN
//std::stringstream sstr;
//sstr << threadMethodArgs.threadIndex << ":";
//sstr << ptrPUC << " --> ";
//sstr << ptrPUC->getSize() << ":" << std::endl;
//std::cout << sstr.str();
//sstr.clear(); sstr.str("");
//DEBUG END
    if(nullptr == ptrPUC)
    {
        /** ����� ���� ��������� threadMethodArgs.rc
        ������ ��� ������ � ���������� ��� (�� ������� ����) � �����
        */
        threadMethodArgs.rc = _ERROR_NO_ESTIMATOR_PARAMS_;
        return;
    }
    IParamsUnion* params = ptrPUC->getFirstPtr();
    if(nullptr == params)
    {
        /** ����� ���� ��������� threadMethodArgs.rc
        ������ ��� ������ � ���������� ��� (�� ������� ����) � �����
        */
        threadMethodArgs.rc = _ERROR_NO_ESTIMATOR_PARAMS_;
        return;
    }

    const size_t dataSize = m_data.size();

    /// Begin Alina' code
///TODO!!! REMOVE HARD-CODE! Wrap in IParamsUnion
///DELETE!!
/// ��� ����� ���� ���, ��� ��������� ��������� ������ ���� ��� �� � �����������?
///TODO!!! ���� ����� ��� ����� �������!
    std::vector<double> result;
    std::ifstream srcFile("generated_dataset.txt.estimates", std::ios::in);
    std::string tmpString("");
    size_t i = 0;
    double value = 0.0f;
    if (!srcFile || srcFile.fail())
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ":generated_dataset.txt.estimates NOT open!");
        return;
    }
    else
    {
        srcFile >> tmpString;

        i = std::string::npos;
        while((i = tmpString.find(DELIMITER_TO_STRING_ITEMS)) != std::string::npos)
        {
            std::string buf = tmpString;
            buf.erase(i);
            tmpString.erase(0,i+1);
            i = buf.find(DELIMITER_NAME_VALUE);
            if(i != std::string::npos)
            {
                buf.erase(0,i+1);
            }
            /** we are pessimists */
            int rc = _ERROR_NO_INPUT_DATA_;
            try{
                rc = strTo<double>(tmpString.c_str(), value); //OLD          value = atof(tmpString.c_str());
                /** � ��������� ��� ��������� - ��������� ����� ������������ ����� ���� strTo<double>,
                ������� ������ ��� ����, ����� ����������� � �������� ��������� � ���� ����� ����������
                ___unguarded_readlc_active_add_func ()
                MSDN: the internal CRT function  ___unguarded_readlc_active_add_func is obsolete and no longer used, it is exported by the CRT library to preserve binary compatibility.
                The original purpose of ___unguarded_readlc_active_add_func was to return the number of functions that referenced the locale without locking it.
                �.�. ����� ��� ���-�� ����� � �������� ������? �� �� ����� ������?
                */
                std::istringstream buffer(tmpString);
                buffer >> value;
                if(buffer.fail()) value = NAN;
            }
            catch(std::exception e)
            {
                rc = _ERROR_WRONG_INPUT_DATA_;
            }
            if(rc != _RC_SUCCESS_)
            {
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong Input data =>" + tmpString.c_str());
            }
//OLD value = atof(buf.c_str());
            result.push_back(value);
            value = 0.0f;
        }
    }
    if((i = tmpString.find(DELIMITER_NAME_VALUE)) != std::string::npos)
    {
        tmpString.erase(0,i+1);
        /** we are pessimists */
        int rc = _ERROR_NO_INPUT_DATA_;
        try{
            rc = strTo<double>(tmpString.c_str(), value); //OLD          value = atof(tmpString.c_str());
            /** � ��������� ��� ��������� - ��������� ����� ������������ ����� ���� strTo<double>,
            ������� ������ ��� ����, ����� ����������� � �������� ��������� � ���� ����� ����������
            ___unguarded_readlc_active_add_func ()
            MSDN: the internal CRT function  ___unguarded_readlc_active_add_func is obsolete and no longer used, it is exported by the CRT library to preserve binary compatibility.
            The original purpose of ___unguarded_readlc_active_add_func was to return the number of functions that referenced the locale without locking it.
            �.�. ����� ��� ���-�� ����� � �������� ������? �� �� ����� ������?
            */
            std::istringstream buffer(tmpString);
            buffer >> value;
            if(buffer.fail()) value = NAN;
        } catch(std::exception e){
            rc = _ERROR_WRONG_INPUT_DATA_;
        }
        if(rc != _RC_SUCCESS_)
        {
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong Input data =>" + tmpString.c_str());
        }
//OLD  value = atof(tmpString.c_str());
        result.push_back(value);
    }
    srcFile.close();

//DEBUG BEGIN
//std::stringstream sstr;
//sstr << threadMethodArgs.threadIndex << ":";
//sstr << params->getSize() << ":" << result.size() << "=>";
//for(size_t ind = 0; ind < result.size(); ++ind)
//{
//    sstr << result[ind] << ";";
//}
//sstr << std::endl;
//std::cout << sstr.str();
//sstr.clear(); sstr.str("");
//DEBUG END


    if(params->getSize() > result.size()){
        threadMethodArgs.rc = _ERROR_WRONG_INPUT_DATA_;
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Missing Input data in generated_dataset.txt.estimates!");
        return;
    }

    IParamsUnion *hardcodedModelParams = params->clone();
    IParams *ptr = hardcodedModelParams->ptrIParamsUnionToPtrIParams(I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_noiseModelIndex);
    size_t size1 = ptr->getSize();
    for(i = 0; ( (i+1) < result.size()) && (i < size1); ++i)
    {
        ptr->setParam(i, result[i+1]);
    }
    ptr = hardcodedModelParams->ptrIParamsUnionToPtrIParams(I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_particleGeneratingModelIndex);
    size_t size2 = ptr->getSize();
    for(i = 0; ( (i+size1+1) < result.size()) && (i < size2); ++i)
    {
        ptr->setParam(i, result[size1+i+1]);
    }
    result.clear(); tmpString.clear();
    std::stringstream logFileName;
    logFileName << THREAD_LOCAL_LOG_FILE_NAME_PREFIX << threadMethodArgs.threadIndex << THREAD_LOCAL_LOG_FILE_NAME_EXT;
    Check check(dataSize, size1+size2,logFileName.str(),hardcodedModelParams);
    delete hardcodedModelParams;

    /// End Alina' code

    /** ����� �� ������ ���������� ����������
    ���������� ������
    */
    size_t numParticles = 0;
    int rc = getNumberOfParticlesFromEstimatorParams(numParticles);
    if(rc != _RC_SUCCESS_)
    {
        threadMethodArgs.rc = rc;
        return;
    }

    std::vector<double> particles(numParticles);

    unsigned int counterBestStatisticsFound = 0;
    bool isStatisticsInitialized = false;

    size_t t = 0;
    I_PF_AdditiveModel* model = dynamic_cast<I_PF_AdditiveModel*>(m_additiveModel);
    if(nullptr == model)
    {
        threadMethodArgs.rc = _ERROR_NO_MODEL_;
        return;
    }

    double valLoglikelihood;

//DEBUG
//size_t tmpPointsCounter = 0;
//END DEBUG
    size_t sizeOrgCompact = m_puc->getSize();
    while (!(ptrPUC->isEnd()))
    {
        tPointEstimatesData currPointEstimates;
///SEE tPointEstimatesData's CTOR        currPointEstimates.logLikelihood = tmpParam4logLikelihood;
//DEBUG BEGIN
 //std::stringstream sStr;
 //sStr << "locPUC" << ptrPUC->toString() << std::endl
// << "params=" << params << std::endl;
//DEBUG END
//REDUNDANT???         mutexReduction.lock();
        /** ��������� id ������� ����� ����������� � �������� ��������
         � �������� ��� � currPointEstimates.id
        */
        rc = m_puc->getId_byIParamsUnion(params, currPointEstimates.id);
//REDUNDANT???         mutexReduction.unlock();
///TODO!!! ���� ����������, ��� ���-�� ���������� ����� id (> 2 000 000 000)
        if((_RC_SUCCESS_ != rc) || (currPointEstimates.id >= sizeOrgCompact))
        {
//DEBUG END
 std::stringstream sStr;
 sStr << currPointEstimates.id << "- wrong id" << std::endl
 << "params=" << params->toString() << std::endl
 << "globalPUC" << m_puc->toString() << std::endl
 << "threadPUC" << ptrPUC->toString() << std::endl;
 std::cout << sStr.str();
 sStr.clear();
//DEBUG END
///TODO!!! clean up tmp stuff!
          threadMethodArgs.rc = _ERROR_WRONG_ESTIMATOR_PARAMS_;
          return;
        }

        currPointEstimates.logLikelihood = -DBL_MIN;
        currPointEstimates.isNeedToConsider = true;
        model->initParticles(I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_particleGeneratingModelIndex, params, particles);

        /**
        ������ ������ �������
        IAA: replaced with cross-platform Performance Timing solution
        */
        float duration;
        PerformanceTiming pt;

        /**
        ������ �� ���� ���������� � ����������
        ������� ������������� ��� �������� ������ ���������� params
        */
        for (t = 1; t < dataSize; ++t)
        {
            model->generateParticles(I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_particleGeneratingModelIndex, params, particles);

            valLoglikelihood = currPointEstimates.logLikelihood;
            valLoglikelihood += model->estimateHypothesis( I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_noiseModelIndex, params, m_data[t], particles, t);

            currPointEstimates.logLikelihood = valLoglikelihood;

            duration = pt.getDurationAsNanoSecs();
///TODO!!! ��� �� ������������ � ������ ������������ ���� �������?
            //if (isStatisticsInitialized && (float)(timeToCheckEnd.QuadPart - timeToCheckStart.QuadPart) / timeFreq.QuadPart > (compactSize*0.1 + 0.5)*m_timeTrajectory[threadMethodArgs.threadIndex].averageTime)
            if (isStatisticsInitialized && (!passStatistics(t, valLoglikelihood, threadMethodArgs.tmp_statistics)))
            ///commented Alina's check
            /*** if ( isStatisticsInitialized && t>200 &&(!check.checkFutility(t, size, hypLogLikelihood, logLikelihood, PF_params, model,I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_noiseModelIndex,particles,m_data,log)))// if ( isStatisticsInitialized && (!passStatistics(t,  hypLogLikelihood, tmp_statistics, logLikelihood )))
            */
            {
                ///Begin Alina's code
                ///��� ������� ������� ������� ��� 6 ������ ���������� + ��� 1 ������ -  ��� ������� �������������
///IAA: �� �� ����� �������� � ������� valLoglikelihood �������� ������ ���������!
/// ���� ����� �������� ��� ������ ��� "������������",
/// ���� �� ������ � �� �����
                check.writeEstimatesVals(params, currPointEstimates.id, valLoglikelihood);
                /// ��� ������� ������� ��������� ���������� ��������� "�������� ������� ������ ���������� ��� ���", 1 - ���� �������� ������� ������ ����������, ID ����� � �������� == ������� ������ ����������, ����� ����������,
                check.writeEstInfo(1,t,currPointEstimates.id, 6);
                ///End Alina's code

                /// ����� ���� �������� �� ������������������
                /// ������������ �������� ����� isNeedToConsider
                ///currPointEstimates.isNeedToConsider = false;
                /// if (isNeedToConsider == false), ��������� ������ ������
                threadMethodArgs.tmp_statistics = threadMethodArgs.statistics;
                /**
                Interrupt data track processing for current params
                */
                break;
            }
        }/// ��������� ������ �� ����������, ���� �� �����, ���� �������� ��������
        /** ����� ������ �������
        IAA: replaced with cross-platform Performance Timing solution
        */
        duration = pt.getDurationAsNanoSecs();

        mutexReduction.lock();
        m_timeTrajectory[threadMethodArgs.threadIndex].averageTime = (m_timeTrajectory[threadMethodArgs.threadIndex].averageTime * (float)m_timeTrajectory[threadMethodArgs.threadIndex].cnt + duration) / (float)m_timeTrajectory[threadMethodArgs.threadIndex].cnt;
        m_timeTrajectory[threadMethodArgs.threadIndex].cnt++;
        mutexReduction.unlock();

        ///Begin Alina's code
        if(t == dataSize)
        {
            check.writeEstimatesVals(params, currPointEstimates.id, valLoglikelihood);
            check.writeEstInfo(0,t,currPointEstimates.id,6);
        }
        ///End Alina's code

        if (t < dataSize)
        {
            mutexReduction.lock();
//OLD(PUSH_BACK)            m_compactEstimatesData[threadMethodArgs.threadIndex].push_back(currPointEstimates);
            try
            {
                m_compactEstimatesData[threadMethodArgs.threadIndex].emplace_back(currPointEstimates.id, currPointEstimates.logLikelihood, currPointEstimates.isNeedToConsider);
            }
            catch (const std::bad_alloc&)
            {
                std::cout << "bad_alloc in thread with index " << threadMethodArgs.threadIndex << std::endl;
                threadMethodArgs.rc = _ERROR_NO_ROOM_;
///TODO!!! ������ ����� �� ��������? ������ �� �������� � ����?
                return;
            }
            mutexReduction.unlock();
            params = ptrPUC->getNextPtr();
            continue;
        }
        valLoglikelihood = currPointEstimates.logLikelihood;
        // Begin Maria's code 07.07.17
//OLD        mutexReduction.lock();
//OLD        m_bestLogLikelihood[threadMethodArgs.threadIndex]->getParam(0, valBestLogLikelihood);
//OLD       mutexReduction.unlock();
//OLD        if (valLoglikelihood > valBestLogLikelihood)
        // End Maria's code 07.07.17
        if (valLoglikelihood > m_bestLogLikelihood[threadMethodArgs.threadIndex])
        {
            /**
            ���������� ����� ������ ������ �������������
            */
            evaluateNewStatistics(AVERAGE_MODE, counterBestStatisticsFound, threadMethodArgs.statistics, threadMethodArgs.tmp_statistics);
            counterBestStatisticsFound++;
            isStatisticsInitialized = true;
            mutexReduction.lock();
            // Begin Maria's code 07.07.17
//OLD            m_bestLogLikelihood[threadMethodArgs.threadIndex]->setParam(0, valLoglikelihood);
            m_bestLogLikelihood[threadMethodArgs.threadIndex] = valLoglikelihood;
            // End Maria's code 07.07.17
            mutexReduction.unlock();
        }
        else
        {
            threadMethodArgs.tmp_statistics = threadMethodArgs.statistics;
        }
        mutexReduction.lock();
//OLD(PUSH_BACK)            m_compactEstimatesData[threadMethodArgs.threadIndex].push_back(currPointEstimates);
        try
        {
            m_compactEstimatesData[threadMethodArgs.threadIndex].emplace_back(currPointEstimates.id, currPointEstimates.logLikelihood, currPointEstimates.isNeedToConsider);
        }
        catch (const std::bad_alloc&)
        {
            std::cout << "bad_alloc in thread with index" << threadMethodArgs.threadIndex << std::endl;
            threadMethodArgs.rc = _ERROR_NO_ROOM_;
///TODO!!! ������ ����� �� ��������? ������ �� �������� � ����?
            return;
        }
        mutexReduction.unlock();
        params = ptrPUC->getNextPtr();
//DEBUG BEGIN
//size_t id = 0;
//m_puc->getId_byIParamsUnion(params, id);
//std::cout << ++tmpPointsCounter << "; params=" ;// << ptrCompact->getCurrentId() << " " << id;
//std::cout << params->toString() << " after getNext!" << std::endl;
//DEBUG END
    }///end of walk through compact
//DEBUG BEGIN
std::cout << "Success" << threadMethodArgs.threadIndex << std::endl;
//DEBUG END
    /** ����� ���, ��� ����� �� ������,
    �������� ��� �������� ������ ��������� threadMethodArgs, ���������� ���� ��������� ������� �� ������,
    ����� �������� rc �� ������ ������
    */
    threadMethodArgs.rc = _RC_SUCCESS_;
}
///END Maria's code
/*********************************************************
* get Number Of Particles From Estimator Params
* PARAMS [OUT] numberOfParticles -- Number Of Particles
 RETURNs _RC_SUCCESS_ if success
 _ERROR_NO_ESTIMATOR_PARAMS_ otherwise
**********************************************************/
int PF_PointEstimator::getNumberOfParticlesFromEstimatorParams(size_t& numberOfParticles) const
{
    std::stringstream sstr;
    if(!m_estimatorParams )
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's params NOT set!");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    double tmpDbl = NAN;
    m_estimatorParams->getParam(indexNumberOfParticles, tmpDbl);
    if(tmpDbl < DBL_MIN)
    {
        sstr.clear(); sstr.str(std::string());
        sstr << "Wrong Number of particles : " << (int)tmpDbl;
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + sstr.str());
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    numberOfParticles = (size_t) tmpDbl;
    return _RC_SUCCESS_;
}
/**
  ��� ��� ���������� � ���� ��������������� ������������ ������� statistics,
  �������� �������� ������ � �������� ���������
  ��� ���������� ����������� ��������� ���������� ������� �� �������� � ��������������� ������������ ������
*/
void PF_PointEstimator::evaluateNewStatistics(enum STATISTICS_ESTIMATE_MODE mode, unsigned int& counterBestStatisticsFound, std::vector<double>& statistics, std::vector<double>& tmp_statistics)
{
    if (counterBestStatisticsFound == 0)
    {
        mode = REPLACE_MODE;
    }
    switch(mode)
    {
    case REPLACE_MODE:
    {
        statistics = tmp_statistics;
        return;
    }
    case AVERAGE_MODE:
    {
        for(unsigned int i = 0; i < tmp_statistics.size(); ++i)
        {
            statistics[i] = ((statistics[i] * (double)counterBestStatisticsFound) + tmp_statistics[i])/(double)(counterBestStatisticsFound + 1);
        }
        return;
    }
    }
    return;
}
/*********************************************************
* get Number Of Particles From Estimator Params
* PARAMS [IN] t -- index of trajectory point
* PARAMS [IN] logLikelihood -- current estimate of likelihood
* PARAMS [OUT] statistics -- vector with statistics
 RETURNs _RC_SUCCESS_ if success
 _ERROR_NO_ESTIMATOR_PARAMS_ otherwise
**********************************************************/
bool PF_PointEstimator::passStatistics(unsigned t, double logLikelihood, std::vector<double>& statistics)
{
    if (logLikelihood < statistics[t])
        return false;
    /** ����� ������ �� ������ ������ ������ �����������, ���������� � ���� �����, � ������ */
    statistics[t] = logLikelihood;
///TODO!!! �� �������� ���������, ��� ������ statistics ��� ��������!
///� ����� ��� ����� ������� ���������� ��������� ��������� ����������� {INC = \SUM_{i = t+1}^{track_size} {statistics[i] }
/// ��������� ��� � ��� ������������, � ������� �������� ����������, �����������
/// � ����� �������� � ������ ��������� �������� �������������!

    return true;
}
/** CTOR
*/
PF_PointEstimator::PF_PointEstimator(Logger*& log) : FakePointEstimator(log)
{
    /** MUST re-init m_pet since in FakePointEstimatyor this member
    has been initialized as FakePointEstimator type
     */
    m_pet            = PointEstimatorsTypes::PF_AdditiveModel_PointEstimator;
    /** m_task = nullptr; m_logger = log; already initialized in FakePointEstimator's CTOR */
    m_model          = nullptr;
    m_additiveModel  = nullptr;
    /** Model's Params Compact */
    m_puc            = nullptr;
    /** we put here estimates, which are produced by estimator */
    m_setOfParamsUnionEstimates = nullptr;
    /** we put here the estimates' loglikelihood, which are produced by estimator */
    m_setOfLogLikelihoods = nullptr;
    /** Estimator's Params */
    m_estimatorParams = nullptr;
}
/** DTOR
*/
PF_PointEstimator::~PF_PointEstimator()
{
    m_data.clear();
    delete m_estimatorParams;
    delete m_setOfParamsUnionEstimates;
    delete m_setOfLogLikelihoods;
    delete m_puc;
}
/** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */

/*********************************************************
* parse And Create Estimator Params
* PARAMS [IN] paramsMap -- map with estimator's params as strings
* PARAMS [IN] postfix -- postfix to search proper params in map
* PARAMS [OUT] size -- size of array ptrToPtrIParams of pointers
* PARAMS [OUT] ptrToPtrIParams -- result of parsing
* PARAMS [IN] log -- logger to log errors
 RETURNs _RC_SUCCESS_ if success
 _ERROR_WRONG_WORKFLOW_,_ERROR_WRONG_ARGUMENT_, _ERROR_NO_ROOM_, _ERROR_WRONG_MODEL_PARAMS_  otherwise
**********************************************************/
int PF_PointEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& paramsMap, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
    if(m_additiveModel == nullptr)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": NONE additive model set to parse estimator's params.");
        return _ERROR_WRONG_WORKFLOW_;
    }
    size_t TotalAddendsOfAdditiveModel = 0;
    int rc = m_additiveModel->getTotalOfAddends(TotalAddendsOfAdditiveModel);
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": NONE addend in additive model to parse estimator's params.");
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** ������ �������� ��������� �� IParams ��������� ���������� ������!
    */
    auto ptrAddendsParams = new IParams *[TotalAddendsOfAdditiveModel][TOTAL_OF_INTERVAL_ESTIMATE_INDEXES];

    /** IAA : ������� ������� �� ����������� ������� ��������� �������� ������ ������,
    ������� (�������) ���������� ������� ������� ��������
    */
    for(size_t i = 0; i < TotalAddendsOfAdditiveModel; ++i)
    {
        for(size_t j = LeftBoundary; j < TOTAL_OF_INTERVAL_ESTIMATE_INDEXES; ++j)
        {
            ptrAddendsParams[i][j]=nullptr;
///TODO!!! move to grammar!
            /** GRAMMAR! ����� �������� � ���� ��������� ���� ���������,
             ���� ��� ������ ������ � ���� �� ����, �������� ��� ���������,
             �������� ����� ���� ���������� ����������, ������������� ���������� ������ ����������,
             � �������� ���� ��������� ���������
            */
            std::string postfixTmp = IIntervalEstimator::grammarIntervalEstimates[j];
            std::stringstream ss;
            ss << IAdditiveModelEstimator::grammarAdditiveModelEstimatorConfig[ADDITITIVE_MODEL_ADDENS_REPO_ENTRIES_ITEM] << "_" << i << "_" << postfixTmp;
            std::string tmpStr = ss.str();
            std::vector<std::string> keys;
            iteratorParamsMap itParam = paramsMap.find(tmpStr);
            if (itParam != paramsMap.end())
            {
                std::string repoEntriesStr = itParam->second;
#ifdef _DEBUG_
                std::cout << repoEntriesStr << std::endl;
#endif
                log->log(WORKFLOW_TASK_NOTIFICATION, "Get entries from repo with keys :  " + repoEntriesStr);
                /** �����, ���������� ��������� ��� ����, ����� ������� �������� ��������� ������
                 ����-������� ������ � �� ���������!
                 ��������� ������ ��� ��� � ���� ����������, ������ ��� ��������� � ���������� ���� ����� �������� ��� ������!
                 */
                if(DataManagerGrammar::splitAllRepoKeysAsSingleString(repoEntriesStr,keys) != _RC_SUCCESS_)
                {
                    delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                    log->error(WORKFLOW_TASK_ERROR, "No entries");
                    return _ERROR_WRONG_ARGUMENT_;
                }
            }
            else
            {
                delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": NO FOUND REPO_INDEX_ITEM " + tmpStr + " for addend of additive model to parse estimator's params.");
                return _ERROR_WRONG_WORKFLOW_;
            }
            size_t dimension = keys.size();
            DataInfo** di = nullptr;
            di = new DataInfo* [dimension];
            if(di == nullptr)
            {
                delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "No room to allocate DataInfo** for Estimator's/Model's params");
                return _ERROR_NO_ROOM_;
            }
            else
            {
                for(size_t index_di = 0; index_di < dimension; ++index_di)
                {
                    di[index_di] = nullptr;
                }
            }
            rc = m_task->getRepoDataForTargetObj(keys, static_cast<size_t> (dimension), static_cast<DataInfo** const> (di));
            if(_RC_SUCCESS_ != rc)
            {
                for(size_t index_di = 0; index_di < dimension; ++index_di){
                    delete di[index_di]; di[index_di] = nullptr;
                }
                delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Can not get data from repository!");
                return rc;
            }
///tODO!!! ���������, ��� ��������� ��� grammarIntervalEstimates[j]

            /** ����� ���������� ������ ��� ��������������� IParams
            ������� ������� ���� di �������� ���������� (������),
            ����� �� ������ (��������������) ���������� di � ���� IParams � ������ ��� ����������!
            /
            enum IModel::ModelsTypes addendModelType = IModel::ModelsTypes::FAKE_MODEL;
            int rc = m_additiveModel->getAddendType(i, addendModelType);
            if(rc != _RC_SUCCESS_)
            {
                size_t k = j;
                size_t l = i;
                while(i >= 0)
                {
                    while(k >= 0)
                    {
                        delete ptrAddendsParams[i][j];
                        ptrAddendsParams[i][j] = nullptr;
                        --k;
                    }
                    --l;
                }
                for(size_t index_DI = 0; index_DI < dimension; ++index_DI) delete di[index_DI];
                delete [] di;

                return rc;
            }
///TODO!!! ��� ����� ���� �� ��������� ������ di, ������������ ��� ���������� � IParams, ��������������� ���� ���������� ����� ���������� (������)
            rc = IModel::parseDataInfoAndCreateModelParams(addendModelType, dimension, (const DataInfo ** const) di, ptrAddendsParams[i][j], (Logger* const) m_logger);
*/
            IModel* addendModel = nullptr;
            rc = m_additiveModel->getPtrToAddend(i, addendModel);
            if(rc != _RC_SUCCESS_)
            {
                int k = j;
                int l = i;
                while(l >= 0)
                {
                    while(k >= 0)
                    {
                        delete ptrAddendsParams[i][j];
                        ptrAddendsParams[i][j] = nullptr;
                        --k;
                    }
                    --l;
                }
                for(size_t index_DI = 0; index_DI < dimension; ++index_DI) delete di[index_DI];
                delete [] di;

                delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                return rc;
            }
//DEBUG BEGIN
for(size_t index_DI = 0; index_DI < dimension; ++index_DI)
std::cout <<  di[index_DI]->m_dataModelName << ":" << di[index_DI]->m_dataSetName << std::endl;
//DEBUG END
            rc = addendModel->parseDataInfoAndCreateModelParams(dimension, const_cast<const DataInfo ** const> (di), ptrAddendsParams[i][j]);

            for(size_t index_DI = 0; index_DI < dimension; ++index_DI) delete di[index_DI];
            delete [] di;

            if((rc != _RC_SUCCESS_) || (!ptrAddendsParams[i][j]))
            {
                int k = j;
                int l = i;
                while(l >= 0)
                {
                    while(k >= 0)
                    {
                        delete ptrAddendsParams[i][j];
                        ptrAddendsParams[i][j] = nullptr;
                        --k;
                    }
                    --l;
                }

                delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Can not parse DataInfo* and create IParams of Model's params");
                return rc;
            }
        }
    }
    /** ��� ����� ��� �������� ���������� IParams LeftBoundaries � ������ ParamsUnion,
    ������������ ������ ����� ���� ��������
    */
    /** ���� � ��� ��� ����������� ������ ��� PF ���������� ������ � ����� ���������� */
    size_t totalOfItems = I_PF_AdditiveModel::dim_I_PF_AdditiveModel;
///TODO!!! �����-����������� ���� ������ 4.6 �� ����� �������� ������, ����� ������ enum I_PF_AdditiveModel::dim_I_PF_AdditiveModel �������� ��������� ���������� totalOfItems
///TODO!!! BUG!    IParams* items[I_PF_AdditiveModel::dim_I_PF_AdditiveModel] =
///    {
///        ptrAddendsParams[0][LeftBoundary],
///        ptrAddendsParams[1][LeftBoundary]
///    };
    IParams** items = new IParams*[I_PF_AdditiveModel::dim_I_PF_AdditiveModel];

    items[0] = ptrAddendsParams[0][LeftBoundary];
    items[1] = ptrAddendsParams[1][LeftBoundary];

    IParamsUnion * puLB = IParamsUnion::createParamsUnion(static_cast<size_t> (totalOfItems), const_cast<const IParams**> (items), m_logger);
    delete items[0]; items[0]=nullptr;
    delete items[1]; items[1]=nullptr;
    if(!puLB)
    {
        delete [] items;
        delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Can not create IParamsUnion for LeftBoundary of Model's compact");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** ����� �������� ���������� IParams RightBoundaries � ������ ParamsUnion,
    ������������ ������� ������ ���� ��������
    */
    items[0] = ptrAddendsParams[0][RightBoundary];
    items[1] = ptrAddendsParams[1][RightBoundary];
    IParamsUnion * puRB = IParamsUnion::createParamsUnion(static_cast<size_t>(totalOfItems), const_cast<const IParams**> (items), m_logger);
    delete items[0]; items[0]=nullptr;
    delete items[1]; items[1]=nullptr;
    if(nullptr == puRB)
    {
        delete [] items;
        delete puLB; puLB = nullptr;
        delete puRB; puRB = nullptr;
        delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Can not create IParamsUnion for RightBoundary of Model's compact");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** create IParams for increments */
    /// TODO: memory leak!!!!!!!!!!!
    auto dimOfAddendsParams = new size_t [TotalAddendsOfAdditiveModel];
    for(size_t i = 0; i < TotalAddendsOfAdditiveModel; ++i)
    {
        dimOfAddendsParams[i] = puRB->ptrIParamsUnionToPtrIParams(i)->getSize();
        items[i] = IParams::createEmptyParams(m_logger, IParams::Params1D, dimOfAddendsParams[i]);
        if(!items[i])
        {
            int j = i;
            while(j >= 0)
            {
                if(items[j])
                {
                    delete items[j];
                    items[j]=nullptr;
                }
                --j;
            }
            delete [] items;
            delete puLB; puLB = nullptr;
            delete puRB; puRB = nullptr;
            delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
            delete [] dimOfAddendsParams; dimOfAddendsParams = nullptr;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "No room to allocate IParams* for Estimator's/Model's increment params");
            return _ERROR_NO_ROOM_;
        }
    }
    /** ������ ����� �������� ���������� �� �����������
    ��� ������� �� ���������� ������� �� ���������
    */
    for(size_t i = 0; i < TotalAddendsOfAdditiveModel; ++i)
    {
        for(size_t j = 0; j < dimOfAddendsParams[i]; ++j)
        {
            std::stringstream ss;
            ss << IParamsCompact::grammar[IParamsCompact::indexesOfGrammarForCompact::indexOfIncrementValue];
            ss << "_" << i << "_" << j;
            std::string tmpStr = ss.str();
            iteratorParamsMap itParam = paramsMap.find(tmpStr);
            if (itParam != paramsMap.end())
            {
                double increment = NAN;
                int rc = _ERROR_NO_INPUT_DATA_;
                rc = strTo<double>(itParam->second.c_str(), increment); //OLD          double increment = atof(itParam->second.c_str());
                if((rc != _RC_SUCCESS_) || (increment < 0))
                {
                    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong increments for Model's Compact =>" + itParam->second.c_str());

                    delete items[0]; items[0]=nullptr;
                    delete items[1]; items[1]=nullptr;
                    delete [] items;
                    delete puLB;     puLB = nullptr;
                    delete puRB;     puRB = nullptr;
                    delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                    delete [] dimOfAddendsParams; dimOfAddendsParams = nullptr;
                    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": NEGATIVE increments for Model's Compact forbidden!");
                    return _ERROR_WRONG_MODEL_PARAMS_;
                }
                rc = items[i]->setParam(j, increment);
            }
            else
            {
///TODO!!! clean up
                delete items[0]; items[0]=nullptr;
                delete items[1]; items[1]=nullptr;
                delete [] items;
                delete puLB;     puLB = nullptr;
                delete puRB;     puRB = nullptr;
                delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
                delete [] dimOfAddendsParams; dimOfAddendsParams = nullptr;
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": NOT FOUND " + tmpStr + " for addend of additive model to parse estimator's params.");
                return _ERROR_WRONG_WORKFLOW_;
            }
        }
    }///end for to pull out incrementValues
    delete [] dimOfAddendsParams; dimOfAddendsParams = nullptr;
    /** �������� ParamsUnion �� �����������
    */
    IParamsUnion * puINCR = IParamsUnion::createParamsUnion(static_cast<size_t>(totalOfItems), const_cast<const IParams**>(items), m_logger);
    delete items[0]; items[0] = nullptr;
    delete items[1]; items[1] = nullptr;
    delete [] items; items = nullptr;
    delete [] ptrAddendsParams; ptrAddendsParams = nullptr;
    if(nullptr == puINCR)
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Can not create IParamsUnion for increments of Model's compact");
        return _ERROR_WRONG_ARGUMENT_;
    }
    bool isIncrementTypeStepValue = true;
    std::stringstream ss;
    ss << IParamsCompact::grammar[IParamsCompact::indexesOfGrammarForCompact::indexOfIncrementType];
    std::string tmpStr = ss.str();
    iteratorParamsMap itParam = paramsMap.find(tmpStr);
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != paramsMap.end())
    {
        int tmpInt=-1;
        rc = strTo<int>(itParam->second.c_str(), tmpInt); //OLD tmpInt = atoi(itParam->second.c_str());
        paramsMap.erase(itParam);
        isIncrementTypeStepValue = (tmpInt == 0) ? false : true;
    }

    if(rc != _RC_SUCCESS_)
    {
        delete puLB; puLB = nullptr;
        delete puRB; puRB = nullptr;
        delete puINCR; puINCR = nullptr;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": NOT FOUND " + tmpStr + " for addend of additive model to parse estimator's params.");
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** � voila - ��� ��� ����� ��������� �������� ��� ParamsUnion � �������!
    */
    IParamsUnionCompact * puc = IParamsUnionCompact::createParamsUnionCompact( (const IAdditiveModel*) m_additiveModel,  puLB, puRB, puINCR, isIncrementTypeStepValue, m_logger);

std::cout <<"L" << DELIMITER_TO_STRING_ITEMS << puLB->toString() << std::endl;
std::cout <<"R" << DELIMITER_TO_STRING_ITEMS << puRB->toString() << std::endl;
#ifdef _BOUNDARIES_TEST_
std::string boundaries = "boundaries";
    if((puLB) && (puRB))
    {
        std::ofstream estimatesFile(boundaries.c_str(), std::ios_base::app);
        if (estimatesFile)
        {
            estimatesFile <<"L" << DELIMITER_TO_STRING_ITEMS << puLB->toString() << std::endl;
            estimatesFile <<"R" << DELIMITER_TO_STRING_ITEMS << puRB->toString() << std::endl;
        }
        estimatesFile.close();
    }
#endif

    delete puLB; puLB = nullptr;
    delete puRB; puRB = nullptr;
    delete puINCR; puINCR = nullptr;

    ///TODO!!!  ���  ��� � ����� ����� � ��� di!  int rc = this->setEstimatorParams(dimension, (const DataInfo**) di);
    if(!puc)
    {
        /** �������� di[] */
///TODO!!! �����������, ��� � ��� �������            freeArrayOfDataInfo(dimension, di);
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Wrong estimator params provided for Estimator");
        return _ERROR_WRONG_ARGUMENT_;
    }

    /** ����� ����� ������ � IParamsUnionCompact* m_puc ��� ����� �������!
     ������ ����� ��������� ��������� ��������� ������.
     */
    m_puc = puc;
    /** Once we have parsed Compact's params
    Let's parse PF_PointEstimator's estimators
    */
    int tmpInt = -1;

    itParam = paramsMap.find(PF_PointEstimator::grammar[indexMode]);
    if (itParam != paramsMap.end())
    {
        /** we are pessimists */
        int rc = _ERROR_NO_INPUT_DATA_;
        rc = strTo<int>(itParam->second.c_str(), tmpInt); //OLD tmpInt = atoi(itParam->second.c_str());
        paramsMap.erase(itParam);
        if((rc != _RC_SUCCESS_) || ((tmpInt < 0) || (tmpInt > FakeIndex_PF_PointEstimatorMode)) )
        {
            ss.clear(); ss.str(std::string());
            ss << "Wrong Mode of PF_Estimator : " << tmpInt;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
///TODO tmpInt >= 0???
        tmpInt = (tmpInt > 0) ? tmpInt : FakeIndex_PF_PointEstimatorMode;
    }
    else
    {
        tmpInt = FakeIndex_PF_PointEstimatorMode;
        ss.clear(); ss.str(std::string());
        ss << " Mode of PF_Estimator NOT FOUND! Use default value : " << tmpInt;
        m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
    }
    IParams* params = IParams::createEmptyParams(m_logger, IParams::Params1D, FakeIndex_IndexesOfGrammarPF_PointEstimator);
    PF_PointEstimatorModes mode = static_cast<enum PF_PointEstimatorModes> (tmpInt);
    if(!params)
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "NO room to allocate estimator's params.");
        return _ERROR_NO_ROOM_;
    }

    params->setParam(indexMode, mode);

    itParam = paramsMap.find(PF_PointEstimator::grammar[indexNumberOfParticles]);
    if (itParam != paramsMap.end())
    {
        /** we are pessimists */
        int rc = _ERROR_NO_INPUT_DATA_;
        rc = strTo<int>(itParam->second.c_str(), tmpInt); //OLD tmpInt = atoi(itParam->second.c_str());
        paramsMap.erase(itParam);
        if((rc != _RC_SUCCESS_) || (tmpInt <= 0))
        {
            ss.clear(); ss.str(std::string());
            ss << "Wrong Number of particles : " << tmpInt;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    else
    {
        ss.clear(); ss.str(std::string());
        ss << " Number of particles NOT FOUND! Use default value : " << DEFAULT_NUMBER_OF_PARTICLES;
        m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
        tmpInt = DEFAULT_NUMBER_OF_PARTICLES;
    }
    /** params have been already allocated above = IParams::createEmptyParams(m_logger, IParams::Params1D, FakeIndex_IndexesOfGrammarPF_PointEstimator);
    */
    if(!params)
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "NO room to allocate estimator's params.");
        return _ERROR_NO_ROOM_;
    }
    params->setParam(indexNumberOfParticles, tmpInt);
///BEGIN Maria's code
    size_t tmpTotalOfThreads = 0;
    tmpInt = -1;
    itParam = paramsMap.find(PF_PointEstimator::grammar[indexNumberOfThreads]);
    if (itParam != paramsMap.end())
    {
        /** we are pessimists */
        int rc = _ERROR_NO_INPUT_DATA_;
        rc = strTo<int>(itParam->second.c_str(), tmpInt); //OLD tmpInt = atoi(itParam->second.c_str());
        paramsMap.erase(itParam);
        if((rc != _RC_SUCCESS_) || (tmpInt <= 0))
        {
            ss.clear(); ss.str(std::string());
            ss << "Wrong Number of threads : " << tmpInt;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
        tmpTotalOfThreads = static_cast<size_t> (tmpInt);
    }
    else
    {
        ss.clear(); ss.str(std::string());
        ss << " Number of threads NOT FOUND! Use default value : " << DEFAULT_NUMBER_OF_THREADS;
        m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
        tmpTotalOfThreads = DEFAULT_NUMBER_OF_THREADS;
    }

    /** ���������, ��� ��� ���������� ������� (= ����� ������������) ��������� ���������
    */
    /**
    ��� ��������� ������������� ����� ����� ������ ��� ������ MT
    */
    if( mode == INDEX_MT)
    {
        if(tmpTotalOfThreads > m_puc->getMinRelativeSizeAlongAxis())
        {
            /** If total of threads (=total of subcompacts) twice (or greater) as much as max relative size of compact
            So far we can not clone that number of disjoint subcompacts!
            */
// std::cout << (2*tmpTotalOfThreads) << " " << m_puc->getMaxRelativeSizeAlongAxis() << std::endl;
            if( (2*tmpTotalOfThreads) > m_puc->getMaxRelativeSizeAlongAxis())
            {
                ss.clear();
                ss.str(std::string());
                ss << " '" << tmpTotalOfThreads << "'" << " - wrong number of threads! It is GT max_relative_size/2 of compact at least along one of the compact's axis! " << " We will get issue with subcompact cloning!";
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
                return _ERROR_WRONG_ARGUMENT_;
            }
        }
        params->setParam(indexNumberOfThreads, tmpTotalOfThreads);

        double tmpDbl = -1.f;
        itParam = paramsMap.find(PF_PointEstimator::grammar[indexThresholdDistanceValue]);
        if (itParam != paramsMap.end())
        {
            int rc = _ERROR_NO_INPUT_DATA_;
            rc = strTo<double>(itParam->second.c_str(), tmpDbl); //OLD          tmpDbl = atof(itParam->second.c_str());
            if(rc != _RC_SUCCESS_)
            {
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong THRESHOLD_DISTANCE_VALUE =>" + itParam->second.c_str());
                return _ERROR_WRONG_MODEL_PARAMS_;
            }
            tmpDbl = (tmpDbl > 0.0f) ? tmpDbl : DEFAULT_THRESHOLD_DISTANCE_VALUE;
        }
        else
        {
            ss.clear();
            ss.str(std::string());
            ss << " ThresholdDistanceValue NOT FOUND! Use default value : " << DEFAULT_THRESHOLD_DISTANCE_VALUE;
            m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
            tmpDbl = DEFAULT_THRESHOLD_DISTANCE_VALUE;
        }

        IParamsUnion *tmpLeftBoundaryOfOrgCompact = m_puc->getParamsUnionCopy(0);
        IParamsUnion *tmpRightBoundaryOfOrgCompact = m_puc->getParamsUnionCopy(m_puc->getSize() - 1);
        double maxDistance = 0.0f;
        rc = IParamsUnion::norm2( (const IParamsUnion*) (tmpLeftBoundaryOfOrgCompact), (const IParamsUnion*) (tmpRightBoundaryOfOrgCompact), maxDistance);
        delete tmpLeftBoundaryOfOrgCompact;
        tmpLeftBoundaryOfOrgCompact = nullptr;
        delete tmpRightBoundaryOfOrgCompact;
        tmpRightBoundaryOfOrgCompact = nullptr;
        if( _RC_SUCCESS_ != rc)
        {
            ss.clear();
            ss.str(std::string());
            ss << " Wrong diameter of compact!";
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** ��������, ��� ���� ����� ����� ���� �������� ��� ���������� ������������
        */

        if ((tmpDbl < DBL_MIN) || (tmpDbl > maxDistance/tmpTotalOfThreads ))
        {
            ss.clear();
            ss.str(std::string());
            ss << " Wrong ThresholdDistanceValue : " << tmpDbl << ">" << maxDistance << "/" << tmpTotalOfThreads ;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
        params->setParam(indexThresholdDistanceValue, tmpDbl);

        tmpInt = -1;
        itParam = paramsMap.find(PF_PointEstimator::grammar[indexAlgorithmToMakeSubcompacts]);
        if (itParam != paramsMap.end())
        {
            IndexesOfAlgorithmsToMakeSubcompacts i = static_cast<IndexesOfAlgorithmsToMakeSubcompacts>(0);
            while( i < FakeIndex_AlgorithmsToMakeSubcompacts)
            {
                if(itParam->second == grammarAlgorithmsToMakeSubcompacts[i]){
                    tmpInt = i;
                    break;
                }
                i = static_cast<IndexesOfAlgorithmsToMakeSubcompacts>(static_cast<int>(i) + 1);
            }

            if(tmpInt < 0)
            {
                ss.clear();
                ss.str(std::string());
                ss << "Wrong subcompacts' algorithm : " << tmpInt;
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
                return _ERROR_WRONG_ARGUMENT_;
            }
        }
        else
        {
            ss.clear();
            ss.str(std::string());
            ss << " Subcompacts' algorithm NOT FOUND! Use default value : " << grammarAlgorithmsToMakeSubcompacts[DETERMINISTIC_ALGORITHM];
            m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
            tmpInt = DETERMINISTIC_ALGORITHM;
        }
        params->setParam(indexAlgorithmToMakeSubcompacts, tmpInt);

        tmpInt = -1;
        itParam = paramsMap.find(PF_PointEstimator::grammar[indexSubcompactSize]);
        if (itParam != paramsMap.end())
        {
            /** we are pessimists */
            int rc = _ERROR_NO_INPUT_DATA_;
            rc = strTo<int>(itParam->second.c_str(), tmpInt); //OLD tmpInt = atoi(itParam->second.c_str());
            paramsMap.erase(itParam);
            if((rc != _RC_SUCCESS_) || (tmpInt <= 0))
            {
                ss.clear();
                ss.str(std::string());
                ss << "Wrong subcompacts' size : " << tmpInt;
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
                return _ERROR_WRONG_ARGUMENT_;
            }
//OLD            tmpInt = (tmpInt > 0) ? tmpInt : DEFAULT_SUBCOMPACTS_SIZE;
        }
        else
        {
            ss.clear();
            ss.str(std::string());
            ss << " Subcompacts' size NOT FOUND! Use default value : " << DEFAULT_SUBCOMPACTS_SIZE;
            m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
            tmpInt = DEFAULT_SUBCOMPACTS_SIZE;
        }
        /** ���������, ��� ����������� � �������� ���������� � ��������� ������� ����� ������� � �������� �������
        */
        if( (tmpInt*tmpTotalOfThreads) > m_puc->getMaxRelativeSizeAlongAxis())
        {
            ss.clear();
            ss.str(std::string());
            ss << " '" << tmpInt << "'" << " - wrong subcompacts' size! " << " We will get issue with subcompact cloning!";
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            delete params;
            params = nullptr;
            m_estimatorParams = nullptr;
            return _ERROR_WRONG_ARGUMENT_;
        }
        params->setParam(indexSubcompactSize, tmpInt);
    }
///END Maria's code

    m_estimatorParams = params;

    return _RC_SUCCESS_;
}///end parseAndCreateEstimatorParams()
/** generate DataInfo */
/*********************************************************
* make DataInfo Array to retrun  Estimates
* PARAMS [OUT] dimension -- size of array DataInfo* of pointers
* PARAMS [OUT] di -- result as array DataInfo*
 RETURNs _RC_SUCCESS_ if success
 _ERROR_WRONG_OUTPUT_DATA_,_ERROR_NO_ROOM_,  otherwise
**********************************************************/
int PF_PointEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** &di) const
{
    if ((nullptr == m_setOfParamsUnionEstimates) || (nullptr == m_setOfLogLikelihoods))
    {
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
    size_t size = m_setOfParamsUnionEstimates->getSetSize();
    if (!size)
    {
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
///TODO!!! We MUST return k-best estimates !
    /** Meantime we put the VERY LAST ParamsUnion (which has been stored in m_setOfParamsUnionEstimates)
    as final estimate to be returned from this estimator!
    */
    IParamsUnion * pu = m_setOfParamsUnionEstimates->getParams(size - 1);
    if (nullptr == pu)
    {
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
    size_t totalOfItems = pu->getTotalOfItemsInParamsUnion();
    if (!totalOfItems)
    {
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
///TODO!!! ��� ����� �������� ��� ������� ��� DataInfo � ������� LogLikelihood!
    dimension = totalOfItems + 1;
    /**
    we put each Params of final ParamsUnion
    into separate DataInfo instance
    */
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** loop to prepare DI for Estimates ONLY!
    DI for logLikelyhood will be created later!
    */
    for(size_t i = 0; i < totalOfItems; ++i)
    {
        IParams* ptrIParams = pu->ptrIParamsUnionToPtrIParams(i);
        if(nullptr == ptrIParams)
        {
            delete [] di;
            return _ERROR_WRONG_OUTPUT_DATA_;
        }
        size = ptrIParams->getSize();
        /** dimension of m_dataSetSizes,
        m_dataSetTrackNames,
        m_DataSets
        */
        size_t totalOfDataSets = 1;
        /** the size of _trackSizes (the ??? index of single-dimension sets) */
        size_t* di_0_dataSetSizes = new size_t[totalOfDataSets];
        if(!di_0_dataSetSizes)
        {
            delete [] di;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetSizes for estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(di_0_dataSetSizes, 0, totalOfDataSets*sizeof(size_t));
        /** ��� �� ���� ���� �� totalOfDataSets ��������� ��������� DataInfo
        */
        for(size_t j = 0; j < totalOfDataSets; ++j)
        {
            di_0_dataSetSizes[j]=size;
        }

        /** allocate array of ptrs to 1-D sets */
        double** data = new double*[totalOfDataSets];
        if (!data)
        {
            delete [] di_0_dataSetSizes;
            int j = i;
            while(j >= 0)
            {
                delete di[j];
                --j;
            }
            delete [] di;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data, 0, totalOfDataSets*sizeof(double*));
        for(size_t k = 0; k < totalOfDataSets; ++k)
        {
            data[k] = new double[di_0_dataSetSizes[k]];
            if (!data[k])
            {
                int j = k;
                while(j)
                {
                    --j;
                    delete [] data[j];
                }
                delete [] data;
                delete [] di_0_dataSetSizes;
                j = i;
                while(j >= 0)
                {
                    delete di[j];
                    --j;
                }
                delete [] di;

                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
                return _ERROR_NO_ROOM_;
            }
            std::memset(data[k], 0, di_0_dataSetSizes[k]*sizeof(double));
            for(size_t l = 0; l < di_0_dataSetSizes[k]; ++l)
            {
                double tmp = 0;
                ptrIParams->getParam(l, tmp);
                data[k][l] = tmp;
            }
        }
        /** Here we put ptr to data as double const ** const
        to the DataInfo CTOR
        */
        di[i] = new DataInfo(data);
        if (!di[i])
        {
            int j = i;
            while(j >= 0)
            {
                delete di[j];
                --j;
            }
            size_t k = totalOfDataSets;
            while(k)
            {
                --k;
                if(data[k]) delete [] data[k];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo* for estimates");
            return _ERROR_NO_ROOM_;
        }
//OLD        di[i]->setValues(data);
        di[i]->m_dimension = totalOfDataSets;
        /** the name of data source, for example name of source file or name of Estimator */
        di[i]->m_dataCreatorName = pointEstimatorNames[PointEstimatorsTypes::PF_AdditiveModel_PointEstimator];

        enum IModel::ModelsTypes modelType = IModel::FAKE_MODEL;
        m_additiveModel->getAddendType(i,modelType);
        di[i]->m_dataModelName = IModel::modelNames[modelType];
        /** the name of data set, for example regression location or regression coefficients
        */
        IModel * addend = nullptr;
        m_additiveModel->getPtrToAddend(i, addend);
        if(nullptr == addend)
        {
            int j = i;
            while(j >= 0)
            {
                delete di[j];
                --j;
            }
            int k = totalOfDataSets;
            while(k)
            {
                --k;
                if(data[k]) delete [] data[k];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong addend to fill DataInfo dataset nmae");
            return _ERROR_NO_ROOM_;
        }
        di[i]->m_dataSetName = IParams::paramsNames[addend->getModelParamsType()];

        di[i]->m_dataSetTrackNames = new std::string [di[i]->m_dimension];
        if (! di[i]->m_dataSetTrackNames)
        {
            int j = i;
            while(j >= 0)
            {
                delete di[j];
                --j;
            }
            size_t k = totalOfDataSets;
            while(k)
            {
                --k;
                if(data[k]) delete [] data[k];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetTrackNames");
            return _ERROR_NO_ROOM_;
        }
        for(size_t k = 0; k < totalOfDataSets; ++k)
        {
            di[i]->m_dataSetTrackNames[k]=grammarPointEstimates[grammarSizeForPointEstimates-1];
        }
        /** the size of _trackSizes (the ??? index of single-dimension sets)
        */
        di[i]->m_dataSetSizes = di_0_dataSetSizes;
    }///end for totalOfItems
///TODO!!! MAke the very last di for likelihood
    IParams* ptrIParams = m_setOfLogLikelihoods->getParamsCopy(m_setOfLogLikelihoods->getSetSize() - 1);
    if(nullptr == ptrIParams)
    {
        delete [] di;
        return _ERROR_WRONG_OUTPUT_DATA_;
    }
    size = ptrIParams->getSize();
    /** dimension of m_dataSetSizes,
    m_dataSetTrackNames,
    m_DataSets
    */
    size_t totalOfDataSets = 1;
    /** the size of _trackSizes (the ??? index of single-dimension sets) */
    size_t* di_0_dataSetSizes = new size_t[totalOfDataSets];
    if(!di_0_dataSetSizes)
    {
        int i = dimension - 1;
        while(i >= 0)
        {
            delete di[i];
            --i;
        }
        delete [] di;
        delete ptrIParams;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetSizes for loglikelihood estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, totalOfDataSets*sizeof(size_t));
    /** ��� �� ���� ���� �� totalOfDataSets ��������� ��������� DataInfo
    */
    for(size_t j = 0; j < totalOfDataSets; ++j)
    {
        di_0_dataSetSizes[j]=size;
    }

    /** allocate array of ptrs to 1-D sets */
    double** data = new double*[totalOfDataSets];
    if (!data)
    {
        delete [] di_0_dataSetSizes;
        int i = dimension - 1;
        while(i >= 0)
        {
            delete di[i];
            --i;
        }
        delete ptrIParams;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return loglikelihood estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, totalOfDataSets*sizeof(double*));
    for(size_t k = 0; k < totalOfDataSets; ++k)
    {
        data[k] = new double[di_0_dataSetSizes[k]];
        if (!data[k])
        {
            while(k)
            {
                --k;
                delete [] data[k];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            int i = dimension - 1;
            while(i >= 0)
            {
                delete di[i];
                --i;
            }
            delete [] di;
            delete ptrIParams;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return loglikelihood estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[k], 0, di_0_dataSetSizes[k]*sizeof(double));
        for(size_t l = 0; l < di_0_dataSetSizes[k]; ++l)
        {
            double tmp = 0;
            ptrIParams->getParam(l, tmp);
            data[k][l] = tmp;
        }
    }///end of loop over all datasets
    /** Here we put ptr to data as double const ** const
    to the DataInfo CTOR
    */
    di[dimension - 1] = new DataInfo(data);
    if (!di[dimension - 1])
    {
        size_t k = totalOfDataSets;
        while(k)
        {
            --k;
            if(data[k]) delete [] data[k];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;

        int i = dimension - 1;
        while(i >= 0)
        {
            delete di[i];
            --i;
        }
        delete [] di;
        delete ptrIParams;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo* for loglikelihood estimates");
        return _ERROR_NO_ROOM_;
    }
//OLD	di[dimension - 1]->setValues(data);
    di[dimension - 1]->m_dimension = totalOfDataSets;
    /** the name of data source, for example name of source file or name of Estimator */
    di[dimension - 1]->m_dataCreatorName = pointEstimatorNames[PointEstimatorsTypes::PF_AdditiveModel_PointEstimator];

    enum IModel::ModelsTypes modelType = IModel::FAKE_MODEL;
    m_additiveModel->getAddendType( I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_noiseModelIndex,modelType);

    di[dimension - 1]->m_dataModelName = IModel::modelNames[modelType];
    /** the name of data set, for example regression location or regression coefficients
    */
///TODO!!! HARD-CODE!
    di[dimension - 1]->m_dataSetName = "LogLikelihoodEstimates";

    di[dimension - 1]->m_dataSetTrackNames = new std::string [di[dimension - 1]->m_dimension];
    if (! di[dimension - 1]->m_dataSetTrackNames)
    {
        size_t k = totalOfDataSets;
        while(k)
        {
            --k;
            if(data[k]) delete [] data[k];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        int i = dimension - 1;
        while(i >= 0)
        {
            delete di[i];
            --i;
        }
        delete [] di;
        delete ptrIParams;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }
    for(size_t k = 0; k < totalOfDataSets; ++k)
    {
        di[dimension - 1]->m_dataSetTrackNames[k]=grammarPointEstimates[grammarSizeForPointEstimates-1];
    }
    /** the size of _trackSizes (the ??? index of single-dimension sets)
    */
    di[dimension - 1]->m_dataSetSizes = di_0_dataSetSizes;
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    delete ptrIParams;
    return _RC_SUCCESS_;
}///end getEstimatesAsDataInfoArray()

/** IPointEstimator stuff */
/*********************************************************
* setEstimatorParams
* PARAMS [IN] dimension -- size of array DataInfo* of pointers
* PARAMS [IN] ptrToPtrDataInfo -- array DataInfo*
 RETURNs _RC_SUCCESS_ if success
 _ERROR_WRONG_OUTPUT_DATA_,_ERROR_NO_ROOM_,  otherwise
**********************************************************/
int PF_PointEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
/*********************************************************
* setEstimatorParams
* PARAMS [IN] params --
 RETURNs _RC_SUCCESS_ if success
  otherwise
**********************************************************/
int PF_PointEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
/*********************************************************
* setModel
* PARAMS [IN] model --
 RETURNs _RC_SUCCESS_ if success
  otherwise
**********************************************************/
int PF_PointEstimator::setModel(IModel* model)
{
///TODO!!! ��� �������, ������� ���� ��� ��������, ������ ��� ������ � estimarePoint �������������� ������ PF_ADDITIVE_MODEL � ����� ����������
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setModel is not implemented yet!");
}
/*********************************************************
* setModel
* PARAMS [IN] additiveModel --
 RETURNs _RC_SUCCESS_ if success
  otherwise
**********************************************************/
int PF_PointEstimator::setModel(IAdditiveModel* additiveModel)
{
    enum AdditiveModelsTypes amt = FAKE_ADDITIVE_MODEL;
    additiveModel->getModelType(amt);
    if(amt == FAKE_ADDITIVE_MODEL)
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's model NOT ADDITIVE!");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** Make sure we deal with I_PF_AdditiveModel! */
    if(nullptr == dynamic_cast<I_PF_AdditiveModel*>(additiveModel))
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Estimator's model does NOT implement PF_ADDITIVE_MODEL interface!");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_additiveModel = additiveModel;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    setParamsUnionSet();
    setParamsSet();

    return _RC_SUCCESS_;
}
/*********************************************************
* setParamsSet
 RETURNs _RC_SUCCESS_ if success
  otherwise
**********************************************************/
int PF_PointEstimator::setParamsSet()
{
    /**
    � ���� ���������� ����� �������� ������ ��������� �������������,
    ��� ���������� � ���� Params1D ����������� �������.
    ������� ��� ����� ���������� �� ����� ������ ���������� ������,
    �.�. ��� ����� ���������� ������������ �������� ����������� PLAIN_MODEL
    (������ ����������������� ������)
    � � ��������� ��� ������� ������ ������ ��������� �� ��� �� ����� ������,
    �.�. ������� ��������� Params1D, ����������� � ���� ���,
    �� �����!
    */
    const IModel* pstModel = nullptr;
    IParamsSet * pst = IParamsSet::createEmptyParamsSet(pstModel, m_logger);
    /** This way at least so far. */
    if (pst)
    {
        m_setOfLogLikelihoods = pst;
    }
    return _RC_SUCCESS_;
}
/*********************************************************
* setParamsUnionSet
 RETURNs _RC_SUCCESS_ if success
  otherwise
**********************************************************/
int PF_PointEstimator::setParamsUnionSet()
{
    IParamsUnionSet* pust = IParamsUnionSet::createEmptyParamsUnionSet((const IAdditiveModel*) m_additiveModel, m_logger);
    if(nullptr == pust)
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "Can not create ParamsUnion set to keep estimates of PF_ADDITIVE_MODEL : " + m_additiveModel->toStringAllAddends());
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** This way at least so far. */
    m_setOfParamsUnionEstimates = pust;
    return _ERROR_WRONG_ARGUMENT_;
}

/**************************************************************************************************************************
*   ORIGINAL IMPLEMENTATION Single Thread PointEstimators::PF_PointEstimator::estimatePoint
*   Main method TODO!!!
**************************************************************************************************************************/
int PF_PointEstimator::estimatePoint_ST(ptrInt threadInterruptSignal)
{
    if ((!m_puc) || (!m_setOfLogLikelihoods) || (!m_setOfParamsUnionEstimates))
    {
        return _ERROR_WRONG_WORKFLOW_;
    }
    std::stringstream sstr;
    /** ���� �� �������������� (�� ����� � ��� �������) ���������������� private-������� (_id) ��������,
        �� ����� �������� �������������� ���������� ���������� ������ ����� ����������
    */
    /** ���������� items �� IParamsUnion* -> I_PF_AdditiveModelParams* ����� �������
    ������ � ������� ��������� ������� I_PF_AdditiveModel-������ : getPtrToNoiseParams, getPtrToParticleGeneratingParams
    */

///TODO!!! ���� ��������! �.�. getParamsUnionCopy(m_puc->getCurrentId()); �� ������ _id, �� ����������� ����� getNextPtr()
///��-������, ������ ��������� �� _currentParamsUnion, � �� ��� �����!, �.�. �������, ��� ����� ������ ��������� PF_params ����� ������ ����������!
///� ��� ��������� ��� ������!
/// �.�. ����������, ��� ����� ����� ������� ����  getNextPtr()! �� �������������� ������ m_puc->setCurrentId(--requiredId)!
/// TODO int rc = m_puc->setCurrentId(--requiredId); IParamsUnion* PF_params = m_puc->getNextPtr(); <- �� � ���� ������ �� ������ ������ �� ����� ���������!
///    IParamsUnion* PF_params = m_puc->getParamsUnionCopy(m_puc->getCurrentId());
    /** Following stupid example! use getFirts() if requiredId==0
     int rc = m_puc->setCurrentId(0);
     IParamsUnion*  PF_params = m_puc->getNextPtr();/// <- �� � ���� ������ �� ������ ������ �� ����� ���������!
    */
    IParamsUnion*  PF_params = m_puc->getFirstPtr();/// <- �� � ���� ������ �� ������ ������ �� ����� ���������!
//DEBUG std::cout << PF_params->toString() << std::endl;

    if(nullptr == PF_params)
    {
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** ��������� ������� �������� ����� � PF_params ��� � ������� getMext()! */
    /** get the size of sample trajectory (s.p. trajectory)
    */
    size_t size = m_data.size();
    /** get numParticles from EstimatorParams (they have been already retrieved from EstimatorParams.cfg)
    */
    size_t numParticles = 0;
    int rc = getNumberOfParticlesFromEstimatorParams(numParticles);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
    std::vector<double> particles(numParticles);
    /** container to keep
     the best logLikelihood (!singular)
       for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE
         OR
     the AVERAGE of counterBestStatisticsFound number of the best logLikelihoods
       for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE
      */
    std::vector<double> statistics(size, -DBL_MAX);
    /** container to keep logLikelihood for all the points of s.p.trajectory given current vector of Model Params
    */
    std::vector<double> tmp_statistics(size, -DBL_MAX);
    /** this flag means "We have found AT LEAST ONE candidate for OPTIMUM into Model Params Compact"
    */
    bool isStatisticsInitialized = false;
    /**  counter of the best Likelihoods found so far */
    unsigned int counterBestStatisticsFound = 0;
    /***/
    double logLikelihood=-DBL_MAX;
    size_t sizeOfLikelihoodEstimateAsParams = 1;
    IParams * logLikelihoodAsParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, sizeOfLikelihoodEstimateAsParams);
    if (!logLikelihoodAsParams)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to keep Estimator's loglikelihood estimate");
        return _ERROR_NO_ROOM_;
    }

    /** LogLikelihood for current vector of Model Params */
    double hypLogLikelihood;
    /** s.p.trajectory argument (time or point index) */
    unsigned t = 0;

    I_PF_AdditiveModel* model = dynamic_cast<I_PF_AdditiveModel*>(m_additiveModel);

    sstr.clear(); sstr.str(std::string());
#ifdef _DEBUG_
size_t counter = 0;
#endif
    /** Loop through the set of Model Params into Model Params Compact */
    while (!m_puc->isEnd())
    {
#ifdef _DEBUG_
++counter;
#endif
        /** initialize with "almost zero" to get LogLikelihood for current Model Params */
        hypLogLikelihood = -DBL_MIN;
        /** ����� ��� ��� t = 0 ����, �� ������� ����� �� ����� ����������, �.�.
        ��� t = 0 �� ������ ������ ������������������� particles!
        � � ������������ �������� �� � ���� ����� ��� ����� �.�.
TODO!!! If we have NOT ESTIMATED Wiener's initial condition,
We MUST START from t = 0
        */
        model->initParticles(I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_particleGeneratingModelIndex, PF_params, particles);
        /** Loop through points of s.p. trajectory
        to estimate the probability (LogLikelihood) of current Model Params */
        for (t = 1; t < size; t++)
        {
            /** 1 -> index Of ParticleGeneratorParams */
            model->generateParticles(I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_particleGeneratingModelIndex, PF_params, particles);
            /** estimate (LogLikelihood) of current Model Params */
            /** 0 -> index Of NoiseParams */
            /** for Wiener we CAN NOT estimate Likelihood at t = 1
            BECAUSE we have defined initial condition at that point!
            So DON'T PASS t = 0 to Wiener_PDF! Skip the initial point!
            */
            hypLogLikelihood += model->estimateHypothesis( I_PF_AdditiveModel::indexesOfAddendsPF_AdditiveModel::_noiseModelIndex, PF_params, m_data[t], particles, t);
#ifdef _DEBUG_
///TODO!!! ��-�������� � �������� ������ ������� �������� ���� ����� � ��������� �������� ����������,
/// �� ���� ������ �� ������ ����� ����� �������!!!
//            sstr.clear(); sstr.str(std::string());
//            sstr << "\tcurrent hypothesis LogLikelihood:\t" << hypLogLikelihood;
//            m_logger->log(ALGORITHM_7_LEVEL_INFO, sstr.str());
#endif
            /** Trying to reject current Model Params
             as non-optimal ones
             */
///TODO!!! ��������� ������ �� ��������� ��������� ���������� ���������������� �������� ���������� ������
/// � ���� hypLogLikelihood = -DBL_MIN
            /** ������ �������� ��� �� ������ ����� ���������,
             ���� ������ hypLogLikelihood � ���� ����� ��� �������� ������� ����������
             ��������� ���� "hypLogLikelihood ������� �� ����������� � ���� ����� �� ��������� ������������ � ����� ������� �������� ����������"
            ����� ������� �������������� �� 3-4 * 10^6 ����� � ������������ ����������, �� ������ ���� ����������� �����,
            �.�. ��� ����������� ������������ ��������� ����� � ������ ����� ���������� ��������� �������� ��������� ��������� ����� ��������� ��������!

            ����� ���� ������� ������� (statistics[size-1] > -DBL_MAX)
            ��� ������������, ��� � ����� ������� �� ��� ���� �� ���� ��� ������ ��� ���������� �� �����
            � ��� ������, ��� ���� � ��������� ����� ���������� hypLogLikelihood �������� �������� (>-LDBL_MAX)
            ����� ������� �������������� �� 6 * 10^3 ����� � ������������ ����������, � ����� �� �� ������ ���� ����������� �����!

            ������ ������������ �������: ������ ����� ���������, ��� ��� ���� �� ���� ��� ������ ���� ���� �� �����!
            ( ����������� ������� "������ statistics ��� �������� �� �����")
            � ����� ���������, ��� ������ hypLogLikelihood � ������� ����� ��� �������� ������� ����������
            ����������� ������� �� ���� "hypLogLikelihood ������ �� ����������� � ���� ����� �� ��������� ������������ � ����� ������� �������� ����������"
            ����� ������� �������������� �� 11 * 10^3 ����� � ������������ ����������, � ����� �� �� ������ ���� ����������� �����!
            */
            if ( isStatisticsInitialized && (!passStatistics(t, hypLogLikelihood, tmp_statistics)))
            {
                tmp_statistics = statistics;
/// �� ���� ������ �� ������ ����� ����� �������!!!
                sstr.clear(); sstr.str(std::string());
                sstr << std::endl <<" paramsSetId = " << m_puc->getCurrentId()
                << ", hypothesis = {" << std::endl << PF_params->toString()
                << std::endl << "}, hypLogLikelihood = " << hypLogLikelihood
                << std::endl << "Failed statistics at point t = " << t;
                m_logger->log(ALGORITHM_6_LEVEL_INFO, sstr.str());
                sstr.clear(); sstr.str(std::string());
///TODO!!! �������� ����������� ��������� �������� ����� � ������� ������� ����������!
/// ������ �������� �� �������� �����, ����� �� �������� ������� ������ � ���������� �������������� ������ ����� ???
                break;
            }
            /** Let's check if current thread must end! */
            /** Removed TerminateThreads() from code.
            Wrong! since platform-specific: if (WaitForSingleObject( (ptrVoid)(*event), 1) == WAIT_TIMEOUT) */
            if (threadInterruptSignal)
            {
                if ( *(threadInterruptSignal) != 0x1 )
                {
                    continue;
                }
                else
                {
                    *threadInterruptSignal = 0x0;
                    delete logLikelihoodAsParams;
                    logLikelihoodAsParams = nullptr;
                    return THREAD_EXECUTION_INTERRUPTED;
                }
            }

        } /** End of Loop through all the points of s.p. trajectory to estimate the probability of current Model Params */

        /** if we terminated for-loop because current model params are far from optimum
        Move to the next point in params space */
        if (t < size)
        {
///TODO!!! ��� ����� ����� ���� ��, �������������� ������ ����������� ������� ������������� � ��� �����������, � ������� ������, ������� �� �� ��������� ���, � �� ��������� �������, ������ �������� ����� ���� ����� ������������ ������� (� �������) ���������� ������� ������������� � ���� �����������
/// �� �������� ���������� ������� ��� ��� ��������� ������ ����������� ������� �������������, ��������� �������� �� ������� �� ����� ������ �� ���� ����������� ��� ���������� � ���� ������ ����������!

///�� �������! ��� ��������� �� ���� ������ IParamsUnionCompact            delete PF_params; PF_params = nullptr;
            PF_params = m_puc->getNextPtr();
            continue;
        }
/// �� ���� ������ �� ������ ����� ����� �������!!!
        sstr.clear(); sstr.str(std::string());
///TODO!!! ����� ������������� ����� ������, �.�. ��� ������, ������� ����������� ������ ��� ������� � �������� ��������� ���������, �� � ������ ��� �� �����!
        sstr << std::endl << " paramsSetId = " << m_puc->getCurrentId() <<
        ", hypothesis = {" << std::endl << PF_params->toString()
        << std::endl << "}, current LogLikelihood = " << hypLogLikelihood << ", best current LogLikelihood = " << logLikelihood;
        /** if likelihood of current model params (just processed) is more then previous the best one
        we have to SAVE NEW the BEST LIKELIHOOD of current model params
        AND the vector "statistics" obtained for current model params */
//std::cout <<'\t' << hypLogLikelihood << '\t' << logLikelihood << std::endl;
        if (hypLogLikelihood > logLikelihood)
        {
            /** OLD! trivial implementation as REPLACE_MODE: statistics = tmp_statistics; */
///TODO!!! ������ ��� ��� ���������, ���������� �� ������������� ����������� statistics, ����������, ����� ������ ������� �����
            evaluateNewStatistics(AVERAGE_MODE, counterBestStatisticsFound, statistics, tmp_statistics);
            counterBestStatisticsFound++;
            isStatisticsInitialized = true;
            /**
            ����� ��������� ������� ������ ������ ���������� ������
            ���������� ������ m_setOfParamsUnionEstimates
             ����������� ������� PF_params
             � ���� ����������� IParamsUnionCompact*
              (PF_params - ��������� �� ���������� ���� � IParamsUnionCompact)
            */

            /** ������ ����� �������� ��-nullptr bestEstimate � _setOfParamsEstimates.
            � � ����� m_setOfParamsUnionEstimates ����������� ������ ���������� �-��������� ������ ������
            */
            /** current best estimate
            will be very LAST ParamsUnion which we have put into m_setOfParamsUnionEstimates,
            i.e. the ParamsUnion which has id = size of m_setOfParamsUnionEstimates - 1
            */
            bool isSaved = m_setOfParamsUnionEstimates->addParamsUnionCopy(m_additiveModel, PF_params, m_logger);

            logLikelihood = hypLogLikelihood;
            /**
            ����� ��������� ������� ������ ������ ��������� �������������,
            �� ����� ����������� �������logLikelihoodAsParams!
            ���������� ������ ����� ������ ��������� ������������� �
            ������������ ��������� logLikelihoodAsParams
            � ����� ��� ��������� ����� ���������� � m_setOfLogLikelihoods->addParamsCopyToSet
            */
            /** size-1 > 0, �.�. size >= DEFAULT_MIN_SIZE_OF_DATA_TO_BE_PROCESSED */
            logLikelihoodAsParams->setParam(0, logLikelihood/(size-1));
            isSaved = m_setOfLogLikelihoods->addParamsCopyToSet(nullptr, logLikelihoodAsParams, m_logger);
///TODO!!! ����� �� ������ �������� �������� ������������������ if(!isSaved)
            m_logger->log(ALGORITHM_ESTIMATOR_NOTIFICATION, "Get new maximum");
            m_logger->log(ALGORITHM_ESTIMATOR_NOTIFICATION, sstr.str());
        }
        else
        {
            tmp_statistics = statistics;
            m_logger->log(ALGORITHM_7_LEVEL_INFO, sstr.str());
        }
///TODO!!! ��� ����� ����� ���� ��, �������������� ������ ����������� ������� ������������� � ��� �����������, � ������� ������, ������� �� �� ��������� ���, � �� ��������� �������, ������ �������� ����� ���� ����� ������������ ������� (� �������) ���������� ������� ������������� � ���� �����������
/// �� �������� ���������� ������� ��� ��� ��������� ������ ����������� ������� �������������, ��������� �������� �� ������� �� ����� ������ �� ���� ����������� ��� ���������� � ���� ������ ����������!
///�� �������! ��� ��������� �� ���� ������ IParamsUnionCompact        delete PF_params; PF_params = nullptr;
        PF_params = m_puc->getNextPtr();
        sstr.clear(); sstr.str(std::string());
    }
#ifdef _DEBUG_
std::cout << "total of points:" << counter << std::endl;
#endif
///�� �������! ��� ��������� �� ���� ������ IParamsUnionCompact : delete PF_params; PF_params = nullptr;
    delete logLikelihoodAsParams;
    logLikelihoodAsParams = nullptr;
    return THREAD_EXECUTION_ACCOMPLISHED;
}///end PF_PointEstimator::estimatePoint_ST()

/**************************************************************************************************************************
*   Main method for multi-thread mode
* estimatePoint_MT(ptrInt threadInterruptSignal)
* PARAM [IN|OUT] threadInterruptSignal -- int to exchange information on estimator state
* returns _RC_SUCCESS_ if success
 or _ERROR_WRONG_WORKFLOW_,
**************************************************************************************************************************/
int PF_PointEstimator::estimatePoint_MT(ptrInt threadInterruptSignal)
{
    if ((!m_puc) || (!m_setOfLogLikelihoods) || (!m_setOfParamsUnionEstimates))
    {
        return _ERROR_WRONG_WORKFLOW_;
    }
    std::stringstream sstr;
    /** ���� �� �������������� (�� ����� � ��� �������) ���������������� private-������� (_id) ��������,
        �� ����� �������� �������������� ���������� ���������� ������ ����� ����������
    */
    /** ���������� items �� IParamsUnion* -> I_PF_AdditiveModelParams* ����� �������
    ������ � ������� ��������� ������� I_PF_AdditiveModel-������ : getPtrToNoiseParams, getPtrToParticleGeneratingParams
    */
    size_t cntThreads = 0;
    int rc = getNumberOfThreadsFromEstimatorParams(cntThreads);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
//DEBUG
    std::cout << cntThreads << " - cntThreads";
//END DEBUG
    m_compactEstimatesData.resize(cntThreads);
    m_timeTrajectory.resize(cntThreads);
    m_bestLogLikelihood.resize(cntThreads);
    for(size_t i = 0; i < cntThreads; ++i)
    {
        m_bestLogLikelihood[i] = NAN;
    }

    /** ����������� ��������� ����� */
    std::vector<IParamsUnion*> startPoints(cntThreads);

    rc = makeStartPoints(m_puc, cntThreads, startPoints);

    if(_RC_SUCCESS_ != rc){
        for(size_t i = 0; i < cntThreads; ++i)
        {
            if(startPoints.at(i))
                delete startPoints.at(i);
        }
        return rc;
    }

///TODO!!! ����� ������� ������������
///���� ����� ������� "������ ��������", ������� ����� ����������� ��-��� ���������, ������ ��������,
///� ���������� �������� (�.�. double), � �� � ������������� (�.�. size_t),
/// �� �� ���������, ��� ����� ������� ��������� �� ������� (� � ��������� ���������� ������ ������� �����, ���� �� � ������������� ��������)
///��������, � ���������, ������ �������� ��� ���������� (��� ����� �� ������ �� ���������) ������ 1.0,
///� ��� �� ����� ������� � �������� ����� � �������� � compactSize = 0.05?
/// ����� ���� ������ ������ ����������� ������������ ����� ������ => ���������� �� ��������� ����� ��
/// ����� �������� ����� ����� ������������ ����� ����� �� ��������� �� ��������������� ������������ ���.
/// (��� ������������ ���� ����� ��������� �� ��������� ����� �� �� "������", � �� "������"*"���������")
///IAA: ������� �������, ���� ������ �������� �������� size_t, �� ��� ������������� ����,
/// �.�. ���������� ����� ����� (== ������ ����� ������������� �����!)
/// �������, ��� �������, � ������� ����� �������� ������ � ���������� �������� (double), �� ���� ������������� �� �����
    unsigned short compactsSize = 0;
    rc = getSubcompactsSizeFromEstimatorParams(compactsSize);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
    // ���������� ����� �������� �� m_puc
///TODO!!!    findCompactsSize(m_puc, startPoints, cntThreads,/*increments,*/ &compactsSize);
//DEBUG
    std::cout << startPoints.size() << " - cnt of Start points"<<std::endl;
//END DEBUG
    /** ������������ ������������ � ���������� �������� ��� �������� ���������� ������� �������
    */
//OLD    std::vector<IParamsUnionCompact*> compacts(cntThreads);
    std::vector<tArgsThread> argsForThreads(cntThreads);

    for(size_t i = 0; i < cntThreads; ++i)
    {
        IParamsUnionCompact *currCompact = nullptr;
        rc = cloneSubCompact(m_puc, compactsSize, startPoints.at(i), &currCompact);
//DEBUG std::cout << currCompact->getSize() << std::endl;
        if(rc != _RC_SUCCESS_)
        {
            size_t j;
            for(j = 0; j < cntThreads; ++j)
            {
                if(startPoints.at(j))
                    delete startPoints.at(j);
            }
            return rc;
        }
        argsForThreads[i].statistics.resize(m_data.size(), -DBL_MAX);
        argsForThreads[i].tmp_statistics.resize(m_data.size(), -DBL_MAX);
        argsForThreads[i].setPUC(currCompact);
//DEBUG
std::cout << argsForThreads[i].getPtrPUC()->getSize() << " - currCompactSize\n";
//std::cout << argsThreadMethod.getPtrPUC()->getFirstPtr()->toString() << std::endl;
//END DEBUG
        /** We're  anticipating Success! */
        argsForThreads[i].rc = _RC_SUCCESS_;
        argsForThreads[i].threadIndex = i;
    }
    std::vector<std::thread> threads(cntThreads);
/*** TODO!!! ��� � ���� ��������� ����������
  tArgsThread argsThreadMethod;
 ���� ����!
��-������, ��� ��������� �� �����, � ��� ����� ��������� �� ��������,
� ����� ��������� argsThreadMethod ���������� �� ������, ���������� ���������,
������� ��������� �������, ������� ������ ���, �� ��������� paramsCompact
����� �������, ������� ����� ������,
 �� ��������� �� ���� ��-�������� �������� ������ ������� compacts
 � ����� ������� ���������� �� compacts[i]�������� �� ����� ����������!
 ��� �� ����� ����, � �����, ��� ������ ������� ������� compacts.
*/
//OLD    tArgsThread argsThreadMethod;
    for(size_t i = 0; i < cntThreads; ++i)
    {
        m_bestLogLikelihood[i] = -DBL_MAX;
        threads.at(i) = std::thread(&PF_PointEstimator::estimateLogLikelihoodOfParamsUnionCompact, this, std::ref(argsForThreads.at(i)));
    }
    /** "���������" �� ������, ������� ������ ��� "������",
    ����� ��������� �� ����������
    */
    for(size_t i = 0; i < cntThreads; ++i)
    {
        threads.at(i).join();
    }

    bool isSaved = 0;
///TODO!!! Check if double is enough to keep id of params from compact
    double idLogLikelihoodToChange;
    double maxDiffer = DBL_MAX;
    std::vector<tPointEstimatesData> currEstimates;
    std::vector<IParamsUnion*> paramsUnionEstimates;
//OLD    std::vector<IParams*> logLikelihoods;
    std::vector<double> logLikelihoods;
    std::cout << m_compactEstimatesData.size() << " - m_compactEstimatesData.size()"<<std::endl;
    for(size_t i = 0; i < cntThreads; ++i)
    {
///TODO!!! ��� ����� ������� ������� ��������� argsForThreads.at(i).rc
        currEstimates = m_compactEstimatesData[i];
//DEBUG
        std::cout << currEstimates.size() << " - currEstimates.size()"<<std::endl;
//END DEBUG
        for(size_t j = 0; j < currEstimates.size(); ++j)
        {
            if(! currEstimates[j].isNeedToConsider)
            {
                continue;
            }
            /*** ���� ����� � ����������� ��� ����
            �� ��������� id � logLikelihood ���� �����,
            �������������� �� �������
            */
            if(logLikelihoods.size() < SIZE_OF_SET_OF_LOGLIKELIHOODS)
            {
//OLD(PUSH_BACK)                paramsUnionEstimates.push_back(m_puc->getParamsUnionCopy(currEstimates[j].id));
//OLD(PUSH_BACK)                logLikelihoods.push_back(currEstimates[j].logLikelihood);
                try
                {
                    paramsUnionEstimates.emplace_back(m_puc->getParamsUnionCopy(currEstimates[j].id));
                    logLikelihoods.emplace_back(currEstimates[j].logLikelihood);
                }
                catch (const std::bad_alloc&)
                {
                    std::cout << "bad_alloc in estimatePoint_MT "<< std::endl;
                    break;
                }
            }
            /** ���� �� ����� � ����������� ��� ���,
            �� ���������, ��� � ������� �������������� currEstimates[j] ��� logLikelihood ����� ��������� ����� ������ currEstimates[j].logLikelihood � ���� currEstimates[...], �������� �� ����� ��������� ��� ������� ���� ��������� logLikelihoods
            */
            else
            {
                idLogLikelihoodToChange = -1;
                maxDiffer = 0.f;
//OLD                    currEstimates[j].logLikelihood->getParam(0, valFromEstimatesData);
                //OLD valFromEstimatesData = currEstimates[j].logLikelihood;
                for(int k = 0; k < SIZE_OF_SET_OF_LOGLIKELIHOODS; ++k)
                {
//OLD                        logLikelihoods.at(k)->getParam(0, valFromSet);
//OLD                        valFromSet = logLikelihoods[k];
///TODO!!! � ��� �� ����� ��������� � � ����� �������?
                    if((currEstimates[j].logLikelihood - logLikelihoods[k]) > maxDiffer)
                    {
                        maxDiffer = currEstimates[j].logLikelihood - logLikelihoods[k];
                        idLogLikelihoodToChange = k;
//DEBUG std::cout << maxDiffer  << " " << idLogLikelihoodToChange << " - id\n";
                    }
                }
                if (idLogLikelihoodToChange >= 0)
                {
                    // ������
                    logLikelihoods.at(static_cast<size_t>(idLogLikelihoodToChange)) = currEstimates[j].logLikelihood;
                    // m_setOfLogLikelihoods[idLogLikelihoodToChange] = currEstimates[j].logLikelihood;
					delete paramsUnionEstimates.at((size_t)idLogLikelihoodToChange);
                    paramsUnionEstimates.at((size_t)idLogLikelihoodToChange) = m_puc->getParamsUnionCopy(currEstimates[j].id);
                        // m_setOfParamsUnionEstimates[idLogLikelihoodToChange] = m_puc->getParamsUnionCopy(currEstimates[j].id);
                }
            }///end else try to replace items in containers
        }/// end for j
    }

std::cout << logLikelihoods.size() << " - logLikelihoods.size() ; "<<  paramsUnionEstimates.size() << " - paramsUnionEstimates.size()"<< std::endl;
///IAA: TODO!!! ���� �� ���� �� ��� ���������� ������������ ����� (������) ��������� �������� �������� ������ ��������� �������������.
/// � ��� �� ����� ��������� ������ ��������� �������������, ���������� ��� ������� �����������?
/// ����� ������� ������� � ������������� ����������� ����������� ��������?
/// �� ��� ���, ���� �� �� ������������ � ����������� ������������ �������� � ��������,
/// ��� �������� ��������� ���� ��� ���������� ������ ��������� �������������, ���� ��� ���������� �� ���������� (��� ��������� ������� ��������).
/// � ��� ����� �� ����� ����������� ������� ������-�� ��������
///"�������������� ��������" ��� �����������, ��� ����������� ������ ��������� ������������� � �������� ����� ��������,
///����� �������� ��������� �������� ������ ��������� ������������� ����� ��������.

///TODO!!! clean up all vectors!
    for(size_t i = 0; i < cntThreads; ++i)
    {
        if(startPoints.at(i))
            delete startPoints.at(i);
    }
std::cout << " startPoints deleted;"<< std::endl;
    IParams* copyOfLH = IParams::createEmptyParams(nullptr, IParams::ParamsTypes::Params1D, 1);
    if(!copyOfLH)
    {
        return _ERROR_NO_ROOM_;
    }
std::cout << " copyOfLH allocated;"<< std::endl;
    for(size_t i = 0, size = paramsUnionEstimates.size(); i < size; ++i)
    {
///TODO!!! BUG! �� ����, ��� ��� SIZE_OF_SET_OF_LOGLIKELIHOODS ��������� �������� paramsUnionEstimates!
/// � �� ��������� �������� � ��� �������� � ��������� 0 < i < SIZE_OF_SET_OF_LOGLIKELIHOODS
        isSaved = m_setOfParamsUnionEstimates->addParamsUnionCopy(m_additiveModel, paramsUnionEstimates.at(i), m_logger);
std::cout << paramsUnionEstimates.at(i)->toString() << std::endl;
        delete paramsUnionEstimates.at(i);
        copyOfLH->setParam(0, logLikelihoods[i]);
        isSaved = m_setOfLogLikelihoods->addParamsCopyToSet(nullptr, copyOfLH, m_logger);
std::cout << copyOfLH->toString() << std::endl;
    }
    delete copyOfLH;
std::cout << " copyOfLH allocated;"<< std::endl;
    return THREAD_EXECUTION_ACCOMPLISHED;
}///end estimatePoint_MT()

int PF_PointEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}
/*********************************************************
* setData
* PARAMS [IN] dimension -- size of array DataInfo* of pointers
* PARAMS [IN] ptrToPtrDataInfo -- array DataInfo*
 RETURNs _RC_SUCCESS_ if success
 _ERROR_WRONG_INPUT_DATA_ otherwise
**********************************************************/
int PF_PointEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension < 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];

    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong dimension of DI array");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == nullptr) || (di->m_dataSet[0] == nullptr))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
///TODO!!! ���������, ��� � ������ ������ ���������� ������ ��� ������ ���������� ������! �.�. ����� �����-�� ��������� ��������� (������� �� ����� ����������, ���������� ������), ����� ������� ������� � �������������� ����������� ������� ��������� ���������� ������
    if(DEFAULT_MIN_SIZE_OF_DATA_TO_BE_PROCESSED > di->m_dataSetSizes[0])
    {
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);
    for(size_t i = 0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
#ifdef _DEBUG_
//std::cout << m_data[i] << std::endl;
#endif
    }
#ifdef _DEBUG_
//std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}
/**************************************************************************************************************************
* Entry point to start estimation :
  Single Thread OR Multi-Threads
*   Main method TODO!!!
**************************************************************************************************************************/
int PF_PointEstimator::estimatePoint(ptrInt threadInterruptSignal)
{
   PF_PointEstimatorModes mode = FakeIndex_PF_PointEstimatorMode;
   int rc = getPF_EstimatorMode(mode);
   if(_RC_SUCCESS_ != rc){
       return rc;
   }
   switch(mode)
   {
    case INDEX_ST:
        return estimatePoint_ST(threadInterruptSignal);
    break;
    case INDEX_MT:
        return estimatePoint_MT(threadInterruptSignal);
    break;
    default: // FakeIndex_PF_PointEstimatorMode
#ifdef _BOUNDARIES_TEST_ /** ��� ����,�������� ����� ������ ����� ����������,
 ����� ������ ������ �������, �� �� ����������, ��������� �������� ������, ���� �� �������.
 ���� ����� ����� ��� ����,
 ����� ����������� �������� �������������� ����������� ������������ ������
 ������ 750 ������ ��� ������ <-- seed ��� ������� �.�. ����� ���� � ��� �� ������ ��� ����-������� ���������������� ��������
*/
        Sleep(750);
return _RC_SUCCESS_;
#endif
           return THREAD_EXECUTION_INTERRUPTED;
   }
   return THREAD_EXECUTION_INTERRUPTED;
}


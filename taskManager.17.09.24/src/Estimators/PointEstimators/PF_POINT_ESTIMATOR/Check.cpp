#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <time.h>
#include "Check.h"
#include "../../../Log/errors.h"
/**
Check check(m_data.size(),"estimate.log", param);
size - ������ �����, filename - ��� �����
double * param - ��������� ������ �� ����� wiener_params.PoissonDrvnParetoJumpsExpMeanReverting_params.c**/
Check::Check(size_t sizeOfData, size_t sizeOfParams, std::string logFileName, const IParamsUnion* const etalon_params)
{
///TODO!!! ��� ��� ���� - ������ � ���� ����� � ������������!
    if(!setLogFile(logFileName))
        printf("%s ","ERROR");
    time(&_tmStartTime);
    _sizeOfParams = sizeOfParams;
    _time_est = 0;
///TODO!!! check rc !!! and LOG if error!
    writeSeries(sizeOfData);
    writeModelParamVal(etalon_params);

    writeSeriesModel();
    writeEstimates ();
}

Check::~Check()
{
    _logFile<<std::endl;
    _logFile.close();
}

bool Check:: setLogFile (std::string fileName)
{
    _logFile.open(fileName.c_str(),std::ios_base::app);
    if (!_logFile.is_open()) {
        return false;
    }

    return true;
}

bool Check:: writeSeries(size_t sizeOfData)
{
    std::stringstream str;
    time(&_tmStopTime);
    _time_series = difftime( _tmStopTime,_tmStartTime);


    str << "INSERT INTO series VALUES ("
    << "'$FAKE_ID_SERIES_"<< _time_series << "' , "  ///////M(last ver): << "'$FAKE_ID_SERIES_"<< _time_series << ", "
    << "'model_0.0', 'model_series0, track10', '" //������-�� ��� ������� ����� ��� ������ ����� ����� ��������!!!!
    << sizeOfData //size - ������ ���������� � � ������ EstimatePoint ����, ���� �������� ���� ����������
    <<"', '1', '" //1 � id ������� dsrc
    <<"$FAKE_ID_DS');"<<std::endl;
    _logFile<<str.str();
    _logFile<<std::endl;
    return true;
}


bool Check::writeSeriesModel()
{
    std::stringstream str;
    str.clear();
    str<<"INSERT INTO series_to_model_param_vals VALUES('$FAKE_ID_SERIES_"
    << _time_series
    <<"', '$FAKE_ARG');"<<std::endl;
    _logFile<<str.str();
    _logFile<<std::endl;
    return true;
}

/**
���� ����� ���������� ��� �� ���  writeEstimatesVals �� ������� � � ����� ������ ����������
bool isRejected - ��� ���� isneedtoConsider � ��������� tPointEstimatesData
pointInRejectedTrack - ��� t - ����� ����������, � ������� �� ������� ������� � ������������������ �������
size_t id - id �� ��������� tPointEstimatesData - id ����� � ����� ��������
����� �������� ���������� ���� ��������� tPointEstimatesData - �� ���� ��� �������
*/

bool Check::writeEstInfo(bool isRejected, double pointInRejectedTrack, size_t id, int index)
{
    std::stringstream str;
    str.clear();
    str<<"INSERT INTO estimate_info VALUES ('"
    << id <<"', '" //������ ����� � ��������
    << isRejected <<"', '" // 1 - ���� ���������� - �.�. ������ ����������������
    << pointInRejectedTrack <<"', " ////M(last ver): << pointInRejectedTrack
    << "'$FAKE_ID_SERIES_"<< _time_series <<"');"<<std::endl;
    _logFile<<str.str();
  //  logFile<<std::endl;
    return true;
}

//� ���� ������ �������� ������ ������� ���� ����� ��� ����:������:�������
std::string Check:: getDate()
{
    char buffer[80];
    time_t seconds = time(NULL);
///TODO!!! ���� �� ����� �����?  -  � ������ ����?
///TODO!!! � ������ "��������� ��� �����-������������� �������?" ����� �� ����� ������� ������������� � ���������������� �������.
    tm* timeinfo = localtime(&seconds);
    const char* format = DEFAULT_DATE_TIME_FORMAT;
    strftime(buffer, 80, format, timeinfo);
    return std::string(buffer);
}

//��������� ������� � ������� Estimates � ���������� �� � ���
bool Check:: writeEstimates ()
{
    size_t size = _sizeOfParams + 1;
    auto suffixes = new std::string[size]{
        "WIENERIC",
        "WIENERMU",
        "WIENERSIGMA",
        "POISSONLAMBDA",
        "MEANREVER",
        "PARETOXM",
        "PARETOALPHA",
        "LOG"};
    auto name_est = new std::string[size]{
        "'WienerIC'",
        "'WienerMu'",
        "'WienerSigma'",
        "'PoissonLambda'",
        "'MeanRevertingRate'",
        "'ParetoX_m'",
        "'ParetoAlpha'",
        "'Log'"};
    auto description = new std::string[size]{
        "'IC of Wiener Process'",
        "'Mean of Wiener CDF'",
        "'SD of Wiener CDF'",
        "'Lambda of Poisson CDF'",
        "'lambda_c'",
        "'Shape parameter of Pareto CDF'",
        "'Shift/Scale parameter of Pareto CDF'",
        "'Log'"
        };
///TODO!!! ����� �� ���� ����� ������� ������������� � ���������������� �������.
    localtime(&_tmStopTime);
    time_t dif = difftime(_tmStopTime,_tmStartTime);
    for(size_t i = 0; i < size; ++i)
    {
        std::stringstream str;
        str.clear();
        _time_est = dif;
        str << "INSERT INTO estimates VALUES ("
//OLD        <<"'$FAKE_ID_EST_"<< _time_est <<"', "
        <<"'$FAKE_ID_EST_"<< i <<"', "
        << name_est[i]<<", "
        << description[i]<<", '0', '" //0 �����������
        << getDate()<<"', " //��� ������ ���� ����������
        <<"'$FAKE_ID_DS', '$FAKE_ID_SERIES_"
        <<_time_series << "', '$FAKE_ID_"
        << suffixes[i]<<"');"<<std::endl;
        _logFile<<str.str();
    }
    _logFile<<std::endl;
    delete [] suffixes;
    delete [] name_est;
    delete [] description;
    return true;
}

/** ��������� ������� � ������� EstimatesVal,
 �������� ���� ����� �� ������� ����� �������� ������� ����������������� � ����� ������
 � ������ ���, ����� ����������� ��������� ����� ��������
 PARAM [IN] PF_param --- ������� ����� ��������
 PARAM [IN] arg -- �������� ��������� = ���������� ����� ����� ����������, �� 0 �� ���������� ���������� ������ ��� ������ ���������� ���������� ������ ������
 PARAM [IN] logLikelihood -- ������ ��������� ������������� � ���� ����� �������� ��� ���������� ����� ���������� �������� � arg
 */
bool Check::writeEstimatesVals(IParamsUnion* PF_param, size_t arg, double logLikelihood)
{
    size_t totalOfItems;
    totalOfItems = PF_param->getTotalOfItemsInParamsUnion();
    size_t size=0;
    for(size_t i=0; i<totalOfItems; ++i)
    {
        size += PF_param->ptrIParamsUnionToPtrIParams(i)->getSize();
    }
    if(size != _sizeOfParams)
    {
        return false;
    }
    /** PF_param -> size + logLikelihood -> 1 */
    size = _sizeOfParams + 1;
    auto suffixes = new std::string[size]{
        "WIENERIC",
        "WIENERMU",
        "WIENERSIGMA",
        "POISSONLAMBDA",
        "MEANREVER",
        "PARETOXM",
        "PARETOALPHA",
        "LOG"};
    auto est_val = new double[size];

    int k = 0;

    for (size_t i = 0; i < totalOfItems; ++i)
    {
        size_t totalOfParams = PF_param->ptrIParamsUnionToPtrIParams(i)->getSize();
        for(size_t j=0; j < totalOfParams; ++j)
        {
            double tmpDbl = NAN;
            int rc = PF_param->ptrIParamsUnionToPtrIParams(i)->getParam(j,tmpDbl);
            if(rc)
            {
                delete [] est_val;
                return _ERROR_WRONG_ARGUMENT_;
            }
            est_val[k] = tmpDbl;
            k++;
        }
    }
    /** � ��������� �������� ����� �������������
    */
    est_val[size-1] = logLikelihood;
    /** �� ����� ����� _sizeOfParams+1 �������� ����������
    */
    for(size_t i = 0; i < size; ++i)
    {
        std::stringstream str;
        str.clear();
        str << "INSERT INTO estimate_vals VALUES ('"
        << arg <<"', '"
        << est_val[i]<<"', "
        << "'$FAKE_ID_EST_" << i <<"');"<<std::endl;
        _logFile<<str.str();
    }
    //logFile<<std::endl;
    delete [] est_val;
    delete [] suffixes;
    return true;
}

bool Check:: writeModelParamVal(const IParamsUnion* const etalon_params)
{
    size_t totalOfItems = etalon_params->getTotalOfItemsInParamsUnion();
    size_t size=0;
    for(size_t i=0; i<totalOfItems; ++i)
    {
        size += etalon_params->ptrIParamsUnionToPtrIParams(i)->getSize();
    }
///TODO!!! ���� ���, �� ������ ���� ������ ����� ������ �� ����� ����������, ����� �������� ������������ ������!
    auto suffixes = new std::string[_sizeOfParams]{
        "$FAKE_ID_MOD_WIENERIC",
        "$FAKE_ID_MOD_WIENERMU",
        "$FAKE_ID_MOD_WIENERSIGMA",
        "$FAKE_ID_MOD_LAMBDA",
        "$FAKE_ID_MOD_LAMBDA_C",
        "$FAKE_ID_MOD_PARETOX_M",
        "$FAKE_ID_MOD_PARETOALPHA"
        };
/// ������ ���� ������ ����� ������ �� ����� ����������, ����� �������� ������������ ������!
    auto params = new double[size];

    int k = 0;

    for (size_t i = 0; i < totalOfItems; ++i)
    {
        size_t totalOfParams = etalon_params->ptrIParamsUnionToPtrIParams(i)->getSize();
        for(size_t j=0; j < totalOfParams; ++j)
        {
            double tmpDbl = NAN;
            int rc = etalon_params->ptrIParamsUnionToPtrIParams(i)->getParam(j,tmpDbl);
            if(rc)
            {
                delete [] params;
                delete [] suffixes;
                return _ERROR_WRONG_ARGUMENT_;
            }
            params[k] = tmpDbl;
            k++;
        }
    }
    for(size_t i = 0; i < size; ++i)
    {
        std::stringstream str;
        str.clear();
        str << "INSERT INTO model_param_vals VALUES ("
        << "'$FAKE_ID_MP'"<<", "
        << "'$FAKE_ARG', '"
        << params[i]<<"', '"
        << suffixes[i]<<"');"<<std::endl;
       _logFile<<str.str();
    }
  _logFile<<std::endl;
  delete [] params;
  delete [] suffixes;
  return true;
}

/**
�������� ���� ����� �� ������� - ����� ������������ ��������� ����������
 � ��������� ������ ���������� �� �����������������
 false - ������ ���������� ����������������
 unsigned t -  ������� ����� ����������
 unsigned size - ������ ����������,
 double logLikelihood - ����������� ������ ��������� �������������,
 double bestloglikelihood - ������ �� ������ ������ ������ ��������� ������������� ��� ������ ����� ��������
 � ������ ������������ model, _particleGeneratingModelIndex, PF_params - ������� ���� � ������ estimatePoint
*/
bool Check:: checkFutility (size_t t, size_t size, double logLikelihood,double bestLoglikelihood,
                            IParamsUnion* PF_param, I_PF_AdditiveModel * model,
                            int indexOfParams,std::vector<double> particles,std::vector<double> m_data, double & log)
{

    double maxMode=0;
    log = 0;
///TODO!!! ������ �������� ���� ������ ��� ������ ������������� �������� ������� ���������� ������?
/// �������� ���� ��� 1 ���������� ������, �� ��� �������� ������
/// ��, �����, �� ��������� ���������� ������ ������������ �������� ���������� (�.�.�����? ��� ����� ��������)
/// � ���� �� � ���� ������������� ������������� ������ ����������� �������� ������ �������
/// � ����� ������ ��������� ������� ����� ����� �������������� ����� � �����, ����� ��� ����� ����-��� � ���� ������� (������� ����, ������ � ��� � ���������, ����� ����� ��� ��� �����������)
/// ������ ��� ��� �������. �������, ��� ��� ��� �������� � �������� �������, �� ����������� ���� TODO
/// ���� ��������� �� ���� ��� ������, ��� �� �������� �������� �������!
/// �.�. ��� ����������� �������� � ������� (����� ���� ����������������) : ������ ������ "�������" � �� �������� ������� ���������, ��� ��� "�������"?

    model->getMaxMode(indexOfParams, PF_param, maxMode);

    IParamsUnion *par = PF_param->clone();
    IParams *ptr;
    par->getItemCopy(indexOfParams,ptr);
///TODO!!! ��� �� ��������� �������?
    ptr->setParam(1,maxMode);
    par->setItem(indexOfParams,ptr);


    for(size_t i = t+1; i < size; ++i)
        log+=model->estimateHypothesis( indexOfParams, par, m_data[i], particles, i);

    delete par;

    if( logLikelihood + log < bestLoglikelihood)
    {
        return false;
    }
    return true;
}

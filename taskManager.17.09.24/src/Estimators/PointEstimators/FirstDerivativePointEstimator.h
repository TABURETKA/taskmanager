#ifndef __FIRSTDERIVATIVEPOINTESTIMATOR_H__633569422542968750
#define __FIRSTDERIVATIVEPOINTESTIMATOR_H__633569422542968750
#include <vector>
#include "../Estimator.h"
#include "FakePointEstimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace PointEstimators{
///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[Params1D] ??? + ���������� ����� ����������???
#define FIRSTDERIVATIVE_POINTESTIMATOR_DATASETNAME "derivatives"
#define FIRSTDERIVATIVE_POINTESTIMATOR_TRACKNAME_0 "derivative_values"

    /** class container */
/** implementation of abstract Base class IPointEstimator */
class FirstDerivativePointEstimator : public virtual IPointEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakePointEstimator
*/
                                      public virtual FakePointEstimator
{
public:
    FirstDerivativePointEstimator(Logger*& log);
    ~FirstDerivativePointEstimator();
    /** IEstimator stuff */
    using FakePointEstimator::setTask;
    using FakePointEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakePointEstimator::toString;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IPointEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

///TODO!!! ��� ���� � setData()???
    virtual int estimatePoint(ptrInt threadInterruptSignal);

protected:
    using   FakePointEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    FirstDerivativePointEstimator(const FirstDerivativePointEstimator&);
    void operator=(const FirstDerivativePointEstimator&);
/** enum PointEstimatorsTypes m_pet; has been Moved to FakePointEstimator */
/** Task* m_task; has been Moved to FakePintEstimator */
/** Logger* m_logger; has been Moved to FakePointEstimator */
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
//OLD    IParamsCompact* _pst;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    IParams* m_estimatorParams;

///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;
};

} /// end namespace PointEstimators
#endif

#include "../../Params/ParamsSet.h"//IParams
#include "../../Models/Model.h" //ModelTypes
#include "../../Log/errors.h"
#include "../../DataCode/DataModel/DataMetaInfo.h" //DataInfo
#include "FirstDerivativePointEstimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace PointEstimators;

PointEstimators::FirstDerivativePointEstimator::FirstDerivativePointEstimator(Logger*& log) : FakePointEstimator(log)
{
  /** MUST re-init m_pet since in FakePointEstimatyor this member
  has been initialized as FakePointEstimator type
   */
  m_pet = PointEstimatorsTypes::FirstDerivativePointEstimator;
  /** _task = NULL; m_logger  = log; already initialized in FakePointEstimator's CTOR */
  m_model  = NULL;
  m_estimatorParams  = NULL;
  m_setOfParamsEstimates = NULL;

}
PointEstimators::FirstDerivativePointEstimator::~FirstDerivativePointEstimator()
{
    m_data.clear();
    delete m_setOfParamsEstimates;
}
    /** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */

/** generate DataInfo */
int PointEstimators::FirstDerivativePointEstimator::getEstimatesAsDataInfoArray(size_t & dimension, DataInfo** & di) const
{
///TODO!!!
//    m_logger->log(ALGORITHM_1_LEVEL_INFO, "FirstDerivativePointEstimator::generateDataInfo is not implemented yet!");
///�� ������ ��������� ������ ���� ��� ��������� ���� ������������ � ��������� �������
    IParams* ptrIParams = m_setOfParamsEstimates->getFirstPtr();
    size_t size = ptrIParams->getSize();
    if (!size)
    {
        return _ERROR_WRONG_OUTPUT_DATA_;
    }

    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }


    ///TODO!!! magic 1 == dimension of FirstDerivativePointEstimator's estimates!
    size_t di_0_dimension = 1;
    /** the size of _trackSizes (the ??? index of single-dimension sets) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if (!di_0_dataSetSizes)
    {
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetSizes for estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    di_0_dataSetSizes[0]=size;

    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    data[0] = new double[di_0_dataSetSizes[0]];
    if (!data[0])
    {
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(data[0], 0, di_0_dataSetSizes[0]*sizeof(double));
    for(size_t j=0; j < di_0_dataSetSizes[0]; ++j)
    {
        double tmp=0;
        ptrIParams->getParam(j, tmp);
        data[0][j] = tmp;
    }
    /** Here we put ptr to data as double const ** const
    to the DataInfo CTOR
    */
    di[0] = new DataInfo(data);

    if (!di[0])
    {
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = pointEstimatorNames[PointEstimatorsTypes::FirstDerivativePointEstimator];

    /// the name of data set, for example regression location or regression coefficients
    di[0]->m_dataSetName = FIRSTDERIVATIVE_POINTESTIMATOR_DATASETNAME;
    if (m_model)
    {
        di[0]->m_dataModelName = m_model->toString();
    } else
    {
        di[0]->m_dataModelName = IModel::modelNames[IModel::ModelsTypes::PlainModel];
    }

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate dataSetTrackNames for estimates");
        return _ERROR_NO_ROOM_;
    }
    di[0]->m_dataSetTrackNames[0]=FIRSTDERIVATIVE_POINTESTIMATOR_TRACKNAME_0;

    /** the size of _trackSizes (the ??? index of single-dimension sets) */
    di[0]->m_dataSetSizes = di_0_dataSetSizes;

    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    return _RC_SUCCESS_;
}

int PointEstimators::FirstDerivativePointEstimator::setModel(IModel* model)
{
///TODO!!! Wrong design! May be put FirstDerivativePointEstimator into class inherited INonParametricEstiamot?
/// So, it wiil use nonparametric model
    if(!model)
    {
/** PlainModel instance we DO create!
    So, HERE we expect model != NULL
 */
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    setParamsSet();
    return _RC_SUCCESS_;
}

///TODO!!! ��� ���� � setData()???
   /** No need of ParamsCompact! At least so far
int PointEstimators::FirstDerivativePointEstimator::setParamsCompact()
{
///TODO!!!
    m_logger->log(ALGORITHM_1_LEVEL_INFO, "FirstDerivativePointEstimator::setParamsCompact is not implemented yet!");
}
*/
int PointEstimators::FirstDerivativePointEstimator::setParamsSet()
{
    /**
    � ���� ���������� ����� �������� ������ ������ �����������,
    ��� ���������� � ���� Params1D �����������, �� ������� ������ ������� �������� ������ ������
    ������� ��� ����� ���������� �� ����� ������ ���������� ������,
    �.�. ��� ����� ���������� ������������ �������� ����������� PLAIN_MODEL
    (������ ����������������� ������)
    � � ��������� ��� ������� ������ ������ ��������� �� ��� �� ����� ������,
    �.�. ������� ��������� Params1D, ����������� � ���� ���,
    �� �����!
    */
    const IModel* pstModel = NULL;
    IParamsSet * pst = IParamsSet::createEmptyParamsSet(pstModel, m_logger);
    /** This way at least so far. */
    if (pst)
    {
/// pst already has initialized ptr tor its model       pst->setModel(NULL, m_logger);
        m_setOfParamsEstimates = pst;
    }
    return _RC_SUCCESS_;
}
int PointEstimators::FirstDerivativePointEstimator::estimatePoint(ptrInt threadInterruptSignal)
{
///TODO!!! ???
     int rc = _RC_SUCCESS_;
//     m_logger->log(ALGORITHM_1_LEVEL_INFO, "FirstDerivativePointEstimator::estimatePoint is not implemented yet!");
     size_t size = m_data.size() - 1;
     if (size > 0)
     {
         IParams * derivatives= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, size);
         if (!derivatives)
         {
             m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to keep Estimator's estimates");
             return _ERROR_NO_ROOM_;
         }
         for(size_t i=0; i< size; ++i)
         {
             derivatives->setParam(i, m_data[i+1] - m_data[i]);
         }
///TODO!!! ��� ������ ��������: ��������� �������� �� ������ �������� ���������� ������, � ������ ����� ��������� ��������������� � ���� ���� ������, �.�. � ParamsSet ���������� ������ ��������� �� ����������� ������ ������ NULL
         if(!m_setOfParamsEstimates)
         {
             this->setParamsSet();
         }
         rc = _RC_SUCCESS_;
         if (m_setOfParamsEstimates->addParamsCopyToSet(NULL, derivatives, m_logger) != true )
         {
             delete derivatives;
             derivatives = NULL;
             m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Can not put derivatives into Estimator's m_setOfParamsEstimates.");
             rc = _ERROR_WRONG_OUTPUT_DATA_;
         }
         /** ptr to allocated params is stored in ParamsSet,
         anyway delete ptrToParams
         */
         delete derivatives;
     }
     else
     {
         rc = _ERROR_WRONG_OUTPUT_DATA_;
     }
     return rc;
}

int PointEstimators::FirstDerivativePointEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
///TODO!!!
//    m_logger->log(ALGORITHM_1_LEVEL_INFO, "FirstDerivativePointEstimator::setData is not implemented yet!");
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];

    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;

}

int PointEstimators::FirstDerivativePointEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}
    /** IPointEstimator stuff */
int PointEstimators::FirstDerivativePointEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parseAndCreateEstimatorParams is not implemented yet!");
}
int PointEstimators::FirstDerivativePointEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int PointEstimators::FirstDerivativePointEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

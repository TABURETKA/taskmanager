#ifndef __FAKEPOINTESTIMATOR_H__633569422542968750
#define __FAKEPOINTESTIMATOR_H__633569422542968750
#include "../Estimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace PointEstimators{
    /** class container */
/** implementation of basic methods
 declared in abstract Base class IPointEstimator
 The methods setTask, setLog, toString
 have to be inherited all PointEstimators
*/
class FakePointEstimator : public virtual IPointEstimator
{
public:
    FakePointEstimator(Logger*& log);
    virtual ~FakePointEstimator();
    /** IEstimator stuff */
    virtual void setTask(Task* const & task);
    virtual void setLog(Logger* const & log);
///TODO!!! ��� ���� � setData()???
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    virtual std::string toString() const;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IPointEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimatePoint(ptrInt threadInterruptSignal);

protected:
    int logErrMsgAndReturn(int rc, std::string msg) const;
    virtual int parsingFailed(std::string msg) const;
    enum PointEstimatorsTypes m_pet;
    Task* m_task;
    Logger* m_logger;
private:
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    FakePointEstimator(const FakePointEstimator&);
    void operator=(const FakePointEstimator&);
};

} /// end namespace PointEstimators
#endif

#ifndef __LinearTransformationPointEstimator_H__633569422542968750
#define __LinearTransformationPointEstimator_H__633569422542968750
#include <vector>
#include "../Estimator.h"
#include "FakePointEstimator.h"

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace PointEstimators{

///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[Params1D] ??? + ���������� ����� ����������???
#define LINEARTRANSFORMATION_POINTESTIMATOR_DATASETNAME "Linear TransformationP"
#define LINEARTRANSFORMATION_POINTESTIMATOR_TRACKNAME_0 "Linear Transformation Values"

    /** class container */
/** implementation of abstract Base class IPointEstimator */
class LinearTransformationPointEstimator : public virtual IPointEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakePointEstimator
*/
        public virtual FakePointEstimator
{
public:
    LinearTransformationPointEstimator(Logger*& log);
    ~LinearTransformationPointEstimator();
    /** IEstimator stuff */
    using FakePointEstimator::setTask;
    using FakePointEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);

    /** ����� ������, ����������� ��� ������� ���������! */
    using FakePointEstimator::toString;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IPointEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);
    virtual int estimatePoint(ptrInt threadInterruptSignal);

protected:
    using   FakePointEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    LinearTransformationPointEstimator(const LinearTransformationPointEstimator&);
    void operator=(const LinearTransformationPointEstimator&);

    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    IParams* m_estimatorParams;

    std::vector<double> m_data;
};

} /// end namespace PointEstimators
#endif

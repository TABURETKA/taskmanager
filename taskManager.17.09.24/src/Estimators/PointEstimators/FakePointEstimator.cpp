#include "../../Log/Log.h"
#include "../../Log/errors.h"
#include "FakePointEstimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace PointEstimators;

PointEstimators::FakePointEstimator::FakePointEstimator(Logger*& log)
{
 /** so far noting to allocate */
  m_pet = PointEstimatorsTypes::FakePointEstimator;
  m_task = NULL;
  m_logger  = log;
}
PointEstimators::FakePointEstimator::~FakePointEstimator()
{
 /** so far noting to delete */
}
    /** IEstimator stuff */
///TODO!!! ������������ � setTask, � setLog!
void PointEstimators::FakePointEstimator::setTask(Task* const & task)
{
      m_task = task;
}

void PointEstimators::FakePointEstimator::setLog(Logger* const & log)
{
      m_logger = log;
}

int PointEstimators::FakePointEstimator::logErrMsgAndReturn(int rc, std::string msg) const
{
    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": " + msg);
    return _ERROR_WRONG_WORKFLOW_;
}

/** ����� ������, ����������� ��� ������� ���������! */
std::string PointEstimators::FakePointEstimator::toString() const
{
  return (std::string) pointEstimatorNames[m_pet];
}

    /** generate DataInfo */
int PointEstimators::FakePointEstimator::getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " getEstimatesAsDataInfoArray is not implemented yet!");
}

    /** IPointEstimator stuff */
int PointEstimators::FakePointEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parseAndCreateEstimatorParams is not implemented yet!");
}
int PointEstimators::FakePointEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

int PointEstimators::FakePointEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int PointEstimators::FakePointEstimator::setModel(IModel* model)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setModel is not implemented yet!");
}

///TODO!!! ��� ���� � setData()???
/** No need so far
int PointEstimators::FakePointEstimator::setParamsCompact(IParamsCompact * pst)
{
///TODO!!!
    m_logger->log(ALGORITHM_1_LEVEL_INFO, "FakePointEstimator::setParamsCompact is not implemented yet!");
}
*/
int PointEstimators::FakePointEstimator::setParamsSet()
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setParamsSet is not implemented yet!");
}
int PointEstimators::FakePointEstimator::estimatePoint(ptrInt threadInterruptSignal)
{
///TODO!!! ???
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " estimatePoint is not implemented yet!");
}

int PointEstimators::FakePointEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}

int PointEstimators::FakePointEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setData is not implemented yet!");
}

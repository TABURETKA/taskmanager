#ifndef __GAUSSIANOUTLIERSBYQUANTILEPOINTESTIMATOR_H__633569422542968750
#define __GAUSSIANOUTLIERSBYQUANTILEPOINTESTIMATOR_H__633569422542968750
#include <vector>
#include "../Estimator.h"
#include "FakePointEstimator.h"
///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[Params1D] ??? + ���������� ����� ����������???
#define GAUSSIANOUTLIERSBYQUANTILE_POINTESTIMATOR_DATASETNAME "outliers"
#define GAUSSIANOUTLIERSBYQUANTILE_POINTESTIMATOR_TRACKNAME_0 "outliers_values"
#define GAUSSIANOUTLIERSBYQUANTILE_POINTESTIMATOR_TRACKNAME_1 "outliers_indexes"

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace PointEstimators{
    /** class container */
/** implementation of abstract Base class IPointEstimator */
class GaussianOutliersByQuantilePointEstimator : public virtual IPointEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakePointEstimator
*/
                                                 public virtual FakePointEstimator
{
public:
    enum IndexesOfGrammarGaussianOutliersByQuantile{
        indexPMax,
        indexPMin,
        indexPInc,
        FAKE_INDEX_GRAMMAR
    };
/* OLD C++ < C++11    static const char* grammar[FAKE_INDEX_GRAMMAR]; */
    static constexpr const char* grammar[FAKE_INDEX_GRAMMAR]=
    {
        "indexPMax",
        "indexPMin",
        "indexPInc"
    };

    GaussianOutliersByQuantilePointEstimator(Logger*& log);
    ~GaussianOutliersByQuantilePointEstimator();
    /** IEstimator stuff */
    using FakePointEstimator::setTask;
    using FakePointEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakePointEstimator::toString;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IPointEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

///TODO!!! ��� ���� � setData()???
    virtual int estimatePoint(ptrInt threadInterruptSignal);

protected:
    using   FakePointEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    GaussianOutliersByQuantilePointEstimator(const GaussianOutliersByQuantilePointEstimator&);
    void operator=(const GaussianOutliersByQuantilePointEstimator&);
/** enum PointEstimatorsTypes _pet; has been Moved to FakePointEstimator */
/** Task* m_task; has been Moved to FakePintEstimator */
/** Logger* m_logger; has been Moved to FakePointEstimator */
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
//OLD    IParamsCompact* _pst;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    IParams* m_estimatorParams;

///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;
};

} /// end namespace PointEstimators
#endif

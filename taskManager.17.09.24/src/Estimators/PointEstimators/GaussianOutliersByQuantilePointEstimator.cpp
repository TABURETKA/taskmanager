#include <algorithm> //sort()
#include <sstream> //stringstrim
#include "../../Params/ParamsSet.h"//IParams
#include "../../Models/Model.h" //ModelTypes
#include "../../Log/errors.h"
#include "../../StringStuff/StringStuff.h" /* strTo<> */
#include "../../DataCode/DataModel/DataMetaInfo.h" //DataInfo
#include "GaussianOutliersByQuantilePointEstimator.h"
#define DEFAULT_MAX_PVALUE 0.999f
#define DEFAULT_MIN_PVALUE 0.90f
#define DEFAULT_INC_PVALUE 0.01f
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace PointEstimators;
///static in GaussianOutliersByQuantilePointEstimator
constexpr const char* GaussianOutliersByQuantilePointEstimator::grammar[FAKE_INDEX_GRAMMAR];

PointEstimators::GaussianOutliersByQuantilePointEstimator::GaussianOutliersByQuantilePointEstimator(Logger*& log) : FakePointEstimator(log)
{
   /** MUST re-init m_pet since in FakePointEstimatyor this member
  has been initialized as FakePointEstimator type
   */
  m_pet = PointEstimatorsTypes::GaussianOutliersByQuantilePointEstimator;
  /** _task = NULL; m_logger  = log; already initialized in FakePointEstimator's CTOR */
  m_model  = NULL;
  m_estimatorParams  = NULL;
  m_setOfParamsEstimates = NULL;
}

PointEstimators::GaussianOutliersByQuantilePointEstimator::~GaussianOutliersByQuantilePointEstimator()
{
    m_data.clear();
    delete m_estimatorParams;
    delete m_setOfParamsEstimates;
}
    /** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */
int PointEstimators::GaussianOutliersByQuantilePointEstimator::getEstimatesAsDataInfoArray(size_t & dimension, DataInfo** & di) const
{
//    m_logger->log(ALGORITHM_1_LEVEL_INFO, "GaussianOutliersByQuantilePointEstimator::generateDataInfo is not implemented yet!");
    size_t size =(m_setOfParamsEstimates) ? (m_setOfParamsEstimates->getSetSize()) : 0;
    if (!size)
    {
        return _ERROR_ESTIMATES_NOT_FOUND_;
    }
    IParams* ptrIParams = m_setOfParamsEstimates->getFirstPtr();

    size =(ptrIParams) ? (ptrIParams->getSize()) : 0;
    if (!size)
    {
        return _ERROR_WRONG_OUTPUT_DATA_;
    }

    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }

    /** 2 == dimension of GaussianOutliers's estimates! */
    size_t di_0_dimension = IParamsGaussian::GaussianCDFParamsDimension;

    /** the size of _trackSizes (the ??? index of single-dimension sets) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetSizes for estimates");
        return _ERROR_NO_ROOM_;
    }

    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    di_0_dataSetSizes[0]=size;
    di_0_dataSetSizes[1]=size;

    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        delete [] di_0_dataSetSizes;
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            while(i)
            {
                --i;
                delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        IParams* ptrIParams = m_setOfParamsEstimates->getFirstPtr();
        if (i == 1) ptrIParams = m_setOfParamsEstimates->getNextPtr();
        for(size_t j=0; j < di_0_dataSetSizes[i]; ++j)
        {
            double tmp=0;
            ptrIParams->getParam(j, tmp);
            data[i][j] = tmp;
        }
    }
    /** Here we put ptr to data as double const ** const
    to the DataInfo CTOR
    */
    di[0] = new DataInfo(data);

    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
                --i;
                delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    di[0]->m_dimension = di_0_dimension;

    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = pointEstimatorNames[PointEstimatorsTypes::GaussianOutliersByQuantilePointEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = GAUSSIANOUTLIERSBYQUANTILE_POINTESTIMATOR_DATASETNAME;
    if (m_model)
    {
        di[0]->m_dataModelName = m_model->toString();
    } else
    {
        di[0]->m_dataModelName = IModel::modelNames[IModel::ModelsTypes::PlainModel];
    }

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to allocate ptrs to dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }

    di[0]->m_dataSetTrackNames[0]=GAUSSIANOUTLIERSBYQUANTILE_POINTESTIMATOR_TRACKNAME_0;
    di[0]->m_dataSetTrackNames[1]=GAUSSIANOUTLIERSBYQUANTILE_POINTESTIMATOR_TRACKNAME_1;

    /** the size of _trackSizes (the ??? index of single-dimension sets) */
    di[0]->m_dataSetSizes = di_0_dataSetSizes;
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    return _RC_SUCCESS_;
}


    /** IPointEstimator stuff */

int PointEstimators::GaussianOutliersByQuantilePointEstimator::setModel(IModel* model)
{
/// Gaussian Model valid ONLY!
    enum IModel::ModelsTypes mt=IModel::ModelsTypes::FAKE_MODEL;
    model->getModelType(mt);
    if(mt != IModel::ModelsTypes::GaussianModel)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
    return rc;
}
/** No need of ParamsCompact! At least so far
int PointEstimators::GaussianOutliersByQuantilePointEstimator::setParamsCompact(IParamsCompact * pst)
{
///TODO!!!
    m_logger->log(ALGORITHM_1_LEVEL_INFO, "PointEstimators::GaussianOutliersByQuantilePointEstimator::setParamsCompact is not implemented yet!");
}
*/
int PointEstimators::GaussianOutliersByQuantilePointEstimator::setParamsSet()
{
   /** ����� ���������� ��� ������ ������ ���������� �� ����� ��� ���������� ��� ������,
   ������ ��� ��� ������ = ������� ������
   �� ���� ����������� ������� ������� IParamsSet � ��������������
   �������� ��������� ������ ��������� �� �������� ������
//OLD   ��� ������ ������ Gaussian (����� �� ��������� ��������� PLAIN_MODEL),
//OLD   � ����� ������� � IParamsSet ��� ������ � NULL
   */
   const IModel* pstModel = NULL;
   IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) pstModel, m_logger);
   /** This way at least so far. */
   if (pst)
   {
//OLD       pst->setModel(NULL, m_logger);
       m_setOfParamsEstimates = pst;
   }
   return _RC_SUCCESS_;
}
/******************************************************************
* this estimator IGNOREs its parameter threadInterruptSignal
* because its life-time very short, its interrupt does not make sense
******************************************************************/
int PointEstimators::GaussianOutliersByQuantilePointEstimator::estimatePoint(ptrInt threadInterruptSignal)
{
    if(NULL == m_estimatorParams)
    {
        return _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }
///TODO!!! ???
    std::vector<double> abs_first_deriv;
    std::vector<double> outliers_indexes;
    std::vector<double> outliers_values;
    /** We have already loaded first_derivatives into m_data
    */
    for(size_t i=0; i < m_data.size(); ++i)
    {
///TODO!!! ������������� ��� : "Overflow" <- !
        abs_first_deriv.push_back(m_data[i]);
    }
    /** First estimate mediana of first_derivatives
    */
    sort(abs_first_deriv.begin(), abs_first_deriv.end());
    double mediana = (abs_first_deriv.size() % 2) ? (abs_first_deriv[abs_first_deriv.size() / 2]) : (abs_first_deriv[abs_first_deriv.size()/2 - 1] + abs_first_deriv[abs_first_deriv.size() / 2])/2.0f;
    /** Second shift first_derivatives as subtraction of mediana
    */
    for(size_t i=0; i < abs_first_deriv.size(); ++i)
    {
        abs_first_deriv[i] -= mediana;
    }
    /** Third make abs first_derivatives after shift transform
    */
    for(size_t i=0; i < abs_first_deriv.size(); ++i)
    {
        abs_first_deriv[i] = fabsf(abs_first_deriv[i]);
    }
    sort(abs_first_deriv.begin(), abs_first_deriv.end());

    /** Fourth estimate outliers using half-normal DF model for abs first_derivatives
    */
    unsigned counterOutliers = outliers_indexes.size();
//!!!! ��� ����� ����� ���� �� p-values
    double p_value = DEFAULT_MAX_PVALUE;
    /** if HalfNormalDF_Quantile.cfg has been parsed
    */
    int rc = m_estimatorParams->getParam(indexPMax, p_value);
    if((_RC_SUCCESS_ != rc) || (p_value > 1.0f) || (p_value < 0.0f))
    {
        return _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }

    double minPValue = DEFAULT_MIN_PVALUE;
    rc = m_estimatorParams->getParam(indexPMin, minPValue);
    if ((_RC_SUCCESS_ != rc) || (minPValue > 1.0f) || (minPValue < 0.0f) || (p_value < minPValue))
    {
        return _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }
    double incPValue = DEFAULT_INC_PVALUE;
    rc = m_estimatorParams->getParam(indexPInc, incPValue);
    if((_RC_SUCCESS_ != rc) || (minPValue > 1.0f) || (minPValue < 0.0f))
    {
        return _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }
    double threshold;
    while((0 == counterOutliers) && (p_value > minPValue))
    {
        rc = IModelGaussian::Quantil(abs_first_deriv, p_value, threshold);
        if(rc != _RC_SUCCESS_)
        {
            return rc;
        }
        for (size_t i = 0; i < m_data.size() - 1; ++i)
        {
            if (m_data[i] > threshold)
            {
///TODO!!! ������������� ��� : "Overflow" <- !
                outliers_indexes.push_back(i);
                outliers_values.push_back(m_data[i]);
            }
        }
        counterOutliers = outliers_indexes.size();
        p_value -= incPValue;
    }
    std::stringstream logStr;
    logStr << "Threshold: " << threshold << " for p-value: " << p_value;
    m_logger->log(ALGORITHM_1_LEVEL_INFO, logStr.str());

    std::stringstream appLog;
    appLog << "Estimated indexes of outliers:\n";
    for (size_t i = 0; i < counterOutliers; ++i) {
        appLog << outliers_indexes[i] << " " << outliers_values[i] << std::endl;
  }
    m_logger->log(ALGORITHM_2_LEVEL_INFO, appLog.str());
///TODO!!! � � ���� ����� ���� ���� ������
/// �� �������� outliers_indexes, outliers_values ???
///TODO!!! �������� ������ � m_setOfParamsEstimates!!!
     size_t size = outliers_values.size();
//DEBUG if(!size) std::cout << "NOT found outliers!" << std::endl;
     if (size > 0)
     {
         IParams * outliersValues= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, size);
         IParams * outliersIndexes= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, size);
         if ((!outliersValues) || (!outliersIndexes))
         {
             if (outliersValues) delete outliersValues;
             if (outliersIndexes) delete outliersIndexes;
             m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": No room to keep Estimator's estimates");
             abs_first_deriv.clear();
             outliers_indexes.clear();
             outliers_values.clear();
             return _ERROR_NO_ROOM_;
         }
         for(size_t i=0; i< size; ++i)
         {
             outliersValues->setParam(i, outliers_values[i]);
             outliersIndexes->setParam(i, outliers_indexes[i]);
         }
///TODO!!! ��� ������ ��������: ��������� �������� �� ������ �������� ���������� ������, � ������ ����� ��������� ��������������� � ���� ���� ������, �.�. � ParamsSet ���������� ������ ��������� �� ����������� ������ ������ NULL
//OLD        m_setOfParamsEstimates->addParams(NULL, outliersValues, m_logger);
        /** ptr to allocated params estimates is stored in ParamsSet,
        anyway delete outliersValues
        */
       if ( m_setOfParamsEstimates->addParamsCopyToSet(NULL, outliersValues, m_logger) == false)
       {
            delete outliersValues; outliersValues=NULL;
            delete outliersIndexes; outliersIndexes=NULL;
            delete m_setOfParamsEstimates;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Attempt to put values of Gaussian Outliers into ParamsSet failed!");
            return _ERROR_WRONG_ARGUMENT_;
       }
       /** Once we put copy of outliersValues to setOfEstimatorParams
       free outliersValues
       */
       delete outliersValues; outliersValues=NULL;
        /** ptr to allocated params estimates is stored in ParamsSet,
        anyway delete outliersIndexes
        */
       if ( m_setOfParamsEstimates->addParamsCopyToSet(NULL, outliersIndexes, m_logger) == false)
       {
            delete outliersValues; outliersValues=NULL;
            delete outliersIndexes; outliersIndexes=NULL;
            delete m_setOfParamsEstimates;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Attempt to put indexes of Gaussian Outliers into ParamsSet failed!");
            return _ERROR_WRONG_ARGUMENT_;
       }
       /** Once we put copy of outliersIndexes to setOfEstimatorParams
       free outliersIndexes
       */
       delete outliersIndexes; outliersIndexes = NULL;
     }
     abs_first_deriv.clear();
     outliers_indexes.clear();
     outliers_values.clear();
    return _RC_SUCCESS_;
}

int PointEstimators::GaussianOutliersByQuantilePointEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
///TODO!!!
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];

    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(pointEstimatorNames[m_pet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}

int PointEstimators::GaussianOutliersByQuantilePointEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}

int PointEstimators::GaussianOutliersByQuantilePointEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
///TODO!!!
    /** Let's parse GaussianOutliersByQuantilePointEstimator's params
    */
    double tmpDbl = NAN;

    auto itParam = mapParams.find(GaussianOutliersByQuantilePointEstimator::grammar[indexPMax]);
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), tmpDbl); //OLD          tmpDbl = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc == _RC_SUCCESS_)
    {
        if((tmpDbl < 0.0f) || (tmpDbl > 1.0f))
        {
            std::stringstream ss;
            ss << "Wrong Max p-value of GaussianOutliersByQuantilePointEstimator: " << tmpDbl;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    else
    {
        tmpDbl = DEFAULT_MAX_PVALUE;
        std::stringstream ss;
        ss << " Max p-value of GaussianOutliersByQuantilePointEstimator NOT FOUND! Use default value : " << tmpDbl;
        m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
    }
    IParams* params = IParams::createEmptyParams(m_logger, IParams::Params1D, FAKE_INDEX_GRAMMAR);
    if(!params)
    {
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + "NO room to allocate estimator's params.");
        return _ERROR_NO_ROOM_;
    }
    params->setParam(indexPMax, tmpDbl);
    if(rc != _RC_SUCCESS_){
        delete params; params = nullptr;
        return rc;
    }

    tmpDbl = NAN;
    itParam = mapParams.find(GaussianOutliersByQuantilePointEstimator::grammar[indexPMin]);

    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), tmpDbl); //OLD          tmpDbl = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc == _RC_SUCCESS_)
    {
        if((tmpDbl < 0.0f) || (tmpDbl > 1.0f))
        {
            std::stringstream ss;
            ss << "Wrong Min p-value of GaussianOutliersByQuantilePointEstimator: " << tmpDbl;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    else
    {
        tmpDbl = DEFAULT_MIN_PVALUE;
        std::stringstream ss;
        ss << " Min p-value of GaussianOutliersByQuantilePointEstimator NOT FOUND! Use default value : " << tmpDbl;
        m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
    }
    params->setParam(indexPMin, tmpDbl);
    if(rc != _RC_SUCCESS_){
        delete params; params = nullptr;
        return rc;
    }

    tmpDbl = NAN;
    itParam = mapParams.find(GaussianOutliersByQuantilePointEstimator::grammar[indexPInc]);
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), tmpDbl); //OLD          tmpDbl = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc == _RC_SUCCESS_)
    {
        if((tmpDbl < 0.0f) || (tmpDbl > 1.0f))
        {
            std::stringstream ss;
            ss << "Wrong increment p-value of GaussianOutliersByQuantilePointEstimator: " << tmpDbl;
            m_logger->error(WORKFLOW_TASK_ERROR, std::string(pointEstimatorNames[m_pet]) + ss.str());
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    else
    {
        tmpDbl = DEFAULT_INC_PVALUE;
        std::stringstream ss;
        ss << " Increment p-value of GaussianOutliersByQuantilePointEstimator NOT FOUND! Use default value : " << tmpDbl;
        m_logger->warning(ALGORITHM_ESTIMATOR_WARNING, std::string(pointEstimatorNames[m_pet]) +  ss.str());
    }
    rc = params->setParam(indexPInc, tmpDbl);
    if(rc != _RC_SUCCESS_){
        delete params; params = nullptr;
        return rc;
    }

    if(m_estimatorParams){
        delete m_estimatorParams;
        m_estimatorParams = nullptr;
    }
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}
int PointEstimators::GaussianOutliersByQuantilePointEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int PointEstimators::GaussianOutliersByQuantilePointEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

#include "./Estimator.h"
#include "../Log/Errors.h"

#include "./IntervalEstimators/SampleMeanIntervalEstimator.h"
#include "./IntervalEstimators/PoissonLambdaIntervalEstimator.h"
#include "./IntervalEstimators/ParetoIntervalEstimator.h"
#include "./IntervalEstimators/WienerSigmaIntervalEstimator.h"
#include "./IntervalEstimators/WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates.h"

#include "./PointEstimators/FakePointEstimator.h"
#include "./PointEstimators/FirstDerivativePointEstimator.h"
#include "./PointEstimators/LinearTransformationPointEstimator.h" /// new
#include "./PointEstimators/GaussianOutliersByQuantilePointEstimator.h"
#include "./PointEstimators/PF_POINT_ESTIMATOR/PF_PointEstimator.h"

#include "./SimpleEstimators/ModifiedExpRegressionEstimator.h"
#include "./SimpleEstimators/PW_PolynomialRegressionEstimatorForGivenSubsets.h"
#include "./SimpleEstimators/PW_PolynomialRegressionEstimator.h" /// new
#include "./SimpleEstimators/ScalarArithmeticWithDatasetEstimator.h"

/** static in IEstimator
*/
constexpr const char * IEstimator::estimatorTypesNames[FAKE_ESTIMATOR_TYPE];
/** static in ISimpleEstimator
*/
constexpr const char * ISimpleEstimator::simpleEstimatorNames[FAKE_SIMPLE_ESTIMATOR];

enum SimpleEstimatorsTypes getSimpleEstimatorTypeBySimpleEstimatorName(const std::string& simpleEstimatorName)
{
  size_t i=0;
  for(i=0; i < FAKE_SIMPLE_ESTIMATOR; ++i)
  {
      if (simpleEstimatorName == ISimpleEstimator::simpleEstimatorNames[i])
      {
          return (enum SimpleEstimatorsTypes)i;
      }
  }
  return FAKE_SIMPLE_ESTIMATOR;
}
/** static in IPointEstimator
*/
constexpr const char * IPointEstimator::pointEstimatorNames[FAKE_POINT_ESTIMATOR];

enum PointEstimatorsTypes getPointEstimatorTypeByPointEstimatorName(const std::string& pointEstimatorName)
{
  size_t i=0;
  for(i=0; i < FAKE_POINT_ESTIMATOR; ++i)
  {
      if (pointEstimatorName == IPointEstimator::pointEstimatorNames[i])
      {
          return (enum PointEstimatorsTypes)i;
      }
  }
  return FAKE_POINT_ESTIMATOR;
}
///////////////////////////////
/** static in IIntervalEstimator
*/
constexpr const char * IIntervalEstimator::intervalEstimatorNames[FAKE_INTERVAL_ESTIMATOR];

enum IntervalEstimatorsTypes getIntervalEstimatorTypeByIntervalEstimatorName(const std::string& intervalEstimatorName)
{
  size_t i=0;
  for(i=0; i < FAKE_INTERVAL_ESTIMATOR; ++i)
  {
      if (intervalEstimatorName == IIntervalEstimator::intervalEstimatorNames[i])
      {
          return (enum IntervalEstimatorsTypes)i;
      }
  }
  return FAKE_INTERVAL_ESTIMATOR;
}

/** static in IPointEstimator
*/
constexpr const char* IPointEstimator::grammarPointEstimates[grammarSizeForPointEstimates];
///TODO!!! �������� ��� - ���������� ����� ��������� ���������� - ��� �������!!!
/** static in IIntervalEstimator
*/
constexpr const char* IIntervalEstimator::grammarIntervalEstimates[grammarSizeForIntervalEstimates];

/** static in IAdditiveModelEstimator
*/
constexpr const char* IAdditiveModelEstimator::grammarAdditiveModelEstimatorConfig[GrammarSizeForAdditiveModelEstimatorConfig];

///////////////////////////////
/********************************************************************************************
*  static method to create instances of ALL estimator (Entry point to the chain of factories)
********************************************************************************************/
IEstimator* IEstimator::createEstimator(const enum EstimatorsTypes estimatorType, const std::string& estimatorName, Logger*& log)
{
    switch (estimatorType)
    {
        case SimpleEstimator:
        {
             /** estimatorName -> SimpleEstimatorsTypes */
             enum SimpleEstimatorsTypes et = getSimpleEstimatorTypeBySimpleEstimatorName(estimatorName);
             if (et != FAKE_SIMPLE_ESTIMATOR)
             {
                 return ISimpleEstimator::createSimpleEstimator(et, log);
             }
             else
               return NULL;
             break;
        }
        case PointEstimator:
        {
             /** estimatorName -> PointEstimatorsTypes */
             enum PointEstimatorsTypes et = getPointEstimatorTypeByPointEstimatorName(estimatorName);
             if (et != FAKE_POINT_ESTIMATOR)
             {
                 return IPointEstimator::createPointEstimator(et, log);
             }
             else
               return NULL;
             break;
        }
        case  IntervalEstimator:
        {
             /** estimatorName -> IntervalEstimatorsTypes */
             enum IntervalEstimatorsTypes et = getIntervalEstimatorTypeByIntervalEstimatorName(estimatorName);
             if (et != FAKE_INTERVAL_ESTIMATOR)
             {
               return IIntervalEstimator::createIntervalEstimator(et, log);
             }
             else
               return NULL;
             break;
        }
        default:
             return NULL;
    }
}///end IEstimator::createEstimator(const enum EstimatorsTypes estimatorType, const std::string& estimatorName, Logger*& log)

/**
*  static method to create instances of ALL SIMPLE estimator (Entry point to the chain of Simple Estimators' factories)
*/
IEstimator* ISimpleEstimator::createSimpleEstimator(const enum SimpleEstimatorsTypes estimatorType, Logger*& log)
{
    switch (estimatorType)
    {
        case FakeSimpleEstimator:
               return NULL;
        case  ModifiedExpRegressionEstimator:
               return new SimpleEstimators::ModifiedExpRegressionEstimator(log);
        case  PW_PolynomialRegressionEstimatorForGivenSubsets:
               return new SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets(log);
		case  PW_PolynomialRegressionEstimator:
               return new SimpleEstimators::PW_PolynomialRegressionEstimator(log);
        case  ScalarArithmeticWithDatasetEstimator:
               return new SimpleEstimators::ScalarArithmeticWithDatasetEstimator(log);
        default:
             return NULL;
    }
}///end ISimpleEstimator::createSimpleEstimator(const enum SimpleEstimatorsTypes estimatorType, Logger*& log)

/**
*  static method to create instances of ALL POINT estimator (Entry point to the chain of Point Estimators' factories)
*/
IEstimator* IPointEstimator::createPointEstimator(const enum PointEstimatorsTypes estimatorType, Logger*& log)
{
    switch (estimatorType)
    {
        case  PF_AdditiveModel_PointEstimator:
        {
               return new PointEstimators::PF_PointEstimator(log);
        }
        case PF_StatOffcast_PointEstimator:
               return NULL;
        case FakePointEstimator:
               return new PointEstimators::FakePointEstimator(log);
        case FirstDerivativePointEstimator:
               return new PointEstimators::FirstDerivativePointEstimator(log);
        case LinearTransformationPointEstimator:
               return new PointEstimators::LinearTransformationPointEstimator(log); /// new
        case GaussianOutliersByQuantilePointEstimator:
               return new PointEstimators::GaussianOutliersByQuantilePointEstimator(log);
        default:
             return NULL;
    }
}///end IPointEstimator::createPointEstimator(const enum PointEstimatorsTypes estimatorType, Logger*& log)

/**
*  static method to create instances of ALL POINT estimator (Entry point to the chain of Interval Estimators' factories)
*/
IEstimator* IIntervalEstimator::createIntervalEstimator(const enum IntervalEstimatorsTypes estimatorType, Logger*& log)
{
    switch (estimatorType)
    {
        case  MLE_IntervalEstimator:
               return NULL;
        case SampleMeanIntervalEstimator:
               return new IntervalEstimators::SampleMeanIntervalEstimator(log);
        case  PolyRegressionIntervalEstimator:
               return NULL;
        case PoissonLambdaIntervalEstimator:
               return new IntervalEstimators::PoissonLambdaIntervalEstimator(log);
        case ParetoIntervalEstimator:
               return new IntervalEstimators::ParetoIntervalEstimator(log);
        case WienerSigmaIntervalEstimator:
               return new IntervalEstimators::WienerSigmaIntervalEstimator(log);
        case WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates:
               return new IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates(log);
        default:
             return NULL;
    }
}///end IIntervalEstimator::createIntervalEstimator(const enum IntervalEstimatorsTypes estimatorType, Logger*& log)

/** static method in IEstimator
*/
int IEstimator::runEstimator(IEstimator*& est, ptrInt ptrInterrupt)
{
    IIntervalEstimator * iest = dynamic_cast<IIntervalEstimator*>(est);
    if(iest)
    {
        return iest->estimateInterval(ptrInterrupt);
    }
    IPointEstimator * pest = dynamic_cast<IPointEstimator*>(est);
    if(pest)
    {
        return pest->estimatePoint(ptrInterrupt);
    }
    ISimpleEstimator * sest = dynamic_cast<ISimpleEstimator*>(est);
    if(sest)
    {
        return sest->estimate(ptrInterrupt);
    }
    return _ERROR_WRONG_ARGUMENT_;
}///end IEstimator::runEstimator(IEstimator*& est, ptrInt ptrInterrupt)

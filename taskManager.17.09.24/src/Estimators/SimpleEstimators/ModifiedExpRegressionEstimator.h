#ifndef __MODIFIEDEXPREGRESSIONESTIMATOR_H__633569422542968750
#define __MODIFIEDEXPREGRESSIONESTIMATOR_H__633569422542968750
#include <vector> /* vector */
#include "../Estimator.h"
#include "FakeSimpleEstimator.h"

#define DEFAULT_MIN_SIZE_OF_DATA_TO_BE_PROCESSED         3
#define DEFAULT_EXP_MULTIPLIER_IF_REGRESSION_FAILED      0.0f
#define DEFAULT_MEAN_REVERTING_RATE_IF_REGRESSION_FAILED -1.0f
///TODO!!! move to the class members to refactor getEstimatesAsDataInfoArray
///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[Params1D] ??? + ���������� ����� ����������???
#define MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_DATASETNAME "ModifiedExpRegressionEstimates"
#define MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_0 "coef_0"
#define MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_1 "coef_1"

/** FakeIndex_ModifiedExpRegressionEstimatorParams - ��� ����������� ���������� ��������� ����� ����������!
�� ������������� ������!
*/
enum IndexesOfModifiedExpRegressionEstimatorParams{
        indexOfFloorSematics = 0,
        indexOfFloorValue = 1,
        FakeIndex_ModifiedExpRegressionEstimatorParams
};
static const char* grammarModifiedExpRegressionEstimatorParams[FakeIndex_ModifiedExpRegressionEstimatorParams]={
    "indexOfFloorSematics",
    "indexOfFloorValue"
};
enum ModifiedExpRegressionParamsSemantics {
    floorValue,
    floorItemIndexInData,
    floorLastItemIndexInData,
    floorLastPositiveItemIndexInData,
    FAKE_FLOOR_PARAM_SEMANTICS
};
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace SimpleEstimators{
    /** class container */
/** implementation of basic methods
 declared in abstract Base class ISimpleEstimator
 The methods setTask, setLog, toString
 have to be inherited all SimpleEstimators
*/
class ModifiedExpRegressionEstimator : public virtual ISimpleEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakeSimpleEstimator
*/
                                       public virtual FakeSimpleEstimator
{
public:
    ModifiedExpRegressionEstimator(Logger*& log);
    virtual ~ModifiedExpRegressionEstimator();
    /** IEstimator stuff */
    using FakeSimpleEstimator::setTask;
    using FakeSimpleEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakeSimpleEstimator::toString;
    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** ISimpleEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimate(ptrInt threadInterruptSignal);
protected:
    using FakeSimpleEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
/** ��������� ������ Y(t)=param[0] * exp(-param[1] * t)
�������������� ��������������� � �������� � ����
ln(Y(t))= ln(param[0]) + (-param[1] * t)
����������� ������� ������ ����� = 2
*/
    const static size_t m_estimatesDim = 2;
    virtual int setParamsSet();
    virtual int getFloorValue(double& floor);
    /** assign and copy CTORs are disabled */
    ModifiedExpRegressionEstimator(const ModifiedExpRegressionEstimator&);
    void operator=(const ModifiedExpRegressionEstimator&);
/** enum SimpleEstimatorsTypes _set; has been Moved to FakeSimpleEstimator */
/** Task* _task; has been Moved to FakeSimpleEstimator */
/** Logger* m_logger; has been Moved to FakeSimpleEstimator */
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    IParams* m_estimatorParams;

///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;

};

} /// end namespace SimpleEstimators
#endif

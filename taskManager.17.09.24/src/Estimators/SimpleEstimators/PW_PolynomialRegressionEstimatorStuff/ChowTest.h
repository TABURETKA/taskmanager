#ifndef CHOWTEST_H__
#define CHOWTEST_H__

#include "EstimatorLS.h"
#include <map>
#include <iostream>
#include <string>

///TODO!!! ������ ������, ����� �� ������ �� ���� ������, � ������� ���������� ���� h-����
using std::vector;
using std::map;
using std::ifstream;
using std::string;

int getDistMap(string distrFile, map<int, vector<double>> &distMap);
bool New_Chow_test(Segment &segment, const size_t polynomDegree, ProcessInfo *processInfo, map<int, vector<double>> &distMap);

void Chow_test(string distrFile, vector<Segment>& segment, vector<double> x, vector<double> y, int degPolynom);

#endif

#include <float.h> //DBL_MIN
#include "EstimatorLS.h"
#include "../../../Log/errors.h"
/*
double estimateNewYexp(const double x, ProcessInfo* processInfo, int ind_segm)
{
    return processInfo->getValexp(x, ind_segm);
}*/

double estimateNewYpoly(const double x, Segment &segment, size_t polynomDegree)
{
	return LinearRegr(x, segment.params, polynomDegree);
}

bool TestNextResidualAsGaussianOutlier(Segment &segment, size_t polynomDegree, ProcessInfo *processInfo, double newSegmentSelectorCoefficient)
{
	if (segment.end > (int)processInfo->x.size() - 1)
		return false;

	int xNext = segment.end;
	double yNext = processInfo->y[xNext];
	double xTmp = processInfo->x[xNext];
	double yE = estimateNewYpoly(xTmp, segment, polynomDegree);

	double delta = fabs(yNext - yE);
	double threshold = newSegmentSelectorCoefficient * segment.standartDeviation;
	if (delta <= threshold)
	{
		return true;
    }
    return false;
}

/*
	@brief  Decides if next point is suitable for current segment, if yes adds it and recounts parameters of the segment
	@param  segment -- current segment
	@param  obj -- data structure with all information about task
	@param  ind_segm -- index of segment we work with
	@param  processInfo -- special structure with full information about parameters of the process
	@return Returns true if possible to add point to current segment, in other case returns false
*/
void addPoint(Segment &segment, size_t polynomDegree, const std::vector< double > &x, const std::vector< double > &y)
{
    ++segment.end;
	InitSegment(segment, x, y, polynomDegree);
}

///TODO!!! ��� �� ���� ���������� �������� �������� �������
/// ������ param.size() (� �������� ���� ����� ��� degPolynom)
/// �.�. ��� ��������� �������������� ������
/*
    @brief  Estimates one value for linear regression
    @param  x -- value
    @param  param -- pair of parameters for segment
    @return Returns value of linear regression with set parameters
*/
double LinearRegr(double x, const std::vector<double> param, size_t degPolynom)
{
	// ���������� ����� �������� �������� degPolynom + 1 == param.size()
	// ����� ����� ����� ������ degPolynom, ��� ��� ����������� ������������ ��������
	// ��� ������������� �������� �������������� degPolynom + 1
    double y = param[0];
    double tmpX = x;
	for (size_t i = 1; i < degPolynom + 1; ++i)
    {
        y += param[i] * tmpX;
        tmpX *= x;
    }
    return y;
    //return param[0] + param[1] * x + param[2] * x * x + param[3] * x * x * x + param[4] * x * x * x * x + param[5] * x * x * x * x * x;
    //return param[0] * x + param[1];
}

double EvaluateL2DistancePolyRegression(const std::vector<double> &x, const std::vector<double> &y, const size_t& begin, const size_t &end, const std::vector<double> polynomParams, size_t degPolynom)
{
	double variance = 0;
	for (size_t i = begin; i != end; ++i)
	{
		variance += pow(y[i] - LinearRegr(x[i], polynomParams, degPolynom), 2);
	}
	///NB unbiased estimate!
	return variance / (end - begin - 2);
}

/*
    @brief  Estimates sum of elemets of array
    @param  x -- array
    @param  begin, end -- boundaries in array
    @return Returns sum of all elements in x array from begin index to end
*/
double Sum(const std::vector< double > &x, size_t begin, size_t end)
{
    double res = 0;
    for (size_t i = begin; i != end; ++i)
    {
        res += x[i];
    }
    return res;
}

/*
    @brief  Estimates average of elemets of array
    @param  x -- array
    @param  begin, end -- boundaries of array
    @return Returns first moment
*/
double FirstMoment(const std::vector<double> &x, size_t begin, size_t end)
{
    return Sum(x, begin, end) / (end - begin);
}

/// ���������� ������� ������� ���������� ��������� � ���
std::vector<std::vector<double> > BuildXMatr(const std::vector<double> &x, const int begin, const int end, const int degPolynom)
{
    std::vector < std::vector<double> > X(degPolynom + 1, std::vector<double>(degPolynom + 1));
    std::vector<double> xSumValues(2 * degPolynom);
	for (size_t j = 0; j < xSumValues.size(); ++j)
	{
		xSumValues[j] = 0;
	}
    for(int i = begin; i != end; ++i)
    {
        double xTmp = x[i];
        for(size_t j = 0; j < xSumValues.size(); ++j)
        {
            xSumValues[j] += xTmp;
            xTmp *= x[i];
        }
    }

    for(size_t i = 0; i < X.size(); ++i)
    {
        for(size_t j = 0; j < X.size(); ++j)
        {
            if(i == 0 && j == 0)
            {
                X[i][j] = end - begin;
            }
            else
            {
                X[i][j] = xSumValues[i + j - 1];
            }
        }
    }

	/*
	std::cout << "X:" << std::endl;
	for (size_t i = 0; i < X.size(); ++i)
	{
		for (size_t j = 0; j < X.size(); ++j)
		{
			std::cout << X[i][j]<< "	";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	*/
	return X;
}
/// ���������� ������ ����� ������� ���������� ��������� � ���
std::vector<double> BuildRightPart(const std::vector<double> &x, const std::vector<double> &y, const int begin, const int end, const int degPolynom)
{
    std::vector<double> b(degPolynom + 1);
	for (size_t j = 0; j < b.size(); ++j)
	{
		b[j] = 0;
	}

    for(int i = begin; i != end; ++i)
    {
        double yTmp = y[i];
        for(size_t j = 0; j < b.size(); ++j)
        {
            b[j] += yTmp;
            yTmp *= x[i];
        }
    }

	/*
	std::cout << "b:" << std::endl;
	for (size_t j = 0; j < b.size(); ++j)
	{
		std::cout << b[j] << "  ";
	}
	std::cout << std::endl;
	*/
    return b;
}

/// ����� ��� ������� ������� ���������� ���������
/*
    @brief  Using least squares method for linear regression estimates its parameters
    @param  x, y -- input arrays
    @param  begin, end -- boundaries of arrays
    @return Returns pair of parameters
*/
std::vector<double> FindParams(const std::vector<double> &x, const std::vector< double > &y, size_t begin, size_t end, size_t degPolynom)
{
    std::vector<std::vector<double> > A = BuildXMatr(x, begin, end ,degPolynom);
    std::vector<double> b = BuildRightPart(x, y, begin, end, degPolynom);
    for(size_t i = 0 ; i < A.size(); ++i)
    {
        A[i].push_back(b[i]);
    }

    std::vector<double> par(A.size());


    size_t n = A.size();
    for (size_t i = 0; i < n; i++)
    {
        double maxEl = std::abs(A[i][i]);
        size_t maxRow = i;
        for (size_t k = i + 1; k < n; k++)
        {
            if (std::abs(A[k][i]) > maxEl)
            {
                maxEl = std::abs(A[k][i]);
                maxRow = k;
            }
        }

        for (size_t k = i; k < n + 1;k++)
        {
            double tmp = A[maxRow][k];
            A[maxRow][k] = A[i][k];
            A[i][k] = tmp;
        }
        for (size_t k = i + 1; k < n; k++)
        {
            double c = -A[k][i] / A[i][i];
            for (size_t j = i; j < n + 1; j++)
            {
                if (i == j)
                {
                    A[k][j] = 0;
                }
                else
                {
                    A[k][j] += c * A[i][j];
                }
            }
        }
    }

	/*
	std::cout << "A1:" << std::endl;
	for (size_t i = 0; i < A.size(); ++i)
	{
		for (size_t j = 0; j < A[i].size(); ++j)
		{
			std::cout << A[i][j] << "  ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
	*/

    for (int i = n - 1; i >= 0; i--)
    {
        par[i] = A[i][n] / A[i][i];
        for (int k = i - 1;k >= 0; k--)
        {
            A[k][n] -= A[k][i] * par[i];
        }
    }

	/*
	std::cout << "A2:" << std::endl;
	for (size_t i = 0; i < A.size(); ++i)
	{
		for (size_t j = 0; j < A[i].size(); ++j)
		{
			std::cout << A[i][j] << "	";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;


	std::cout << "par:" << std::endl;
	for (size_t j = 0; j < par.size(); ++j)
	{
		std::cout << par[j] << " ";
	}
	std::cout << std::endl;
	*/

    return par;
}

/*
    @brief  Estimates first moment for all elemets of array
    @param  x,y -- two arrays of input distributions
    @param  begin, end -- boundaries of arrays
    @param  a -- parameter of distribution
    @return
*/
double S(double a, const std::vector<double> &x, const std::vector<double> &y, size_t begin, size_t end)
{
    double Ey = FirstMoment(y, begin, end);
    double Ex = FirstMoment(x, begin, end);

    double res = 0;
    double sum = 0;

    size_t i;

    for (i = begin; i != end; ++i)
    {
        res += pow((y[i] - Ey), 2);
        sum += pow((x[i] - Ex), 2);
    }

    res += a * a * sum;
    return res;
}

/*
    @brief  Estimates parameters of new segment and initializes it
    @param  x,y -- two arrays with input distributions
    @param  segment -- data structure
*/
void InitSegment(Segment &segment, const std::vector< double > &x, const std::vector< double > &y, const int degPolynom)
{
    std::vector<double> parameters = FindParams(x, y, segment.begin, segment.end, degPolynom);
	segment.params = parameters;
	segment.standartDeviation = sqrt(EvaluateL2DistancePolyRegression(x, y, segment.begin, segment.end, segment.params, degPolynom));
}




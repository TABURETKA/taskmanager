#include "../../../Log/errors.h"
#include "ChowTest.h"

/*
    @brief  Evaluates distance between two vector of data
    @param  x, y -- arrays with fisher distribution data
    @param  s -- segment with information about approximation line
    @param  ind_segm -- index of segment we work with
    @param  processInfo -- structure with information about process
*/
double getDistance(vector<double> x, vector<double> y, Segment s,  int degPolynom)
{
    double ESS = 0;
    std::vector<double> par = s.params;
    for (int i = s.begin; i < s.end; ++i)
    {
		ESS += pow((LinearRegr(x[i], par, degPolynom) - y[i]), 2);
    }
    return ESS;
}

bool readFisherDist(ifstream &inputStream, map<int, vector<double>>& m)
{
    int rows;
	if (!(inputStream >> rows))
        return false;

    int degs;
	if (!(inputStream >> degs))
        return false;

	vector<double> vc;
	for (int i = 0; i < rows; ++i)
    {
        int numDegreesOfFreedom;
		vc.clear();

        if (!(inputStream >> numDegreesOfFreedom))
            return false;

		for (int j = 0; j < degs; ++j)
		{
			double value;
			if (!(inputStream >> value))
				return false;
			vc.push_back(value);
		}
        m[numDegreesOfFreedom] = vc;
    }
    return true;
}

double getFval(int degr1, int degr2, map<int, vector<double>> &t, int numrows, int numdegs)
{
    int d1 = degr1;
    int d2 = degr2-1;

	if (degr1 >= numrows)
    {
		d1 = numrows - 1;
    }

	if (d2 >= numdegs)
    {
		d2 = numdegs - 1;
    }

    vector<double> tmp = t[d1];
    return tmp[d2];
}

int getDistMap(string distrFile, map<int, vector<double>> &distMap)
{
	ifstream fIn(distrFile.c_str());

	/// ������ ������ �� ����� ������������� ������.
	if (!readFisherDist(fIn, distMap))
	{
		return _ERROR_WRONG_ARGUMENT_;
	}
	return _RC_SUCCESS_;
}

bool New_Chow_test(Segment &segmentLeft, const size_t polynomDegree, ProcessInfo *processInfo, map<int, vector<double>> &distMap)
{
	int endl = (int)processInfo->x.size() - 1;
	if (segmentLeft.end > endl)
		return false;

	int rightSegmentBegin = segmentLeft.end;
	int leftSegmentSize = segmentLeft.end - segmentLeft.begin;

	int rightSegmentEnd = rightSegmentBegin + leftSegmentSize;
	if (rightSegmentEnd > endl + 1)
	{
		rightSegmentEnd = endl + 1;
	}

	if (rightSegmentEnd - rightSegmentBegin < 7) // min num of points to estimate gaus params
	{
		// ������ true ������ ������ ����� ��������� � �������, ��� ��� � ������ �������� ����� ���� �����
		return true;
	}
	/// ������ ������ �������.
	Segment segmentRight(rightSegmentBegin, rightSegmentEnd);

	/// ������ ����� �������.
	Segment segmentFull(segmentLeft.begin, segmentRight.end);
	int n = segmentFull.end - segmentFull.begin;

	/// ��������� �������� ������� � �����������
	InitSegment(segmentRight, processInfo->x, processInfo->y, polynomDegree);
	InitSegment(segmentFull, processInfo->x, processInfo->y, polynomDegree);

	/// ������� SSR � ������� ��������
	double distFirst = getDistance(processInfo->x, processInfo->y, segmentLeft, polynomDegree);

	/// ������� SSR � ������� ��������
	double distSecond = getDistance(processInfo->x, processInfo->y, segmentRight, polynomDegree);

	/// ������� SSR � ������������� ��������
	double distFull = getDistance(processInfo->x, processInfo->y, segmentFull, polynomDegree);

	/// ������� F-����������
	double F = ((distFull - distFirst - distSecond) / (polynomDegree + 1)) / ((distFirst + distSecond) / (n - 2 * (polynomDegree + 1)));
	/// ������� ������������� ������
	int f1 = polynomDegree + 1;
	int f2 = n - 2 * (polynomDegree + 1);
	double a = getFval(f1, f2, distMap, distMap.size(), distMap[1].size());

	if (F > a)
	{
		//����� ������������ ��������� �����
		//������ � ������� ������� � �� ���������
		return false;
	}
	// ������ true ������ ������ ����� ��������� � �������, ��� ��� ��� �� ����� ������������ ���������
	return true;
}
/*
    @breif  Evaluates Chow STATISTIC
    @param  distrFile -- name of file with Fisher distribution data
    @param segments -- vector of segments before merging
    @param  x, y -- arrays with input distribution data
    @param  degPolynom -- degree of aproximation polynom
    @param  processInfo -- structure with information about process
*/
void Chow_test(string distrFile, vector<Segment> &segments, vector<double> x, vector<double> y, int degPolynom)
{
    ifstream fIn(distrFile.c_str());
	map<int, vector<double>> distMap;

    vector<Segment> newSegments;

	/// ������ ������ �� ����� ������������� ������.
	readFisherDist(fIn, distMap);
    size_t i = 0;

    /// ���� ������������ ���������.
    bool stateChanged = false;
    double distFull, distFirst, distSecond;
    do
    {
        i = 0;
        stateChanged = false;
        /// �������� �� ���������.
        while (segments.size() > i + 1)
        {
            /// ������ �������, ��� ����������� ���� �������� ��������� ���������.
            Segment s(segments[i].begin, segments[i + 1].end);

            /// ��������� ������� ������� � �����������
            InitSegment(s, x, y, degPolynom);

            /// ������� SSR � ������� ��������
			distFirst = getDistance(x, y, segments[i], degPolynom);

            /// ������� SSR � ������� ��������
			distSecond = getDistance(x, y, segments[i + 1], degPolynom);

            /// ������� SSR � ������������� ��������
			distFull = getDistance(x, y, s, degPolynom);

            /// ������� F-����������
            double F = ((distFull - distFirst - distSecond) / (degPolynom + 1)) / ((distFirst + distSecond) / (s.end - s.begin - 2 * (degPolynom + 1)));

			int degree = (s.end - s.begin) - 2 * (degPolynom + 1);

            /// ������� ������������� ������
			double a = getFval(degree, 2 * degPolynom + 1, distMap, distMap.size(), distMap[1].size());
            /// ��������� ��������
            if (F < a)
            {
                segments.erase(segments.begin() + i);
                segments[i] = s;

                stateChanged = true;
                break;
            }
            else
            {
                i++;
            }
        }
    } while (stateChanged && segments.size() > 1);
}

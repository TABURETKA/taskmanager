#ifndef ESTIMATOR_BASE_H__
#define ESTIMATOR_BASE_H__

#include <utility>
#include <vector> /* vector */
#include <algorithm> /* max */
#include <assert.h>
#include <cmath>
#include <fstream>
#include <iterator>
#include <map>

#include <iostream>

//#include "ConfigParse.h"
//#define MIN_NUMBER_OF_POINTS_IN_SEGMENT 6 // minimum amount of points in one segment
//#include "../../../Model/Model.h"
#include "../../../Models/Model.h" //ModelTypes
using std::vector;
using std::ofstream;

typedef double(*RegressionVal) (const double x, const std::vector<double> &param, const int deg);

///TODO!!! ������������� �� ������ � ����� ��� ������ �� ����� �������, �� ������ �� stand-alone class!
struct Segment
{
public:
    Segment(int begin_, int end_)
        : begin(begin_)
        , end(end_)
		, standartDeviation(0)
    {}

public:
    int    begin, end;
    std::vector<double> params;
	double standartDeviation;
};

typedef struct parameters
{
    std::string input_file_distr;   // name of the file with distribution values
	bool chow_test_flag;
    std::string f_distr_file;
    parameters()
    {
        input_file_distr = "";
		chow_test_flag = 0;
        f_distr_file = "";
    }
} parameters_t;

double LinearRegr(double x, const std::vector<double> param, size_t deg);


///TODO!!! ������������� �� ������ � ����� ��� ������ �� ����� �������, �� ������ �� stand-alone class!
class ProcessInfo
    {

    public:

        std::vector<double> x, y;

        ProcessInfo() {};
        ~ProcessInfo()
        {
            x.clear();
            y.clear();
        };

		/*
        double getVal(double x, size_t degree, const vector<double> & params) const
        {
			return LinearRegr(x, params, degree);
        }*/
    };

bool TestNextResidualAsGaussianOutlier(Segment &segment, size_t polynomDegree, ProcessInfo *processInfo, double newSegmentSelectorCoefficient);
void addPoint(Segment &segment, size_t polynomDegree, const std::vector< double > &x, const std::vector< double > &y);
double estimateNewYpoly(const double x, Segment &segment, ProcessInfo* processInfo, size_t polynomDegree);

/**###**/
double S(double a, std::vector< double > const &x, std::vector< double > const &y, size_t begin, size_t end);
std::vector<double> FindParams(std::vector< double > const &x, std::vector< double > const &y, size_t begin, size_t end, size_t degPolynom);
double FirstMoment(std::vector< double > const &x, size_t begin, size_t end);
double Sum(std::vector< double > const &x, size_t begin, size_t end);
double EvaluateL2DistancePolyRegression(const std::vector<double> &x, const std::vector<double> &y, const size_t& begin, const size_t &end, const std::vector<double> polynomParams, size_t degPolynom);
void InitSegment(Segment &segment, const std::vector< double > &x, const std::vector< double > &y, const int deg);
void SaveResults(ofstream &resultStream, vector<Segment> s, vector<double> x, vector<double> y);
#endif

#include "../../CommonStuff/Defines.h" /* isNAN_double */
#include "../../Log/Log.h"
#include "../../Log/errors.h"
#include "../../Models/Model.h" //ModelTypes
#include "../../Params/ParamsSet.h"//IParams
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../../DataCode/DataModel/DataMetaInfo.h" //DataInfo
#include "../../DataCode/Grammar/DataManagerGrammar.h" //DELIMITER_DATASET_ITEMS_SEMANTICS_IN_TRACK_NAME
#include "ModifiedExpRegressionEstimator.h"
#include <sstream> /* stringstream */
#include <fstream> /* ofstream */

int allocateMatrix(size_t dim, double **& ptrMatrix);
void freeMatrix(size_t dim, double ** & ptrMatrix);

int Gauss(size_t dim,  double **& matrix, double*& y);
//#define _DEBUG_
///DEBUG START
int saveDataToFile(std::string fName, std::stringstream& output);

int saveDataToFile(std::string fName, std::stringstream& output)
{
    static size_t counter=0;
    ++counter;
    std::stringstream ss;
    ss << fName << counter;
    fName = ss.str();
    std::ofstream outputFile( fName.c_str(), std::ios::out);
    if (!outputFile)
    {
        std::cout << "Failed to write the result to file:" << fName << std::endl;
        return -1;
    }
    outputFile << output.str();
    return _RC_SUCCESS_;
}
///DEBUG END
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace SimpleEstimators;
///TODO!!! move to matrix stuff!
int allocateMatrix(size_t dim, double **& ptrMatrix)
{
    double ** matrix = new double*[dim];
    if(! matrix)
    {
        return _ERROR_NO_ROOM_;
    }
    std::memset(matrix, 0, dim*sizeof(double*));
    for(size_t i=0; i < dim; ++i)
    {
        matrix[i] = new double[dim];
        if(! matrix[i])
        {
            while(--i)
            {
                if(matrix[i])
                {
                    delete [] matrix[i];
                }
            }//end while
            delete [] matrix;
            return _ERROR_NO_ROOM_;
        }
        std::memset(matrix[i], 0, dim*sizeof(double));
    }
    ptrMatrix = matrix;
    return _RC_SUCCESS_;
}
///TODO!!! move to matrix stuff!
void freeMatrix(size_t dim, double ** & ptrMatrix)
{
//    double ** matrix = ptrMatrix;
    if(! ptrMatrix) return;
    for(size_t i=0; i < dim; ++i)
    {
        if(ptrMatrix[i])
            delete [] ptrMatrix[i];
    }
    delete [] ptrMatrix;
    ptrMatrix = NULL;
}

///TODO!!! move to matrix stuff!
int Gauss(size_t dim,  double **& matrix, double*& y)
{
    int i, j, k;
    /* //DEBUG start
          std::stringstream ss4Matrix;
        for(i = 0; i < dim; ++i)
        {
            for(j = 0; j < dim; ++j)
            {
                ss4Matrix << matrix[i][j] << " ";
            }
            ss4Matrix << y[i] << std::endl;
        }
        saveDataToFile("MatrixBefore", ss4Matrix);
    *///DEBUG end

    // straight
    for (i = 0; i < (int)dim - 1; ++i)
    {
        for (j = i + 1; j < (int)dim; ++j)
        {
            for (k = i + 1; k < (int)dim; ++k)
            {
                matrix[j][k] -= matrix[i][k] * matrix[j][i] / matrix[i][i];
            }
            y[j] -= y[i] * matrix[j][i] / matrix[i][i];
            matrix[j][i] = 0;
        }
    }

    // backward
    for (i = (int)dim - 1; i > 0; --i)
    {
        for (j = i - 1; j >= 0; --j)
        {
            y[j] -= y[i] * matrix[j][i] / matrix[i][i];
            matrix[j][i] = 0;
        }
    }

    for (i = 0; i < (int)dim; ++i)
    {
        y[i] /= matrix[i][i];
        matrix[i][i] = 1.;
    }

    /*//DEBUG start
          std::stringstream ss4Matrix;
        for(i = 0; i < dim; ++i)
        {
            for(j = 0; j < dim; ++j)
            {
                ss4Matrix << matrix[i][j] << " ";
            }
            ss4Matrix << y[i] << std::endl;
        }
        saveDataToFile("MatrixAFTER", ss4Matrix);
    *///DEBUG end
    return _RC_SUCCESS_;
}

SimpleEstimators::ModifiedExpRegressionEstimator::ModifiedExpRegressionEstimator(Logger*& log)  : FakeSimpleEstimator(log)
{
    /** so far noting to allocate */
    m_set = SimpleEstimatorsTypes::ModifiedExpRegressionEstimator;
    /** m_task = NULL; m_logger  = log; already initialized in FakePointEstimator's CTOR */
    m_model  = NULL;
    m_estimatorParams  = NULL;
    m_setOfParamsEstimates = NULL;
}
SimpleEstimators::ModifiedExpRegressionEstimator::~ModifiedExpRegressionEstimator()
{
    m_data.clear();
    delete m_estimatorParams;
    delete m_setOfParamsEstimates;
}

/** generate DataInfo */
int SimpleEstimators::ModifiedExpRegressionEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    if(!m_setOfParamsEstimates)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No estimates so far.");
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** Total of DataInfo to be created */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** m_estimatesDim == 2 == coef[o] * exp(- coef[1] * t) */
    /**  ��� ����� ����� ���������,
    ����������� �� ������, �����
     ��� ��������� ������ m_estimatesDim ����� ���������������
     ��� ����������� ��� ������������� ������� di_0_dataSetSizes
     � ������������� ������� data.
     ���� �� ������ ���,
     ����� m_estimatesDim ����� ���������������
     ��� ���������� �������� di_0_dataSetSizes (������ ����������� 1)
     � ���������� �������� data (������ ����������� 1).
    */
    size_t di_0_dimension = m_estimatesDim;
    size_t sizeOfSingleArray = 1;
    size_t modelDim = 0;
    if(m_model)
    {
        /**
        No need to check if (dynamic_cast<IModelPolynomial*>(model))
        Because in setModel(model,log) we have checked the model type is 'PolynomialModel'.
        */
        dynamic_cast<IModelPolynomial*>(m_model)->getDimension(modelDim);
        if(modelDim >= m_estimatesDim)
        {
            di_0_dimension = 1;
            sizeOfSingleArray = m_estimatesDim;
        }
    }
    /** allocate room for _trackSizes (each track size == ModifiedExpRegression model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    /** init _trackSizes (each track size == 1 - single parameter of ModifiedExpRegression model ) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        di_0_dataSetSizes[i] = sizeOfSingleArray;
    }
/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        IParams * params = m_setOfParamsEstimates->getParamsCopy(i);
        if(! params)
        {
            while(i)
            {
                --i;
                delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;
            std::stringstream tmp_ss;
            tmp_ss << ISimpleEstimator::simpleEstimatorNames[m_set] << ": " << i << "-th params estimates is missing in m_setOfParamsEstimates.";
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, tmp_ss.str());
            return _ERROR_WRONG_OUTPUT_DATA_;
        }
        size_t size = params->getSize();
        if(size != di_0_dataSetSizes[i])
        {
            delete params;
            while(i)
            {
                --i;
                delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;
            std::stringstream tmp_ss;
            tmp_ss << ISimpleEstimator::simpleEstimatorNames[m_set] << ": mismatch of " << i << "-th params dimension in m_setOfParamsEstimates and dataSetSizes's dimension to save those params.";
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, tmp_ss.str());
            return _ERROR_WRONG_OUTPUT_DATA_;
        }
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            delete params;
            while(i)
            {
                --i;
                delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        for(size_t j= 0; j < di_0_dataSetSizes[i]; ++j)
        {
            data[i][j] = NAN;
            params->getParam(j, data[i][j]);
        }
        delete params;
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** m_estimatesDim == 2 == coef[0] and coef[1]! */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = ISimpleEstimator::simpleEstimatorNames[SimpleEstimatorsTypes::ModifiedExpRegressionEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_DATASETNAME;
std::cout << di[0]->m_dataSetName << std::endl;
    di[0]->m_dataModelName = m_model->toString();
std::cout << di[0]->m_dataModelName << std::endl;
    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }
    if(di[0]->m_dimension == m_estimatesDim)
    {
        di[0]->m_dataSetTrackNames[0] = MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_0;
std::cout << di[0]->m_dataSetTrackNames[0] << std::endl;
        di[0]->m_dataSetTrackNames[1] = MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_1;
std::cout << di[0]->m_dataSetTrackNames[1] << std::endl;
    }
    else if(di[0]->m_dimension == 1)
    {
        di[0]->m_dataSetTrackNames[0] = std::string(MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_0) +
                                        DELIMITER_DATASET_ITEMS_SEMANTICS_IN_TRACK_NAME +
                                        std::string(MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_1);
std::cout << di[0]->m_dataSetTrackNames[0] << std::endl;
    }


    /** put _trackSizes (each track size == ModifiedExpRegression model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    //We have already put data into di[0]! See CTOR di[0]->dataSet = data;
    return _RC_SUCCESS_;
}

/** ModifiedExpRegressionEstimator stuff */
int SimpleEstimators::ModifiedExpRegressionEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
    iteratorParamsMap itParam = mapParams.find(grammarModifiedExpRegressionEstimatorParams[IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorSematics]);
    double floor=NAN;
    enum ModifiedExpRegressionParamsSemantics floorParamSemantics = FAKE_FLOOR_PARAM_SEMANTICS;
    if (itParam != mapParams.end())
    {
        /** ��� ����� ����� �������� ������� ��������� ����������� map-����� �� ������ ����������,
        ������� �������� �� ���� �������� (LAST_VALUE, LAST_POSITIVE_VALUE, AVEARGE_VALUE, ... )!
        */
        size_t tmpInt;
        int rc = strTo<size_t>(itParam->second.c_str(), tmpInt); //OLD          floorParamSemantics = (enum ModifiedExpRegressionParamsSemantics) atoi(itParam->second.c_str());
        mapParams.erase(itParam);
        if((rc != _RC_SUCCESS_) || (tmpInt >= FAKE_FLOOR_PARAM_SEMANTICS))
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong floor semantics!");
            return _ERROR_WRONG_ARGUMENT_;
        }

        floorParamSemantics = (enum ModifiedExpRegressionParamsSemantics)tmpInt; //OLD (enum ModifiedExpRegressionParamsSemantics) atoi(itParam->second.c_str());

        switch(floorParamSemantics)
        {
        case floorValue:
        {
            itParam = mapParams.find(grammarModifiedExpRegressionEstimatorParams[IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorValue]);
            /** we are pessimists */
            int rc = _ERROR_NO_INPUT_DATA_;
            if (itParam != mapParams.end())
            {
                rc = strTo<double>(itParam->second.c_str(), floor); //OLD          floor = atof(itParam->second.c_str());
                mapParams.erase(itParam);
            }
            if(rc != _RC_SUCCESS_)
            {
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No floor value for " + grammarModifiedExpRegressionEstimatorParams[IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorSematics] + " semantics.");
                return _ERROR_WRONG_ARGUMENT_;
            }
        }
        break;
        case floorItemIndexInData:
        {
            itParam = mapParams.find(grammarModifiedExpRegressionEstimatorParams[IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorValue]);
            /** we are pessimists */
            int rc = _ERROR_NO_INPUT_DATA_;
            if (itParam != mapParams.end())
            {
                rc = strTo<double>(itParam->second.c_str(), floor); //OLD          floor = atoi(itParam->second.c_str());
                mapParams.erase(itParam);
            }
            if(rc != _RC_SUCCESS_)
            {
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No index to evaluate floor value for " + grammarModifiedExpRegressionEstimatorParams[IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorSematics] + " semantics.");
                return _ERROR_WRONG_ARGUMENT_;
            }
        }
        break;
        case floorLastItemIndexInData:
        /** nothing to parse so far */
        break;
        case floorLastPositiveItemIndexInData:
        /** nothing to parse so far */
        break;
        }
    }
    else
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong value of " + grammarModifiedExpRegressionEstimatorParams[IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorSematics]);
        return _ERROR_WRONG_ARGUMENT_;
    }
    IParams * params= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, IndexesOfModifiedExpRegressionEstimatorParams::FakeIndex_ModifiedExpRegressionEstimatorParams);
    if (!params)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to keep Estimator Params");
        return _ERROR_NO_ROOM_;
    }
    params->setParam(IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorSematics, floorParamSemantics);
    switch(floorParamSemantics)
    {
        case floorValue:
        case floorItemIndexInData:
            params->setParam(IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorValue, floor);
    }
    int rc = this->setEstimatorParams(params);
    return rc;
}
/** IAA: ��� ���� ����� ���� �� ����������, �.�. ����� �������������� ���������� ��������� ����� ����������� ���� �� �������� */
int SimpleEstimators::ModifiedExpRegressionEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    delete m_estimatorParams;
    m_estimatorParams = NULL;
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

int SimpleEstimators::ModifiedExpRegressionEstimator::setEstimatorParams(IParams*& params)
{
    size_t size = params->getSize();
    if (size != FakeIndex_ModifiedExpRegressionEstimatorParams)
    {
        delete params;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong Estimator Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
    delete m_estimatorParams;
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}
int SimpleEstimators::ModifiedExpRegressionEstimator::setModel(IModel* model)
{
/// Polynomial Model valid ONLY!
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
    model->getModelType(mt);
    if(mt != IModel::ModelsTypes::PolynomialModel)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    size_t modelDim = 0;
    /**
    No need to check : if(dynamic_cast<IModelPolynomial*>(model))...
    Because we have checked the model type is 'PolynomialModel'.
    */
    dynamic_cast<IModelPolynomial*>(model)->getDimension(modelDim);
    if(modelDim < m_estimatesDim)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Model Dimension LT expected to be set for ModifiedExpRegressionEstimator.");
        return _ERROR_WRONG_ARGUMENT_;
    }
    if(modelDim > m_estimatesDim)
    {
        std::stringstream tmp_ss;
        tmp_ss << ISimpleEstimator::simpleEstimatorNames[m_set] << ": Model Dimension GT expected to be set for ModifiedExpRegressionEstimator! Only first " << m_estimatesDim << " will be evaluated.";
        m_logger->log(ALGORITHM_4_LEVEL_INFO, tmp_ss.str());
    }
    m_model = model;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
    return rc;
}

int SimpleEstimators::ModifiedExpRegressionEstimator::setParamsSet()
{
    /** ��� �������� ����� ����
    ������ ����� IParamsSet::createEmptyParamsSet
    �� ���������� �������� m_model!
    ������ ����� ���������� = ������� ������ Params1D.
    */
    IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
    if (pst)
    {
//OLD        pst->setModel(m_model, m_logger);
        this->m_setOfParamsEstimates = pst;
    }
    return _RC_SUCCESS_;
}

int SimpleEstimators::ModifiedExpRegressionEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(IPointEstimator::pointEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];
//    std::cout << "di->m_dimension:" << di->m_dimension  << std::endl;
    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(IPointEstimator::pointEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
//    std::cout << "di->m_dataSetSizes[0]:" << di->m_dataSetSizes[0] << std::endl;
//    std::cout << "di->m_dataSet:" << di->m_dataSet << std::endl;

    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(IPointEstimator::pointEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
    }
#ifdef _DEBUG_
    std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}

int SimpleEstimators::ModifiedExpRegressionEstimator::getFloorValue(double& floor)
{
    double floorSemantics=0;
    int rc = m_estimatorParams->getParam(IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorSematics, floorSemantics);
    std::cout << "floorSemantics:" <<  floorSemantics << std::endl;
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No estimatorParam on floorSemantics.");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    switch((size_t)floorSemantics)
    {
    case ModifiedExpRegressionParamsSemantics::floorValue :
    {
        rc = m_estimatorParams->getParam(IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorValue, floor);
        if(rc != _RC_SUCCESS_)
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No floorValue in estimatorParam.");
            return _ERROR_NO_ESTIMATOR_PARAMS_;
        }
        return _RC_SUCCESS_;
    }
    break;
    case ModifiedExpRegressionParamsSemantics::floorItemIndexInData :
    {
        double indexOfItem = NAN;
        rc = m_estimatorParams->getParam(IndexesOfModifiedExpRegressionEstimatorParams::indexOfFloorValue, indexOfItem);
        if(rc != _RC_SUCCESS_)
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No index of floorValue in estimatorParam.");
            return _ERROR_NO_ESTIMATOR_PARAMS_;
        }
        if((indexOfItem < 0) || (indexOfItem >= m_data.size() ))
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Index of floorValue in estimatorParam is out of range.");
            return _ERROR_NO_ESTIMATOR_PARAMS_;
        }
        floor = m_data[(size_t)indexOfItem];
        return _RC_SUCCESS_;
    }
    break;
    case ModifiedExpRegressionParamsSemantics::floorLastItemIndexInData :
    {
        if(m_data.size() < DEFAULT_MIN_SIZE_OF_DATA_TO_BE_PROCESSED)
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No enough items into input data to define floorValue.");
            return _ERROR_NO_ESTIMATOR_PARAMS_;
        }
        floor = m_data[(size_t) (m_data.size() - 1) ];
        return _RC_SUCCESS_;
    }
    break;
    case ModifiedExpRegressionParamsSemantics::floorLastPositiveItemIndexInData :
    {
        double indexOfItem = NAN;
        for(int i = m_data.size() -1; i >=0; --i)
        {
            if(m_data[i] > 0.0f)
            {
                indexOfItem = i;
                break;
            }
        }
        if(isNAN_double(indexOfItem))
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No positive items into input data to define floorValue.");
            return _ERROR_NO_ESTIMATOR_PARAMS_;
        }
        floor = m_data[(size_t)indexOfItem];
        return _RC_SUCCESS_;
    }
    break;
    default:
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong floorSemantics in estimatorParam.");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
}

int SimpleEstimators::ModifiedExpRegressionEstimator::estimate(ptrInt threadInterruptSignal)
{
//    std::cout << "In estimate1:" << m_setOfParamsEstimates << " : m_setOfParamsEstimates.size:" << m_setOfParamsEstimates->getSetSize() << std::endl;

    if (m_data.size() == 0)
    {
        return _ERROR_NO_INPUT_DATA_;
    }
    if (! m_estimatorParams)
    {
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    double * coeffs=NULL;
    coeffs = new double[m_estimatesDim];
    if (!coeffs)
    {
        return _ERROR_NO_ROOM_;
    }
    std::memset(coeffs, 0, m_estimatesDim * sizeof(double));
//    std::vector<std::vector<double> > matrix(m_estimatesDim, std::vector<double>(m_estimatesDim, 0));
    double ** matrix=NULL;
    int rc = allocateMatrix(m_estimatesDim, matrix);
    if (rc != _RC_SUCCESS_)
    {
        delete [] coeffs;
        return rc;
    }
    size_t sizeOfDataHeadToBeProcessed = 0;
    /**
    ��� ����, ����� ��������������� ������� ���������������� ����������,
    ����� ���������� �� �.���������� � ������ �������� �������� �� ���� ��������,
    ��� ����� �������, ����� ������� ����� ������, ���� ��������� �������� floor,
    �� ������� "��������" ������ ��������, ������������� ����� ��������.
    */
    double floor = NAN;
    rc = getFloorValue(floor);
    if ((rc != _RC_SUCCESS_) || isNAN_double(floor))
    {
        freeMatrix(m_estimatesDim, matrix);
        matrix = NULL;
        delete [] coeffs; coeffs = NULL;
    }
    else
    {
        for (size_t i = 0; i < m_data.size(); ++i)
        {
            m_data[i] = m_data[i] - floor;
            if (m_data[i] > 0)
            {
                sizeOfDataHeadToBeProcessed++;
            }
        }
    }

    IParams * estimates= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, m_estimatesDim);
    if (!estimates)
    {
        freeMatrix(m_estimatesDim, matrix);
        matrix = NULL;
        delete [] coeffs; coeffs = NULL;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to keep Estimator's estimates");
        return _ERROR_NO_ROOM_;
    }
    /** if m_setOfParamsEstimates is not initilized yet
    let's init m_setOfParamsEstimates right now!
     */
    if(!m_setOfParamsEstimates)
    {
        rc = this->setParamsSet();
        if (rc != _RC_SUCCESS_)
        {
            delete estimates; estimates = NULL;
            freeMatrix(m_estimatesDim, matrix);
            matrix = NULL;
            delete [] coeffs; coeffs = NULL;
            return _ERROR_NO_ROOM_;
        }
    }
    m_setOfParamsEstimates->clear();
    if (sizeOfDataHeadToBeProcessed < DEFAULT_MIN_SIZE_OF_DATA_TO_BE_PROCESSED)
    {
        rc = _RC_SUCCESS_;
        estimates->setParam(0, DEFAULT_EXP_MULTIPLIER_IF_REGRESSION_FAILED);
        estimates->setParam(1, DEFAULT_MEAN_REVERTING_RATE_IF_REGRESSION_FAILED);
        if(m_setOfParamsEstimates->addParamsCopyToSet(m_model, estimates, m_logger) != true)
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Can not put DEFAULT value of estimate into Estimator's m_setOfParamsEstimates");
            rc = _ERROR_WRONG_OUTPUT_DATA_;
        }
        delete estimates; estimates = NULL;
        freeMatrix(m_estimatesDim, matrix);
        matrix = NULL;
        delete [] coeffs; coeffs = NULL;
        return rc;
    }
    /**
     ����� �������� ������ � �������������� ����������,
    ��������� ������ Y(t)=param[0] * exp(-param[1] * t) �������������� ���������������
    � �������� � ����
    ln(Y(t))= ln(param[0]) + (-param[1] * t)
    ����� ���� ����������� �����������
    SUM_1^dataSize {ln(Y(t)) - ln(param[0]) + param[1] * t}^2
    ��� ����� ������ ������� �� ���� ���������� ���������

    SUM_1^dataSize {ln(Y(t)} =  ln(param[0]) * dataSize - param[1] * SUM_1^dataSize {t}
    SUM_1^dataSize {t * ln(Y(t)} = ln(param[0]) * SUM_1^dataSize {t} - param[1] * SUM_1^dataSize {t^2}
    ���� ����, ��� ������, ������������ ������� ���������������� ����������
    ���� �� ���� �������� (������� � ������� ������� �������� ���������),
    ��� ����� ��� ��� �������� �������������,
    �� �� ����� ������ ������������ �������,
    �.�. ����� �������� �� ��� �������� �� ����� �������� �����
    "���������" �� floor ����� ��������������!
    �.�. �������� �������������!
    */
//    std::vector<double> x_i(dataSize, 1.);
    double * LS_solution = new double[sizeOfDataHeadToBeProcessed];
    if(! LS_solution)
    {
        delete estimates; estimates = NULL;
        freeMatrix(m_estimatesDim, matrix);
        matrix = NULL;
        delete [] coeffs; coeffs = NULL;

        return _ERROR_NO_ROOM_;
    }
    for(size_t i=0; i < sizeOfDataHeadToBeProcessed; ++i)
    {
        LS_solution[i] = 1.0;
    }
    /** make matrix and right-side of
     the system of normal equations
    */
    double elem;
    for (int i = 0; i < (int)m_estimatesDim; ++i)
    {
        elem = 0;
        for (int j = 0; j < (int)sizeOfDataHeadToBeProcessed; ++j)
            elem += LS_solution[j];
        for (int j = i; j >= 0; --j)
            matrix[i - j][j] = elem;

        int k = 0;
        for (int j = 0; j < (int)m_data.size(); ++j)
        {
            if (m_data[j] > 0)
            {
                coeffs[i] += log(m_data[j]) * LS_solution[k];
                LS_solution[k] *= (double)j;
                k++;
            }
        }
    }
    for (int i = 1; i < (int)m_estimatesDim; ++i)
    {
        elem = 0;
        for (int j = 0; j < (int)sizeOfDataHeadToBeProcessed; ++j)
            elem += LS_solution[j];
        for (int j = 0; j < (int)m_estimatesDim - i; ++j)
            matrix[j + i][m_estimatesDim - 1 - j] = elem;

        for (int j = 0; j < (int)sizeOfDataHeadToBeProcessed; ++j)
        {
            LS_solution[j] *= (double)j;
        }
    }
    Gauss(m_estimatesDim,  matrix, coeffs);
    /** evaluate exp's multiplier parameter
    ��������� ������ Y(t)=param[0] * exp(-param[1] * t)
    �� ������������� �������� param[1] �������� - ������!
    ��. ������������ �������!
    */
    if(coeffs[1] > 0.0f)
    {
        coeffs[0] = NAN;
        coeffs[1] = NAN;
    }
    else
    {
        coeffs[0] = exp(coeffs[0]);
    }

    /** save estimates! */
    for(size_t i=0; i < m_estimatesDim; ++i)
    {
        estimates->setParam(i, coeffs[i]);
    }
    rc = _RC_SUCCESS_;
    if(m_setOfParamsEstimates->addParamsCopyToSet(m_model, estimates, m_logger) != true)
    {
        rc = _ERROR_WRONG_OUTPUT_DATA_;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Can not add estimates to put into Estimator's m_setOfParamsEstimates.");
    }
//    std::cout << "In estimate2:" << m_setOfParamsEstimates << " : m_setOfParamsEstimates.size:" << m_setOfParamsEstimates->getSetSize() << std::endl;
#ifdef _DEBUG_
    IParams * tmp_params = m_setOfParamsEstimates->getParamsCopy(0);
    size_t size=0;
    tmp_params->getParamsSize(size);
///    std::cout << "In ModExp estimates(). params.size" <<  size << std::endl;
    double tmp_double0=NAN;
    double tmp_double1=NAN;
    tmp_params->getParam(0, tmp_double0);
    tmp_params->getParam(1, tmp_double1);
    std::cout << "In ModExp params estimates :" <<  size << ":" << tmp_double0 << ":"  << tmp_double1 << std::endl;
    delete tmp_params;
    /**/
    std::stringstream tmp_ss;
    tmp_ss << "Trend:  " << coeffs[0] << " * exp( " << coeffs[1] << " )";
    m_logger->log(ALGORITHM_4_LEVEL_INFO, tmp_ss.str());
#endif
    delete LS_solution; LS_solution = NULL;
    delete estimates; estimates = NULL;
    freeMatrix(m_estimatesDim, matrix);
    matrix = NULL;
    delete [] coeffs; coeffs = NULL;

    return _RC_SUCCESS_;
}


int SimpleEstimators::ModifiedExpRegressionEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}

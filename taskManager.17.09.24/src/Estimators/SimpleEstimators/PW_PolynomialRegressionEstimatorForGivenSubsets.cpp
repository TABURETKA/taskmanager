#include "../../CommonStuff/Defines.h" /* isNAN_double */
#include "../../Log/Log.h"
#include "../../Log/errors.h"
#include "../../Models/Model.h" //ModelTypes
#include "../../Params/ParamsSet.h"//IParams
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../../Task/Task.h" /* grammarTaskStepConfig[] */
#include "../../DataCode/Grammar/DataManagerGrammar.h" /* DataManagerGrammar */
//OLD #include <stdlib.h> //atof()
#include "../../DataCode/DataModel/DataMetaInfo.h" //DataInfo
#include "PW_PolynomialRegressionEstimatorForGivenSubsets.h"
#include <sstream> /* stringstream */

#define _DEBUG_

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace SimpleEstimators;

SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::PW_PolynomialRegressionEstimatorForGivenSubsets(Logger*& log)  : FakeSimpleEstimator(log)
{
 /** so far noting to allocate */
  m_set = SimpleEstimatorsTypes::PW_PolynomialRegressionEstimatorForGivenSubsets;
  m_estimatesDim = 0;
  /** m_task = NULL; m_logger  = log; already initialized in FakePointEstimator's CTOR */
  m_model                     = NULL;
  m_estimatorParams           = NULL;
  m_setOfParamsEstimates      = NULL;
  m_nestedRegressionEstimator = NULL;
}
SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::~PW_PolynomialRegressionEstimatorForGivenSubsets()
{
    m_data.clear();
    delete m_estimatorParams;
    delete m_setOfParamsEstimates;
    delete m_nestedRegressionEstimator;
}

    /** generate DataInfo */
int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    /** Total of DataInfo to be created = 1
    */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    /**
    totalOfSubsets - �������� ����� ����������, ������� ����� ������������������
    �� ������ ��������� ������ ������ � ��������� ���������, ������������ ������� ������� ������,
    �� ������� ������ ��������� ������������� ������
    */
    size_t di_0_dimension = m_setOfParamsEstimates->getSetSize();
    if(!di_0_dimension)
    {
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No info on boundaries of subsets into m_setOfParamsEstimates!");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    /** allocate room for ptrs to _trackSizes (right now each track size == ModifiedExpRegression model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    /** init _trackSizes (each track size == m_estimatesDim == ������������ ������������ �������������� ������ ) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        /** m_estimatesDim == ������������ ������������ �������������� ������,
        ������� ������ ���� �������������� ��� �������� ����� ���������� ��������� �� ������,
        ��� ����������� � ���� ������� ������������ ��� ����������� �������������� ������ � ���� ��� �������
        ������������� ����� �� ��������� ������
       */
        di_0_dataSetSizes[i] = m_estimatesDim;
    }
     /** ��� ��� DataInfo->dataSet ������ double const ** const,
     ��� ����� ���������������� ��� ��������,
     ��� ������� � ������������� ��������� ������ data ����, ��� ��������� di[0]
     */
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            while(i){
              --i;
              delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        /** fill in 1-D arrays in DI with estimates  */
///TODO!!! ��������� ����� ��������� ������������ ������������� ������ ��� i-�� �����?
        IParams * params = m_setOfParamsEstimates->getParamsCopy(i);
        for(size_t j=0; j < di_0_dataSetSizes[i]; ++j)
        {
            data[i][j] = NAN;
            params->getParam(j, data[i][j]);
#ifdef _DEBUG_
std::cout << "In RW_regression getEstimatesAs.. " << data[i][j] << std::endl;
#endif
        }
        delete params;
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
//OLD	di[0]->setValues(data);
    /** m_estimatorParams.totalOfSubsets! */
    di[0]->m_dimension = di_0_dimension;

///TODO!!! ��� ����� � ����������� �� ���������� ������� ���������������� ���� ����������� ����� ����������, ���� ����������� ���������
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = ISimpleEstimator::simpleEstimatorNames[SimpleEstimatorsTypes::PW_PolynomialRegressionEstimatorForGivenSubsets];

///TODO!!! ��� ����� � ����������� �� ���������� ������� ���������������� ���� ����������� ����� ����������, ���� ����������� ���������
    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = PW_POLYNOMIAL_REGRESSION_GIVEN_SUBSETS_SIMPLPEESTIMATOR_DATASETNAME;
///TODO!!! ��� ����� � ����������� �� ���������� ������� ���������������� ���� ����������� ����� ����������, ���� ����������� ���������
    di[0]->m_dataModelName = m_model->toString();

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }

    for(size_t i = 0; i < di[0]->m_dimension; ++i)
    {
///TODO!!! ��� ����� � ����������� �� ���������� ������� ���������������� ���� ����������� ����� ����������, ���� ����������� ���������
        di[0]->m_dataSetTrackNames[i] = POLYNOMIAL_REGRESSION_COEFS_ESTIMATES_TRACKNAME;
    }

    /** put _trackSizes (each track size == ModifiedExpRegression model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    /** NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    We have already put all the above stuff into di[0]! See CTOR di[0]->dataSet = data;
    */
    return _RC_SUCCESS_;
}

/*********************************************************************************************************************
 PW_PolynomialRegressionEstimatorForGivenSubsets stuff
**********************************************************************************************************************/
int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::parseAndCreateEstimatorParams(std::map<std::string, std::string>& paramsMap, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
    /** IAA : ������� ������� �� ����������� ������� ��������� �������� ������ ������,
    ������� (�������) ���������� ������� ������� ��������
    */
    std::vector<std::string> keys;
    iteratorParamsMap itParam = paramsMap.find(Task::grammarTaskStepConfig[Task::grammarTaskConfigItems::REPO_ENTRIES_ITEM]);
    if (itParam != paramsMap.end())
    {
        std::string repoEntriesStr= itParam->second;
#ifdef _DEBUG_
std::cout << repoEntriesStr << std::endl;
#endif
        log->log(WORKFLOW_TASK_NOTIFICATION, "Get entries from repo with keys :  " + repoEntriesStr);
        /** �����, ���������� ��������� ��� ����, ����� ������� �������� ��������� ������
         ����-������� ������ � �� ���������!
         ��������� ������ ��� ��� � ���� ����������, ������ ��� ��������� � ���������� ���� ����� �������� ��� ������!
         */
        if(DataManagerGrammar::splitAllRepoKeysAsSingleString(repoEntriesStr,keys) != _RC_SUCCESS_)
        {
            log->error(WORKFLOW_TASK_ERROR, "No entries");
            return _ERROR_WRONG_ARGUMENT_;;
        }
    }
    int dimension = keys.size();
    DataInfo** di = NULL;
    di = new DataInfo* [dimension];
    if(di == NULL)
    {
        m_logger->error(WORKFLOW_TASK_ERROR, "No room to allocate DataInfo** for Estimator's/Model's input_data");
        return _ERROR_NO_ROOM_;
    }
    m_task->getRepoDataForTargetObj(keys, static_cast<size_t> (dimension), static_cast<DataInfo** const>(di) );
    int rc = this->setEstimatorParams(dimension, (const DataInfo**) di);
    /** �������� di[] */
///TODO!!! �����������, ��� � ��� �������
    freeArrayOfDataInfo(dimension, di);
    if(rc != _RC_SUCCESS_)
    {
            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong estimator params provided for Estimator");
            return _ERROR_WRONG_ARGUMENT_;
    }
    /** ����� ����� ������ � m_estimatorParams ��� ����� �������!
     ������ �� ����� ���������, ���� �����.
     � ����� ����� ��������� � ������ � cfg-����� ���������,
     ���������� ��� cfg-����� ��� �������������� ����������,
     ������� ������ ���� ������ ����, �  PW_PolynomialRegressionEstimatorForGivenSubsets.
    */
///TODO!!! ������ � �����, ���� ����� ���� ����� � �������� ��������� ����� configTask()
    /** Do Estimator initialization stuff */
    enum EstimatorsTypes et = FAKE_ESTIMATOR_TYPE;
    /** to create Estimator instance
    first find out info on its type
    */
//OLD    itParam = paramsMap.find(grammarTaskStepConfig[ESTIMATOR_TYPE_ITEM]);
    itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorForGivenSubsetsParams[IndexesOfPW_PolynomialRegressionEstimatorForGivenSubsetsParams::indexTypeOfNestedRegressionEstimator]);
    if (itParam != paramsMap.end())
    {
        std::string estimatorTypeName = itParam->second;
        /** get meta-info on Estimator-type */
///TODO!!! this snippet is not yet polymorphic!
        if (itParam->second == IEstimator::estimatorTypesNames[PointEstimator])
        {
            et = PointEstimator;
        }
        else if (itParam->second == IEstimator::estimatorTypesNames[IntervalEstimator])
        {
            et = IntervalEstimator;
        }
        else if (itParam->second == IEstimator::estimatorTypesNames[SimpleEstimator])
        {
            et = SimpleEstimator;
        }
        else
        {
            log->error(WORKFLOW_TASK_ERROR, "Wrong ESTIMATOR_TYPE set:" + estimatorTypeName);
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** get meta-info on Estimator-class of given Estimator-type
         and create instance of Estimator
        */
//OLD        itParam = paramsMap.find(grammarTaskStepConfig[ESTIMATOR_NAME_ITEM]);
        itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorForGivenSubsetsParams[IndexesOfPW_PolynomialRegressionEstimatorForGivenSubsetsParams::indexNameOfNestedRegressionEstimator]);
        if (itParam != paramsMap.end())
        {
            const std::string estimatorName = itParam->second;
//OLD            Logger * logger = &_log;
            m_nestedRegressionEstimator = IEstimator::createEstimator(static_cast<const enum EstimatorsTypes> (et), estimatorName, log );
            if (m_nestedRegressionEstimator == nullptr)
            {
                log->error(WORKFLOW_TASK_ERROR, "Wrong ESTIMATOR_NAME set:" + estimatorName);
                return _ERROR_WRONG_ARGUMENT_;
            }
        }
        else
        {
            log->error(WORKFLOW_TASK_ERROR, std::string(grammarPW_PolynomialRegressionEstimatorForGivenSubsetsParams[IndexesOfPW_PolynomialRegressionEstimatorForGivenSubsetsParams::indexNameOfNestedRegressionEstimator]) + " not set");
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    else
    {
        log->error(WORKFLOW_TASK_ERROR, std::string(grammarPW_PolynomialRegressionEstimatorForGivenSubsetsParams[IndexesOfPW_PolynomialRegressionEstimatorForGivenSubsetsParams::indexTypeOfNestedRegressionEstimator]) + " not set");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** Do NESTED Estimator Params initialization stuff ("ESTIMATOR_PARAMS_SRC") */
//OLD    itParam = paramsMap.find(grammarTaskStepConfig[ESTIMATOR_PARAMS_SRC_ITEM]);
    itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorForGivenSubsetsParams[IndexesOfPW_PolynomialRegressionEstimatorForGivenSubsetsParams::indexCfgFileNameOfNestedRegressionEstimator]);
    if (itParam != paramsMap.end())
    {
        /** here we call Config in order to parse EstimatorParams.cfg (if exists)
        and make map
        */
        std::string estimatorParamsCfg = itParam->second;
        ConfigParser estimatorParamsParser(estimatorParamsCfg);
        /** We don't kill current task if config for estimator does not exist
        */
        if (estimatorParamsParser.fileExists())
        {
            auto mapEstimatorParams = estimatorParamsParser.parse();

            /** create HARD-CODED empty ParamsSet for EstimatorParams */
            /** params input from file -
            fill map with IEstimator Params
            */
            size_t size = 0;
            IParams* ptrToPtrIParams=NULL;
            std::string postfix = "=";
            rc = m_nestedRegressionEstimator->parseAndCreateEstimatorParams(mapEstimatorParams, postfix, size, &ptrToPtrIParams, log);
            if(rc)
            {
                log->error(WORKFLOW_TASK_ERROR, "Wrong map of estimator params provided for Estimator");
                return _ERROR_WRONG_ARGUMENT_;
            }
        }
    }
    /** assign current model to the NESTED regression estimator
    */
    if(m_model)
    {
        rc = m_nestedRegressionEstimator->setModel(m_model);
        if(rc)
        {
            log->error(WORKFLOW_TASK_ERROR, "Can not set specified model type to Estimator");
        ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
            return _ERROR_WRONG_ARGUMENT_;
        }
    }

///TODO!!! ��� ����� ��������� ����� �������� ���������� ���������� ���������� � ������������� ��� ����� ��� cgf-�����!
/// ��������, ��� ��� ��������� ���������� ���������� ����� ������������ ��� ����� ����� ����� PW_PolynomialRegressionEstimatorForGivenSubsets!
///    int rc = this->setEstimatorParams(params);
    return _RC_SUCCESS_;
}

bool SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::validateEstimatorParamsAndInputData()
{
    if((m_data.size() == 0) || (m_estimatorParams == NULL))
    {
        return false;

    }
    size_t size = m_estimatorParams->getSize();
    if(!size)
    {
        return false;
    }
    if ((int)m_data.size() < (int)minLengthOfSingleSubset)
    {
        return false;
    }
    double lastIndex=NAN;
    m_estimatorParams->getParam(size-1, lastIndex);
    if ((isNAN_double(lastIndex)) || (lastIndex >= m_data.size() - minLengthOfSingleSubset) )
    {
        return false;
    }
    return true;
}

int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    /** ��� ��� ���� ����� � ����� ������� Task! */
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong estimator params as repo data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    const DataInfo* di = ptrToPtrDataInfo[0];
    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong estimator params as repo data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong estimator params as repo data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    delete m_estimatorParams;
    m_estimatorParams = NULL;
    /** Let's filter out zero-values
    as we treat m_dataSet[0][i] as indexes of dataset's elements
    */
    std::vector<double> indexes;
    for(size_t i=0; i < di->m_dataSetSizes[0]; ++i)
    {
        if(di->m_dataSet[0][i] > 0.0f)
        {
///TODO!!! ������������� ��� : "Overflow" <- !
            indexes.push_back(di->m_dataSet[0][i]);
        }
    }
    /** Let's validate indexes of subsets' boundaries
    */
    for(size_t i=0; (i+1) < indexes.size(); ++i)
    {
        if(indexes[i] > indexes[i+1])
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data : indexes of subsets' boundaries NUST be ASC ordered!");
            return _ERROR_WRONG_INPUT_DATA_;
        }
    }
    /** Now then indexes are retrieved we have to save them into instance's member m_estimatorParams
    */
    IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, indexes.size());
    if(! ptrIParams)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }

    for(size_t i=0; i < indexes.size(); ++i)
    {
        ptrIParams->setParam(i, indexes[i]);
#ifdef _DEBUG_
std::cout << indexes[i] << std::endl;
#endif
    }
      m_estimatorParams = ptrIParams;
    return _RC_SUCCESS_;
}

int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::setEstimatorParams(IParams*& params)
{
    size_t size = params->getSize();
    if (size != FakeIndex_PW_PolynomialRegressionEstimatorForGivenSubsetsParams)
    {
        delete params;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong Estimator Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
    delete m_estimatorParams;
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}

int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::setModel(IModel* model)
{
    /** if m_model has not been initialized
    the nestedRegressionEstimator's model is NULL!
    We must initialize it here!
    */
    bool isModelInitialized = (m_model != NULL);
/// Polynomial Model valid ONLY!
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
    model->getModelType(mt);
    if(mt != IModel::ModelsTypes::PolynomialModel)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
    No need to check if (dynamic_cast<IModelPolynomial*>(model))
    Because we have checked the model type is 'PolynomialModel'.
    */
    dynamic_cast<IModelPolynomial*>(m_model)->getDimension(m_estimatesDim);
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
    /** assign current model to the NESTED regression estimator as well
    */
    if((!isModelInitialized) && (m_model) && (m_nestedRegressionEstimator))
    {
        rc = m_nestedRegressionEstimator->setModel(m_model);
        if(rc)
        {
            m_logger->error(WORKFLOW_TASK_ERROR, "Can not set specified model type to Estimator");
        ///����� ���� ��� � ��������� model! ��������� ��� ������ ����������, � ����� ������ : ��� ����� ������� ������? ��������� ����� ��� �������� �������? ��� ������� ��� ��� �����?
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    return rc;
}

int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::setParamsSet()
{
   /** ��� �������� ����� ����
   ������ ����� IParamsSet::createEmptyParamsSet
   �� ���������� �������� m_model!
   ������ ����� ���������� = ������� ������ Params1D.
   */
   IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
   /** This way at least so far. */
   if (pst)
   {
//OLD       pst->setModel(m_model, m_logger);
       m_setOfParamsEstimates = pst;
   }
   return _RC_SUCCESS_;
}

int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];

    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
    }
#ifdef _DEBUG_
std::cout << "Input data size:" << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}


int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::estimate(ptrInt threadInterruptSignal)
{
///TODO!!!
/**
��� ����, ����� ��������������� ������� ���������������� ����������,
� parseAndCreateEstimatorParams �������� ������������� ���, ������� ������������ � configTask()
��� �������� ���������� ����������, ��� ������, ��� ������, ������������ ��� ���� ������,
� ������������ ��� ��� ����������, � ��� ��� floor ����, �� � ����� ������ ��� ��������� ����� ���� ������������
*/
    if(!validateEstimatorParamsAndInputData())
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input dat or/and info on boundaries of subsets!");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    size_t size = m_estimatorParams->getSize();
    /**
    If very last subset is too short
    we will not process it.
    */
    double lastIndex = NAN;
    m_estimatorParams->getParam(size-1, lastIndex);
    if(lastIndex+minLengthOfSingleSubset > m_data.size() - 1)
    {
        size--;
    }
    /** � parseAndCreateEstimatorParams() �� ��� ������� ��������� ���������� ����������,
    ��� ��������� ���������� ���������� �������������� ������
    (�� ��, ��� � ����� PW_PolynomialRegressionEstimatorForGivenSubsets),
    ��� ��������� ���������� ���������� ��� ����� � ��� �������������!
    ��������� ��������� ��� ��������� � ����� ���� floor.
    */
    /** ������ � ����� �� ���� ������ ��������� ������ ������ �������� ��� ������� ����� ������ ������������� ������������� ������
    */
    for(size_t indexOfSubset =0; indexOfSubset < size; ++indexOfSubset)
    {

     /** �� ������ ��������� ������ ������ ��������� DI**, ����������� � ���� ��������� ����� �������� ������,
     � ���� ����������
     */
        DataInfo** di = NULL;
        /** Total of DataInfo to be created = 1
        */
        size_t dimensionOfInputDataForNestedRegerssionEstimator = 1;
        di = new DataInfo* [dimensionOfInputDataForNestedRegerssionEstimator];
        if(!di)
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo** for input_data of nested Regression Estimator");
            return _ERROR_NO_ROOM_;
        }
        di[0] = NULL;
        int rc = putDataSubsetAsDataInfoArray(indexOfSubset, di);
        if(rc != _RC_SUCCESS_)
        {
            /** there might be ONLY single di[0] after putDataSubsetAsDataInfoArray() */
            freeArrayOfDataInfo(dimensionOfInputDataForNestedRegerssionEstimator, di);
            return _ERROR_WRONG_INPUT_DATA_;
        }
        rc = m_nestedRegressionEstimator->setData(dimensionOfInputDataForNestedRegerssionEstimator, const_cast<const tagDataInfo**> (di) );
        if(rc != _RC_SUCCESS_)
        {
            /** there might be ONLY single di[0] after putDataSubsetAsDataInfoArray() */
            freeArrayOfDataInfo(dimensionOfInputDataForNestedRegerssionEstimator, di);
            return _ERROR_WRONG_INPUT_DATA_;
        }
        /** �������� ������ ������������� ������������� ������ ��� �������� �����
        */
        int interrupt=0;
        rc = IEstimator::runEstimator(m_nestedRegressionEstimator, &interrupt);
        /** ������ di[] �� �������� ������ ���������� ����������,
        ��������� � ��� �� ����� �������� �� ����������
        ����������� �� ������ ������������� ������������� ������
        ��� �������� �����
        */
        freeArrayOfDataInfo(dimensionOfInputDataForNestedRegerssionEstimator, di);
        if(rc != _RC_SUCCESS_)
        {
            return _ERROR_WRONG_INPUT_DATA_;
        }
        /** �������� �� ���������� ����������� �� ������ ������������� ������������� ������ ��� �������� �����
        � ��������� ��� ������!
        */
        size_t dimentionOfEstimates=0;
        rc = m_nestedRegressionEstimator->getEstimatesAsDataInfoArray(dimentionOfEstimates, di);
        if(rc != _RC_SUCCESS_)
        {
            return _ERROR_WRONG_INPUT_DATA_;
        }
///TODO!!! �������� ������������ ����� ��� ������������ ������ �� di � IParams
    /** Now then estimates of regression coeffs are retrieved from nested regression estimator
     we have to save them into instance's member m_setOfParamsEstimates
    */
        IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, di[0]->m_dataSetSizes[0]);
        if(! ptrIParams)
        {
            return _ERROR_WRONG_ARGUMENT_;
        }

        for(size_t i=0; i < di[0]->m_dataSetSizes[0]; ++i)
        {
            ptrIParams->setParam(i, di[0]->m_dataSet[0][i]);
#ifdef _DEBUG_
double tmp_value=NAN;
ptrIParams->getParam(i,tmp_value);
std::cout << i << ":" << di[0]->m_dataSet[0][i] << ":" << tmp_value << std::endl;
std::cout << "In PW_regression we put into estimates..." << tmp_value << std::endl;
#endif
        }
        /** clean up ptrIParams after all
        */
          if(m_setOfParamsEstimates->addParamsCopyToSet(m_model, ptrIParams, m_logger) != true)
          {
            delete ptrIParams;
            freeArrayOfDataInfo(dimentionOfEstimates, di);
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Can not put Nested Regression Estimator's esitmates into PW_PolynomialRegressionEstimatorForGivenSubsets's m_setOfParamsEstimates.");
              return _ERROR_WRONG_OUTPUT_DATA_;
          };
          delete ptrIParams;
          /** ������ di[] ������ ��� �� ������ ���������� ���������� */
        freeArrayOfDataInfo(dimentionOfEstimates, di);
    }

#ifdef _DEBUG_
IParams * params = m_setOfParamsEstimates->getParamsCopy(0);
for(size_t j=0; j < 2; ++j)
{
    double tmp_double = NAN;
    params->getParam(j, tmp_double);
std::cout << "In PW_regression what finally have in estimates.. " << tmp_double << std::endl;
}
delete params;
#endif
    return _RC_SUCCESS_;
}

/** generate DataInfo */
int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::putDataSubsetAsDataInfoArray(size_t indexOfSubset, DataInfo** ptrToPtrDI) const
{
    /** Total of DataInfo to be created = 1
    ��������, ��� � ������� ���������� di �� ������� ���� ����, � ��� ������ ���� ������� di[0] ����� � �����!
    */
    if(!ptrToPtrDI)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wring ptrDI to allocate DataInfo* for input_data of nested Regression Estimator");
        return _ERROR_NO_ROOM_;
    }

    /**
    totalOfSubsets - �������� ����� ����������, ������� ����� ������������������
    �� ������ ��������� ������ ������ � ��������� ���������, ������������ ������� ������� ������,
    �� ������� ������ ��������� ������������� ������
    */
    size_t di_0_dimension = 0;
    double startIndex     = 0;
    double endIndex       = 0;
    m_estimatorParams->getParam(  indexOfSubset, startIndex);
    /** precautions for very last subset */
    size_t size = m_estimatorParams->getSize();
    if(size)
    {
        /** if the very last subset */
        if(indexOfSubset == size-1)
        {
            endIndex = m_data.size() - 1;
        }
        else
        {
            m_estimatorParams->getParam(indexOfSubset+1, endIndex);
        }
    }
    else
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong info on boundaries of subsets in PW_PolynomialRegressionEstimatorForGivenSubsets::m_estimatorParams!");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    di_0_dimension = (size_t)endIndex - (size_t)startIndex;
    if(!di_0_dimension)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong info on boundaries of subsets!");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    /** allocate room for _trackSizes (each track size == ModifiedExpRegression model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[1];
    if(!di_0_dataSetSizes)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate ptrs to dataSetSizes for input_data of nested Regression Estimator");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, 1*sizeof(size_t));
    /** init _trackSize (track size ==  ������������ �������� ���������� subset) */
    di_0_dataSetSizes[0] = di_0_dimension;

/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[1];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room for input_data of nested Regression Estimator");
        delete [] di_0_dataSetSizes;
        return _ERROR_NO_ROOM_;
    }
    data[0] = new double[di_0_dataSetSizes[0]];
    if (!data[0])
    {
        delete [] data;
        delete [] di_0_dataSetSizes;
         m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(data[0], 0, di_0_dataSetSizes[0]*sizeof(double));
//std::cout << (size_t)startIndex << ";" << (size_t)endIndex << ";" << std::endl;
    for(size_t i = (size_t)startIndex; i < endIndex; ++i)
    {
        data[0][i - (size_t)startIndex] = m_data[i];
//std::cout << i << ":" << m_data[i] << std::endl;
    }
    /** We pass input data pointers to DataInfo's CTOR
    to save them into the instance of DataInfo as double const ** const
    */
    /**
    � ������� ���������� di ���� ������ ���� ������� di[0]!
    */
    ptrToPtrDI[0] = new DataInfo(data);
    if (!ptrToPtrDI[0])
    {
        delete [] data[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
//OLD	ptrToPtrDI[0]->setValues(data);
    /** m_estimatorParams.totalOfSubsets!
    */
    ptrToPtrDI[0]->m_dimension = 1;
    ptrToPtrDI[0]->m_dataSetSizes = di_0_dataSetSizes;
    return _RC_SUCCESS_;
}

int SimpleEstimators::PW_PolynomialRegressionEstimatorForGivenSubsets::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}

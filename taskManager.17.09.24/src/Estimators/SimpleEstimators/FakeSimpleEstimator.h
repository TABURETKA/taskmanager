#ifndef __FAKESIMPLEESTIMATOR_H__633569422542968750
#define __FAKESIMPLEESTIMATOR_H__633569422542968750
#include "../Estimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace SimpleEstimators{
    /** class container */
/** implementation of basic methods
 declared in abstract Base class ISimpleEstimator
 The methods setTask, setLog, toString
 have to be inherited all SimpleEstimators
*/
class FakeSimpleEstimator : public virtual ISimpleEstimator
{
public:
    FakeSimpleEstimator(Logger*& log);
    virtual ~FakeSimpleEstimator();
    /** IEstimator stuff */
    virtual void setTask(Task* const & task);
    virtual void setLog(Logger* const & log);
///TODO!!! ��� ���� � setData()???
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    virtual std::string toString() const;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** ISimpleEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimate(ptrInt threadInterruptSignal);

protected:
    int logErrMsgAndReturn(int rc, std::string msg) const;
    virtual int parsingFailed(std::string msg) const;
    enum SimpleEstimatorsTypes m_set;
    Task* m_task;
    Logger* m_logger;
private:
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    FakeSimpleEstimator(const FakeSimpleEstimator&);
    void operator=(const FakeSimpleEstimator&);
//    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
//    IParamsSet* _pst;
//    IParams* _estimatorParams;
};

} /// end namespace SimpleEstimators
#endif

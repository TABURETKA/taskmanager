#include "../../CommonStuff/Defines.h" /* isNAN_double */
#include "../../Log/Log.h"
#include "../../Log/errors.h"
#include "../../Models/Model.h" //ModelTypes
#include "../../Params/ParamsSet.h"//IParams
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
//OLD #include <stdlib.h> //atof()
#include "../../DataCode/DataModel/DataMetaInfo.h" //DataInfo
#include "../../DataCode/Grammar/DataManagerGrammar.h" //DELIMITER_DATASET_ITEMS_SEMANTICS_IN_TRACK_NAME
#include "ScalarArithmeticWithDatasetEstimator.h"
#include <sstream> /* stringstream */
#include <fstream> /* ofstream */
#include <float.h> /* DBL_MIN */
///static in ScalarArithmeticWithDatasetEstimator
const char* SimpleEstimators::ScalarArithmeticWithDatasetEstimator::grammar[FAKE_INDEX_GRAMMAR_SCALAR_ARITHMETIC]={
    "ArithmeticOperation",
    "ArithmeticOperandSematics",
    "ArithmeticOperand"
};

///static in ScalarArithmeticWithDatasetEstimator
const char* SimpleEstimators::ScalarArithmeticWithDatasetEstimator::grammarForOperations[FAKE_OPERATION_PARAM_SEMANTICS]={
    "ADDITION",
    "SUBTRACTION",
    "MULTIPLICATION",
    "DIVISION"
};
///static in ScalarArithmeticWithDatasetEstimator
const char* SimpleEstimators::ScalarArithmeticWithDatasetEstimator::grammarForOperationResult[FAKE_OPERATION_PARAM_SEMANTICS]={
    "SUM",
    "DIFFERENCE",
    "PRODUCT",
    "RATIO"
};

///static in ScalarArithmeticWithDatasetEstimator
const char* SimpleEstimators::ScalarArithmeticWithDatasetEstimator::grammarForOperandSemantics[FAKE_OPERAND_PARAM_SEMANTICS]={
    "VALUE",
    "REFERENCE"
};

SimpleEstimators::ScalarArithmeticWithDatasetEstimator::ScalarArithmeticWithDatasetEstimator(Logger*& log)  : FakeSimpleEstimator(log)
{
    /** so far noting to allocate */
    m_set = SimpleEstimatorsTypes::ScalarArithmeticWithDatasetEstimator;
    /** m_task = NULL; m_logger  = log; already initialized in FakePointEstimator's CTOR */
    m_model  = NULL;
    m_estimatorParams  = NULL;
    m_setOfParamsEstimates = NULL;
}
SimpleEstimators::ScalarArithmeticWithDatasetEstimator::~ScalarArithmeticWithDatasetEstimator()
{
    m_data.clear();
    delete m_estimatorParams;
    delete m_setOfParamsEstimates;
}

/** generate DataInfo */
int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    if(!m_setOfParamsEstimates)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No estimates so far.");
        return _ERROR_WRONG_WORKFLOW_;
    }
    /** Total of DataInfo to be created */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    size_t di_0_dimension = 1;
    /** sizeOfSingleArray == m_data.size() */
    size_t sizeOfSingleArray = m_estimatesDim;
//    size_t modelDim = 0;

    /** allocate room for _trackSizes (each track size == ModifiedExpRegression model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    /** init _trackSizes (each track size == 1 - single parameter of ModifiedExpRegression model ) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        di_0_dataSetSizes[i] = sizeOfSingleArray;
    }
/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        IParams * params = m_setOfParamsEstimates->getParamsCopy(i);
        if(! params)
        {
            while(i)
            {
                --i;
                delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;
            std::stringstream tmp_ss;
            tmp_ss << ISimpleEstimator::simpleEstimatorNames[m_set] << ": " << i << "-th params estimates is missing in m_setOfParamsEstimates.";
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, tmp_ss.str());
            return _ERROR_WRONG_OUTPUT_DATA_;
        }
        size_t size = params->getSize();
        if(size != di_0_dataSetSizes[i])
        {
            delete params;
            while(i)
            {
                --i;
                delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;
            std::stringstream tmp_ss;
            tmp_ss << ISimpleEstimator::simpleEstimatorNames[m_set] << ": mismatch of " << i << "-th params dimension in m_setOfParamsEstimates and dataSetSizes's dimension to save those params.";
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, tmp_ss.str());
            return _ERROR_WRONG_OUTPUT_DATA_;
        }
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            delete params;
            while(i)
            {
                --i;
                delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        for(size_t j= 0; j < di_0_dataSetSizes[i]; ++j)
        {
            data[i][j] = NAN;
            params->getParam(j, data[i][j]);
        }
        delete params;
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** m_estimatesDim == 1 */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = ISimpleEstimator::simpleEstimatorNames[SimpleEstimatorsTypes::ScalarArithmeticWithDatasetEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = SCALAR_ARITHMETIC_OPERATION_WITH_DATASETNAME;
    di[0]->m_dataModelName = m_model->toString();

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }

    double operation=FAKE_OPERATION_PARAM_SEMANTICS;
    int rc = this->getOperation(operation);
    if(rc != _RC_SUCCESS_)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    const enum ScalarArithmeticOperation op = static_cast<const enum ScalarArithmeticOperation>(static_cast<size_t>(operation));
    di[0]->m_dataSetTrackNames[0] = grammarForOperationResult[op];

    /** put _trackSizes (each track size == ModifiedExpRegression model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    //We have already put data into di[0]! See CTOR di[0]->dataSet = data;
    return _RC_SUCCESS_;
}

/** ScalarArithmeticWithDatasetEstimator stuff */
int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
    iteratorParamsMap itParam = mapParams.find(grammar[indexOfArithmeticOperation]);
    double operation=FAKE_OPERATION_PARAM_SEMANTICS;
    if (itParam != mapParams.end())
    {
        for(size_t op=0; op < FAKE_OPERATION_PARAM_SEMANTICS; ++op)
        {
            if(itParam->second ==  grammarForOperations[op])
            {
                operation = (enum ScalarArithmeticOperation) op;
                break;
            }
        }
    }
    else
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Arithmetic Operation NOT FOUND in cfg-file!" );
        return _ERROR_WRONG_ARGUMENT_;
    }


    itParam = mapParams.find(grammar[indexOfArithmeticOperandSematics]);
    double operandValue=NAN;
    enum ScalarArithmeticOperandSemantics operandSemantics = FAKE_OPERAND_PARAM_SEMANTICS;
    if (itParam != mapParams.end())
    {
        /** ��� ����� ����� �������� ������� ��������� ����������� map-����� �� ������ ����������,
        ������� �������� �� ���� �������� (VALUE, REF, ... )!
        */
        /** we are pessimists */
        int rc = _ERROR_NO_INPUT_DATA_;
        size_t tmpInt;
        rc = strTo<size_t>(itParam->second.c_str(), tmpInt); //OLD operandSemantics = (enum ScalarArithmeticOperandSemantics) atoi(itParam->second.c_str());
        mapParams.erase(itParam);

        if((rc != _RC_SUCCESS_) || (tmpInt >= FAKE_OPERAND_PARAM_SEMANTICS))
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Operand Semantics Not Found!");
            return _ERROR_WRONG_ARGUMENT_;
        }
        operandSemantics = static_cast<enum ScalarArithmeticOperandSemantics> (tmpInt);
        switch(operandSemantics)
        {
        case VALUE:
        {
            itParam = mapParams.find(grammar[indexOfArithmeticOperand]);
            /** we are pessimists */
            int rc = _ERROR_NO_INPUT_DATA_;
            if (itParam != mapParams.end())
            {
                rc = strTo<double>(itParam->second.c_str(), operandValue); //OLD          operandValue = atof(itParam->second.c_str());
                mapParams.erase(itParam);
            }
            if(rc != _RC_SUCCESS_)
            {
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Not found OR wrong operand value for " + grammar[indexOfArithmeticOperandSematics] + " semantics.");
                return _ERROR_WRONG_ARGUMENT_;
            }
        }

        break;
        case REFERENCE:
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Operand value BY REFERENCE NOT IMPLEMENTED!");
            return _ERROR_WRONG_ARGUMENT_;
            break;
        default:
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong Operand Semantics!");
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    else
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Operand semantics NOT FOUND in cfg-file!" );
        return _ERROR_WRONG_ARGUMENT_;
    }

    IParams * params= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, FAKE_INDEX_SCALAR_ARITHMETIC_PARAMS);
    if (!params)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to keep Estimator Params");
        return _ERROR_NO_ROOM_;
    }
    params->setParam(indexOfOperation, operation);
    params->setParam(indexOfOperandValue, operandValue);
    int rc = this->setEstimatorParams(params);
    return rc;
}
/** IAA: ��� ���� ����� ���� �� ����������, �.�. ����� �������������� ���������� ��������� ����� ����������� ���� �� �������� */
int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    delete m_estimatorParams;
    m_estimatorParams = NULL;
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::setEstimatorParams(IParams*& params)
{
    size_t size = params->getSize();
    if (size != FAKE_INDEX_SCALAR_ARITHMETIC_PARAMS)
    {
        delete params;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong Estimator Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
    delete m_estimatorParams;
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}
int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::setModel(IModel* model)
{
/// Polynomial Model valid ONLY!
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
    model->getModelType(mt);
    if(mt != IModel::ModelsTypes::PlainModel)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /** Model Dimension DOES NOT MATTER Here!
    m_estimatesDim will be defined up front any estimation
    right in estimates() as m_data.size()
    */
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
    return rc;
}

int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::setParamsSet()
{
    /** ��� �������� ����� ����
    ������ ����� IParamsSet::createEmptyParamsSet
    �� ���������� �������� m_model!
    ������ ����� ���������� = ������� ������ Params1D.
    */
    IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
    /** This way at least so far. */
    if (pst)
    {
//OLD        pst->setModel(m_model, m_logger);
        this->m_setOfParamsEstimates = pst;
    }
    return _RC_SUCCESS_;
}

int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(IPointEstimator::pointEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];
//    std::cout << "di->m_dimension:" << di->m_dimension  << std::endl;
    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(IPointEstimator::pointEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
//    std::cout << "di->m_dataSetSizes[0]:" << di->m_dataSetSizes[0] << std::endl;
//    std::cout << "di->m_dataSet:" << di->m_dataSet << std::endl;

    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(IPointEstimator::pointEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
    }
#ifdef _DEBUG_
    std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}

int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::getOperation(double& operation) const
{
    int rc = m_estimatorParams->getParam(indexOfOperation, operation);
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No estimatorParam on operation.");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    return _RC_SUCCESS_;
}

int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::getOperandValue(double& operand) const
{
    int rc = m_estimatorParams->getParam(indexOfOperandValue, operand);
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No estimatorParam on operand.");
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    return _RC_SUCCESS_;
}

int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::estimate(ptrInt threadInterruptSignal)
{
    if (m_data.size() == 0)
    {
        return _ERROR_NO_INPUT_DATA_;
    }
    m_estimatesDim = m_data.size();
    if (! m_estimatorParams)
    {
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    m_setOfParamsEstimates->clear();

    double operand=NAN;
    int rc = getOperandValue(operand);
    if(rc != _RC_SUCCESS_)
    {
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }

    double operation=FAKE_OPERATION_PARAM_SEMANTICS;
    rc = getOperation(operation);
    if(rc != _RC_SUCCESS_)
    {
        return _ERROR_NO_ESTIMATOR_PARAMS_;
    }
    const enum ScalarArithmeticOperation op = static_cast<const enum ScalarArithmeticOperation>(static_cast<size_t>(operation));
    /** Merely for safety-sake */
    if ((op == DIVISION) && (fabs(operand) < DBL_MIN))
    {
        return _ERROR_WRONG_INPUT_DATA_;
    }
    IParams * tmpIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, m_estimatesDim);
    if (tmpIParams == NULL)
    {
        return _ERROR_NO_ROOM_;
    }
    for(size_t i=0; i < m_estimatesDim; ++i)
    {
        double tmpDbl=m_data[i];
        switch(op){
        case ADDITION:
            tmpDbl += operand;
            break;
        case SUBTRACTION:
            tmpDbl -= operand;
            break;
        case MULTIPLICATION:
            tmpDbl *= operand;
            break;
        case DIVISION :
            tmpDbl /= operand;
            break;
        case FAKE_OPERATION_PARAM_SEMANTICS:
            return _ERROR_NO_ESTIMATOR_PARAMS_;
        }
        tmpIParams->setParam(i, tmpDbl);
    }
    rc = _RC_SUCCESS_;
    if(m_setOfParamsEstimates->addParamsCopyToSet(m_model, (const IParams*)tmpIParams, m_logger) != true)
    {
        delete tmpIParams;
        rc = _ERROR_WRONG_OUTPUT_DATA_;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Can not add estimates to put into Estimator's m_setOfParamsEstimates.");
    }
//    std::cout << "In estimate2:" << m_setOfParamsEstimates << " : m_setOfParamsEstimates.size:" << m_setOfParamsEstimates->getSetSize() << std::endl;
    /* DEBUG
    */
    delete tmpIParams;
    return rc;
}


int SimpleEstimators::ScalarArithmeticWithDatasetEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}

#include "../../Log/Log.h"
#include "../../Log/errors.h"
#include "FakeSimpleEstimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace SimpleEstimators;

SimpleEstimators::FakeSimpleEstimator::FakeSimpleEstimator(Logger*& log)
{
 /** so far noting to allocate */
  m_set = SimpleEstimatorsTypes::FakeSimpleEstimator;
  m_task = NULL;
  m_logger  = log;
}
SimpleEstimators::FakeSimpleEstimator::~FakeSimpleEstimator()
{
 /** so far noting to delete */
}
    /** IEstimator stuff */
///TODO!!! ������������ � setTask, � setLog!
void SimpleEstimators::FakeSimpleEstimator::setTask(Task* const & task)
{
      m_task = task;
}

void SimpleEstimators::FakeSimpleEstimator::setLog(Logger* const & log)
{
      m_logger = log;
}

int SimpleEstimators::FakeSimpleEstimator::logErrMsgAndReturn(int rc, std::string msg) const
{
    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(IPointEstimator::pointEstimatorNames[m_set]) + ": " + msg);
    return _ERROR_WRONG_WORKFLOW_;
}

/** ����� ������, ����������� ��� ������� ���������! */
std::string SimpleEstimators::FakeSimpleEstimator::toString() const
{
  return  static_cast<std::string> (IPointEstimator::pointEstimatorNames[m_set]);
}

    /** generate DataInfo */
int SimpleEstimators::FakeSimpleEstimator::getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " getEstimatesAsDataInfoArray is not implemented yet!");
}

    /** ISimpleEstimator stuff */
int SimpleEstimators::FakeSimpleEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parseAndCreateEstimatorParams is not implemented yet!");
}
int SimpleEstimators::FakeSimpleEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int SimpleEstimators::FakeSimpleEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
        return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int SimpleEstimators::FakeSimpleEstimator::setModel(IModel* model)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setModel is not implemented yet!");
}

///TODO!!! ��� ���� � setData()???
/** No need so far
int SimpleEstimators::FakeSimpleEstimator::setParamsCompact(IParamsCompact * pst)
{
///TODO!!!
    m_logger->log(ALGORITHM_1_LEVEL_INFO, "FakeSimpleEstimator::setParamsCompact is not implemented yet!");
}
*/
int SimpleEstimators::FakeSimpleEstimator::setParamsSet()
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setParamsSet is not implemented yet!");
}
int SimpleEstimators::FakeSimpleEstimator::estimate(ptrInt threadInterruptSignal)
{
///TODO!!! ???
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " estimate is not implemented yet!");
}

int SimpleEstimators::FakeSimpleEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}

int SimpleEstimators::FakeSimpleEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setData is not implemented yet!");
}

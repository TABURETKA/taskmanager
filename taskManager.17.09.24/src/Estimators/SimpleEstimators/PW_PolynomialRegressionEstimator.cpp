#include "../../CommonStuff/Defines.h" /* isNAN_double */
#include "../../Log/Log.h"
#include "../../Log/errors.h"
#include "../../Models/Model.h" //ModelTypes
#include "../../Params/ParamsSet.h"//IParams
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../../DataCode/Grammar/DataManagerGrammar.h" /* DataManagerGrammar */
#include "../../DataCode/DataModel/DataMetaInfo.h" //DataInfo
#include "PW_PolynomialRegressionEstimator.h"
#include <sstream> /* stringstream */

///#define _DEBUG_

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace SimpleEstimators;

SimpleEstimators::PW_PolynomialRegressionEstimator::PW_PolynomialRegressionEstimator(Logger*& log)  : FakeSimpleEstimator(log)
{
 /** so far noting to allocate */
  m_set = SimpleEstimatorsTypes::PW_PolynomialRegressionEstimator;
  m_estimatesDim = 0;
  m_numStartPoints = MIN_NUMBER_OF_POINTS_IN_SEGMENT;
  /** _task = NULL; m_logger  = log; already initialized in FakePointEstimator's CTOR */
  m_model                     = NULL;
  m_estimatorParams           = NULL;
  m_setOfParamsEstimates      = NULL;
///TODO!!! ������
  m_processInfo               = NULL;
  m_structuralChangeTesterParams = NULL;

}
SimpleEstimators::PW_PolynomialRegressionEstimator::~PW_PolynomialRegressionEstimator()
{
    m_data.clear();

	for (auto& item : m_fisherDistMap)
	{
		item.second.clear();
	}
	m_fisherDistMap.clear();

    delete m_estimatorParams;
    delete m_setOfParamsEstimates;

    delete m_processInfo;
	delete m_structuralChangeTesterParams;

    m_segments.clear();
}
int SimpleEstimators::PW_PolynomialRegressionEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
 ///TODO!!! ����-����� ���������� ����������� � 1?
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];

    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    /** for test **/
    m_processInfo = new ProcessInfo();

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
    }

	double param = NAN;
	m_estimatorParams->getParam(IndexesOfPW_PolynomialRegressionEstimatorParams::indexForecastStartPointIndex, param);
	// ������ �����, � ������� ���������� �������, ����������� �� ��������� ���������� ���������� ��������.
	int forecastStartPointIndex = (int)param;

	param = NAN;
	m_estimatorParams->getParam(IndexesOfPW_PolynomialRegressionEstimatorParams::indexTrainingSampleSize, param);
	// ����� ��������� �������.
	int trainingSampleSize = (int)param;

	//�������� ������ ������ (�������� ��������) ������ ��� �����,
	// ������������ � ������� ������� forecastStartPointIndex - trainingSampleSize � ��  forecastStartPointIndex
	// ��� �����, ������������ � ������� ������� forecastStartPointIndex ����� ��������� �������
	int counter = 0;
	for (int i = forecastStartPointIndex - trainingSampleSize; i < forecastStartPointIndex; ++i)
	{
		m_processInfo->x.push_back(counter);
		m_processInfo->y.push_back(m_data[i]);
		counter++;
	}

#ifdef _DEBUG_
std::cout << "Input data size:" << m_data.size() << std::endl;
#endif
    return _RC_SUCCESS_;
}
int SimpleEstimators::PW_PolynomialRegressionEstimator::setEstimatorParams(IParams*& params)
{
    delete m_estimatorParams;
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}
    /** generate DataInfo */
int SimpleEstimators::PW_PolynomialRegressionEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
	if (!m_setOfParamsEstimates)
	{
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No estimates so far.");
		return _ERROR_WRONG_WORKFLOW_;
	}
    /** Total of DataInfo to be created = 1
    */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }

    /** for test **/
	size_t di_0_dimension = m_setOfParamsEstimates->getSetSize(); /** for test: ���������, ��������� + ������� � ��������� **/

    if(!di_0_dimension)
    {
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No info on boundaries of subsets into m_setOfParamsEstimates!");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    /** allocate room for ptrs to _trackSizes (right now each track size == ModifiedExpRegression model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    /** init _trackSizes (each track size == m_estimatesDim == ������������ ������������ �������������� ������ ) */

	/** array of ptrs to 1-D sets */
	double** data = new double*[di_0_dimension];
	if (!data)
	{
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to return estimates");
		delete[] di_0_dataSetSizes;
		delete[] di;
		return _ERROR_NO_ROOM_;
	}
	std::memset(data, 0, di_0_dimension*sizeof(double*));

	IParams* ptrIParams = m_setOfParamsEstimates->getFirstPtr();
	size_t id = m_setOfParamsEstimates->getCurrentId();
	while (id < di_0_dimension)
	{
		size_t size = ptrIParams->getSize();
		if (!size)
		{
			return _ERROR_WRONG_OUTPUT_DATA_;
		}
		di_0_dataSetSizes[id] = size;

		data[id] = new double[di_0_dataSetSizes[id]];
		if (!data[id])
		{
			delete[] data;
			delete[] di_0_dataSetSizes;
			delete[] di;
			m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(simpleEstimatorNames[m_set]) + ": No room to return estimates");
			return _ERROR_NO_ROOM_;
		}
		std::memset(data[id], 0, di_0_dataSetSizes[id] * sizeof(double));
		for (size_t j = 0; j < di_0_dataSetSizes[id]; ++j)
		{
			double tmp = 0;
			ptrIParams->getParam(j, tmp);
			data[id][j] = tmp;
		}
		ptrIParams = m_setOfParamsEstimates->getNextPtr();
		id = m_setOfParamsEstimates->getCurrentId();
	}

    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** m_estimatorParams.totalOfSubsets! */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = ISimpleEstimator::simpleEstimatorNames[SimpleEstimatorsTypes::PW_PolynomialRegressionEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = PW_POLYNOMIAL_REGRESSION_SIMPLPE_ESTIMATOR_DATASETNAME;
    di[0]->m_dataModelName = m_model->toString();

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }
    for(size_t i = 0; i < di[0]->m_dimension; ++i)
    {
		di[0]->m_dataSetTrackNames[i] = m_trNames[i];
    }

    /** put _trackSizes (each track size == ModifiedExpRegression model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    /** NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    We have already put all the above stuff into di[0]! See CTOR di[0]->dataSet = data;
    */
    return _RC_SUCCESS_;
}
/*********************************************************************************************************************
 PW_PolynomialRegressionEstimator stuff
**********************************************************************************************************************/
int SimpleEstimators::PW_PolynomialRegressionEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong estimator params as repo data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    return _RC_SUCCESS_;
}

int SimpleEstimators::PW_PolynomialRegressionEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& paramsMap, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
	iteratorParamsMap itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorParams[IndexesOfPW_PolynomialRegressionEstimatorParams::indexStructuralChangeTesterName]);
	if (itParam != paramsMap.end())
	{
	    std::string tmp = itParam->second.c_str();
	    if(!tmp.empty())
		{
			m_structuralChangeTesterName = tmp;
		}
		else
		{
			log->error(WORKFLOW_TASK_ERROR, "Wrong param: StructuralChangeTesterName is empty");
			return _ERROR_WRONG_ARGUMENT_;
		}
	}
	else {
		log->error(WORKFLOW_TASK_ERROR, "Wrong param: StructuralChangeTesterName");
		return _ERROR_WRONG_ARGUMENT_;
	}

	itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorParams[IndexesOfPW_PolynomialRegressionEstimatorParams::indexStructuralChangeTesterCfgFileName]);
	if (itParam != paramsMap.end())
	{
		std::string tmp = itParam->second.c_str();
		if (!tmp.empty())
		{
			m_structuralChangeTesterCfgFileName = tmp;
		}
		else
		{
			log->error(WORKFLOW_TASK_ERROR, "Wrong PARAMS: StructuralChangeTesterCfgFileName is empty");
			return _ERROR_WRONG_ARGUMENT_;
		}
	}
	else {
		log->error(WORKFLOW_TASK_ERROR, "Wrong PARAMS: StructuralChangeTesterCfgFileName");
		return _ERROR_WRONG_ARGUMENT_;
	}

	int rc = initChangeTester();
	if (rc)
	{
		log->error(WORKFLOW_TASK_ERROR, "Wrong Init: StructuralChangeTester");
		return _ERROR_WRONG_ARGUMENT_;
	}

	std::vector<double> indexes;
	itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorParams[IndexesOfPW_PolynomialRegressionEstimatorParams::indexForecastStartPointIndex]);
	if (itParam != paramsMap.end())
	{
        double startPointAsDbl = NAN;
        int rc = _ERROR_NO_INPUT_DATA_;
        rc = strTo<double>(itParam->second.c_str(), startPointAsDbl); //OLD          float StartPointAsFloat = atof(itParam->second.c_str());
        if(rc != _RC_SUCCESS_)
        {
			log->error(WORKFLOW_TASK_ERROR, "Wrong ForecastStartPointIndex:" + itParam->second);
			return _ERROR_WRONG_ARGUMENT_;
        }
		if (startPointAsDbl < 0.0f)
		{
			log->error(WORKFLOW_TASK_ERROR, "Wrong ForecastStartPointIndex:" + itParam->second);
			return _ERROR_WRONG_ARGUMENT_;
		}
		indexes.push_back((double)startPointAsDbl);
	}
	else {
		log->error(WORKFLOW_TASK_ERROR, "Wrong ForecastStartPointIndex:");
		return _ERROR_WRONG_ARGUMENT_;
	}
	itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorParams[IndexesOfPW_PolynomialRegressionEstimatorParams::indexTrainingSampleSize]);
	if (itParam != paramsMap.end())
	{
        int rc = _ERROR_NO_ESTIMATOR_PARAMS_;
        double sizeAsDbl = NAN;
        rc = strTo<double>(itParam->second.c_str(), sizeAsDbl); //OLD float SizeAsFloat = atof(itParam->second.c_str());
        paramsMap.erase(itParam);

        if((rc != _RC_SUCCESS_) || (sizeAsDbl < 1.0f))
		{
			log->error(WORKFLOW_TASK_ERROR, "Wrong TrainingSampleSize:" + itParam->second);
			return _ERROR_WRONG_ARGUMENT_;
		}
		indexes.push_back((double)sizeAsDbl);
	}
	else {
		log->error(WORKFLOW_TASK_ERROR, "Wrong TrainingSampleSize:");
		return _ERROR_WRONG_ARGUMENT_;
	}

	itParam = paramsMap.find(grammarPW_PolynomialRegressionEstimatorParams[IndexesOfPW_PolynomialRegressionEstimatorParams::indexForecastHorizont]);
	if (itParam != paramsMap.end())
	{
        int rc = _ERROR_NO_ESTIMATOR_PARAMS_;
        double horizontAsDbl = NAN;
        rc = strTo<double>(itParam->second.c_str(), horizontAsDbl); //OLD float HorizontAsFloat = atof(itParam->second.c_str());
        paramsMap.erase(itParam);

        if((rc != _RC_SUCCESS_) || (horizontAsDbl < 1.0f))
		{
			log->error(WORKFLOW_TASK_ERROR, "Wrong ForecastHorizont:" + itParam->second);
			return _ERROR_WRONG_ARGUMENT_;
		}
		indexes.push_back((double)horizontAsDbl);
	}
	else {
		log->error(WORKFLOW_TASK_ERROR, "Wrong ForecastHorizont:");
		return _ERROR_WRONG_ARGUMENT_;
	}

	IParams * ptrIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, indexes.size());
	if (!ptrIParams)
	{
		return _ERROR_WRONG_ARGUMENT_;
	}

	for (size_t i = 0; i < indexes.size(); ++i)
	{
		ptrIParams->setParam(i, indexes[i]);
#ifdef _DEBUG_
		std::cout << indexes[i] << std::endl;
#endif
	}
	rc = this->setEstimatorParams(ptrIParams);
	return rc;


///TODO!!! ����-����� ���������� ����������� � 1? �������������� ���?
//    int dimension = 1;
//    int rc = this->setEstimatorParams(dimension, NULL);
//    if(rc != _RC_SUCCESS_)
//    {
//            m_logger->error(WORKFLOW_TASK_ERROR, "Wrong estimator params provided for Estimator");
//            return _ERROR_WRONG_ARGUMENT_;
//    }
//    return _RC_SUCCESS_;
}
int SimpleEstimators::PW_PolynomialRegressionEstimator::setModel(IModel* model)
{
    /** if m_model has not been initialized
    the nestedRegressionEstimator's model is NULL!
    We must initialize it here!
    */
    bool isModelInitialized = (m_model != NULL);
/// Polynomial Model valid ONLY!
    enum IModel::ModelsTypes mt = IModel::ModelsTypes::FAKE_MODEL;
    model->getModelType(mt);
    if(mt != IModel::ModelsTypes::PolynomialModel)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
    No need to check if (dynamic_cast<IModelPolynomial*>(model))
    Because we have checked the model type is 'PolynomialModel'.
    */
    dynamic_cast<IModelPolynomial*>(m_model)->getDimension(m_estimatesDim);
	//m_numStartPoints = std::max( (const size_t)m_numStartPoints, m_estimatesDim + 1);
	m_numStartPoints = std::max( static_cast<size_t>(m_numStartPoints), m_estimatesDim + 1);

    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
    return rc;
}

int SimpleEstimators::PW_PolynomialRegressionEstimator::setParamsSet()
{
   /** ��� �������� ����� ����
   ������ ����� IParamsSet::createEmptyParamsSet
   �� ���������� �������� m_model!
   ������ ����� ���������� = ������� ������ Params1D.
   */
   IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
   /** This way at least so far. */
   if (pst)
   {
       m_setOfParamsEstimates = pst;
   }

   return _RC_SUCCESS_;
}

bool SimpleEstimators::PW_PolynomialRegressionEstimator::validateEstimatorParamsAndInputData()
{
	/// ������ validateEstimatorParamsAndInputData() ����� ��������������� �������� � ���������� �������
    if(m_data.size() == 0)
    {
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data size!");
        return false;
    }

    size_t size = m_estimatorParams->getSize();
    if(!size)
    {
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input estimator params size!");
        return false;
    }

	double param = NAN;
	m_estimatorParams->getParam(IndexesOfPW_PolynomialRegressionEstimatorParams::indexForecastStartPointIndex, param);
	if (isNAN_double(param))
	{
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input params: ForecastStartPointIndex!");
		return false;
	}
	// ������ �����, � ������� ���������� �������, ����������� �� ��������� ���������� ���������� ��������.
	int forecastStartPointIndex = (int)param;

	param = NAN;
	m_estimatorParams->getParam(IndexesOfPW_PolynomialRegressionEstimatorParams::indexTrainingSampleSize, param);
	if (isNAN_double(param))
	{
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input params: TrainingSampleSize!");
		return false;
	}
	// ����� ��������� �������.
	int trainingSampleSize = (int)param;

	if ((int)m_data.size() < forecastStartPointIndex)
    {
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data size!");
		return false;
    }

	if ((int)m_data.size() < trainingSampleSize)
	{
		m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(ISimpleEstimator::simpleEstimatorNames[m_set]) + ": Wrong input data size!");
		return false;
	}
    return true;
}
int SimpleEstimators::PW_PolynomialRegressionEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}
int PW_PolynomialRegressionEstimator::parseAndCreateStructuralChangeTesterParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams* &ptrToIParams)
{
	iteratorParamsMap itParam;
	std::vector<double> indexes;
	if (m_structuralChangeTesterName == structuralChangeTestersNames[IndexesOfstructuralChangeTestersNames::indexNextResidualAsGaussianOutlierTester])
	{
		itParam = mapParams.find(grammarNextResidualAsGaussianOutlierTesterParams[IndexesOfNextResidualAsGaussianOutlierTesterParams::indexNewSegmentSelectorCoefficient]);
		if (itParam != mapParams.end())
		{
            int rc = _ERROR_NO_ESTIMATOR_PARAMS_;
            double coefficient = NAN;
            rc = strTo<double>(itParam->second.c_str(), coefficient); //OLD float coefficient = atof(itParam->second.c_str());
            mapParams.erase(itParam);

            if((rc != _RC_SUCCESS_) || (coefficient <= 0))
			{
///TODO!!! ������ �� ����� � ���???
				return _ERROR_WRONG_ARGUMENT_;
			}
			indexes.push_back((double)coefficient);
		}
		ptrToIParams = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, indexes.size());
		if (!ptrToIParams)
		{
			return _ERROR_WRONG_ARGUMENT_;
		}
		for (size_t i = 0; i < indexes.size(); ++i)
		{
			ptrToIParams->setParam(i, indexes[i]);
		}
		return _RC_SUCCESS_;
	}
	if (m_structuralChangeTesterName == structuralChangeTestersNames[IndexesOfstructuralChangeTestersNames::indexGregoryChowTester])
	{
		itParam = mapParams.find(grammarGregoryChowTesterParams[IndexesOfGregoryChowTesterParams::indexDistFile]);
		std::string tmp = itParam->second.c_str();
		if (tmp.empty())
		{
			return _ERROR_WRONG_ARGUMENT_;
		}
		int rc = getDistMap(tmp, this->m_fisherDistMap);
		if (rc)
		{
			return  _ERROR_WRONG_ARGUMENT_;
		}
		return _RC_SUCCESS_;
	}
	return _ERROR_WRONG_ARGUMENT_;
}

int PW_PolynomialRegressionEstimator::initChangeTester()
{
	ConfigParser structuralChangeTesterParamsParser(m_structuralChangeTesterCfgFileName);
	if (structuralChangeTesterParamsParser.fileExists())
	{
		auto mapEstimatorParams = structuralChangeTesterParamsParser.parse();

		size_t size = 0;
		std::string postfix = "=";
		int rc = parseAndCreateStructuralChangeTesterParams(mapEstimatorParams, postfix, size, m_structuralChangeTesterParams);
		if (rc)
		{
			return _ERROR_WRONG_ARGUMENT_;
		}
	}
	return _RC_SUCCESS_;
}

int PW_PolynomialRegressionEstimator::TestCurrentPointIsStructuralChange(Segment &segment, ProcessInfo *processInfo, size_t polynomDegree, bool &result)
{
	if (m_structuralChangeTesterName == structuralChangeTestersNames[IndexesOfstructuralChangeTestersNames::indexNextResidualAsGaussianOutlierTester])
	{
		double newSegmentSelectorCoefficient = NAN;
		m_structuralChangeTesterParams->getParam(IndexesOfNextResidualAsGaussianOutlierTesterParams::indexNewSegmentSelectorCoefficient, newSegmentSelectorCoefficient);
		if (isNAN_double(newSegmentSelectorCoefficient))
		{
			return _ERROR_WRONG_ARGUMENT_;
		}
		result = TestNextResidualAsGaussianOutlier(segment, polynomDegree, processInfo, newSegmentSelectorCoefficient);
		return _RC_SUCCESS_;
	}

	if (m_structuralChangeTesterName == structuralChangeTestersNames[IndexesOfstructuralChangeTestersNames::indexGregoryChowTester])
	{
		result = New_Chow_test(segment, polynomDegree, processInfo, m_fisherDistMap);
		return _RC_SUCCESS_;
	}

	return _ERROR_WRONG_ARGUMENT_;
}

int SimpleEstimators::PW_PolynomialRegressionEstimator::estimate(ptrInt threadInterruptSignal)
{
    if(!validateEstimatorParamsAndInputData())
    {
        return _ERROR_WRONG_INPUT_DATA_;
    }

    m_segments.push_back(Segment(0, m_numStartPoints));
    InitSegment(m_segments[0], m_processInfo->x, m_processInfo->y,  m_estimatesDim);

    size_t i, k, endI;
	endI = m_processInfo->x.size();
	k = 0;
	bool addNewPointInCurrentSegment = false;
	while (m_segments[k].end < endI)
    {
        /** check if current point is structural change point
        If so, init/create next segment!
        */

		int rc = TestCurrentPointIsStructuralChange(m_segments[k], m_processInfo, m_estimatesDim, addNewPointInCurrentSegment);
		if (rc)
		{
			return  _ERROR_WRONG_OUTPUT_DATA_;
		}
		if (addNewPointInCurrentSegment)
		{
			// adding new point into segment, recounting parameters
			addPoint(m_segments[k], m_estimatesDim, m_processInfo->x, m_processInfo->y);
		}
		else
		{
			if (m_segments[k].end == (int)endI)
			{
				break;
			}

#ifdef UPDATE_INDEX

			int newSegmentStartIndex = m_segments[k].end;
			for (int ui = newSegmentStartIndex; ui < endI; ++ui)
			{
				m_processInfo->x[ui] = ui - newSegmentStartIndex;
			}
#endif
			///break if the number of points remained in dataset is less than MIN_NUMBER_OF_POINTS_IN_SEGMENT
			if (m_segments[k].end + m_numStartPoints >= (int)endI)
			{
				m_segments.push_back(Segment(m_segments[k].end, (int)endI));
				InitSegment(m_segments[m_segments.size() - 1], m_processInfo->x, m_processInfo->y, m_estimatesDim);
				break;
			}

			/** since the current point is structural change point
			create next piece of piece-wise model
			*/
			m_segments.push_back(Segment(m_segments[k].end, m_segments[k].end + m_numStartPoints));
            InitSegment(m_segments[m_segments.size() - 1], m_processInfo->x, m_processInfo->y, m_estimatesDim);
            ++k;
        }
    }

	IParams * mean = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, endI);
	if (!mean)
	{
		return _ERROR_NO_ROOM_;
	}

	IParams * std = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, endI);
	if (!std)
	{
		delete mean;
		mean = NULL;
		return _ERROR_NO_ROOM_;
	}
	std::stringstream tmpstream;


	for (size_t i = 0; i< m_segments.size(); ++i)
	{
		IParams * fresults = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, m_segments[i].params.size());
		if (!fresults)
		{
			delete mean;
			mean = NULL;
			delete std;
			std = NULL;
			return _ERROR_NO_ROOM_;
		}
		IParams * fresultsInd = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, 2);
		if (!fresultsInd)
		{
			delete mean;
			mean = NULL;
			delete std;
			std = NULL;
			delete fresults;
			fresults = NULL;
			return _ERROR_NO_ROOM_;
		}
		for (size_t j = 0; j < m_segments[i].params.size(); ++j)
		{
			fresults->setParam(j, m_segments[i].params[j]);
		}
		tmpstream << POLYNOMIAL_REGRESSION_COEFS_ESTIMATES_TRACKNAME << "_" << i;

		m_trNames.push_back(tmpstream.str());
		tmpstream.str(std::string());
		tmpstream.clear();
		fresultsInd->setParam(0, m_segments[i].begin);
		fresultsInd->setParam(1, m_segments[i].end);
		tmpstream << POLYNOMIAL_REGRESSION_INTERVAL_BOUNDARIES_TRACKNAME << "_" << i;
		m_trNames.push_back(tmpstream.str());
		tmpstream.str(std::string());
		tmpstream.clear();

		size_t k = 0;
		for (k = m_segments[i].begin; k < m_segments[i].end; ++k)
		{
			double value = LinearRegr(m_processInfo->x[k], m_segments[i].params, m_segments[i].params.size() - 1);
			mean->setParam(k, value);
			std->setParam(k, m_segments[i].standartDeviation);
		}

		if (m_setOfParamsEstimates->addParamsCopyToSet(m_model, fresults, m_logger) != true)
		{
			delete mean;
			mean = NULL;
			delete std;
			std = NULL;
			delete fresults;
			fresults = NULL;
			delete fresultsInd;
			fresultsInd = NULL;
			return  _ERROR_WRONG_OUTPUT_DATA_;
		}
		if (m_setOfParamsEstimates->addParamsCopyToSet(m_model, fresultsInd, m_logger) != true)
		{
			delete mean;
			mean = NULL;
			delete std;
			std = NULL;
			delete fresults;
			fresults = NULL;
			delete fresultsInd;
			fresultsInd = NULL;
			return  _ERROR_WRONG_OUTPUT_DATA_;
		}
		delete fresults;
		delete fresultsInd;
	}

	if (m_setOfParamsEstimates->addParamsCopyToSet(m_model, mean, m_logger) != true)
	{
		delete mean;
		mean = NULL;
		delete std;
		std = NULL;
		return  _ERROR_WRONG_OUTPUT_DATA_;
	}
	m_trNames.push_back(POLYNOMIAL_REGRESSION_MEAN_TRACKNAME);
	if (m_setOfParamsEstimates->addParamsCopyToSet(m_model, std, m_logger) != true)
	{
		delete mean;
		mean = NULL;
		delete std;
		std = NULL;
		return  _ERROR_WRONG_OUTPUT_DATA_;
	}
	m_trNames.push_back(POLYNOMIAL_REGRESSION_VARIANCE_TRACKNAME);
	//*/
	delete mean;
	delete std;

	//*  for focst/
	double param = NAN;
	m_estimatorParams->getParam(IndexesOfPW_PolynomialRegressionEstimatorParams::indexForecastHorizont, param);
	int hort = (int)param;

	m_estimatorParams->getParam(IndexesOfPW_PolynomialRegressionEstimatorParams::indexForecastStartPointIndex, param);
	// ������ �����, � ������� ���������� �������, ����������� �� ��������� ���������� ���������� ��������.
	int forecastStartPointIndex = (int)param;

	IParams * focst = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, hort);
	if (!focst)
	{
		return _ERROR_NO_ROOM_;
	}

    IParams * src_for_focst = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, hort);
	if (!src_for_focst)
	{
		return _ERROR_NO_ROOM_;
	}

	int counter = 0;
	double index = m_processInfo->x[m_segments[m_segments.size() - 1].end - 1] + 1;

	for (size_t i = index; i < hort + index; i++)
	{
		double value = LinearRegr((double)i, m_segments[m_segments.size() - 1].params, m_estimatesDim);
		focst->setParam(counter, value);
		src_for_focst->setParam(counter, m_data[counter + forecastStartPointIndex]);
		counter++;
	}
	if (m_setOfParamsEstimates->addParamsCopyToSet(m_model, focst, m_logger) != true)
	{
		return  _ERROR_WRONG_OUTPUT_DATA_;
	}
	if (m_setOfParamsEstimates->addParamsCopyToSet(m_model, src_for_focst, m_logger) != true)
	{
		return  _ERROR_WRONG_OUTPUT_DATA_;
	}

	m_trNames.push_back("forecast");
	m_trNames.push_back("src_for_forecast");

	delete focst;
	delete src_for_focst;

	/// ��� ��������

	IParams * srcv = IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, endI);
	if (!srcv)
	{
		return _ERROR_NO_ROOM_;
	}

	for (size_t i = 0; i < endI; i++)
	{
		srcv->setParam(i, m_processInfo->y[i]);
	}
	if (m_setOfParamsEstimates->addParamsCopyToSet(m_model, srcv, m_logger) != true)
	{
		return  _ERROR_WRONG_OUTPUT_DATA_;
	}
	m_trNames.push_back("src");

	delete srcv;

    return _RC_SUCCESS_;
}

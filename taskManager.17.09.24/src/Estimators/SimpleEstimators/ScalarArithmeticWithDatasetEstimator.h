#ifndef __SCALARARITHMETICWITHDATASETESTIMATOR_H__633569422542968750
#define __SCALARARITHMETICWITHDATASETESTIMATOR_H__633569422542968750
#include <vector> /* vector */
#include "../Estimator.h"
#include "FakeSimpleEstimator.h"

///TODO!!! move to the class members to refactor getEstimatesAsDataInfoArray
#define SCALAR_ARITHMETIC_OPERATION_WITH_DATASETNAME "ResultOfScalarArithmenticWithDataset"
///#define MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_0 "coef_0"
///#define MODIFIED_EXP_REGRESSION_SIMPLPEESTIMATOR_TRACKNAME_1 "coef_1"

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace SimpleEstimators{
    /** class container */
/** implementation of basic methods
 declared in abstract Base class ISimpleEstimator
 The methods setTask, setLog, toString
 have to be inherited all SimpleEstimators
*/
class ScalarArithmeticWithDatasetEstimator : public virtual ISimpleEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakeSimpleEstimator
*/
                                       public virtual FakeSimpleEstimator
{
public:
    enum IndexesOfScalarArithmeticEstimatorParams{
        indexOfOperation,
        indexOfOperandValue,
        FAKE_INDEX_SCALAR_ARITHMETIC_PARAMS
    };

    /** FAKE_INDEX_GRAMMAR_SCALARARITHMETIC - ��� ����������� ���������� ��������� ����� ����������!
    */
    enum IndexesOfScalarArithmeticWithDatasetEstimatorParams{
        indexOfArithmeticOperation = 0,
        indexOfArithmeticOperandSematics = 1,
        indexOfArithmeticOperand = 2,
        FAKE_INDEX_GRAMMAR_SCALAR_ARITHMETIC
    };
    static const char* grammar[FAKE_INDEX_GRAMMAR_SCALAR_ARITHMETIC];
    enum ScalarArithmeticOperation {
        ADDITION=0,
        SUBTRACTION=1,
        MULTIPLICATION=2,
        DIVISION=3,
        FAKE_OPERATION_PARAM_SEMANTICS=4
    };
    static const char* grammarForOperations[FAKE_OPERATION_PARAM_SEMANTICS];
    static const char* grammarForOperationResult[FAKE_OPERATION_PARAM_SEMANTICS];
    enum ScalarArithmeticOperandSemantics {
        VALUE,
        REFERENCE,
        FAKE_OPERAND_PARAM_SEMANTICS
    };
    static const char* grammarForOperandSemantics[FAKE_OPERAND_PARAM_SEMANTICS];

    ScalarArithmeticWithDatasetEstimator(Logger*& log);
    virtual ~ScalarArithmeticWithDatasetEstimator();
    /** IEstimator stuff */
    using FakeSimpleEstimator::setTask;
    using FakeSimpleEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakeSimpleEstimator::toString;
    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** ISimpleEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimate(ptrInt threadInterruptSignal);
protected:
    using FakeSimpleEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
    size_t m_estimatesDim;
    virtual int setParamsSet();
    virtual int getOperation(double& operation) const;
    virtual int getOperandValue(double& operand) const;
    /** assign and copy CTORs are disabled */
    ScalarArithmeticWithDatasetEstimator(const ScalarArithmeticWithDatasetEstimator&);
    void operator=(const ScalarArithmeticWithDatasetEstimator&);
/** enum SimpleEstimatorsTypes _set; has been Moved to FakeSimpleEstimator */
/** Task* _task; has been Moved to FakeSimpleEstimator */
/** Logger* m_logger; has been Moved to FakeSimpleEstimator */
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    IParams* m_estimatorParams;

///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;
};

} /// end namespace SimpleEstimators
#endif

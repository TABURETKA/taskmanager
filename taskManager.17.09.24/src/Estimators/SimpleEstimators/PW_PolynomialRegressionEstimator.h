#ifndef __PW_POLYNOMIALREGRESSIONESTIMATOR
#define __PW_POLYNOMIALREGRESSIONESTIMATOR
#include "../Estimator.h"
#include "FakeSimpleEstimator.h"
#include "PW_PolynomialRegressionEstimatorStuff/EstimatorLS.h"
#include "PW_PolynomialRegressionEstimatorStuff/ChowTest.h"

///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[Params1D] ??? + ���������� ����� ����������???
#define PW_POLYNOMIAL_REGRESSION_SIMPLPE_ESTIMATOR_DATASETNAME "PW_PolynomialRegressionEstimates"
#define POLYNOMIAL_REGRESSION_COEFS_ESTIMATES_TRACKNAME "PolynomialRegressionCoeffs"
#define POLYNOMIAL_REGRESSION_INTERVAL_BOUNDARIES_TRACKNAME "PolynomialRegressionIntervalBoundaries"
#define POLYNOMIAL_REGRESSION_MEAN_TRACKNAME "PolynomialRegressionMean"
#define POLYNOMIAL_REGRESSION_VARIANCE_TRACKNAME "PolynomialRegressionVariance"
#define MIN_NUMBER_OF_POINTS_IN_SEGMENT 15 // minimum amount of points in one segment

#define UPDATE_INDEX


/** FakeIndex_PW_PolynomialRegressionEstimatorParams - ��� ����������� ���������� ��������� ����� ����������!
�� �������������� ������!
*/

enum IndexesOfPW_PolynomialRegressionEstimatorParams{
		indexForecastStartPointIndex = 0,
		indexTrainingSampleSize = 1,
		indexForecastHorizont = 2,
		indexStructuralChangeTesterName = 3,
		indexStructuralChangeTesterCfgFileName = 4,
        FakeIndex_PW_PolynomialRegressionEstimatorParams
};

/** tokens to parse map for Estimator Params */
static const char * grammarPW_PolynomialRegressionEstimatorParams[FakeIndex_PW_PolynomialRegressionEstimatorParams] = {
	"ForecastStartPointIndex",
	"TrainingSampleSize",
	"ForecastHorizont",
	"StructuralChangeTesterName",
	"StructuralChangeTesterCfgFileName"
};

/** structuralChangeTesters **/
enum IndexesOfstructuralChangeTestersNames
{
	indexNextResidualAsGaussianOutlierTester = 0,
	indexGregoryChowTester = 1,
	FakeIndex_structuralChangeTestersNames
};

static const char * structuralChangeTestersNames[FakeIndex_structuralChangeTestersNames] = {
	"NextResidualAsGaussianOutlierTester",
	"GregoryChowTester"
};

enum IndexesOfNextResidualAsGaussianOutlierTesterParams
{
	indexNewSegmentSelectorCoefficient = 0,
	FakeIndex_NextResidualAsGaussianOutlierTesterParams
};

static const char * grammarNextResidualAsGaussianOutlierTesterParams[FakeIndex_NextResidualAsGaussianOutlierTesterParams] = {
	"NewSegmentSelectorCoefficient"
};

enum IndexesOfGregoryChowTesterParams
{
	indexDistFile = 0,
	FakeIndex_GregoryChowTesterParams
};

static const char * grammarGregoryChowTesterParams[FakeIndex_GregoryChowTesterParams] = {
	"DistFile"
};

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace SimpleEstimators{
/** class container */
/** implementation of basic methods
 declared in abstract Base class ISimpleEstimator
 The methods setTask, setLog, toString
 have to be inherited all SimpleEstimators
*/
class PW_PolynomialRegressionEstimator : public virtual ISimpleEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakeSimpleEstimator
*/
                                       public virtual FakeSimpleEstimator
{
public:
    PW_PolynomialRegressionEstimator(Logger*& log);
    virtual ~PW_PolynomialRegressionEstimator();

    /** IEstimator stuff */
    using FakeSimpleEstimator::setTask;
    using FakeSimpleEstimator::setLog;

    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;


    /** ����� ������, ����������� ��� ������� ���������! */
    using FakeSimpleEstimator::toString;

    /** ISimpleEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimate(ptrInt threadInterruptSignal);
protected:
    using FakeSimpleEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
    bool validateEstimatorParamsAndInputData();
	int TestCurrentPointIsStructuralChange(Segment &segment, ProcessInfo *processInfo, size_t polynomDegree, bool &result);
	int parseAndCreateStructuralChangeTesterParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams* &ptrToIParams);
	int initChangeTester();
/** ��������� �������������� ������������� ������ Y(t)=param[0] + param[1] * t + ...
����� ����� ������������ �����������, ��� ������ ���� �������� � ����� ���������� �������������� ������!
����������� ������� ������ ����� �������� �� ����������� �������������� ������ m_model, ��������� ��������� ��������!
*/
    size_t m_estimatesDim;
    int m_numStartPoints;
    virtual int setParamsSet();

    /** assign and copy CTORs are disabled */
    PW_PolynomialRegressionEstimator(const PW_PolynomialRegressionEstimator&);
    void operator=(const PW_PolynomialRegressionEstimator&);
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    IParams*     m_estimatorParams;
    std::vector<double> m_data;
///TODO!!! �����������, ����� �� �� ������������ ��� ������������ ��������� ������?
    std::vector< Segment > m_segments;
///TODO!!! ��� ���������� ���� ����� ����������, �� ������. ����� ��� ��������� � ����� ���������� ��������� �������?
    ProcessInfo * m_processInfo;

	/// testers stuff
	
	std::vector< std::string > m_trNames;
	std::string m_structuralChangeTesterName;
	std::string m_structuralChangeTesterCfgFileName;
	
	///tester params stuff
	std::map<int, vector<double>> m_fisherDistMap;
	IParams*    m_structuralChangeTesterParams;

};

} /// end namespace SimpleEstimators
#endif

#ifndef __PW_POLYNOMIALREGRESSIONESTIMATORFORGIVENSUBSETS_H__633569422542968750
#define __PW_POLYNOMIALREGRESSIONESTIMATORFORGIVENSUBSETS_H__633569422542968750
#include <vector> /* vector */
#include "../Estimator.h"
#include "FakeSimpleEstimator.h"
///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[Params1D] ??? + ���������� ����� ����������???
#define PW_POLYNOMIAL_REGRESSION_GIVEN_SUBSETS_SIMPLPEESTIMATOR_DATASETNAME "PW_PolynomialRegressionEstimates"
#define POLYNOMIAL_REGRESSION_COEFS_ESTIMATES_TRACKNAME "PolynomialRegressionCoeffs"

/** FakeIndex_PW_PolynomialRegressionEstimatorForGivenSubsetsParams - ��� ����������� ���������� ��������� ����� ����������!
�� �������������� ������!
*/
enum IndexesOfPW_PolynomialRegressionEstimatorForGivenSubsetsParams{
        indexTypeOfNestedRegressionEstimator,
        indexNameOfNestedRegressionEstimator,
        indexCfgFileNameOfNestedRegressionEstimator,
        FakeIndex_PW_PolynomialRegressionEstimatorForGivenSubsetsParams
};
static const char* grammarPW_PolynomialRegressionEstimatorForGivenSubsetsParams[FakeIndex_PW_PolynomialRegressionEstimatorForGivenSubsetsParams]={
       "TypeOfNestedRegressionEstimator",
       "NameOfNestedRegressionEstimator",
       "CfgNameOfNestedRegressionEstimatorCfg"
};
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace SimpleEstimators{
    /** class container */
/** implementation of basic methods
 declared in abstract Base class ISimpleEstimator
 The methods setTask, setLog, toString
 have to be inherited all SimpleEstimators
*/
class PW_PolynomialRegressionEstimatorForGivenSubsets : public virtual ISimpleEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakeSimpleEstimator
*/
                                       public virtual FakeSimpleEstimator
{
public:
    PW_PolynomialRegressionEstimatorForGivenSubsets(Logger*& log);
    virtual ~PW_PolynomialRegressionEstimatorForGivenSubsets();
    /** IEstimator stuff */
    using FakeSimpleEstimator::setTask;
    using FakeSimpleEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakeSimpleEstimator::toString;
    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** ISimpleEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimate(ptrInt threadInterruptSignal);
protected:
    using FakeSimpleEstimator::logErrMsgAndReturn;
    virtual int parsingFailed(std::string msg) const;

private:
    const static size_t minLengthOfSingleSubset = 3;
    bool validateEstimatorParamsAndInputData();
    int putDataSubsetAsDataInfoArray(size_t indexOfSubset, DataInfo** di) const;
/** ��������� �������������� ������������� ������ Y(t)=param[0] + param[1] * t + ...
����� ����� ������������ �����������, ��� ������ ���� �������� � ����� ���������� �������������� ������!
����������� ������� ������ ����� �������� �� ����������� �������������� ������ m_model, ��������� ��������� ��������!
*/
    size_t m_estimatesDim;
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    PW_PolynomialRegressionEstimatorForGivenSubsets(const PW_PolynomialRegressionEstimatorForGivenSubsets&);
    void operator=(const PW_PolynomialRegressionEstimatorForGivenSubsets&);
/** enum SimpleEstimatorsTypes _set; has been Moved to FakeSimpleEstimator */
/** Task* m_task; has been Moved to FakeSimpleEstimator */
/** Logger* m_logger; has been Moved to FakeSimpleEstimator */
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    IParams* m_estimatorParams;

    IEstimator* m_nestedRegressionEstimator;
///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;

};

} /// end namespace SimpleEstimators
#endif


#pragma once

#include "../../EstimatorFactory.h"
#include "EstimatorLS.h"
#include "ConfigParse.h"
#include "Filestuff.h"
#include "Error.h"
#include "EstimatorLSPW.h"


class EstimatorRegr : public IntervalEstimator
{
public:
    EstimatorRegr();
    bool estimateInterval(vector<double> track,
        PF_Model* model,
        PF_Params* start, PF_Params* end,
        Logger* log);

    bool checkNeedFix(vector<double> track, Logger* log);

private:
    void clear();
	void estimateBoundaries();
    double quantilThreshold(vector<double> samples);
	void startPoint();
	void intensity();
	void getDeviation();
	void evalFirstDifferences();
	void regressionCoefficient();
	void fillParamsBoundaries(PF_Params* start, PF_Params* end);
    double getBestLambda(vector<double> lambda, double coef0, int pos_start, int pos_end);
    void getPointsOfChange();
	double startMax, startMin;
	double devMax, devMin;
	double intensMax, intensMin;
	double regrMax, regrMin;
	double jumpMax, jumpMin;
	double threshold;
	vector<double> dev;
	vector<double> regressionCoef;
    vector<double> jump_time;
    vector<double> jump;

	vector<double> def_loading;
	vector<double> abs_def_loading;
    vector<double> track;
	Logger* logger;
	/** ISO C++ forbids initialization of member 'configFile' making 'configFile' static|
        invalid in-class initialization of static data member of non-integral type 'std::string'
    */
    std::string configFile ; //= "estConfig.ini";
};

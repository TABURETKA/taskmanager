#include <float.h> //DBL_MIN
#include "EstimatorLS.h"

/*
    @brief  Estimates one value for linear regression
    @param  x -- value
    @param  param -- pair of parameters for segment
    @return Returns value of linear regression with set parameters
*/
double LinearRegr(const double x, const std::vector<double> param, int degPolynom)
{
    double y = param[0];
    double tmpX = x;
    for(size_t i = 1; i < param.size(); ++i)
    {
        y += param[i] * tmpX;
        tmpX *= x;
    }
    return y;
    //return param[0] + param[1] * x + param[2] * x * x + param[3] * x * x * x + param[4] * x * x * x * x + param[5] * x * x * x * x * x;
    //return param[0] * x + param[1];
}


/*
    @brief  Estimates L2 distance between linear regression and true y values
    @param  x, y -- arrays of input information
    @param  begin, end -- boundaries of segment for arrays x, y
    @param  a, b -- two parameters of regression for current segment
    @param  f -- fuction for calculation regression
    @return Returns distance
*/
double EvaluateL2Distance(const std::vector<double> &x, const std::vector<double> &y, const size_t& begin, const size_t &end, const std::vector<double> &v, const RegressionVal& f, const int deg)
{
    double variance = 0;
    for (size_t i = begin; i != end; ++i)
    {
        variance += pow(y[i] - f(x[i], v, deg), 2);
    }
    ///NB unbiased estimate!
    return variance / (end - begin - 2);
}



double EvaluateL2DistanceExp(const std::vector< double > &xn, const std::vector< double > &y, const size_t begin, const size_t end, const double coefExp, const double coef0)
{
    vector<double> x;
    for(size_t i = 0; i < y.size(); ++i)
    {
        x.push_back(i);
    }
    double variance = 0;
    for (size_t i = begin; i != end; ++i)
    {
        variance += pow(y[i] - coef0 * exp(-coefExp * x[i - begin]), 2);
    }
    ///NB unbiased estimate!
    return variance / (end - begin - 2);
}


/*
    @brief  Estimates sum of elemets of array
    @param  x -- array
    @param  begin, end -- boundaries in array
    @return Returns sum of all elements in x array from begin index to end
*/
double Sum(const std::vector< double > &x, const size_t begin, const size_t end)
{
    double res = 0;
    for (size_t i = begin; i != end; ++i)
    {
        res += x[i];
    }
    return res;
}


/*
    @brief  Estimates average of elemets of array
    @param  x -- array
    @param  begin, end -- boundaries of array
    @return Returns first moment
*/
double FirstMoment(const std::vector<double> &x, const size_t begin, const size_t end)
{
    return Sum(x, begin, end) / (end - begin);
}


std::vector<std::vector<double> > BuildXMatr(const std::vector<double> &x, const int begin, const int end, const int degPolynom)
{
    std::vector < std::vector<double> > X(degPolynom + 1, std::vector<double>(degPolynom + 1));
    std::vector<double> xSumValues(2 * degPolynom);
    for(int i = begin; i != end; ++i)
    {
        double xTmp = x[i - begin];
        for(size_t j = 0; j < xSumValues.size(); ++j)
        {
            xSumValues[j] += xTmp;
            xTmp *= x[i - begin];
        }
    }

    for(size_t i = 0; i < X.size(); ++i)
    {
        for(size_t j = 0; j < X.size(); ++j)
        {
            X[i][j] = xSumValues[i + j - 1];
        }
    }
    X[0][0] = end - begin;
    return X;
}

std::vector<double> BuildRightPart(const std::vector<double> &x, const std::vector<double> &y, const int begin, const int end, const int degPolynom)
{
    std::vector<double> b(degPolynom + 1);
    for(int i = begin; i != end; ++i)
    {
        double yTmp = y[i];
        for(size_t j = 0; j < b.size(); ++j)
        {
            b[j] += yTmp;
            yTmp *= x[i - begin];
        }
    }
    return b;
}





/*
    @brief  Using least squares method for linear regression estimates its parameters
    @param  x, y -- input arrays
    @param  begin, end -- boundaries of arrays
    @return Returns pair of parameters
*/
std::vector<double> FindParams(const std::vector<double> &xk, const std::vector< double > &y, const size_t begin, const size_t end, const int degPolynom)
{
    std::vector<double> x;
    for(size_t i = 0; i < y.size(); ++i)
    {
        x.push_back(i);
    }
    std::vector<std::vector<double> > A = BuildXMatr(x, begin, end ,degPolynom);
    std::vector<double> b = BuildRightPart(x, y, begin, end, degPolynom);
    for(size_t i = 0 ; i < A.size(); ++i)
    {
        A[i].push_back(b[i]);
    }

    std::vector<double> par(A.size());


    size_t n = A.size();
    for (size_t i = 0; i < n; i++)
    {
        double maxEl = abs(A[i][i]);
        size_t maxRow = i;
        for (size_t k = i + 1; k < n; k++)
        {
            if (abs(A[k][i]) > maxEl)
            {
                maxEl = abs(A[k][i]);
                maxRow = k;
            }
        }

        for (size_t k = i; k < n + 1;k++)
        {
            double tmp = A[maxRow][k];
            A[maxRow][k] = A[i][k];
            A[i][k] = tmp;
        }
        for (size_t k = i + 1; k < n; k++)
        {
            double c = -A[k][i] / A[i][i];
            for (size_t j = i; j < n + 1; j++)
            {
                if (i == j)
                {
                    A[k][j] = 0;
                }
                else
                {
                    A[k][j] += c * A[i][j];
                }
            }
        }
    }

    for (int i = n - 1; i >= 0; i--)
    {
        par[i] = A[i][n] / A[i][i];
        for (int k = i - 1;k >= 0; k--)
        {
            A[k][n] -= A[k][i] * par[i];
        }
    }

    return par;
}


/*
    @brief  Estimates first moment for all elemets of array
    @param  x,y -- two arrays of input distributions
    @param  begin, end -- boundaries of arrays
    @param  a -- parameter of distribution
    @return
*/
double S(const double a, const std::vector<double> &x, const std::vector<double> &y, const size_t begin, const size_t end)
{
    double Ey = FirstMoment(y, begin, end);
    double Ex = FirstMoment(x, begin, end);

    double res = 0;
    double sum = 0;

    size_t i;

    for (i = begin; i != end; ++i)
    {
        res += pow((y[i] - Ey), 2);
        sum += pow((x[i] - Ex), 2);
    }

    res += a * a * sum;
    return res;
}

double getBestLambda(const std::vector<double> &lambda, const double coef0, const int pos_start, const int pos_end, const std::vector<double> &x, const std::vector<double> &y)
{
    vector<double> variance_v;
    for(size_t i = 0; i < lambda.size(); ++i) // for each lambda
    {
        double cur_lambda = lambda[i];
        double variance = 0;
        for(int j = pos_start; j < pos_end; ++j)
        {
            variance += pow((coef0 * exp(-cur_lambda * x[j])) - y[j], 2);
        }
        variance_v.push_back(variance);
    }
    double min = variance_v[0];
    int k = 0;
    for(size_t i = 1; i < lambda.size(); ++i)
    {
        if(variance_v[i] < min)
        {
            min = variance_v[i];
            k = i;
        }
    }
    return lambda[k];
}


double countLambda(const std::vector<double> &x, const std::vector<double> &y, const std::vector<double> &coef, const int start, const int end, const int degPolynom)
{
    double best_lambda = 0;
    std::vector<double> lambda;

    if(degPolynom == 1)
    {
        if(coef[0] < DBL_MIN)
        {
            best_lambda = -coef[1] / coef[0];
        }
    }
    else if(degPolynom == 2)
    {
        if(coef[0] < DBL_MIN)
        {
            lambda.push_back(-coef[1]/coef[0]);
            lambda.push_back(sqrt(2 * coef[2] / coef[0]));
            best_lambda = getBestLambda(lambda, coef[0], start, end, x, y);
        }
    }
    else if(degPolynom > 2)
    {
        lambda.push_back(-2 * coef[2] / coef[1]);
        lambda.push_back(-3 * coef[3] / coef[2]);
        if(coef[3] / coef[1] > 0)
            lambda.push_back(sqrt(6 * coef[3] / coef[1]));
        best_lambda = getBestLambda(lambda, coef[0], start, end, x, y);
    }

    return best_lambda;
}

vector<double> UpdateY(const vector<double> &x, const vector<double> &y, const int start, int ind_segm, ProcessInfo *processInfo)
{
    vector<double> t_array = y;
    for(size_t i = start; i < y.size(); ++i)
    {
        double t = processInfo->getVal(x[i], ind_segm);
        t_array[i] -= t;
    }
    return t_array;
}

/*
    @brief  Estimates parameters of new segment and initializes it
    @param  x,y -- two arrays with input distributions
    @param  segment -- data structure
*/
void InitSegment(Segment &segment, const std::vector< double > &x, const std::vector< double > &y, const int degPolynom, ProcessInfo *processInfo)
{
    //std::pair<double, double> parameters = FindEaEb(x, y, segment.begin, segment.end);
    std::vector<double> parameters = FindParams(x, y, segment.begin, segment.end, degPolynom);

    size_t  ind_segm = processInfo->coef0.size() - 1;
    segment.params = parameters;
    processInfo->coef0[ind_segm] = parameters[0];
    processInfo->lambda[ind_segm] = countLambda(x, y, parameters, segment.begin, segment.end, degPolynom);
    processInfo->start[ind_segm] = segment.begin;
    segment.variance = sqrt(EvaluateL2DistanceExp(x, y, segment.begin, segment.end, processInfo->lambda[ind_segm], processInfo->coef0[ind_segm]));
}



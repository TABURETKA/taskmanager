#ifndef ARGPARSE_H__
#define ARGPARSE_H__

#include <map>
#include <vector>
#include <iostream>
#include <stdio.h> //printf
#include <string>

#include "filestuff.h"
#include "error.h"
static std::string g_nameInputFile;

enum typeArg { ARG_ERR, ARG_HELP, ARG_FILE };

typedef struct cmd_param
{
    std::string config_fileName; // name of configuration file with some settings
    cmd_param()
    {
        config_fileName = "";
    }

} cmd_param_t;


int ParseArguments(int numArg, char * args[], cmd_param_t * my_cmd_param);

#endif

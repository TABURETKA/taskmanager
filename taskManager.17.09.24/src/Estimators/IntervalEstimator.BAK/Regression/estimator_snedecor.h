#ifndef ESTIMATOREX_H__
#define ESTIMATOREX_H__

#include "estimator_base.h"
#include "chow_test.h"
#include <vector>

using std::vector;
namespace regression
{
    class EstimatorSnedecor : public EstimatorBase
    {
    private:
        void initParameters(parameters_t* param);

        friend double GetSnedecorValue(int numOfDegreesOfFreedom, EstimatorSnedecor &);
        friend bool ReadSnedecorCoeff(std::ifstream &inputStream, EstimatorSnedecor &);
        friend bool AddPoint(Segment &segment, EstimatorSnedecor &);

    public:
        void estimate(parameters_t* my_param);
        EstimatorSnedecor() {};
        ~EstimatorSnedecor() {};
    };

}
#endif

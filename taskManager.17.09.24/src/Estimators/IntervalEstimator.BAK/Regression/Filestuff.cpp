#ifdef _WIN32
#include <io.h>
#include <windows.h>
#else
#include <unistd.h>
#endif
#include <stdio.h>
#include "Filestuff.h"

bool isFileExist(const char* filename)
{
    int result = -1;
#ifdef _WIN32
    result = _access(filename, 0);
#else
    result = access(filename, 0);
#endif
    if (0 == result)
    {
        return true;
    }
    return false;
}

#ifdef _WIN32
bool IsFileExists(const char * filename)
{
    return GetFileAttributes((LPCTSTR)filename) != 0xFFFFFFFF;
}
#endif



bool readInputData(std::ifstream &inputStream, std::vector< double > &x, std::vector< double > &y)
{
    int numPairs;

    if (!(inputStream >> numPairs))
        return false;

    for (int i = 0; i != numPairs; ++i)
    {
        double value;

        if (!(inputStream >> value))
            return false;
        x.push_back(value);

        if (!(inputStream >> value))
            return false;
        y.push_back(value);
    }

    return true;
}

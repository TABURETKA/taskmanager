#include "./headers/argparse.h"


typedef void(*InterfaceFunc_t) (const std::vector<std::string> &arguments, cmd_param_t * my_cmd_param);
std::map<std::string, InterfaceFunc_t> tableCommand;


/*
    @brief  Sets all elements of string to upper case
    @param  str -- input string
    @return string with all elements in upper case
*/
std::string UpperCase(char* str)
{
    int i = 0;
    while (str[i])
    {
        if (str[i] > 'a' && str[i] < 'z')
        {
            str[i] = toupper(str[i]);
        }
        ++i;
    }
    return std::string(str);
}


/*
    @brief  Updates special data structure with a name of configuration file
    @param  arguments -- vector with arguments for command
    @param  my_cmd_param -- special data structure with parametres for algorithm
*/
void Interface_Config(const std::vector <std::string> &arguments, cmd_param_t * my_cmd_param)
{
    if (arguments.size() != 1) // Wrong amounts of arguments
    {
        error_inf e;
        e.err = ERR_WR_NUM_ARG;
        e.module = "Configuration file";
        e.procedure = "Input data file";
        throw(e);
        return;
    }
    if (!isFileExist(arguments[0].c_str()))
    {
        error_inf e;
        e.err = ERR_FILE_EX;
        e.module = "Configuration file";
        e.procedure = "Input data file";
        throw(e);
        return;
    }
    else
    {
        my_cmd_param->config_fileName = arguments[0];
    }
}


/*
	@brief  Initializes map with pairs (function, string)
*/
void InterfaceInit()
{
    tableCommand[":CONFIG"] = &Interface_Config;
}


/*
    @brief  Parses input long string with all arguments
    @param  numArg -- amount of words in string
    @param  args -- string with arguments
    @param  my_cmd_param -- special data structure to be filled with information for algorithm
*/
int ParseArguments(int numArg, char * args[], cmd_param_t * my_cmd_param)
{
    std::map<std::string, std::vector<std::string> > commands;
    std::string filename = "";
    InterfaceInit();
    int num = 1;
    while (num < numArg)
    {
        if (args[num][0] == ':')
        {
            std::string  com = UpperCase(args[num]);
            std::vector<std::string> listArg;
            num++;
            while (num < numArg && args[num][0] != ':')
            {
                listArg.push_back(args[num]);
                num++;
            }
            if (commands.count(com) == 0)
            {
                commands[com] = listArg;
            }
            else
            {
                return -1;
            }
        }
        else
            return -1;
    }

    for (std::map<std::string, std::vector<std::string> >::iterator it = commands.begin(); it != commands.end(); ++it)
    {
        std::string curCommand = (*it).first;
        if (tableCommand.find(curCommand) == tableCommand.end())
        {
            error_inf e;
            e.err = ERR_UNKNW_PRM;
            e.module = "Command line";
            e.procedure = "Flags";
            throw(e);
            return -1;
        }
        else
        {
            (tableCommand[curCommand])((*it).second, my_cmd_param);
        }
    }
    return 0;
}


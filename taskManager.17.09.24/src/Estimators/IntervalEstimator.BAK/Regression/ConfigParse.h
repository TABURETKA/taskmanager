#ifndef CONFIGPARSE_H__
#define CONFIGPARSE_H__

#include <string.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>

#include "Filestuff.h"
#include "Error.h"
#include "../../../StringStuff/StringStuff.h"
enum est_t { ESTIMATOR_SIMPLE, ESTIMATOR_STUDENT, ESTIMATOR_SNEDECOR};

typedef struct parameters
{
    std::string input_file_distr;   // name of the file with distribution values
	bool chow_test_flag;
    std::string f_distr_file;
    parameters()
    {
        input_file_distr = "";
		chow_test_flag = 0;
        f_distr_file = "";
    }
} parameters_t;

int ParseConfigFile(std::string fileName, parameters* my_param);

#endif

#include "EstimatorLSPW.h"

namespace regression
{

    double Estimator::estimateNewY(const double x, ProcessInfo* processInfo, int ind_segm)
    {
        return processInfo->getVal(x, ind_segm);
    }



/*
	@brief  Decides if next point is suitable for current segment, if yes adds it and recounts parameters of the segment
	@param  segment -- current segment
	@param  obj -- data structure with all information about task
	@param  ind_segm -- index of segment we work with
	@param  processInfo -- special structure with full information about parameters of the process
	@return Returns true if possible to add point to current segment, in other case returns false
*/
	bool Estimator::addPoint(Segment &segment, const Estimator &obj, int ind_segm, ProcessInfo *processInfo)
    {
        if (segment.end >= (int)processInfo->x.size() - 1)
            return false;

        int xNext = segment.end;
        double yNext = processInfo->y[xNext];
        double xTmp = processInfo->x[xNext];

        double yE = estimateNewY(xTmp, processInfo, ind_segm);
        double delta = fabs(yNext - yE);
        if (delta <= THREE_SIGMA_COEF  * segment.variance) // adding new point into segment, recounting parameters
        {
            ++segment.end;
            std::vector<double> parameters = FindParams(processInfo->x, processInfo->y, segment.begin, segment.end, degPolynom);
            segment.params = parameters;
            processInfo->coef0[ind_segm] = parameters[0];
            processInfo->lambda[ind_segm] = countLambda(processInfo->x, processInfo->y, parameters, segment.begin, segment.end, degPolynom);
            processInfo->start[ind_segm] = segment.begin;
            double newVariance = sqrt(EvaluateL2DistanceExp(processInfo->x, processInfo->y, segment.begin, segment.end, processInfo->lambda[ind_segm], processInfo->coef0[ind_segm]));
            segment.variance = newVariance;
            return true;
        }

        return false;
    }

/*
	@breif  Main function which is looking for points of significant changings in coefficients of regression (x,y)
	@param  param -- data structure with input arguments for algorithm
	@param  processInfo -- special structure with full information about parameters of the process
*/
	void Estimator::estimate(parameters_t* param, ProcessInfo *processInfo)
    {
        s_segments.push_back(Segment(0, numStartPoints));
        processInfo->resize();
        InitSegment(s_segments[0], processInfo->x, processInfo->y,  degPolynom, processInfo);

        size_t i, k, endI;

        for (i = numStartPoints - 1, k = 0, endI = processInfo->x.size(); i != endI; ++i)
        {
            ///init/create next segment if the current point does not fit current segment
            if (!addPoint(s_segments[k], *this, k, processInfo))
            {
                if(processInfo->lambda[k] < 0.01)
                {
                    int liniarPolynomDeg = 1;
                    std::vector<double> parameters = FindParams(processInfo->x, processInfo->y, s_segments[k].begin, s_segments[k].end, liniarPolynomDeg);

                    s_segments[k].params = parameters;
                    processInfo->coef0[k] = parameters[0];
                    processInfo->polynomDegree[k] = liniarPolynomDeg;
                    processInfo->lambda[k] = countLambda(processInfo->x, processInfo->y, parameters, s_segments[k].begin, s_segments[k].end, liniarPolynomDeg);
                    processInfo->start[k] = s_segments[k].begin;
                    double newVariance = sqrt(EvaluateL2DistanceExp(processInfo->x, processInfo->y, s_segments[k].begin, s_segments[k].end, processInfo->lambda[k], processInfo->coef0[k]));
                    s_segments[k].variance = newVariance;
                }
                ///break if the number of points remained in dataset is less than MIN_NUMBER_OF_POINTS_IN_SEGMENT
                if (s_segments[k].end + numStartPoints >= (int)endI)
                    break;
                processInfo->y = UpdateY(processInfo->x, processInfo->y, s_segments[k].end, k, processInfo);
                s_segments.push_back(Segment(s_segments[k].end + 1, s_segments[k].end + numStartPoints+1));
                processInfo->resize();
                InitSegment(s_segments[s_segments.size() - 1], processInfo->x, processInfo->y, degPolynom, processInfo);
                i += numStartPoints;
                ++k;
            }
        }

        std::pair<std::vector<int>, std::vector<int> > result_info;
        if (param->chow_test_flag)
        {
            Chow_test(param->f_distr_file, s_segments, processInfo->x, processInfo->y, degPolynom, processInfo);
        }

        return ;
    }

}






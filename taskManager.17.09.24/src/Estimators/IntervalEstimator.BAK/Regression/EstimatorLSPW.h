#ifndef ESTIMATORLSPW_H__
#define ESTIMATORLSPW_H__

#include <assert.h>
#include <cmath>
#include <fstream>
#include <iterator>
#include <map>

#include "EstimatorLS.h"
#include "ChowTest.h"
#include "../../../Model/Model.h"

#define THREE_SIGMA_COEF 3.2
using std::vector;

namespace regression
{
    class Estimator : public EstimatorLS
    {
    private:
        bool addPoint(Segment &segment, const Estimator &, int ind_segm, ProcessInfo *processInfo);
        double estimateNewY(const double x, ProcessInfo *processInfo, int ind);
    public:
        void estimate(parameters_t* my_param, ProcessInfo *processInfo);
        Estimator() {};
        virtual ~Estimator() {};

    };

}

#endif


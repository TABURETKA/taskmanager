#include "./headers/estimator_student.h"
namespace regression
{


    /*
        @brief  Returns Student staistic
        @param  numOfDegreesOfFreedom -- parameter to find value
		@param  obj -- data structure with all information about task
        @return Returns Student statistic
    */
    double GetStudentValue(int numOfDegreesOfFreedom, EstimatorStudent &obj)
    {
        std::map< int, double >::iterator findIter = obj.s_Data.find(numOfDegreesOfFreedom);

        if (findIter != obj.s_Data.end())
            return (*findIter).second;
        ///TODO ??? 1.6
        return 1.6;
    }


    /*
        @brief  Fills obj field s_Data with data from input stream
        @param  inputStream -- input stream
		@param  obj -- data structure with all information about task
        @return Returns false if something wrong with input stream
    */
    bool ReadStudentCoeff(std::ifstream &inputStream, EstimatorStudent &obj)
    {
        int numPairs;

        if (!(inputStream >> numPairs))
            return false;

        for (int i = 0; i != numPairs; ++i)
        {
            int    numDegreesOfFreedom;
            double value;

            if (!(inputStream >> numDegreesOfFreedom))
                return false;

            if (!(inputStream >> value))
                return false;

            obj.s_Data[numDegreesOfFreedom] = value;
        }

        return true;
    }


    /*
        @brief  Decides if next point is suitable for current segment, if yes adds it and recounts parameters of the segment
        @param  segment -- current segment
		@param  obj -- data structure with all information about task
		@return Returns true if possible to add point to current segment, in other case returns false
    */
    bool AddPoint(Segment &segment, EstimatorStudent &obj)
    {
        if (segment.end >= (int)obj.x.size())
            return false;

        double Ex = FirstMoment(obj.x, segment.begin, segment.end);

        double divisor = 0;
        for (int i = segment.begin; i != segment.end; ++i)
        {
            divisor += pow((obj.x[i] - Ex), 2);
        }

        divisor *= (segment.end - segment.begin - 2);
        double t = GetStudentValue(segment.end - segment.begin - 2, obj);
        double delta = t * sqrt(S(segment.params[1], obj.x, obj.y, segment.begin, segment.end) / divisor);

        std::pair<double, double> paramNext = FindEaEb(obj.x, obj.y, segment.end - 1, segment.end + 1);

        if (fabs(paramNext.second - segment.params[1]) < delta)
        {
            ++segment.end;

            paramNext = FindEaEb(obj.x, obj.y, segment.begin, segment.end);

            double newVariance = sqrt(EvaluateL2Distance(obj.x, obj.y, segment.begin, segment.end, paramNext.second, paramNext.first, &LinearRegr));


            segment.variance = newVariance;
            segment.params[0] = paramNext.first;
            segment.params[1] = paramNext.second;
            return true;
        }

        return false;
    }


    /*
		@breif  Initializes parameters of algorithm
		@param  param -- data structure with some information for algorithm
    */
    void EstimatorStudent::initParameters(parameters_t* param)
    {
        distr_filename = param->input_file_distr;
        result_filename = param->output_result;
    }

    /*
        @breif  Main function which is looking for points of significant changings in coefficients of linear progression (x,y)
        @param  param -- data structure with input arguments for algorithm
    */
    void EstimatorStudent::estimate(parameters_t* param)
    {

        std::vector< double > x2, y2, lambda2;
        std::vector< int >    xKink, yKink;

        initParameters(param);

        std::ifstream fIn(distr_filename.c_str());

        s_Data.clear();
        ReadStudentCoeff(fIn, *this);

        s_segments.push_back(Segment(0, numStartPoints - 1));
        InitSegment(s_segments[0], x, y);

        size_t i, k, endI;

        for (i = numStartPoints - 1, k = 0, endI = x.size(); i != endI; ++i)
        {
            if (!AddPoint(s_segments[k], *this))
            {
                if (s_segments[k].end + numStartPoints >= (int)endI)
                    break;

                s_segments.push_back(Segment(s_segments[k].end, s_segments[k].end + numStartPoints));
                InitSegment(s_segments[s_segments.size() - 1], x, y);
                i += numStartPoints - 1;
                ++k;
            }
        }

        std::ofstream fResult(result_filename.c_str());
        for (size_t i = 0; i < s_segments.size(); ++i)
        {
            fResult << i << " segment has a = " << s_segments[i].params[1] << std::endl;
        }
        double lambda = EstimateLambda(s_segments, x, y);

        fResult << "1 / lamda (Process): " << lambda << std::endl;
    }
}


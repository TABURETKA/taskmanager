#include "Error.h"

/*
    @brief  Prints information about errors
    @param  err_inf -- data structure with all information about an error
*/
void ErrorInformation(error_inf err_inf, Logger* logger)
{
    std::string module = err_inf.module;
    std::string proc = err_inf.procedure;
    error_t err = err_inf.err;
    switch (err)
    {
        case ERR_FILE_EX:
        {
            std::string str = "There was an error in " + module + " with " + proc + ": No file found.";
            //logger->log(1, str.c_str());
            break;
        }
        case ERR_NAN:
        {
            std::string str = "There was an error in " + module + " with " + proc + ": Not a number.";
            //logger->log(1, str.c_str());
            break;
        }
        case ERR_TYPE_EST:
        {
            std::string str = "There was an error in " + module + " with " + proc + ": Wrong estimation type.";
            //logger->log(1, str.c_str());
            break;
        }
        case ERR_WR_NUM_ARG:
        {
            std::string str = "There was an error in " + module + " with " + proc + ": Wrong amount of parameters.";
            //logger->log(1, str.c_str());
            break;
        }
        case ERR_UNKNW_PRM:
        {
            std::string str =  "There was an error in " + module + " with " + proc + ": Unknown commands.";
           //logger->log(1, str.c_str());
            break;
        }
        default:
        {
            break;
        }
    }
    return;
}

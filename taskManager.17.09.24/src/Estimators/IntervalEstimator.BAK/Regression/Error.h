#ifndef ERROR_H__
#define ERROR_H__

#include<string>
#include<stdio.h>
#include "../../Log/Log.h"

enum error_t {ERR_WR_NUM_ARG, ERR_TYPE_EST, ERR_FILE_EX, ERR_NAN, ERR_UNKNW_PRM};

typedef struct error_information
{
	error_t err;
	std::string module;
	std::string procedure;
} error_inf;

void ErrorInformation(error_inf err, Logger* logger);

#endif

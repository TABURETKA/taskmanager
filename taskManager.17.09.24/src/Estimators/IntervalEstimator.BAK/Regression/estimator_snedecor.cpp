#include "./headers/estimator_snedecor.h"

namespace regression
{
	/*
        @brief  Returns Snedecor staistic
        @param  numOfDegreesOfFreedom -- parameter to find value
	@param  obj -- data structure with all information about task
        @return Returns Snedecor statistic
    */
    double GetSnedecorValue(int numOfDegreesOfFreedom, EstimatorSnedecor &obj)
    {
        std::map< int, double >::iterator findIter = obj.s_Data.find(numOfDegreesOfFreedom);

        if (findIter != obj.s_Data.end())
            return (*findIter).second;
        return 3.0;
    }


	/*
        @brief  Fills obj field s_Data with data from input stream
        @param  inputStream -- input stream
		@param  obj -- data structure with all information about task
        @return Returns false if something wrong with input stream
    */
	bool ReadSnedecorCoeff(std::ifstream &inputStream, EstimatorSnedecor &obj)
    {
        int numPairs;

        if (!(inputStream >> numPairs))
            return false;

        double prevNumDegreeOfFreedom;
        double prevValue;

        for (int i = 0; i != numPairs; ++i)
        {
            int    numDegreesOfFreedom;
            double value;

            if (!(inputStream >> numDegreesOfFreedom))
                return false;

            if (!(inputStream >> value))
                return false;

            if (i == 0)
            {
                prevNumDegreeOfFreedom = numDegreesOfFreedom;
                prevValue = value;
            }

            if (prevNumDegreeOfFreedom < numDegreesOfFreedom)
            {
                for (int j = (int)prevNumDegreeOfFreedom; j <= numDegreesOfFreedom; ++j)
                {
                    double alpha = (double(j - prevNumDegreeOfFreedom)) / (double(numDegreesOfFreedom - prevNumDegreeOfFreedom));

                    obj.s_Data[j] = prevValue * (1 - alpha) + value * alpha;
                }

            }

            prevNumDegreeOfFreedom = numDegreesOfFreedom;
            prevValue = value;

            obj.s_Data[numDegreesOfFreedom] = value;

        }

        return true;
    }


	/*
        @brief  Decides if next point is suitable for current segment, if yes adds it and recounts parameters of the segment
        @param  segment -- current segment
		@param  obj -- data structure with all information about task
		@return Returns true if possible to add point to current segment, in other case returns false
    */
	bool AddPoint(Segment &segment, EstimatorSnedecor &obj)
    {
        if (segment.end >= (int)obj.x.size())
            return false;

        int n = segment.end - segment.begin;

        double s = S(segment.params[1], obj.x, obj.y, segment.begin, segment.end);
        std::pair<double, double> paramNext = FindEaEb(obj.x, obj.y, segment.end - 1, segment.end + 1);

        double sum = 0;
        for (int i = segment.begin; i != segment.end; ++i)
        {
            sum += pow(obj.x[i] * (paramNext.second - segment.params[1]), 2);
        }

        sum /= n;
        double Ex = FirstMoment(obj.x, segment.begin, segment.end);

        double val1 = pow(paramNext.first - segment.params[0], 2)
            + 2 * Ex*(paramNext.first - segment.params[0])*(paramNext.second - segment.params[1])
            + sum;

        double val2 = 2 * s * GetSnedecorValue(n - 2, obj) / (n * (n - 2));

        if (val1 < val2)
        {
            ++segment.end;

            paramNext = FindEaEb(obj.x, obj.y, segment.begin, segment.end);

            double newVariance = sqrt(EvaluateL2Distance(obj.x, obj.y, segment.begin, segment.end, paramNext.second, paramNext.first, &LinearRegr));


            segment.variance = newVariance;
            segment.params[0] = paramNext.first;
            segment.params[1] = paramNext.second;
            return true;
        }

        return false;
    }


    /*
		@breif  Initializes parameters of algorithm
		@param  param -- data structure with some information for algorithm
    */
    void EstimatorSnedecor::initParameters(parameters_t* param)
    {
        distr_filename = param->input_file_distr;
        result_filename = param->output_result;
    }


    /*
        @breif  Main function which is looking for points of significant changings in coefficients of linear progression (x,y)
        @param  param -- data structure with input arguments for algorithm
    */
    void EstimatorSnedecor::estimate(parameters_t* param)
    {

        std::vector< double > x2, y2, lambda2;
        std::vector< int >    xKink, yKink;

        initParameters(param);
        std::ifstream fIn(distr_filename.c_str());
        s_Data.clear();
        ReadSnedecorCoeff(fIn, *this);

        s_segments.push_back(Segment(0, numStartPoints - 1));
        InitSegment(s_segments[0], x, y);

        size_t i, k, endI;

        for (i = numStartPoints - 1, k = 0, endI = x.size(); i != endI; ++i)
        {
            if (!AddPoint(s_segments[k], *this))
            {
                if (s_segments[k].end + numStartPoints >= (int)endI)
                    break;

                s_segments.push_back(Segment(s_segments[k].end, s_segments[k].end + numStartPoints));
                InitSegment(s_segments[s_segments.size() - 1], x, y);
                i += numStartPoints - 1;
                ++k;
            }
        }

        std::ofstream fResult(result_filename.c_str());
        for (size_t i = 0; i < s_segments.size(); ++i)
        {
            fResult << i << " segment has a = " << s_segments[i].params[1] << std::endl;
        }
        double lambda = EstimateLambda(s_segments, x, y);

        fResult << "1 / lamda (Process): " << lambda << std::endl;
    }
}


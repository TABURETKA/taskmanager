#include <iostream>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>


#include "EstimatorRegr.h"

///TODO !!! hard-coded ini-file name
EstimatorRegr::EstimatorRegr() : configFile("estConfig.ini")
{
};

void EstimatorRegr::clear()
{
    dev.clear();
    regressionCoef.clear();
    jump_time.clear();
    jump.clear();
	def_loading.clear();
	abs_def_loading.clear();
    track.clear();
	logger = 0;
}

bool EstimatorRegr::estimateInterval(
    vector<double> track, PF_Model* model, PF_Params* start, PF_Params* end, Logger* log )
{
    clear();
    this->track = track;
    this->logger = log;

    estimateBoundaries();
    fillParamsBoundaries(start, end);

    return model->processModel->estimateAndFillParamBoundaries(
        jump,
        threshold,
        0, // doesn't used in estimmateAndFill...() function. What does it parameter for?
        start->processParams, end->processParams);
}

bool EstimatorRegr::checkNeedFix( vector<double> track, Logger* log )
{
    return false;
}

void EstimatorRegr::estimateBoundaries()
{
    evalFirstDifferences();
	getPointsOfChange();
	getDeviation();
	startPoint();
	intensity();
	regressionCoefficient();
}


void EstimatorRegr::getPointsOfChange()
{


    parameters_t *my_param = new parameters();
    try
    {
        ParseConfigFile(configFile, my_param); // Parse configuration file
    }
    catch (error_inf&  err)
    {
        ErrorInformation(err, logger); // In case of error show information about it
        return ;
    }
    ProcessInfo *info = new ProcessInfo();
    regression::Estimator *problem;
	problem = new regression::Estimator();
    info->y = def_loading;
    for(size_t i = 0; i < def_loading.size(); ++i)
    {
        info->x.push_back(i);
    }
    problem->degPolynom = 5;
    problem->numStartPoints = problem->degPolynom + 1;
    problem->estimate(my_param, info);
    jump_time = info->start;
    regressionCoef = info->lambda;
    for(size_t i = 0; i < jump_time.size(); ++i)
    {
        double t = ((i == 0) ? (def_loading[0]) : (def_loading[jump_time[i-1]]));
        jump.push_back(t);
	}

	delete my_param;
	delete problem;
	delete info;

	unsigned num_app = jump_time.size();

	stringstream appLog;
	appLog << "Estimated applications time:\n";
	for (unsigned i = 0; i < num_app; ++i)
    {
		appLog << jump_time[i] << " " << jump[i] << std::endl;
    }
	logger->log(ALGORITHM_1_LEVEL_INFO, appLog.str());
}

void EstimatorRegr::getDeviation()
{
	sort(abs_def_loading.begin(), abs_def_loading.end());
	double th = quantilThreshold(abs_def_loading);
	threshold = th;

	stringstream logStr;
	logStr << "Threshold: " << th;
	logger->log(ALGORITHM_1_LEVEL_INFO, logStr.str());

	double sum_dev_min = 0.;
	int num_dev_min = 0;
	double sum_dev_max = 0.;
	int num_dev_max = 0;

	for (unsigned i = 0; i < def_loading.size(); ++i) {

		if (abs_def_loading[i] < th) {
			sum_dev_max += abs_def_loading[i] * abs_def_loading[i];
			num_dev_max++;
		}

		if (def_loading[i] > 0 && def_loading[i] < th) {
			sum_dev_min += def_loading[i] * def_loading[i];
			num_dev_min++;
		}
	}
	devMin = sqrt(sum_dev_min / (double)num_dev_min);
	devMax = sqrt(sum_dev_max / (double)num_dev_max);
}

void EstimatorRegr::startPoint()
{
	startMin = track[0] - devMax;
	startMax = track[0];

	if (jump_time.size() > 0)
	{
	    unsigned time = jump_time[0];
		double point = track[time - 1];

		if (startMin > point - devMax * sqrt((double)time))
			startMin = point - devMax * sqrt((double)time);
		if (startMax < point)
			startMax = point;
	}
	// startPoint is similar or smaller track[0]
	if (startMax > track[0]) {
		startMax = track[0];
	}
}


void EstimatorRegr::regressionCoefficient()
{

    double max = regressionCoef[0];
    double min = regressionCoef[0];
    for(size_t i = 1; i < regressionCoef.size(); ++i)
    {
        if(regressionCoef[i] > max)
        {
            max = regressionCoef[i];
        }
        if(regressionCoef[i] < min)
        {
            min = regressionCoef[i];
        }
    }
    regrMax = max;
    regrMin = min;
}

// Find the intense of the application commings
void EstimatorRegr::intensity()
{
	if (jump_time.size() == 0) {
		intensMin = 0.5 / (double)track.size();
		intensMax = 2. / (double)track.size();
		return;
	}

	if (jump_time.size() == 1) {
			intensMin = 0.5 / (double)track.size();
			intensMax = 2. / (double)jump_time[0];
			return;
	}

	double time_avg = (double)(jump_time[jump_time.size() - 1] - jump_time[0]) / (double)(jump_time.size() - 1);
	double time_avg_dev = time_avg / sqrt((double)jump_time.size());
    intensMin = max(0.5 / time_avg, 1. / (time_avg + time_avg_dev));
    intensMax = 1. / (time_avg - time_avg_dev);
}

void EstimatorRegr::evalFirstDifferences()
{
    def_loading.resize(track.size() - 1);
    abs_def_loading.resize(track.size() - 1);

    for (unsigned i = 0; i < track.size() - 1; i++) {
        def_loading[i] = track[i + 1] - track[i];
        abs_def_loading[i] = fabs(def_loading[i]);
    }

}
void EstimatorRegr::fillParamsBoundaries(PF_Params* start, PF_Params* end)
{
	start->noiseParams->setStartPoint(startMin);
	start->noiseParams->setDeviation(devMin);
	start->processParams->setIntesity(intensMin);
	start->processParams->setRegression(regrMin);

	end->noiseParams->setStartPoint(startMax);
	end->noiseParams->setDeviation(devMax);
	end->processParams->setIntesity(intensMax);
	end->processParams->setRegression(regrMax);
}

double EstimatorRegr::quantilThreshold(vector<double> samples)
{
	int n = samples.size();
	double mediana; // Quantile 0.674 of N(0,1) is 0.75, which is mediana of N+(0, 1)
	if (n % 2 == 0) {
        mediana = samples[n / 2];
	}
	else {
        mediana = (samples[n / 2] + samples[n / 2 +1]) / 2.;
	}
	return mediana * 5.;
}

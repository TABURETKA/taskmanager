#include "ChowTest.h"

/*
    @brief  Evaluates distance between two vector of data
    @param  x, y -- arrays with fisher distribution data
    @param  s -- segment with information about approximation line
    @param  ind_segm -- index of segment we work with
    @param  processInfo -- structure with information about process
*/
double getDistance(vector<double> x, vector<double> y, Segment s, ProcessInfo* processInfo, int ind_segm)
{
    double ESS = 0;
    std::vector<double> par = s.params;
    for (int i = s.begin; i < s.end; ++i)
    {
        double xTmp = x[i - s.begin];
        double buf = 0;
        for(int j = 0; j < ind_segm; ++j)
        {
            buf += processInfo->getVal(x[i], j);
        }
        ESS += pow((processInfo->getVal(xTmp, ind_segm) - y[i] + buf), 2);
    }
    return ESS;
}

/*
    @breif  Fills data structure m with Fisher distribution data
    @param  inputStream -- stream from opened file
    @param  m -- map<int, double> with distribution elements (pairs)
*/
bool readFisherDist(ifstream &inputStream, map<int, double>& m)
{
    int numPairs;

    if (!(inputStream >> numPairs))
        return false;

    for (int i = 0; i != numPairs; ++i)
    {
        int    numDegreesOfFreedom;
        double value;

        if (!(inputStream >> numDegreesOfFreedom))
            return false;

        if (!(inputStream >> value))
            return false;

        m[numDegreesOfFreedom] = value;
    }
    return true;
}

void CopyProcessInfoSegm(ProcessInfo* buffer_to, ProcessInfo* buffer_from, int ind_to, int ind_from)
{
    buffer_to->coef0[ind_to] = buffer_from->coef0[ind_from];
    buffer_to->lambda[ind_to] = buffer_from->lambda[ind_from];
    buffer_to->polynomDegree[ind_to] = buffer_from->polynomDegree[ind_from];
    buffer_to->start[ind_to] = buffer_from->start[ind_from];
}


/*
    @breif  Evaluates Chow STATISTIC
    @param  distrFile -- name of file with Fisher distribution data
    @param segments -- vector of segments before merging
    @param  x, y -- arrays with input distribution data
    @param  degPolynom -- degree of aproximation polynom
    @param  processInfo -- structure with information about process
*/
void Chow_test(string distrFile, vector<Segment> &segments, vector<double> x, vector<double> y, int degPolynom, ProcessInfo * processInfo)
{
    ifstream fIn(distrFile.c_str());
    map<int, double> t;
    vector<Segment> newSegments;
    vector<double> new_y = y;
    ProcessInfo *buffer_info = new ProcessInfo();
    buffer_info->resize();
    readFisherDist(fIn, t);
    size_t i = 0;
    bool stateChanged = false;
    double distFull, distFirst, distSecond;
    do
    {
        i = 0;
        stateChanged = false;
        while (segments.size() > i + 1)
        {
            Segment s(segments[i].begin, segments[i + 1].end);
            ProcessInfo *tmpPrcInf = new ProcessInfo();
            tmpPrcInf->resize();
            InitSegment(s, x, new_y, degPolynom, tmpPrcInf);
            double f = tmpPrcInf->lambda[0];
            double sdf = tmpPrcInf->coef0[0];
            distFirst = getDistance(x, new_y, segments[i], processInfo, i);
            distSecond = getDistance(x, new_y, segments[i + 1], processInfo, i + 1);
            CopyProcessInfoSegm(buffer_info, processInfo, 0, i);
            CopyProcessInfoSegm(processInfo, tmpPrcInf, i, 0);
            distFull = getDistance(x, new_y, s, processInfo, i);
            CopyProcessInfoSegm(processInfo, buffer_info, i, 0);
            double F = ((distFull - distFirst - distSecond) / (degPolynom + 1)) / ((distFirst + distSecond) / (s.end - s.begin - 2 * (degPolynom + 1)));
            int degree = (s.end - s.begin) - 2 * (degPolynom + 1);
            double a = t[degree];
            if (F < t[degree])
            {
                segments.erase(segments.begin() + i);
                segments[i] = s;
//                int sdllfjddsf = s.begin;
//                int lsdjfsddlfj = s.end;
                processInfo->coef0.erase(processInfo->coef0.begin() + i);
                processInfo->lambda.erase(processInfo->lambda.begin() + i);
                processInfo->polynomDegree.erase(processInfo->polynomDegree.begin() + i);
                processInfo->start.erase(processInfo->start.begin() + i);

                CopyProcessInfoSegm(processInfo, tmpPrcInf, i, 0);
                stateChanged = true;
                delete tmpPrcInf;
                break;
            }
            else
            {
                new_y = UpdateY(x, new_y, segments[i + 1].begin, i, processInfo);
                i += 1;
            }
            delete tmpPrcInf;
        }
        new_y = y;
    } while (stateChanged && segments.size() > 1);
    delete buffer_info;
}

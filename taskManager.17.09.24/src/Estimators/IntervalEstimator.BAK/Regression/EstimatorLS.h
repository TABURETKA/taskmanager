#ifndef ESTIMATOR_BASE_H__
#define ESTIMATOR_BASE_H__

#include <utility>
#include <vector>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <iterator>
#include <map>
#include "ConfigParse.h"
#define MIN_NUMBER_OF_POINTS_IN_SEGMENT 6 // minimum amount of points in one segment
#include "../../../Model/Model.h"
using std::vector;
using std::ofstream;

typedef double(*RegressionVal) (const double x, const std::vector<double> &param, const int deg);


struct Segment
{
public:
    Segment(int begin_, int end_)
        : begin(begin_)
        , end(end_)
        , variance(0)
    {}

public:
    int    begin, end;
    std::vector<double> params;
    double variance;
};
namespace regression
{


    class EstimatorLS
    {
    public:
        std::vector< Segment > s_segments;
        int numStartPoints;
/** ISO C++ requires const int degPolynom to be static */
        int degPolynom;
        EstimatorLS() : numStartPoints(MIN_NUMBER_OF_POINTS_IN_SEGMENT), degPolynom(5) {};
        ~EstimatorLS() {};

    };
}
vector<double> UpdateY(const vector<double> &x, const vector<double> &y, const int start, int ind_segm, ProcessInfo *processInfo);
double countLambda(const std::vector<double> &x, const std::vector<double> &y, const std::vector<double> &coef, const int start, const int end, const int deg);
double S(double a, std::vector< double > const &x, std::vector< double > const &y, const size_t begin, const size_t end);
std::vector<double> FindParams(std::vector< double > const &x, std::vector< double > const &y, const size_t begin, const size_t end, const int degPolynom);
double FirstMoment(std::vector< double > const &x, const size_t begin, const size_t end);
double Sum(std::vector< double > const &x, const size_t begin, const size_t end);
double EvaluateL2Distance(std::vector< double > const &x, std::vector< double > const &y, const size_t begin, const size_t end, const std::vector<double> &v,  const RegressionVal &f, const int deg);
double EvaluateL2DistanceExp(std::vector< double > const &x, std::vector< double > const &y, const size_t begin, const size_t end, const double coefExp, const double coef0);
void InitSegment(Segment &segment, const std::vector< double > &x, const std::vector< double > &y, const int deg, ProcessInfo *processInfo);
double EstimateLambda(const std::vector< Segment > &segments, const std::vector< double > &x, const std::vector< double > &y);
double LinearRegr(const double x, const std::vector<double>& param, const int deg);
void SaveResults(ofstream &resultStream, vector<Segment> s, vector<double> x, vector<double> y);
#endif

#ifndef ESTIMATOR_STUDENT_H__
#define ESTIMATOR_STUDENT_H__

#include "estimator_base.h"
#include "chow_test.h"

using std::vector;

namespace regression
{
    class EstimatorStudent : public EstimatorBase
    {
    private:
        void initParameters(parameters_t* param);

        friend double GetSnedecorValue(int numOfDegreesOfFreedom, EstimatorStudent &);
        friend bool ReadSnedecorCoeff(std::ifstream &inputStream, EstimatorStudent &);
        friend bool AddPoint(Segment &segment, EstimatorStudent &);

    public:
        void estimate(parameters_t* my_param);
        EstimatorStudent() {};
        ~EstimatorStudent() {};
    };

}
#endif

#ifndef CHOWTEST_H__
#define CHOWTEST_H__

#include "EstimatorLS.h"
#include <vector>
#include <map>
#include <iostream>
#include <string>

using std::vector;
using std::map;
using std::ifstream;
using std::string;

void Chow_test(string distrFile, vector<Segment>& segment, vector<double> x, vector<double> y, int degPolynom, ProcessInfo * processInfo);

#endif

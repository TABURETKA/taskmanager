#include "ConfigParse.h"
#include "Filestuff.h"

typedef void(*InterfaceFunc_t) (const std::vector<std::string> &arguments, parameters_t* params);

std::map<std::string, InterfaceFunc_t> tableParametrs;


/*
    @brief  Sets all elements of string to upper case
    @param  str -- input string
    @return string with all elements in upper case
*/
std::string UpperCase(std::string str)
{
    int i = 0;
    while (str[i])
    {
        if (str[i] > 'a' && str[i] < 'z')
        {
            str[i] = toupper(str[i]);
        }
        ++i;
    }
    return std::string(str);
}


/*
@brief  Updates special data structure with a name of output file with results of algorithm
@param  arguments -- vector with arguments for command
@param  params -- special data structure with parameters for algorithm
*/
void Update_chow_test_flag(const std::vector <std::string> &arguments, parameters_t* params)
{
    if (arguments.size() != 1) // Wrong amounts of arguments
    {
        error_inf e;
        e.err = ERR_WR_NUM_ARG;
        e.module = "Configuration file";
        e.procedure = "Chow test flag";
        throw(e);
        return;
    }
    if (arguments[0] == "1")
    {
        params->chow_test_flag = true;
    }
}


/*
@brief  Updates special data structure with a name of output file with results of algorithm
@param  arguments -- vector with arguments for command
@param  params -- special data structure with parameters for algorithm
*/
void Update_chow_test_distr(const std::vector <std::string> &arguments, parameters_t* params)
{
    if (arguments.size() > 1) // Wrong amounts of arguments
    {
        error_inf e;
        e.err = ERR_WR_NUM_ARG;
        e.module = "Configuration file";
        e.procedure = "Input file with distribution for Chow test";
        throw(e);
        return;
    }
    if (arguments.size() == 0)
    {
        return;
    }
    else // arguments.size() == 1
    {
        if (!isFileExist(arguments[0].c_str()))
        {
            error_inf e;
            e.err = ERR_FILE_EX;
            e.module = "Configuration file";
            e.procedure = "Input file with distribution for Chow test";
            throw(e);
            return;
        }
        else
        {
            params->f_distr_file = arguments[0];
        }
    }
}



/*
    @brief Checks if structure was set correct
    @param my_param -- data structure with parameters for method from configuration file
*/
void CheckParameters(parameters_t* my_param)
{
    if (my_param->chow_test_flag == true && my_param->f_distr_file == "")
    {
        error_inf e;
        e.err = ERR_FILE_EX;
        e.module = "Configuration file";
        e.procedure = "Input file with distribution for Chow test";
        throw(e);
        return;
    }
}


/*
  @brief  Initializes map with pairs (function, string)
*/
void InterfaceInit1()
{
    tableParametrs["CHOW_TEST"] = &Update_chow_test_flag;
    tableParametrs["CHOW_DISTR"] = &Update_chow_test_distr;
}


/*
    @brief  Parses configuration file
    @param  numArg -- amount of words in string
    @param  args -- string with arguments
    @param  my_param -- special data structure with information for the algorithm
*/
int ParseConfigFile(std::string fileName, parameters_t *my_param)
{

    std::map<std::string, std::vector<std::string> > commands;
    InterfaceInit1();
    if (!isFileExist(fileName.c_str()))
    {
        error_inf e;
        e.err = ERR_FILE_EX;
        e.module = "Configuration file";
        e.procedure = "Configuration file existence";
        throw e;
        return -1;
    }
    std::ifstream inputStream(fileName.c_str(), ios_base::in);
    std::string s;
    std::string delimiter = "=";
    while (std::getline(inputStream, s))
    {
        if (s.size() == 0)
        {
            break;
        }

        std::string com = UpperCase(s.substr(0, s.find(delimiter)));
        std::vector<std::string> listArg;
        string s1 = s.substr(s.find(delimiter) + 1, s.size());
        listArg.push_back(s1);
        if (commands.count(com) == 0)
        {
            commands[com] = listArg;
        }
        else
        {
            return -1;
        }
    }
    inputStream.close();
    for (std::map<std::string, std::vector<std::string> >::iterator it = commands.begin(); it != commands.end(); ++it)
    {
        std::string curCommand = (*it).first;
        if (tableParametrs.find(curCommand) == tableParametrs.end())
        {
            error_inf e;
            e.err = ERR_UNKNW_PRM;
            e.module = "Configuration file";
            e.procedure = "Flags";
            throw(e);
            return -1;
        }
        else
        {
            (tableParametrs[curCommand])((*it).second, my_param);
        }
    }
    CheckParameters(my_param);
    return 0;
}

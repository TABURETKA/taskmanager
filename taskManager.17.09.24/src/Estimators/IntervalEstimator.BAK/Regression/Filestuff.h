#ifndef _FILESTUFF_H_
#define _FILESTUFF_H_

#include <fstream> ///ifstream
#include <vector> ///vector

bool isFileExist(const char* filename);

#ifdef _WIN32
bool IsFileExists(const char * filename);
#endif
bool readInputData(std::ifstream &inputStream, std::vector< double > &x, std::vector< double > &y);
//static std::string TrimStr(const std::string& src, const std::string& c);
#endif /// _FILESTUFF_H_

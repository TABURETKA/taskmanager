#include "EstimatorMLE.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

bool EstimatorMLE::estimateInterval(vector<double> track, PF_Model* model, PF_Params* start, PF_Params* end, Logger* log )
{
    clear();

    this->track = track;
    this->logger = log;

    estimateBoundaries();
    fillParamsBoundaries(start, end);

    return model->processModel->estimateAndFillParamBoundaries(
        jump,
        threshold,
        regrMax,
        start->processParams, end->processParams);
}

bool EstimatorMLE::checkNeedFix( vector<double> track, Logger* log )
{
    clear();

    this->track = track;
    this->logger = log;

	EvalFirstDifferences();
	DeviationAndSpikes();

    if (jump.size() < 2) {
        logger->log(ALGORITHM_ESTIMATOR_NOTIFICATION, "Fix by jump size");
        return true;
    }

    StartPoint_old();
    double intervalOld = fabs(startMax - startMin);
    StartPoint();
    double intervalNew = fabs(startMax - startMin);

    if (intervalNew * 2. < intervalOld) {
        logger->log(ALGORITHM_ESTIMATOR_NOTIFICATION, "Fix by mu interval");
        return true;
    }

    logger->log(ALGORITHM_ESTIMATOR_NOTIFICATION, "No fix need for mu");
    return false;
}

void EstimatorMLE::clear()
{
    track.clear();

	def_loading.clear();
	abs_def_loading.clear();
	jump.clear();
	jump_time.clear();
	dev.clear();
	regressionCoef.clear();

	logger = 0;
}

void EstimatorMLE::estimateBoundaries()
{
	EvalFirstDifferences();
	DeviationAndSpikes();

	StartPoint();
	Intensity();
	Regression();
}

void EstimatorMLE::fillParamsBoundaries(PF_Params* start, PF_Params* end)
{
	start->noiseParams->setStartPoint(startMin);
	start->noiseParams->setDeviation(devMin);
	start->processParams->setIntesity(intensMin);
	start->processParams->setRegression(regrMin);

	end->noiseParams->setStartPoint(startMax);
	end->noiseParams->setDeviation(devMax);
	end->processParams->setIntesity(intensMax);
	end->processParams->setRegression(regrMax);
}

// Finite differencial fuction
void EstimatorMLE::EvalFirstDifferences()
{
    def_loading.resize(track.size() - 1);
    abs_def_loading.resize(track.size() - 1);

    for (unsigned i = 0; i < track.size() - 1; i++) {
        def_loading[i] = track[i + 1] - track[i];
        abs_def_loading[i] = fabs(def_loading[i]);
    }

}

// Identify the time of process jump
void EstimatorMLE::DeviationAndSpikes()
{
	sort(abs_def_loading.begin(), abs_def_loading.end());
	double th = QuantilThreshold(abs_def_loading);
	threshold = th;

	stringstream logStr;
	logStr << "Threshold: " << th;
	logger->log(ALGORITHM_1_LEVEL_INFO, logStr.str());

	double sum_dev_min = 0.;
	int num_dev_min = 0;
	double sum_dev_max = 0.;
	int num_dev_max = 0;

	for (unsigned i = 0; i < def_loading.size(); ++i) {
		if (def_loading[i] > th) {
			jump_time.push_back(i + 1);
			jump.push_back(def_loading[i]);
		}

		if (abs_def_loading[i] < th) {
			sum_dev_max += abs_def_loading[i] * abs_def_loading[i];
			num_dev_max++;
		}

		if (def_loading[i] > 0 && def_loading[i] < th) {
			sum_dev_min += def_loading[i] * def_loading[i];
			num_dev_min++;
		}
	}
	devMin = sqrt(sum_dev_min / (double)num_dev_min);
	devMax = sqrt(sum_dev_max / (double)num_dev_max);

	unsigned num_app = jump_time.size();

	stringstream appLog;
	appLog << "Estimated applications time:\n";
	for (unsigned i = 0; i < num_app; ++i) {
		appLog << jump_time[i] << " " << jump[i] << std::endl;
  }
	logger->log(ALGORITHM_2_LEVEL_INFO, appLog.str());
}

void EstimatorMLE::StartPoint()
{
/** ����� devMax - ��� ������ ������ ��� ����������� �����
�.�. ������ �.�. ����� - ������ �������� - ������������ ����������� �����
     ������ �.�. ������ - ������ �������� (����� ��������� ���� �� ����������� ����������� �����???)
 */
	startMin = track[0] - devMax;
	startMax = track[0];

	if (jump_time.size() > 0)
	{
	    unsigned time = jump_time[0];
		double point = track[time - 1];
/** ����� �������� �����, �������������� ������ ������� ������
� �������������� �� ��� ���������� ������ ����������� ���������
*/
		if (startMin > point - devMax * sqrt((double)time))
			startMin = point - devMax * sqrt((double)time);
/** ���� �������� �����, �������������� ������ ������� ������,
 ������ �������� ����� ������ �����, �� ��� ���������� � �������� ������ �.�. ������
*/

		if (startMax < point)
			startMax = point;
	}
	// startPoint is similar or smaller track[0]
/** ���� �������� �����, �������������� ������� ������, ����������� �������� � ������ �����,
����� ����� �������� ������ �.�. �� �������� � ������ �����
*/
	if (startMax > track[0]) {
		startMax = track[0];
	}
}

void EstimatorMLE::StartPoint_old()
{
	startMin = track[0] - devMax;
	startMax = track[0];

	for (unsigned i = 0; i < jump_time.size(); i++)
	{
		double point = track[jump_time[i] - 1];
/** ��������� �� ��������� ���� �����, �������������� �������
� �������������� �� ��� ���������� ������ ����������� ���������
�.�. ������� �.�. ����� ����� ������� �� ���� �������,
� ������� �.�. ������ �����  �������� �� ���� �������,
*/
		if (startMin > point)
			startMin = point;
		if (startMax < point)
			startMax = point;
	}
/** ���� �������� �� ������� �����, �������������� �������,
 ����������� �������� � ������ ����� + ������ ������ ��� ����������� �����,
 ����� ����� �������� ������ �.�. ������ �� �������� � ������ ����� + ������ ������ ��� ����������� �����,
*/
	// startPoint is similar or smaller track[0]
	if (startMax > track[0] + devMax) {
		startMax = track[0] + devMax;
	}
}

// Find the intense of the application commings
void EstimatorMLE::Intensity()
{
	if (jump_time.size() == 0) {
		intensMin = 0.5 / (double)track.size();
		intensMax = 2. / (double)track.size();
		return;
	}

	if (jump_time.size() == 1) {
			intensMin = 0.5 / (double)track.size();
			intensMax = 2. / (double)jump_time[0];
			return;
	}

	double time_avg = (double)(jump_time[jump_time.size() - 1] - jump_time[0]) / (double)(jump_time.size() - 1);
	double time_avg_dev = time_avg / sqrt((double)jump_time.size());
    intensMin = max(0.5 / time_avg, 1. / (time_avg + time_avg_dev));
    intensMax = 1. / (time_avg - time_avg_dev);
}

// Find the interval for intense of serving the application
void EstimatorMLE::Regression()
{
    logger->log(ALGORITHM_3_LEVEL_INFO, "Find regression coefficient");
	LS_ExpRegression();
	RegressionBoundaries();
}

void EstimatorMLE::RegressionBoundaries()
{
	if (regressionCoef.size() == 0) {
		regrMin = 0.;
		regrMax = 1.;
		return;
	}
	if (regressionCoef.size() == 1) {
		regrMin = 0.1 * regressionCoef[0];
		regrMax = 10. * regressionCoef[0];
		return;
	}
	regrMin = regressionCoef[0];
	regrMax = regressionCoef[0];

	for (unsigned i = 1; i < regressionCoef.size(); i++) {
		if (regrMax < regressionCoef[i]) {
			regrMax = regressionCoef[i];
		}
		if (regrMin > regressionCoef[i]) {
			regrMin = regressionCoef[i];
		}
	}
}

// Find the intenses of serving the application by least squares method
void EstimatorMLE::LS_ExpRegression()
{
	jump_time.push_back(track.size());

	double maxCoef0 = abs_def_loading[abs_def_loading.size() - 1] + devMax;
	stringstream ss;
	ss << "Max coef 0: " << maxCoef0;
	logger->log(ALGORITHM_4_LEVEL_INFO, ss.str());

/** ����� �������� ��������� ������������ �������� ������ ������, ��� �������� ������ ������ ��������� */
    unsigned i_start, i_end;
	for (unsigned i = 0; i < jump_time.size() + 1; ++i)
	{
	    if (i == 0) {
            i_start = 0;
            i_end = jump_time[0];
	    }
        else if (i == jump_time.size()) {
            i_start = jump_time[i - 1];
            i_end = track.size();
        }
        else {
            i_start = jump_time[i - 1];
            i_end = jump_time[i];
        }
/** ����� ��������� ������ �������������� ������
 ��� ����������� ������������ �������� ������ ������, �.�. ������ ������ ���������,
 ��� ���������� (floor) ����� ���������� ��������
 */
		std::vector<double> coef = LeastQuatersExp(i_start, i_end, track[i_end - 1]);
/** ����������� ������ �������������� ������ �� ������� �� ������������!
��� ������ �� �������������� ����������!
*/
		// Lam_c
		if (coef[1] < 0 && coef[0] <= maxCoef0) {
            regressionCoef.push_back(-coef[1]);
		}
		else {
            logger->log(ALGORITHM_4_LEVEL_INFO, "bad coefficients\n");
		}
/** ����� ������ ��� ��������� ������ �������������� ������
 ��� ����������� ������������ �������� ������ ������, �.�. ������ ������ ���������,
 �� ���������� (floor) ��� �� ����� ���������� ��������!
  ���, ���������� (floor), ������ �����  �������� ���������� �������� � ������������� ������������ ��������� ������������ ��������!
*/
		coef = LeastQuatersExp(i_start, i_end, track[i_end - 1] - devMax );
		if (coef[1] < 0 && coef[0] <= maxCoef0) {
            regressionCoef.push_back(-coef[1]);
		}
		else {
            logger->log(ALGORITHM_4_LEVEL_INFO, "bad coefficients\n");
		}
	}

	jump_time.pop_back();
}


std::vector<double> EstimatorMLE::LeastSquares4PolynomialRegression(unsigned i_beg, unsigned i_end)
{
	std::vector<double> coef(PolynomialDim);
	int i, j, k, row, col;
	std::vector< std::vector<double> > matrix;
	std::vector<double> x_i;
	matrix.assign(PolynomialDim, std::vector<double>(PolynomialDim, 0.));

	int totalOfInputParams = i_end - i_beg;
	x_i.assign(totalOfInputParams, 1.);

	double sum = 0.;
	for (i = 0; i < totalOfInputParams; ++i)
		sum += x_i[i];

	row = 0;
	col = 0;

	/** matrix_{0,0}=dim */
	matrix[row][col] = sum;
	for (j = 1; j < PolynomialDim; ++j)
	{
		sum = 0;
		for (k = 0; k < totalOfInputParams; ++k)
		{
			x_i[k] = x_i[k] * (k + 1);
			sum += x_i[k];
		}
		row = 0;
		col = j;
		while ((row < PolynomialDim) && (col >= 0))
		{
			matrix[row][col] = sum;
			row++;
			col--;
		}
	}

	for (i = 1; i < PolynomialDim; ++i)
	{
		sum = 0;
		for (k = 0; k < totalOfInputParams; ++k)
		{
			x_i[k] = x_i[k] * (k + 1);
			sum += x_i[k];
		}
		col = PolynomialDim - 1;
		row = i;
		while ((row < PolynomialDim) && (col >= 0))
		{
			matrix[row][col] = sum;
			row++;
			col--;
		}
	}

	for (i = 0; i < PolynomialDim; ++i)
	{
		coef[i] = 0.;
		for (j = (int)i_beg; j < (int)i_end; ++j)
		{
			double multiplier = 1.;
			/** j^{i} */
			for (k = 0; k < i; k++)
				multiplier *= (j + 1);
			coef[i] += track[j] * multiplier;
		}
	}

	Gauss(matrix, coef);

	for (k = 0; k < PolynomialDim; ++k)
	{
		if (isnan_double(coef[k]))
		{
			for (i = 0; i < (int)PolynomialDim; i++)
			{
				for (j = 0; j < (int)PolynomialDim; j++)
				{
					matrix[i].clear();
				}
			}
			matrix.clear();
			coef.clear();
			x_i.clear();
			return coef;
		}
	}

	for (i = 0; i < PolynomialDim; i++)
	{
		for (j = 0; j < PolynomialDim; j++)
		{
			matrix[i].clear();
		}
	}
	matrix.clear();
	x_i.clear();

	return coef;
}

// Exponential least quaters method for a part of a process [i_beg:i_end]
std::vector<double> EstimatorMLE::LeastQuatersToProc(unsigned i_beg, unsigned i_end)
{
    std::vector<std::vector<double> > matrix(PolynomialDim + 1, std::vector<double>(PolynomialDim + 1, 0));
    std::vector<double> coef(PolynomialDim + 1, 0);
    std::vector<double> x_i(i_end - i_beg, 1.);
    double elem;

  int j;
  for (int i = 0; i < PolynomialDim + 1; ++i)
  {
    elem = 0;
    for (j = 0; j < (int)(i_end - i_beg); ++j)
      elem += x_i[j];
    for (j = i; j >= 0; --j)
      matrix[i - j][j] = elem;

    for (j = 0; j < (int)(i_end - i_beg); ++j)
    {
      coef[i] += track[i_beg + j] * x_i[j];
      x_i[j] *= (double)j;
    }
  }

  for (int i = 1; i < PolynomialDim + 1; ++i)
  {
    elem = 0;
    for (j = 0; j < (int)(i_end - i_beg); ++j)
      elem += x_i[j];
    for (j = 0; j < PolynomialDim + 1 - i; ++j)
      matrix[j + i][PolynomialDim - j] = elem;

    for (j = 0; j < (int)(i_end - i_beg); ++j)
    {
      x_i[j] *= (double)j;
    }
  }

  Gauss(matrix, coef);

  return coef;
}

// Least squares method for modified exp!
///TODO!!! ������� � ��������� ���������!
std::vector<double> EstimatorMLE::LeastQuatersExp(unsigned i_beg, unsigned i_end, double floor, TODO offsetInTrack)
{
	std::vector<std::vector<double> > matrix(MaxN + 1, std::vector<double>(MaxN + 1, 0));
	std::vector<double> coef(MaxN + 1, 0.);

	int size = i_end - i_beg;
	int dataSize = 0;
	std::vector<double> data(size, 0);
/**
��� ����, ����� ��������������� ������� ���������������� ����������,
����� ���������� �� �.���������� � ������ �������� �������� �� ���� ��������,
��� ����� �������, ����� ������� ����� ������, ���� ��������� �������� floor,
�� ������� "��������" ������ ��������, ������������� ����� ��������.
*/
    for (int i = 0; i < size; i++) {
        data[i] = track[i_beg + i] - floor;
        if (data[i] > 0) {
            dataSize++;
        }
    }

	if (dataSize < 3) {
        coef[1] = 1;
        return coef;
	}
/**
 ����� �������� ������ � �������������� ����������,
��������� ������ Y(t)=param[0] * exp(-param[0] * t) �������������� ���������������
� �������� � ����
ln(Y(t))= ln(param[0]) + (-param[1] * t)
����� ���� ����������� �����������
SUM_1^dataSize {ln(Y(t)) - ln(param[0]) + param[0] * t}^2
��� ����� ������ ������� �� ���� ���������� ���������

SUM_1^dataSize {ln(Y(t)} =  ln(param[0]) * dataSize - param[1] * SUM_1^dataSize {t}
SUM_1^dataSize {t * ln(Y(t)} = ln(param[0]) * SUM_1^dataSize {t} - param[1] * SUM_1^dataSize {t^2}
���� ����, ��� ������, ������������ ������� ���������������� ����������
���� �� ���� �������� (������� � ������� ������� �������� ���������),
��� ����� ��� ��� �������� �������������,
�� �� ����� ������ ������������ �������,
�.�. ����� �������� �� ��� �������� �� ����� �������� �����
"���������" �� floor ����� ��������������!
�.�. �������� �������������!
*/
    std::vector<double> x_i(dataSize, 1.);
    double elem;

    int j;
	for (int i = 0; i < MaxN + 1; ++i)
    {
        elem = 0;
        for (j = 0; j < dataSize; ++j)
          elem += x_i[j];
        for (j = i; j >= 0; --j)
          matrix[i - j][j] = elem;

        unsigned k = 0;
        for (j = 0; j < size; ++j)
        {
            if (data[j] > 0) {
                coef[i] += log(data[j]) * x_i[k];
                x_i[k] *= (double)j;
                k++;
            }
        }
    }

	for (int i = 1; i < MaxN + 1; ++i)
    {
        elem = 0;
        for (j = 0; j < dataSize; ++j)
          elem += x_i[j];
        for (j = 0; j < MaxN + 1 - i; ++j)
            matrix[j + i][MaxN - j] = elem;

        for (j = 0; j < dataSize; ++j)
        {
          x_i[j] *= (double)j;
        }
    }

    Gauss(matrix, coef);

    coef[0] = exp(coef[0]);

    stringstream ss3;
    ss3 << "Trend:  " << coef[0] << " * exp( " << coef[1] << " )";
    logger->log(ALGORITHM_4_LEVEL_INFO, ss3.str());

	return coef;
}

// Evaluate process by coefficients
double EstimatorMLE::EvalPolynom(std::vector<double> coef, unsigned x_i)
{
  double x = (double)x_i;
  double y = 0;

	for (int i = PolynomialDim; i >= 0; --i)
  {
    y *= x;
    y += coef[i];
  }
  return y;
}

// Evaluate process by coefficients
double EstimatorMLE::EvalPolynom(std::vector<double> coef, double x)
{
  double y = 0;

	for (int i = PolynomialDim; i >= 0; --i)
  {
    y *= x;
    y += coef[i];
  }
  return y;
}

// Evaluate differential of a process by coefficients
double EstimatorMLE::EvalDifPolynom(std::vector<double> coef, unsigned x_i)
{
  double x = (double)x_i;
  double y = 0;

	for (int i = PolynomialDim; i > 0; --i)
  {
    y *= x;
    y += coef[i] * (double)(PolynomialDim - i);
  }
  return y;
}

// Evaluate differential of a process by coefficients
double EstimatorMLE::EvalDifPolynom(std::vector<double> coef, double x)
{
  double y = 0;

	for (int i = PolynomialDim; i > 0; --i)
  {
    y *= x;
    y += coef[i] * (double)(PolynomialDim - i);
  }
  return y;
}

// Matrix equation resolving
void EstimatorMLE::Gauss(std::vector<std::vector<double> >& matrix, std::vector<double>& y)
{
	int i, j, k;

	// straight
	for (i = 0; i < MaxN; ++i) {
		for (j = i + 1; j < MaxN + 1; ++j) {
			for (k = i + 1; k < MaxN + 1; ++k) {
				matrix[j][k] -= matrix[i][k] * matrix[j][i] / matrix[i][i];
			}
			y[j] -= y[i] * matrix[j][i] / matrix[i][i];
			matrix[j][i] = 0;
		}
	}

	// backward
	for (i = MaxN; i > 0; --i) {
		for (j = i - 1; j >= 0; --j) {
			y[j] -= y[i] * matrix[j][i] / matrix[i][i];
			matrix[j][i] = 0;
		}
	}

	for (i = 0; i < MaxN + 1; ++i) {
		y[i] /= matrix[i][i];
		matrix[i][i] = 1.;
	}
}

bool EstimatorMLE::isnan_double(double x) { return (x != x); }

double EstimatorMLE::QuantilThreshold(vector<double> samples)
{
	int n = samples.size();
	double mediana; // Quantile 0.674 of N(0,1) is 0.75, which is mediana of N+(0, 1)

	if (n % 2 == 0) {
        mediana = samples[n / 2];
	}
	else {
        mediana = (samples[n / 2] + samples[n / 2 +1]) / 2.;
	}

	// Find quantile 0.999
	// Quantile 0.999 of N(0,1) is 3.090, which is quantile 0.998 of N+(0, 1)
	// 3.090 / 0.674 = 4.5845 ~ 5.;
	return mediana * 5.;
}


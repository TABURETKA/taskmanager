#pragma once

#include "../../EstimatorFactory.h"

///TODO!!! ��� "����" �� ������������ ����������� ��� ���� �������, ������� ������ ��� ��������� ����� � ���������� ������
class EstimatorMLE : public IntervalEstimator
{
public:
    bool estimateInterval(vector<double> track,
        PF_Model* model,
        PF_Params* start, PF_Params* end,
        Logger* log);


    bool checkNeedFix(vector<double> track, Logger* log);

private:
    void clear();

	void estimateBoundaries();
	void fillParamsBoundaries(PF_Params* start, PF_Params* end);

	void EvalFirstDifferences();
	void DeviationAndSpikes();

	void StartPoint();
	void Intensity();
	void Regression();

	void LS_ExpRegression();
	void RegressionBoundaries();

	void StartPoint_old();

	std::vector<double> LeastSquares4PolynomialRegression(unsigned i_beg, unsigned i_end);
	std::vector<double> LeastQuatersToProc(unsigned i_beg, unsigned i_end);
	std::vector<double> LeastQuatersExp(unsigned i_beg, unsigned i_end, double floor);
	void Gauss(std::vector<std::vector<double> >& matrix, std::vector<double>& y);
	double EvalPolynom(vector<double> coef, unsigned x_i);
	double EvalPolynom(vector<double> coef, double x);
	double EvalDifPolynom(vector<double> coef, unsigned x_i);
	double EvalDifPolynom(vector<double> coef, double x);
	bool isnan_double(double x);
	double QuantilThreshold(vector<double> samples);

	vector<double> track;

	vector<double> def_loading;
	vector<double> abs_def_loading;
	vector<double> jump;
	vector<double> dev;
	vector<double> regressionCoef;

	vector<unsigned> jump_time;
/** ISO C++ requires const int MaxN to be static */
	const static int MaxN = 1;
/** ISO C++ requires const int PolynomialDim to be static */
	const static int PolynomialDim = 5;
/** ISO C++ requires const int SpikeTailLengthMin to be static */
	const static int SpikeTailLengthMin = 10;

	double startMax, startMin;
	double devMax, devMin;
	double intensMax, intensMin;
	double regrMax, regrMin;
	double jumpMax, jumpMin;
	double threshold;

	Logger* logger;
};


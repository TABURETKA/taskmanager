//OLD #include <stdlib.h> //atof()
#include <math.h> //min, max
#include <algorithm> //sort()
#include <sstream> //stringstream
#include "../../Log/errors.h"
#include "../../Params/ParamsSet.h" // Params.h is included
#include "../../Models/Model.h" //IModelPareto
#include "../../DataCode/DataModel/DataMetaInfo.h"
#include "../PointEstimators/FirstDerivativePointEstimator.h" //FIRSTDERIVATIVE_POINTESTIMATOR_TRACKNAME_0
#include "WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates.h"
#include "../../Config/ConfigParser.h" //MapParams
#include "../../DataCode/Grammar/DataManagerGrammar.h" //DataManagerGrammar
#include "../../Task/Task.h" //Task
#include <assert.h>     /** assert */
//#define _DEBUG_

//OLD #define WIENER_INTERVAL_ESTIMATOR_PARAMS_SIZE 2

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace IntervalEstimators;

///static in WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates
const char* WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::grammar[FakeIndex_IndexesOfGrammarWienerInitCond]={
       "WienerSigmaEstimate",
       "WienerTrajectorySubsetIndexes"
};
IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates(Logger*& log) : FakeIntervalEstimator(log)
{
    /** MUST re-init _pet since in FakeIntervalEstimatyor this member
  has been initialized as FakeIntervalEstimator type
   */
  m_iet = IntervalEstimatorsTypes::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates;
  /** we MUST INIT logger in CTOR() of each Estimator! */
  /** m_task = NULL; m_logger  = log; already initialized in FakeIntervalEstimator's CTOR */
  m_model  = NULL;
  /** here we get estimates, which are produced by this estimator */
  m_setOfParamsEstimates = NULL;
  m_leftBound = NULL;
  m_rightBound = NULL;
  m_setOfEstimatorParams  = NULL;
}
/*********************************************************************
*                          DTOR()
**********************************************************************/
IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::~WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates()
{
  m_data.clear();
  delete m_setOfEstimatorParams;

  delete m_setOfParamsEstimates;
  /** NO WAY delete m_leftBound, m_rightBound!
  * They point to the same estimates
  * we just deleted from m_setOfParamsEstimates!

  delete m_leftBound;
  delete m_rightBound;
  */

}
    /** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */

/** generate DataInfo */
int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    /** Total of DataInfo to be created */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    size_t di_0_dimension = IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES;
    /** allocate room for _trackSizes (each track size == Wiener model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    /** init _trackSizes (each track size == Wiener model parameters dimension) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        di_0_dataSetSizes[i] = IParamsWiener::WienerCDFParamsDimension;
    }
/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            while(i){
              --i;
              delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
///REDUNDANT        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        for(size_t j=0; j < di_0_dataSetSizes[i]; ++j)
        {
            data[i][j]=NAN;
        }
        if(i == IntervalEstimateIndexes::LeftBoundary)
        {
            data[i][IParamsWiener::indexesOfGrammarForWienerParams::INITIAL_CONDITION] = m_leftBound->initialCondition();
        }
        if(i == IntervalEstimateIndexes::RightBoundary)
        {
            data[i][IParamsWiener::indexesOfGrammarForWienerParams::INITIAL_CONDITION] = m_rightBound->initialCondition();
        }
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = intervalEstimatorNames[IntervalEstimatorsTypes::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = WIENER_INTERVALESTIMATOR_DATASETNAME;
    di[0]->m_dataModelName = m_model->toString();

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }

    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::LeftBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::LeftBoundary];
    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::RightBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::RightBoundary];

    /** put _trackSizes (each track size == Wiener model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    //We have already put data into di[0]! See CTOR di[0]->dataSet = data;
    return _RC_SUCCESS_;
}

int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension < 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];

    if(di->m_dimension != 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong dimension of DI array");
        return _ERROR_WRONG_INPUT_DATA_;
    }
///TODO!!! beforehand put "values of first derivative" into TRACK_NAME and compare it right here with di->dataSetTrackNames[0]!
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL) || (di->m_dataSet[0] == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);
    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
#ifdef _DEBUG_
std::cout << m_data[i] << std::endl;
#endif
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}
/** IIntervalEstimator stuff */
/***********************************************************************************************************************
*
************************************************************************************************************************/
void IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::deleteWienerSigmaIntervalEstimates(size_t dim, IParams** & ptrToPtrIParams)
{
    size_t i=0;
    while(i<dim)
    {
        if(ptrToPtrIParams[i])
            delete ptrToPtrIParams[i];
        ++i;
    }
    delete [] ptrToPtrIParams;
}

///TODO!!! SO far size and ptrToPtrIParams are redundant !
int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::parseAndCreateEstimatorParams(std::map<std::string, std::string>& paramsMap, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* logger)
{
    IParams** wienerSigmaIntervalEstimates = new  IParams*[TOTAL_OF_INTERVAL_ESTIMATE_INDEXES];
    /** GRAMMAR! ����� �������� � ���� ��������� ��� ����� ������ (���� ������),
     ���������� ���� �� ��������� ���������� ������������ ������ (������ ��������, ������� � R^1 �.�. ������ ���)
    */
    for(size_t j = LeftBoundary; j < TOTAL_OF_INTERVAL_ESTIMATE_INDEXES; ++j)
    {
            wienerSigmaIntervalEstimates[j] = NULL;
            std::string postfix = IIntervalEstimator::grammarIntervalEstimates[j];
            std::stringstream ss;
            ss << WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::grammar[indexWienerSigmaEstimate] << "_" << postfix;
            std::string tmpStr     = ss.str();
std::cout << postfix << '\t' << WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::grammar[indexWienerSigmaEstimate] << '\t' << std::endl;

            std::vector<std::string> keys;
            iteratorParamsMap itParam = paramsMap.find(tmpStr);
            if (itParam == paramsMap.end())
            {
                deleteWienerSigmaIntervalEstimates(j, wienerSigmaIntervalEstimates);
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": NO FOUND REPO_INDEX_ITEM " + tmpStr + " for Wiener Sigma interval estimates to parse estimator's params.");
                return _ERROR_WRONG_WORKFLOW_;
            }
            std::string repoEntriesStr= itParam->second;
#ifdef _DEBUG_
            std::cout << repoEntriesStr << std::endl;
#endif
            logger->log(WORKFLOW_TASK_NOTIFICATION, "Get entries from repo with keys :  " + repoEntriesStr);
            /** �����, ���������� ��������� ��� ����, ����� ������� �������� ��������� ������
             ����-������� ������ � �� ���������!
             ��������� ������ ��� ��� � ���� ����������, ������ ��� ��������� � ���������� ���� ����� �������� ��� ������!
            */
            if(DataManagerGrammar::splitAllRepoKeysAsSingleString(repoEntriesStr,keys) != _RC_SUCCESS_)
            {
                deleteWienerSigmaIntervalEstimates(j, wienerSigmaIntervalEstimates);
                m_logger->error(WORKFLOW_TASK_ERROR, "No entries");
                return _ERROR_WRONG_ARGUMENT_;;
            }
            size_t dimension = keys.size();
            if(dimension != 1)
            {
                deleteWienerSigmaIntervalEstimates(j, wienerSigmaIntervalEstimates);
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong number of key into REPO_INDEX_ITEM " + tmpStr + " for Wiener Sigma interval estimates to parse estimator's params.");
                return _ERROR_WRONG_WORKFLOW_;
            }
            DataInfo** di = NULL;
            di = new DataInfo* [dimension];
            if(di == NULL)
            {
                deleteWienerSigmaIntervalEstimates(j, wienerSigmaIntervalEstimates);
                m_logger->error(WORKFLOW_TASK_ERROR, std::string(intervalEstimatorNames[m_iet]) + "No room to allocate DataInfo** for Estimator's/Model's params");
                return _ERROR_NO_ROOM_;
            }
            int rc = m_task->getRepoDataForTargetObj(keys, (size_t) dimension, (DataInfo** const) di);
            if(rc != _RC_SUCCESS_)
            {
                for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
                delete [] di;

                deleteWienerSigmaIntervalEstimates(j, wienerSigmaIntervalEstimates);
                return rc;
            }
///TODO!!! ���������, ��� ��������� ��� grammarIntervalEstimates[j]

/// � ��� ����� ��������� ������ ��� ��������������� IParams
///TODO!!! ������� ������� ���� di ������, ����� ��� ������� ���������� di � ���� IParams � ������ ��� ����������!
            wienerSigmaIntervalEstimates[j] = IParams::createEmptyParams(m_logger, IParams::WienerParams);
            if(wienerSigmaIntervalEstimates[j] == NULL)
            {
                for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
                delete [] di;

                deleteWienerSigmaIntervalEstimates(j, wienerSigmaIntervalEstimates);
                return _ERROR_NO_ROOM_;
            }
///            enum IModel::ModelsTypes mt = IModel::ModelsTypes::WienerModel;
///TODO???            int rc = IModel::parseDataInfoAndCreateModelParams(mt, dimension, (const DataInfo ** const) di, wienerSigmaIntervalEstimates[j], (Logger* const) m_logger);
            size_t index = IParamsWiener::indexesOfGrammarForWienerParams::SIGMA;
            double sigma = di[0]->m_dataSet[0][index];
            wienerSigmaIntervalEstimates[j]->setParam(index, sigma);
            for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
            delete [] di;

    }///end for WienerSigmaEstimates
    std::vector<std::string> keys;
    std::string tmpStr = WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::grammar[indexOfWienerTrajectorySubsetIndexes];
    iteratorParamsMap itParam = paramsMap.find(tmpStr);
    if (itParam == paramsMap.end())
    {
        deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": NO FOUND REPO_INDEX_ITEM " + tmpStr + " for Trajectory Subset points to parse estimator's params.");
        return _ERROR_WRONG_WORKFLOW_;
    }
    std::string repoEntriesStr= itParam->second;
#ifdef _DEBUG_
        std::cout << repoEntriesStr << std::endl;
#endif
    logger->log(WORKFLOW_TASK_NOTIFICATION, "Get entries from repo with keys :  " + repoEntriesStr);
    /** �����, ���������� ��������� ��� ����, ����� ������� �������� ��������� ������
     ����-������� ������ � �� ���������!
     ��������� ������ ��� ��� � ���� ����������, ������ ��� ��������� � ���������� ���� ����� �������� ��� ������!
    */
    if(DataManagerGrammar::splitAllRepoKeysAsSingleString(repoEntriesStr,keys) != _RC_SUCCESS_)
    {
        deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
        logger->error(WORKFLOW_TASK_ERROR, "No entries");
        return _ERROR_WRONG_ARGUMENT_;;
    }

    size_t dimension = keys.size();
    DataInfo** di = NULL;
    di = new DataInfo* [dimension];
    if(di == NULL)
    {
        deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
        m_logger->error(WORKFLOW_TASK_ERROR, std::string(intervalEstimatorNames[m_iet]) + "No room to allocate DataInfo** for Estimator's/Model's params");
        return _ERROR_NO_ROOM_;
    }
    int rc = m_task->getRepoDataForTargetObj(keys, (size_t) dimension, (DataInfo** const) di);
    if(rc != _RC_SUCCESS_)
    {
        for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
        delete [] di;

        deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
        return rc;
    }

    DataInfo* ptrDI = di[0];
    if(ptrDI->m_dimension != 1)
    {
        for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
        delete [] di;

        deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong dataset dimension for Wiener Trajectory Subset indexes");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if ((ptrDI->m_dataSetSizes[0] == 0) || (ptrDI->m_dataSet == NULL) || (ptrDI->m_dataSet[0] == NULL))
    {
        for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
        delete [] di;

        deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": NO indexes of Wiener Trajectory Subset");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    IParams * indexes = IParams::createEmptyParams(m_logger, IParams::Params1D, ptrDI->m_dataSetSizes[0]);
    if(!indexes)
    {
        for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
        delete [] di;

        deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
        return rc;
    }
    for(size_t i=0; i < ptrDI->m_dataSetSizes[0]; ++i)
    {
        indexes->setParam(i, ptrDI->m_dataSet[0][i]);
#ifdef _DEBUG_
std::cout << ptrDI->m_dataSet[0][i] << std::endl;
#endif
    }
    for(size_t index_di=0; index_di < dimension; ++index_di) delete di[index_di];
    delete [] di;

    rc = _RC_SUCCESS_;
    if(m_setOfEstimatorParams == NULL)
    {
        m_setOfEstimatorParams = IParamsSet::createEmptyParamsSet(NULL, m_logger);
        if(m_setOfEstimatorParams == NULL)
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can not allocate set of Estimator's Params");
            rc = _ERROR_NO_ROOM_;
        }
    }
    /** First parameter of addParamsCopyToSet() is NULL
    because we put into the same set the ptrIParams for DIFFERENT Models!
    */
///TODO!!! ����� ����� ����, ��� ����� ������� ����������� ����� � ���� ��������� ������� � �� �����������, �� ��� ������� ������ �����������
    for(size_t i=0; i < SUBSET_INDEXES; ++i)
    {
#ifdef _DEBUG_
std::cout << wienerSigmaIntervalEstimates[i]->toString() << std::endl;
#endif
        m_setOfEstimatorParams->addParamsCopyToSet(NULL, wienerSigmaIntervalEstimates[i], logger);
    }
    m_setOfEstimatorParams->addParamsCopyToSet(NULL, indexes, logger);

    deleteWienerSigmaIntervalEstimates(TOTAL_OF_INTERVAL_ESTIMATE_INDEXES, wienerSigmaIntervalEstimates);
    delete indexes;
///tODO!!! ���������, ��� ��������� ��� grammarIntervalEstimates[j]
    return rc;
}

int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return _ERROR_WRONG_WORKFLOW_;
}
int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::setEstimatorParams(IParams*& params)
{
///TODO!!!
    return _ERROR_WRONG_WORKFLOW_;
}

int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::setModel(IModel* model)
{
    if(! model)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel  param model is not specified");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IModelWiener * pm = dynamic_cast<IModelWiener *> (model);
    if(! pm)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel param model is not ptr to IModelWiener");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
#ifdef _DEBUG_
std::cout << m_model->toString() << std::endl;
#endif
    return rc;
}
/*********************************************************************************************************************
* We merely create params set with two empty WienerParams into it.
* The set will be filled with estimates later, in estimateInterval()
*********************************************************************************************************************/
int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::setParamsSet()
{
///TODO!!! WE NEED FACTORY to make ParamsSet!
///��� ���� �������� ������ ParamsSet,
///� ��� ��������� ��� IParams ���� �����,
///���� �� ���� ������ ����� ����������
/// ���������� � ������ ���������� ���� ������ ������ �� �����,
///��������� ���� ��������� ����������� (������ ��� ����� ������),
///�� ��� ����������� ����������� ����� ����� ���������� ��������� �� ������!
/// ������-�� ������ ��� ���������� ��� ������ ���� ���� ����������
/// �� ������ ���� �������. ������� ����� ��������������� ������ ������ (���������� )
    if (m_model == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Model must be set before any attempt to create ParamsSet of Wiener Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
    if (pst == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create ParamsSet of Wiener Model Params failed!");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** create empty WienerParams */
    for(size_t i=0; i < IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES; ++i)
    {
/// IParams * ptrToParams= IParams::createEmptyParams(WienerParams, 1); <- 1-�� �� ����� ������!
        IParams * ptrToParams= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::WienerParams);
        if (ptrToParams == NULL)
        {
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create Wiener Model Params failed!");
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** ptr to allocated params is stored in ParamsSet,
         anyway delete ptrToParams
        */
       if ( pst->addParamsCopyToSet((const IModel*) m_model, ptrToParams, m_logger) == false)
       {
            delete ptrToParams;
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to put Wiener Model Params into ParamsSet failed!");
            return _ERROR_WRONG_ARGUMENT_;
       }
       /** Once we put copy of ptrToParams to setOfEstimatorParams
       free ptrToParams
       */
       delete ptrToParams;
    }

    IParams * params = pst->getFirstPtr();
    IParamsWiener * pp_left = dynamic_cast<IParamsWiener*> (params);
    assert(pp_left != NULL);
    m_leftBound = pp_left;

    params = pst->getNextPtr();
    IParamsWiener * pp_right = dynamic_cast<IParamsWiener*> (params);
    assert(pp_right != NULL);
    m_rightBound = pp_right;
    /** For safety-sake in run-time (when assert won't help)! */
    if ((m_leftBound == NULL) || (m_rightBound == NULL))
    {
        delete pst;
///NO WAY        delete m_leftBound;
        m_leftBound = NULL;
///NO WAY        delete m_rightBound;
        m_rightBound = NULL;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can't create ParamsSet of Wiener Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    m_setOfParamsEstimates = pst;
    return _RC_SUCCESS_;
}

/******************************************************************
* this estimator IGNOREs its parameter threadInterruptSignal
* because its life-time very short, its interrupt does not make sense
******************************************************************/
int IntervalEstimators::WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::estimateInterval(ptrInt threadInterruptSignal)
{
    if (m_data.size() == 0) {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No data to estimate Wiener Params Interval");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    IParams * wienerSigmaIntervalEstimates = m_setOfEstimatorParams->getParamsCopy(SIGMA_MAX);
    if( dynamic_cast<IParamsWiener*>(wienerSigmaIntervalEstimates) == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No Wiener Sigma estimates in estimator Params' set");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    double sigmaMax = dynamic_cast<IParamsWiener*>(wienerSigmaIntervalEstimates)->sigma();
    double initConditionLeftBoundary = m_data[0] - sigmaMax;
    double initConditionRightBoundary = m_data[0];

    delete wienerSigmaIntervalEstimates;

    IParams * indexesOfTrajectorySubsetPoints =  m_setOfEstimatorParams->getParamsCopy(SUBSET_INDEXES);
#ifdef _DEBUG_
std::cout << "indexesOfTrajectorySubsetPoints:" << indexesOfTrajectorySubsetPoints->toString() << std::endl;
#endif
    if( indexesOfTrajectorySubsetPoints == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No indexes Of Trajectory Subset Points in estimator Params' set");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    size_t size= indexesOfTrajectorySubsetPoints->getSize();
#ifdef _DEBUG_
std::cout << "indexesOfTrajectorySubsetPoints->getSize():" << indexesOfTrajectorySubsetPoints->getSize() << std::endl;
#endif
    for(size_t i=0; i < size; ++i)
    {
        double index=0;
        indexesOfTrajectorySubsetPoints->getParam(i,index);
#ifdef _DEBUG_
if (( ((size_t)index < 0)&&((size_t)index > size-1)))
{
    std::cerr << "Assert �� WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates::estimateInterval:\t" << std::endl
    << "Expected non-negative index :\t" << index << std::endl;
    abort();
}
#endif // _DEBUG_
        /**
        If we'v got fake outliers (fake jump identified)
        at the very first point
        we skip it!
        */
        if (index < 0){
            continue;
        }
/** ��������� �� ��������� ���� �����, ��������������� �������������� �������
� �������������� �� ��� ���������� ������ ����������� ���������
�.�. ������� �.�. ����� ����� ������� �� ���� �������,
� ������� �.�. ������ �����  �������� �� ���� �������,
*/
        double ordinata = m_data[(size_t)index];

        if (initConditionLeftBoundary > ordinata)
            initConditionLeftBoundary = ordinata;
        if (initConditionRightBoundary < ordinata)
            initConditionRightBoundary = ordinata;
    }
    delete indexesOfTrajectorySubsetPoints;
    indexesOfTrajectorySubsetPoints = nullptr;
/** ���� �������� �� ������� �����, �������������� �������,
 ����������� �������� � ������ ����� + ������ ������ ��� ����������� �����,
 ����� ����� �������� ������ �.�. ������ �� �������� � ������ ����� + ������ ������ ��� ����������� �����,
*/
    // startPoint is similar or smaller track[0]
    if (initConditionRightBoundary > m_data[0] + sigmaMax) {
        initConditionRightBoundary = m_data[0] + sigmaMax;
    }
    m_leftBound->setInitialCondition(initConditionLeftBoundary);
    m_rightBound->setInitialCondition(initConditionRightBoundary);

#ifdef _DEBUG_
for(size_t i = 0; i < m_data.size(); ++i)
{
    std::cout << m_data[i] << std::endl;
}
#endif
#ifdef _DEBUG_
std::cout << "right sigma" << m_rightBound->sigma() << std::endl;
std::cout << "left sigma" << m_leftBound->sigma() << std::endl;
#endif
    return _RC_SUCCESS_;
}

#ifndef __SAMPLEMEANINTERVALESTIMATOR_H__633569422542968750
#define __SAMPLEMEANINTERVALESTIMATOR_H__633569422542968750

#include <vector>
#include "../../Params/ParamsSet.h"
#include "../Estimator.h"
#include "FakeIntervalEstimator.h"
///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[ParetoParams] ??? + ���������� ����� ����������???
#define SAMPLE_MEAN_INTERVALESTIMATOR_DATASETNAME "SampleMeanBoundaries";
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */

namespace IntervalEstimators{
   /** class container */
/** implementation of abstract Base class IIntervalEstimator */
class SampleMeanIntervalEstimator : public virtual IIntervalEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakeIntervalEstimator
*/
                                public virtual FakeIntervalEstimator
{
public:
    enum IndexesOfDataLocationSemantics{
        indexOfDataInRow,
        indexOfDataInCol,
        confidenceRadiusAsTotalOfSDs,
        FakeIndex_DataLocationSemantics
    };
    static const char* grammarOfDataLocationSemantics[FakeIndex_DataLocationSemantics];

    enum IndexesOfSampleMeanIntervalEstimatorParams{
        EstimateVar =0,
        EstimateSD  =1,
        FakeIndex_SampleMeanIntervalEstimatorParams
    };

    SampleMeanIntervalEstimator(Logger*& log);
    ~SampleMeanIntervalEstimator();
    /** IEstimator stuff */
    using FakeIntervalEstimator::setTask;
    using FakeIntervalEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
//    using FakeIntervalEstimator::setEstimatorParams;
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakeIntervalEstimator::toString;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IIntervalEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimateInterval(ptrInt threadInterruptSignal);
protected:
    using FakeIntervalEstimator::logErrMsgAndReturn;
    using FakeIntervalEstimator::parsingFailed;

private:
   const static size_t m_estimatesDim = 1;
///� ����� ������ ������ �����������
///��� ��������������� �� �������� ����� ����������� ParamsSet ��� ParamsCompact!
///����� ��� ���� �� � �������, � ���������!
/// ��� ����� ������ ���������� ��� � ������� ��� ������� ��� ������������� ParamsSet ��� ParamsCompact
/// � ����� �� ��� ������ ���� ��������� ParamsSet ��� ParamsCompact
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    SampleMeanIntervalEstimator(const SampleMeanIntervalEstimator&);
    void operator=(const SampleMeanIntervalEstimator&);
/** enum PointEstimatorsTypes m_pet; has been Moved to FakePointEstimator */
/** Task* m_task; has been Moved to FakePintEstimator */
/** Logger* m_logger; has been Moved to FakePointEstimator */
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    /** ptr to first IParams in IParamsSet _setOfParamsEstimates */
    IParams* m_leftBound;
    /** ptr to second IParams in IParamsSet _setOfParamsEstimates */
    IParams* m_rightBound;

    IParams* m_estimatorParams;

///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;
};

} /// end namespace IntervalEstimators
#endif

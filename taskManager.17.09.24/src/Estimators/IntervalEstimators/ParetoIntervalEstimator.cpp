//OLD #include <stdlib.h> //atof()
#include <math.h> //min, max
#include <algorithm> //sort()
#include "../../Log/errors.h"
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../../Params/ParamsSet.h" // Params.h is included
#include "../../Models/Model.h" //IModelPareto
#include "../../DataCode/DataModel/DataMetaInfo.h"
#include "ParetoIntervalEstimator.h"
#include <assert.h>     /** assert */

//#define _DEBUG_

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace IntervalEstimators;

IntervalEstimators::ParetoIntervalEstimator::ParetoIntervalEstimator(Logger*& log) : FakeIntervalEstimator(log)
{
    /** MUST re-init _pet since in FakeIntervalEstimatyor this member
  has been initialized as FakeIntervalEstimator type
   */
  m_iet = IntervalEstimatorsTypes::ParetoIntervalEstimator;
  /** we MUST INIT logger in CTOR() of each Estimator! */
  /** _task = NULL; m_logger  = log; already initialized in FakeIntervalEstimator's CTOR */
  m_model  = NULL;
  /** here we get estimates, which are produced by this estimator */
  m_setOfParamsEstimates = NULL;
  m_leftBound = NULL;
  m_rightBound = NULL;
  m_estimatorParams  = NULL;

}
/*********************************************************************
*                          DTOR()
**********************************************************************/
IntervalEstimators::ParetoIntervalEstimator::~ParetoIntervalEstimator()
{
  m_data.clear();
  delete m_estimatorParams;
  delete m_setOfParamsEstimates;
  /** NO WAY delete m_leftBound, m_rightBound!
  * They point to the same estimates
  * we just deleted from m_setOfParamsEstimates!

  delete m_leftBound;
  delete m_rightBound;
  */

}
    /** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */

/** generate DataInfo */
int IntervalEstimators::ParetoIntervalEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    /** Total of DataInfo to be created */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di, 0, dimension*sizeof(DataInfo*));
    /** 2 == leftBoundary and rightBoundary! */
    size_t di_0_dimension = IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES;
    /** allocate room for _trackSizes (each track size == Pareto model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));

    /** init _trackSizes (each track size == Pareto model parameters dimension) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        di_0_dataSetSizes[i] = IParamsPareto::ParetoCDFParamsDimension;
    }
/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));

    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            while(i){
              --i;
              delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        if(i == IntervalEstimateIndexes::LeftBoundary)
        {
            data[i][0] = m_leftBound->xM();
            data[i][1] = m_leftBound->k();
        }
        if(i == IntervalEstimateIndexes::RightBoundary)
        {
            data[i][0] = m_rightBound->xM();
            data[i][1] = m_rightBound->k();
        }
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = intervalEstimatorNames[IntervalEstimatorsTypes::ParetoIntervalEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = PARETO_INTERVALESTIMATOR_DATASETNAME;
    di[0]->m_dataModelName = m_model->toString();

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }


    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::LeftBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::LeftBoundary];
    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::RightBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::RightBoundary];

    /** put _trackSizes (each track size == Pareto model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    //We have already put data into di[0]! See CTOR di[0]->dataSet = data;
    return _RC_SUCCESS_;
}

int IntervalEstimators::ParetoIntervalEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension < 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];

    if(di->m_dimension < 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
///TODO!!! beforehand put "values of outliers" into TRACK_NAME and compare it right here with di->dataSetTrackNames[0]!
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);

    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
#ifdef _DEBUG_
std::cout << m_data[i] << std::endl;
#endif
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}
/** IIntervalEstimator stuff */
///TODO!!! SO far size and ptrToPtrIParams are redundant !
int IntervalEstimators::ParetoIntervalEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
//OLD    iteratorParamsMap itParam = mapParams.find(grammarParetoParams[2]);
    /** we are pessimists */
    int rc = _ERROR_NO_ESTIMATOR_PARAMS_;
    iteratorParamsMap itParam = mapParams.find(IParamsPareto::grammar[IParamsPareto::indexesOfGrammarForParetoParams::K_MIN]);
    double kMIN=NAN;
    if (itParam != mapParams.end()) {
        rc = strTo<double>(itParam->second.c_str(), kMIN); //OLD kMIN = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Not found Or wrong value of " + IParamsPareto::grammar[IParamsPareto::indexesOfGrammarForParetoParams::K_MIN]);
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** we are pessimists */
    rc = _ERROR_NO_ESTIMATOR_PARAMS_;
    double kMAX=NAN;
//OLD 4TESTs        itParam = mapParams.find(grammarParetoParams[3] + postfix);
    itParam = mapParams.find(IParamsPareto::grammar[IParamsPareto::indexesOfGrammarForParetoParams::K_MAX]);
    if (itParam != mapParams.end()) {
        rc = strTo<double>(itParam->second.c_str(), kMAX); //OLD kMAX = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Not found OR wrong value of " + IParamsPareto::grammar[IParamsPareto::indexesOfGrammarForParetoParams::K_MAX]);
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** we are pessimists */
    rc = _ERROR_NO_ESTIMATOR_PARAMS_;
    double Xmin_thrshld=NAN;
//OLD 4TESTs            itParam = mapParams.find(grammarParetoParams[4] + postfix);
    itParam = mapParams.find(IParamsPareto::grammar[IParamsPareto::indexesOfGrammarForParetoParams::X_MIN_THRESHOLD]);
    if (itParam != mapParams.end()) {
        rc = strTo<double>(itParam->second.c_str(), Xmin_thrshld); //OLD  Xmin_thrshld = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Not found OR wrong value of " + IParamsPareto::grammar[IParamsPareto::indexesOfGrammarForParetoParams::X_MIN_THRESHOLD]);
        return _ERROR_WRONG_ARGUMENT_;
    }

    if(mapParams.size() > 0)
    {
        m_logger->warning(WarningLevels::ALGORITHM_ESTIMATOR_WARNING, std::string(intervalEstimatorNames[m_iet]) + ": Redundant params in estimator's map!");
        return _ERROR_WRONG_ARGUMENT_;
    }

    IParams * params= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, IndexesOfParetoIntervalEstimatorParams::FakeIndex_ParetoIntervalEstimatorParams);
    if (!params)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to keep Estimator Params");
        return _ERROR_NO_ROOM_;
    }
    rc = params->setParam(IndexesOfParetoIntervalEstimatorParams::kMin, kMIN);
    if(rc != _RC_SUCCESS_)
    {
        delete params; params = nullptr;
        return rc;
    }
    rc = params->setParam(IndexesOfParetoIntervalEstimatorParams::kMax, kMAX);
    if(rc != _RC_SUCCESS_)
    {
        delete params; params = nullptr;
        return rc;
    }
    rc = params->setParam(IndexesOfParetoIntervalEstimatorParams::Xmin_threshold, Xmin_thrshld);
    if(rc != _RC_SUCCESS_)
    {
        delete params; params = nullptr;
        return rc;
    }
    rc = this->setEstimatorParams(params);
    return rc;
}

int IntervalEstimators::ParetoIntervalEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

int IntervalEstimators::ParetoIntervalEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
    size_t size = params->getSize();
    if (size != ParetoIntervalEstimator::FakeIndex_ParetoIntervalEstimatorParams)
    {
        delete params;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong Estimator Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}

int IntervalEstimators::ParetoIntervalEstimator::setModel(IModel* model)
{
    if(! model)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel  param model is not specified");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IModelPareto * pm = dynamic_cast<IModelPareto *> (model);
    if(! pm)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel param model is not ptr to IModelPareto");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
#ifdef _DEBUG_
    std::cout << m_model->toString() << std::endl;
#endif
    return rc;
}

int IntervalEstimators::ParetoIntervalEstimator::setParamsSet()
{
///TODO!!! WE NEED FACTORY to make ParamsSet!
///��� ���� �������� ������ ParamsSet,
///� ��� ��������� ��� IParams ���� �����,
///���� �� ���� ������ ����� ����������
/// ���������� � ������ ���������� ���� ������ ������ �� �����,
///��������� ���� ��������� ����������� (������ ��� ������ ������),
///�� ��� ����������� ����������� ����� ����� ���������� ��������� �� ������!
/// ������-�� ������ ��� ���������� ��� ������ ���� ���� ����������
/// �� ������ ���� �������. ������� ����� ��������������� ������ ������ (���������� )
    if (m_model == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Model must be set before any attempt to create ParamsSet of Pareto Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
    if (pst == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create ParamsSet of Pareto Model Params failed!");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** create empty ParetoParams */
    for(size_t i=0; i < IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES; ++i)
    {
        IParams * ptrToParams= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::ParetoParams);
        if (ptrToParams == NULL)
        {
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create Pareto Model Params failed!");
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** ptr to allocated params is stored in ParamsSet,
        anyway delete ptrToParams
        */
       if ( pst->addParamsCopyToSet((const IModel*) m_model, ptrToParams, m_logger) == false)
       {
            delete ptrToParams;
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to put Pareto Model Params into ParamsSet failed!");
            return _ERROR_WRONG_ARGUMENT_;
       }
       /** Once we put copy of ptrToParams to setOfEstimatorParams
       free ptrToParams
       */
       delete ptrToParams;
    }

    IParams * params = pst->getFirstPtr();
    IParamsPareto * pp_left = dynamic_cast<IParamsPareto*> (params);
    assert(pp_left != NULL);
    m_leftBound = pp_left;

    params = pst->getNextPtr();
    IParamsPareto * pp_right = dynamic_cast<IParamsPareto*> (params);
    assert(pp_right != NULL);
    m_rightBound = pp_right;
    /** For safety-sake in run-time (when assert won't help)! */
    if ((m_leftBound == NULL) || (m_rightBound == NULL))
    {
        delete pst;
        delete m_leftBound; m_leftBound = NULL;
        delete m_rightBound; m_rightBound = NULL;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can't create ParamsSet of Pareto Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    m_setOfParamsEstimates = pst;
    return _RC_SUCCESS_;
}

/******************************************************************
* this estimator IGNOREs its parameter threadInterruptSignal
* because its life-time very short, its interrupt does not make sense
******************************************************************/
int IntervalEstimators::ParetoIntervalEstimator::estimateInterval(ptrInt threadInterruptSignal)
{
    if (m_data.size() == 0) {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No data to estimate Pareto Params Interval");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    double defaultKmin = 1;
    int rc = m_estimatorParams->getParam(kMin, defaultKmin);
    double defaultKmax= 4;
    rc = m_estimatorParams->getParam(kMax, defaultKmax);
    std::sort(m_data.begin(), m_data.end());
    if (m_data[0] <= 0) {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Sample from Pareto DF MUST keep POSITIVE values ONLY!");
        m_data.clear();
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if (m_data.size() < 2) {
        m_logger->log(ALGORITHM_ESTIMATOR_NOTIFICATION, std::string(intervalEstimatorNames[m_iet]) + ": Only one spike found through track");
        /** magic ESTIMATOR_PARAMS 1, 4 -> default values which come FROM MODEL! */
        double x = m_data[0];
        m_rightBound->setParetoXm(x);
        m_leftBound->setParetoXm(x / defaultKmax);
        m_leftBound->setParetoK( defaultKmin);
        m_rightBound->setParetoK(defaultKmax);
#ifdef _DEBUG_
std::cout << "right Xm" << m_rightBound->xM() << std::endl;
std::cout << "left Xm" << m_leftBound->xM() << std::endl;
std::cout << "left K" << m_leftBound->k() << std::endl;
std::cout << "right K" << m_rightBound->k() << std::endl;
#endif
        return _RC_SUCCESS_;
    }

    rc = m_leftBound->setParetoXm(m_data[0]);

#ifdef _DEBUG_
    for(size_t i = 0; i < m_data.size(); ++i)
    {
        std::cout << m_data[i] << std::endl;
    }
#endif
///TODO!!! check rc!
    m_rightBound->setParetoXm(m_data[m_data.size() / 2]);
///TODO!!! ���������� ��� ��������� "�������"!
    if (m_data.size() < 5) {
        m_leftBound->setParetoK(defaultKmin);
        m_rightBound->setParetoK(defaultKmax);
        return _RC_SUCCESS_;
    }

    double sum_min = 0;
    double sum_max = 0;
    double n_Max = (double)m_data.size();
    double n_Min = (double)(m_data.size() - m_data.size() / 2);

    for (unsigned i = 0; i < m_data.size(); ++i) {
        sum_min += m_data[i] / n_Max;
    }

    if (n_Min < 2) {
        n_Min = n_Max;
///TODO!!! threshold is outer param of this estimator! == 0.999 quantilThreshold of N+(0,1)
        double threshold=0;
///TODO!!! ��� � ���� ����������, ��� ��� � estimatorParams!
/// � ��������� ��� ����� ������, ������ ��� �� ����� ������,
/// �� ������ ������� �� ���������� �����,
/// � ���������� ���� ������, �������������� ��� ��� ����������!
        rc = m_estimatorParams->getParam(Xmin_threshold, threshold);
        if(rc)
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong Xmin");
            return rc;
        }
        sum_max = sum_min + threshold;
    }
    else {
        for (unsigned i = m_data.size() / 2; i < m_data.size(); ++i) {
            sum_max += m_data[i] / n_Min;
        }
    }

    double min_xM = m_leftBound->xM();
    double max_xM = m_rightBound->xM();
#ifdef _DEBUG_
std::cout << "right Xm" << max_xM << std::endl;
std::cout << "left Xm" << min_xM << std::endl;
#endif
///    paramsMax->k =  max(1. / (1. - paramsMin->xM / sum_min), 1. / (1. - paramsMax->xM / sum_max));
    double K = std::max(1. / (1. - min_xM / sum_min), 1. / (1. - max_xM / sum_max));
///    paramsMax->k = min(4., paramsMax->k);
    K = std::min(defaultKmax, K);
    m_rightBound->setParetoK(K);
///paramsMin->k = min(1. / (1. - paramsMin->xM / sum_min), 1. / (1. - paramsMax->xM / sum_max));
    K = std::min(1. / (1. - min_xM / sum_min), 1. / (1. - max_xM / sum_max));
///    paramsMin->k = max(1., paramsMin->k);
    K = std::max(defaultKmin, K);
    if(K > m_rightBound->k())
    {
        double tmpK=m_rightBound->k();
        m_rightBound->setParetoK(K);
        K = tmpK;
    }
    m_leftBound->setParetoK(K);

#ifdef _DEBUG_
std::cout << "right K" << m_rightBound->k() << std::endl;
std::cout << "left K" << m_leftBound->k() << std::endl;
#endif
    return _RC_SUCCESS_;
}

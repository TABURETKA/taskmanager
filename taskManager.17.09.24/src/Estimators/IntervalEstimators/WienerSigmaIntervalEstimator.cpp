#include <math.h> //min, max
#include <algorithm> //sort()
#include "../../Log/errors.h"
#include "../../Params/ParamsSet.h" // Params.h is included
#include "../../Models/Model.h" //IModelPareto
#include "../../DataCode/DataModel/DataMetaInfo.h"
#include "../PointEstimators/FirstDerivativePointEstimator.h" //FIRSTDERIVATIVE_POINTESTIMATOR_TRACKNAME_0
#include "WienerSigmaIntervalEstimator.h"
#include <assert.h>     /** assert */
//#define _DEBUG_

//OLD #define WIENER_INTERVAL_ESTIMATOR_PARAMS_SIZE 2

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace IntervalEstimators;

#define DEFAULT_MAX_PVALUE 0.999f

IntervalEstimators::WienerSigmaIntervalEstimator::WienerSigmaIntervalEstimator(Logger*& log) : FakeIntervalEstimator(log)
{
    /** MUST re-init _pet since in FakeIntervalEstimatyor this member
  has been initialized as FakeIntervalEstimator type
   */
  m_iet = IntervalEstimatorsTypes::WienerSigmaIntervalEstimator;
  /** we MUST INIT logger in CTOR() of each Estimator! */
  /** _task = NULL; m_logger  = log; already initialized in FakeIntervalEstimator's CTOR */
  m_model  = NULL;
  /** here we get estimates, which are produced by this estimator */
  m_setOfParamsEstimates = NULL;
  m_leftBound = NULL;
  m_rightBound = NULL;
  m_estimatorParams  = NULL;
}
/*********************************************************************
*                          DTOR()
**********************************************************************/
IntervalEstimators::WienerSigmaIntervalEstimator::~WienerSigmaIntervalEstimator()
{
  m_data.clear();
  delete m_estimatorParams;
  delete m_setOfParamsEstimates;
  /** NO WAY delete m_leftBound, m_rightBound!
  * They point to the same estimates
  * we just deleted from m_setOfParamsEstimates!

  delete m_leftBound;
  delete m_rightBound;
  */

}
    /** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */

/** generate DataInfo */
int IntervalEstimators::WienerSigmaIntervalEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    /** Total of DataInfo to be created */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    size_t di_0_dimension = IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES;
    /** allocate room for _trackSizes (each track size == Wiener model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    /** init _trackSizes (each track size == Wiener model parameters dimension) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        di_0_dataSetSizes[i] = IParamsWiener::WienerCDFParamsDimension;
    }
/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            while(i){
              --i;
              delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
///REDUNDANT        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        for(size_t j=0; j < di_0_dataSetSizes[i]; ++j)
        {
            data[i][j]=NAN;
        }
        if(i == IntervalEstimateIndexes::LeftBoundary)
        {
            data[i][IParamsWiener::indexesOfGrammarForWienerParams::SIGMA] = m_leftBound->sigma();
        }
        if(i == IntervalEstimateIndexes::RightBoundary)
        {
            data[i][IParamsWiener::indexesOfGrammarForWienerParams::SIGMA] = m_rightBound->sigma();
        }
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = intervalEstimatorNames[IntervalEstimatorsTypes::WienerSigmaIntervalEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = WIENER_INTERVALESTIMATOR_DATASETNAME;
    di[0]->m_dataModelName = m_model->toString();

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }


    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::LeftBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::LeftBoundary];
    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::RightBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::RightBoundary];

    /** put _trackSizes (each track size == Wiener model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    //We have already put data into di[0]! See CTOR di[0]->dataSet = data;
    return _RC_SUCCESS_;
}

int IntervalEstimators::WienerSigmaIntervalEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension < 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];
    if((di->m_dimension != 1) || (di->m_dataSetTrackNames[0] != FIRSTDERIVATIVE_POINTESTIMATOR_TRACKNAME_0))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }

///TODO!!! beforehand put "values of first derivative" into TRACK_NAME and compare it right here with di->dataSetTrackNames[0]!
    if ((di->m_dataSetSizes[0] == 0) || (di->m_dataSet == NULL) || (di->m_dataSet[0] == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di->m_dataSetSizes[0]);
    for(size_t i=0; i<di->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di->m_dataSet[0][i];
#ifdef _DEBUG_
std::cout << m_data[i] << std::endl;
#endif
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif


///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}
/** IIntervalEstimator stuff */
///TODO!!! SO far size and ptrToPtrIParams are redundant !
int IntervalEstimators::WienerSigmaIntervalEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
    return _ERROR_WRONG_WORKFLOW_;
}

int IntervalEstimators::WienerSigmaIntervalEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return _ERROR_WRONG_WORKFLOW_;
}
int IntervalEstimators::WienerSigmaIntervalEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
    return _ERROR_WRONG_WORKFLOW_;
}

int IntervalEstimators::WienerSigmaIntervalEstimator::setModel(IModel* model)
{
    if(! model)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel  param model is not specified");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IModelWiener * pm = dynamic_cast<IModelWiener *> (model);
    if(! pm)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel param model is not ptr to IModelWiener");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
#ifdef _DEBUG_
std::cout << m_model->toString() << std::endl;
#endif
    return rc;
}
/*********************************************************************************************************************
* We merely create params set with two empty WienerParams into it.
* The set will be filled with estimates later, in estimateInterval()
*********************************************************************************************************************/
int IntervalEstimators::WienerSigmaIntervalEstimator::setParamsSet()
{
///TODO!!! WE NEED FACTORY to make ParamsSet!
///��� ���� �������� ������ ParamsSet,
///� ��� ��������� ��� IParams ���� �����,
///���� �� ���� ������ ����� ����������
/// ���������� � ������ ���������� ���� ������ ������ �� �����,
///��������� ���� ��������� ����������� (������ ��� ����� ������),
///�� ��� ����������� ����������� ����� ����� ���������� ��������� �� ������!
/// ������-�� ������ ��� ���������� ��� ������ ���� ���� ����������
/// �� ������ ���� �������. ������� ����� ��������������� ������ ������ (���������� )
    if (m_model == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Model must be set before any attempt to create ParamsSet of Wiener Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
    if (pst == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create ParamsSet of Wiener Model Params failed!");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** create empty WienerParams */
    for(size_t i=0; i < IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES; ++i)
    {
/// IParams * ptrToParams= IParams::createEmptyParams(WienerParams, 1); <- 1-�� �� ����� ������!
        IParams * ptrToParams= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::WienerParams);
        if (pst == NULL)
        {
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create Wiener Model Params failed!");
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** ptr to allocated params is stored in ParamsSet,
        anyway delete ptrToParams
        */
       if ( pst->addParamsCopyToSet((const IModel*) m_model,ptrToParams, m_logger) == false)
       {
            delete ptrToParams;
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to put Wiener Model Params into ParamsSet failed!");
            return _ERROR_WRONG_ARGUMENT_;
       }
       /** Once we put copy of ptrToParams to setOfEstimatorParams
       free ptrToParams
       */
       delete ptrToParams;
    }

    IParams * params = pst->getFirstPtr();
    IParamsWiener * pp_left = dynamic_cast<IParamsWiener*> (params);
    assert(pp_left != NULL);
    m_leftBound = pp_left;

    params = pst->getNextPtr();
    IParamsWiener * pp_right = dynamic_cast<IParamsWiener*> (params);
    assert(pp_right != NULL);
    m_rightBound = pp_right;
    /** For safety-sake in run-time (when assert won't help)! */
    if ((m_leftBound == NULL) || (m_rightBound == NULL))
    {
        delete pst;
        delete m_leftBound; m_leftBound = NULL;
        delete m_rightBound; m_rightBound = NULL;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can't create ParamsSet of Wiener Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    m_setOfParamsEstimates = pst;
    return _RC_SUCCESS_;
}

/******************************************************************
* this estimator IGNOREs its parameter threadInterruptSignal
* because its life-time very short, its interrupt does not make sense
******************************************************************/
int IntervalEstimators::WienerSigmaIntervalEstimator::estimateInterval(ptrInt threadInterruptSignal)
{
    if (m_data.size() == 0) {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No data to estimate Wiener Params Interval");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    double defaultMu = 0.0f;

    std::vector<double> abs_first_deriv;
    for(size_t i=0; i < m_data.size(); ++i)
    {
///TODO!!! ������������� ��� : "Overflow" <- !
        abs_first_deriv.push_back(fabsf(m_data[i]));
    }
    sort(abs_first_deriv.begin(), abs_first_deriv.end());
    double threshold;

    double pValue = DEFAULT_MAX_PVALUE;
///TODO!!! ����� �����������! ������ ����� ����� ����! ��� � ������ GaussianOutliersByQuantilePointEstimator
    int rc = IModelGaussian::Quantil(abs_first_deriv, pValue, threshold);
    if(rc != _RC_SUCCESS_)
    {
        return rc;
    }
    double sum_deviations_min = 0.;
    int counter_deviations_min = 0;
    double sum_absDeviations_max = 0.;
    size_t counter_absDeviations_max = 0;

    for (size_t i = 0; i < m_data.size(); ++i)
    {
        if (fabs(m_data[i]) < threshold) {
            sum_absDeviations_max += fabs(m_data[i]) * fabs(m_data[i]);
            counter_absDeviations_max++;
        }
        if ((m_data[i] > 0) && (m_data[i] < threshold)) {
            sum_deviations_min += m_data[i] * m_data[i];
            counter_deviations_min++;
        }
    }
    double minDeviations = sqrt(sum_deviations_min / (double)counter_deviations_min);
    double maxDeviations = sqrt(sum_absDeviations_max / (double)counter_absDeviations_max);

    m_leftBound->setSDeviation(minDeviations);
    m_rightBound->setSDeviation(maxDeviations);

#ifdef _DEBUG_
for(size_t i = 0; i < m_data.size(); ++i)
{
    std::cout << m_data[i] << std::endl;
}
#endif
#ifdef _DEBUG_
std::cout << "right sigma" << m_rightBound->sigma() << std::endl;
std::cout << "left sigma" << m_leftBound->sigma() << std::endl;
#endif
    return _RC_SUCCESS_;
}

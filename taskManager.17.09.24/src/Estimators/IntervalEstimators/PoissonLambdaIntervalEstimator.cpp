#include "../../CommonStuff/Defines.h" /* isNAN_double */
#include <math.h> //min, max
#include <algorithm> //sort()
#include "../../Log/errors.h"
#include "../../Params/ParamsSet.h" // Params.h is included
#include "../../Models/Model.h" //IModelPoisson
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../../DataCode/DataModel/DataMetaInfo.h"
#include "PoissonLambdaIntervalEstimator.h"
#include <assert.h>     /** assert */
//#define _DEBUG_
/** ��� ����������� ���������� ��������� ����� ����������!
�� ������ POISSON!
*/
//OLD #define POISSON_INTERVAL_ESTIMATOR_PARAMS_SIZE 2

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace IntervalEstimators;

IntervalEstimators::PoissonLambdaIntervalEstimator::PoissonLambdaIntervalEstimator(Logger*& log) : FakeIntervalEstimator(log)
{
    /** MUST re-init _pet since in FakeIntervalEstimatyor this member
  has been initialized as FakeIntervalEstimator type
   */
  m_iet = IntervalEstimatorsTypes::PoissonLambdaIntervalEstimator;
  /** we MUST INIT logger in CTOR() of each Estimator! */
  /** _task = NULL; m_logger  = log; already initialized in FakeIntervalEstimator's CTOR */
  m_model  = NULL;
  /** here we get estimates, which are produced by this estimator */
  m_setOfParamsEstimates = NULL;
  m_leftBound = NULL;
  m_rightBound = NULL;
  m_estimatorParams  = NULL;
  m_dataSizeOriginal = 0;
}
/*********************************************************************
*                          DTOR()
**********************************************************************/
IntervalEstimators::PoissonLambdaIntervalEstimator::~PoissonLambdaIntervalEstimator()
{
  m_data.clear();
  delete m_estimatorParams;

  delete m_setOfParamsEstimates;
  /** NO WAY delete m_leftBound, m_rightBound!
  * They point to the same estimates
  * we just deleted from m_setOfParamsEstimates!

  delete m_leftBound;
  delete m_rightBound;
  */

}
    /** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */

/** generate DataInfo */
int IntervalEstimators::PoissonLambdaIntervalEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    /** Total of DataInfo to be created */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    size_t di_0_dimension = IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES;
    /** allocate room for _trackSizes (each track size == Poisson model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));
    /** init _trackSizes (each track size == Poisson model parameters dimension) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        di_0_dataSetSizes[i] = IParamsPoisson::PoissonCDFParamsDimension;
    }
/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));
    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            while(i){
              --i;
              delete [] data[i];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));
        if(i == IntervalEstimateIndexes::LeftBoundary)
        {
            data[i][0] = m_leftBound->lambda();
        }
        if(i == IntervalEstimateIndexes::RightBoundary)
        {
            data[i][0] = m_rightBound->lambda();
        }
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        size_t i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = intervalEstimatorNames[IntervalEstimatorsTypes::PoissonLambdaIntervalEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = POISSON_INTERVALESTIMATOR_DATASETNAME;
    di[0]->m_dataModelName = m_model->toString();

    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }

    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::LeftBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::LeftBoundary];
    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::RightBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::RightBoundary];

    /** put _trackSizes (each track size == Poisson model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    //We have already put data into di[0]! See CTOR di[0]->dataSet = data;
    return _RC_SUCCESS_;
}

int IntervalEstimators::PoissonLambdaIntervalEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension < 2)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di_0 = ptrToPtrDataInfo[0];

    if(di_0->m_dimension < 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong original input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
///TODO!!! beforehand put "values" into TRACK_NAME and compare it right here with di->dataSetTrackNames[0]!
/// then put "outliers_indexes"??? (OR USE any alias?) into TRACK_NAME and compare it right here with di->dataSetTrackNames[1]!
///TODO!!! ������ � �����
    if ((di_0->m_dataSetSizes[0] == 0) || (di_0->m_dataSet == NULL))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong original input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
///!!! ��������� ���� ������ �� �����, �� ����� ���� ����� ������ ��� ������!
/// �.�. �������
     assert(di_0->m_dataSetSizes[0] > 0);
     m_dataSizeOriginal=di_0->m_dataSetSizes[0];

///TODO!!! beforehand  put "outliers_indexes"??? (OR USE any alias?) into TRACK_NAME and compare it right here with di->dataSetTrackNames[1]!
///TODO!!! ������ � �����
    const DataInfo* di_1 = ptrToPtrDataInfo[1];
    if ((di_1 == NULL) || (di_1->m_dataSet == NULL) || (di_1->m_dataSetSizes[0] == 0))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    if (di_1->m_dataSetSizes[0] >= m_dataSizeOriginal )
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data : total of Poisson events GE size of original dataset!");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.resize(di_1->m_dataSetSizes[0]);

    for(size_t i=0; i < di_1->m_dataSetSizes[0]; ++i)
    {
        m_data[i] = di_1->m_dataSet[0][i];
#ifdef _DEBUG_
std::cout << m_data[i] << std::endl;
#endif
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}
/** IIntervalEstimator stuff */
///TODO!!! SO far size and ptrToPtrIParams are redundant !
int IntervalEstimators::PoissonLambdaIntervalEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
    iteratorParamsMap itParam = mapParams.find(grammarPoissonLambdaIntervalEstimatorParams[lambdaMinDenom]);
    double lambdaMinDenom=NAN;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), lambdaMinDenom); //OLD          lambdaMinDenom = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong value of " + grammarPoissonLambdaIntervalEstimatorParams[IndexesOfPoissonLambdaIntervalEstimatorParams::lambdaMinDenom]);
        return _ERROR_WRONG_ARGUMENT_;
    }

    double lambdaMaxDenom=NAN;

    itParam = mapParams.find(grammarPoissonLambdaIntervalEstimatorParams[IndexesOfPoissonLambdaIntervalEstimatorParams::lambdaMaxDenom]);
    /** we are pessimists */
    rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<double>(itParam->second.c_str(), lambdaMaxDenom); //OLD          lambdaMaxDenom = atof(itParam->second.c_str());
        mapParams.erase(itParam);
    }
    if(rc != _RC_SUCCESS_)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong value of " + grammarPoissonLambdaIntervalEstimatorParams[IndexesOfPoissonLambdaIntervalEstimatorParams::lambdaMaxDenom]);
        return _ERROR_WRONG_ARGUMENT_;
    }

    IParams * params= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, FakeIndex_PoissonLambdaIntervalEstimatorParams);
    if (!params)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to keep Estimator Params");
        return _ERROR_NO_ROOM_;
    }
    params->setParam(IndexesOfPoissonLambdaIntervalEstimatorParams::lambdaMinDenom, lambdaMinDenom);
    params->setParam(IndexesOfPoissonLambdaIntervalEstimatorParams::lambdaMaxDenom, lambdaMaxDenom);
    rc = this->setEstimatorParams(params);
    return rc;
}

int IntervalEstimators::PoissonLambdaIntervalEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

int IntervalEstimators::PoissonLambdaIntervalEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
    size_t size = params->getSize();
    if (size != FakeIndex_PoissonLambdaIntervalEstimatorParams)
    {
        delete params;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong Estimator Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}

int IntervalEstimators::PoissonLambdaIntervalEstimator::setModel(IModel* model)
{
    if(! model)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel  param model is not specified");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IModelPoisson * pm = dynamic_cast<IModelPoisson *> (model);
    if(! pm)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel param model is not ptr to IModelPoisson");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
#ifdef _DEBUG_
std::cout << m_model->toString() << std::endl;
#endif
    return rc;
}

int IntervalEstimators::PoissonLambdaIntervalEstimator::setParamsSet()
{
///TODO!!! WE NEED FACTORY to make ParamsSet!
///��� ���� �������� ������ ParamsSet,
///� ��� ��������� ��� IParams ���� �����,
///���� �� ���� ������ ����� ����������
/// ���������� � ������ ���������� ���� ������ ������ �� �����,
///��������� ���� ��������� ����������� (������ ��� �������� ������),
///�� ��� ����������� ����������� ����� ����� ���������� ��������� �� ������!
/// ������-�� ������ ��� ���������� ��� ������ ���� ���� ����������
/// �� ������ ���� �������. ������� ����� ��������������� ������ ������ (���������� )
    if (m_model == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Model must be set before any attempt to create ParamsSet of Poisson Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
    if (pst == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create ParamsSet of Poisson Model Params failed!");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** create empty PoissonParams */
    for(size_t i=0; i < IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES; ++i)
    {
        IParams * ptrToParams= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::PoissonParams);
        if (pst == NULL)
        {
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create Poisson Model Params failed!");
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** ptr to allocated params is stored in ParamsSet,
        anyway delete ptrToParams
        */
       if ( pst->addParamsCopyToSet((const IModel*) m_model,ptrToParams, m_logger) == false)
       {
            delete ptrToParams;
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to put Poisson Model Params into ParamsSet failed!");
            return _ERROR_WRONG_ARGUMENT_;
       }
       /** Once we put copy of ptrToParams to setOfEstimatorParams
       free ptrToParams
       */
       delete ptrToParams;
    }

    IParams * params = pst->getFirstPtr();
    IParamsPoisson * pp_left = dynamic_cast<IParamsPoisson*> (params);
    assert(pp_left != NULL);
    m_leftBound = pp_left;

    params = pst->getNextPtr();
    IParamsPoisson * pp_right = dynamic_cast<IParamsPoisson*> (params);
    assert(pp_right != NULL);
    m_rightBound = pp_right;
    /** For safety-sake in run-time (when assert won't help)! */
    if ((m_leftBound == NULL) || (m_rightBound == NULL))
    {
        delete pst;
        delete m_leftBound; m_leftBound = NULL;
        delete m_rightBound; m_rightBound = NULL;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can't create ParamsSet of Poisson Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    m_setOfParamsEstimates = pst;
    return _RC_SUCCESS_;
}

/******************************************************************
* this estimator IGNOREs its parameter threadInterruptSignal
* because its life-time very short, its interrupt does not make sense
******************************************************************/
int IntervalEstimators::PoissonLambdaIntervalEstimator::estimateInterval(ptrInt threadInterruptSignal)
{
    if (m_data.size() == 0) {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No data to estimate Poisson Params Interval");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    double defaultLambdaMinDenom = NAN;
    /** if Poisson.cfg has been parsed
    */
    int rc = -100;
    if(m_estimatorParams)
    {
        rc = m_estimatorParams->getParam(lambdaMinDenom, defaultLambdaMinDenom);
    }
    if ((rc != _RC_SUCCESS_) || (isNAN_double(defaultLambdaMinDenom)))
    {
        /** For safety-sake!
        */
        defaultLambdaMinDenom = 0.5f;
    }
    double defaultLambdaMaxDenom= NAN;
    /** if Poisson.cfg has been parsed
    */
    if(m_estimatorParams)
    {
        rc = m_estimatorParams->getParam(lambdaMaxDenom, defaultLambdaMaxDenom);
    }
    if ((rc != _RC_SUCCESS_) || (isNAN_double(defaultLambdaMaxDenom)))
    {
        /** For safety-sake!
        */
        defaultLambdaMaxDenom= 2.0f;
    }
    double lambdaMin = defaultLambdaMinDenom / (double)m_dataSizeOriginal;
    double lambdaMax = defaultLambdaMaxDenom / (double)m_dataSizeOriginal;
    if (m_dataSizeOriginal < 2) {
        m_logger->log(ALGORITHM_ESTIMATOR_NOTIFICATION, std::string(intervalEstimatorNames[m_iet]) + ": Too small original dataset to search for Poisson events!");
        /** magic ESTIMATOR_PARAMS 0.5f, 2.0f -> default values which come FROM MODEL! */
       if (m_dataSizeOriginal == 1)
       {
        ///TODO!!!
           lambdaMax = defaultLambdaMaxDenom / (double)m_data[0];
       }

       return _RC_SUCCESS_;
    }
    /** First estimate parameter of exponential d.f */
    double time_avg = (double)(m_data[m_data.size() - 1] - m_data[0]) / (double)(m_data.size() - 1);
    /** and s.d. of parameter of exponential d.f */
    double time_avg_dev = time_avg / sqrt((double)m_data.size());

    lambdaMin = std::min(0.5 / time_avg, 1. / (time_avg + time_avg_dev));
    lambdaMax = 1. / (time_avg - time_avg_dev);
    m_leftBound->setPoissonLambda(lambdaMin);
    m_rightBound->setPoissonLambda(lambdaMax);

#ifdef _DEBUG_
std::cout << "right lambda" << m_rightBound->lambda() << std::endl;
std::cout << "left lambda" << m_leftBound->lambda() << std::endl;
#endif
#ifdef _DEBUG_
for(size_t i = 0; i < m_data.size(); ++i)
{
    std::cout << m_data[i] << std::endl;
}
#endif
    return _RC_SUCCESS_;
}

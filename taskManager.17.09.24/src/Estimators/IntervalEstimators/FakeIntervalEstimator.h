#ifndef __FAKEINTERVALESTIMATOR_H__633569422542968750
#define __FAKEINTERVALESTIMATOR_H__633569422542968750
#include "../Estimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
namespace IntervalEstimators{
    /** class container */
/** implementation of basic methods
 declared in abstract Base class IIntervalEstimator
 The methods setTask, setLog, toString
 have to be inherited all IntervalEstimators
*/
class FakeIntervalEstimator : public virtual IIntervalEstimator
{
public:
    FakeIntervalEstimator(Logger*& log);
    virtual ~FakeIntervalEstimator();
    /** IEstimator stuff */
    virtual void setTask(Task* const & task);
    virtual void setLog(Logger* const & log);
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    virtual std::string toString() const;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IPointEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

///TODO!!! ��� ���� � setData()???
    virtual int estimateInterval(ptrInt threadInterruptSignal);

protected:
    int logErrMsgAndReturn(int rc, std::string msg) const;
    virtual int parsingFailed(std::string msg) const;
    enum IntervalEstimatorsTypes m_iet;
    Task* m_task;
    Logger* m_logger;
private:
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    FakeIntervalEstimator(const FakeIntervalEstimator&);
    void operator=(const FakeIntervalEstimator&);

//    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
//    IParamsSet* m_pst;
//    IParams* m_estimatorParams;

///TODO!!! ��� ���� � �����, � ������� ����� setData()???

};

} /// end namespace IntervalEstimators
#endif

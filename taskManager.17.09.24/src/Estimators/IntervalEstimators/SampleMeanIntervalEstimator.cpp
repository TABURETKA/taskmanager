//OLD #include <stdlib.h> //atof()
#include <math.h> //min, max
#include <float.h> /* DBL_MIN */
#include <algorithm> //sort()
#include "../../Log/errors.h"
#include "../../CommonStuff/Defines.h" /* isNAN_double() */
#include "../../Config/ConfigParser.h" /* iteratorParamsMap */
#include "../../Params/ParamsSet.h" // Params.h is included
#include "../../Models/Model.h" /* IModel */
#include "../../DataCode/DataModel/DataMetaInfo.h"
#include "SampleMeanIntervalEstimator.h"
#include <assert.h>     /** assert */

//#define _DEBUG_

/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace IntervalEstimators;

///static
const char* IntervalEstimators::SampleMeanIntervalEstimator::grammarOfDataLocationSemantics[FakeIndex_DataLocationSemantics]={
        "indexOfDataInRow",
        "indexOfDataInCol",
        "confidenceRadiusAsTotalOfSDs"
};

IntervalEstimators::SampleMeanIntervalEstimator::SampleMeanIntervalEstimator(Logger*& log) : FakeIntervalEstimator(log)
{
    /** MUST re-init _pet since in FakeIntervalEstimatyor this member
  has been initialized as FakeIntervalEstimator type
   */
  m_iet = IntervalEstimatorsTypes::SampleMeanIntervalEstimator;
  /** we MUST INIT logger in CTOR() of each Estimator! */
  /** _task = NULL; m_logger  = log; already initialized in FakeIntervalEstimator's CTOR */
  m_model  = NULL;
  /** here we get estimates, which are produced by this estimator */
  m_setOfParamsEstimates = NULL;
  m_leftBound = NULL;
  m_rightBound = NULL;
  m_estimatorParams  = NULL;

}
/*********************************************************************
*                          DTOR()
**********************************************************************/
IntervalEstimators::SampleMeanIntervalEstimator::~SampleMeanIntervalEstimator()
{
  m_data.clear();
  delete m_estimatorParams;

  delete m_setOfParamsEstimates;
  /** NO WAY delete m_leftBound, m_rightBound!
  * They point to the same estimates
  * we just deleted from m_setOfParamsEstimates!

  delete m_leftBound;
  delete m_rightBound;
  */

}
    /** IEstimator stuff */
/** ������������ � setTask, � setLog, � toString! */
/** ����� ������, ����������� ��� ������� ���������! */

/** generate DataInfo */
int IntervalEstimators::SampleMeanIntervalEstimator::getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** & di) const
{
    /** Total of DataInfo to be created */
    dimension = 1;
    di = new DataInfo* [dimension];
    if(!di)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo** for estimates");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di, 0, dimension*sizeof(DataInfo*));
    /** 2 == leftBoundary and rightBoundary! */
    size_t di_0_dimension = IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES;
    /** allocate room for _trackSizes (each track size == Pareto model parameters dimension) */
    size_t* di_0_dataSetSizes = new size_t[di_0_dimension];
    if(!di_0_dataSetSizes)
    {
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate ptrs to dataSetSizes");
        return _ERROR_NO_ROOM_;
    }
    std::memset(di_0_dataSetSizes, 0, di_0_dimension*sizeof(size_t));

    /** init _trackSizes (each track size == m_estimatesDim - Params1D dimension) */
    for(size_t i=0; i < di_0_dimension; ++i)
    {
        di_0_dataSetSizes[i] = m_estimatesDim;
    }
/// ��� ��� DataInfo->dataSet ������ double const ** const, ��� ����� ���������������� ��� ��������, ��� ������� � ������������� ��������� ������ data ����, ��� �������� di[0]
    /** array of ptrs to 1-D sets */
    double** data = new double*[di_0_dimension];
    if (!data)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
        delete [] di_0_dataSetSizes;
        delete [] di;
        return _ERROR_NO_ROOM_;
    }
    std::memset(data, 0, di_0_dimension*sizeof(double*));

    for(size_t i = 0; i < di_0_dimension; ++i)
    {
        data[i] = new double[di_0_dataSetSizes[i]];
        if (!data[i])
        {
            int j =i;
            while(j){
              --j;
              delete [] data[j];
            }
            delete [] data;
            delete [] di_0_dataSetSizes;
            delete [] di;

            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to return estimates");
            return _ERROR_NO_ROOM_;
        }
        std::memset(data[i], 0, di_0_dataSetSizes[i]*sizeof(double));

        for(size_t j=0; j < di_0_dataSetSizes[i]; ++j)
        {
            if(i == IntervalEstimateIndexes::LeftBoundary)
            {
                int rc = m_leftBound->getParam(j, data[i][j]);
                if(_RC_SUCCESS_ != rc){
                    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can not get left bound from interval estimates!");
///TODO!!! �������� ���� di!
                    return _ERROR_WRONG_INPUT_DATA_;
                }
            }
            if(i == IntervalEstimateIndexes::RightBoundary)
            {
                int rc = m_rightBound->getParam(j, data[i][j]);
                if(_RC_SUCCESS_ != rc){
                    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can not get right bound from interval estimates!");
///TODO!!! �������� ���� di!
                    return _ERROR_WRONG_INPUT_DATA_;
                }
            }
        }
    }
    /** We put estimates pointers to DataInfo CTOR to save them as double const ** const */
    di[0] = new DataInfo(data);
    if (!di[0])
    {
        int i = di_0_dimension;
        while(i)
        {
            --i;
            delete [] data[i];
        }
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate DataInfo* for estimates");
        return _ERROR_NO_ROOM_;
    }
    /** 2 == leftBoundary and rightBoundary! */
    di[0]->m_dimension = di_0_dimension;
    /** the name of data source, for example name of source file or name of Estimator */
    di[0]->m_dataCreatorName = intervalEstimatorNames[IntervalEstimatorsTypes::SampleMeanIntervalEstimator];

    /** the name of data set, for example regression location or regression coefficients */
    di[0]->m_dataSetName = SAMPLE_MEAN_INTERVALESTIMATOR_DATASETNAME;
    if(m_model)
    {
        di[0]->m_dataModelName = m_model->toString();
    } else {
        di[0]->m_dataModelName = IModel::modelNames[IModel::ModelsTypes::PlainModel];
    }


    di[0]->m_dataSetTrackNames = new std::string [di[0]->m_dimension];
    if (! di[0]->m_dataSetTrackNames)
    {
        delete [] di[0];
        delete [] data;
        delete [] di_0_dataSetSizes;
        delete [] di;

        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to allocate dataSetTrackNames");
        return _ERROR_NO_ROOM_;
    }


    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::LeftBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::LeftBoundary];
    di[0]->m_dataSetTrackNames[IntervalEstimateIndexes::RightBoundary]=grammarIntervalEstimates[IntervalEstimateIndexes::RightBoundary];

    /** put _trackSizes (each track size == Pareto model parameters dimension) into di */
    di[0]->m_dataSetSizes =  di_0_dataSetSizes; //new int[di[0]->dimension];
    //NO WAY    delete [] data; delete [] di_0_dataSetSizes; delete di[o]; delete [] di;
    //We have already put data into di[0]! See CTOR di[0]->dataSet = data;
    return _RC_SUCCESS_;
}

int IntervalEstimators::SampleMeanIntervalEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    if(dimension < 1)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    m_data.clear();
    const DataInfo* di = ptrToPtrDataInfo[0];
    if (di->m_dataSet == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": NO input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }

    if(m_estimatorParams == NULL)
    {
        return _ERROR_WRONG_WORKFLOW_;
    }
    bool isDataInCol = true;
    int col = -1;
    int row = 0; /** by default data are in first row */
    double tmpDbl=NAN;
    m_estimatorParams->getParam(indexOfDataInRow, tmpDbl);
    if((int)tmpDbl >= row)
    {
        row = (int)tmpDbl;
        isDataInCol = false;
    } else {
        row = -1;
        m_estimatorParams->getParam(indexOfDataInCol, tmpDbl);
        if((int)tmpDbl > col)
            col = (int)tmpDbl;
    }
    /** Semantics of m_dimension in DataInfo -> total of rows in m_dataSet */
    if ((!isDataInCol) && (di->m_dimension <= (size_t)row))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Specified row does not exist into input data");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    else if(isDataInCol)
    {
        for(size_t i=0; i < di->m_dimension; ++i)
        {
            if(di->m_dataSetSizes[i] <= (size_t)col)
            {
                m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Specified column does not exist into input data");
                return _ERROR_WRONG_INPUT_DATA_;
            }
        }
    }
    size_t size = di->m_dimension;
    if(!isDataInCol)
    {
        size = di->m_dataSetSizes[row];
    }
    m_data.resize(size);

    if(!isDataInCol)
    {
        for(size_t i=0; i < size; ++i)
        {
            m_data[i] = di->m_dataSet[row][i];
#ifdef _DEBUG_
            std::cout << m_data[i] << std::endl;
#endif
        }
    } else /** data in col */
    {
        for(size_t i=0; i < size; ++i)
        {
            m_data[i] = di->m_dataSet[i][col];
#ifdef _DEBUG_
            std::cout << m_data[i] << std::endl;
#endif
        }
    }
#ifdef _DEBUG_
std::cout << m_data.size() << std::endl;
#endif
///TODO!!! So far we don't save metainfo on input data!
    return _RC_SUCCESS_;
}
/** IIntervalEstimator stuff */
///TODO!!! SO far size and ptrToPtrIParams are redundant !
int IntervalEstimators::SampleMeanIntervalEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
///TODO!!!
    iteratorParamsMap itParam = mapParams.find(grammarOfDataLocationSemantics[IndexesOfDataLocationSemantics::indexOfDataInCol]);
    int iRow=-1;
    int iCol=-1;
    /** we are pessimists */
    int rc = _ERROR_NO_INPUT_DATA_;
    if (itParam != mapParams.end())
    {
        rc = strTo<int>(itParam->second.c_str(), iCol); //OLD iCol = atoi(itParam->second.c_str());
        mapParams.erase(itParam);
    }

    if((rc != _RC_SUCCESS_) || (iCol < 0))
    {
        m_logger->log(ALGORITHM_4_LEVEL_INFO, std::string(intervalEstimatorNames[m_iet]) + ": Info on data in col NOT FOUND :" + grammarOfDataLocationSemantics[IndexesOfDataLocationSemantics::indexOfDataInCol]);
        itParam = mapParams.find(grammarOfDataLocationSemantics[IndexesOfDataLocationSemantics::indexOfDataInRow]);
        /** we are pessimists */
        rc = _ERROR_NO_INPUT_DATA_;
        if (itParam != mapParams.end())
        {
            rc = strTo<int>(itParam->second.c_str(), iRow); //OLD iRow = atoi(itParam->second.c_str());
            mapParams.erase(itParam);
        }

        if((rc != _RC_SUCCESS_) || (iRow < 0))
        {
            m_logger->log(ALGORITHM_4_LEVEL_INFO, std::string(intervalEstimatorNames[m_iet]) + ": Info on data in rol NOT FOUND :" + grammarOfDataLocationSemantics[IndexesOfDataLocationSemantics::indexOfDataInRow]
                         + " Take they are in 0-col." );
            iCol = 0;
        }
    }///end if iCol Not found

    int confidenceRadius = 0;
    itParam = mapParams.find(grammarOfDataLocationSemantics[IndexesOfDataLocationSemantics::confidenceRadiusAsTotalOfSDs]);
    if (itParam != mapParams.end()) {
        /** we are pessimists */
        int rc = _ERROR_NO_INPUT_DATA_;
        rc = strTo<int>(itParam->second.c_str(), confidenceRadius); //OLD                  confidenceRadius = atoi(itParam->second.c_str());
        mapParams.erase(itParam);

        if((rc != _RC_SUCCESS_) || (confidenceRadius <= 0))
        {
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": confidenceRadiusAsTotalOfSDs MUST be positive!");
            return _ERROR_WRONG_ESTIMATOR_PARAMS_;
        }
    }
    else {
        m_logger->log(ALGORITHM_4_LEVEL_INFO, std::string(intervalEstimatorNames[m_iet]) + ": Info on confidenceRadiusAsTotalOfSDs NOT FOUND :" + grammarOfDataLocationSemantics[IndexesOfDataLocationSemantics::confidenceRadiusAsTotalOfSDs]);
        return _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }

    IParams * params= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, FakeIndex_DataLocationSemantics);
    if (!params)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No room to keep Estimator Params");
        return _ERROR_NO_ROOM_;
    }
    rc = params->setParam(IndexesOfDataLocationSemantics::indexOfDataInRow, iRow);
    /**
     We can not ignore above rc!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=nullptr;
        return rc;
    }
    rc = params->setParam(IndexesOfDataLocationSemantics::indexOfDataInCol, iCol);
    /**
     We can not ignore above rc!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=nullptr;
        return rc;
    }
    rc = params->setParam(IndexesOfDataLocationSemantics::confidenceRadiusAsTotalOfSDs, confidenceRadius);
    /**
     We can not ignore above rc!
    */
    if(_RC_SUCCESS_ != rc)
    {
        delete params; params=nullptr;
        return rc;
    }
    rc = this->setEstimatorParams(params);
    return rc;
}

int IntervalEstimators::SampleMeanIntervalEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

int IntervalEstimators::SampleMeanIntervalEstimator::setEstimatorParams(IParams*& params)
{
    size_t size = params->getSize();
    if (size != SampleMeanIntervalEstimator::FakeIndex_DataLocationSemantics)
    {
        delete params;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Wrong Estimator Params");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_estimatorParams = params;
    return _RC_SUCCESS_;
}

int IntervalEstimators::SampleMeanIntervalEstimator::setModel(IModel* model)
{
    if(! model)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel() param model is not specified");
        return _ERROR_WRONG_ARGUMENT_;
    }
    IModelPlain * m = dynamic_cast<IModelPlain *> (model);
    if(! m)
    {
        m_logger->error(ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + "::setModel() param model is not ptr to IModelPlain");
        return _ERROR_WRONG_ARGUMENT_;
    }
    m_model = model;
    /**
     Mandatory initialization for each estimator's setModel()!
    */
    int rc = setParamsSet();
#ifdef _DEBUG_
    std::cout << m_model->toString() << std::endl;
#endif
    return rc;
}

int IntervalEstimators::SampleMeanIntervalEstimator::setParamsSet()
{
///TODO!!! WE NEED FACTORY to make ParamsSet!
///��� ���� �������� ������ ParamsSet,
///� ��� ��������� ��� IParams ���� �����,
///���� �� ���� ������ ����� ����������
/// ���������� � ������ ���������� ���� ������ ������ �� �����,
///��������� ���� ��������� ����������� (������ ��� ������ ������),
///�� ��� ����������� ����������� ����� ����� ���������� ��������� �� ������!
/// ������-�� ������ ��� ���������� ��� ������ ���� ���� ����������
/// �� ������ ���� �������. ������� ����� ��������������� ������ ������ (���������� )
    if (m_model == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Model must be set before any attempt to create ParamsSet of Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    IParamsSet * pst = IParamsSet::createEmptyParamsSet((const IModel*) m_model, m_logger);
    if (pst == NULL)
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create ParamsSet of Model Params failed!");
        return _ERROR_WRONG_ARGUMENT_;
    }
    /** create empty ParetoParams */
    for(size_t i=0; i < IntervalEstimateIndexes::TOTAL_OF_INTERVAL_ESTIMATE_INDEXES; ++i)
    {
        IParams * ptrToParams= IParams::createEmptyParams(m_logger, IParams::ParamsTypes::Params1D, m_estimatesDim);
        if (ptrToParams == NULL)
        {
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to create Empty Model Params failed!");
            return _ERROR_WRONG_ARGUMENT_;
        }
        /** ptr to allocated params is stored in ParamsSet,
        anyway delete ptrToParams
        */
       if ( pst->addParamsCopyToSet((const IModel*) m_model, ptrToParams, m_logger) == false)
       {
            delete ptrToParams;
            delete pst;
            m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Attempt to put Empty Model Params into ParamsSet failed!");
            return _ERROR_WRONG_ARGUMENT_;
       }
       /** Once we put copy of ptrToParams to setOfEstimatorParams
       free ptrToParams
       */
       delete ptrToParams;
    }

    IParams * params = pst->getFirstPtr();
    assert(params != NULL);
    m_leftBound = params;

    params = pst->getNextPtr();
    assert(params != NULL);
    m_rightBound = params;
    /** For safety-sake in run-time (when assert won't help)! */
    if ((m_leftBound == NULL) || (m_rightBound == NULL))
    {
        delete pst;
        delete m_leftBound; m_leftBound = NULL;
        delete m_rightBound; m_rightBound = NULL;
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": Can't create ParamsSet of Plain Model Params");
        return _ERROR_WRONG_MODEL_PARAMS_;
    }
    m_setOfParamsEstimates = pst;
    return _RC_SUCCESS_;
}

/******************************************************************
* this estimator IGNOREs its parameter threadInterruptSignal
* because its life-time very short, its interrupt does not make sense
******************************************************************/
int IntervalEstimators::SampleMeanIntervalEstimator::estimateInterval(ptrInt threadInterruptSignal)
{
    if (m_data.size() == 0) {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No data to estimate Pareto Params Interval");
        return _ERROR_WRONG_INPUT_DATA_;
    }
    double mean = 0.0f;
    double var  = 0.0f;
    size_t counter =0;
    for(size_t i=0; i < m_data.size(); ++i)
    {
        if(!isNAN_double(m_data[i]))
        {
            ++counter;
            mean +=m_data[i];
        }
    }
    if( counter <= 1)
    {
        return _ERROR_WRONG_INPUT_DATA_;
    }
    mean /= counter;
    for(size_t i=0; i < m_data.size(); ++i)
    {
        if(!isNAN_double(m_data[i]))
        {
            var += (m_data[i] - mean)*(m_data[i] - mean);
        }
    }
    double confidenceRadius=0.0f;
    int rc = m_estimatorParams->getParam(confidenceRadiusAsTotalOfSDs, confidenceRadius);
    if((rc) || (confidenceRadius < DBL_MIN))
    {
        m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": No confidenceRadiusAsTotalOfSDs!");
        return _ERROR_WRONG_ESTIMATOR_PARAMS_;
    }
    double sd = sqrt(var/(counter-1));
    double left  = mean - sd * confidenceRadius;
    double right = mean + sd * confidenceRadius;
    m_leftBound->setParam(0,left);
    m_rightBound->setParam(0,right);
#ifdef _DEBUG_
m_rightBound->getParam(0,right);
m_leftBound->getParam(0,left);
std::cout << "left " << left << std::endl;
std::cout << "right " << right << std::endl;
#endif
    return _RC_SUCCESS_;
}

#ifndef __WIENERINTERVALESTIMATOR_H__633569422542968750
#define __WIENERINTERVALESTIMATOR_H__633569422542968750

#include <vector>
#include "../../Params/ParamsSet.h"
#include "../Estimator.h"
#include "FakeIntervalEstimator.h"
///TODO!!!! ��������� � ������-���� ������, ������������ IParams::paramsNames[WienerParams] ??? + ���������� ��� ���������???
#define WIENER_INTERVALESTIMATOR_DATASETNAME "WienerParams";
/** forward declaration */
class IParamsWiener;
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */

namespace IntervalEstimators{
   /** class container */
/** implementation of abstract Base class IIntervalEstimator */
class WienerSigmaIntervalEstimator : public virtual IIntervalEstimator,
/**
 Basic methods setTask, setLog, toString are inherited from FakeIntervalEstimator
*/
                                public virtual FakeIntervalEstimator
{
public:
    WienerSigmaIntervalEstimator(Logger*& log);
    ~WienerSigmaIntervalEstimator();
    /** IEstimator stuff */
    using FakeIntervalEstimator::setTask;
    using FakeIntervalEstimator::setLog;
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo);
    /** ����� ������, ����������� ��� ������� ���������! */
    using FakeIntervalEstimator::toString;

    /** generate DataInfo */
    virtual int getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const;

    /** IIntervalEstimator stuff */
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * log);
    virtual int setEstimatorParams(IParams*& params);
    virtual int setModel(IModel* model);

    virtual int estimateInterval(ptrInt threadInterruptSignal);

protected:
    using FakeIntervalEstimator::logErrMsgAndReturn;
    using FakeIntervalEstimator::parsingFailed;

private:
///� ����� ������ ������ �����������
///��� ��������������� �� �������� ����� ����������� ParamsSet ��� ParamsCompact!
///����� ��� ���� �� � �������, � ���������!
/// ��� ����� ������ ���������� ��� � ������� ��� ������� ��� ������������� ParamsSet ��� ParamsCompact
/// � ����� �� ��� ������ ���� ��������� ParamsSet ��� ParamsCompact
    virtual int setParamsSet();
    /** assign and copy CTORs are disabled */
    WienerSigmaIntervalEstimator(const WienerSigmaIntervalEstimator&);
    void operator=(const WienerSigmaIntervalEstimator&);
/** enum PointEstimatorsTypes m_pet; has been Moved to FakePointEstimator */
/** Task* m_task; has been Moved to FakePintEstimator */
/** Logger* m_logger; has been Moved to FakePointEstimator */
    IModel* m_model;
    /** here we get estimates, which are produced by estimator */
    IParamsSet * m_setOfParamsEstimates;
    /** ptr to first IParams in IParamsSet _setOfParamsEstimates */
    IParamsWiener* m_leftBound;
    /** ptr to second IParams in IParamsSet _setOfParamsEstimates */
    IParamsWiener* m_rightBound;

    IParams* m_estimatorParams;

///TODO!!! ��� ���� � �����, � ������� ����� setData()???
    std::vector<double> m_data;
};

} /// end namespace IntervalEstimators
#endif

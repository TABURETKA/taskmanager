#include "../../Log/Log.h"
#include "../../Log/errors.h"
#include "FakeIntervalEstimator.h"
/** NON-anonymous namespace, to expose implementations from corresponding cpp-file */
using namespace IntervalEstimators;

IntervalEstimators::FakeIntervalEstimator::FakeIntervalEstimator(Logger*& log)
{
 /** so far noting to allocate */
  m_iet = IntervalEstimatorsTypes::FakeIntervalEstimator;
  m_task = NULL;
  m_logger  = log;
//  m_model  = NULL;
//  _pst  = NULL;
//  _estimatorParams  = NULL;


}
IntervalEstimators::FakeIntervalEstimator::~FakeIntervalEstimator()
{
 /** so far noting to delete */
}
    /** IEstimator stuff */
///TODO!!! ������������ � setTask, � setLog!
void IntervalEstimators::FakeIntervalEstimator::setTask(Task* const & task)
{
      m_task = task;
}

void IntervalEstimators::FakeIntervalEstimator::setLog(Logger* const & log)
{
      m_logger = log;
}

int IntervalEstimators::FakeIntervalEstimator::logErrMsgAndReturn(int rc, std::string msg) const
{
    m_logger->error(ErrorLevels::ALGORITHM_ESTIMATOR_ERROR, std::string(intervalEstimatorNames[m_iet]) + ": " + msg);
    return _ERROR_WRONG_WORKFLOW_;
}
    /** ����� ������, ����������� ��� ������� ���������! */
std::string IntervalEstimators::FakeIntervalEstimator::toString() const
{
  return (std::string) intervalEstimatorNames[m_iet];
}

    /** generate DataInfo */
int IntervalEstimators::FakeIntervalEstimator::getEstimatesAsDataInfoArray(size_t &, DataInfo** &) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " getEstimatesAsDataInfoArray is not implemented yet!");
}

    /** IPointEstimator stuff */
int IntervalEstimators::FakeIntervalEstimator::parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger* log)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parseAndCreateEstimatorParams is not implemented yet!");
}
int IntervalEstimators::FakeIntervalEstimator::setEstimatorParams(IParams*& params)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}
int IntervalEstimators::FakeIntervalEstimator::setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setEstimatorParams is not implemented yet!");
}

int IntervalEstimators::FakeIntervalEstimator::setModel(IModel* model)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setModel is not implemented yet!");
}

///TODO!!! ��� ���� � setData()???
/** No need so far
int IntervalEstimators::FakeIntervalEstimator::setParamsCompact(IParamsCompact * pst)
{
///TODO!!!
    m_logger->log(ALGORITHM_1_LEVEL_INFO, "FakeIntervalEstimator::setParamsCompact is not implemented yet!");
}
*/
int IntervalEstimators::FakeIntervalEstimator::setParamsSet()
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setParamsSet is not implemented yet!");
}
int IntervalEstimators::FakeIntervalEstimator::estimateInterval(ptrInt threadInterruptSignal)
{
///TODO!!! ???
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " estimateInterval is not implemented yet!");
}

int IntervalEstimators::FakeIntervalEstimator::parsingFailed(std::string msg) const
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " parsingFailed is not implemented yet!");
}

int IntervalEstimators::FakeIntervalEstimator::setData(size_t dimension, const DataInfo** ptrToPtrDataInfo)
{
///TODO!!!
    return logErrMsgAndReturn(_ERROR_WRONG_WORKFLOW_, " setData is not implemented yet!");
}

#ifndef _ESTIMATOR_H__633569422542968750
#define _ESTIMATOR_H__633569422542968750
#include <string> /* std::string */
#include <map>    /* std::map */
#define ptrInt int*

// DON'T pollute global scope! using namespace std;
/** return codes for estimators */
enum RC_Estimator{
  THREAD_EXECUTION_INTERRUPTED=-1,
  THREAD_EXECUTION_ACCOMPLISHED=0
};


/** So far TWO TYPES of Estimators */
enum EstimatorsTypes{
  SimpleEstimator,
  PointEstimator,
  IntervalEstimator,
  FAKE_ESTIMATOR_TYPE
};

/** Available TYPES of Simple Estimators */
enum SimpleEstimatorsTypes{
  FakeSimpleEstimator,
  ModifiedExpRegressionEstimator,
  PW_PolynomialRegressionEstimatorForGivenSubsets,
  PW_PolynomialRegressionEstimator, /// new
  ScalarArithmeticWithDatasetEstimator,
  FAKE_SIMPLE_ESTIMATOR
};

enum SimpleEstimatorsTypes getSimpleEstimatorTypeBySimpleEstimatorName(const std::string& simpleEstimatorName);

/** Available TYPES of Point Estimators */
enum PointEstimatorsTypes{
  FakePointEstimator,
  FirstDerivativePointEstimator,
  LinearTransformationPointEstimator, /// new
  GaussianOutliersByQuantilePointEstimator,
  PF_AdditiveModel_PointEstimator,
  PF_StatOffcast_PointEstimator,
  FAKE_POINT_ESTIMATOR
};

enum PointEstimatorsTypes getPointEstimatorTypeByPointEstimatorName(const std::string& pointEstimatorName);

enum IntervalEstimateIndexes{
    LeftBoundary=0,
    RightBoundary=1,
    TOTAL_OF_INTERVAL_ESTIMATE_INDEXES=2
};
/** Available TYPES of Interval Estimators */
enum IntervalEstimatorsTypes{
  FakeIntervalEstimator,
  SampleMeanIntervalEstimator,
  PoissonLambdaIntervalEstimator,
  ParetoIntervalEstimator,
  WienerSigmaIntervalEstimator,
  WienerInitialConditionIntervalEstimatorGivenTrajSubsetAndSigmaIntervalEstimates,
  MLE_IntervalEstimator,
  PolyRegressionIntervalEstimator,
  FAKE_INTERVAL_ESTIMATOR
};

enum IntervalEstimatorsTypes getIntervalEstimatorTypeByIntervalEstimatorName(const std::string& intervalEstimatorName);


enum indexesOfGrammarAdditiveModelEstimatorConfig{
  ADDITITIVE_MODEL_ADDENS_REPO_ENTRIES_ITEM,
  GrammarSizeForAdditiveModelEstimatorConfig
};

/** forward declarations */
class Task;
class IParams;
class IModel;
class Logger;
class IParamsSet;
class IParamsCompact;
class IParamsUnionSet;
class IParamsUnionCompact;

/** forward declarations */
typedef struct tagDataInfo DataInfo;

/** abstract Base class - interface */
class IEstimator
{
public:
/* OLD C++ < C++11     static const char * estimatorTypesNames[FAKE_ESTIMATOR_TYPE]; */
    static constexpr const char * estimatorTypesNames[FAKE_ESTIMATOR_TYPE]=
    {
        "SIMPLEESTIMATOR",
        "POINTESTIMATOR",
        "INTERVALESTIMATOR"
    };
/** � ����� ������ ������ - factory,
 ����� ������, ����� ������� ��������� ����������.
 ���� ������-��������� ����� ������� �����, � ���������� IEstimator, ����� �� ������ �������������� �������� ��������
 � ������ ���������� ���������-������ ����� ���� ������ ���� ������,
 ����� ����� ����� �������������� ���������� ��� ���������
 ����������� ������ ������
*/
    /** log is mandatory! Any Estimator MUST HAVE ptr to Logger */
    static IEstimator* createEstimator(const enum EstimatorsTypes estimatorType, const std::string& estimatorName, Logger*& log);
    /** runEstimator - static method to control start of any estimator!
    actually wrapper to call estimatePoint for Point Estimator instance
    or to call estimateInterval for Interval Estimator instance
     */
    static int runEstimator(IEstimator*& est, ptrInt ptrInterrupt);
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class!
    */
    IEstimator(){};
    virtual ~IEstimator() {}
///TODO!!! ����� ��������� setTask(), setLog() � protected??? �.�. ���������������� ����� CTOR?
    virtual void setTask(Task* const & task) =0;
    virtual void setLog(Logger* const & log) =0;
    /** each estimator MAY get COPY of input data from repository
    OR it may work with CONST original input data! (in array ptrToPtrDataInfo[])
    Estimator decides which way to proceed with input data.
    */
    /** NB const DataInfo***
     Because ACTUALLY we MUST keep unchanged the array of double
     referenced by ptrToPtrDataInfo[i]->m_data!
     DON'T DELETE IT!
     BTW you may ASSIGN NULL to ptrToPtrDataInfo[i]->m_data
     */
    /** ��������� ��� ������ ��������� � ����������, �������������, ������ ���� �����������
    �� ���� �����������, ��������� ������ ��������� ������ ������ ������ �������� � ������-��
    ��������� ������� � ��� ����� ���������������.
    �.�. � �������� ������, � ��������� ������ ���������� ��� ����� ��������� ������� (Task ���������� ����),
    ��������� �������� ����������� ��� ����� � �����������.
    ��� ��� ����, ����� Task ��� ����������� ��� Dependencies Injections (��������� ������ � ���������),
    ����� ����� � ������ ���������� ������� ���������� ����� ��� ������.
     */
    virtual int setData(size_t dimension, const DataInfo** ptrToPtrDataInfo) = 0;
    virtual int setEstimatorParams(size_t dimension, const DataInfo** ptrToPtrDataInfo) = 0;

    virtual std::string toString() const = 0;
    /** get estimates evaluated by the Estimator as DataInfo Array
    */
    virtual int getEstimatesAsDataInfoArray(size_t& dimension, DataInfo** &) const = 0;

/** ��� ������ ����������� ��� ������� ���������!
    ������� ���
    ����� ���� ������� ������ ���� ��������� ���������� ��� ������!

///TODO!!!    virtual IParams* createEmptyParamsOfEstimator() const =0;
///TODO!!! � ��� �� ����� �������������� ������ ��� ���������� � ���������� ����������?
/// ���� ������, �� ����� ������ �������� enum EstimatorsTypes estimatorType,
///TODO!!!   virtual IParams* parseAndCreateEstimatorParams(map<std::string, std::string>& mapParams, std::string postfix) const =0;
*/
    /** ��� ��� ������ ����� ����������� � ������� ������! */
/** meantime IAdditiveModel is derived from IModel,
    so, we don't have any problem with model!
    BUT SO FAR model may require IParams or  IAdditiveModelParams (array of params)!
    That's why we have to deffer the declaration of modelParams
    as FAR AS POSSIBLE! To the Estimators implementation!

    How can we descriminate at INTERFACE LEVEL
    between models which deal with IParams
    and those deal with IAdditiveModelParams?????
��������� ������ �� ����� � ���������� ������, ������� ��� ��������!
��� �� ��� ������ ����������, �� �� ��� ���������? ��� ���?
������ ����� ��������� �� ��� ��������� ����� ������,����� ��� ���� ������������!
�����, ����� ��� ����� Task! � ���������� � ��� ������ ����������� �� ������!
���������� ������ ������ ������� ������,
���������� �� �������� ������ � ������� �� ��� ���������!
*/
    virtual int parseAndCreateEstimatorParams(std::map<std::string, std::string>& mapParams, std::string postfix, size_t& size, IParams** ptrToPtrIParams, Logger * log) =0;
    virtual int setEstimatorParams(IParams*& params) =0;

    virtual int setModel(IModel* model) =0;

protected:
    virtual int parsingFailed(std::string msg) const =0;

private:
    /** assign and copy CTORs are disabled */
    IEstimator(const IEstimator&);
    void operator=(const IEstimator&);
};
/**
��������� ���������� ��� public ��������� IEstimator!
� � ��� ������ ����� ���� ����������� ParamSetCompact ��� ArrayOfParamsCompact!
��� ����� ������ setParamsCompact, setParamsCompact(ParamSetCompact *), setParamsCompact(ArrayOfParamsCompact *)
� ������ ���������� ����������� ����� ��� � �� �����
��� ��� ���������� ����� ����, �������� ����� ������� ������ ������������
� ��� �� �� �����������������
��� ����� ������ � ParamSet, ParamCompact ������ ��� � Task!
�� �������� ������ �����������, �� ������� ���� ��� ���������� �������, ����� �������������������  � ������� ������!
������ ��� ������ ������ �����, ��� ��������� ������� ��� �� ����������!
*/

/** abstract Base class - interface */
class ISimpleEstimator : public virtual IEstimator
{
public:
    /** tokens to name Simple Estimators */
/* OLD C++ < C++11    static const char * simpleEstimatorNames[FAKE_SIMPLE_ESTIMATOR]; */
    static constexpr const char * simpleEstimatorNames[FAKE_SIMPLE_ESTIMATOR]=
    {
        "FAKESIMPLEESTIMATOR",
        "ModifiedExpRegressionEstimator",
        "PW_PolynomialRegressionEstimatorForGivenSubsets",
        "PW_PolynomialRegressionEstimator", /// new
        "ScalarArithmeticWithDatasetEstimator"
    };
    static const size_t grammarSizeForSimpleEstimates = 1;
/** tokens to parse map for Simple Estimates */
    static constexpr const char* grammarSimpleEstimates[grammarSizeForSimpleEstimates]=
    {
        "SimpleEstimates"
    };
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class!
    */
    ISimpleEstimator(){};
    static IEstimator* createSimpleEstimator(const enum SimpleEstimatorsTypes estimatorType, Logger*& log);
    virtual int estimate(ptrInt threadInterruptSignal) =0;
private:
    /** here we set pointer, reference to container for estimates, which will be produced by estimator */
/** � ����� ������ ������ �����������
 ��� ��������������� �� �������� ����� ����������� ParamsSet ��� ParamsCompact!
 ����� ��� ���� �� � �������, � ���������!
 ��� ����� ������ ���������� ��� � ������� ��� ������� ��� ������������� ParamsSet ��� ParamsCompact
 � ����� �� ��� ������ ���� ��������� ParamsSet ��� ParamsCompact
*/
//OLD??? USe ParamsSet insteadof ParamsCompact to save point estimates???
//    Mandatory initialization into each estimator's setModel()!
//    virtual int setParamsCompact() =0;
    /**
     Mandatory initialization into each estimator's setModel()!
    */
///TODO!!! REDUNDANT HERE in Interface declaration!
    virtual int setParamsSet() =0;
    /** assign and copy CTORs are disabled */
    ISimpleEstimator(const ISimpleEstimator&);
    void operator=(const ISimpleEstimator&);
};

/** abstract Base class - interface */
class IPointEstimator : public virtual IEstimator
{
public:
    /** tokens to name Point Estimators */
    static constexpr const char * pointEstimatorNames[FAKE_POINT_ESTIMATOR]=
    {
        "FAKEPOINTESTIMATOR",
        "FirstDerivativePointEstimator",
        "LinearTransformationPointEstimator", /// new
        "GaussianOutliersByQuantilePointEstimator",
        "PF_ADDITIVE_MODEL_POINT_ESTIMATOR",
        "PF_STATOFFCAST"
    };

    static const size_t grammarSizeForPointEstimates = 1;
/** tokens to parse map for Point Estimates */
/* OLD C++ < C++11    static const char* grammarPointEstimates[grammarSizeForPointEstimates]; */
    static constexpr const char* grammarPointEstimates[grammarSizeForPointEstimates]=
    {
        "PointEstimates"
    };
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class!
    */
    IPointEstimator(){};
    static IEstimator* createPointEstimator(const enum PointEstimatorsTypes estimatorType, Logger*& log);
    virtual int estimatePoint(ptrInt threadInterruptSignal) =0;
private:
    /** here we set pointer, reference to container for estimates, which will be produced by estimator */
/** � ����� ������ ������ �����������
 ��� ��������������� �� �������� ����� ����������� ParamsSet ��� ParamsCompact!
 ����� ��� ���� �� � �������, � ���������!
 ��� ����� ������ ���������� ��� � ������� ��� ������� ��� ������������� ParamsSet ��� ParamsCompact
 � ����� �� ��� ������ ���� ��������� ParamsSet ��� ParamsCompact
*/
//OLD??? USe ParamsSet insteadof ParamsCompact to save point estimates???
//    Mandatory initialization into each estimator's setModel()!
//    virtual int setParamsCompact() =0;
    /**
     Mandatory initialization into each estimator's setModel()!
    */
///TODO!!! REDUNDANT HERE in Interface declaration!
    virtual int setParamsSet() =0;
    /** assign and copy CTORs are disabled */
    IPointEstimator(const IPointEstimator&);
    void operator=(const IPointEstimator&);
};

/** abstract Base class - interface */
class IIntervalEstimator : public virtual IEstimator
{
public:
/** tokens to name Interval Estimators */
/* OLD C++ < C++11 extern const char * intervalEstimatorNames[FAKE_INTERVAL_ESTIMATOR]; */
    static constexpr const char * intervalEstimatorNames[FAKE_INTERVAL_ESTIMATOR]=
    {
        "FAKEINTERVALESTIMATOR",
        "SampleMeanIntervalEstimator",
        "IntervalPoissonLambda",
        "IntervalPareto",
        "IntervalWienerSigma",
        "IntervalWienerInitialConditionGivenTrajSubsetAndSigmaIntervalEstimates",
        "MLE",
        "POLYREGRESSION"
    };
    static const size_t grammarSizeForIntervalEstimates = TOTAL_OF_INTERVAL_ESTIMATE_INDEXES;
    /** tokens to parse map for Interval Estimates */
/* OLD C++ < C++11    static const char*  grammarIntervalEstimates[grammarSizeForIntervalEstimates]; */

    static constexpr const char* grammarIntervalEstimates[grammarSizeForIntervalEstimates]=
    {
        "IntervalEstimatesLeft",
        "IntervalEstimatesRight"
    };
    /** Public CTOR merely to calm down compiler!
    IT DOES NOT matter! It's pure abstract class!
    */
    IIntervalEstimator(){};
    static IEstimator* createIntervalEstimator(const enum IntervalEstimatorsTypes estimatorType, Logger*& log);
    virtual int estimateInterval(ptrInt threadInterruptSignal) = 0;
private:
    /** here we set pointer, reference to container for estimates, which will be produced by estimator */
/** � ����� ������ ������ �����������
 ��� ��������������� �� �������� ����� ����������� ParamsSet ��� ParamsCompact!
 ����� ��� ���� �� � �������, � ���������!
 ��� ����� ������ ���������� ��� � ������� ��� ������� ��� ������������� ParamsSet ��� ParamsCompact
 � ����� �� ��� ������ ���� ��������� ParamsSet ��� ParamsCompact
*/
    /**
     Mandatory initialization into each estimator's setModel()!
    */
///TODO!!! REDUNDANT HERE in Interface declaration!
    virtual int setParamsSet() =0;
    /** assign and copy CTORs are disabled */
    IIntervalEstimator(const IIntervalEstimator&);
    void operator=(const IIntervalEstimator&);

};

#endif

#ifndef _ADDITIVE_MODEL_ESTIMATOR_H__633569422542968750
#define _ADDITIVE_MODEL_ESTIMATOR_H__633569422542968750
#include <string> // std::string

#include "Estimator.h" // std::string

/** forward dclaration */
class IAdditiveModel;
/** abstract Base class - interface */
/**
��� ���������� ������� �������� ������� ��������� ��������� ���������� IAdditiveModelEstimator
  ������� ���� ������ � ���� ������, ������� ����� ���������� ����������.
  ���������� ������ IAdditiveModel* ����� �� �������� � ���� IModel*,
  ������� ������� ������������ ����� setModel(IModel* model) � IEstimator-����������!
  �� ����� ������������ ������ � ���� (IEstimator), � ������� (IAdditiveModelEstimator)
  �����-���� ������� �� ����� ������������ ����� �� ���.
*/
class IAdditiveModelEstimator : public virtual IEstimator
{
public:
/* OLD C++ < C++11    static const char* grammarAdditiveModelEstimatorConfig[GrammarSizeForAdditiveModelEstimatorConfig]; */
    static constexpr const char* grammarAdditiveModelEstimatorConfig[GrammarSizeForAdditiveModelEstimatorConfig]={
        "ADDITITIVE_MODEL_ADDENS_REPO_ENTRIES_ITEM"
    };
    virtual int setModel(IAdditiveModel* model) =0;

};
#endif

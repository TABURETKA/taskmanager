#include "robust_estimator.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

RobustEstimator::RobustEstimator(const vector<double>& track, Logger* log) :
track(track), logger(log)
{}

void RobustEstimator::estimateBoundaries()
{
	EvalFirstDifferences();
	DeviationAndSpikes();

	StartPoint();
	Intensity();
	Regression();
}

void RobustEstimator::fillParamsBoundaries(PF_Params* start, PF_Params* end)
{
	start->noiseParams->setStartPoint(startMin);
	start->noiseParams->setDeviation(devMin);
	start->processParams->setIntesity(intensMin);
	start->processParams->setRegression(regrMin);

	end->noiseParams->setStartPoint(startMax);
	end->noiseParams->setDeviation(devMax);
	end->processParams->setIntesity(intensMax);
	end->processParams->setRegression(regrMax);
}

vector<double> RobustEstimator::getSpikes()
{
	return jump;
}

double RobustEstimator::getMinDeviation()
{
	return devMin;
}

double RobustEstimator::getMaxRegression()
{
	return regrMax;
}

// Finite differencial fuction
void RobustEstimator::EvalFirstDifferences()
{
    def_loading.resize(track.size() - 1);
    abs_def_loading.resize(track.size() - 1);

    for (unsigned i = 0; i < track.size() - 1; i++) {
        def_loading[i] = track[i + 1] - track[i];
        abs_def_loading[i] = fabs(def_loading[i]);
    }

}

// Identify the time of process jump
void RobustEstimator::DeviationAndSpikes()
{
	sort(abs_def_loading.begin(), abs_def_loading.end());
	double th = QuantilThreshold(abs_def_loading);

	stringstream logStr;
	logStr << "Theshold: " << th;
	logger->log(5, logStr.str());

	double sum_dev_min = 0.;
	int num_dev = 0;
	devMax = 0;

	for (unsigned i = 0; i < def_loading.size(); ++i) {
		if (def_loading[i] > th) {
			jump_time.push_back(i + 1);
			jump.push_back(def_loading[i]);
		}

		if (abs_def_loading[i] < th) {
			sum_dev_min += abs_def_loading[i] * abs_def_loading[i];
			num_dev++;
			devMax = max(devMax, abs_def_loading[i]);
		}
	}
	devMin = sqrt(sum_dev_min / (double)num_dev);

	unsigned num_app = jump_time.size();

	stringstream appLog;
	appLog << "Estimated applications time:\n";
	for (unsigned i = 0; i < num_app; ++i) {
		appLog << jump_time[i] << " " << jump[i] << std::endl;
  }
	logger->log(5, appLog.str());
}

void RobustEstimator::StartPoint()
{
	startMin = track[0] - devMax;
	startMax = track[0];

	for (unsigned i = 0; i < jump_time.size(); i++)
	{
		double point = track[jump_time[i] - 1];

		if (startMin > point)
			startMin = point;
		if (startMax < point)
			startMax = point;
	}
	// startPoint is similar or smaller track[0]
	if (startMax > track[0] + devMax) {
		startMax = track[0] + devMax;
	}
}

// Find the intense of the application commings
void RobustEstimator::Intensity()
{
	if (jump_time.size() == 0) {
		intensMin = 0.5 / (double)track.size();
		intensMax = 1. / (double)track.size();
		return;
	}

	if (jump_time.size() == 1) {
			intensMax = 0.5 / (double)track.size();
			intensMin = 1. / (double)jump_time[0];
	}

	double time_avg = (double)(jump_time[jump_time.size() - 1] - jump_time[0]) / (double)(jump_time.size() - 1);
	// Add one more points
	double time_added_avg1 = (double)track.size() / (double)(jump_time.size() - 1);
	// Add first and last time points
	double time_added_avg2 = (double)track.size() / (double)(jump_time.size() + 1);

	double lam_estimation = 1. / time_avg;
	double lam_dev_estimation1 = 1. / time_added_avg1;
	double lam_dev_estimation2 = 1. / time_added_avg2;

    if (lam_estimation > lam_dev_estimation2) {
        intensMax = lam_estimation;
        intensMin = lam_dev_estimation1;
    }
    else if (lam_estimation < lam_dev_estimation1) {
        intensMax = lam_dev_estimation2;
        intensMin = lam_estimation;
    }
    else {
        intensMax = lam_dev_estimation1;
        intensMin = lam_dev_estimation2;
    }
}

// Find the interval for intense of serving the application
void RobustEstimator::Regression()
{
    logger->log(10, "Find regression coefficient");
	LS_ExpRegression();
	RegressionBoundaries();
}

void RobustEstimator::RegressionBoundaries()
{
	if (regressionCoef.size() == 0) {
		regrMin = 0.;
		regrMax = 1.;
		return;
	}
	if (regressionCoef.size() == 1) {
		regrMin = 0.1 * regressionCoef[0];
		regrMax = 10. * regressionCoef[0];
		return;
	}
	regrMin = regressionCoef[0];
	regrMax = regressionCoef[0];

	for (unsigned i = 1; i < regressionCoef.size(); i++) {
		if (regrMax < regressionCoef[i]) {
			regrMax = regressionCoef[i];
		}
		if (regrMin > regressionCoef[i]) {
			regrMin = regressionCoef[i];
		}
	}
}

// Find the intenses of serving the application by lest squares method
void RobustEstimator::LS_ExpRegression()
{
	jump_time.push_back(track.size());

	double maxCoef0 = abs_def_loading[abs_def_loading.size() - 1] + devMax;
	stringstream ss;
	ss << "Max coef 0: " << maxCoef0;
	logger->log(10, ss.str());

    unsigned i_start, i_end;
	for (unsigned i = 0; i < jump_time.size() + 1; ++i)
	{
	    if (i == 0) {
            i_start = 0;
            i_end = jump_time[i];
	    }
        else if (i == jump_time.size()) {
            i_start = jump_time[i];
            i_end = track.size();
        }
        else {
            i_start = jump_time[i - 1];
            i_end = jump_time[i];
        }
		std::vector<double> coef = LeastQuatersExp(i_start, i_end, track[i_end - 1]);
		// Lam_c
		if (coef[1] < 0 && coef[0] <= maxCoef0) {
            regressionCoef.push_back(-coef[1]);
		}
		else {
            logger->log(10, "bad coefficients\n");
		}

		coef = LeastQuatersExp(i_start, i_end, track[i_end - 1] - devMax );
		if (coef[1] < 0 && coef[0] <= maxCoef0) {
            regressionCoef.push_back(-coef[1]);
		}
		else {
            logger->log(10, "bad coefficients\n");
		}
	}

	jump_time.pop_back();
}


std::vector<double> RobustEstimator::LeastSquares4PolynomialRegression(unsigned i_beg, unsigned i_end)
{
	std::vector<double> coef(PolynomialDim);
	int i, j, k, row, col;
	std::vector< std::vector<double> > matrix;
	std::vector<double> x_i;
	matrix.assign(PolynomialDim, std::vector<double>(PolynomialDim, 0.));

	int totalOfInputParams = i_end - i_beg;
	x_i.assign(totalOfInputParams, 1.);

	double sum = 0.;
	for (i = 0; i < totalOfInputParams; ++i)
		sum += x_i[i];

	row = 0;
	col = 0;

	/** matrix_{0,0}=dim */
	matrix[row][col] = sum;
	for (j = 1; j < PolynomialDim; ++j)
	{
		sum = 0;
		for (k = 0; k < totalOfInputParams; ++k)
		{
			x_i[k] = x_i[k] * (k + 1);
			sum += x_i[k];
		}
		row = 0;
		col = j;
		while ((row < PolynomialDim) && (col >= 0))
		{
			matrix[row][col] = sum;
			row++;
			col--;
		}
	}

	for (i = 1; i < PolynomialDim; ++i)
	{
		sum = 0;
		for (k = 0; k < totalOfInputParams; ++k)
		{
			x_i[k] = x_i[k] * (k + 1);
			sum += x_i[k];
		}
		col = PolynomialDim - 1;
		row = i;
		while ((row < PolynomialDim) && (col >= 0))
		{
			matrix[row][col] = sum;
			row++;
			col--;
		}
	}

	for (i = 0; i < PolynomialDim; ++i)
	{
		coef[i] = 0.;
		for (j = (int)i_beg; j < (int)i_end; ++j)
		{
			double multiplier = 1.;
			/** j^{i} */
			for (k = 0; k < i; k++)
				multiplier *= (j + 1);
			coef[i] += track[j] * multiplier;
		}
	}

	Gauss(matrix, coef);

	for (k = 0; k < PolynomialDim; ++k)
	{
		if (isnan_double(coef[k]))
		{
			for (i = 0; i < (int)PolynomialDim; i++)
			{
				for (j = 0; j < (int)PolynomialDim; j++)
				{
					matrix[i].clear();
				}
			}
			matrix.clear();
			coef.clear();
			x_i.clear();
			return coef;
		}
	}

	for (i = 0; i < PolynomialDim; i++)
	{
		for (j = 0; j < PolynomialDim; j++)
		{
			matrix[i].clear();
		}
	}
	matrix.clear();
	x_i.clear();

	return coef;
}

// Exponential least quaters method for a part of a process [i_beg:i_end]
std::vector<double> RobustEstimator::LeastQuatersToProc(unsigned i_beg, unsigned i_end)
{
    std::vector<std::vector<double> > matrix(PolynomialDim + 1, std::vector<double>(PolynomialDim + 1, 0));
    std::vector<double> coef(PolynomialDim + 1, 0);
  std::vector<double> x_i(i_end - i_beg, 1.);
  double elem;

  int j;
  for (int i = 0; i < PolynomialDim + 1; ++i)
  {
    elem = 0;
    for (j = 0; j < (int)(i_end - i_beg); ++j)
      elem += x_i[j];
    for (j = i; j >= 0; --j)
      matrix[i - j][j] = elem;

    for (j = 0; j < (int)(i_end - i_beg); ++j)
    {
      coef[i] += track[i_beg + j] * x_i[j];
      x_i[j] *= (double)j;
    }
  }

  for (int i = 1; i < PolynomialDim + 1; ++i)
  {
    elem = 0;
    for (j = 0; j < (int)(i_end - i_beg); ++j)
      elem += x_i[j];
    for (j = 0; j < PolynomialDim + 1 - i; ++j)
      matrix[j + i][PolynomialDim - j] = elem;

    for (j = 0; j < (int)(i_end - i_beg); ++j)
    {
      x_i[j] *= (double)j;
    }
  }

  Gauss(matrix, coef);

  return coef;
}

// Exponential least quaters method
std::vector<double> RobustEstimator::LeastQuatersExp(unsigned i_beg, unsigned i_end, double floor)
{
	std::vector<std::vector<double> > matrix(MaxN + 1, std::vector<double>(MaxN + 1, 0));
	std::vector<double> coef(MaxN + 1, 0.);

	int size = i_end - i_beg;
	int dataSize = 0;
	std::vector<double> data(size, 0);

//    stringstream ss1, ss2;
  //  ss1 << "Data:  ";
    //ss2 << "Row:  ";
    for (unsigned i = 0; i < size; i++) {
        data[i] = track[i_beg + i] - floor;
      //  ss1 << data[i] << " ";
        if (data[i] > 0) {
            dataSize++;
        //    ss2 << data[i] << " ";
        }
    }
    //logger->log(10, ss1.str());
    //logger->log(10, ss2.str());

	if (dataSize < 3) {
        coef[1] = 1;
        return coef;
	}

    std::vector<double> x_i(dataSize, 1.);
    double elem;

    int j;
	for (int i = 0; i < MaxN + 1; ++i)
    {
        elem = 0;
        for (j = 0; j < dataSize; ++j)
          elem += x_i[j];
        for (j = i; j >= 0; --j)
          matrix[i - j][j] = elem;

        unsigned k = 0;
        for (j = 0; j < size; ++j)
        {
            if (data[j] > 0) {
                coef[i] += log(data[j]) * x_i[k];
                x_i[k] *= (double)j;
                k++;
            }
        }
    }

	for (int i = 1; i < MaxN + 1; ++i)
    {
        elem = 0;
        for (j = 0; j < dataSize; ++j)
          elem += x_i[j];
        for (j = 0; j < MaxN + 1 - i; ++j)
            matrix[j + i][MaxN - j] = elem;

        for (j = 0; j < dataSize; ++j)
        {
          x_i[j] *= (double)j;
        }
    }

    Gauss(matrix, coef);

    coef[0] = exp(coef[0]);

    stringstream ss3;
    ss3 << "Trend:  " << coef[0] << " * exp( " << coef[1] << " )";
    logger->log(10, ss3.str());

	return coef;
}

// Evaluate process by coefficients
double RobustEstimator::EvalPolynom(std::vector<double> coef, unsigned x_i)
{
  double x = (double)x_i;
  double y = 0;

	for (int i = PolynomialDim; i >= 0; --i)
  {
    y *= x;
    y += coef[i];
  }
  return y;
}

// Evaluate process by coefficients
double RobustEstimator::EvalPolynom(std::vector<double> coef, double x)
{
  double y = 0;

	for (int i = PolynomialDim; i >= 0; --i)
  {
    y *= x;
    y += coef[i];
  }
  return y;
}

// Evaluate differential of a process by coefficients
double RobustEstimator::EvalDifPolynom(std::vector<double> coef, unsigned x_i)
{
  double x = (double)x_i;
  double y = 0;

	for (int i = PolynomialDim; i > 0; --i)
  {
    y *= x;
    y += coef[i] * (double)(PolynomialDim - i);
  }
  return y;
}

// Evaluate differential of a process by coefficients
double RobustEstimator::EvalDifPolynom(std::vector<double> coef, double x)
{
  double y = 0;

	for (int i = PolynomialDim; i > 0; --i)
  {
    y *= x;
    y += coef[i] * (double)(PolynomialDim - i);
  }
  return y;
}

// Matrix equation resolving
void RobustEstimator::Gauss(std::vector<std::vector<double> >& matrix, std::vector<double>& y)
{
	int i, j, k;

	// straight
	for (i = 0; i < MaxN; ++i) {
		for (j = i + 1; j < MaxN + 1; ++j) {
			for (k = i + 1; k < MaxN + 1; ++k) {
				matrix[j][k] -= matrix[i][k] * matrix[j][i] / matrix[i][i];
			}
			y[j] -= y[i] * matrix[j][i] / matrix[i][i];
			matrix[j][i] = 0;
		}
	}

	// backward
	for (i = MaxN; i > 0; --i) {
		for (j = i - 1; j >= 0; --j) {
			y[j] -= y[i] * matrix[j][i] / matrix[i][i];
			matrix[j][i] = 0;
		}
	}

	for (i = 0; i < MaxN + 1; ++i) {
		y[i] /= matrix[i][i];
		matrix[i][i] = 1.;
	}
}

bool RobustEstimator::isnan_double(double x) { return (x != x); }

double RobustEstimator::QuantilThreshold(vector<double> samples)
{
	int n = samples.size();

	return samples[n / 2] * 3.;
}

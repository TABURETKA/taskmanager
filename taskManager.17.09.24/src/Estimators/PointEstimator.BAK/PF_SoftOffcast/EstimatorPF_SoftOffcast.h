#pragma once

#include "../../EstimatorFactory.h"


class EstimatorPF_SoftOffcast : public PointEstimator
{
public:
    int estimatePoint(ptrInt pSignal,
                   vector<double> track,
                   PF_Model* model,
                   ParamSet* paramSet, PF_Params* result,
                   double& logLikelihood,
                   unsigned numParticles, Logger& log);

private:
    bool passStatistics(unsigned t, double logLikelihood, double dev, vector<double>& statistics);
};

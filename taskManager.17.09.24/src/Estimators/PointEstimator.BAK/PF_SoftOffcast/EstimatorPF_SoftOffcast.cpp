#include "EstimatorPF_SoftOffcast.h"

#include <sstream>
#include <float.h>

using namespace std;


int EstimatorPF_SoftOffcast::estimatePoint(ptrInt pSignal,
                            vector<double> track,
                            PF_Model* model, ParamSet* paramSet,
                            PF_Params* result, double& logLikelihood,
                            unsigned numParticles, Logger& log)
{
    PF_Params* params = paramSet->getParams(paramSet->getCurrentId());
    /** get the size of sample (s.p. trajectory) */
    const unsigned size = track.size();
    vector<double> particles(numParticles);
   /** container to keep
    the best logLikelihood (!singular)
      for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE
        OR
    the AVERAGE of counterBestStatisticsFound number of the best logLikelihoods
      for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE */
    vector<double> statistics(size, -DBL_MAX);
    /** container to keep logLikelihood for all the points of s.p.trajectory given current vector of Model Params */
    vector<double> tmp_statistics(size, -DBL_MAX);
    /** this flag means "We have found AT LEAST ONE candidate for OPTIMUM into Model Params Compact" */
    bool isStatisticsInitialized=false;
    /**  counter of the best Likelihoods found so far */
    unsigned int counterBestStatisticsFound=0;
    /** LogLikelihood for current vector of Model Params */
    double hypLogLikelihood;
    /** Loop through the set of Model Params into Model Params Compact */
    double hypLogLikelihoodThresholdShift = DBL_MIN;
    /** s.p.trajectory argument (time or point index) */
    unsigned t = 0;
    /** Loop through the set of Model Params into  Params Compact */
    while (!paramSet->end())
    {
        hypLogLikelihood = -DBL_MIN;
///TODO!!! ��� ����� �������� ������ �� ��������� ��������� ���������� ���������������� �������� ���������� ������
//TODO  hypLogLikelihoodThresholdShift = model->noiseModel->estimateHypothesisDev(params->noiseParams, particles.size());
        /** Loop through s.p. trajectory
        to estimate the probability (LogLikelihood) of current Model Params  */
        for (t = 0; t < size; t++)
        {
            /** Let's check if current thread must end! */
            ///TODO!!! see TerminateThreads.TODO! if (WaitForSingleObject(event, 1) != WAIT_TIMEOUT) !!!!!! return THREAD_EXECUTION_INTERRUPTED

            if (t == 0) {
                model->initParticles(params, particles);
            }
            else {
                model->generateParticles(params, particles);
            }
            hypLogLikelihood += model->estimateHypothesis(track[t], params, particles, t);

            /** Trying to reject current Model Params
             as non-optimal ones
             */
///TODO!!! ��-�������� � �������� ������ ������� �������� ���� ����� � ��������� �������� ����������, ����� �� ������ �� ���� �����
            stringstream sstr;
            sstr << "current hypothesis LogLikelihood:\t" << hypLogLikelihood;
            log.log(ALGORITHM_6_LEVEL_INFO, sstr.str());
///TODO!!! ��������� ������ �� ��������� ��������� ���������� ���������������� �������� ���������� ������
/// � ���� hypLogLikelihoodThresholdShift = -DBL_MIN
            /** ������ ������������ �������: ������ ����� ���������, ��� ��� ���� �� ���� ��� ������ ���� ���� �� �����!
            ( ����������� ������� "������ statistics ��� �������� �� �����")
            � ����� ���������, ��� ������ hypLogLikelihood � ������� ����� ��� �������� ������� ����������
            ����������� ������ ��� ���� "hypLogLikelihood ������ �� ����������� � ���� ����� �� ��������� ������������ � ����� ������� �������� ����������"
            ����� ������� ������ �������������� �� 11 * 10^3 ����� � ������������ ����������, � ����� �� �� ������ ���� ����������� �����!
             */
            if (isStatisticsInitialized && (!passStatistics(t, hypLogLikelihood, hypLogLikelihoodThresholdShift, tmp_statistics)))
            {
                tmp_statistics = statistics;

                stringstream sstr;
                sstr << "paramsSetId = " << paramSet->getCurrentId() <<
                     ", hypothesis = {\n" << params->toString() <<
                     "\n}, hypLogLikelihood = " << hypLogLikelihood <<
                     "\nFailed statistics at point t = " << t;
                     log.log(ALGORITHM_6_LEVEL_INFO, sstr.str());
///TODO!!! �������� ����������� ��������� �������� ����� � ������� ������� ����������!
/// ������ �������� �� �������� �����, ����� �� �������� ������� ������ � ���������� �������������� ������ ����� ???
                break;
             }
            /** Let's check if current thread must end! */
            if (pSignal)
            {
                if ( *(pSignal) != 0x1 )
                {
                    continue;
                }
                else
                {
                    *pSignal = 0x0;
                    return THREAD_EXECUTION_INTERRUPTED;
                }
            }
        } /** End of Loop through all the points of s.p. trajectory to estimate the probability of current Model Params */

        if (t < size) {
            params = paramSet->getNext();
            continue;
        }

        stringstream sstr;
        sstr << "paramsSetId = " << paramSet->getCurrentId() <<
                ", hypothesis = {\n" << params->toString() <<
                "\n}, hypLogLikelihoodThresholdShift = " << hypLogLikelihoodThresholdShift << ", hypLogLikelihood = " << hypLogLikelihood;

        if (logLikelihood < hypLogLikelihood) {
            counterBestStatisticsFound++;
            isStatisticsInitialized=true;
            logLikelihood = hypLogLikelihood;
            result->copy((PF_Params*)params);
            log.log(ALGORITHM_ESTIMATOR_NOTIFICATION, "Get new maximum");
            log.log(ALGORITHM_ESTIMATOR_NOTIFICATION, sstr.str());

            statistics = tmp_statistics;
        }
        else {
            tmp_statistics = statistics;
            log.log(ALGORITHM_6_LEVEL_INFO, sstr.str());
        }

        params = paramSet->getNext();
    }
  return THREAD_EXECUTION_ACCOMPLISHED;
}

bool EstimatorPF_SoftOffcast::passStatistics(unsigned t, double logLikelihood, double dev, vector<double>& statistics)
{
    if (logLikelihood + dev < statistics[t])
        return false;
   /** ����� ������ �� ������ ������ ������ �����������, ���������� � ���� �����, � ������ */
    statistics[t] = max(logLikelihood, statistics[t]);
///TODO!!! �� �������� ���������, ��� ������ statistics ��� ��������!
///� ����� ��� ����� ������� ���������� ��������� ��������� ����������� {INC= \SUM_{i=t+1}^{track_size} {statistics[i] }
/// ��������� ��� � ��� ������������, � ������� �������� ����������, �����������
/// � ����� �������� � ������ ��������� �������� �������������!

    return true;
}

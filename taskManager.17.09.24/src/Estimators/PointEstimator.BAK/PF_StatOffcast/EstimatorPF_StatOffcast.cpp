#include "EstimatorPF_StatOffcast.h"
#include <windows.h> //WaitForSingleObject()
#include <sstream>
#include <float.h>

using namespace std;

/** static member definition */
//TODO!!! stringstream EstimatorPF_StatOffcast::m_sstr;

int EstimatorPF_StatOffcast::estimatePoint(ptrInt pSignal,
                            vector<double> track,
                            PF_Model* model, ParamSet* paramSet,
                            PF_Params* result, double& logLikelihood,
                            unsigned numParticles, Logger& log)
{
    stringstream sstr;
    PF_Params* params = paramSet->getParams(paramSet->getCurrentId());

    /** get the size of sample (s.p. trajectory)
    */
    const unsigned size = track.size();
    vector<double> particles(numParticles);
   /** container to keep
    the best logLikelihood (!singular)
      for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE
        OR
    the AVERAGE of counterBestStatisticsFound number of the best logLikelihoods
      for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE
  */
    vector<double> statistics(size, -DBL_MAX);
    /** container to keep logLikelihood for all the points of s.p.trajectory given current vector of Model Params
    */
    vector<double> tmp_statistics(size, -DBL_MAX);
    /** this flag means "We have found AT LEAST ONE candidate for OPTIMUM into Model Params Compact"
    */
    bool isStatisticsInitialized=false;
    /**  counter of the best Likelihoods found so far */
    unsigned int counterBestStatisticsFound=0;
    /** LogLikelihood for current vector of Model Params */
    double hypLogLikelihood;
    /** s.p.trajectory argument (time or point index) */
    unsigned t = 0;
    /** Loop through the set of Model Params into Model Params Compact */
    while (!paramSet->end())
    {
        /** initialize with "almost zero" to get LogLikelihood for current Model Params */
        hypLogLikelihood = -DBL_MIN;
        /** Loop through points of s.p. trajectory
        to estimate the probability (LogLikelihood) of current Model Params */
        for (t = 0; t < size; t++)
        {
            if (t == 0) {
                model->initParticles(params, particles);
            }
            else {
                model->generateParticles(params, particles);
            }
            /** estimate (LogLikelihood) of current Model Params */
            hypLogLikelihood += model->estimateHypothesis(track[t], params, particles, t);
///TODO!!! ��-�������� � �������� ������ ������� �������� ���� ����� � ��������� �������� ����������, ����� �� ������ �� ���� �����
            sstr.clear();
            sstr << "current hypothesis LogLikelihood:\t" << hypLogLikelihood;
            log.log(ALGORITHM_7_LEVEL_INFO, sstr.str());
            /** Trying to reject current Model Params
             as non-optimal ones
             */
///TODO!!! ��������� ������ �� ��������� ��������� ���������� ���������������� �������� ���������� ������
/// � ���� hypLogLikelihood = -DBL_MIN
            /** ������ �������� ��� �� ������ ����� ���������,
             ���� ������ hypLogLikelihood � ���� ����� ��� �������� ������� ����������
             ��������� ���� "hypLogLikelihood ������� �� ����������� � ���� ����� �� ��������� ������������ � ����� ������� �������� ����������"
            ����� ������� �������������� �� 3-4 * 10^6 ����� � ������������ ����������, �� ������ ���� ����������� �����,
            �.�. ��� ����������� ������������ ��������� ����� � ������ ����� ���������� ��������� �������� ��������� ��������� ����� ��������� ��������!

            ����� ���� ������� ������� (statistics[size-1] > -DBL_MAX)
            ��� ������������, ��� � ����� ������� �� ��� ���� �� ���� ��� ������ ��� ���������� �� �����
            � ��� ������, ��� ���� � ��������� ����� ���������� hypLogLikelihood �������� �������� (>-LDBL_MAX)
            ����� ������� �������������� �� 6 * 10^3 ����� � ������������ ����������, � ����� �� �� ������ ���� ����������� �����!

            ������ ������������ �������: ������ ����� ���������, ��� ��� ���� �� ���� ��� ������ ���� ���� �� �����!
            ( ����������� ������� "������ statistics ��� �������� �� �����")
            � ����� ���������, ��� ������ hypLogLikelihood � ������� ����� ��� �������� ������� ����������
            ����������� ������� �� ���� "hypLogLikelihood ������ �� ����������� � ���� ����� �� ��������� ������������ � ����� ������� �������� ����������"
            ����� ������� �������������� �� 11 * 10^3 ����� � ������������ ����������, � ����� �� �� ������ ���� ����������� �����!
            */
            if ( isStatisticsInitialized && (!passStatistics(t, hypLogLikelihood, tmp_statistics)))
            {
                tmp_statistics = statistics;

                // stringstream sstr;
                sstr.clear();
                sstr << "paramsSetId = " << paramSet->getCurrentId() <<
                     ", hypothesis = {\n" << params->toString() <<
                     "\n}, hypLogLikelihood = " << hypLogLikelihood <<
                     "\nFailed statistics at point t = " << t;
                     log.log(ALGORITHM_6_LEVEL_INFO, sstr.str());
///TODO!!! �������� ����������� ��������� �������� ����� � ������� ������� ����������!
/// ������ �������� �� �������� �����, ����� �� �������� ������� ������ � ���������� �������������� ������ ����� ???
                break;
             }
            /** Let's check if current thread must end! */
            /** Removed TerminateThreads() from code.
            Wrong! since platform-specific: if (WaitForSingleObject( (ptrVoid)(*event), 1) == WAIT_TIMEOUT) */
            if (pSignal)
            {
                if ( *(pSignal) != 0x1 )
                {
                    continue;
                }
                else
                {
                    *pSignal = 0x0;
                    return THREAD_EXECUTION_INTERRUPTED;
                }
            }

        } /** End of Loop through all the points of s.p. trajectory to estimate the probability of current Model Params */

        /** if we terminated for-loop because current model params are far from optimum
        Move to the next point in params space */
        if (t < size) {
///TODO!!! ��� ����� ����� ���� ��, �������������� ������ ����������� ������� ������������� � ��� �����������, � ������� ������, ������� �� �� ��������� ���, � �� ��������� �������, ������ �������� ����� ���� ����� ������������ ������� (� �������) ���������� ������� ������������� � ���� �����������
/// �� �������� ���������� ������� ��� ��� ��������� ������ ����������� ������� �������������, ��������� �������� �� ������� �� ����� ������ �� ���� ����������� ��� ���������� � ���� ������ ����������!
            params = paramSet->getNext();
            continue;
        }

        // stringstream sstr;
        sstr.clear();
        sstr << "paramsSetId = " << paramSet->getCurrentId() <<
                ", hypothesis = {\n" << params->toString() <<
                "\n}, current LogLikelihood = " << hypLogLikelihood << ", best current LogLikelihood = " << logLikelihood;
        /** if likelihood of current model params (just processed) is more then previous the best one
        we have to SAVE NEW the BEST LIKELIHOOD of current model params
        AND the vector "statistics" obtained for current model params */
        if (hypLogLikelihood > logLikelihood) {
            /** OLD! trivial implementation as REPLACE_MODE: statistics = tmp_statistics; */
///TODO!!! ������ ��� ��� ���������, ���������� �� ������������� ����������� statistics, ����������, ����� ������ ������� �����
            evaluateNewStatistics(AVERAGE_MODE, counterBestStatisticsFound, statistics, tmp_statistics);
            counterBestStatisticsFound++;
            isStatisticsInitialized=true;
            logLikelihood = hypLogLikelihood;
            result->copy((PF_Params*)params);

            log.log(ALGORITHM_ESTIMATOR_NOTIFICATION, "Get new maximum");
            log.log(ALGORITHM_ESTIMATOR_NOTIFICATION, sstr.str());
        }
        else {
            tmp_statistics = statistics;
            log.log(ALGORITHM_7_LEVEL_INFO, sstr.str());
        }
///TODO!!! ��� ����� ����� ���� ��, �������������� ������ ����������� ������� ������������� � ��� �����������, � ������� ������, ������� �� �� ��������� ���, � �� ��������� �������, ������ �������� ����� ���� ����� ������������ ������� (� �������) ���������� ������� ������������� � ���� �����������
/// �� �������� ���������� ������� ��� ��� ��������� ������ ����������� ������� �������������, ��������� �������� �� ������� �� ����� ������ �� ���� ����������� ��� ���������� � ���� ������ ����������!
        params = paramSet->getNext();
    }
  return THREAD_EXECUTION_ACCOMPLISHED;
}
/**
  ��� ��� ���������� � ���� ��������������� ������������ ������� statistics,
  �������� �������� ������ � �������� ���������
  ��� ���������� ����������� ��������� ���������� ������� �� �������� � ��������������� ������������ ������
*/
void EstimatorPF_StatOffcast::evaluateNewStatistics(enum STATISTICS_ESTIMATE_MODE mode, unsigned int& counterBestStatisticsFound, vector<double>& statistics, vector<double>& tmp_statistics)
{
   if (counterBestStatisticsFound == 0)
   {
       mode = REPLACE_MODE;
   }
   switch(mode)
   {
     case REPLACE_MODE:
     {
         statistics = tmp_statistics;
         return;
     }
     case AVERAGE_MODE:
     {
         for(unsigned int i=0; i < tmp_statistics.size(); ++i)
         {
             statistics[i]= ((statistics[i] * (double)counterBestStatisticsFound) + tmp_statistics[i])/(double)(counterBestStatisticsFound + 1);
         }
         return;
     }
   }
  return;
}

bool EstimatorPF_StatOffcast::passStatistics(unsigned t, double logLikelihood, vector<double>& statistics)
{
    if (logLikelihood < statistics[t])
        return false;
   /** ����� ������ �� ������ ������ ������ �����������, ���������� � ���� �����, � ������ */
    statistics[t] = logLikelihood;
///TODO!!! �� �������� ���������, ��� ������ statistics ��� ��������!
///� ����� ��� ����� ������� ���������� ��������� ��������� ����������� {INC= \SUM_{i=t+1}^{track_size} {statistics[i] }
/// ��������� ��� � ��� ������������, � ������� �������� ����������, �����������
/// � ����� �������� � ������ ��������� �������� �������������!

    return true;
}

#pragma once

#include "../../EstimatorFactory.h"


class EstimatorPF_StatOffcast : public PointEstimator
{
private:
    /** It's merely declaration! Don't forget definition into the code! */
    //TODO!!! static /** neither thread_local nor __thread work! gcc 4.7 */ stringstream m_sstr;
public:
    int estimatePoint(ptrInt pSignal,
                   vector<double> track,
                   PF_Model* model,
                   ParamSet* paramSet, PF_Params* result,
                   double& logLikelihood,
                   unsigned numParticles, Logger& log);

private:
    enum STATISTICS_ESTIMATE_MODE
    {
        REPLACE_MODE,
        AVERAGE_MODE
    };
    bool passStatistics(unsigned t, double logLikelihood, vector<double>& statistics);
    void evaluateNewStatistics(enum STATISTICS_ESTIMATE_MODE mode, unsigned int& counterBestStatisticsFound, vector<double>& statistics, vector<double>& tmp_statistics);
};

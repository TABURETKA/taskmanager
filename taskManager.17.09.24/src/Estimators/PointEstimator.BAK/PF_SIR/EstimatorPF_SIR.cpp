#include "EstimatorPF_SIR.h"
#include "../../../Distributions/Distributions.h"

#include <sstream>
#include <float.h>
#include <math.h>
#include <iostream>

using namespace std;


int EstimatorPF_SIR::estimatePoint(ptrInt pSignal,
                            vector<double> track,
                            PF_Model* model_, ParamSet* paramSet,
                            PF_Params* result, double& logLikelihood,
                            unsigned numParticles, Logger& logger)
{
    PF_Params* params = paramSet->getParams(paramSet->getCurrentId());
    model = model_;

    /** get the size of sample (s.p. trajectory) */
    const unsigned size = track.size();
///TODO!!! � ������� �� ������ PF' �����������!
 /** ������-�� �������� particles, weights, statistics
 � ����� ������ */
    particles.resize(numParticles);
    weights.assign(numParticles, 1.);
    statistics.assign(size, -DBL_MAX);
   /** container to keep
    the best logLikelihood (!singular)
      for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE
        OR
    the AVERAGE of counterBestStatisticsFound number of the best logLikelihoods
      for all the points of s.p.trajectory given Compact of Model Params in AVERAGE_MODE */
    vector<double> statistics(size, -DBL_MAX);
    /** container to keep logLikelihood for all the points of s.p.trajectory given current vector of Model Params */
    vector<double> tmp_statistics(size, -DBL_MAX);
    /** this flag means "We have found AT LEAST ONE candidate for OPTIMUM into Model Params Compact" */
    bool isStatisticsInitialized=false;
    /**  counter of the best Likelihoods found so far */
    unsigned int counterBestStatisticsFound=0;
    /** LogLikelihood for current vector of Model Params */
    double hypLogLikelihood;
    /** s.p.trajectory argument (time or point index) */
    unsigned t = 0;
    double hypMargProb;
    unsigned nResample;
    /** Loop through the set of Model Params into Model Params Compact */
    while (!paramSet->end())
    {
        //cout << "Hypoteza " << paramSet->getCurrentId() << endl << params->toString() << endl;
        hypLogLikelihood = 0;
        nResample = 0;
        /** Loop through s.p. trajectory
        to estimate the probability of current Model Params  */
        for (t = 0; t < size; t++)
        {
            /** Let's check if current thread must end! */
            ///TODO!!! see TerminateThreads.TODO! if (WaitForSingleObject(event, 1) != WAIT_TIMEOUT) !!!!!! return THREAD_EXECUTION_INTERRUPTED
            //cout << "T = " << t << endl;
            if (t == 0) {
                model->initParticles(params, particles);
            }
            else {
                model->generateParticles(params, particles);
            }
            getWeights(params, track[t]);
            if (passResemplingCriteria()) {
                //cout << "passResemplingCriteria" << endl;
                resample();
                nResample++;
            }
            hypMargProb = model->noiseModel->estimatePointMove(params->noiseParams, track[t] - hypMean(), t);
            hypLogLikelihood += log(hypMargProb);
            //cout << "Marg = " << hypMargProb << endl <<
            //        "hypLogLikelihood = " << hypLogLikelihood << endl << endl;
///TODO!!! ��-�������� � �������� ������ ������� �������� ���� ����� � ��������� �������� ����������, ����� �� ������ �� ���� �����
            stringstream sstr;
            sstr << "current hypothesis probability:\t" << hypLogLikelihood;
            logger.log(ALGORITHM_6_LEVEL_INFO, sstr.str());

            /** Trying to reject current Model Params
             as non-optimal ones
             */
///TODO!!! ��������� ������ �� ��������� ��������� ���������� ���������������� �������� ���������� ������
/// � ���� hypProbDev = -DBL_MIN
            /** ������ ������������ �������: ������ ����� ���������, ��� ��� ���� �� ���� ��� ������ ���� ���� �� �����!
            ( ����������� ������� "������ statistics ��� �������� �� �����")
            � ����� ���������, ��� ������ hypLogLikelihood � ������� ����� ��� �������� ������� ����������
            ����������� ������ ��� ���� "hypLogLikelihood ������ �� ����������� � ���� ����� �� ��������� ������������ � ����� ������� �������� ����������"
            ����� ������� ������ �������������� �� 11 * 10^3 ����� � ������������ ����������, � ����� �� �� ������ ���� ����������� �����!
             */
            if (isStatisticsInitialized && (!passStatistics(hypLogLikelihood, tmp_statistics)))
            {
                tmp_statistics = statistics;

                stringstream sstr;
                sstr << "paramsSetId = " << paramSet->getCurrentId() <<
                     ", hypothesis = {\n" << params->toString() <<
                     "\n}, hypLogLikelihood = " << hypLogLikelihood << " nResample = " << nResample <<
                     "\nFailed statistics at point t = " << t;
                logger.log(ALGORITHM_6_LEVEL_INFO, sstr.str());
///TODO!!! �������� ����������� ��������� �������� ����� � ������� ������� ����������!
/// ������ �������� �� �������� �����, ����� �� �������� ������� ������ � ���������� �������������� ������ ����� ???
                break;
            }
            /** Let's check if current thread must end! */
            if (pSignal)
            {
                if ( *(pSignal) != 0x1 )
                {
                    continue;
                }
                else
                {
                    *pSignal = 0x0;
                    return THREAD_EXECUTION_INTERRUPTED;
                }
            }
        } /** End of Loop through all the points of s.p. trajectory to estimate the probability of current Model Params */

        if (t < size) {
            params = paramSet->getNext();
            continue;
        }
///TODO!!! � ������� �� ������ PF' �����������!
 /** ������-�� ���������� �������� ������� statistics ����� tmp_statistics */
        statistics = tmp_statistics;

        stringstream sstr;
        sstr << "paramsSetId = " << paramSet->getCurrentId() <<
                ", hypothesis = {\n" << params->toString() <<
                "\n}, logLikelihood = " << hypLogLikelihood << " nResample = " << nResample;
        logger.log(ALGORITHM_6_LEVEL_INFO, "Get new maximum");
        logger.log(ALGORITHM_6_LEVEL_INFO, sstr.str());

        counterBestStatisticsFound++;
        isStatisticsInitialized=true;
        logLikelihood = hypLogLikelihood;
        result->copy((PF_Params*)params);
///TODO!!! ����� ���������� ������������?
        statistics = tmp_statistics;

        params = paramSet->getNext();
    }
  return THREAD_EXECUTION_ACCOMPLISHED;
}

bool EstimatorPF_SIR::passStatistics(double logLikelihood, vector<double>& stat)
{
    if (logLikelihood < stat[t])
        return false;
   /** ����� ������ �� ������ ������ ������ �����������, ���������� � ���� �����, � ������ */
    stat[t] = logLikelihood;
///TODO!!! �� �������� ���������, ��� ������ statistics ��� ��������!
///� ����� ��� ����� ������� ���������� ��������� ��������� ����������� {INC= \SUM_{i=t+1}^{track_size} {statistics[i] }
/// ��������� ��� � ��� ������������, � ������� �������� ����������, �����������
/// � ����� �������� � ������ ��������� �������� �������������!
    return true;
}

bool EstimatorPF_SIR::passResemplingCriteria()
{
    unsigned size = weights.size();
    double sqSum = 0.;
    double threshold = (double)size / 2.;

    for (unsigned i = 0; i < size; i++) {
        sqSum += weights[i] * weights[i];
    }
    return (1. / sqSum) < threshold;
}

void EstimatorPF_SIR::normilizeWeights()
{
    unsigned size = weights.size();
    double sum = 0.;
    vector<double> un_w = weights;

    for (unsigned i = 0; i < size; i++) {
        sum += weights[i];
    }
    if (sum < DBL_MIN ) {
        weights.assign(size, 1. / (double)size);
        return;
    }
    //cout << "Normalized\n";
    for (unsigned i = 0; i < size; i++) {
        weights[i] /= sum;
        //cout << i << " " << weights[i] << endl;
    }
}


void EstimatorPF_SIR::getWeights(PF_Params* params, double point)
{
    unsigned numParticles = particles.size();
    //cout << endl << "Point = " << point << endl;
    for (unsigned i = 0; i < numParticles; i++) {
        weights[i] *= model->noiseModel->estimatePointMove(params->noiseParams, particles[i] - point, t);
        //cout << i << " p=" << particles[i] << " w=" << weights[i] << endl;
    }

    normilizeWeights();
}

void EstimatorPF_SIR::resample()
{
    vector<double> new_particles(particles.size());
    unsigned size = particles.size();
    double f_size = (double)particles.size();

    // old particles index
    unsigned j = 0;
    // probability function estimation
    double prob = weights[0];

    double u = Distributions::Uniform() / f_size; // u_0 ~ U(0, 1/N)
    double u_step = 1. / f_size;                  // u_i = u_1 + i / N, i > 0

    for (unsigned i = 0; i < size; i++) {
        while (prob < u) {
            //cout << i << "  " << j << "  " << u << "  " << prob << endl;
            j++;
            prob += weights[j];
        }
        //cout << i << "  " << j << "  " << u << "  " << prob << "  part=" << particles[j] << endl;
        new_particles[i] = particles[j];

        u += u_step;
    }
    weights.assign(size, 1. / f_size);

    particles = new_particles;
}

double EstimatorPF_SIR::hypMean()
{
    double mean = 0.;
    unsigned size = particles.size();

    for (unsigned i = 0; i < size; i++) {
        mean += particles[i] * weights[i];
    }
    //cout << "hypMean = " << mean << endl;
    return mean;
}

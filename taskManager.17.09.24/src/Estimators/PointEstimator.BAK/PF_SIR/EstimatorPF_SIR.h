#pragma once

#include "../../EstimatorFactory.h"


class EstimatorPF_SIR : public PointEstimator
{
public:
    int estimatePoint(ptrInt pSignal,
                   vector<double> track,
                   PF_Model* model,
                   ParamSet* paramSet, PF_Params* result,
                   double& logLikelihood,
                   unsigned numParticles, Logger& log);

private:
    bool passStatistics(double probability, vector<double>& stat);
    bool passResemplingCriteria();

    void normilizeWeights();
    void getWeights(PF_Params* params, double point);
    void resample();

    double hypMean();

    vector<double> particles;
    vector<double> weights;
    vector<double> statistics;
    unsigned t;

    PF_Model* model;
};

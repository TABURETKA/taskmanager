#pragma once

#include "../../EstimatorFactory.h"

class EstimatorPF : public PointEstimator
{
private:
    /** It's merely declaration! Don't forget definition into the code! */
    //TODO!!! static /** neither thread_local nor __thread work! gcc 4.7 */ stringstream m_sstr;
public:
    int estimatePoint(ptrInt pThreadInterruptSignal,
                   vector<double> track,
                   PF_Model* model,
                   ParamSet* paramSet, PF_Params* result,
                   double& probability,
                   unsigned numParticles, Logger& log);
};

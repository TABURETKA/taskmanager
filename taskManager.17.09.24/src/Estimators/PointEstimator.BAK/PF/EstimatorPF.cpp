#include "EstimatorPF.h"

#include <sstream>

using namespace std;

/** static member definition */
//TODO!!! stringstream EstimatorPF::m_sstr;

int EstimatorPF::estimatePoint(ptrInt pSignal,
                            vector<double> track,
                            PF_Model* model, ParamSet* paramSet,
                            PF_Params* result, double& probability,
                            unsigned numParticles, Logger& log)
{

    stringstream sstr;
///TODO!!! ��� ����� �������� ������ ��������� process! � ������ ���� ����������, �.�. ����� �� ���������� ��������� ������� ����������, noise!
    PF_Params* params = paramSet->getParams(paramSet->getCurrentId());

    const unsigned size = track.size();
    vector<double> particles(numParticles);

    double hypProbability;
    unsigned t = 0;

    while (!paramSet->end())
    {
        hypProbability = 0;
        for (t = 0; t < size; t++)
        {
            if (t == 0) {
                model->initParticles(params, particles);
            }
            else {
                model->generateParticles(params, particles);
            }
            hypProbability += model->estimateHypothesis(track[t], params, particles, t);
            /** thread_local - to prevent classical mistake: creating a new stringstream every single time
             */
            // neither thread_local nor  __thread stringstream sstr;
            sstr.clear();
            sstr << "current hypothesis probability:\t" << hypProbability;
            log.log(ALGORITHM_GENERAL_INFO, sstr.str());
            /** Let's check if current thread must end! */
            if (pSignal)
            {
                if ( *(pSignal) != 0x1 )
                {
                    continue;
                }
                else
                {
                    *pSignal = 0x0;
                    return THREAD_EXECUTION_INTERRUPTED;
                }
            }
        } /** End of Loop through all the points of s.p. trajectory to estimate the probability of current Model Params */


        /** thread_local - to prevent classical mistake: creating a new stringstream every single time
        */
        // neither thread_local nor __threa stringstream sstr;
        sstr.clear();
        sstr << "paramsSetId = " << paramSet->getCurrentId() <<
                ", hypothesis = {\n" << params->toString() <<
                "\n}, probability = " << hypProbability;
        log.log(ALGORITHM_GENERAL_INFO, sstr.str());

        if (hypProbability > probability) {
            probability = hypProbability;
            result->copy((PF_Params*)params);
            log.log(ALGORITHM_ESTIMATOR_NOTIFICATION, "Get new maximum: " + result->toString());
        }

        params = paramSet->getNext();
    }
  return THREAD_EXECUTION_ACCOMPLISHED;
}

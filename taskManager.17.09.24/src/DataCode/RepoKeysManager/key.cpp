#include <iostream> /* cout */

#include "key.h"
#include <sstream>
#include <algorithm>
#include "../../CommonStuff/Defines.h"
#include "../../Log/errors.h"
#include "../../StringStuff/StringStuff.h" /* TrimStr() */
///ctor
Key::Key()
{
}
///ctor
Key::Key(std::vector<int> indexesOfSrcData, int indexInRepo)
{
    this->m_indexesOfSrcData = indexesOfSrcData;
    this->m_indexInRepo = indexInRepo;
}
///dtor
Key::~Key()
{
    m_indexesOfSrcData.clear();
}
///copy ctor
Key::Key(const Key& other)
{
///TODO!!! � ��� ��� ����� ��������� � ��������, ���� ��� ���� (� ������ ������ ���������� ������������) - private?
    this->m_indexesOfSrcData = other.getIndexesOfData();
    this->m_indexInRepo = other.getIndex();
}
int Key::getIndex() const
{
    return m_indexInRepo;
}
std::vector<int> Key::getIndexesOfData() const
{
    return m_indexesOfSrcData;
}
/**
    int Key::convert(std::string source, std::vector<int> &result)
    converts list of numbers as string to vector<int>
    PARAMS [IN] : std::string source - source in this format "  [ 1, 3, 8]  "
    PARAMS [OUT]: std::vector<int> &result - result
    RETURNs : _RC_SUCCESS_ if success,
              _ERROR_WRONG_ARGUMENT_ otherwise
 */
int Key::convert(std::string source, std::vector<int> &result)
{
    size_t i;
//OLS    while((i=source.find(' '))!=std::string::npos)
//        source.erase(i, 1);
    std::string delimiters = DEFAULT_DELIMITERS;
    TrimStr(source,delimiters);
    if( (source[0] != '[') || (source[source.length() - 1] != ']') )
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    source.erase(source.length() - 1);
    source.erase(0,1);

    int value = 0;
    while((i=source.find(DELIMITER_LIST_ITEMS))!=std::string::npos)
    {
        std::string buf = source;
        buf.erase(i);
        source.erase(0,i+1);
        std::istringstream buffer(buf);
        buffer >> value;
        result.push_back(value);
        value = 0;
    }
    std::istringstream buffer(source);
    buffer >> value;
    result.push_back(value);

    if( std::find(result.begin(), result.end(), 0) != result.end() )
    {
        /** IAA: ����� ���� �������� : ��������� ��������� ������,
         � ������� ������ �������� =0 (����� ����� ������� ��������������� �� �����),
         � ����� ����� ���� ������ ��� ���� � ����� ��������.
         �� ��������� ������� ������ ����� ����� ������ ��������, ���� ������ ����� �������� =0!
         Parent's key = 0 must be single and is assigned ONLY for datasets retrieved from file!
        */
        if(result.size() != 1)
        {
            result.clear();
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
    return _RC_SUCCESS_;
}
/**
    int Key::convertFromString(std::string sk)
    converts list of numbers as string to vector<int> (member 'parents' of this class)
    PARAMS [IN] : std::string sk - source key in this format "[ 6 [ 1, 3, 8]  ]"
    result as side-effect : this->parents vector
    RETURNs : _RC_SUCCESS_ if success,
              _ERROR_WRONG_ARGUMENT_ otherwise
 */
int Key::convertFromString(std::string sk)
{
    size_t i;
//OLD    while((i=sk.find(' '))!=std::string::npos)
//        sk.erase(i, 1);
    std::string delimiters = DEFAULT_DELIMITERS;
    TrimStr(sk, delimiters);

    if( (sk[0] != '[') || (sk[sk.length() - 1] != ']') )
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    sk.erase(sk.length() - 1);
    sk.erase(0,1);

    i=sk.find('[');
    std::string buf = sk;
    buf.erase(i);
    std::istringstream buffer(buf);
    buffer >> this->m_indexInRepo;
    if(this->m_indexInRepo <= 0)
        return _ERROR_WRONG_ARGUMENT_;

    sk.erase(0,i);
    return this->convert(sk,this->m_indexesOfSrcData);
}
/*!
    bool Key::compare( Key* & other)

     \brief:  compare keys
BAD!!!     \return: - 0, if success, else 1
     \params: Key* & other - other key
 */
bool Key::compare(const Key* & other) const
{
    if(this->m_indexInRepo == other->getIndex())
    {
        if(this->m_indexesOfSrcData.size() == other->getIndexesOfData().size())
        {
            for(unsigned int j = 0; j < this->m_indexesOfSrcData.size(); ++j)
            {
///DEBUG BEGIN
//int op1= m_indexesOfSrcData[j];
//int op2= other->getIndexesOfData()[j];
//std::cout << op1 << ":" << op2 << std::endl;
///DEBUG END
                if(this->m_indexesOfSrcData[j] != other->getIndexesOfData()[j])
                    return false;
            }
            return true;
        }
    }
    return false;
}

std::string Key::toString() const
{
  std::stringstream keyAsStr;
  keyAsStr << m_indexInRepo << "[";
  for(unsigned int j = 0; j < this->m_indexesOfSrcData.size(); ++j)
  {
     keyAsStr << this->m_indexesOfSrcData[j] << ",";
  }
  keyAsStr <<  "]";
  return keyAsStr.str();
}

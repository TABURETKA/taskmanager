#ifndef KEY_H
#define KEY_H

#include <vector>
#include <string>

class Key
{
    public:
///TODO!!! semantic for -1 ???
/// -1 - is a default value, when key's specimen created but don't have information in it.
/// default value = -1 because it is incorrect index (first index in repo = 1; external = 0 (only for history) )
///  It seems that it is bug. It would be better if there would be a compilation error, when forgot to register the index than not the correct value.
///  maybe I added it here to debuging

        Key(std::vector<int> indexesOfSrcData, int indexInRepo = -1);
        Key();
        virtual ~Key();
        Key(const Key& other);

        int convert(std::string source, std::vector<int> &result);
        int convertFromString(std::string sk);
        int getIndex() const;
        std::vector<int> getIndexesOfData() const;

        bool compare(const Key* & other) const;
        std::string toString() const;
    protected:

    private:
    /** IAA: ��� ������������� ���� ����������� ������ ������.
     ������������� ����� ������ ������ ����� ���� ����� ������.
     �.�. ��������� ����� ����� ��� ����� �� ������� � �������������� ������, �������� ���� ���� �������.
    */
    int m_indexInRepo; /** index of repo's entry (index of single DataCell) with this key; */
    /** IAA: ���������� ����� ���� - ������ ������ ���� m_indexesOfSrcData
      ������-�� �� EstimatorSources � DataManager.
     ����, � ���� � ��� ��������� � �����������, ��� ������ ����� ����, ��� ��������� �������� ���� ������ �
     ��� ������ ���� �������� � ����������� (��� ��� ���� �������� ��� ����� ������).
    */
    std::vector<int> m_indexesOfSrcData; /** indexes of data for creating repo's current entry (history of data) (if the sources of the data are not from repo, parents = 0) */
};

#endif // KEY_H

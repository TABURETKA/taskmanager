#ifndef _DATAMANAGERGRAMMAR_H_
#define _DATAMANAGERGRAMMAR_H_
#include <string>
#include <vector>

#define DELIMITER_DATASET_ITEMS_SEMANTICS_IN_TRACK_NAME "!"
/** forward declaration */
typedef struct tagDataInfo DataInfo;
class Key;
class DataModel;

class DataManagerGrammar
{

 public:
  static int splitAllRepoKeysAsSingleString(std::string allRepoKeysAsSingleString, std::vector<std::string> &keys);
  static int splitRepoKeysAndFilers(const std::string& keysAndFilters, std::string& repoKeys, std::string& filters);
  static std::string makeFilterPrefix(const DataInfo *& pDI);
  static std::string makeFullDatasetName (const DataInfo *& pDI, const std::string& filterPrefix, size_t datasetIndex);
  static int replace_filter (std::string& filter, const std::string& replace_str, int index);
///TODO!!! ���������� ���, ����� � ���������� �� ����� ���� ���������� ��������� �� �����������! ���������� �� ������ ���� �������� �� ����������� ���������� �����������!
  static int validation (Key* & key, std::string & subfilter, int indexSubfilter, const DataModel * repo);
///TODO!!! ���������� ���, ����� � ���������� �� ����� ���� ���������� ��������� �� �����������! ���������� �� ������ ���� �������� �� ����������� ���������� �����������!
  static int check_replace_filter (Key* & key, std::string & currentFilter, const DataModel * repo);
  static int check_replace_filter(std::string & currentFilter, std::string & newFilter);

};
#endif /* end _DATAMANAGERGRAMMAR_H_ */

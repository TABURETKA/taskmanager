#include <iostream>
#include "DataManagerGrammar.h"
#include "../../CommonStuff/Defines.h" /** DEFAULT_DELIMITERS */
#include "../../StringStuff/StringStuff.h" /** splitStringByDelimiter() */
#include "../../Log/Errors.h" /** errors macros */
#include "../DataModel/DataMetaInfo.h" /** DataInfo */
#include "../DataModel/DataModel.h" ///TODO!!! ��-�� DataCell! ������, ��������� ��������������� �������!
#include "../RepoKeysManager/key.h" /** Key */
#include <queue>          // std::queue
#include <regex>

/**
 MACRO to parse lists of repo-indexes
*/
#define REPO_INDEXES_DELIMITER ';'
#define REPO_INDEX_FILTERS_DELIMITER ':'
/** Macro to parse (split) the items in filters list as string retrieved from cfg-file */
#define FILTER_TOKENS_DELIMITER '.'

/*!
  Splits allRepoKeys As Single String and put RepoKeys to vector of strings
  PARAMS [IN]  allRepoKeysAsSingleString -- input list of src indexes as string
  PARAMS [OUT] keys -- vector of the list's items
  RETURNS _RC_SUCCESS_ if success,
          _ERROR_WRONG_ARGUMENT_ - otherwise
BAD!     \return: - -1 - fail; number of keys - success
*/
int DataManagerGrammar::splitAllRepoKeysAsSingleString(std::string allRepoKeysAsSingleString, std::vector<std::string> &keys)
{
#ifdef _DEBUG_
   std::cout << allRepoKeysAsSingleString << '\t' << keys.size() << std::endl;
#endif // _DEBUG_
    if(allRepoKeysAsSingleString.empty())
        return _ERROR_WRONG_ARGUMENT_;
///TODO!!! how allRepoKeysAsSingleString is copied to keys[0] right here????
    size_t sum = 0, current_pos = 0;
    std::string delimiters = DEFAULT_DELIMITERS;
    std::string tmp;
    for (unsigned int i = 0; i < allRepoKeysAsSingleString.length(); ++i)
    {
        if(allRepoKeysAsSingleString[i] == REPO_INDEXES_DELIMITER)
        {
            sum++;
            tmp = allRepoKeysAsSingleString;
            tmp.erase(i,tmp.length());
            tmp.erase(0,current_pos);
            TrimStr(tmp,delimiters);
            keys.push_back(tmp);
            current_pos = i+1;
        }
    }
    TrimStr(allRepoKeysAsSingleString.erase(0,current_pos),delimiters);
    keys.push_back(allRepoKeysAsSingleString);
    return _RC_SUCCESS_;
}

int DataManagerGrammar::splitRepoKeysAndFilers(const std::string& keysAndFilters, std::string& repoKeys, std::string& filters)
{
    size_t pos = keysAndFilters.find_first_of(REPO_INDEX_FILTERS_DELIMITER);
    if (pos != std::string::npos)
    {
         splitStringByDelimiter(keysAndFilters, repoKeys, filters, REPO_INDEX_FILTERS_DELIMITER);
    }
    return _RC_SUCCESS_;
}

/**
  * Filter Grammar Stuff: makes filter prefix for given Metadata (DataInfo)
  * Namely, it concatenates dataCreatorName, dataModelName and dataSetName
  * into the string with default FILTER_TOKENS_DELIMITER between those tokens
  PARAMS [IN] pDI -- ptr to metadata
  RETURNS filterPrefix as std::string
*/
std::string DataManagerGrammar::makeFilterPrefix(const DataInfo *& pDI)
{
    std::string filterPrefix = pDI->m_dataCreatorName;
    filterPrefix += FILTER_TOKENS_DELIMITER;
    filterPrefix += pDI->m_dataModelName;
    filterPrefix += FILTER_TOKENS_DELIMITER;
    filterPrefix += pDI->m_dataSetName;
    return filterPrefix;
}

/** Filter Grammar Stuff : makes fullname of single dataset.
  * Namely, it concatenates given filter prefix for that dataset and its datasetTrackName
  * (filter prefix is concatenation of THREE tokens dataCreatorName, dataModelName and dataSetName
        into the string with default FILTER_TOKENS_DELIMITER between those tokens)
  PARAMS [IN] filterPrefix -- given filter prefix for that dataset
  PARAMS [IN] pDI -- ptr to metadata
  PARAMS [IN] datasetIndex -- the index of the dataset into DataCell.values, the full of which is required
  RETURNS full Dataset Name as std::string
  */
std::string DataManagerGrammar::makeFullDatasetName (const DataInfo *& pDI, const std::string& filterPrefix, size_t datasetIndex)
{
    if (pDI->m_dimension <= datasetIndex)
    {
        return std::string();
    }

    return std::string(filterPrefix) + FILTER_TOKENS_DELIMITER + pDI->m_dataSetTrackNames[datasetIndex];
}

/** Old filter (param.param(new_param).param.param) replace on
  * new filter (param.new_param.param.param).
  * PARAMS [IN] filter -- given filter (param.param(new_param).param.param)
  * PARAMS [IN] replace_str -- new param in filter (new_param)
  * PARAMS [IN] index -- the index of the param in filter, the numbering starts with 1 and ends 4
  */
int DataManagerGrammar::replace_filter (std::string& filter, const std::string& replace_str, int index)
{
    auto nthOccurrence = [](const std::string& str, const char separebale, int nth)
    {
        auto pos = 0u;
        auto count = 0;

        while (count != nth)
        {
            pos += 1;
            pos = str.find(separebale, pos);
            if (pos == std::string::npos)
                return _ERROR_WRONG_ARGUMENT_;
            count++;
        }

        return  _ERROR_WRONG_ARGUMENT_;//WHAT DOES IT MEAN? static_cast<int>(pos);
    };


    auto start = index == 1 ? 0u : nthOccurrence(filter, FILTER_TOKENS_DELIMITER, index - 1) + 1;
    auto end = index == 4 ? filter.size() : nthOccurrence(filter, FILTER_TOKENS_DELIMITER, index);
///TODO!!! � ��� �� �� ����� ����� ����������? ���� � ��� ������ auto, � ������� �������� �� int, ������ �� ������ �� ����, �� �������
    if ((start == -1) || (end == -1))
    {
        return _ERROR_WRONG_ARGUMENT_;
    }

    filter.replace(filter.begin() + start, filter.begin() + end, replace_str.begin() + 1, replace_str.end() - 1);
    return _RC_SUCCESS_;
}

/** Validation new param of filter.
  * PARAMS [IN] this -- point to class

  * PARAMS [IN] key -- key of the data to retrieved from the repository
  * PARAMS [IN] subfilter -- new param in filter (new_param)
  * PARAMS [IN] indexSubfilter -- the index of the param in filter, the numbering starts with 1 and ends 4

  * RETURNS _RC_SUCCESS_ if subfilter is correct,
  * _ERROR_WRONG_INPUT_DATA_ otherwise
  */
///TODO!!! ���������� ���, ����� � ���������� �� ����� ���� ���������� ��������� �� �����������! ���������� �� ������ ���� �������� �� ����������� ���������� �����������!
int DataManagerGrammar::validation (Key* & key, std::string & subfilter, int indexSubfilter, const DataModel * repo)
{
    /**
    ��������� ������� � ��������� ��������� (������� ������) �������� ������ ������.
    ��� ������� ������������ ��� �������� (�������) ����� �� ������� ���������
    �������� ������ ������
    */
    std::queue<int> keysIndex;

    auto indexesOfData = key->getIndexesOfData();
    for (auto const & idx : indexesOfData)
    {
        keysIndex.push(idx);
    }
    /**
    ���� ������� ��������� �������� ������ ������ �� �����������
    (������� �� ������  ������������ ������� ������ �� �����������)
    */
    while (!keysIndex.empty())
    {
        auto keyIndex = keysIndex.front();
        keysIndex.pop();
///TODO!!! � ����������, ������� �� ���������� ��� ������������� ������� ������, �� ������ ���� ������� ������������� � ���������� ���������� ������ �����������
        DataCell * pDataCell = nullptr;
        /** returns ptr to the data cell from the repository
        int index - index of the data cell to be retrieved from the repository by key (only! no filter applied)
        ����� �������� ��������� �� ��������� ��� ������������ ������ ������,
        ������� � ������ ������ �������� � �����������.
        � ������ ��� ���������� ����������� ����� ���� �� ����������!
        */
        auto rc = repo->getDataWithIndex(keyIndex - 1, pDataCell);
        if (rc != _RC_SUCCESS_)
        {
            return _ERROR_WRONG_ARGUMENT_;
        }

        std::string compareString;
///TODO!!! we BADLy need enum to do with single elements of metadata
        if (indexSubfilter == 1)
        {
            compareString = pDataCell->m_di->m_dataCreatorName;
        }
        else if (indexSubfilter == 2)
        {
            compareString = pDataCell->m_di->m_dataModelName;
        }
        else if (indexSubfilter == 3)
        {
            compareString = pDataCell->m_di->m_dataSetName;
        }
        else if (indexSubfilter == 4)
        {
///TODO!!! BUG : WHY 0 ONLY? there are more then 1 TRACK!
            compareString = pDataCell->m_di->m_dataSetTrackNames[0];
        }
        /**
        � ������� �� ����� ����� ���������, �������
        */
        if (!compareString.compare(subfilter))
        {
            return _RC_SUCCESS_;
        }

        indexesOfData = pDataCell->m_k->getIndexesOfData();
        for (auto const & idx : indexesOfData)
        {
            if (idx == 1)
            {
                continue;
            }
            keysIndex.push(idx);
        }
    }
    return _ERROR_WRONG_ARGUMENT_;
}

/** Validation new param of filter.
  * PARAMS [IN] validation -- function validation

  * PARAMS [IN] key -- key of the data to be retrieved from the repository
  * PARAMS [OUT] currentFilter -- filter
  */
//    auto checkFilterAndReplace = [validation](Key *& key, std::string & currentFilter) {
///TODO!!! ���������� ���, ����� � ���������� �� ����� ���� ���������� ��������� �� �����������! ���������� �� ������ ���� �������� �� ����������� ���������� �����������!
int DataManagerGrammar::check_replace_filter (Key* & key, std::string & currentFilter, const DataModel * repo)
{
    /**
    \\w - ���� alphanumeric ������ (��������� ����� ��� �����), �� ��, ��� � [a-zA-Z0-9].
    ��� ��� ���������?
    ��������,
    (\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?)
    � ������ ���������� ����� ������ ���� ������� ������, � ��������� ����������� ��� �����������.
    � ��� ��� ������� ����������.
    ������ ������ ����������? ������ ��� ����� �������� ����� ����� ����� ��������.
    */
    static std::regex base_regex(
        "(\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?)|"
        "(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?)"
        "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?)"
        "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)))"
    );
    std::smatch base_match;

    if (std::regex_match(currentFilter, base_match, base_regex))
    {
        for (auto i = 0u, k = 0u; i < base_match.size(); ++i)
        {
            std::ssub_match sub_match = base_match[i];

            auto size = sub_match.str().size();
            k = (size == currentFilter.size() ? i : k);
            if (size == 0 || size == currentFilter.size())
            {
                continue;
            }

            std::string subfilter(sub_match.str());
            subfilter = subfilter.substr(1, size - 2);
            std::cout << "Submatch " << i - k << ": " << subfilter << '\n';

            auto rc = validation(key, subfilter, i - k, repo);
            if (rc == _RC_SUCCESS_)
            {
                DataManagerGrammar::replace_filter(currentFilter, subfilter, i - k);
            }
        }
    }
    return _RC_SUCCESS_;
}

/** Make two filters from input one.
  * PARAMS [IN|OUT] currentFilter -- input filter, retrieved from config-file,
  *                                   current filter to search for data track as output (all the brackets (...) removed from input filter)
  * PARAMS [OUT]    newFilter -- filter to search in data track history as output (to be made from input currentFilter by replacement tokens before brackets (...) with tokens into (...))
  */
int DataManagerGrammar::check_replace_filter(std::string & currentFilter, std::string & newFilter)
{
    static std::regex base_regex("(\\w+(\\(\\w+\\)).\\w+(\\(\\w+\\))?.\\w+(\\("
                                 "\\w+\\))?.\\w+(\\(\\w+\\))?)|"
                                 "(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\)).\\w+(\\("
                                 "\\w+\\))?.\\w+(\\(\\w+\\))?)"
                                 "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+("
                                 "\\(\\w+\\)).\\w+(\\(\\w+\\))?)"
                                 "|(\\w+(\\(\\w+\\))?.\\w+(\\(\\w+\\))?.\\w+("
                                 "\\(\\w+\\))?.\\w+(\\(\\w+\\)))");
    static std::regex r("(?=\\((\\w+)\\))");

    if (std::regex_match(currentFilter, base_regex))
    {
        std::string oldFilter = currentFilter;
        newFilter = currentFilter;
        for (auto begin = std::sregex_iterator(currentFilter.begin(), currentFilter.end(), r),
                end = std::sregex_iterator(); begin != end; ++begin)
        {
            auto substring = (*begin)[1].str();
            std::cout << substring << std::endl;

            auto rex_for_old("\\(" + substring + "\\)");
            std::cout << rex_for_old << std::endl;

            std::regex e_for_old(rex_for_old);
            oldFilter = std::regex_replace(oldFilter, e_for_old, "");
            std::cout << oldFilter << std::endl;

            auto rex_for_new("\\w+\\(" + substring + "\\)");
            std::cout << rex_for_new << std::endl;
            std::regex e_for_new(rex_for_new);
            newFilter = std::regex_replace(newFilter, e_for_new, substring);
            std::cout << newFilter << std::endl;
        }
        currentFilter = oldFilter;
    }
    return _RC_SUCCESS_;
}

#ifndef DEMONSTRATOR_H
#define DEMONSTRATOR_H

#include "../DataModel/DataModel.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <ostream>

class Demonstrator
{
    public:
        Demonstrator();
        virtual ~Demonstrator();

        void showDataInfo(DataInfo* &di);
        void showKey(Key* &k);
        void showCell(DataCell* & c);
        void setStream(std::stringstream* str);
        void setDefaultStream();

    protected:

    private:
        std::ostream* fp;
};

#endif // DEMONSTRATOR_H

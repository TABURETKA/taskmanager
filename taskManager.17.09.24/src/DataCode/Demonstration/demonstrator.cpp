#include "demonstrator.h"
#include "../../CommonStuff/Defines.h" /** delimiter macros */

Demonstrator::Demonstrator()
{
    //ctor
    this->setDefaultStream();
}

Demonstrator::~Demonstrator()
{
    //dtor
}

void Demonstrator::showDataInfo(DataInfo* &di)
{
	*fp << "DataInfo: startInfo" << std::endl;
	*fp << "Creator: " << di->m_dataCreatorName << std::endl;
	*fp << "Model: " << di->m_dataModelName << std::endl;
	*fp << "DataSetName: " << di->m_dataSetName << std::endl;
	*fp << "dimension: " << di->m_dimension << std::endl;
	*fp << "Values: " << std::endl;
	for (size_t j = 0; j < di->m_dimension; ++j)
	{
		*fp << " TrackName: " << di->m_dataSetTrackNames[j];
		*fp << std::endl;
		*fp << " Size:   " << di->m_dataSetSizes[j] << "   Values:  ";
		for (size_t l = 0; l < di->m_dataSetSizes[j]; ++l)
			*fp << "  " << di->m_dataSet[j][l] << "  ";
		*fp << std::endl;
	}
	*fp << "DataInfo: endInfo" << std::endl;
	return;
}

void Demonstrator::showKey(Key* &k)
{
	*fp << "Key: [ " << k->getIndex() << " [ ";
	for (unsigned int j = 0; j < k->getIndexesOfData().size(); ++j){
		if (j > 0)
			*fp << DELIMITER_LIST_ITEMS;
		*fp << k->getIndexesOfData()[j];
	}
	*fp << "] ]";
	*fp << std::endl;
	return;
}

void Demonstrator::showCell(DataCell* & c)
{
	*fp << std::endl << "<-$--" << std::endl;
	this->showKey(c->m_k);
	this->showDataInfo(c->m_di);
	*fp << "Comment:   " << c->m_comment;
	*fp << std::endl << "--$->" << std::endl;
	return;
}

void Demonstrator::setStream(std::stringstream* str)
{
    this->fp = str;
}

void Demonstrator::setDefaultStream()
{
    this->fp = &std::cout;
}

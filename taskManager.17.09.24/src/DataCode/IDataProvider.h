#ifndef __IDATAPROVIDER__H__633569422542968750
#define __IDATAPROVIDER__H__633569422542968750

// DON'T pollute global scope! using namespace std;
/** forward declaration */
class Logger;
/** classes interfaces */

/**
����������� ��������� IDataProvider, ������� ������ ����������� ���,
��� ������ ���� ������ � ����������� (��������� ��������� ������, ������, ����������)
*/
class IDataProvider
{
public:
    virtual ~IDataInfoProvider() = default;


    /** ���������� ������ ������� ���������� ��� DI, ������� ��� ������ ������ ������.
    � ������� ���������� getEstimatesAsDataInfoArray - ��� ������ ������� ���������� � DataInfo**
    � ����� ������ �������� ������ ��� ��� ���������,
    ����� ��� � ��� ������ ��������� ������ DataInfo ������ ���������� �������
    PARAM [OUT] dimension -- total of DataInfo in the array DI[] the DataInfoProvider can make and return
    */
///IAA: ����     virtual int getDimensionOfPtrDI(size_t & dimension) const = 0;
/// ��������� ��, ��� ��������� Dimension Of Ptr �� ��������, ������� ������������, �� ���� ������� ���� �����������
    int getDimensionOfDIarray(size_t& dimension) const=0;

///IAA: ��� ��� ������, � ����� ���������� �������� � ����� �����, � ������ �� ������ �� ����������,
/// ������ ��� ����� ��� ������, �.�. �� �� ������ ������ ������ ������� - ������� ������ ����� 5 ������� (4-� ��� �����)
///    virtual int getDataInfo(size_t itemIndex, DataInfo::ptr_data_info_type & dataInfo) const = 0;
///    virtual int getDataInfoArray(boost::shared_ptr<DataInfo::ptr_data_info_type[]> & dataInfoArray) const = 0;


    /** ���������� ������ ������� ������ �� ����� DI, ������� ��� ������ ������ ������.
    � ������� ���������� getEstimatesAsDataInfoArray - ��� ������ ������� ���������� � DataInfo**
    � ����� ������ �������� ������ ��� ��� ���������,
    ����� ��� � ��� ������ ��������� ������ DataInfo ������ ���������� �������
    PARAM [IN] dimension -- total of DataInfo in the array DI[] the DataInfoProvider should return
    PARAM [OUT] ptrToPtrDI -- DI array to initialized into getDIarray
    */
    int getDIarray(size_t dimension, DataInfo** ptrToPtrDI) const=0;

    /** ���������� ������ ������� ������������ DI, �� ���� ��������� DI[], ������� ��� ������ ������ ������.
    DataInfo* � ����� ������ �������� ������ ��� ���� ���������
    ����� ��� � ��� ������ ��������� ������ DataInfo ������ ���������� �������
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
    PARAM [OUT] ptrToDI -- DI array to initialized into getDIarray
    */
    int getSingleDI_ByDIindex(size_t DIindex, DataInfo* ptrToDI) const=0;

/// ��� ��������� ������, ������ �� getDataSetByDIindexAndDSindex() - ���, �� ����, ������������ ���������� DataInfo,
/// � ��� ���� ��������, ��� �� �������� ������ ���������� ��� � �������� ���������������� DI
/// ��������� ������ �� ���������� �����, �� � ����������
    /**
     * To get DataSet name for given DI[DIindex]
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
    PARAM [OUT] refDSName -- ref to DataSet name for given DI[DIindex]
     */
    virtual int getDataSetName(size_t DIindex, boost::string_ref refDSName) const = 0;
/// ������ ���������, ����������, ����, � ���� �� �������� ������ ����� ����
    /**
     * To get creator name for given DI[itemIndex]
     * ....
     */
    virtual int getCreatorName(size_t DIindex, boost::string_ref refCreatorName) const = 0;

    /**
     * To get model name for given DI[itemIndex]
     * ....
     */
    virtual int getModelName(size_t DIindex, boost::string_ref refCreatorName) const = 0;

    /**
     * to get dimension of m_dataSetSizes, m_dataSetTrackNames, m_DataSets
     * ...
     */
    virtual int getDimensionOfDataSetsByDIindex(size_t DIindex, size_t& dimension) const = 0;

///IAA: ����� �� ���������� ���������� �� �������� � ����������, � ���� ���� ����, ��� �������� � ���������, ��� ����� ��� ��� � �����������-�����������
    virtual int getDataSetTrackNameByDIindexAndDSindex(size_t DIindex, size_t  dataSetIndex, char*& dsTrackName) const = 0;

///IAA:: ����� �� �������������, �.�. DataSetSize Of Track ���� ���������, � �� ���� ���� ����������������, �.�. ����������, ��� �� �� ����������
    virtual int getDataSetSizeOfTrackByDIindexAndDSindex(size_t DIindex, size_t  dataSetIndex, size_t & size) const = 0;

    virtual int getPtrToTrack(size_t DIindex,
                              size_t dataSetIndex,
                              bool & disposeMemoryAfterProccesing,
                              boost::shared_ptr<double *> ptrTrack) const = 0;
///IAA: ���� ������� ���� ������� ��������, ���� �� ��� ����������, �� � ����� ����� ����� ���� �� ������������ ���-�� ����������� ��������
/// (�.�. ���� DI �� ������� ��������� DI[], ��� ����������/������ ��� � ������ ������ ����������)
/// ��������� �� ��� �����������, ��� ������-�� ����������� ���� ��������� �������� � ����������/???������ �������� DI
    /**
    to get creator name for given DI[DIindex]
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
??? ��� ����� �������� �����, ������ ��� ������ �� ��������� *creatorName ������ �������� ��� ������ getCreatorName()
    PARAM [OUT] * creatorName -- creatorName as const char*
    ������������
    const char * creatorName = getCreatorNameByDIindex(size_t DIindex) const=0;
    ���� ������ � ����������, �� ������ nullptr
    */
    int getCreatorNameByDIindex(size_t DIindex, char** creatorName) const=0;

    /**
    to get model name for given DI[DIindex]
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
??? ��� ����� �������� �����, ������ ��� ������ �� ��������� *modelName ������ �������� ��� ������ getModelName()
    PARAM [OUT] * modelName -- modelName as const char*
    ������������
    const char * modelName = getModelNameByDIindex(size_t DIindex) const=0;
    ���� ������ � ����������, �� ������ nullptr
    */
    int getModelNameByDIindex(size_t DIindex, char** modelName) const=0;

    /**
    to get DataSet name for given DI[DIindex]
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
??? ��� ����� �������� �����, ������ ��� ������ �� ��������� *dsName ������ �������� ��� ������ getDataSetName()
    PARAM [OUT] * dsName -- Dataset name as const char*
    ������������
    const char * dsName = getDataSetNameByDIindex(size_t DIindex) const=0;
    ���� ������ � ����������, �� ������ nullptr
    */
    int getDataSetNameByDIindex(size_t DIindex, char** dsName) const=0;

    /**
    to get dimension of m_dataSetSizes,  m_dataSetTrackNames,  m_DataSets
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
    PARAM [OUT] dimension -- total of m_DataSets (and/or m_dataSetSizes, m_dataSetTrackNames) in the DI[DIindex]
    */
    int getDimensionOfDataSetsByDIindex(size_t DIindex, size_t& dimension) const=0;

    /**
    to get DataSet Track name for given DI[DIindex] and m_DataSets[dataSetIndex]
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
    PARAM [IN] dataSetIndex -- index of DataSet < dimension we get from getDimensionOfDataSetsByDIindex(...)
    PARAM [OUT] * dsTrackName -- track name as const char*
??? ��� ����� �������� �����, ������ ��� ������ �� ��������� *dsTrackName ������ �������� ��� ������ getDataSetTrackName()
    ������������
    const char * dsTrackName = getDataSetTrackNameByDIindexAndDSindex(size_t DIindex, size_t  dataSetIndex) const=0;
    ���� ������ � ����������, �� ������ nullptr
    */
    int getDataSetTrackNameByDIindexAndDSindex(size_t DIindex, size_t  dataSetIndex, char** dsTrackName) const=0;


    /**
    to get m_dataSetSizes[dataSetIndex] as size of single m_DataSets[dataSetIndex]
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
    PARAM [IN] dataSetIndex -- index of DataSet < dimension we get from getDimensionOfDataSetsByDIindex(...)
    PARAM [OUT] size -- size of of single DI[DIindex].m_DataSets[dataSetIndex]
    */
    int getSizeOfDataSetByDIindexAndDSindex(size_t DIindex, size_t dataSetIndex, size_t& size) const=0;


    /**
    ??? �������� ��� ��� �������? ����� � �����������, ��� ���������� ��� ������ ������� �� ������������ ������, ��� ������� � ������ ���������� ������� ���. IDataProvider, �.�. ��� �� ���������� � ��������� DI
    �.�. ����� ����� ����, ��� � ����� ��������� �����

    to get data of single m_DataSets[dataSetIndex] for given DI[itemIndex]
    PARAM [IN] DIindex -- index of DI < dimension we get from getDimensionOfDIarray(...)
    PARAM [IN] dataSetIndex -- index of DataSet < dimension we get from getDimensionOfDataSetsByDIindex(...)
    PARAM [IN] size -- size of data (already allocated)
    PARAM [OUT] data -- COPY of DATA from single DI[DIindex].m_DataSets[dataSetIndex]
    */
    int getDataSetByDIindexAndDSindex(size_t DIindex, size_t dataSetIndex, size_t size, double* data) const=0;
/// ��� �����, ���� ��������� ����� �� ���������� � ������ �����  
/// ����� ������ ��� ��� ������ ���������� � ����� ������� getDataSetByDIindexAndDSindex, �.�. ������ ��� ������ deep copy

    int getDataSetByDIindexAndDSindex(size_t DIindex,
                                      size_t dataSetIndex,
                                      bool & disposeMemoryAfterProccesing,
                                      boost::shared_ptr<double *> ptrTrack) const=0;

protected:
    IDataInfoProvider() = default;
    IDataInfoProvider(const IDataInfoProvider &) = delete;
    IDataInfoProvider & operator=(const IDataInfoProvider &) = delete;

    size_t dimensionDIarray; /// <-- �� ��������� 0, ����� ����� ���� ������ ������, ���� ����������� ����� ������������������� ���������� ����� IDataProvider
/// ���� ������������, ��� � ���� DI[] ���� � ��� �� creator. �� ��� ����� ����� �������� �� const char ** ����������� dimensionDIarray
    const char * creatorName;
    void setCreatorName(const char* creatorName) =0;

    /// ������ const char ** ����������� dimensionDIarray
    const char ** modelNames;
    void setNodelName(size_t DIindex, const char* modelName) =0;

    /// ������ const char ** ����������� dimensionDIarray
    const char ** dsNames;
    void setDatasetName(size_t DIindex, const char* dsName) =0;

    /// ������ ����������� dimensionDIarray
    size_t * dimensionsOfDatasets;
    int setDimensionOfDataSetsByDIindex(size_t DIindex, size_t dimension) =0;


    /// ??? �������� �������� ������ � ���, � ��� ������� ����� ������ ������� DI. ����� ����� �����������? �� �������� � ��������� � DI?
    int setDataSetTrackNameByDIindexAndDSindex(size_t DIindex, size_t  dataSetIndex, const char * dsTrackName) =0;

    /// ��������, ����� �������/����� ������� ����� � ��� ����� ��������� �� ������ ����� �����, �������� ����� � ���-�����, ��� ��� ������������ DI,
    /// ??? �� ��� ����� ������� ������� ����� �� ������ getSizeOfDataSetByDIindexAndDSindex(...)
    int setSizeOfDataSetByDIindexAndDSindex(size_t DIindex, size_t dataSetIndex, size_t size) =0;
    int setDataSetByDIindexAndDSindex(size_t DIindex, size_t dataSetIndex, size_t size, double const * const src_data) =0;

private:
    /** assign and copy CTORs are disabled */
    IDataProvider(const IDataProvider&) = delete;
    void operator=(const IDataProvider&) = delete;
}

#endif

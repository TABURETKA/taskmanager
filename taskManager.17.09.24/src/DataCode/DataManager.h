#ifndef MANAGER_H
#define MANAGER_H

#include "DataModel/DataModel.h"
#include "RepoKeysManager/key.h"

#define _DEBUG_

#ifdef _DEBUG_
#include "Demonstration/demonstrator.h"
#endif
/** IAA: put definitions of repo data semantics
 here (in DataManager.h)
 because that semantics is required by Task, Estimator and Model.
 So, we include DataManager.h into Task,
 and into those Estimators and Models which require their input data and/or their parameters from repo.
*/
enum RepoDataSemantics{
  EstimatorData  =0,
  EstimatorParams=1,
  ModelParams    =2,
  FAKE_REPO_DATA_SEMANTICS
};
/** The structure that stores information about the indexes of raw data  for estimator estimator_name. */
typedef struct tagEstimatorSources
{
///BUG!!! memory leaks! ADD DTOR to clear m_estimator_name and m_indexesOfSrcData
    ~tagEstimatorSources(){
        m_estimator_name.clear();
        m_indexesOfSrcData.clear();
    }
    /** m_indexOfOwnerWithGiven_estimator_name - first part of surrogate key for the struct instance EstimatorSources,
    It identifies the instance of estimator
    in case at some moment we have two or more running estimators with the same "m_estimator_name"
    */
    size_t m_indexOfOwnerWithGiven_estimator_name;
    /** m_estimator_name - second part of of surrogate key for the struct instance EstimatorSources */
    std::string m_estimator_name; /** name of running estimator */
    std::vector<int> m_indexesOfSrcData; /** repo's indexes of raw input data (indexes of separate TRACKS!) for m_indexOfOwnerWithGiven_estimator_name-th estimator with given "estimator_name" */
} EstimatorSources;

class DataManager
{
    public:
        DataManager();
        virtual ~DataManager();

        int setNewDataToRepository(DataInfo* &di, std::string parents = "[ 0 ]", std::string comment = "No comment");

        int getData(std::string key, DataInfo* &di);
        unsigned int repSize();

        int setSourcesForEstimator(std::vector<int> parents, std::string estimator_name);
///TODO!!! side-effect! Why it removes (calls removeSourcesForEstimator(...)) as well???
        std::string getSourcesForEstimator(std::string estimator_name, size_t indexOfOwnerWithGiven_estimator_name = 1);

        int splitAllRepoKeysAsSingleString(std::string string_of_keys, std::vector<std::string> &keys);
        int getIndexFromKey(std::string key, int& index);

#ifdef _DEBUG_
        /** For demonstration, DEBUGGING purposes ONLY! */
        void showAllMap(bool full_mode = true);
        void showDataInfo(DataInfo* &di);
        void showKey(Key* &k);
        void setDemonstrationStream(std::stringstream* str) { this->demons->setStream(str); }
        void setDefaultDemonstrationStream() { this->demons->setDefaultStream(); }
#endif
    private:
///TODO!!! remove any DataCell from DataManager!
        int prepareDataInfoForDataCell(DataCell* & data, DataInfo* &di);
        int countParentsForEstimator(std::string est_name);
        int removeSourcesForEstimator(std::string estimator_name, size_t indexOfOwnerWithGiven_estimator_name = 1);

    private:
        DataModel* m_rep;
#ifdef _DEBUG_
        Demonstrator* demons;
#endif
        /** vector of EstimatorSources, because not only one estimator may working at the same time (maybe now we can leave only an array parents.parents)
        */
        std::vector<EstimatorSources*> m_indexesOfEstimatorsSources;
};

#endif // MANAGER_H

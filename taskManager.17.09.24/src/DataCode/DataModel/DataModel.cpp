#include <iostream>
#include <string>
#include "DataModel.h"
#include "../Grammar/DataManagerGrammar.h" /** grammar stuff */
#include "../../Log/errors.h" /** error macros */
#include <list> /** std::list */
#include "../../StringStuff/StringStuff.h" /** parseParamList() */
#include <cassert>     /** assert */

///ctor
DataModel::DataModel()
{
    assert(SIZE_CONST > 0);
    this->m_availableSize = SIZE_CONST;
    this->m_massiveOfData.resize(SIZE_CONST, NULL);

    this->m_currentSize = 0;
}
///dtor
DataModel::~DataModel()
{
    for (unsigned int i = 0; i < this->m_currentSize; ++i)
        this->deleteDataCell(m_massiveOfData[i]);
}

unsigned int DataModel::getCurrentSize()
{
    return this->m_currentSize;
}

void DataModel::freeValues(DataCell* & data)
{
	/*
    for (size_t i = 0; i < data->m_di->m_dimension; ++i)
    {
        if(data->m_values[i]){
            delete[] data->m_values[i];
            data->m_values[i] = nullptr;
        }
    }
    delete[] data->m_values;
    data->m_values = nullptr;
	*/
}

void DataModel::deleteDataCell(DataCell* & data)
{
    //this->freeValues(data);
    delete data->m_di;
    delete data->m_k;
    data->m_k = NULL;
    delete data;
    data = NULL;
    return;
}

/**
* Retrieves Metadata (ptr to DataInfo) on subset of Datasets
 which have been stored into repository with given key
 The criteria to select such Datasets are given into filters-string as list with given delimiter
  PARAMS [IN] key -- ref to key of the DataCell, which MUST exist into repository
  PARAMS [IN] filters -- criteria to select Datasets into subset are given as list with given delimiter
  PARAMS [IN] delimiter -- delimiter of separate filters into list, which has been passed as filters-string
  PARAMS [OUT] pDI -- Metadata (ptr to DataInfo) on subset of Datasets
  which have been stored into repository with given key
  RETURNS _RC_SUCCESS_ if success
      _ERROR_WRONG_ARGUMENT_, _ERROR_WRONG_INPUT_DATA_ or _ERROR_NO_ROOM_ otherwise
*/
int DataModel::getDataWithRepoKeyAndFilters(Key* & key, std::string filters, const char delimiter, DataInfo* & pDI)
{
    DataCell * pDataCell = NULL;
    int rc = this->getDataWithKey(key, pDataCell);
    if (rc != _RC_SUCCESS_)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    ///TODO!! Herein Filter Grammar Stuff begins
    std::list<std::string> l = parseParamList(filters, delimiter);
    if (l.size() > pDataCell->m_di->m_dimension)
    {
        return _ERROR_WRONG_ARGUMENT_;
    }
    std::vector<int> indexesOfDatasets;
    const DataInfo * pDI_tmp = pDataCell->m_di;

    rc = fillVectorOfDatasetIndexesByFilter(pDI_tmp, l, indexesOfDatasets, key);
    if (rc)
    {
        return _ERROR_WRONG_INPUT_DATA_;
    }
    ///TODO!! Herein Filter Grammar Stuff ends
    size_t dimension = indexesOfDatasets.size();
    if (!dimension)
    {
        return _ERROR_WRONG_INPUT_DATA_;
    }

	tagDataInfo* pDI_new = new tagDataInfo();
	if (!pDI_new)
	{
		return _ERROR_NO_ROOM_;
	}

	size_t* indexes = new size_t[dimension];
	for (size_t i = 0; i < dimension; ++i)
	{
		indexes[i] = indexesOfDatasets[i];
	}
	rc = copyDataInfo(pDataCell->m_di, pDI_new, indexes, dimension);
	delete[] indexes;
	if (rc == _RC_SUCCESS_)
	{
		pDI = pDI_new;
	}
	return rc;
}

int DataModel::getDataWithKey(Key* & pKey, DataInfo* & pDI)
{
    DataCell* pDC = NULL;
    int rc = this->getDataWithKey(pKey, pDC);
    if (rc != _RC_SUCCESS_)
    {
        return rc;
    }
    tagDataInfo* pDI_new = new tagDataInfo();
    if (!pDI_new)
    {
        return _ERROR_NO_ROOM_;
    }

    rc = copyDataInfo(pDC->m_di, pDI_new);
	if (rc == _RC_SUCCESS_)
    {
        pDI = pDI_new;
    }
    return rc;
}
/*! PRIVATE STUUF! Because it deals with DataCells
  returns ptr to the data cell from the repository
  PARAMS [IN] Key* & key -- key of the data to retrieved from the repository
  PARAMS [OUT] DataCell* &c - result
  Returns: _RC_SUCCESS_ if success,
          _ERROR_WRONG_ARGUMENT_ otherwise
 */
int DataModel::getDataWithKey(Key* & key, DataCell* &c)
{
    for (unsigned int i = 0; i < this->m_currentSize; ++i)
    {
        if (this->m_massiveOfData[i]->m_k->compare(const_cast<const Key*&>(key)) == true)
        {
            c = this->m_massiveOfData[i];
            return _RC_SUCCESS_;
        }
    }
    return _ERROR_WRONG_ARGUMENT_;
}

///TODO!!! Must be PRIVATE STUUF! Because it deals with DataCells
/*!
 PRIVATE STUUF! Because it deals with DataCells
  returns ptr to the data cell from the repository
  PARAMS [IN] int index - index of the data cell to be retrieved from the repository by key (only! no filter applied)
  PARAMS [OUT] DataCell* &c - result
  Returns: _RC_SUCCESS_ if success,
          _ERROR_WRONG_ARGUMENT_ otherwise
*/
int DataModel::getDataWithIndex(unsigned int index, DataCell* &c) const
{
    if (index < this->m_currentSize)
    {
        c = this->m_massiveOfData[index];
        return _RC_SUCCESS_;
    }
    return _ERROR_WRONG_ARGUMENT_;
}

int DataModel::prepareDataInfoForDataCell(DataCell* & ptrDataCell, DataInfo* &ptrDI)
{
	tagDataInfo* ptrDI_copy = new tagDataInfo();
	if (!ptrDI_copy)
	{
		return _ERROR_NO_ROOM_;
	}
	int rc = copyDataInfo(ptrDI, ptrDI_copy);
	if (rc != _RC_SUCCESS_)
	{
		delete ptrDI_copy;
		return rc;
	}
	ptrDataCell->m_di = ptrDI_copy;
	return _RC_SUCCESS_;
}

/*!
    void DataModel::setData(DataInfo* &di, Key* & pKey, std::string &comment)
     \brief:  adds a new entry to the repository
     \params: DataInfo* &di -  ptr to new dataInfo
     \params: Key* & pKey - repo key
     \params: std::string &comment - comment
*/
int DataModel::setData(DataInfo* &di, Key* & pKey, std::string &comment)
{
    DataCell* data = new DataCell;
    if (! data)
    {
        delete pKey;
        return _ERROR_NO_ROOM_;
    }

    if(prepareDataInfoForDataCell(data,di) != _RC_SUCCESS_)
    {
///BUG!!! memory leak!
        delete pKey;
///BUG!!! memory leak!
        delete data;
        return _ERROR_WRONG_ARGUMENT_;
    }
    data->m_comment = comment;
    data->m_k = pKey;

    assert(SIZE_CONST > 0);
    if (this->m_currentSize++ > this->m_availableSize)
    {
        this->m_availableSize += SIZE_CONST;
        this->m_massiveOfData.resize(this->m_availableSize, NULL);
    }
    this->m_massiveOfData[this->m_currentSize - 1] = data;

    return _RC_SUCCESS_;
}

/**
  * Filter Grammar Stuff: loop through the filters and try to find out the matching dataset into DataCell,
  * which DataInfo is provided.
  * If any dataset's fullname matches current filter the index of the dataset populates vector<int>
  PARAMS [IN] pDI -- ptr to metadata
  PARAMS [IN] l -- ref to list of filter as std::list<std::string>
  PARAMS [IN] Key* & key -- key of the data to retrieved from the repository
  PARAMS [OUT] indexesOfDatasets -- the indexes (as std::vector<int>) of the datasets to be filtered from the DataCell
  RETURNS _RC_SUCCESS_ if success
  _ERROR_WRONG_INPUT_DATA_ if required datasets are not found for any filter
*/
int DataModel::fillVectorOfDatasetIndexesByFilter(const DataInfo *& pDI, std::list<std::string> & l, std::vector<int> & indexesOfDatasets, Key* & key)
{
    auto filterPrefix = DataManagerGrammar::makeFilterPrefix(pDI);
    while (l.size()) {
        auto index = -1;
        auto currentFilter = l.front();
/* WRONG PALCE AND WORKFLOW!
///TODO!!! ����� ������� � ��� �� ������ ���������, ��� ������������� ����� ������ ������������� ���������� � �����������?
/// �.�. ��� �������� ���������� ����� � �������, ������� ���������� ��������� ������������ ������������ ������, ��������� � ��������������� ����� DataCell,
/// �� ������������� ����� �������� ������������� ������������?
///TODO!!! ���������� ���, ����� � ���������� �� ����� ���� ���������� ��������� �� �����������! ���������� �� ������ ���� �������� �� ����������� ���������� �����������!
        const DataModel * repo = const_cast<const DataModel *> (this);
        DataManagerGrammar::check_replace_filter(key, currentFilter, repo);
*/
        std::string newFilter;
        for (auto i = 0u; i < pDI->m_dimension; ++i) {
            auto fullNameOfDatasetTrack = DataManagerGrammar::makeFullDatasetName(pDI, filterPrefix, i);
            DataManagerGrammar::check_replace_filter(currentFilter, newFilter);
            auto pos = std::string::npos;
            /// TODO: What the hell with comparison (CS or CI)???

std::cout << "fullNameOfDatasetTrack:" << fullNameOfDatasetTrack << "; currentFilter:" << currentFilter << std::endl;

            if ((pos = fullNameOfDatasetTrack.find(currentFilter)) != std::string::npos)
            {
                index = i;
///TODO!!! rigth after the datatrack has been found start its history check! Right here!
                break;
            }
        }

        if (index < 0) {
            return _ERROR_WRONG_INPUT_DATA_;
        }

        indexesOfDatasets.push_back(index);
        l.pop_front();
    }

    return _RC_SUCCESS_;
}

#ifndef DATAMODEL_H
#define DATAMODEL_H

#include "DataMetaInfo.h"
#include <vector>
#include <list>

#include "../RepoKeysManager/key.h"


#define SIZE_CONST 100

///TODO!!! herein MUST be forward declaration ONLY! struct DataCell;
/// structure for storing one entry ( a multidimensional dataset )
typedef struct
{
    /// data info & data
    DataInfo* m_di;
    ///    the key to the data set
    Key* m_k;
    /// meta info
    std::string  m_comment;
    /// data
///TODO!!! ���������� DataCell � ����� � ������� values � private ������? ����� � ��� ������ ����� ���������!
    //double** m_values;
} DataCell;

class DataModel
{
public:
    DataModel();
    virtual ~DataModel();
    void deleteDataCell(DataCell* & data);
    /// getters
    int getDataWithRepoKeyAndFilters(Key* & key, std::string filters, const char delimiter, DataInfo* & di);
    int getDataWithKey(Key* & pKey, DataInfo* & pDI);
    ///TODO!!! ��-�����, ��������� ���������,���, ��� ����� �������, ��� DataInfo* !
    int getDataWithIndex(unsigned int index, DataCell* &c) const;
    unsigned int getCurrentSize();

    /// setters
//OLD    void setData(DataCell* data);
    int setData(DataInfo* &di, Key* & pKey, std::string &comment);

private:
    void freeValues(DataCell* & data);
    int prepareDataInfoForDataCell(DataCell* & ptrDataCell, DataInfo* &ptrDI);
    ///TODO!!! ��-�����, ��������� ���������,���, ��� ����� �������, ��� DataInfo* !
    int getDataWithKey(Key* & key, DataCell* &c);
    //std::string makeFilterPrefix(const DataInfo*& di);
    //std::string makeFullDatasetName(const std::string& filterPrefix, const DataInfo *& pDI, size_t datasetIndex);
    int fillVectorOfDatasetIndexesByFilter(const DataInfo *& pDI, std::list<std::string> & l, std::vector<int> & indexesOfDatasets, Key *& key);
    unsigned int m_currentSize;
    unsigned int m_availableSize;

    /** ��� ��������� � ������� ������ "�� ������"
    - ��� ������
    */
    std::vector<DataCell*> m_massiveOfData;
};

#endif // DATAMODEL_H

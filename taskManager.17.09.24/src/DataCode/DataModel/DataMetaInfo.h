#ifndef DATAINFO_H
#define DATAINFO_H
 /* std::memset */
#include <cstring>
#include <string>
#include "../../Log/errors.h" /** errors macros */

///structure for storing information about data
typedef struct tagDataInfo
{
///TODO!!! ����������� ������ ���� � ��� ����� ����������, m_dimension, �������, � ����� ��� � m_dataSetSizes!
    tagDataInfo (double ** p): m_dataCreatorName(""),
	                           m_dataModelName(""),
                               m_dataSetName(""),
                               m_dimension(0),
                               m_dataSetTrackNames(nullptr),
                               m_dataSetSizes(nullptr),
							   m_dataSet(p)
        {};
	tagDataInfo()           : m_dataCreatorName(""),
	                          m_dataModelName(""),
                              m_dataSetName(""),
                              m_dimension(0),
                              m_dataSetTrackNames(nullptr),
                              m_dataSetSizes(nullptr),
							  m_dataSet(nullptr)
///TODO!!! ���������, ��� ���! ��������� � p ��������!
    {

	}

	void setValues(double ** p)
	{
		this->m_dataSet = p;

	};

    ~tagDataInfo(){
        if(m_dataSet)
        {
            /** NB ACTUALLY we MUST keep unchanged the array of double
            referenced by di[0]->m_data!
            DON'T DELETE IT! ONLY ASSIGN NULL to di[0]->m_data
            */
            for(size_t i = 0; i < m_dimension; ++i)
            {
                delete [] m_dataSet[i];
                m_dataSet[i] = nullptr;
            }
///BUG!!! the memory has been deleted somehow
			if(m_dataSet) delete [] m_dataSet;
			m_dataSet = nullptr;
        }

        if(m_dataSetSizes) delete [] m_dataSetSizes;
        m_dataSetSizes = NULL;

        if (m_dataSetTrackNames)
        {
            for(size_t i = 0; i < m_dimension; ++i)
            {
                m_dataSetTrackNames[i].clear();
            }
            delete [] m_dataSetTrackNames;
        }
        m_dataSetTrackNames = NULL;
        m_dimension = 0;
    }
    /// the name of data source, for example name of source file or name of Estimator
    std::string m_dataCreatorName;
    /// the name of model
    std::string m_dataModelName;
    /// the name of data set, for example regression location or regression coefficients
    std::string m_dataSetName;
    /// number of data tracks in the dataset
    size_t m_dimension;
    /// the name of each dataset, for example first coefficient, second coefficient
///TODO!!! ��������� �������� ��� ����, ��������!
    std::string* m_dataSetTrackNames;
    /// the sizes of each dataset
    size_t* m_dataSetSizes;
    /** we forbid any changes of dataset's items
    because m_data is reference to the ORIGINAL DATA
    which MUST BE unchanged along the session!  */
///TODO!!! dataSet = NULL
    //double const ** const m_dataSet; // = NULL;
	double ** m_dataSet; // = NULL;
} DataInfo;
/*OLD
inline int freeDataInfo(DataInfo*& di)
{
    if(di->m_dataSet)
    {
        / NB ACTUALLY we MUST keep unchanged the array of double
        referenced by di[0]->m_data!
        DON'T DELETE IT! ONLY ASSIGN NULL to di[0]->m_data
        /
        for(size_t i = 0; i < di->m_dimension; ++i)
        {
            delete [] di->m_dataSet[i];
        }
        di->m_dimension = 0;
        if(di->m_dataSet) delete [] di->m_dataSet;

        if(di->m_dataSetSizes) delete [] di->m_dataSetSizes;
        di->m_dataSetSizes = NULL;

        if (di->m_dataSetTrackNames) delete [] di->m_dataSetTrackNames;
        di->m_dataSetTrackNames = NULL;
    }
    delete di;
    di = NULL;
    return _RC_SUCCESS_;
}
*/
inline int freeArrayOfDataInfo(size_t dimension, DataInfo**& di)
{
    for(size_t j=0; j < dimension; ++j)
    {
//OLD        freeDataInfo(di[j]);
//OLD        di[j] = NULL;
        for(size_t index=0; index < dimension; ++index)
        {
            if(di[index])
            {
                delete di[index];
                di[index] = nullptr;
            }
        }
    }
    delete [] di;
    return _RC_SUCCESS_;
}

/*
     \brief:  create a copy of DataInfo
     \return: - 0, if success, else 1
     \params: DataInfo* &di1 - old data
              DataInfo* &di2 - new data
 */
inline int copyDataInfo(const DataInfo* di1, DataInfo* &di2, size_t* indexes = NULL, int dimension = 0)
{
	size_t dim;
	if (indexes == NULL)
	{
		dim = di1->m_dimension;
	}
	else
	{
		dim = dimension;
	}

    size_t* ds = new size_t[dim];
    if (! ds)
    {
        return _ERROR_NO_ROOM_;
    }
    std::memset(ds, 0, dim*sizeof(size_t));
    std::string* names = new std::string[dim];
    if (! names)
    {
        delete[] ds;
        return _ERROR_NO_ROOM_;
    }

	di2->m_dataSet = new double*[dim];
	if (!di2->m_dataSet)
	{
		delete[] ds;
		return _ERROR_NO_ROOM_;
	}
	std::memset(di2->m_dataSet, 0, dim*sizeof(double*));


    for(size_t i = 0; i < dim ; ++i)
    {
		if (indexes == NULL)
		{
			ds[i] = di1->m_dataSetSizes[i];
			names[i] = di1->m_dataSetTrackNames[i];
		}
		else
		{
			ds[i] = di1->m_dataSetSizes[indexes[i]];
			names[i] = di1->m_dataSetTrackNames[indexes[i]];
		}

		di2->m_dataSet[i] = new double[ds[i]];
		if (!di2->m_dataSet[i])
		{
			delete[] ds;
			while(i)
			{
			i--;
			if (di2->m_dataSet[i]) delete[] di2->m_dataSet[i];
			}
			delete[] di2->m_dataSet;
			return _ERROR_NO_ROOM_;
		}
		std::memset(di2->m_dataSet[i], 0, ds[i] * sizeof(double));
		for (size_t j = 0; j < ds[i]; ++j)
		{
			if (indexes == NULL)
			{
				di2->m_dataSet[i][j] = di1->m_dataSet[i][j];
			}
			else
			{
				di2->m_dataSet[i][j] = di1->m_dataSet[indexes[i]][j];
			}
		}
    }
    di2->m_dataCreatorName = di1->m_dataCreatorName;
    di2->m_dataModelName = di1->m_dataModelName;
    di2->m_dataSetName = di1->m_dataSetName;
    di2->m_dimension = dim;
    di2->m_dataSetSizes = ds;
    di2->m_dataSetTrackNames = names;
    return _RC_SUCCESS_;
}
#endif

#include <iostream> /* cout */
#include <cstring>
#include <sstream>
#include <iterator>
#include "../DataCode/Grammar/DataManagerGrammar.h" /** data manager grammar */
#include "DataManager.h"
#include "../CommonStuff/Defines.h" /** delimiter macros */
#include "../Log/Errors.h" /** errors macros */

std::string vecToString(std::vector<int> vec);

///TODO!!! This wrong place (wrong file) for utility function vecToString(...)
std::string vecToString(std::vector<int> vec)
{
    std::ostringstream oss;
    const char delimiter = DELIMITER_LIST_ITEMS;
    for(size_t i=0; i < vec.size(); ++i)
    {
        if(i > 0)
            oss << delimiter;
        oss << vec[i];
    }
//std::cout << oss.str() << std::endl;
#ifdef _DEBUG_
// ����� ������ �� �����-�� ��� � �������, ����� "1,�����0"
//    oss.clear();
//    if (!vec.empty())
//    {
//      std::copy(vec.begin(), vec.end()-1, std::ostream_iterator<int>(oss, &delimiter));
//      oss << vec.back();
//    }
//std::cout << oss.str() << std::endl;
#endif
    return oss.str();
}
///ctor
DataManager::DataManager()
{
    this->m_rep = new DataModel();
#ifdef _DEBUG_
    this->demons = new Demonstrator();
#endif
}
///dtor
DataManager::~DataManager()
{
    delete this->m_rep;
#ifdef _DEBUG_
    delete this->demons;
#endif
    for(size_t i = 0u, size = m_indexesOfEstimatorsSources.size(); i < size; ++i)
    {
       delete m_indexesOfEstimatorsSources[i];
       m_indexesOfEstimatorsSources[i] = nullptr;
    }
}


/*!
  Adds a new entry to the repository
  PARAMS [IN] DataInfo* &di - new data to put into repository
///TODO!!! std::string == array???
  PARAMS [IN] std::string parents -- array??? of parents of the data (sources, params)
  PARAMS [IN] std::string comment -- meta information
  Returns: - _RC_SUCCESS_, if success,
          _ERROR_WRONG_ARGUMENT_, _ERROR_NO_ROOM_ otherwise
 */
int DataManager::setNewDataToRepository(DataInfo* &di, std::string parents, std::string comment)
{
    std::vector<int> vp;
    /** Create a unique key:for new estimates
    which we put to repository
    */
    Key* k = NULL;
    if(k->convert(parents,vp) != _RC_SUCCESS_)
        return _ERROR_WRONG_ARGUMENT_;
    k = new Key(vp,this->repSize() + 1);
    if (! k)
    {
        return _ERROR_NO_ROOM_;
    }
    return this->m_rep->setData(di,k,comment);
}

/**
    int DataManager::getData(std::string key, DataInfo* &di)
     returns the STUB (as ptr to DataInfo) to input data which have been stored in the repository beforehand
     PARAMS [IN]: std::string key -- key of the data
     PARAMS [OUT]: DataInfo* &di -- result
     RETURNs: _RC_SUCCESS_ if success,
              _ERROR_NO_ROOM_ or _ERROR_WRONG_ARGUMENT_ otherwise
 */
int DataManager::getData(std::string key, DataInfo* &di)
{
    Key* k = new Key();
    if (! k)
    {
        return _ERROR_NO_ROOM_;
    }
    /** ����� ����� ������� key � ����������� � ��������� � ���!
    ����� �� ����������� key �������� ������� � ������ ����������
    ��������� ��� ������� (�.�. ���������� ��� key) � �������
    */
    std::string repoKey= key;
    std::string filters= "";
    /** So far splitRepoKeysAndFilers() allways returns _RC_SUCCESS_ */
    int rc = DataManagerGrammar::splitRepoKeysAndFilers(key, repoKey, filters);

    rc = k->convertFromString(repoKey);
    if(rc == _RC_SUCCESS_)
    {
///TODO!!! ������ �� ����� ��������� �� �������� � ������� �������� �������� DataModel!
//OLD        DataCell* c;
        DataInfo * pDI = NULL;
        /** ���� ���� �����, �� ������ ����� �� c->values ������������� ������ �� double *, ������� �������� ��������!
        �.�. ����� ����� ������ ������ ��������� (c)
        ��������� getDataWithKey � ��� ����� ���������� - ������� � ���������!
        ������ c �������� DataInfo* � ��� ����� �� ��� �������� ������� � DataModel!
        */
        if(! filters.empty())
        {
            rc = this->m_rep->getDataWithRepoKeyAndFilters(k, filters, DELIMITER_LIST_ITEMS, pDI);
        }
        else
        {
            rc = this->m_rep->getDataWithKey(k,pDI);
        }
        if(rc == 0)
        {
            di = pDI;
        }
    }
    delete k;
    return rc;
}
/*!
  Counts the the number of running estimator with given name "est_name"
  PARAMS [IN]: std::string est_name given name of running estimator
  Returns: the number of running estimator with given name "est_name"
 */
int DataManager::countParentsForEstimator(std::string est_name)
{
    int counter = 0;
    for(unsigned int i = 0; i < this->m_indexesOfEstimatorsSources.size(); ++i)
    {
        if(this->m_indexesOfEstimatorsSources[i]->m_estimator_name == est_name)
            counter++;
    }
    return counter;
}
/**
�������� ������, ������� ���������� ������ ����������,
�������������� ��������� � �����������.
������ ������ ��� ������� ����������
����������� ��������� �� ��� � �� �� ����������.
������� (������� ����� �����������) ���� �������� ������ ������ � ��������
��������� ��������� ��� ���������� ��������/����������� ������,
��� ������� ������, ���������� ���� �����������,
����� �� ���������� ��� ������ ��������� � �����������.
������� ������� �������� ������ ����������
������� ��������� �� ����� ��� ������.
��� ����������� � ���� ������, DataManager::setSourcesForEstimator(sources, estimator->toString());
� ���� ������� ������ � ����� parents ����� ������ DataManager,
parents - ������ std::vector<EstimatorSources*> parents (������� ����� � ),
�� ��������� ������ ����������, ����� ��������� �������������
������� �� ���� ���������� ������,
�������������� ����� ������������ ���� ��� ���� ������.
������������� � ������ ����������� ���,
��� � ���� ������ (��� ��������, ��� � �����������) ������������ �����
�������� � ���� ��������������� ������������ - �����������.
� � ����������� ��� ������ ������ ����� �����!
��� ���������� ����� ������ ������ ����������
����� ������� ����� �����������, �� ������� �� ������� �������� ������ ����� ����������.
��� (������� ����� � ��������� �������) ����� ��������:
std::string sources = this->taskManager->getDataManager()->getSourcesForEstimator(estimator->toString());
� ����� ��� ���������� ������ � ������ ������� �����
���������� � ����������� �������
this->taskManager->getDataManager()->setNewData(di[i],sources);

  Put metadata (indexes) of sources/input data for estimator with given name "estimator_name".
  into std::vector parents
  PAAMS [IN/OUT]: std::vector<int> parents -- array of sources' indexes
  PARAMS [IN] std::string estimator_name -- given name of estimator
  Returns: - number of entries in "parents"
 */
int DataManager::setSourcesForEstimator(std::vector<int> parents, std::string estimator_name)
{
    size_t indexOfOwnerWithGiven_estimator_name = this->countParentsForEstimator(estimator_name);
    EstimatorSources* es = new EstimatorSources;
///BUG! memory leak!
    es->m_estimator_name = estimator_name;
    es->m_indexOfOwnerWithGiven_estimator_name = indexOfOwnerWithGiven_estimator_name + 1;
    es->m_indexesOfSrcData = parents;
///TODO!!! ������������� ��� : "Overflow" <- !
///BUG! memory leak!
    this->m_indexesOfEstimatorsSources.push_back(es);
    return es->m_indexOfOwnerWithGiven_estimator_name;
}
/*!
  Removes sources' indexes ONLY in the massive parents for estimator estimator_name & number == number.
///TODO!!! int???
  PARAMS [IN] int number -- index of entry into repository
  PARAMS [IN] std::string estimator_name -- name of estimator which owns given data from repository
  Returns - _RC_SUCCESS_ - if success;
   _ERROR_WRONG_ARGUMENT_ - otherwise
 */
int DataManager::removeSourcesForEstimator(std::string estimator_name, size_t indexOfOwnerWithGiven_estimator_name)
{
    for(unsigned int i = 0; i < this->m_indexesOfEstimatorsSources.size(); ++i)
    {
        if(this->m_indexesOfEstimatorsSources[i]->m_estimator_name == estimator_name)
        {
            if(this->m_indexesOfEstimatorsSources[i]->m_indexOfOwnerWithGiven_estimator_name == indexOfOwnerWithGiven_estimator_name)
            {
                delete m_indexesOfEstimatorsSources[i];
                m_indexesOfEstimatorsSources.erase(this->m_indexesOfEstimatorsSources.begin() + i);
                for(unsigned int j = 0; j < this->m_indexesOfEstimatorsSources.size(); ++j)
                {
                    if(this->m_indexesOfEstimatorsSources[j]->m_estimator_name == estimator_name)
                    {
                        if(this->m_indexesOfEstimatorsSources[j]->m_indexOfOwnerWithGiven_estimator_name > indexOfOwnerWithGiven_estimator_name)
                        {
                           this->m_indexesOfEstimatorsSources[j]->m_indexOfOwnerWithGiven_estimator_name--;
                        }
                    }
                }
                return _RC_SUCCESS_;
            }
        }
    }
    return _ERROR_WRONG_ARGUMENT_;
}
///TODO!!! �������������, ����� std::string � �������� ������������� rc!
/*!
    std::string DataManager::getSourcesForEstimator(std::string estimator_name, int number)

     \brief:  - get sources from the massive parents for estimator estimator_name & number == number.
     \return: - string of sources - success; "error" - fail
     \params: - size_t indexOfOwnerWithGiven_estimator_name - number of entry
                std::string estimator_name - name of estimator
 */
std::string DataManager::getSourcesForEstimator(std::string estimator_name, size_t indexOfOwnerWithGiven_estimator_name)
{
    std::string result = "error";
    for(unsigned int i = 0; i < this->m_indexesOfEstimatorsSources.size(); ++i)
    {
        if(this->m_indexesOfEstimatorsSources[i]->m_estimator_name == estimator_name)
        {
            if(this->m_indexesOfEstimatorsSources[i]->m_indexOfOwnerWithGiven_estimator_name == indexOfOwnerWithGiven_estimator_name)
            {
                result = "[" + vecToString(this->m_indexesOfEstimatorsSources[i]->m_indexesOfSrcData) + "]";
///TODO!!! Check rc = !
                this->removeSourcesForEstimator(estimator_name,indexOfOwnerWithGiven_estimator_name);
                return result;
            }
        }
    }
    return result;
}

/*!
    int DataManager::getIndexFromKey(std::string key, int& index)
    gets (from the dataMap) the index of entry which key == key .
     PARAMS [IN] : key -- key of entry as string
     PARAMS [OUT]: index -- key of entry as int
     RETURNs - _RC_SUCCESS_ if success,
               _ERROR_NO_ROOM_ or _ERROR_WRONG_ARGUMENT_ otherwise
*/
int DataManager::getIndexFromKey(std::string key, int& index )
{
///TODO!!! � ����� ��� ������ ���� �������� � ��������� ����������� Key? ������ �� �� ������ ��� ��� "��������" ������� � ���� ��������� �� � ���! � �� ����� ������ ��� �������������� ������ ������!
    Key* k = new Key();
    if (! k)
    {
        return _ERROR_NO_ROOM_;
    }
    if(k->convertFromString(key) == _RC_SUCCESS_)
    {
///TODO!!! ������ �� "���������" convertFromString �� ������� �� ������ ��������� ������� int? � ����� ����� �� ����� ���� number !
        index = k->getIndex();
        if (index < 0)
        {
            return _ERROR_WRONG_ARGUMENT_;
        }
    }
//old        result = k->getnumber();
    delete k;
    return _RC_SUCCESS_;
}

/*!
    void DataManager::showAllMap(bool full_mode)
     \brief:  shows all the entries from the repository
     \params: bool full_mode - if true shows cell, if false shows only key of entry
 */
#ifdef _DEBUG_
void DataManager::showAllMap(bool full_mode)
{
    unsigned int size = this->repSize();
    for(unsigned int i = 0; i < size; ++i)
    {
///TODO!!! DataManager �� ������ ������ ����� � DataCell!
        DataCell* c;
        int rc = this->m_rep->getDataWithIndex(i,c);
        if (rc != _RC_SUCCESS_)
        {
            return;
        }
        if(full_mode)
            this->demons->showCell(c);
        else
            this->demons->showKey(c->m_k);
    }
    return;
}
#endif
/*!
    void DataManager::showDataInfo(DataInfo* &di)

     \brief:  show a specimen of DataInfo
     \params: DataInfo* &di - specimen of DataInfo
*/
#ifdef _DEBUG_
void DataManager::showDataInfo(DataInfo* &di)
{
    this->demons->showDataInfo(di);
    return;
}
#endif
/*!
    void DataManager::showKey(Key* &k)

     \brief:  show a specimen of Key
     \params: Key* &k specimen of Key
*/
#ifdef _DEBUG_
void DataManager::showKey(Key* &k)
{
    this->demons->showKey(k);
    return;
}
#endif
/*!
  Returns the size of the repository,
  i.e. total of ???
 */
unsigned int DataManager::repSize()
{
    return this->m_rep->getCurrentSize();
}

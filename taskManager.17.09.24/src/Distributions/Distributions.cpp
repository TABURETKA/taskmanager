//#include <stdlib.h>
#include "distributions.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <time.h> /* time() */
#include "gsl/gsl_randist.h"
#include "../Log/errors.h" // rc codes
#include <float.h> /* DBL_MIN */
Distributions::Distributions()
{}
Distributions::~Distributions()
{}
/** We use GSL::mersenne-twister generator */
gsl_rng * Distributions::generator = 0;

int Distributions::InitGenerator()
{
    if (! Distributions::generator)
    {
        Distributions::generator = gsl_rng_alloc(gsl_rng_mt19937);
        if (!Distributions::generator)
        {
            ///TODO!!! handle allocation error somewhow!
            ///For example: Throw user-defined exception!
        }
        else
            gsl_rng_set(Distributions::generator, static_cast<unsigned long int>(time(nullptr)));
    }
    return _RC_SUCCESS_;
}

int Distributions::FreeGenerator()
{
    if (Distributions::generator)
    {
        gsl_rng_free (Distributions::generator);
    }
    return _RC_SUCCESS_;
}

double Distributions::Heviside(double x)
{
  if (x > 0)
    return 1.;
  return 0;
}

/*************************************
*          Uniform r.v. generator
*************************************/
double Distributions::Uniform()
{
  /** Merely for safety-sake! */
  if (! Distributions::generator)
      Distributions::InitGenerator();
  return gsl_ran_flat(Distributions::generator, 0, 1);
/// DON'T USE! ((rand() + 0.5) / (RAND_MAX + 1.0));
}
/*************************************
*
**************************************/
size_t Distributions::Poisson(double lambda)
{
    return gsl_ran_poisson(Distributions::generator, lambda);
}
/*************************************
*         Gaussian  r.v. generator
*************************************/
double Distributions::Normal(double mean, double sigma)
{
    double x1 = Uniform();
    double x2 = Uniform();
    double norm = sqrt(-2 * log(x1)) * sin(2 * M_PI * x2);

    return mean + sigma * norm ;
}
/*************************************
*         Exponential  r.v. generator
*************************************/
double Distributions::Exponential(double lambda)
{
    return -1. / lambda * log(1. - Uniform());
}
/*************************************
*         Pareto  r.v. generator
*************************************/
double Distributions::Pareto(double x_min, double k)
{
    return x_min / pow(1. - Uniform(), 1. / k);
}

static const double rel_error = 1E-12;

/*
double erfc(double x);

double erf(double x)
{
  static const double two_sqrtpi=  1.128379167095512574;        // 2/sqrt(pi)
  if (fabs(x) > 2.2) {
    return 1.0 - erfc(x);        //use continued fraction when fabs(x) > 2.2
  }
  double sum= x, term= x, xsqr= x*x;
  int j= 1;

  do {
    term*= xsqr/j;
    sum-= term/(2*j+1);
    ++j;
    term*= xsqr/j;
    sum+= term/(2*j+1);
    ++j;
  } while (fabs(term/sum) > rel_error);   // CORRECTED LINE

  return two_sqrtpi*sum;
}


double erfc(double x)
{
  static const double one_sqrtpi=  0.564189583547756287;        // 1/sqrt(pi)

  if (fabs(x) < 2.2) {
    return 1.0 - erf(x);        //use series when fabs(x) < 2.2
  }
  if (x < 0) {               //continued fraction only valid for x>0
    return 2.0 - erfc(-x);
  }
  double a=1, b=x;                //last two convergent numerators
  double c=x, d=x*x+0.5;          //last two convergent denominators
  double q1, q2= b/d;             //last two convergents (a/c and b/d)
  double n= 1.0, t;

  do {
    t= a*n+b*x;
    a= b;
    b= t;
    t= c*n+d*x;
    c= d;
    d= t;
    n+= 0.5;
    q1= q2;
    q2= b/d;
  } while (fabs(q1-q2)/q2 > rel_error);

  return one_sqrtpi*exp(-x*x)*q2;
}
*/

/*************************************
*         Gaussian PDF
*************************************/
double Distributions::Phi_PDF(double x, double mu, double sigma)
{
    return 1. / (sqrt(2. * M_PI) * sigma) * exp(-(x - mu) * (x - mu) / (2. * sigma * sigma));
}
/*************************************
*         Gaussian CDF
*************************************/
double Distributions::Phi_CDF(double x, double mu, double sigma)
{
     return 0.5 * ( 1. + erf( (x - mu) / (sqrt(2.) * sigma) ) );
}
/*************************************
*         Pareto PDF
*************************************/
double Distributions::Pareto_PDF(double x, double x_min, double k)
{
    if((x < x_min) || (k < DBL_MIN))
    {
        return NAN;
    }
    double pdf = k;
    for(size_t i=0; i < k; ++i)
    {
        pdf *= (x_min/x);
    }
    pdf /= x;
    return pdf;
}
/*************************************
*         Pareto CDF
*************************************/
double Distributions::Pareto_CDF(double x, double x_min, double k)
{
    if((x < x_min) || (k < DBL_MIN))
    {
        return NAN;
    }
    double survival = 1.;
    for(size_t i=0; i < k; ++i)
    {
        survival *= (x_min/x);
    }
    double cdf = 1. - survival;
    return cdf;
}
/*************************************
*         Poisson CDF
*************************************/
double Distributions::Poisson_CDF(size_t x, double lambda)
{
    double cdf = 0;
    double addend = 1;
    for(size_t i=1; i <= x; ++i )
    {
        addend *= lambda / i;
        cdf += addend;
    }
    cdf *= exp(-lambda);
    return cdf;
}
/*************************************
*         Poisson PDF
*************************************/
double Distributions::Poisson_PDF(size_t x, double lambda)
{
    double pdf = 1.;
    for(size_t i=1; i <= x; ++i )
    {
        pdf *= lambda / i;
    }
    pdf *= exp(-lambda);
    return pdf;
}

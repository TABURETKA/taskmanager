#ifndef _DISTRIBUTIONS_H_
#define _DISTRIBUTIONS_H_
/**
 add search directory gsl-X.XX-[gcc|vs]\include for compiler!
 gsl-X.XX-[gcc|vs]\include MUST HAVE subdirectory "gsl"
*/
#include <gsl/gsl_rng.h>

class Distributions
{
public:
    Distributions();
    ~Distributions();
    static int InitGenerator();
    static int FreeGenerator();
    static double Uniform();
    static size_t Poisson(double lambda);
    static double Normal(double mean, double sigma);
    static double Exponential(double lambda);
    static double Pareto(double x_mean, double k);
    static double Heviside(double x);
    static double Phi_CDF(double x, double mu, double sigma);
    static double Phi_PDF(double x, double mu, double sigma);
    static double Poisson_CDF(size_t x, double lambda);
    static double Poisson_PDF(size_t x, double lambda);
    static double Pareto_CDF(double x, double x_min, double k);
    static double Pareto_PDF(double x, double x_min, double k);
private:
    static gsl_rng * generator;
};

#endif

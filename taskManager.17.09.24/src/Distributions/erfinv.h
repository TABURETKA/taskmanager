#ifndef _ERFINV_H_
#define _ERFINV_H_

#ifndef M_SQRT2
#    define M_SQRT2    1.41421356237309504880
#endif // M_SQRT2

extern "C" {
double erfinv (double x);
}
#endif
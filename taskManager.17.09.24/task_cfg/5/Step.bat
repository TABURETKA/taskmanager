@echo Off
SETLOCAL ENABLEDELAYEDEXPANSION

MD STEP_%1
COPY fisher_dist.txt STEP_%1\fisher_dist.txt	
ECHO STEP_%1=task_cfg/5/STEP_%1/taskStep%1.cfg >> task.cfg

CD STEP_%1

ECHO # set log level >> taskStep%1.cfg 
ECHO LOG_LEVEL=10 >> taskStep%1.cfg 
ECHO # set name of outputfile >> taskStep%1.cfg 
ECHO OUTPUT_FILENAME=task_cfg/5/STEP_%1/outstep1 >> taskStep%1.cfg 
ECHO # type of input data >> taskStep%1.cfg 
ECHO DATA_SET_TYPE=SINGLE >> taskStep%1.cfg 
ECHO # set name of inputfile >> taskStep%1.cfg 
ECHO DATA_SET_NAME= >> taskStep%1.cfg 
ECHO # set repo index of inputdata for this estimator/step >> taskStep%1.cfg 
ECHO REPO_ENTRIES = [ 1 [ 0 ] ] >> taskStep%1.cfg 
ECHO #set estimator type >> taskStep%1.cfg 
ECHO ESTIMATOR_TYPE=SIMPLEESTIMATOR >> taskStep%1.cfg 
ECHO # set estimator >> taskStep%1.cfg 
ECHO ESTIMATOR_NAME=PW_PolynomialRegressionEstimator >> taskStep%1.cfg 
ECHO #set estimator cfg file >> taskStep%1.cfg 
ECHO ESTIMATOR_PARAMS_SRC=task_cfg/5/STEP_%1/PW_regression.cfg >> taskStep%1.cfg 
ECHO #set model type >> taskStep%1.cfg 
ECHO MODEL_TYPE=POLYNOMIAL_MODEL >> taskStep%1.cfg 
ECHO #set model >> taskStep%1.cfg 
ECHO MODEL_NAME=POLYNOMIAL_MODEL >> taskStep%1.cfg 
ECHO #set model cfg file >> taskStep%1.cfg 
ECHO MODEL_PARAMS_SRC=task_cfg/5/STEP_%1/polynomial_model.cfg >> taskStep%1.cfg 

ECHO ForecastStartPointIndex=%2 >> PW_regression.cfg 
ECHO TrainingSampleSize=75 >> PW_regression.cfg
ECHO ForecastHorizont=15 >> PW_regression.cfg
ECHO #StructuralChangeTesterName=NextResidualAsGaussianOutlierTester >> PW_regression.cfg
ECHO StructuralChangeTesterName=GregoryChowTester >> PW_regression.cfg
ECHO #StructuralChangeTesterCfgFileName=task_cfg/5/STEP_%1/GaussianOutlierTester.cfg >> PW_regression.cfg
ECHO StructuralChangeTesterCfgFileName=task_cfg/5/STEP_%1/GregoryChowTest.cfg >> PW_regression.cfg

ECHO DIMENSION=4 >> polynomial_model.cfg 
ECHO DistFile=task_cfg/5/STEP_%1/fisher_dist.txt >> GregoryChowTest.cfg
ECHO NewSegmentSelectorCoefficient=3.0 >> GaussianOutlierTester.cfg

CD ..

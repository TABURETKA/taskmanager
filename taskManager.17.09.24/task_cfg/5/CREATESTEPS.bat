@echo Off
SETLOCAL

MD STEP_0
CD STEP_0
ECHO # set log level > taskStep0.cfg
ECHO LOG_LEVEL=10 \  >> taskStep0.cfg
ECHO    \  >> taskStep0.cfg
ECHO    \  >> taskStep0.cfg
ECHO # name of file for data loader params >> taskStep0.cfg
ECHO DATA_LOADER_PARAMS_SRC_ITEM=task_cfg/5/STEP_0/data_loader.cfg >> taskStep0.cfg

ECHO # type of input data > data_loader.cfg
ECHO DATA_SET_TYPE_ITEM=MULTIPLE >> data_loader.cfg
ECHO # name of inputfile (dataset filename) >> data_loader.cfg
ECHO DATA_SET_FILENAME_ITEM=task_data/5/tracks.txt >> data_loader.cfg

CD ..

ECHO STEP_0=task_cfg/5/STEP_0/taskStep0.cfg > task.cfg


SET /A Q=1024
FOR /L %%f IN (1,1,22) DO (

call Step.bat %%f !Q!
SET /A Q+=1

)
PAUSE